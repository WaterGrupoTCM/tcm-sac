﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WaterSyncLite.MDL;
using WaterSyncLite.DAL;
using WaterSyncLite.Class;

using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.IO;
using System.Xml;
using System.Xml.XPath;
using System.Data.SqlClient;
using System.Globalization;
//using ICSharpCode.SharpZipLib.Zip;
//using ICSharpCode.SharpZipLib.GZip;
using System.Runtime.InteropServices;
//using ICSharpCode.SharpZipLib.Checksums;
//using ICSharpCode.SharpZipLib.BZip2;
//using ICSharpCode.SharpZipLib.Zip.Compression;
//using ICSharpCode.SharpZipLib.Zip.Compression.Streams;

using Org.BouncyCastle.Crypto.Tls;
using System.Threading;
using System.Net;


public partial class ViewExport : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

        try
        {
            this.Master.LoadHeader(this.Master, Request.Path.Substring(Request.Path.LastIndexOf("/") + 1));
            if (!IsPostBack)
            {
                //string script = "$(document).ready(function () { $('[id*=btnReceber]').click(); });";
                //ClientScript.RegisterStartupScript(this.GetType(), "load", script, true);
                //string script = "$(document).ready(function () { $('[id*=btnReceber]').click(); });";
                //ClientScript.RegisterStartupScript(this.GetType(), "click", script, true);
                //System.Web.UI.ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "AlertBox", "alert('Your Message');", true);

                divComponentes.Visible = false;

                string x;
                CidadeDAL cidDAL = new CidadeDAL();
                Cidade cid = new Cidade();
                //ShowMessage("Lucas");
                Empresa empresa = new Empresa(Convert.ToInt32(Request.Cookies["CliID"].Value.ToString().Trim().Trim()), Convert.ToInt32(Request.Cookies["EmpID"].Value.ToString().Trim().Trim()), "");
                txtDirExp.Text = Server.MapPath("~/EXPORT/" + Request.Cookies["CliID"].Value.ToString().PadLeft(2, '0'));
                txtSeparador.Text = xml.GetConfigValue(Server.MapPath("~"), "LOCAL", "EXPSEPARADOR", Convert.ToInt32(Request.Cookies["CliID"].Value.ToString().Trim()).ToString());
                LeituristaDAL ltr = new LeituristaDAL();
                //ltr.listarLeituritas(empresa, cbxLeit);
                cid = cidDAL.getCid(Convert.ToInt32(Request.Cookies["CliID"].Value.ToString().Trim()), carregaConexao());



                bool inicio = false;
                if (Request.Cookies["CliID"].Value.ToString().Trim().Trim() == "17")
                    CarregaLivrosSaoCaetano();
                else
                {
                    if (Convert.ToInt32(Request.Cookies["CliID"].Value.ToString().Trim().Trim()) == 5)
                        chkUnico.Checked = true;
                    CarregaLivros();

                }
            }

        }
        catch (Exception ex)
        {
            ShowMessage("Erro - " + ex.Message);
        }

    }

    private void CarregaLivros()
    {
        try
        {
            string sql;
            if (Request.Cookies["EmpID"].Value.ToString().Trim().Trim() != "0")
            {
                sql = "SELECT LIV_MES, CID_ID, LOC_ID, LIV_ANO FROM WTR_LIVROS "
                    + " WHERE CLI_ID = " + Request.Cookies["CliID"].Value.ToString().Trim().Trim()
                    + " AND EMP_ID = " + Request.Cookies["EmpID"].Value.ToString().Trim().Trim();

                if (!chkTodos.Checked)
                    sql += " AND LIV_FLAG = 'S'";

                if (Request.Cookies["mes"].Value.ToString().Trim().Trim() != "0")
                {
                    sql += " AND LIV_MES = " + Request.Cookies["mes"].Value.ToString().Trim().Trim();
                }

                if (Request.Cookies["ano"].Value.ToString().Trim().Trim().Trim().Length > 0)
                {
                    sql += " AND LIV_ANO = " + Request.Cookies["ano"].Value.ToString().Trim().Trim().Trim();
                }



                sql += " ORDER BY LIV_MES, CID_ID, LOC_ID";

                lstDirExp.Items.Clear();
                Data db = new Data();
                DataTable dt = db.GetDt(carregaConexao(), "Livros", sql);

                if (dt.Rows.Count > 0)
                {
                    for (int x = 0; x < dt.Rows.Count; x++)
                    {
                        #region TEMP
                        /*
								if (chkUnico.Checked)
								{
									string nomeArq = xml.GetConfigValue("LOCAL", "ARQEXPORT");

									string[] pedacos = nomeArq.Split(';');

									if (pedacos.Length == 3)
									{
										lstDirExp.Items.Add(dt.Rows[x][pedacos[0].Trim()].ToString().Trim().PadLeft(3,'0')
											+ dt.Rows[x][pedacos[1].Trim()].ToString().Trim().PadLeft(3,'0')
											+ dt.Rows[x][pedacos[2].Trim()].ToString().Trim().PadLeft(3,'0'));
									}
								}
								else
								{                                
									lstDirExp.Items.Add(dt.Rows[x]["LIV_MES"].ToString().Trim().PadLeft(3,'0')
										+ dt.Rows[x]["CID_ID"].ToString().Trim().PadLeft(3,'0')
										+ dt.Rows[x]["LOC_ID"].ToString().Trim().PadLeft(3,'0'));
								}
								*/
                        #endregion

                        lstDirExp.Items.Add(dt.Rows[x]["LIV_MES"].ToString().Trim().PadLeft(3, '0')
                            + dt.Rows[x]["CID_ID"].ToString().Trim().PadLeft(3, '0')
                            + dt.Rows[x]["LOC_ID"].ToString().Trim().PadLeft(3, '0'));
                    }
                }
                else
                {
                    ShowMessage("Nenhum livro encontrado.");
                }
            }
            else
            {
                lstDirExp.Items.Clear();
            }

        }
        catch (Exception ex)
        {
            ShowMessage("Informação: " + ex.Message);
        }
    }

    private void CarregaLivrosSaoCaetano()
    {
        try
        {
            string sql;

            if (Request.Cookies["EmpID"].Value.ToString().Trim().Trim() != "0")
            {
                sql = "SELECT LIV_MES, CID_ID, GRUPO_ID, LIV_ANO FROM WTR_LIVROS "
                    + " WHERE CLI_ID = " + Request.Cookies["CliID"].Value.ToString().Trim().Trim()
                    + " AND EMP_ID = " + Request.Cookies["EmpID"].Value.ToString().Trim().Trim();

                if (!chkTodos.Checked)
                    sql += " AND LIV_FLAG = 'S'";

                if (Request.Cookies["mes"].Value.ToString().Trim().Trim() != "0")
                {
                    sql += " AND LIV_MES = " + Request.Cookies["mes"].Value.ToString().Trim().Trim();
                }

                if (Request.Cookies["ano"].Value.ToString().Trim().Trim().Trim().Length > 0)
                {
                    sql += " AND LIV_ANO = " + Request.Cookies["ano"].Value.ToString().Trim().Trim().Trim();
                }

                sql += " GROUP BY GRUPO_ID,LIV_MES,LIV_ANO, CID_ID";

                sql += " ORDER BY LIV_MES, CID_ID";

                lstDirExp.Items.Clear();
                Data db = new Data();
                DataTable dt = db.GetDt(carregaConexao(), "Livros", sql);

                if (dt.Rows.Count > 0)
                {
                    for (int x = 0; x < dt.Rows.Count; x++)
                    {
                        #region TEMP
                        /*
								if (chkUnico.Checked)
								{
									string nomeArq = xml.GetConfigValue("LOCAL", "ARQEXPORT");

									string[] pedacos = nomeArq.Split(';');

									if (pedacos.Length == 3)
									{
										lstDirExp.Items.Add(dt.Rows[x][pedacos[0].Trim()].ToString().Trim().PadLeft(3,'0')
											+ dt.Rows[x][pedacos[1].Trim()].ToString().Trim().PadLeft(3,'0')
											+ dt.Rows[x][pedacos[2].Trim()].ToString().Trim().PadLeft(3,'0'));
									}
								}
								else
								{                                
									lstDirExp.Items.Add(dt.Rows[x]["LIV_MES"].ToString().Trim().PadLeft(3,'0')
										+ dt.Rows[x]["CID_ID"].ToString().Trim().PadLeft(3,'0')
										+ dt.Rows[x]["LOC_ID"].ToString().Trim().PadLeft(3,'0'));
								}
								*/
                        #endregion

                        lstDirExp.Items.Add(dt.Rows[x]["LIV_MES"].ToString().Trim().PadLeft(3, '0')
                            + dt.Rows[x]["CID_ID"].ToString().Trim().PadLeft(3, '0')
                            + dt.Rows[x]["GRUPO_ID"].ToString().Trim().PadLeft(3, '0'));
                    }
                }
                else
                {
                    ShowMessage("Nenhum livro encontrado.");
                }
            }
            else
            {
                lstDirExp.Items.Clear();
            }

        }
        catch (Exception ex)
        {
            ShowMessage("Informação: " + ex.Message);
        }
    }

    protected void ShowMessage(string msg)
    {
        String scriptString = "<script language = JavaScript>";
        scriptString += "alert('" + msg + "');";
        scriptString += "</script>";
        Page.ClientScript.RegisterStartupScript(Page.GetType(), "scrt", scriptString);
    }

    protected String carregaConexao()
    {
        String StrCon = "";
        if (Request.Cookies["StrCon"] != null)
        {
            if (Request.Cookies["StrCon"].Value.ToString().Length > 0)
                StrCon = Request.Cookies["StrCon"].Value.ToString();
        }
        return StrCon.Replace('|', ';').Replace("usuario", xml.GetConexaoValue(Server.MapPath("~"), "LOCAL", "USER")).Replace("senha", xml.GetConexaoValue(Server.MapPath("~"), "LOCAL", "PASSWORD"));
    }

    protected void btnReceber_Click(object sender, EventArgs e)
    {

        try
        {

            btDownload.Enabled = false;
            btDownload.Text = "Download";
            GerarArquivo();

            String scriptString = "<script language = JavaScript>";
            scriptString += "HideProgress();";
            scriptString += "</script>";
            Page.ClientScript.RegisterStartupScript(Page.GetType(), "scrt", scriptString);



        }
        catch { }
        finally
        {

        }

    }




    private void GerarArquivo()
    {
        try
        {
            int x = 0;
            while (x < lstDirExp.GetSelectedIndices().Length)
            {
                /*for (int y = 0; y < lstDirBkp.Items.Count; y++)
                 {
                     if (lstDirExp.SelectedItems[x].ToString().Trim() == lstDirBkp.Items[y].ToString().Trim())
                     {
                         x++;
                     }
                 }*/
                if (x >= lstDirExp.GetSelectedIndices().Length)
                {
                    txtStatus.Text += "Exportação finalizada com sucesso - "
                        + DateTime.Now.Hour + ":" + DateTime.Now.Minute + ":" + DateTime.Now.Second + "\n";
                    break;
                }
                else
                {
                    try
                    {

                        txtStatus.Text += ("Início do Processo de exportação "
                            + lstDirExp.SelectedItem.ToString().Trim() + " - "
                            + DateTime.Now.Hour + ":" + DateTime.Now.Minute + ":" + DateTime.Now.Second) + "\n";
                        if (Request.Cookies["CliID"].Value.ToString().Trim().Trim() == "17")
                        {
                            SaoCaetano saoCaetano = new SaoCaetano();

                            saoCaetano.Export(txtDirExp.Text.Trim(), lstDirExp.SelectedItem.ToString().Trim(), Request.Cookies["CliID"].Value.ToString().Trim(), Server.MapPath("~"), carregaConexao(), Response, btDownload, Convert.ToInt16(Request.Cookies["mes"].Value.ToString().Trim()), Convert.ToInt32(Request.Cookies["ano"].Value.ToString().Trim()));
                        }
                        else if (Request.Cookies["CliID"].Value.ToString().Trim().Trim() == "16")
                        {
                            Cosmopolis cosmopolis = new Cosmopolis();
                            cosmopolis.Export(txtDirExp.Text.Trim(), lstDirExp.SelectedItem.ToString().Trim(), Request.Cookies["CliID"].Value.ToString().Trim(), Server.MapPath("~"), carregaConexao(), Response, Convert.ToInt16(Request.Cookies["mes"].Value.ToString().Trim()), Request.Cookies["ano"].Value.ToString().Trim(), btDownload);
                        }
                        else
                        {
                            if (Convert.ToInt16(Request.Cookies["CliID"].Value.ToString()) == 5)
                            {
                                Marilia marilia = new Marilia();
                                marilia.Export(txtDirExp.Text.Trim(), lstDirExp.SelectedItem.ToString().Trim(), Request.Cookies["CliID"].Value.ToString().Trim(), Server.MapPath("~"), carregaConexao(), Response, Convert.ToInt16(Request.Cookies["mes"].Value.ToString().Trim()), Request.Cookies["ano"].Value.ToString().Trim(), btDownload);

                            }
                            else
                            {
                                if (Convert.ToInt16(Request.Cookies["CliID"].Value.ToString()) == 14)
                                {
                                    Clementina clementina = new Clementina();
                                    clementina.Export(txtDirExp.Text.Trim(), lstDirExp.SelectedItem.ToString().Trim(), Request.Cookies["CliID"].Value.ToString().Trim(), Server.MapPath("~"), carregaConexao(), Response, Convert.ToInt16(Request.Cookies["mes"].Value.ToString().Trim()), Request.Cookies["ano"].Value.ToString().Trim(), btDownload);
                                }
                                else if (Convert.ToInt16(Request.Cookies["CliID"].Value.ToString()) == 9)
                                {
                                    Chavantes chavantes = new Chavantes();
                                    chavantes.Export(txtDirExp.Text.Trim(), lstDirExp.SelectedItem.ToString().Trim(), Request.Cookies["CliID"].Value.ToString().Trim(), Server.MapPath("~"), carregaConexao(), Response, Convert.ToInt16(Request.Cookies["mes"].Value.ToString().Trim()), Request.Cookies["ano"].Value.ToString().Trim(), btDownload);
                                }
                                else if (Convert.ToInt16(Request.Cookies["CliID"].Value.ToString()) == 1)
                                {
                                    Pompeia pompeia = new Pompeia();
                                    pompeia.Export(txtDirExp.Text.Trim(), lstDirExp.SelectedItem.ToString().Trim(), Request.Cookies["CliID"].Value.ToString().Trim(), Server.MapPath("~"), carregaConexao(), Response, Convert.ToInt16(Request.Cookies["mes"].Value.ToString().Trim()), Request.Cookies["ano"].Value.ToString().Trim(), btDownload);
                                }
                                else if (Convert.ToInt16(Request.Cookies["CliID"].Value.ToString()) == 7)
                                {
                                    RioPardo RioPardo = new RioPardo();
                                    RioPardo.Export(txtDirExp.Text.Trim(), lstDirExp.SelectedItem.ToString().Trim(), Request.Cookies["CliID"].Value.ToString().Trim(), Server.MapPath("~"), carregaConexao(), Response, Convert.ToInt16(Request.Cookies["mes"].Value.ToString().Trim()), Request.Cookies["ano"].Value.ToString().Trim(), btDownload);
                                }
                                else if (Convert.ToInt16(Request.Cookies["CliID"].Value.ToString()) == 18)
                                {
                                    Iracemapolis iracempapolis = new Iracemapolis();
                                    iracempapolis.Export(txtDirExp.Text.Trim(), lstDirExp.SelectedItem.ToString().Trim(), Request.Cookies["CliID"].Value.ToString().Trim(), Server.MapPath("~"), carregaConexao(), Response, Convert.ToInt16(Request.Cookies["mes"].Value.ToString().Trim()), Request.Cookies["ano"].Value.ToString().Trim(), btDownload);
                                }
                                else if (Convert.ToInt16(Request.Cookies["CliID"].Value.ToString()) == 2)
                                {
                                    Rinopolis rinopolis = new Rinopolis();
                                    rinopolis.Export(txtDirExp.Text.Trim(), lstDirExp.SelectedItem.ToString().Trim(), Request.Cookies["CliID"].Value.ToString().Trim(), Server.MapPath("~"), carregaConexao(), Response, Convert.ToInt16(Request.Cookies["mes"].Value.ToString().Trim()), Request.Cookies["ano"].Value.ToString().Trim(), btDownload);
                                }
                                else if (Convert.ToInt16(Request.Cookies["CliID"].Value.ToString()) == 19)
                                {
                                    string retorno="0";
                                    CasaBranca casabranca = new CasaBranca();
                                    retorno = casabranca.Export(txtDirExp.Text.Trim(), lstDirExp.SelectedItem.ToString().Trim(), Request.Cookies["CliID"].Value.ToString().Trim(), Server.MapPath("~"), carregaConexao(), Response, Convert.ToInt16(Request.Cookies["mes"].Value.ToString().Trim()), Request.Cookies["ano"].Value.ToString().Trim(), btDownload, retorno);
                                    if (Convert.ToInt32(retorno.Split(',')[0]) != 0)
                                    {
                                        if (Convert.ToInt32(retorno.Split(',')[0]) == 1)
                                        {
                                            ShowMessage("A Rota " + Convert.ToInt32(retorno.Split(',')[1]) + " Contém " + Convert.ToInt32(retorno.Split(',')[0]) + " Ligação sem Leitura. Por Favor, Faça a Leitura antes de Fazer a Exportação");
                                        }
                                        else
                                        {
                                            ShowMessage("A Rota " + Convert.ToInt32(retorno.Split(',')[1]) + " Contém " + Convert.ToInt32(retorno.Split(',')[0]) + " Ligações sem Leitura. Por Favor, Faça a Leitura antes de Fazer a Exportação");
                                        }

                                    }

                                }
                                //else PutFile(txtDirExp.Text.Trim(), lstDirExp.SelectedItem.ToString().Trim());
                            }
                        }

                        x++;

                        txtStatus.Text += "Exportação finalizada com sucesso - "
                        + DateTime.Now.Hour + ":" + DateTime.Now.Minute + ":" + DateTime.Now.Second + "\n";

                    }
                    catch (Exception ex)
                    {
                        txtStatus.Text += ("Erro ao gerar arquivo de exportação - " + ex.Message) + "\n";
                    }


                }
            }
        }
        catch (Exception ex)
        {
            txtStatus.Text += ("Informação: " + ex.Message) + "\n";
        }

    }


    public static string ReturnFiletype(string fileExtension)
    {
        switch (fileExtension)
        {
            case ".htm":
            case ".html":
            case ".log":
                return "text/HTML";
            case ".txt":
                return "text/plain";
            case ".doc":
                return "application/ms-word";
            case ".tiff":
            case ".tif":
                return "image/tiff";
            case ".asf":
                return "video/x-ms-asf";
            case ".avi":
                return "video/avi";
            case ".zip":
                return "application/zip";
            case ".xls":
            case ".csv":
                return "application/vnd.ms-excel";
            case ".gif":
                return "image/gif";
            case ".jpg":
            case "jpeg":
                return "image/jpeg";
            case ".bmp":
                return "image/bmp";
            case ".wav":
                return "audio/wav";
            case ".mp3":
                return "audio/mpeg3";
            case ".mpg":
            case "mpeg":
                return "video/mpeg";
            case ".rtf":
                return "application/rtf";
            case ".asp":
                return "text/asp";
            case ".pdf":
                return "application/pdf";
            case ".fdf":
                return "application/vnd.fdf";
            case ".ppt":
                return "application/mspowerpoint";
            case ".dwg":
                return "image/vnd.dwg";
            case ".msg":
                return "application/msoutlook";
            case ".xml":
            case ".sdxl":
                return "application/xml";
            case ".xdp":
                return "application/vnd.adobe.xdp+xml";
            default:
                return "application/octet-stream";
        }
    }

    protected void btDownload_Click(object sender, EventArgs e)
    {
        string filepath = "";

        filepath = txtDirExp.Text.Trim() + @"\" + btDownload.Text.Split(':')[1].Trim();

        DownloadFile(filepath);


        if (Convert.ToInt32(Request.Cookies["CliID"].Value.ToString().Trim()) == 1)
        {
            if (btDownload.Text.Contains("FATURA"))
                btDownload.Text = btDownload.Text.Replace("FATURA", "LEITURA");
            else
                btDownload.Text = btDownload.Text.Replace("LEITURA", "FATURA");

        }

        Response.End();



    }

    protected void DownloadFile(string filepath)
    {
        // Create New instance of FileInfo class to get the properties of the file being downloaded
        FileInfo arquivo = new FileInfo(filepath);

        // Checking if file exists
        if (arquivo.Exists)
        {
            // Clear the content of the response
            Response.ClearContent();

            // LINE1: Add the file name and attachment, which will force the open/cance/save dialog to show, to the header
            Response.AddHeader("Content-Disposition", String.Format("attachment; filename={0}", arquivo.Name));

            // Add the file size into the response header
            Response.AddHeader("Content-Length", arquivo.Length.ToString());

            // Set the ContentType
            Response.ContentType = ReturnFiletype(arquivo.Extension.ToLower());

            // Write the file into the response (TransmitFile is for ASP.NET 2.0. In ASP.NET 1.1 you have to use WriteFile instead)
            Response.TransmitFile(arquivo.FullName);

            // End the response

            Response.Flush();
        }

        // para cima da lista
        //////////////////////
    }
}