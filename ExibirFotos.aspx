﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="ExibirFotos.aspx.cs" Inherits="ExibirFotos" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">


    <link href="Styles/prettyPhoto.css" rel="stylesheet" />
    <!-- Arquivos utilizados pelo jQuery lightBox plugin -->

    <script src="Scripts/jquery-1.3.2.min.js" type="text/javascript"></script>
    <script src="Scripts/jquery-1.4.1.min.js" type="text/javascript"></script>
    <script src="Scripts/jquery.prettyPhoto.js" type="text/javascript"></script>

    <script src="Scripts/jsLB/jquery-1.7.2.min.js" type="text/javascript"></script>
    <script src="Scripts/jsLB/lightbox.js" type="text/javascript"></script>

    <link href="Styles/lightbox.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript">
        $(function () {
            $('#gallery a').lightBox();
        });
    </script>

    <!-- jQuery lightBox plugin - Gallery style */ -->
    <style type="text/css">
        #gallery {
            background-color: #fff;
            padding: 10px;
            width: auto;
        }

            #gallery ul {
                list-style: none;
            }

                #gallery ul li {
                    display: inline;
                }

                #gallery ul img {
                    border: 5px solid #444444;
                    border-width: 5px 5px 20px;
                }

                #gallery ul a:hover img {
                    border: 5px solid #fff;
                    border-width: 5px 5px 20px;
                    color: #fff;
                }

                #gallery ul a:hover {
                    color: #fff;
                }
    </style>
    <style type="text/css">
        .auto-style1 {
            font-size: large;
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">

    <strong><span class="auto-style1">Fotos

    <br />

    </span></strong>

    <div class="imageRow">
        <div class="set">
            <asp:DataList ID="galeriaDataList" RepeatColumns="5" runat="server">
                <ItemTemplate>
                    <a href="<%# Eval("Name","Fotos/"+Request.Cookies["CliID"].Value.PadLeft(2,'0')+"/{0}")%>" rel="lightbox[plants]" title="Clique do lado direito para ir para a próxima imagem.">
                        <img src='<%# Eval("Name","Fotos/"+Request.Cookies["CliID"].Value.PadLeft(2,'0')+"/{0}")%>' width="200" height="200" alt='<%# Eval("Name") %>' />
                    </a>
                    <br/>
                    
                   <div style="text-align: center">
                        <asp:Label ID="lblReferenciaFoto" runat="server" Text='<%#(Eval("Name").ToString().Length < 39 && Eval("Name").ToString().Length  < 18 ) ? 
                        Eval("Name").ToString().Substring(0, 2) + "/" + Eval("Name").ToString().Substring(2, 4):
                        (Eval("Name").ToString().Length <=43 ) ? 
                        Eval("Name").ToString().Substring(6, 2) + "/" + Eval("Name").ToString().Substring(8, 4):
                        Eval("Name").ToString().Substring(34, 2)+"/"
                        +Eval("Name").ToString().Substring(32, 2) + "/" 
                        + Eval("Name").ToString().Substring(28, 4) + " "
                        + Eval("Name").ToString().Substring(36, 2)+":"
                        + Eval("Name").ToString().Substring(38, 2)+":"
                        + Eval("Name").ToString().Substring(40, 2) %>'></asp:Label></div>

                </ItemTemplate>
            </asp:DataList>

        </div>
    </div>

    <asp:Panel ID="PanelRetidas" runat="server" Visible="True">
        <asp:Repeater runat="server" ID="Repeater1">
            <ItemTemplate>

                <div>
                    <hr>
                    <strong>Código DAE:</strong>
                    <%# DataBinder.Eval(Container.DataItem, "LEI_LIGACAO") %>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Rota Leitura:</strong>
                    <%# DataBinder.Eval(Container.DataItem, "GRUPO_ID") %>-<%# DataBinder.Eval(Container.DataItem, "LEI_SEQUENCIA") %>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Situação Leitura:</strong>
                    <%# DataBinder.Eval(Container.DataItem, "SITUACAO") %>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Leiturista:</strong>
                    <%# DataBinder.Eval(Container.DataItem, "LTR_NOME") %>
                    <br />
                    <strong>Hidrômetro:</strong>
                    <%# DataBinder.Eval(Container.DataItem, "HD") %>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Ocorrência:</strong>
                    <%# DataBinder.Eval(Container.DataItem, "GRUPO_ID") %>-<%# DataBinder.Eval(Container.DataItem, "OCORRENCIA") %><br />
                    <br />
                    <strong>Nome:</strong>
                    <%# DataBinder.Eval(Container.DataItem, "LEI_CONSUMIDOR") %>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Categoria:</strong>
                    <%# DataBinder.Eval(Container.DataItem, "CATEGORIA") %>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Economias:</strong>
                    <%# DataBinder.Eval(Container.DataItem, "LEI_ECONOMIA") %>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>TL:</strong>
                    <%# DataBinder.Eval(Container.DataItem, "LEI_LANCAMENTO") %>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Conta retida:</strong>
                    <%# DataBinder.Eval(Container.DataItem, "LEI_RETERCONTA") %>
                    <br />
                    <strong>Endereço:</strong>
                    <%# DataBinder.Eval(Container.DataItem, "ENDERECO") %>
                    <br />
                    <strong>Leitura Anterior:</strong>
                    <%# DataBinder.Eval(Container.DataItem, "LEI_ANTERIOR") %>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Leitura Atual:</strong>
                    <%# DataBinder.Eval(Container.DataItem, "LEI_LEITURA") %>
                    <br />
                    <strong>Consumo Medido:</strong>
                    <%# DataBinder.Eval(Container.DataItem, "LEI_CONSUMO") %>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Média:</strong>
                    <%# DataBinder.Eval(Container.DataItem, "MEDIA") %>
                </div>

            </ItemTemplate>

        </asp:Repeater>
    </asp:Panel>









    <div>
    </div>


</asp:Content>

