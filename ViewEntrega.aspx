﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="ViewEntrega.aspx.cs" Inherits="ViewEntrega" EnableEventValidation="false" %>

<%@ MasterType VirtualPath="~/Site.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

    <link href="Content/lightbox.css" rel="stylesheet" />
    <link href="Content/prettyPhoto.css" rel="stylesheet" />
    <link href="Content/StyleSheet.css" rel="stylesheet" />

    <style type="text/css">
        .minha-classe {
            max-width: 200px;
        }

        .GridPager a, .GridPager span {
            display: block;
            height: 20px;
            width: 30px;
            font-weight: bold;
            text-align: center;
            text-decoration: none;
        }

        .GridPager a {
            background-color: #f5f5f5;
            color: #969696;
            border: 1px solid #969696;
        }

        .GridPager span {
            background-color: dodgerblue;
            color: #000;
            border: 1px solid orangered;
        }

        @media print {


            .row {
                margin-right: -15px;
                margin-left: -15px;
                padding-top: 0px;
                font-size: x-small;
            }

            col-1, .col-2, .col-3, .col-4, .col-5, .col-6, .col-7, .col-8, .col-9, .col-10, .col-11, .col-12, .col-sm-1, .col-sm-2, .col-sm-3, .col-sm-4, .col-sm-5, .col-sm-6, .col-sm-7, .col-sm-8, .col-sm-9, .col-sm-10, .col-sm-11, .col-sm-12, .col-lg-1, .col-lg-2, .col-lg-3, .col-lg-4, .col-lg-5, .col-lg-6, .col-lg-7, .col-lg-8, .col-lg-9, .col-lg-10, .col-lg-11, .col-lg-12 {
                position: relative;
                min-height: 1px;
                padding-right: 15px;
                padding-left: 15px;
                top: 0px;
                left: 0px;
            }

            .col-1, .col-2, .col-3, .col-4, .col-5, .col-6, .col-7, .col-8, .col-9, .col-10, .col-11, .col-12 {
                float: left;
            }

            .col-1 {
                width: 8.333333333333332%;
            }

            .col-2 {
                width: 16.666666666666664%;
            }

            .col-3 {
                width: 25%;
            }

            .col-4 {
                width: 33.33333333333333%;
            }

            .col-5 {
                width: 41.66666666666667%;
            }

            .col-6 {
                width: 50%;
            }

            .col-7 {
                width: 58.333333333333336%;
            }

            .col-8 {
                width: 66.66666666666666%;
            }

            .col-9 {
                width: 75%;
            }

            .col-10 {
                width: 83.33333333333334%;
            }

            .col-11 {
                width: 91.66666666666666%;
            }

            .col-12 {
                width: 100%;
            }

            td {
                padding: 0px;
            }

            .divbutton {
                /*height: 100px;*/
            }

            label {
                margin-bottom: 0px;
                font-weight: inherit;
            }

            labelPesquisa {
                margin-bottom: 0px;
                font-weight: inherit;
            }

            th {
                text-align: center;
            }

            td {
                padding: 0px;
            }

            .FixedHeader {
                position: absolute;
                font-weight: bold;
                vertical-align: text-bottom;
                width: 100%;
            }

            .painelInfo {
                min-height: 120px;
            }


            a.fill-div {
                text-align: center;
                display: block;
                height: 100%;
                width: 100%;
                text-decoration: none;
            }

            .autocomplete_list {
                visibility: visible;
                margin: 0px !important;
                padding: 1px;
                background-color: GrayText;
                color: Black;
                border: buttonshadow;
                border-width: 1px;
                border-style: solid;
                cursor: default;
                text-align: left;
                list-style-type: none;
                font-weight: normal;
                font-family: Arial;
                font-size: 16px;
            }

            .autocomplete_highlighted_listitem {
                background-color: ButtonFace;
                color: Blue;
                padding: 3px;
            }

            .autocomplete_listitem {
                background-color: Window;
                color: Black;
                font-family: Arial;
                font-size: 12px;
                padding: 3px;
            }

            .carousel {
                width: 600px;
                padding-left: 21px;
            }

            /* Indicators list style */
            .article-slide .carousel-indicators {
                bottom: 0;
                left: 0;
                margin-left: 59px;
                width: 100%;
            }
                /* Indicators list style */
                .article-slide .carousel-indicators li {
                    border: medium none;
                    border-radius: 0;
                    float: left;
                    height: 54px;
                    margin-bottom: 5px;
                    margin-left: 0;
                    margin-right: 5px !important;
                    margin-top: 0;
                    width: 100px;
                }
                /* Indicators images style */
                .article-slide .carousel-indicators img {
                    border: 2px solid #FFFFFF;
                    float: left;
                    height: 54px;
                    left: 0;
                    width: 100px;
                }
                /* Indicators active image style */
                .article-slide .carousel-indicators .active img {
                    border: 2px solid #428BCA;
                    opacity: 0.7;
                }

            .ligacao {
            }
        }
    </style>



    <script src="Scripts/jquery-1.4.1.min.js" type="text/javascript"></script>
    <script src="Scripts/jquery.dynDateTime.min.js" type="text/javascript"></script>
    <script src="Scripts/calendar-br.min.js" type="text/javascript"></script>
    <link href="Styles/calendar-blue.css" rel="stylesheet" type="text/css" />
    <link href="Styles/calendar-blue.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript">

        function printDiv(divName) {
            var printContents = document.getElementById(divName).innerHTML;
            var originalContents = document.body.innerHTML;

            document.body.innerHTML = printContents;

            window.print();

            document.body.innerHTML = originalContents;
        }



        function imprime() {
            //you can put your contentID which is you want to print.
            var contents = $("#conteudo").html();
            var frame1 = $('<iframe />');
            frame1[0].name = "frame1";
            frame1.css({ "position": "absolute", "top": "-1000000px" });
            $("body").append(frame1);
            var frameDoc = frame1[0].contentWindow ? frame1[0].contentWindow : frame1[0].contentDocument.document ? frame1[0].contentDocument.document : frame1[0].contentDocument;
            frameDoc.document.open();
            //Create a new HTML document.
            frameDoc.document.write('<html><head><title>DIV Contents</title>');
            frameDoc.document.write('</head><body>');
            //Append the external CSS file.
            frameDoc.document.write('<link href="style.css" rel="stylesheet" type="text/css" />');
            //Append the DIV contents.
            frameDoc.document.write(contents);
            frameDoc.document.write('</body></html>');
            frameDoc.document.close();
            setTimeout(function () {
                window.frames["frame1"].focus();
                window.frames["frame1"].print();
                frame1.remove();
            }, 500);
        }

        function printform() {


            var windowUrl = 'about:blank';
            var uniqueName = new Date();
            var windowName = 'Print' + uniqueName.getTime();
            var printWindow = window.open(windowUrl, windowName, 'left=50000,top=50000,width=0,height=0');

            printWindow.document.write(printContent.innerHTML);
            printWindow.document.close();
            printWindow.focus();
            printWindow.print();
            printWindow.close();
        }
    </script>

    <script type="text/javascript">   
        $(document).ready(function () {
            $(".Calender").dynDateTime({
                showsTime: false,
                ifFormat: "%d/%m/%Y",
                
                align: "pt-BR",
                electric: false,
                singleClick: true,
                displayArea: ".siblings('.dtcDisplayArea')",
                button: ".next()"
            });
        });



    </script>

    <script type="text/javascript">
        function updateAllTextboxes(value) {
            $('input.box-to-change').val(value);
        }
    </script>




</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <form id="form1" runat="server">

        <div id="conteudo">
            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true" runat="server">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h4 class="panel-title"><span class="TitulosPanel">Forma de Entrega</span></h4>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-3">
                                <div class="input-group">
                                    <span class="input-group-addon">Leiturista</span>
                                    <asp:DropDownList ID="ddlLtr" runat="server" CssClass="form-control">
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-sm-6 col-lg-5">
                                <div class="col-sm-4 col-lg-4" style="padding-left: 0px">
                                    <div class="input-group">
                                        <span class="input-group-addon">Mês</span>
                                        <asp:DropDownList ID="cbxMes" runat="server" CssClass="form-control">
                                            <asp:ListItem>Selecione o mês</asp:ListItem>
                                            <asp:ListItem>Janeiro</asp:ListItem>
                                            <asp:ListItem>Fevereiro</asp:ListItem>
                                            <asp:ListItem>Março</asp:ListItem>
                                            <asp:ListItem>Abril</asp:ListItem>
                                            <asp:ListItem>Maio</asp:ListItem>
                                            <asp:ListItem>Junho</asp:ListItem>
                                            <asp:ListItem>Julho</asp:ListItem>
                                            <asp:ListItem>Agosto</asp:ListItem>
                                            <asp:ListItem>Setembro</asp:ListItem>
                                            <asp:ListItem>Outubro</asp:ListItem>
                                            <asp:ListItem>Novembro</asp:ListItem>
                                            <asp:ListItem>Dezembro</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="col-sm-4 col-lg-3" style="padding-right: 0px; padding-left: 10px;">
                                    <div class="input-group">
                                        <span class="input-group-addon" id="">Ano</span>
                                        <asp:TextBox ID="txtAno" runat="server" CssClass="form-control bfh-number"></asp:TextBox>
                                    </div>
                                </div>

                                <div class="col-sm-6 col-lg-5" style="padding-right: 0px; padding-left: 30px">
                                    <div class="input-group">
                                        <span class="input-group-addon">Relatorio</span>
                                        <asp:DropDownList ID="ddlRelatorio" runat="server" CssClass="form-control bfh-number">
                                            <asp:ListItem>Retidas</asp:ListItem>
                                            <asp:ListItem>Protocoladas</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>

                                </div>
                            </div>

                            <div class="col-sm-6 col-lg-3" style="padding-right: 0px; padding-left: 0px">

                                <div class="col-sm-4 col-lg-6" style="padding-right: 0px; padding-left: 10px">
                                    <div class="input-group">
                                        <span class="input-group-addon">Ligação</span>
                                        <asp:TextBox ID="txtIdentificacao" runat="server" CssClass="form-control bfh-number"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="col-sm-4 col-lg-5" style="padding-right: 0px; padding-left: 30px">
                                    <div class="input-group" id="divGrupo" runat="server">
                                        <span class="input-group-addon">Grupo</span>
                                        <asp:TextBox ID="txtGrupo" runat="server" CssClass="form-control bfh-number"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row" style="padding-top: 10px">
                        </div>

                        <div class="row" style="padding-top: 10px">
                            <div class="col-lg-4">
                                <asp:Button ID="btPesquisar" runat="server" Text="Pesquisar" CssClass="btn btn-primary" OnClick="btPesquisar_Click" />
                            </div>
                        </div>

                    </div>
                </div>

                <asp:Panel ID="PanelQtdRegistros" runat="server" Visible="false">
                    <div class="alert alert-info text-center" style="margin-top: 10px;">

                        <div class="row">

                            <div class="col-3">
                            </div>
                            <div class="col-6">
                                <strong style="color: #4f5b69">Registros encontrados: </strong>
                                <asp:Label ID="lblTOTAL" runat="server" ForeColor="#4f5b69" Style="font-weight: 700"></asp:Label>
                            </div>
                            <div class="col-3 text-right">
                                <div class="col-6">
                                </div>
                            </div>
                        </div>

                    </div>

                </asp:Panel>

                <div class="row" style="padding-top: 15px">
                    <div class="col-lg-12">
                        <div id="scrollDiv" style="overflow: auto; height: 100%; width: 100%">

                            <asp:GridView ID="gridFormaEntrega" runat="server" AutoGenerateColumns="False"
                                DataKeyNames="GRUPO_ID" AllowPaging="True" PageSize="50"
                                ShowFooter="True" EmptyDataText="Nenhuma Informação encontrada" CellPadding="3" GridLines="Vertical" BackColor="White"
                                BorderColor="#999999" BorderStyle="None" BorderWidth="1px" Width="100%" OnPageIndexChanging="gridNormal_PageIndexChanging" OnRowDataBound="gridFormaEntrega_RowDataBound">
                                <AlternatingRowStyle BackColor="Gainsboro" />
                                <Columns>

                                  <%--  <asp:TemplateField HeaderText="Sequência" SortExpression="LEI_SEQ" ItemStyle-CssClass="text-center" HeaderStyle-CssClass="text-center">
                                        <EditItemTemplate>
                                            <asp:Label ID="lblSequencia" Text='<%# Eval("LEI_SEQ") %>' runat="server"></asp:Label>
                                        </EditItemTemplate>                                       
                                        <HeaderStyle CssClass="text-center"></HeaderStyle>
                                        <ItemStyle CssClass="text-center"></ItemStyle>
                                    </asp:TemplateField>--%>

                                    <asp:TemplateField HeaderText="Consumidor" ItemStyle-CssClass="text-center" HeaderStyle-CssClass="text-center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblNome" Text='<%# Eval("LEI_CONSUMIDOR") %>' runat="server"></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle CssClass="text-center"></HeaderStyle>
                                        <ItemStyle CssClass="text-center"></ItemStyle>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Endereco" SortExpression="LEI_ENDERECO" ItemStyle-CssClass="text-center" HeaderStyle-CssClass="text-center">
                                        <EditItemTemplate>
                                            <asp:Label ID="lblEndereco" Text='<%# Eval("LEI_ENDERECO") %>' runat="server"></asp:Label>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblEndereco" Text='<%# Eval("LEI_ENDERECO") %>' runat="server"></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle CssClass="text-center"></HeaderStyle>
                                        <ItemStyle CssClass="text-center"></ItemStyle>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Numero" SortExpression="LEI_NUMERO" ItemStyle-CssClass="text-center" HeaderStyle-CssClass="text-center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblNumero" Text='<%# Eval("LEI_NUMERO") %>' runat="server"></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle CssClass="text-center"></HeaderStyle>
                                        <ItemStyle CssClass="text-center"></ItemStyle>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Ligação" SortExpression="LEI_LIGACAO" ItemStyle-CssClass="text-center" HeaderStyle-CssClass="text-center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblLigacao" Text='<%# Eval("LEI_LIGACAO") %>' runat="server"></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle CssClass="text-center"></HeaderStyle>
                                        <ItemStyle CssClass="text-center"></ItemStyle>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Rota" ItemStyle-CssClass="text-center" HeaderStyle-CssClass="text-center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblRota" Text='<%# Eval("LOC_ID") %>' runat="server"></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle CssClass="text-center"></HeaderStyle>
                                        <ItemStyle CssClass="text-center"></ItemStyle>
                                    </asp:TemplateField>

                                     <asp:TemplateField HeaderText="Matricula" SortExpression="LEI_LTRENTREGA" ItemStyle-CssClass="text-center" HeaderStyle-CssClass="text-center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblMatricula" Text='<%# Eval("LEI_LTRENTREGA") %>' runat="server"></asp:Label>
                                        </ItemTemplate>
                                          <ItemTemplate>
                                            <asp:Label ID="lblMatricula" Text='<%# Eval("LEI_LTRENTREGA") %>' runat="server"></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle CssClass="text-center"></HeaderStyle>
                                        <ItemStyle CssClass="text-center"></ItemStyle>
                                    </asp:TemplateField>

                                   <asp:TemplateField HeaderText="Leiturista" SortExpression="ltr_nome" ItemStyle-CssClass="text-center" HeaderStyle-CssClass="text-center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblLeiturista" Text='<%# Eval("LTR_NOME") %>' runat="server"></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle CssClass="text-center"></HeaderStyle>
                                        <ItemStyle CssClass="text-center"></ItemStyle>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Grupo" SortExpression="GRUPO_ID" ItemStyle-CssClass="text-center" HeaderStyle-CssClass="text-center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblHGrupo" Text='<%# Eval("GRUPO_ID") %>' runat="server"></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle CssClass="text-center"></HeaderStyle>
                                        <ItemStyle CssClass="text-center"></ItemStyle>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Mês" ItemStyle-CssClass="text-center" HeaderStyle-CssClass="text-center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblMes" Text='<%# Eval("LIV_MES") %>' runat="server"></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle CssClass="text-center"></HeaderStyle>
                                        <ItemStyle CssClass="text-center"></ItemStyle>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Ano" ItemStyle-CssClass="text-center" HeaderStyle-CssClass="text-center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblAno" Text='<%# Eval("LIV_ANO") %>' runat="server"></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle CssClass="text-center"></HeaderStyle>
                                        <ItemStyle CssClass="text-center"></ItemStyle>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Leiturista"  ItemStyle-CssClass="text-center" HeaderStyle-CssClass="text-center">
                                        <ItemTemplate>
                                            <asp:DropDownList ID="ddlContrato" runat="server" DataTextField="ltr_nome" Style="text-align: center;"
                                                DataValueField="ltr_id" DataSourceID="SqlDataSourceLeituristas" CssClass="form-control" AutoPostBack="True" OnSelectedIndexChanged="ddlLeiturista_SelectedIndexChanged">
                                            </asp:DropDownList>
                                        </ItemTemplate>
                                        <HeaderStyle CssClass="text-center"></HeaderStyle>
                                        <ItemStyle CssClass="text-center"></ItemStyle>
                                    </asp:TemplateField>

                                    
                                    <asp:TemplateField HeaderText="Forma de Entrega" ItemStyle-CssClass="text-center" HeaderStyle-CssClass="text-center">
                                        <ItemTemplate>
                                            <asp:DropDownList ID="ddlEntrega" runat="server" TextField="ent_id" Style="text-align: center;"
                                                DataValueField="ent_descricao" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlEntrega_SelectedIndexChanged" DataSourceID="SqlDataSourceEntrega">                                               
                                            </asp:DropDownList>
                                        </ItemTemplate>
                                        <HeaderStyle CssClass="text-center"></HeaderStyle>
                                        <ItemStyle CssClass="text-center"></ItemStyle>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Data Entrega" ItemStyle-CssClass="tab-pane calendar" HeaderStyle-CssClass="text-center">
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtData" Text='<%# Eval("LEI_DATAENTREGA") %>' runat="server" OnTextChanged="txtDataEntrega_TextChanged" class="Calender"
                                                AutoPostBack="true" Style="text-align: center;" />
                                            <image src="imagens/calendario.png" />
                                        </ItemTemplate>
                                        <HeaderStyle CssClass="text-center"></HeaderStyle>
                                        <ItemStyle CssClass="text-center"></ItemStyle>
                                    </asp:TemplateField>
                                                                       
                                    

                                     <asp:TemplateField HeaderText="Local Entrega" SortExpression="ENT_DESCRICAO" ItemStyle-CssClass="text-center" HeaderStyle-CssClass="text-center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblEntrega" Text='<%# Eval("ENT_DESCRICAO") %>' runat="server"></asp:Label>
                                        </ItemTemplate>
                                          <ItemTemplate>
                                            <asp:Label ID="lblEntrega" Text='<%# Eval("ENT_DESCRICAO") %>' runat="server"></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle CssClass="text-center"></HeaderStyle>
                                        <ItemStyle CssClass="text-center"></ItemStyle>
                                    </asp:TemplateField>


                                    <asp:TemplateField HeaderText="Codigo Entrega" SortExpression="LEI_ENTREGAFATURA" ItemStyle-CssClass="text-center" HeaderStyle-CssClass="text-center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblEntregaId" Text='<%# Eval("LEI_ENTREGAFATURA") %>' runat="server"></asp:Label>
                                        </ItemTemplate>
                                          <ItemTemplate>
                                            <asp:Label ID="lblEntregaId" Text='<%# Eval("LEI_ENTREGAFATURA") %>' runat="server"></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle CssClass="text-center"></HeaderStyle>
                                        <ItemStyle CssClass="text-center"></ItemStyle>
                                    </asp:TemplateField>                                    

                                  
                                </Columns>
                                <FooterStyle BackColor="#CCCCCC" ForeColor="Black" />
                                <HeaderStyle BackColor="#4f5b69" Font-Bold="True" ForeColor="White" />
                                <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Left" CssClass="GridPager" />
                                <RowStyle BackColor="#EEEEEE" ForeColor="Black" />
                                <SelectedRowStyle BackColor="#e1ffff" Font-Bold="True" ForeColor="White" />
                                <SortedAscendingCellStyle BackColor="#F1F1F1"></SortedAscendingCellStyle>
                                <SortedAscendingHeaderStyle BackColor="#0000A9"></SortedAscendingHeaderStyle>
                                <SortedDescendingCellStyle BackColor="#CAC9C9"></SortedDescendingCellStyle>
                                <SortedDescendingHeaderStyle BackColor="#000065"></SortedDescendingHeaderStyle>
                            </asp:GridView>

                        </div>
                    </div>
                </div>



            </div>
            <asp:SqlDataSource ID="SqlDataSourceLeituristas" runat="server"></asp:SqlDataSource>
            <asp:SqlDataSource ID="SqlDataSourceEntrega" runat="server"></asp:SqlDataSource>
            
        </div>
    </form>


</asp:Content>
