﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WaterSyncLite.Class;

public partial class ViewRepasse : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        this.Master.LoadHeader(this.Master, Request.Path.Substring(Request.Path.LastIndexOf("/") + 1));
        
        if (!IsPostBack)
        {

            Data db = new Data();

            txtAno.Text = DateTime.Now.Year.ToString();
            ddlMes.SelectedIndex = DateTime.Now.Month;


            #region LEITURISTAS
            DataTable dt = db.GetDt(carregaConexao(), "WTR_LEIT", "SELECT LTR_ID, LTR_NOME FROM "
                + " WTR_LEITURISTAS WHERE CLI_ID = " + Request.Cookies["CliID"].Value.ToString().Trim()
                + " AND EMP_ID = " + Request.Cookies["EmpID"].Value.ToString().Trim()
                + " ORDER BY LTR_NOME");

            
            if (dt != null)
            {
                ddlLtr.DataSource = dt;
                ddlLtr.DataTextField = "LTR_NOME";
                ddlLtr.DataValueField = "LTR_ID";
                ddlLtr.DataBind();

            }
            #endregion


        }
    }

    protected String carregaConexao()
    {
        String StrCon = "";
        if (Request.Cookies["StrCon"] != null)
        {
            if (Request.Cookies["StrCon"].Value.ToString().Length > 0)
                StrCon = Request.Cookies["StrCon"].Value.ToString();
        }
        return StrCon.Replace('|', ';').Replace("usuario", xml.GetConexaoValue(Server.MapPath("~"), "LOCAL", "USER")).Replace("senha", xml.GetConexaoValue(Server.MapPath("~"), "LOCAL", "PASSWORD"));
    }

    protected void btPesquisar_Click(object sender, EventArgs e)
    {
        try
        {
            PanelMsg.Visible = false;
            Data db = new Data();
            DataTable dt = db.GetDt(carregaConexao(), "WTR_LIVROS", "SELECT LIV_ANO as ANO,LIV_MES as MÊS,GRUPO_ID AS GRUPO,sum(liv_total) as 'TOTAL DE LEITURAS' FROM "
                + " WTR_LIVROS WHERE CLI_ID = " + Request.Cookies["CliID"].Value.ToString().Trim()
                + " AND EMP_ID = " + Request.Cookies["EmpID"].Value.ToString().Trim()
                + " AND LIV_MES = " + ddlMes.SelectedIndex
                + " AND LIV_ANO = " + txtAno.Text
                + " GROUP BY GRUPO_ID,LIV_ANO,LIV_MES"
                + " ORDER BY GRUPO_ID");

            GridView1.DataSource = dt;
            GridView1.DataBind();

            Session["dt"] = dt;

            carregaRepasses();
        }
        catch { }


    }

    protected void carregaRepasses()
    {
        try
        {

            Data db = new Data();
            DataTable dt = db.GetDt(carregaConexao(), "WTR_LIVROS", "SELECT LIV_ANO as ANO,LIV_MES as MÊS,GRUPO_ID AS GRUPO,LOC_ID AS ROTA,LIV_TOTAL as 'TOTAL DE LEITURAS',LIV_NAOLIDA as 'NÃO LIDAS',LIV_LEITURA as LIDAS, LTR_NOME AS LEITURISTA FROM "
                + " WTR_LIVROS INNER JOIN WTR_LEITURISTAS ON (WTR_LIVROS.LTR_ID = WTR_LEITURISTAS.LTR_ID) WHERE WTR_LIVROS.CLI_ID = " + Request.Cookies["CliID"].Value.ToString().Trim()
                + " AND WTR_LIVROS.EMP_ID = " + Request.Cookies["EmpID"].Value.ToString().Trim()
                + " AND LIV_MES = " + ddlMes.SelectedIndex
                + " AND LIV_ANO = " + txtAno.Text
                + " AND LOC_ID > 1000 "
                //+ " GROUP BY GRUPO_ID"
                + " ORDER BY GRUPO_ID");

            GridView2.DataSource = dt;
            GridView2.DataBind();






        }
        catch (Exception ex)
        {
            PanelMsg.Visible = true;
            lblMsg.Text = "Erro ao carregar repasses - " + ex.Message;
        }


    }

    protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            if (e.CommandName == "Repasse")
            {
                Data db = new Data();
                int indexGrid = Convert.ToInt32(e.CommandArgument);
                DataTable dt = (DataTable)Session["dt"];
                DataRow dr = dt.Rows[indexGrid];
                GridView grid =  (GridView) sender;

                DataTable dtVerifica = db.GetDt(carregaConexao(), "WTR_LIVROS", "SELECT * FROM "
            + " WTR_LIVROS WHERE CLI_ID = " + Request.Cookies["CliID"].Value.ToString().Trim()
            + " AND EMP_ID = " + Request.Cookies["EmpID"].Value.ToString().Trim()
            + " AND LIV_MES = " + dr[1].ToString()
            + " AND LIV_ANO = " + dr[0].ToString()
            + " AND LOC_ID = " + (Convert.ToInt32(dr[2].ToString()) + 1000));

                if (dtVerifica.DefaultView.Count > 0)
                {
                    PanelMsg.Visible = true;
                    lblMsg.Text = "O repasse do grupo "+grid.Rows[indexGrid].Cells[3].Text+" já foi gerado, por favor seleione outro grupo de leitura.";
                    return;
                }
                else { PanelMsg.Visible = false; }


                DataTable dtAux = db.GetDt(carregaConexao(), "WTR_LIVROS", "SELECT * FROM "
            + " WTR_LIVROS WHERE CLI_ID = " + Request.Cookies["CliID"].Value.ToString().Trim()
            + " AND EMP_ID = " + Request.Cookies["EmpID"].Value.ToString().Trim()
            + " AND LIV_MES = " + dr[1].ToString()
            + " AND LIV_ANO = " + dr[0].ToString()
            + " AND GRUPO_ID = " + dr[2].ToString()
                    //+ " GROUP BY GRUPO_ID"
            + " ORDER BY GRUPO_ID");
                /*            DataTable dtRota = db.GetDt(carregaConexao(), "WTR_LIVROS",
                            "select * from wtr_livros where liv_mes = " + dr[1].ToString() +
                            " and liv_ano = " + dr[0].ToString() + " and loc_id = " + Convert.ToInt32(dr[3].ToString()));
                            */
                DataTable dtLeituras = db.GetDt(carregaConexao(), "WTR_LEITURAS", "SELECT count(*) as qtdFechadas FROM "
            + " WTR_LEITURAS WHERE CLI_ID = " + Request.Cookies["CliID"].Value.ToString().Trim()
            + " AND EMP_ID = " + Request.Cookies["EmpID"].Value.ToString().Trim()
            + " AND LIV_MES = " + dr[1].ToString()
            + " AND LIV_ANO = " + dr[0].ToString()
            + " AND GRUPO_ID = " + dr[2].ToString()
            + " AND MSG_ID = 17");
                //+ " GROUP BY GRUPO_ID"

                if (dtLeituras.DefaultView.Count > 0)
                {

                    string sql = "INSERT WTR_LIVROS VALUES('"
                                                  + dtAux.DefaultView[0].Row["CLI_ID"].ToString() + "','"
                                                  + dtAux.DefaultView[0].Row["EMP_ID"].ToString() + "','"
                                                  + dtAux.DefaultView[0].Row["LIV_MES"].ToString() + "','"
                                                  + dtAux.DefaultView[0].Row["LIV_MES"].ToString() + "','"
                                                  + dtAux.DefaultView[0].Row["LIV_ANO"].ToString() + "','1','"
                                                  + (Convert.ToInt32(dtAux.DefaultView[0].Row["GRUPO_ID"].ToString()) + 1000).ToString() + "','"
                                                  + "','"
                                                  + DateTime.Now.Date.Day.ToString() + "/" +
                                                  Request.Cookies["mes"].Value.ToString().Trim() + "/" +
                                                  Request.Cookies["ano"].Value.ToString().Trim() + "','"
                                                  + dtLeituras.DefaultView[0].Row["qtdFechadas"].ToString() + "','"
                                                  + "0','"
                                                  + dtLeituras.DefaultView[0].Row["qtdFechadas"].ToString() + "','"
                                                  + "0','"
                                                  + DateTime.Now.ToShortDateString() + "','"
                                                  + ddlLtr.SelectedValue.ToString() + "','"
                                                  + "0','"
                                                  + "0','"
                                                  + dtAux.DefaultView[0].Row["LIV_TIPO"].ToString() + "','"
                                                  + "0','"
                                                  + "N','"
                                                  + dtAux.DefaultView[0].Row["LIV_MSG1"].ToString() + "','"
                                                  + dtAux.DefaultView[0].Row["LIV_MSG2"].ToString() + "','"
                                                  + dtAux.DefaultView[0].Row["LIV_MSG3"].ToString() + "',"
                                                  + dtAux.DefaultView[0].Row["GRUPO_ID"].ToString() + ",'"
                                                  + dtAux.DefaultView[0].Row["LIV_PORCESGOTO"].ToString() + "','"
                                                  + dtAux.DefaultView[0].Row["LIV_VERSAO"].ToString() + "','"
                                                  + dtAux.DefaultView[0].Row["LIV_SEQARQUIVO"].ToString() + "')";


                    db.Exe(carregaConexao(), "wtr_livros", sql);

                    db.Exe(carregaConexao(), "WTR_LEITURAS", "update WTR_LEITURAS set LEI_DATA = '',loc_id = " + (Convert.ToInt32(dtAux.DefaultView[0].Row["GRUPO_ID"].ToString()) + 1000).ToString() + ","
                        + "msg_id = 0,lei_leitura = '',lei_flag  = 'N',ltr_id = " + ddlLtr.SelectedValue.ToString()
                        + " WHERE CLI_ID = " + Request.Cookies["CliID"].Value.ToString().Trim()
          + " AND EMP_ID = " + Request.Cookies["EmpID"].Value.ToString().Trim()
          + " AND LIV_MES = " + dr[1].ToString()
          + " AND LIV_ANO = " + dr[0].ToString()
          + " AND GRUPO_ID = " + dr[2].ToString()
          + " AND MSG_ID = 17");

                    db.Exe(carregaConexao(), "WTR_2VIA", "update WTR_2VIA set WTR_2VIA.loc_id = " + (Convert.ToInt32(dtAux.DefaultView[0].Row["GRUPO_ID"].ToString()) + 1000).ToString()
                        + " WHERE WTR_2VIA.CLI_ID = " + Request.Cookies["CliID"].Value.ToString().Trim()
          + " AND WTR_2VIA.EMP_ID = " + Request.Cookies["EmpID"].Value.ToString().Trim()
          + " AND WTR_2VIA.LIV_MES = " + dr[1].ToString()
          + " AND WTR_2VIA.LIV_ANO = " + dr[0].ToString()
          + " AND WTR_2VIA.GRUPO_ID = " + dr[2].ToString()
          + " AND WTR_2VIA.LEI_LIGACAO IN (SELECT LEI_LIGACAO FROM WTR_LEITURAS LEI" + " WHERE LEI.CLI_ID = " + Request.Cookies["CliID"].Value.ToString().Trim()
          + " AND LEI.EMP_ID = " + Request.Cookies["EmpID"].Value.ToString().Trim()
          + " AND LEI.LIV_MES = " + dr[1].ToString()
          + " AND LEI.LIV_ANO = " + dr[0].ToString()
          + " AND LEI.LOC_ID = " + (Convert.ToInt32(dtAux.DefaultView[0].Row["GRUPO_ID"].ToString()) + 1000).ToString() + ")");
                    


                    db.Exe(carregaConexao(), "WTR_SERVICO", "update WTR_SERVICO  set WTR_SERVICO.loc_id = " + (Convert.ToInt32(dtAux.DefaultView[0].Row["GRUPO_ID"].ToString()) + 1000).ToString()
                        + " WHERE WTR_SERVICO.CLI_ID = " + Request.Cookies["CliID"].Value.ToString().Trim()
          + " AND WTR_SERVICO.EMP_ID = " + Request.Cookies["EmpID"].Value.ToString().Trim()
          + " AND WTR_SERVICO.SRV_MES = " + dr[1].ToString()
          + " AND WTR_SERVICO.SRV_ANO = " + dr[0].ToString()
          + " AND WTR_SERVICO.GRUPO_ID = " + dr[2].ToString()
          + " AND WTR_SERVICO.SRV_LIGACAO IN (SELECT LEI_LIGACAO FROM WTR_LEITURAS LEI" + " WHERE LEI.CLI_ID = " + Request.Cookies["CliID"].Value.ToString().Trim()
          + " AND LEI.EMP_ID = " + Request.Cookies["EmpID"].Value.ToString().Trim()
          + " AND LEI.LIV_MES = " + dr[1].ToString()
          + " AND LEI.LIV_ANO = " + dr[0].ToString()
          + " AND LEI.LOC_ID = " + (Convert.ToInt32(dtAux.DefaultView[0].Row["GRUPO_ID"].ToString()) + 1000).ToString() + ")");



                    carregaRepasses();
                }

            }
        }
        catch { }


    }
    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        
    }
    protected void GridView2_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            int total = GridView1.Rows.Count;
            int i;

            for (i = 0; i < total; i++)
            {
                if (GridView1.Rows[i].RowType == DataControlRowType.DataRow)
                {

                    int grupo = Convert.ToInt32(GridView1.Rows[i].Cells[3].Text);

                    if ((grupo + 1000) == Convert.ToInt32(e.Row.Cells[3].Text))
                    {
                        GridView1.Rows[i].CssClass = "LinhaSelecionada";

                    }
                }
            }
        }
    }
}