﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WaterSyncLite.Class;
using WaterSyncLite.DAL;
using WaterSyncLite.MDL;

public partial class _Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {


        if (!IsPostBack)
        {
            if (Request.Cookies["pw"] != null)
            {
                Request.Cookies["pw"].Values.Clear();
            }
            PanelEmpresa.Visible = false;
            cbxEmpresa.Visible = false;
            cbxMes.Visible = true;
            txtAno.Visible = true;
            txtUser.Visible = true;
            txtPwd.Visible = true;
            Panelreferencia.Visible = true;


            cbxEmpresa.Enabled = false;
            txtAno.Text = DateTime.Now.Year.ToString();
            cbxMes.SelectedIndex = DateTime.Now.Month - 1;

            txtUser.Focus();
        }
    }

    protected void btnLogin_Click(object sender, EventArgs e)
    {
        try
        {
            // if (btnLogin.Text != "Entrar")
            //{

            #region Tratamento
            HttpCookie aCookie = new HttpCookie("pw");
            

            WaterSyncLite.MDL.User user = new WaterSyncLite.MDL.User();
            user.UserName = txtUser.Text.Trim().ToLower().Replace(" ", "");
            if (txtPwd.Visible == false)
            {
                if (Request.Cookies["pw"].Value.ToString().Length > 0)
                    user.Pwd = Request.Cookies["pw"].Value.ToString();
            }
            else
            {

                if (aCookie.Name == "pw")
                {
                    aCookie.Value = "";    //set a blank value to the cookie 
                    aCookie.Expires = DateTime.Now.AddDays(-1);

                    HttpContext.Current.Response.Cookies.Add(aCookie);
                    aCookie.Expires = DateTime.Now.AddMinutes(1);
                    aCookie.Value = txtPwd.Text;
                }
            }


            /*if (Session["pw"] != null && txtPwd.Enabled == false)
            {
                if (Session["pw"].ToString().Length > 0)
                    user.Pwd = Session["pw"].ToString();
            }*/

            if (cbxMes.Text.Length == 0)
            {
                cbxMes.Focus();
                //Response.Write("Informe o Mês");
            }
            else if (txtAno.Text.Length == 0)
            {
                txtAno.Focus();
                //Response.Write("Informe o Ano!");
            }
            else if ((Convert.ToInt32(txtAno.Text.Length) < 4) || (Convert.ToInt32(txtAno.Text) < 1900))
            {
                txtAno.Focus();
                //Response.Write("Ano Inválido!");
            }
            else if (user.UserName.Length == 0)
            {
                txtUser.Focus();
                //Response.Write("Informe o Usuário");
            }
            else if (user.Pwd.Length == 0)
            {
                txtPwd.Focus();
                //Response.Write("Informe a Senha");
            }
            #endregion Tratamento
            //else
            //{
            Empresa emp = new Empresa();
            EmpresaDAL em = new EmpresaDAL();
            UserDAL usr = new UserDAL();
            User usuario = new User();
            usuario = usr.login(user, txtUser, aCookie.Value);
            Data db = new Data();

            if (usuario.Id != 0 && txtPwd.Visible)
            {
                HttpCookie aCookieUser = new HttpCookie("user");
                HttpContext.Current.Response.Cookies.Add(aCookieUser);
                aCookieUser.Expires = DateTime.Now.AddMinutes(1);
                aCookieUser.Value = usuario.Id.ToString();

                if (txtPwd.Visible)
                {
                    //Session["pw"] = txtPwd.Text;
                    /*if (Request.Cookies["pw"] != null)
                    {
                        Request.Cookies["pw"].Values.Clear();
                    }
                    var newCookie = new HttpCookie("pw");
                    newCookie.Value = txtPwd.Text;
                    newCookie.Expires = DateTime.Now.AddMinutes(60); //Expira em 20 minutos. 
                    //newCookie.Domain = "devmedia.com.br"; //Comente Para funcionar Local 
                    //newCookie.Path = "/devMedia"; //Comente Para funcionar Local 
                    //newCookie.Secure = true; //Comente Para funcionar Local 
                    Response.Cookies.Add(newCookie);*/


                }
                //Dados.Reference.Ano = Convert.ToInt32(txtAno.Text.Trim());
                //Dados.Reference.Mes = Convert.ToInt32(cbxMes.Text.ToString());



                //Dados.user = user;

                loadEmpresa(usuario);
                cbxEmpresa.Visible = true;
                PanelEmpresa.Visible = true;
                //lblEmpresa.Visible = true;
                cbxMes.Visible = false;
                //lblMes.Visible = false;
                txtAno.Visible = false;
                //lblAno.Visible = false;
                txtUser.Visible = false;
                //lblUser.Visible = false;
                txtPwd.Visible = false;

                Panelreferencia.Visible = false;
                //lblPsw.Visible = false;
            }
            // }

    //}
            else
            {

                if (cbxEmpresa.SelectedIndex > -1)
                {
                    EmpresaDAL ep = new EmpresaDAL();
                    // Empresa emp = new Empresa();

                    emp.RazaoSocial = cbxEmpresa.Text;
                    emp = ep.getEmp(emp);



                    //carregaDados();
                    //blockEmpresa(false);
                    Config conf = loadConfig(emp);

                    String sql = "update wtr_user set usr_mes = " + (Convert.ToInt32(cbxMes.SelectedIndex.ToString()) + 1) + ",usr_ano = " + Convert.ToInt32(txtAno.Text.ToString()) + " , usr_cli = " + emp.CliId + ", USR_STRCON = '" + conf.StrCon + "'" +
                    " where usr_id = " + Request.Cookies["user"].Value;


                    db.Exe(Conexao.StrCon, "WTR_USER", sql);


                    //Session["StrCon"] = conf.StrCon;
                    if (Request.Cookies["StrCon"] != null)
                    {
                        Request.Cookies["StrCon"].Values.Clear();
                    }
                    var newCookie = new HttpCookie("StrCon");
                    newCookie.Value = conf.StrCon.ToString().Replace(';', '|');
                    newCookie.Expires = DateTime.Now.AddMinutes(60); //Expira em 20 minutos. 
                    //newCookie.Domain = "devmedia.com.br"; //Comente Para funcionar Local 
                    //newCookie.Path = "/devMedia"; //Comente Para funcionar Local 
                    //newCookie.Secure = true; //Comente Para funcionar Local 
                    Response.Cookies.Add(newCookie);



                    this.Master.CarregaCampos(emp.CliId.ToString(), (Convert.ToInt32(cbxMes.SelectedIndex.ToString()) + 1).ToString(), txtAno.Text.ToString(), Request.Cookies["user"].Value.ToString(), emp.EmpId.ToString());

                    if (Request.Cookies["pw"] != null)
                    {
                        Request.Cookies["pw"].Values.Clear();
                    }



                    Response.Redirect("ViewHome.aspx");



                }
                else
                {
                    //Response.Write("Selecione a Empresa!!");
                    return;
                }
            }




        }
        catch (Exception ex)
        {
            Response.Write("Erro:" + ex.Message);
        }
    }

    private Config loadConfig(Empresa emp)
    {
        WaterSyncLite.MDL.Config conf = new WaterSyncLite.MDL.Config();

        try
        {
            Data db = new Data();
            ConfigDAL cfg = new ConfigDAL();
            string sql = "select * from wtr_config where cli_id = " + emp.CliId;
            DataTable dt = db.GetDt(Conexao.StrCon, "WTR_USER", sql);


            conf.Empresa = emp;
            conf.StrCon = dt.DefaultView[0].Row["CFG_BANCOSTR"].ToString().Replace("grupotcm14", "Clementina").Replace("Hr47jcmj58", "senha");

            return conf;
        }
        catch (Exception ex) { return conf; }
    }


    private void loadEmpresa(User usuario)
    {
        try
        {
            EmpresaDAL ep = new EmpresaDAL();

            if (ep.getEmp(usuario.Id, cbxEmpresa) > 1)
            {
                cbxEmpresa.Enabled = true;
                blockCampos();
                //btnLogin.Text = "Entrar";

            }
            else
            {
                Empresa emp = new Empresa();
                ep.getEmp(emp, usuario.Id);


                loadConfig(emp);
                //carregaDados();
            }
        }
        catch (Exception ex)
        {
            //Files.WriteLog(xml.GetConfigValue("LOCAL", "LOG"), "LOG.txt", DateTime.Now.ToString() + " - " + ex.Message + "\r\n");
        }
    }

    private void blockCampos()
    {
        txtAno.Enabled = false;
        txtPwd.Enabled = false;
        txtUser.Enabled = false;

    }
        
}
