﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="Georreferenciamento.aspx.cs" Inherits="teste" %>

<%@ MasterType VirtualPath="~/Site.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
                <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCsFn8_XaUUs5lbMr1s81nwPdc2b7kWnS8&callback=initMap"></script>

    <style type="text/css">
        #map {
            width: 100%;
            height: 400px;
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <form runat="server">

        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h4 class="panel-title"><span class="TitulosPanel">Georreferenciamento</span></h4>
                    </div>
                    <div class="panel-body minha-classe">
                        <asp:Panel ID="PanelMsg" runat="server" Visible="false">
                            <div class="alert alert-info">
                                <strong>Atenção! </strong>
                                <asp:Label ID="lblMsg" runat="server" Text=""></asp:Label>
                            </div>
                        </asp:Panel>
                        <div class="row">
                            <div class="col-lg-3">
                                <div class="input-group">
                                    <span class="input-group-addon">Mês</span>
                                    <asp:DropDownList ID="cbxMes" runat="server" CssClass="form-control">
                                        <asp:ListItem>Selecione o mês</asp:ListItem>
                                        <asp:ListItem>Janeiro</asp:ListItem>
                                        <asp:ListItem>Fevereiro</asp:ListItem>
                                        <asp:ListItem>Março</asp:ListItem>
                                        <asp:ListItem>Abril</asp:ListItem>
                                        <asp:ListItem>Maio</asp:ListItem>
                                        <asp:ListItem>Junho</asp:ListItem>
                                        <asp:ListItem>Julho</asp:ListItem>
                                        <asp:ListItem>Agosto</asp:ListItem>
                                        <asp:ListItem>Setembro</asp:ListItem>
                                        <asp:ListItem>Outubro</asp:ListItem>
                                        <asp:ListItem>Novembro</asp:ListItem>
                                        <asp:ListItem>Dezembro</asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>

                            <div class="col-lg-3" style="padding-left: 0px">

                                <div class="col-lg-6">
                                    <div class="input-group">
                                        <span class="input-group-addon">Ano</span>
                                        <asp:TextBox ID="txtAno" runat="server" CssClass="form-control bfh-number"></asp:TextBox>
                                    </div>
                                </div>

                                <div class="col-lg-6">
                                    <div class="input-group">
                                        <span class="input-group-addon">Rota</span>
                                        <asp:TextBox ID="txtRota" runat="server" CssClass="form-control bfh-number"></asp:TextBox>
                                    </div>
                                </div>


                            </div>

                            <div class="col-lg-3">
                                <div class="input-group">
                                    <span class="input-group-addon">Leiturista</span>
                                    <asp:DropDownList ID="cbxLeit" runat="server" CssClass="form-control">
                                    </asp:DropDownList>
                                </div>
                            </div>

                            <div class="col-lg-2">

                                <div class="checkbox">
                                    <asp:CheckBox ID="chkMapeamento" runat="server" Text="Cidade completa" />

                                </div>
                            </div>
                            <div class="col-lg-1">
                                <asp:Button ID="Button1" runat="server" Text="Pesquisar" CssClass="btn btn-primary" OnClick="btPesquisar_Click" />
                            </div>




                        </div>
                         <div class="row" style="padding-top: 10px">

        <div class="col-lg-12">
            <div id="map"></div>
            <script>
                var map;
                function InitializeMap() {
                    var mapDiv = document.getElementById('map');
                    var myOptions = new google.maps.Map(mapDiv, {
                        center: { lat: -23.6248442, lng: -46.5824176 },
                        zoom: 11
                    });

                    map = new google.maps.Map(document.getElementById("map"), myOptions);
                    // alert('new google.maps.Map(document.getElementById("map"), myOptions)');

                    var style = "";
                    var latitude = '<% = Session["latitude"].ToString() %>';
                    var longitude = '<% = Session["longitude"].ToString() %>';
                    var local = '<% = Session["local"].ToString() %>';
                    var consumidor = '<% = Session["consumidor"].ToString() %>';
                    var leituraAtual = '<% = Session["leitura"].ToString() %>';
                    var leituraAnterior = '<% = Session["leituraAnterior"].ToString() %>';



                    //alert(latitude.split(',')[0] + latitude.split(',')[1] + latitude.split(',')[2] + latitude.split(',')[3] + latitude.split(',')[4] + latitude.split(',')[5] + latitude.split(',')[6]);
                    //var checkboxArray = document.getElementById('waypoints');

                    for (var i = 0; i < latitude.split(',').length; i++) {
                        if (i == 0)
                            addMarker(new google.maps.LatLng(latitude.split(',')[i], longitude.split(',')[i]), "Inicio", local.split('%')[i], consumidor.split('%')[i], leituraAtual.split(',')[i], leituraAnterior.split(',')[i]);
                        else {
                            if (i == latitude.split(',').length - 2) {

                                addMarker(new google.maps.LatLng(latitude.split(',')[i], longitude.split(',')[i]), "Fim", local.split('%')[i], consumidor.split('%')[i], leituraAtual.split(',')[i], leituraAnterior.split(',')[i]);

                            } else {
                                addMarker(new google.maps.LatLng(latitude.split(',')[i], longitude.split(',')[i]), "", local.split('%')[i], consumidor.split('%')[i], leituraAtual.split(',')[i], leituraAnterior.split(',')[i]);
                            }
                        }

                    }
                    //document.getElementById('ContentPlaceHolder1_lblQtdRegistros').innerText = latitude.split(',').length.toString();
                    //alert('document.getElementById(lblQtdRegistros.ClientID).innerText = latitude.split(', ').length.toString()');
                }

                function addMarker(place, style, title, consumidor, leitura, leituraAnterior) {

                    if (style == "Inicio") {
                        var marker = new google.maps.Marker({
                            position: place,
                            map: map,
                            title: title + "\n" + consumidor + "\nLeitura Atual: " + leitura + "\nLeitura Anterior: " + leituraAnterior,
                            icon: {
                                url: 'http://labs.google.com/ridefinder/images/mm_20_red.png',
                                anchor: new google.maps.Point(10, 10),
                                scaledSize: new google.maps.Size(20, 34)


                            }
                        });
                    } else {
                        if (style == "Fim") {

                            var marker = new google.maps.Marker({
                                position: place,
                                map: map,
                                title: title + "\n" + consumidor + "\nLeitura Atual: " + leitura + "\nLeitura Anterior: " + leituraAnterior,
                                icon: {
                                    url: 'http://labs.google.com/ridefinder/images/mm_20_black.png',
                                    anchor: new google.maps.Point(10, 10),
                                    scaledSize: new google.maps.Size(20, 34)

                                }
                            });
                        } else {
                            var marker = new google.maps.Marker({
                                position: place,
                                map: map,
                                title: title + "\n" + consumidor + "\nLeitura Atual: " + leitura + "\nLeitura Anterior: " + leituraAnterior,
                                icon: {
                                    url: 'http://maps.gstatic.com/mapfiles/circle.png',
                                    anchor: new google.maps.Point(10, 10),
                                    scaledSize: new google.maps.Size(10, 17)


                                }
                            });
                        }
                    }
                }

                window.onload = InitializeMap;
                var bikeLayer = new google.maps.BicyclingLayer();
                bikeLayer.setMap(map);

            </script>

        </div>
    </div>

                    </div>
                </div>
            </div>
        </div>

    </form>
   

    <asp:Label ID="lblLatitudes" runat="server" Visible="False"></asp:Label>
    <asp:Label ID="lblLongitudes" runat="server" Visible="False"></asp:Label>
</asp:Content>

