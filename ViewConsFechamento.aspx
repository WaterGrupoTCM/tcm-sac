﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="ViewConsFechamento.aspx.cs" Inherits="ViewConsFechamento" %>
<%@ MasterType VirtualPath="~/Site.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
    <style type="text/css">
     
        .style17
        {
            width: 100%;
        }
        .style19
        {
            width: 151px;
        }
     
        .style101014
        {
            font-size: x-large;
            color: #000099;
        }
        .style101015
        {
            width: 141px;
        }
        .style101017
        {
            width: 119px;
        }
     
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
    <table class="style17">
        <tr>
            <td class="style101014" colspan="3">
                <strong>Fechamento Mensal</strong></td>
        </tr>
        <tr>
            <td class="style101015" style="border-top-style: inset">
                <strong>Mês:</strong> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            </td>
            <td class="style101017" style="border-top-style: inset">
                &nbsp;<strong>Ano:</strong></td>
            <td style="border-top-style: inset">
                <strong>Irregular:</strong></td>
        </tr>
        <tr>
            <td class="style101015">
                <asp:DropDownList ID="cbxMes" runat="server" Height="25px" Width="124px">
                </asp:DropDownList>
            </td>
            <td class="style101017">
                <asp:DropDownList ID="cbxAno" runat="server" Height="25px" Width="91px">
                    <asp:ListItem>2011</asp:ListItem>
                    <asp:ListItem>2012</asp:ListItem>
                    <asp:ListItem>2013</asp:ListItem>
                    <asp:ListItem>2014</asp:ListItem>
                    <asp:ListItem>2015</asp:ListItem>
                    <asp:ListItem>2016</asp:ListItem>
                    <asp:ListItem>2017</asp:ListItem>
                    <asp:ListItem></asp:ListItem>
                </asp:DropDownList>
            </td>
            <td>
                <asp:TextBox ID="txtIrregular" runat="server" Width="60px" Height="23px"></asp:TextBox>
            </td>
        </tr>
    </table>
    <table class="style17">
        <tr>
            <td class="style19">
                <asp:Button ID="linkTotEtapas" runat="server" BorderStyle="None" 
                    onclick="linkTotEtapas_Click" Text="Total Etapas" Width="144px" 
                    style="font-weight: 700" />
            </td>
            <td rowspan="5">
                <asp:TextBox ID="txtDados" runat="server" Enabled="False" Height="364px" 
                    TextMode="MultiLine" Width="581px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="style19">
                <asp:Button ID="linkEtapas" runat="server" BorderStyle="None" 
                    onclick="linkEtapas_Click" Text="Etapas" Width="144px" 
                    style="font-weight: 700" />
            </td>
        </tr>
        <tr>
            <td class="style19">
                <asp:Button ID="linkEtapasCidades" runat="server" BorderStyle="None" 
                    onclick="linkEtapasCidades_Click" Text="Etapas - Cidades" Width="144px" 
                    style="font-weight: 700" />
            </td>
        </tr>
        <tr>
            <td class="style19">
                <asp:Button ID="linkTotalEtapasCid" runat="server" BorderStyle="None" 
                    onclick="linkTotalEtapasCid_Click" Text="Total Etapas - Cidades" 
                    Width="144px" style="font-weight: 700" />
            </td>
        </tr>
        <tr>
            <td class="style19">
                <asp:Button ID="linkISS" runat="server" BorderStyle="None" 
                    onclick="linkISS_Click" Text="ISS" Width="144px" 
                    style="font-weight: 700" />
            </td>
        </tr>
    </table>



</asp:Content>

