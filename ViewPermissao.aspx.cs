﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WaterSyncLite.Class;

public partial class ViewPermissao : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        this.Master.LoadHeader(this.Master, Request.Path.Substring(Request.Path.LastIndexOf("/") + 1));
        if (!IsPostBack)
        {

            Data db = new Data();

            txtAno.Text = DateTime.Now.Year.ToString();
            cbxMes.SelectedIndex = DateTime.Now.Month;

            if (Request.Cookies["CliID"].Value.PadLeft(2, '0').ToString().Trim() == "17")
                panelGrupo.Visible = true;
            else
                panelGrupo.Visible = false;


            #region LEITURISTAS
            DataTable dt = db.GetDt(carregaConexao(), "WTR_LEIT", "SELECT LTR_ID, LTR_NOME FROM "
                + " WTR_LEITURISTAS WHERE CLI_ID = " + Request.Cookies["CliID"].Value.ToString().Trim()
                + " AND EMP_ID = " + Request.Cookies["EmpID"].Value.ToString().Trim()
                + " ORDER BY LTR_NOME");

            DataRow dtr = dt.NewRow();
            dtr["LTR_NOME"] = "Selecione um leiturista...";
            dtr["LTR_ID"] = 0;
            dt.Rows.Add(dtr);
            if (dt != null)
            {
                ddlLtr.DataSource = dt;
                ddlLtr.DataTextField = "LTR_NOME";
                ddlLtr.DataValueField = "LTR_ID";
                ddlLtr.DataBind();
                ddlLtr.SelectedValue = "0";

            }
            #endregion
        }
    }
    protected void ddlAno_SelectedIndexChanged(object sender, EventArgs e)
    {
        /*
        #region Mes

        Data db = new Data();
        DataTable dt = db.GetDt(carregaConexao(), "WTR_LIVROS", "SELECT LIV_MES FROM "
            + " WTR_LIVROS WHERE CLI_ID = " + Request.Cookies["CliID"].Value.ToString().Trim()
            + " AND EMP_ID = " + Request.Cookies["EmpID"].Value.ToString().Trim()
            + " GROUP BY LIV_MES"
            + " ORDER BY LIV_MES");

        if (dt != null)
        {
            ddlAno.DataSource = dt;

            ddlAno.DataTextField = "LIV_MES";
            ddlAno.DataValueField = "LIV_MES";
            ddlAno.DataBind();
            //ddlAno.SelectedValue = "2015";
        }
        #endregion
        */
    }



    protected String carregaConexao()
    {
        String StrCon = "";
        if (Request.Cookies["StrCon"] != null)
        {
            if (Request.Cookies["StrCon"].Value.ToString().Length > 0)
                StrCon = Request.Cookies["StrCon"].Value.ToString();
        }
        return StrCon.Replace('|', ';').Replace("usuario", xml.GetConexaoValue(Server.MapPath("~"), "LOCAL", "USER")).Replace("senha", xml.GetConexaoValue(Server.MapPath("~"), "LOCAL", "PASSWORD"));
    }
    protected DataTable carregaGrid()
    {
        String StrCon = carregaConexao();

        Session["latitude"] = "";
        Session["longitude"] = "";
        Session["local"] = "";

        double porc = 0;
        string pMax = "";
        string pMin = "";


        if (txtAno.Text.Trim().Length <= 0)
        {
            lblMsg.Text = "Digite o ano antes de realizar a consulta.";
            PanelMsg.Visible = true;
            return null;
        }
        else if (txtIdentificacao.Text.Trim().Length <= 0 && txtLivro.Text.Trim().Length <= 0 && txtGrupo.Text.Trim().Length <= 0 && txtMedidor.Text.Trim().Length <= 0)
        {
            lblMsg.Text = "Por favor, Preencha pelo menos mais um campo para realizar a pesquisa.";
            PanelMsg.Visible = true;
            return null;
        }
        Data db = new Data();
        string sql =
            "select LIV_ANO AS ANO, LIV_MES AS MES, LEI_LIGACAO AS LIGACAO,LEI_CONSUMIDOR AS CONSUMIDOR, LOC_ID AS ROTA, LEI_ENDERECO AS ENDEREÇO, LEI_NUMERO AS NUMERO,LEI_LEITURA,MSG_DESCRICAO,WTR_LEITURAS.msg_id ";

        sql += " FROM WTR_LEITURAS INNER JOIN WTR_MENSAGENS ON(WTR_LEITURAS.MSG_ID = WTR_MENSAGENS.MSG_ID and WTR_LEITURAS.cli_id = WTR_MENSAGENS.cli_id and WTR_MENSAGENS.emp_id = WTR_LEITURAS.emp_id) INNER JOIN WTR_LEITURISTAS ON (WTR_LEITURAS.LTR_ID = WTR_LEITURISTAS.LTR_ID) WHERE WTR_LEITURAS.emp_id =" +
                     Request.Cookies["EmpID"].Value.ToString().Trim() + " and WTR_LEITURAS.cid_id =" + Request.Cookies["EmpID"].Value.ToString().Trim() +
                     " and WTR_LEITURAS.CLI_ID = " + Request.Cookies["CliID"].Value.PadLeft(2, '0').ToString().Trim();// Data.cli.ToString();


        #region SQL E FILTROS

        if (txtLivro.Text.Trim().Length > 0)
        {
            if (Request.Cookies["CliID"].Value.PadLeft(2, '0').ToString().Trim() == "4")
                txtLivro.Text = txtLivro.Text.Trim().PadLeft(3, '0');
            else
                sql += " AND LOC_ID = '" + txtLivro.Text.Trim() + "' ";
        }


        if (cbxMes.SelectedIndex > 0)
            sql += " AND LIV_MES = '" + cbxMes.SelectedIndex.ToString() + "' ";

        if (txtAno.Text.Trim().Length > 0)
            sql += " AND LIV_ANO = '" + txtAno.Text.Trim() + "' ";

        #region Grupo
        if (txtGrupo.Text.Trim() != "" && Request.Cookies["CliID"].Value.PadLeft(2, '0').ToString().Trim() == "17")
        {
            sql += " AND GRUPO_ID = " + txtGrupo.Text;
        }
        #endregion
        #region Leiturista
        if (ddlLtr.SelectedValue.ToString() != "0")
        {
            sql += " AND LTR_ID = " + ddlLtr.SelectedValue.ToString();
        }
        #endregion

        #region Identificação e Medidor
        if ((txtIdentificacao.Text.Trim().Length > 0) || (txtMedidor.Text.Trim().Length > 0))
        {
            if (txtIdentificacao.Text.Trim().Length > 0)
                sql += " AND LEI_LIGACAO LIKE '%" + txtIdentificacao.Text.Trim() + "%' ";

            if (txtMedidor.Text.Trim().Length > 0)
                sql += " AND LEI_NUMMED LIKE '%" + txtMedidor.Text.Trim() + "%' ";

        }
        #endregion
        #endregion

        DataTable dt = db.GetDt(StrCon, "WTR_LEITURAS", sql);

        Session["dt"] = dt;

        return dt;

    }
    private void makeLista(string ordeR)
    {
        try
        {
            String StrCon = carregaConexao();
            DataTable dt = carregaGrid();



            //lblTOTAL.Text = dt.Rows.Count + " leitura(s) encontrada(s).";
            if (dt != null)
            {
                if (dt.DefaultView.Count > 0)
                {
                    if (Request.Cookies["CliID"].Value.PadLeft(2, '0').ToString().Trim() == "05")
                        GridView1.AllowPaging = false;



                }
                else
                {
                    lblMsg.Text = ("Nenhum histórico encontrado para esta identificação ou medidor informado.");
                    PanelMsg.Visible = true;
                }
            }
            GridView1.DataSource = dt;
            GridView1.DataBind();
        }
        catch (Exception ex)
        {
            lblMsg.Text = ("Informações do sistema: " + ex.Message);
            PanelMsg.Visible = true;
        }
        finally
        {
            //Cursor.Current = Cursors.Default;
        }
    }



    protected void carregaPermissoes()
    {
        try
        {
            string where = " WHERE ";
            #region SQL E FILTROS

            if (cbxMes.SelectedIndex > 0)
                where += " MES = '" + cbxMes.SelectedIndex.ToString() + "' ";

            if (txtAno.Text.Trim().Length > 0)
                where += " AND ANO = '" + txtAno.Text.Trim() + "' ";

            //where += " and flag = 'N'";


            #endregion
            Data db = new Data();
            DataTable dt = db.GetDt(carregaConexao(), "wtr_permissao", "select * from wtr_permissao" + where);
            GridView2.DataSource = dt;
            GridView2.DataBind();
        }
        catch { }

    }
    protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            if (e.CommandName == "Add")
            {
                Data db = new Data();
                int indexGrid = Convert.ToInt32(e.CommandArgument);
                DataTable dt = (DataTable)Session["dt"];
                DataRow dr = dt.Rows[indexGrid];
                string sql = "select * from wtr_permissao where ligacao like " + dr[2].ToString() + " and mes = " + dr[1].ToString() + " and ano = " + dr[0].ToString();
                DataTable data = db.GetDt(carregaConexao(), "WTR_PERMISSAO", sql);

                if (data.DefaultView.Count <= 0)
                {
                    sql = "insert into wtr_permissao (ligacao,mes,ano,flag) values (" + dr[2].ToString() + "," + dr[1].ToString() + "," + dr[0].ToString() + ",'N')";

                    bool erro = db.Exe(carregaConexao(), "wtr_permissao", sql);
                }
                else
                {
                    if (data.DefaultView[0].Row["flag"].ToString() == "N")
                        sql = "update wtr_permissao set flag = 'S' where ligacao like " + dr[2].ToString() + " and mes = " + dr[1].ToString() + " and ano = " + dr[0].ToString();
                    else sql = "update wtr_permissao set flag = 'N' where ligacao like " + dr[2].ToString() + " and mes = " + dr[1].ToString() + " and ano = " + dr[0].ToString();

                    db.Exe(carregaConexao(), "wtr_permissao", sql);
                }

               
            }
            else if (e.CommandName == "Ver")
            {
                Data db = new Data();
                int indexGrid = Convert.ToInt32(e.CommandArgument);
                DataTable dt = (DataTable)Session["dt"];
                DataRow dr = dt.Rows[indexGrid];
                string sql = "select * from wtr_permissao where ligacao like " + dr[2].ToString() + " and mes = " + dr[1].ToString() + " and ano = " + dr[0].ToString();
                DataTable data = db.GetDt(carregaConexao(), "WTR_PERMISSAO", sql);
                if (!data.DefaultView[0].Row["leituraIncorreta"].ToString().Trim().Equals(dr[7].ToString().Trim()))
                {
                    leitura.Visible = true;
                    lblLeituraAlterada.Text = data.DefaultView[0].Row["leituraIncorreta"].ToString();
                    lblLeituraAtual.Text = dr[7].ToString();
                }
                else leitura.Visible = false;
                if (!data.DefaultView[0].Row["msg_id"].ToString().Trim().Equals(dr[9].ToString().Trim()))
                {
                    ocorrencia.Visible = true;
                    lblMsgAlterada.Text = data.DefaultView[0].Row["msg_descricao"].ToString();
                    lblMsgAtual.Text = db.GetDt(carregaConexao(), "WTR_MENSAGENS", "select * from wtr_mensagens where msg_id = " + dr[8].ToString()).DefaultView[0].Row["msg_descricao"].ToString();
                }
                else ocorrencia.Visible = false;
                Page.ClientScript.RegisterStartupScript(GetType(), "teste", "jQuery('#myModal').modal('show');", true);
            }

            makeLista("");

            carregaPermissoes();


        }
        catch { }


    }

    /*protected void PrintAllPages(object sender, EventArgs e)
    {
        btnSelect.Visible = false;
        GridView1.AllowPaging = false;
        GridView1.DataSource = carregaGrid();
        GridView1.DataBind();

        StringWriter sw = new StringWriter();
        HtmlTextWriter hw = new HtmlTextWriter(sw);
        GridView1.RenderBeginTag(hw);
        GridView1.HeaderRow.RenderControl(hw);
        foreach (GridViewRow row in GridView1.Rows)
        {
            row.RenderControl(hw);
        }
        GridView1.FooterRow.RenderControl(hw);
        GridView1.RenderEndTag(hw);

        StringBuilder sb = new StringBuilder();
        /*StringWriter tw = new StringWriter(sb);
        HtmlTextWriter hwpanel = new HtmlTextWriter(tw);
        Panel6.RenderControl(hw); 
        
        string gridHTML = sb.ToString() + sw.ToString().Replace("\"", "'")
            .Replace(System.Environment.NewLine, "");

        sb.Append("<script type = 'text/javascript'>");
        sb.Append("window.onload = new function(){");
        sb.Append("var printWin = window.open('', '', 'left=0");
        sb.Append(",top=0,width=1000,height=600,status=0');");
        sb.Append("printWin.document.write(\"");
        sb.Append(gridHTML);
        sb.Append("\");");
        sb.Append("printWin.document.close();");
        sb.Append("printWin.focus();");
        sb.Append("printWin.print();");
        sb.Append("printWin.close();};");
        sb.Append("</script>");
        ClientScript.RegisterStartupScript(this.GetType(), "GridPrint", sb.ToString());
        GridView1.AllowPaging = true;
        GridView1.DataBind();
        btnSelect.Visible = true;
    }*/

    protected void GridView2_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            int total = GridView1.Rows.Count;
            int i;

            for (i = 0; i < total; i++)
            {
                if (GridView1.Rows[i].RowType == DataControlRowType.DataRow)
                {

                    int ligacao = Convert.ToInt32(GridView1.Rows[i].Cells[4].Text);

                    if (Convert.ToInt32(e.Row.Cells[0].Text) == ligacao)
                    {
                        if (e.Row.Cells[3].Text == "N")
                            GridView1.Rows[i].CssClass = "LinhaSelecionada";

                        if (e.Row.Cells[4].Text.ToString().Replace("&nbsp;", "").Trim().Length > 0)
                            GridView1.Rows[i].Cells[0].Controls[0].Visible = true;
                        else GridView1.Rows[i].Cells[0].Controls[0].Visible = false;


                    }

                }
            }
        }
    }
    protected void btPesquisar_Click(object sender, EventArgs e)
    {
        PanelMsg.Visible = false;

        makeLista("");

        carregaPermissoes();
    }
    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        escondeColunaGrid(2, e);
        escondeColunaGrid(3, e);
        escondeColunaGrid(9, e);
        escondeColunaGrid(10, e);
        escondeColunaGrid(11, e);
        switch (e.Row.RowType)
        {

            case DataControlRowType.DataRow:
                e.Row.Cells[0].Controls[0].Visible = false;
                break;

        }
    }
    public void escondeColunaGrid(int coluna, GridViewRowEventArgs e)
    {
        switch (e.Row.RowType)
        {
            case DataControlRowType.Header:
                e.Row.Cells[coluna].Visible = false;
                break;
            case DataControlRowType.DataRow:
                e.Row.Cells[coluna].Visible = false;
                break;
            case DataControlRowType.Footer:
                e.Row.Cells[coluna].Visible = false;
                break;
        }
    }

    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView1.PageIndex = e.NewPageIndex;
        makeLista("");

        carregaPermissoes();
        GridView1.DataBind();
    }
}