﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Download : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if(!IsPostBack)
        {

           // Session["arquivo"] = "";
           // Session["nomeArq"] = "";

            Down down = new Down(Session["arquivo"].ToString(), Session["arquivo"].ToString(), Response,this.ClientScript);

            // Create a thread to execute the task, and then
            // start the thread.
            Thread t = new Thread(new ThreadStart(down.Executa));
            t.Start();
            //downloadTxt(cam + @"\T" + tabExport[i].Trim() + "_" + file + "." + ext, file, response);
            t.Join();
            //HttpContext.Current.ApplicationInstance.CompleteRequest();
            //response.Redirect("ViewExport.aspx", false);
           
                
        }
    }

    public class Down
    {
        // State information used in the task.
        private string arquivo;
        private string nomeArq;
        private HttpResponse page;
        private HttpContext context;
        private ClientScriptManager pagina;

        // The constructor obtains the state information.
        public Down(string arquivo, string nomeArq, HttpResponse page,ClientScriptManager pagina)
        {
            this.arquivo = arquivo;
            this.nomeArq = nomeArq;
            this.page = page;
            this.pagina = pagina;
        }

        // The thread procedure performs the task, such as formatting
        // and printing a document.
        public void Executa()
        {
            try
            {

                //Response.ContentType = "application/octet-stream";
                //Response.AppendHeader("Content-Disposition", String.Format("attachment; filename={0}", nomeArq));
                //Response.TransmitFile(arquivo);
                //HttpContext.Current.ApplicationInstance.CompleteRequest();

                // Get the physical Path of the file
                string filepath = arquivo;

                // Create New instance of FileInfo class to get the properties of the file being downloaded
                FileInfo file = new FileInfo(filepath);

                // Checking if file exists
                if (file.Exists)
                {
                    // Clear the content of the response
                    page.Clear();
                    page.ClearHeaders();
                    page.ClearContent();

                    // LINE1: Add the file name and attachment, which will force the open/cance/save dialog to show, to the header
                    page.AddHeader("Content-Disposition", String.Format("attachment; filename={0}", file.Name));

                    // Add the file size into the response header
                    page.AddHeader("Content-Length", file.Length.ToString());

                    // Set the ContentType
                    page.ContentType = ReturnFiletype(file.Extension.ToLower());

                    // Write the file into the response (TransmitFile is for ASP.NET 2.0. In ASP.NET 1.1 you have to use WriteFile instead)
                    page.Write(file.FullName);

                    // End the response
                    page.Clear();
                    page.Write("<script>window.close();</script>");
                    page.Flush();
                    page.End();



                    //send statistics to the class
                }
            }
            catch (ThreadAbortException ex)
            {
                throw;
            }
            finally
            {
                //page.Redirect("ViewExport.aspx", false);
                //ScriptManager.RegisterStartupScript(Me, Me.GetType(), "redirectScript", "window.location.href='whateverurlhere.aspx';", True)
            }
        }

        public static string ReturnFiletype(string fileExtension)
        {
            switch (fileExtension)
            {
                case ".htm":
                case ".html":
                case ".log":
                    return "text/HTML";
                case ".txt":
                    return "text/plain";
                case ".doc":
                    return "application/ms-word";
                case ".tiff":
                case ".tif":
                    return "image/tiff";
                case ".asf":
                    return "video/x-ms-asf";
                case ".avi":
                    return "video/avi";
                case ".zip":
                    return "application/zip";
                case ".xls":
                case ".csv":
                    return "application/vnd.ms-excel";
                case ".gif":
                    return "image/gif";
                case ".jpg":
                case "jpeg":
                    return "image/jpeg";
                case ".bmp":
                    return "image/bmp";
                case ".wav":
                    return "audio/wav";
                case ".mp3":
                    return "audio/mpeg3";
                case ".mpg":
                case "mpeg":
                    return "video/mpeg";
                case ".rtf":
                    return "application/rtf";
                case ".asp":
                    return "text/asp";
                case ".pdf":
                    return "application/pdf";
                case ".fdf":
                    return "application/vnd.fdf";
                case ".ppt":
                    return "application/mspowerpoint";
                case ".dwg":
                    return "image/vnd.dwg";
                case ".msg":
                    return "application/msoutlook";
                case ".xml":
                case ".sdxl":
                    return "application/xml";
                case ".xdp":
                    return "application/vnd.adobe.xdp+xml";
                default:
                    return "application/octet-stream";
            }
        }
    }
}