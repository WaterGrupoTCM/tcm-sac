﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WaterSyncLite.MDL;
using WaterSyncLite.DAL;
using WaterSyncLite.Class;
using System.Data;
using System.IO;

public partial class ViewConsHoraLeitura : System.Web.UI.Page
{
    public static bool horaExists = false;
    public static bool load = false;
    private string camEntrada = "";
    string[] imgsZ;							//Controle de fotos da UC.
    string[] dirsZ;							//Controle de livros (diretórios).

    private Data db = new Data();
    
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            this.Master.LoadHeader(this.Master, Request.Path.Substring(Request.Path.LastIndexOf("/") + 1));
            if (!IsPostBack)
            {
                
                //horaExists = true; 
                load = true;
                //header();
                //header1();
                loadLtr();
                loadLivros();

                load = false;
                cbxAno.Text = Dados.Reference.Ano.ToString();
                cbxMes.SelectedIndex = Dados.Reference.Mes;

                cbxMes.Items.Add("Selecione um mês...");
            }
        }
        catch (Exception ex)
        {
            msg("Problemas no sistema: " + ex.Message);
            Response.Redirect("ViewHome.aspx");
 
        }
    }
    
    protected void cbxEtapa_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            loadLivro();
        }
        catch (Exception ex)
        {
            msg("Informação: " + ex.Message);
        }
    }

    protected void msg(string msg)
    {
        String scriptString = "<script language = JavaScript>";
        scriptString += "confirm('" + msg + "');";
        scriptString += "</script>";
        Page.ClientScript.RegisterStartupScript(Page.GetType(), "scrt", scriptString);
    }

    protected String carregaConexao()
    {
        try
        {
            String StrCon = "";
            if (Request.Cookies["StrCon"] != null)
            {
                if (Request.Cookies["StrCon"].Value.ToString().Length > 0)
                    StrCon = Request.Cookies["StrCon"].Value.ToString();
            }
            return StrCon.Replace('|', ';').Replace("usuario", xml.GetConexaoValue(Server.MapPath("~"), "LOCAL", "USER")).Replace("senha", xml.GetConexaoValue(Server.MapPath("~"), "LOCAL", "PASSWORD"));
        }
        catch (Exception ex )
        {

            msg("Erro de conexão - "+ex.Message);
            return "";
            throw;
        }
       
    }
    private void loadLtr()
    {
        try
        {
            DataTable dt = db.GetDt(carregaConexao(), "WTR_LEITURISTA", "SELECT LTR_ID, CAST(LTR_ID AS VARCHAR(2)) + ' - ' + LTR_NOME + ' ' + LTR_SOBRENOME AS LTR_NOM "
                + " FROM WTR_LEITURISTAS "
                + " WHERE CLI_ID = " + Request.Cookies["CliID"].Value
                + " and EMP_ID = " + Request.Cookies["EmpID"].Value
                + " ORDER BY LTR_NOME");

            DataRow dtr = dt.NewRow();
            dtr["LTR_ID"] = 0;
            dtr["LTR_NOM"] = "Selecione um leiturista...";
            dt.Rows.Add(dtr);

            if (dt != null)
            {
                dt.DefaultView.Sort = "LTR_ID";
                cbxLtr.DataSource = dt.DefaultView;
                cbxLtr.DataTextField = "LTR_NOM";
                cbxLtr.DataValueField = "LTR_ID";
                cbxLtr.DataBind();
                cbxLtr.Text = "Selecione um leiturista...";
            }
        }
        catch (Exception ex)
        {
            msg("Informação: " + ex.Message);
        }
    }


    private void loadLivros()
    {
        try
        {
            string sql = "SELECT DISTINCT(LOC_ID) AS LOC_ID FROM WTR_LIVROS "
                                                                       + " WHERE CLI_ID = " + Request.Cookies["CliID"].Value;
            if (cbxLtr.SelectedValue != "0")
                sql += " and ltr_id = " + cbxLtr.SelectedValue.ToString();
            if (cbxAno.SelectedIndex != 0)
                sql += " and liv_ano = " + cbxAno.SelectedValue.ToString();
            if (cbxMes.SelectedIndex != 0)
                sql += " and liv_mes = " + cbxMes.SelectedIndex.ToString();
            
            sql+= " ORDER BY LOC_ID";
            DataTable dt = db.GetDt(carregaConexao(), "WTR_LIVROS",sql);

            if (dt != null)
            {
                dt.DefaultView.Sort = "LOC_ID";
                cbxLivro.DataSource = dt.DefaultView;
                cbxLivro.DataTextField = "LOC_ID";
                cbxLivro.DataValueField = "LOC_ID";
                cbxLivro.DataBind();
                //cbxLivro.Text = "Selecione um livro...";
            }
        }
        catch (Exception ex)
        {
            msg("Informação: " + ex.Message);
        }
    }

    private void loadLivro()
    {
        try
        {
            string ano = cbxAno.Text.Trim();
            string mes = cbxMes.Text.Trim();
            string etapa = cbxEtapa.Text.Trim();
            string livro = cbxLivro.Text.Trim();

            camEntrada = HttpContext.Current.Request.ApplicationPath +@"\Arquivos\" + ano + @"\" + mes + @"\" + etapa;

            cbxLivro.Items.Clear();

            if (Directory.Exists(camEntrada))
            {
                dirsZ = Directory.GetFiles(camEntrada, "*.vlt");

                for (int y = 0; y < dirsZ.Length; y++)
                {
                    FileInfo f = new FileInfo(dirsZ[y].ToString());
                    cbxLivro.Items.Insert(y, f.Name.Substring(0, f.Name.Length - 4));
                }
            }
            else
            {
                cbxLivro.Text = "";
            }
        }
        catch (Exception ex)
        {
            msg("Informação: " + ex.Message);
        }
    }
    /*private void makeRel(string cam)
    {
        try
        {
            string linha = string.Empty;
            int nlin = 0;
            int totLei = 0;
            string leiAntes = string.Empty;
            string leiPos = string.Empty;
            string horario = string.Empty;
            string data = string.Empty;

            System.DateTime dateAnt;
            System.DateTime datePos;

            int dia = 0;
            int mes = 0;
            int ano = 0;

            int hora = 0;
            int minuto = 0;
            int segundo = 0;

            string filName = cbxLivro.Text.Substring(0, cbxLivro.Text.Length) + ".vlt";

            lsvDados.Items.Clear();

            #region Lendo arquivo

            FileStream ofs = File.Open(cam, FileMode.Open, FileAccess.Read, FileShare.None);
            StreamReader sr = new StreamReader(ofs);

            while (sr.Peek() >= 0)
            {
                if (totLei < 1)
                    linha = sr.ReadLine();

                if ((linha.Length > 118) && (linha.Substring(0, 1) == "1"))
                {
                    nlin = linha.Length;

                    #region Criando as linhas do ListView - PEDIDO
                    ListViewItem item = new ListViewItem();
                    try
                    {
                        horario = linha.Substring(120, 2) + ":" + linha.Substring(122, 2) + ":" + linha.Substring(124, 2);
                        data = linha.Substring(126, 2) + "-" + linha.Substring(128, 2) + "-" + linha.Substring(130, 4);

                        item = new ListViewItem();
                        item.Text = linha.Substring(9, 4);											//Sequência
                        /*item.SubItems.Add(linha.Substring(138,3));								//Leiturista
                        item.SubItems.Add(linha.Substring(13, 8));									//U.C
                        item.SubItems.Add(linha.Substring(61, 10));									//Medidor
                        item.SubItems.Add(linha.Substring(111, 6));									//Leitura
                        item.SubItems.Add(horario + " " + data);									//Horário da Leitura
                        item.SubItems.Add(leiAntes);												//Tempo Anterior
                        item.SubItems.Add(leiPos);													//Tempo Posterior

                        if (totLei > 0)
                        {
                            #region Horario Anterior
                            dia = Convert.ToInt32(linha.Substring(126, 2));
                            mes = Convert.ToInt32(linha.Substring(128, 2));
                            ano = Convert.ToInt32(linha.Substring(130, 4));

                            hora = Convert.ToInt32(linha.Substring(120, 2));
                            minuto = Convert.ToInt32(linha.Substring(122, 2));
                            segundo = Convert.ToInt32(linha.Substring(124, 2));

                            #endregion
                        }

                        dateAnt = new System.DateTime(ano, mes, dia, hora, minuto, segundo);

                        #region Horario Posterior
                        dia = Convert.ToInt32(linha.Substring(126, 2));
                        mes = Convert.ToInt32(linha.Substring(128, 2));
                        ano = Convert.ToInt32(linha.Substring(130, 4));

                        hora = Convert.ToInt32(linha.Substring(120, 2));
                        minuto = Convert.ToInt32(linha.Substring(122, 2));
                        segundo = Convert.ToInt32(linha.Substring(124, 2));

                        datePos = new System.DateTime(ano, mes, dia, hora, minuto, segundo);
                        #endregion


                        System.TimeSpan diff;
                        diff = datePos.Subtract(dateAnt);


                        linha = sr.ReadLine();



                        lsvDados.Items.Add(item);
                    }
                    catch (Exception ex)
                    {
                        msg(ex.Message);
                    }
                    #endregion

                    totLei++;
                }
            }
            #endregion


            if (totLei == 0)
            {
                msg("Nenhuma leitura encontrada.");
            }
            else
            {
            }
        }
        catch (Exception ex)
        {
            msg("Problemas no sistema: " + ex.Message);
        }
    }
     * 
     * */
    
    protected void GridView1_PageIndexChanged(object sender, EventArgs e)
    {

    }

    protected void carregaGrid()
    {
        try
        {
            if (chkLocal.Checked)
            {
                //if (File.Exists(camEntrada + @"\" + cbxLivro.Text.Trim() + ".vlt"))
                //  makeRel(camEntrada + @"\" + cbxLivro.Text.Trim() + ".vlt");
                // lsvDados.Visible = true;
                //lsv.Visible = false;
            }
            else
            {
                #region SQL Filtro

                string sql =
                    "select lei_seq as SEQUENCIA, lei_endereco AS ENDEREÇO, lei_numero AS NUMERO, ltr_id AS LEITURISTA, ";

                if (Request.Cookies["CliID"].Value.ToString().PadLeft(2, '0') == "05" || Request.Cookies["CliID"].Value.ToString().PadLeft(2, '0') == "17")
                    sql += "lei_data +' '+ lei_hora AS DATA ";
                else
                {
                    if (Request.Cookies["CliID"].Value.ToString().PadLeft(2, '0') == "09")
                        sql += " SUBSTRING(LEI_DATA,7,2)+'/'+SUBSTRING(LEI_DATA,9,2)+'/'+SUBSTRING(LEI_DATA,11,4)+' '+SUBSTRING(LEI_DATA,1,2)+':'+SUBSTRING(LEI_DATA,3,2)+':'+SUBSTRING(LEI_DATA,5,2) AS DATA ";
                    else
                    {
                        sql += "lei_data AS DATA ";
                    }
                }
                sql+="from wtr_leituras where lei_data != '' and cli_id = " + Request.Cookies["CliID"].Value + " and emp_id = " + Request.Cookies["EmpID"].Value;

                if ((cbxLtr.SelectedValue != null) && (cbxLtr.SelectedIndex > 0))
                    sql += " and ltr_id = " + cbxLtr.SelectedValue.ToString();

                if (cbxAno.SelectedIndex > 0)
                    sql += " and liv_ano = " + cbxAno.Text;

                if (cbxMes.SelectedIndex > 0)
                    sql += " and liv_mes = " + cbxMes.SelectedIndex.ToString();

                if ((cbxLivro.SelectedValue != null) && (cbxLivro.SelectedIndex >= 0))
                    sql += " and loc_id = " + cbxLivro.SelectedValue.ToString();


                if (Request.Cookies["CliID"].Value.ToString().PadLeft(2, '0') == "09")
                    sql +=
                        "ORDER BY CONVERT(DATETIME,SUBSTRING(LEI_DATA,7,2)+'/'+SUBSTRING(LEI_DATA,9,2)+'/'+SUBSTRING(LEI_DATA,11,4)+' '+SUBSTRING(LEI_DATA,1,2)+':'+SUBSTRING(LEI_DATA,3,2)+':'+SUBSTRING(LEI_DATA,5,2),103)";
                   else sql += " ORDER BY LEI_DATA";
                if (Request.Cookies["CliID"].Value.ToString().PadLeft(2, '0') == "05" || Request.Cookies["CliID"].Value.ToString().PadLeft(2, '0') == "17")
                    sql += ",lei_hora ";

                #endregion

                DataTable dt = db.GetDt(carregaConexao().Replace('|', ';'), "WTR_HORA", sql);

                //lsv.Items.Clear();

                DateTime hora = DateTime.Now.Date;

                if (dt.Rows.Count > 0)
                {
                    GridView1.DataSource = dt;
                    GridView1.DataBind();
                }
                else
                {
                    GridView1.DataSource = null;
                    GridView1.DataBind();
                }

                // lsvDados.Visible = false;
                //lsv.Visible = true;
            }
        }
        catch (Exception ex)
        {
            msg("Informação: " + ex.Message);
        }
    }

    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            GridView1.PageIndex = e.NewPageIndex;
            carregaGrid();
            GridView1.DataBind();
        }
        catch{}
    }
    protected void btnConsultar_Click(object sender, ImageClickEventArgs e)
    {
        carregaGrid();
    }
    protected void cbxLtr_SelectedIndexChanged(object sender, EventArgs e)
    {
        loadLivros();
    }
    protected void cbxAno_SelectedIndexChanged(object sender, EventArgs e)
    {
        loadLivros();
    }
    protected void cbxMes_SelectedIndexChanged(object sender, EventArgs e)
    {
        
        loadLivros();
    }
}