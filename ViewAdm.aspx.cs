﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WaterSyncLite.Class;
using WaterSyncLite.DAL;
using WaterSyncLite.MDL;

public partial class ViewAdm : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        this.Master.LoadHeader(this.Master, Request.Path.Substring(Request.Path.LastIndexOf("/") + 1));
        if (!IsPostBack)
        {


            Data db = new Data();

          

           /* #region EMPRESAS
            DataTable dtEmpresa = db.GetDt(Conexao.StrCon, "WTR_EMPRESA", "SELECT CLI_ID, EMP_RAZAO FROM "
                + " WTR_EMPRESAS  ORDER BY EMP_RAZAO");

            dtr = dtEmpresa.NewRow();
            dtr["EMP_RAZAO"] = "Selecione uma empresa...";
            dtr["CLI_ID"] = 0;
            dtEmpresa.Rows.Add(dtr);
            if (dt != null)
            {
                ddlEmp.DataSource = dtEmpresa;
                ddlEmp.DataTextField = "EMP_RAZAO";
                ddlEmp.DataValueField = "CLI_ID";
                ddlEmp.DataBind();
                ddlEmp.SelectedValue = "0";

            }
            #endregion*/
            UserDAL userDAL = new UserDAL();
            User user = userDAL.getUser(Convert.ToInt32(Request.Cookies["user"].Value));
            loadEmpresa(user);
        }
    }

    protected String carregaConexao()
    {
        String StrCon = "";
        if (Request.Cookies["StrCon"] != null)
        {
            if (Request.Cookies["StrCon"].Value.ToString().Length > 0)
                StrCon = Request.Cookies["StrCon"].Value.ToString();
        }
        return StrCon.Replace('|', ';').Replace("usuario", xml.GetConexaoValue(Server.MapPath("~"), "LOCAL", "USER")).Replace("senha", xml.GetConexaoValue(Server.MapPath("~"), "LOCAL", "PASSWORD"));
    }

    protected void btPesquisar_Click(object sender, EventArgs e)
    {
        Data db = new Data();
        string sql = "select * from wtr_user inner join wtr_empresas on (usr_cli = wtr_empresas.cli_id) where usr_id != 1 and";

        if (ddlEmp.SelectedValue.ToString() != "0")
            sql += " EMP_RAZAO like '" + ddlEmp.SelectedValue + "' and";
        sql = sql.Trim().Substring(0, sql.Length - 3);

        DataTable dtUsuarios = db.GetDt(Conexao.StrCon, "WTR_USER", sql);

        gridUsuarios.DataSource = dtUsuarios;
        gridUsuarios.DataBind();


    }

    private void loadEmpresa(User usuario)
    {
        try
        {
            EmpresaDAL ep = new EmpresaDAL();

            if (ep.getEmp(usuario.Id, ddlEmp) > 1)
            {
                ddlEmp.Enabled = true;
                
            }
            /*else
            {
                Empresa emp = new Empresa();
                ep.getEmp(emp, usuario.Id);
            }*/
        }
        catch (Exception ex)
        {
            //Files.WriteLog(xml.GetConfigValue("LOCAL", "LOG"), "LOG.txt", DateTime.Now.ToString() + " - " + ex.Message + "\r\n");
        }
    }
    protected void chkTelaImport_CheckedChanged(object sender, EventArgs e)
    {
        try
        {
            CheckBox ck1 = (CheckBox)sender;
            GridViewRow grow = (GridViewRow)ck1.NamingContainer;

            if (gridUsuarios.Rows[grow.RowIndex].RowType == DataControlRowType.DataRow)
            {
                string sql = "update wtr_user set ";
                if (((CheckBox)gridUsuarios.Rows[grow.RowIndex].Cells[4].Controls[1]).Checked)
                    sql += " USR_TELAIMPORT = 1,";
                else sql += " USR_TELAIMPORT = 0,";
                if (((CheckBox)gridUsuarios.Rows[grow.RowIndex].Cells[5].Controls[1]).Checked)
                    sql += " USR_TELAEXPORT = 1,";
                else sql += " USR_TELAEXPORT = 0,";
                if (((CheckBox)gridUsuarios.Rows[grow.RowIndex].Cells[6].Controls[1]).Checked)
                    sql += " USR_TELASYNC = 1,";
                else sql += " USR_TELASYNC = 0,";
                if (((CheckBox)gridUsuarios.Rows[grow.RowIndex].Cells[7].Controls[1]).Checked)
                    sql += " USR_TELAREPASSE = 1,";
                else sql += " USR_TELAREPASSE = 0,";
                if (((CheckBox)gridUsuarios.Rows[grow.RowIndex].Cells[8].Controls[1]).Checked)
                    sql += " USR_TELALIBERARROTAS = 1,";
                else sql += " USR_TELALIBERARROTAS = 0,";
                if (((CheckBox)gridUsuarios.Rows[grow.RowIndex].Cells[9].Controls[1]).Checked)
                    sql += " USR_TELAALTERACAOLEITURA = 1,";
                else sql += " USR_TELAALTERACAOLEITURA = 0,";
                if (((CheckBox)gridUsuarios.Rows[grow.RowIndex].Cells[10].Controls[1]).Checked)
                    sql += " USR_TELAANALISECRITICA = 1,";
                else sql += " USR_TELAANALISECRITICA = 0,";
                if (((CheckBox)gridUsuarios.Rows[grow.RowIndex].Cells[11].Controls[1]).Checked)
                    sql += " USR_TELAFECHAMENTO = 1,";
                else sql += " USR_TELAFECHAMENTO = 0,";
                if (((CheckBox)gridUsuarios.Rows[grow.RowIndex].Cells[12].Controls[1]).Checked)
                    sql += " USR_TELADATALEITURA = 1,";
                else sql += " USR_TELADATALEITURA = 0,";
                if (((CheckBox)gridUsuarios.Rows[grow.RowIndex].Cells[13].Controls[1]).Checked)
                    sql += " USR_TELAOCORRENCIAS = 1,";
                else sql += " USR_TELAOCORRENCIAS = 0,";
                if (((CheckBox)gridUsuarios.Rows[grow.RowIndex].Cells[14].Controls[1]).Checked)
                    sql += " USR_TELAROTEIROS = 1,";
                else sql += " USR_TELAROTEIROS = 0,";
                if (((CheckBox)gridUsuarios.Rows[grow.RowIndex].Cells[15].Controls[1]).Checked)
                    sql += " USR_TELAGEOREFERENCIAMENTO = 1";
                else sql += " USR_TELAGEOREFERENCIAMENTO = 0";               

                sql += " where usr_id = " + ((Label)gridUsuarios.Rows[grow.RowIndex].Cells[0].Controls[1]).Text;

                Data db = new Data();
                bool check = db.Exe(Conexao.StrCon, "WTR_USER", sql);
                if (!check)
                {
                    lblMsg.Text = "Erro ao atualizar dados";
                    PanelMsg.Visible = true;
                }
            }
        }
        catch (Exception ex)
        {
            lblMsg.Text = "Erro - " + ex.Message;
            PanelMsg.Visible = true;
        }



    }
}