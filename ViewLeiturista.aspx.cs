﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Globalization;
using WaterSyncLite.MDL;
using WaterSyncLite.DAL;
using WaterSyncLite.Class;
using System.IO;
using System.ServiceModel.Channels;
using System.ServiceModel.Security;
using System.Text;
using AjaxControlToolkit;
using iTextSharp.text;
using Microsoft.WindowsMobile.Samples.Location;
using System.Drawing;
using System.Drawing.Imaging;
using System.Drawing.Drawing2D;

public partial class ViewLeiturista : System.Web.UI.Page
{
    
    protected void Page_Load(object sender, EventArgs e)
    {
        DataTable dtGrid;
        this.Master.LoadHeader(this.Master, Request.Path.Substring(Request.Path.LastIndexOf("/") + 1));
        if (!IsPostBack)
        {
            String StrCon = carregaConexao();

            string sql = "";

            Data db = new Data();
            #region Grid
            dtGrid = db.GetDt(carregaConexao(), "WTR_LEITURISTA", "SELECT LTR_ID AS CÓDIGO, LTR_NOME AS NOME, LTR_SOBRENOME AS SOBRENOME, LTR_USUARIO AS USUARIO, LTR_SENHA AS SENHA FROM "
                + " WTR_LEITURISTAS WHERE CLI_ID = " + Request.Cookies["CliID"].Value.ToString().Trim()
                + " AND EMP_ID = " + Request.Cookies["EmpID"].Value.ToString().Trim()
                + " ORDER BY LTR_ID");

            GridLeiturista.DataSource = dtGrid;
            GridLeiturista.DataBind();

            #endregion
        }
    }


    protected void btCancelar_Click(object sender, EventArgs e)
    {

        txtNome.Text = "";
        txtSobreNome.Text = "";
        txtUsuario.Text = "";
        txtSenha.Text = "";
    }

    protected String carregaConexao()
    {
        String StrCon = "";
        if (Request.Cookies["StrCon"] != null)
        {
            if (Request.Cookies["StrCon"].Value.ToString().Length > 0)
                StrCon = Request.Cookies["StrCon"].Value.ToString();
        }
        return StrCon.Replace('|', ';').Replace("usuario", xml.GetConexaoValue(Server.MapPath("~"), "LOCAL", "USER")).Replace("senha", xml.GetConexaoValue(Server.MapPath("~"), "LOCAL", "PASSWORD"));
    }

    protected void btSalvar_Click(object sender, EventArgs e)
    {
        string strCon = carregaConexao();
        DataTable dt = new DataTable();
        Data db = new Data();
        string erro = "";
        string sql = "";

        try
        {
            sql = "INSERT INTO WTR_LEITURISTAS  (CLI_ID, EMP_ID, LTR_NOME, LTR_SOBRENOME,LTR_TIPO, LTR_USUARIO, LTR_SENHA)VALUES(" + Convert.ToInt32(Request.Cookies["CliID"].Value.ToString().Trim()) + ",1,'" + txtNome.Text + "','" + txtSobreNome.Text + "', 'U', '" + txtUsuario.Text + "', " + txtSenha.Text + ")";
            erro = db.Ins(strCon, sql);
        }
        catch (Exception ex)
        {
            Console.WriteLine(erro);
            throw;
        }

    }
}