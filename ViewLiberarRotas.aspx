﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="ViewLiberarRotas.aspx.cs" Inherits="ViewLiberarRotas" %>
<%@ MasterType VirtualPath="~/Site.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .minha-classe {
            max-width: 200px;
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <form id="form1" runat="server">


        <div class="panel panel-primary">
            <div class="panel-heading">
                <h4 class="panel-title"><span class="TitulosPanel">Gerenciar Rotas</span></h4>
            </div>
            <div class="panel-body">
                <div class="row">

                    <div class="col-lg-4">
                        <div class="input-group">
                            <span class="input-group-addon">Leiturista</span>
                            <asp:DropDownList ID="ddlLtr" runat="server" CssClass="form-control">
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="input-group">
                            <span class="input-group-addon">Mês</span>
                            <asp:DropDownList ID="cbxMes" runat="server" CssClass="form-control">
                                <asp:ListItem>Selecione o mês</asp:ListItem>
                                <asp:ListItem>Janeiro</asp:ListItem>
                                <asp:ListItem>Fevereiro</asp:ListItem>
                                <asp:ListItem>Março</asp:ListItem>
                                <asp:ListItem>Abril</asp:ListItem>
                                <asp:ListItem>Maio</asp:ListItem>
                                <asp:ListItem>Junho</asp:ListItem>
                                <asp:ListItem>Julho</asp:ListItem>
                                <asp:ListItem>Agosto</asp:ListItem>
                                <asp:ListItem>Setembro</asp:ListItem>
                                <asp:ListItem>Outubro</asp:ListItem>
                                <asp:ListItem>Novembro</asp:ListItem>
                                <asp:ListItem>Dezembro</asp:ListItem>
                            </asp:DropDownList>
                        </div>


                    </div>
                    <div class="col-lg-4">
                        <div class="col-lg-6">

                            <div class="input-group">
                                <span class="input-group-addon" id="sizing-addon2">Ano</span>
                                <asp:TextBox ID="txtAno" runat="server" CssClass="form-control bfh-number"></asp:TextBox>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <asp:Button ID="btPesquisar" runat="server" Text="Pesquisar" CssClass="btn btn-primary" OnClick="btPesquisar_Click" />
                        </div>
                    </div>


                </div>


                <div class="row">
                    <div class="col-12">

                    </div>
                </div>

                <div class="row" style="padding-top: 15px">
                    <div class="col-lg-12">
                        <div id="scrollDiv" style="overflow: auto; height: 100%; width: 100%">

                            <asp:GridView ID="gridLiberarRotas" runat="server" AutoGenerateColumns="False"
                                DataKeyNames="loc_id"
                                ShowFooter="True" EmptyDataText="Nenhuma rota encontrada" CellPadding="3" GridLines="Vertical" BackColor="White"
                                BorderColor="#999999" BorderStyle="None" BorderWidth="1px" OnRowDataBound="gridLiberarRotas_RowDataBound" Width="100%" OnRowCommand="gridLiberarRotas_RowCommand">
                                <AlternatingRowStyle BackColor="Gainsboro" />
                                <Columns>

                                    <asp:TemplateField HeaderText="Matrícula" SortExpression="ltr_id" ItemStyle-CssClass="text-center" HeaderStyle-CssClass="text-center">
                                        <EditItemTemplate>
                                            <asp:Label ID="lblMatricula" Text='<%# Eval("ltr_id") %>' runat="server"></asp:Label>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblMatricula" Text='<%# Eval("ltr_id") %>' runat="server"></asp:Label>
                                        </ItemTemplate>

                                        <HeaderStyle CssClass="text-center"></HeaderStyle>

                                        <ItemStyle CssClass="text-center"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Leiturista" ItemStyle-CssClass="text-center" HeaderStyle-CssClass="text-center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblLeitNome" Text='<%# Eval("ltr_nome") %>' runat="server"></asp:Label>
                                        </ItemTemplate>

                                        <HeaderStyle CssClass="text-center"></HeaderStyle>

                                        <ItemStyle CssClass="text-center"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Mês" ItemStyle-CssClass="text-center" HeaderStyle-CssClass="text-center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblMes" Text='<%# Eval("liv_mes") %>' runat="server"></asp:Label>
                                        </ItemTemplate>

                                        <HeaderStyle CssClass="text-center"></HeaderStyle>

                                        <ItemStyle CssClass="text-center"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Ano" ItemStyle-CssClass="text-center" HeaderStyle-CssClass="text-center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblAno" Text='<%# Eval("liv_ano") %>' runat="server"></asp:Label>
                                        </ItemTemplate>

                                        <HeaderStyle CssClass="text-center"></HeaderStyle>

                                        <ItemStyle CssClass="text-center"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Rota" ItemStyle-CssClass="text-center" HeaderStyle-CssClass="text-center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblRota" Text='<%# Eval("loc_id") %>' runat="server"></asp:Label>
                                        </ItemTemplate>

                                        <HeaderStyle CssClass="text-center"></HeaderStyle>

                                        <ItemStyle CssClass="text-center"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Total" SortExpression="liv_total" ItemStyle-CssClass="text-center" HeaderStyle-CssClass="text-center">
                                        <EditItemTemplate>
                                            <asp:Label ID="lblTotal" Text='<%# Eval("liv_total") %>' runat="server"></asp:Label>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblTotal" Text='<%# Eval("liv_total") %>' runat="server"></asp:Label>
                                        </ItemTemplate>

                                        <HeaderStyle CssClass="text-center"></HeaderStyle>

                                        <ItemStyle CssClass="text-center"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Leituras" SortExpression="LIV_LEITURA" ItemStyle-CssClass="text-center" HeaderStyle-CssClass="text-center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblLIV_LEITURA" Text='<%# Eval("LIV_LEITURA") %>' runat="server"></asp:Label>
                                        </ItemTemplate>

                                        <HeaderStyle CssClass="text-center"></HeaderStyle>

                                        <ItemStyle CssClass="text-center"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Não Lidas" SortExpression="LIV_NAOLIDA" ItemStyle-CssClass="text-center" HeaderStyle-CssClass="text-center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblLIV_NAOLIDA" Text='<%# Eval("LIV_NAOLIDA") %>' runat="server"></asp:Label>
                                        </ItemTemplate>

                                        <HeaderStyle CssClass="text-center"></HeaderStyle>

                                        <ItemStyle CssClass="text-center"></ItemStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Data Carga" SortExpression="LIV_DATACARGA" ItemStyle-CssClass="text-center" HeaderStyle-CssClass="text-center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblLIV_DATACARGA" Text='<%# Eval("LIV_DATACARGA") %>' runat="server"></asp:Label>
                                        </ItemTemplate>

                                        <HeaderStyle CssClass="text-center"></HeaderStyle>

                                        <ItemStyle CssClass="text-center"></ItemStyle>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Leiturista" ItemStyle-CssClass="text-center" HeaderStyle-CssClass="text-center">
                                        <ItemTemplate>
                                            <asp:DropDownList ID="ddlContrato" runat="server" DataTextField="ltr_nome"
                                                DataValueField="ltr_id" DataSourceID="SqlDataSourceLeituristas" CssClass="form-control" AutoPostBack="True" OnSelectedIndexChanged="ddlContrato_SelectedIndexChanged">
                                            </asp:DropDownList>
                                        </ItemTemplate>
                                        <HeaderStyle CssClass="text-center"></HeaderStyle>
                                        <ItemStyle CssClass="text-center"></ItemStyle>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Status" SortExpression="LIV_FLAG" ItemStyle-CssClass="text-center minha-classe" HeaderStyle-CssClass="text-center">
                                        <ItemTemplate>
                                            <div class="col-3">
                                            </div>
                                            <div class="col-2">
                                                <asp:Image ID="imgFlag" runat="server" />
                                            </div>
                                            <div class="col-3 text-left">
                                                <asp:Label ID="lblLIV_FLAG" Text='<%# Eval("LIV_FLAG") %>' runat="server"></asp:Label>
                                            </div>
                                            <div class="col-2">
                                            </div>

                                        </ItemTemplate>

                                        <HeaderStyle CssClass="text-center"></HeaderStyle>

                                        <ItemStyle CssClass="text-center"></ItemStyle>
                                    </asp:TemplateField>

                                    <asp:TemplateField ItemStyle-CssClass="text-center" HeaderStyle-CssClass="text-center">
                                        <HeaderTemplate>

                                            <div class="checkbox">
                                                <asp:CheckBox ID="imgDesmarcar" runat="server" ToolTip="Desmarcar Todos" AutoPostBack="True" OnCheckedChanged="imgDesmarcar_CheckedChanged" />Marcar Todas
                                            </div>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <div class="checkbox">
                                                <asp:CheckBox ID="chkMarcar" runat="server" OnCheckedChanged="chkMarcar_CheckedChanged" AutoPostBack="True"></asp:CheckBox>
                                            </div>
                                            <asp:Label ID="lblMsg" runat="server" Text=""></asp:Label>

                                        </ItemTemplate>

                                        <HeaderStyle CssClass="text-center"></HeaderStyle>

                                        <ItemStyle CssClass="text-center"></ItemStyle>
                                    </asp:TemplateField>

                                </Columns>
                                <FooterStyle BackColor="#CCCCCC" ForeColor="Black" />
                                <HeaderStyle BackColor="#4f5b69" Font-Bold="True" ForeColor="White" />
                                <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Left" />
                                <RowStyle BackColor="#EEEEEE" ForeColor="Black" />
                                <SelectedRowStyle BackColor="#e1ffff" Font-Bold="True" ForeColor="White" />
                                <SortedAscendingCellStyle BackColor="#F1F1F1"></SortedAscendingCellStyle>
                                <SortedAscendingHeaderStyle BackColor="#0000A9"></SortedAscendingHeaderStyle>
                                <SortedDescendingCellStyle BackColor="#CAC9C9"></SortedDescendingCellStyle>
                                <SortedDescendingHeaderStyle BackColor="#000065"></SortedDescendingHeaderStyle>
                            </asp:GridView>

                        </div>
                    </div>
                </div>


            </div>
        </div>

        <asp:SqlDataSource ID="SqlDataSourceLeituristas" runat="server"></asp:SqlDataSource>
    </form>


</asp:Content>
