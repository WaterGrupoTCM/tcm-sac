﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WaterSyncLite.MDL;
using WaterSyncLite.DAL;
using WaterSyncLite.Class;

using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.IO;
using System.Xml;
using System.Xml.XPath;
using System.Data.SqlClient;
using System.Globalization;
//using ICSharpCode.SharpZipLib.Zip;
//using ICSharpCode.SharpZipLib.GZip;
using System.Runtime.InteropServices;
using System.Runtime.InteropServices.ComTypes;
using System.Text.RegularExpressions;
//using ICSharpCode.SharpZipLib.Checksums;
//using ICSharpCode.SharpZipLib.BZip2;
//using ICSharpCode.SharpZipLib.Zip.Compression;
//using ICSharpCode.SharpZipLib.Zip.Compression.Streams;
//using System.Data.SqlServerCe;

public partial class ViewImport : System.Web.UI.Page
{
    private CidadeDAL cidDAL = new CidadeDAL();
    private Cidade cid = new Cidade();
    Data db = new Data();
    private string obsMsg;
    private Empresa empresa;
    LeituristaDAL ltr = new LeituristaDAL();
    private string fileFTP = "";
    private string file;
    private string[] fileRinopolis = new string[10];
    private string file2;
    string localImport;
    string localBkp;
    private string path;
    protected void Page_Load(object sender, EventArgs e)
    {
        this.Master.LoadHeader(this.Master, Request.Path.Substring(Request.Path.LastIndexOf("/") + 1));
        if (!IsPostBack)
        {
            //string script = "$(document).ready(function () { $('[id*=btEnviar]').click(); });";
            //ClientScript.RegisterStartupScript(this.GetType(), "load", script, true);
            ltr.listarLeituritas(Request.Cookies["CliID"].Value.ToString().Trim(), Request.Cookies["EmpID"].Value.ToString().Trim(), cbxLeit, carregaConexao());
            carregaListaArquivos();
            if (Request.Cookies["CliID"].Value.ToString().Trim() != "17")
                PanelQtde.Visible = false;
            if (Request.Cookies["CliID"].Value.ToString().Trim() == "17")
                btEnviar.Enabled = false;
            lblQtdLigacoes.Visible = false;




        }

    }

    protected void btnEnviarArq_Click(object sender, EventArgs e)
    {//Faz upload para o servidor dos arquivos de Remessa gerados pelo sistema interno do cliente 

        foreach (HttpPostedFile postedFile in EnviarArq.PostedFiles)
        {

            try
            {
                localImport = Server.MapPath("~/IMPORT/" + Request.Cookies["CliID"].Value.ToString().PadLeft(2, '0'));


                //localBkp = Server.MapPath(xml.GetConfigValue("LOCAL", "IMPORTBKP")); 
                localBkp = Server.MapPath("~/IMPORT/BKP/" + Request.Cookies["CliID"].Value.ToString().PadLeft(2, '0'));
                string caminho = localImport + "\\";
                string nomeArquivo = postedFile.FileName;
                string caminhoCompleto = caminho + nomeArquivo;
                //Response.Write(caminhoCompleto + " MapPath = " + Server.MapPath("~") + @"\Configs\" + Request.Cookies["CliID"].Value.ToString().PadLeft(2, '0') + @"\" + @"Config.xml");
                postedFile.SaveAs(caminhoCompleto);
                lblMsg.Text = nomeArquivo + "  Arquivo enviado com sucesso.";
                PanelMsg.Visible = true;
                carregaListaArquivos();
            }
            catch (Exception ex)
            {
                lblMsg.Text = "Arquivo não enviado erro : " + ex.Message;
            }
        }

    }


    protected String carregaConexao()
    {
        String StrCon = "";
        if (Request.Cookies["StrCon"] != null)
        {
            if (Request.Cookies["StrCon"].Value.ToString().Length > 0)
                StrCon = Request.Cookies["StrCon"].Value.ToString();
        }
        return StrCon.Replace('|', ';').Replace("usuario", xml.GetConexaoValue(Server.MapPath("~"), "LOCAL", "USER")).Replace("senha", xml.GetConexaoValue(Server.MapPath("~"), "LOCAL", "PASSWORD"));
    }
    private void carregaListaArquivos()
    {
        try
        {
            string[] arq;

            int iCont;
            localImport = Server.MapPath("~/IMPORT/" + Request.Cookies["CliID"].Value.ToString().PadLeft(2, '0'));
            lsvArquivos.Items.Clear();
            localBkp = Server.MapPath("~/IMPORT/BKP/" + Request.Cookies["CliID"].Value.ToString().PadLeft(2, '0'));

            if (Directory.Exists(localImport))
            {
                if (xml.GetConfigValue(Server.MapPath("~"), "LOCAL", "FILEIMPORT", Request.Cookies["CliID"].Value.ToString()).Length > 0)  //Nova TAG do arquivo Config.xml
                {
                    arq = Directory.GetFiles(localImport.Trim(), "*." + xml.GetConfigValue(Server.MapPath("~"), "LOCAL", "FILEIMPORT", Request.Cookies["CliID"].Value.ToString()));
                }
                else if ((Convert.ToInt32(Request.Cookies["CliID"].Value.ToString()) == 3) || (Convert.ToInt32(Request.Cookies["CliID"].Value.ToString()) == 101) || (Convert.ToInt32(Request.Cookies["CliID"].Value.ToString()) == 11) || (Convert.ToInt32(Request.Cookies["CliID"].Value.ToString()) == 8) || (Convert.ToInt32(Request.Cookies["CliID"].Value.ToString()) == 16))	//Cetil(IEPE), Ass.Público(Araçatuva), Fiorilli (Tupi Pta)
                {
                    arq = Directory.GetFiles(localImport.Trim(), "*.*");
                }
                else if ((Convert.ToInt32(Request.Cookies["CliID"].Value.ToString()) == 19))
                {
                    arq = Directory.GetFiles(localImport.Trim(), "*.REM");
                }
                else
                    arq = Directory.GetFiles(localImport.Trim(), "*.txt");

                //else if (Convert.ToInt32(Request.Cookies["CliID"].Value.ToString()) ==  5)	//Cebi - MARILIA
                //	arq = Directory.GetFiles(txtDirImp.Text.Trim(),"*.SIM");

                for (int i = 0; i < arq.Length; i++)
                {
                    arq[i] = arq[i].Replace(@"\", "|");
                    string[] part = arq[i].Trim().Split('|');
                    if (chkGeraLivro.Checked)
                    {
                        if ((part[part.Length - 1].Substring(0, 3) != "Liv") && (part[part.Length - 1].Substring(0, 3) != "LIV"))
                        {
                            lsvArquivos.Items.Add(part[part.Length - 1]);

                            string[] arqV;
                            string leit = part[part.Length - 1];
                            arqV = Directory.GetFiles(localImport.Trim(), "LIV*" + leit.Substring(leit.Length - 6, 2) + ".txt");
                            for (int a = 0; a < arqV.Length; a++)
                            {
                                arqV[a] = arqV[a].Replace(@"\", "|");
                                string[] partV = arqV[a].Trim().Split('|');
                                lsvArquivos.Items.Add(partV[partV.Length - 1]);
                            }
                        }
                        else if ((part[part.Length - 1].Substring(0, 6) != "Livro_") && (part[part.Length - 1].Substring(0, 3) != "LIVRO_"))
                        {
                            lsvArquivos.Items.Add(part[part.Length - 1]);
                        }
                    }
                    else
                    {
                        #region DAEM MARILIA - DESABILITADO
                        /*if (Convert.ToInt32(Request.Cookies["CliID"].Value.ToString()) ==  5)
                            {
                                
                                if (part[part.Length - 1].ToString().Trim().Length > 4)
                                {
                                    if (part[part.Length - 1].ToString().Substring(0, 5).ToUpper() != "CARNE")
                                        lstDirImp.Items.Add(part[part.Length - 1]);
                                }
                                else
                                    lstDirImp.Items.Add(part[part.Length - 1]);
                                
                            }
                            else
                            {*/
                        #endregion

                        lsvArquivos.Items.Add(part[part.Length - 1]);
                    }
                }
            }
            #region Copiando Arquivo para a pasta BACKUP
            if (Directory.Exists(localBkp))
            {
                arq = Directory.GetFiles(localBkp.Trim(), "*.txt");
                for (int i = 0; i < arq.Length; i++)
                {
                    arq[i] = arq[i].Replace(@"\", "|");
                    string[] part = arq[i].Trim().Split('|');
                }
            }
            #endregion
        }
        catch (Exception ex) { txtStatus.Text += "\nErro - " + ex.Message; }
    }

    protected void lsvArquivos_SelectedIndexChanged(object sender, EventArgs e)
    {


        if (Convert.ToInt32(Request.Cookies["CliID"].Value) != 17)
        {
            if (Convert.ToInt32(Request.Cookies["CliID"].Value) == 5)
                txtLocal.Text = lsvArquivos.SelectedItem.ToString().Split('.')[0].PadLeft(2, '0').Substring(4, lsvArquivos.SelectedItem.ToString().Split('.')[0].PadLeft(2, '0').Length - 4);
            else
                if (Convert.ToInt32(Request.Cookies["CliID"].Value) == 9)
                txtLocal.Text = lsvArquivos.SelectedItem.ToString().Split('.')[0].PadLeft(2, '0').Substring(lsvArquivos.SelectedItem.ToString().Split('.')[0].PadLeft(2, '0').Length - 3, 3);
            else if (Convert.ToInt32(Request.Cookies["CliID"].Value) == 101)
                lblDataRef.Text = lsvArquivos.SelectedItem.ToString().Substring(2, 2) + "/" +
                                  lsvArquivos.SelectedItem.ToString().Substring(4, 2) + "/" +
                                  lsvArquivos.SelectedItem.ToString().Substring(6, 2);
            else
                if (Convert.ToInt32(Request.Cookies["CliID"].Value) == 14)
                txtLocal.Text = lsvArquivos.SelectedItem.ToString().Split('.')[0].PadLeft(2, '0').Substring(5, lsvArquivos.SelectedItem.ToString().Split('.')[0].PadLeft(2, '0').Length - 5);
            else if (Convert.ToInt32(Request.Cookies["CliID"].Value) == 1)
                txtLocal.Text = lsvArquivos.SelectedItem.ToString().Split('.')[0].Split('_')[1].PadLeft(3, '0');
            else if (Convert.ToInt32(Request.Cookies["CliID"].Value) == 12)
                txtLocal.Text = lsvArquivos.SelectedItem.ToString().Split('.')[0].Split('-')[2];
            else if (Convert.ToInt32(Request.Cookies["CliID"].Value) == 7)
                txtLocal.Text = lsvArquivos.SelectedItem.ToString().Split('.')[0].Split('_')[1];
            else if (Convert.ToInt32(Request.Cookies["CliID"].Value) == 18)
                txtLocal.Text = lsvArquivos.SelectedItem.ToString().Split('.')[0].Split('_')[2].ToUpper().Replace("ROTEIRO", "").Trim();
            else if (Convert.ToInt32(Request.Cookies["CliID"].Value) == 2)
                txtLocal.Text = lsvArquivos.SelectedItem.ToString().Split('.')[0].Split('_')[2].ToUpper().Replace("ROTEIRO", "").Trim();
            else if (Convert.ToInt32(Request.Cookies["CliID"].Value) == 19)
                txtLocal.Text = lsvArquivos.SelectedItem.ToString().Split('.')[0].Substring(6, 2);
            else txtLocal.Text = lsvArquivos.SelectedItem.ToString().Split('.')[0].PadLeft(3, '0');

        }
        else
        {

            string file = lsvArquivos.SelectedItem.ToString();
            string path = Server.MapPath("~/IMPORT/" + Request.Cookies["CliID"].Value.ToString().PadLeft(2, '0'));
            FileStream ofs = File.Open(path + "\\" + file, FileMode.Open, FileAccess.Read, FileShare.None);
            StreamReader sr = new StreamReader(ofs, System.Text.Encoding.GetEncoding("ISO-8859-1"));

            try
            {

                //Aceita caracteres especiais.
                string linha = string.Empty;
                int i = 0;
                while (sr.Peek() >= 0)
                {


                    //lblCount.Refresh();
                    linha = string.Empty;
                    linha = sr.ReadLine();

                    if (linha.Substring(0, 1) == "1" && Convert.ToInt32(Request.Cookies["CliID"].Value) == 17)
                        i++;
                    else
                    {
                        if (linha.Substring(0, 1) != "M" && Convert.ToInt32(Request.Cookies["CliID"].Value) == 101)
                            i++;
                    }


                }


                txtStatus.Text = "Quantidade de registros do arquivo " + file + ": " + i.ToString() + "\n";
                lblQtdLigacoes.Text = i.ToString();
                txtQtdRotas.Visible = true;
                txtLocal.Text = lsvArquivos.SelectedItem.ToString().Substring(1, 2);

                txtQtdRotas.Enabled = true;
                btGerar.Enabled = true;
                GridView1.DataSource = null;
                GridView1.DataBind();
                sr.DiscardBufferedData();
                sr.BaseStream.Seek(0, System.IO.SeekOrigin.Begin);
                String[] ruas = new String[i];
                i = 0;
                while (sr.Peek() >= 0)
                {


                    //lblCount.Refresh();
                    linha = string.Empty;
                    linha = sr.ReadLine();
                    if (linha.Substring(0, 1) == "1" && Convert.ToInt32(Request.Cookies["CliID"].Value) == 17)
                    {
                        ruas[i] = linha.Substring(30, 34).Trim(); //rua
                        i++;
                    }
                    else
                    {
                        if (linha.Substring(0, 1) != "M" && Convert.ToInt32(Request.Cookies["CliID"].Value) == 101)
                        {
                            ruas[i] = linha.Substring(30, 34).Trim(); //rua
                            i++;
                        }
                    }
                }

                /*var todasRuas = ruas.GroupBy(rua => rua).OrderByDescending(rua => rua.Count()).ToList();

                foreach (var dados in todasRuas)
                {

                    
                }*/

                Session["ruas"] = ruas;

            }
            catch (Exception ex)
            {
                sr.Close();
            }
            finally
            {
                sr.Close();
            }




        }
    }

    protected void ddlddlLeiturista_DataBound(object sender, EventArgs e)
    {

    }

    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        #region "Deixa invisível a coluna"
        switch (e.Row.RowType)
        {
            case DataControlRowType.Header:

                break;
            case DataControlRowType.DataRow:
                ltr.listarLeituritas(Request.Cookies["CliID"].Value.ToString(), Request.Cookies["EmpID"].Value.ToString().Trim(), ((DropDownList)e.Row.FindControl("ddlLeiturista")), carregaConexao());
                break;
            case DataControlRowType.Footer:

                break;
        }
        #endregion
    }

    protected void btGerar_Click(object sender, EventArgs e)
    {

        DataTable dt = new DataTable();

        dt.Columns.Add("Rota");
        dt.Columns.Add("Endereco");
        dt.Columns.Add("Qtd");
        dt.Columns.Add("leiturista");





        int total = Convert.ToInt32(lblQtdLigacoes.Text);

        Data db = new Data();
        string[] ruasRotas = new string[Convert.ToInt32(txtQtdRotas.Text)];
        DataTable dtLivro = db.GetDt(carregaConexao(), "wtr_livros", "select max(loc_id) as maiorRota from wtr_livros where liv_mes = " + Request.Cookies["mes"].Value.ToString().Trim() + " and liv_ano = " +
        Request.Cookies["ano"].Value.ToString().Trim() + " AND LOC_ID < 1000");
        int i = 0;

        if (dtLivro.DefaultView.Count > 0)
        {
            if (dtLivro.DefaultView[0].Row["maiorRota"].ToString().Trim() != "")
            {
                i = Convert.ToInt32(dtLivro.DefaultView[0].Row["maiorRota"].ToString());


                for (;
                    i <
                    Convert.ToInt32(txtQtdRotas.Text) +
                    Convert.ToInt32(dtLivro.DefaultView[0].Row["maiorRota"].ToString());
                    i++)
                {
                    DataRow row = dt.NewRow();
                    row["Rota"] = i + 1;
                    if (total % Convert.ToInt32(txtQtdRotas.Text) != 0)
                        if ((Convert.ToInt32(txtQtdRotas.Text) +
                             Convert.ToInt32(dtLivro.DefaultView[0].Row["maiorRota"].ToString())) - 1 == i)
                            row["Qtd"] = (total % Convert.ToInt32(txtQtdRotas.Text)) +
                                         total / Convert.ToInt32(txtQtdRotas.Text);
                        else
                        {
                            row["Qtd"] = total / Convert.ToInt32(txtQtdRotas.Text);
                        }
                    else
                    {
                        row["Qtd"] = total / Convert.ToInt32(txtQtdRotas.Text);
                    }


                    dt.Rows.Add(row);
                    GridView1.DataSource = dt;
                    GridView1.DataBind();

                }
            }
            else
            {
                for (i = 0; i < Convert.ToInt32(txtQtdRotas.Text); i++)
                {
                    DataRow row = dt.NewRow();
                    row["Rota"] = i + 1;
                    if (total % Convert.ToInt32(txtQtdRotas.Text) != 0)
                        if ((Convert.ToInt32(txtQtdRotas.Text)) - 1 == i)
                            row["Qtd"] = (total % Convert.ToInt32(txtQtdRotas.Text)) + total / Convert.ToInt32(txtQtdRotas.Text);
                        else
                        {
                            row["Qtd"] = total / Convert.ToInt32(txtQtdRotas.Text);
                        }
                    else
                    {
                        row["Qtd"] = total / Convert.ToInt32(txtQtdRotas.Text);
                    }
                    dt.Rows.Add(row);
                    GridView1.DataSource = dt;
                    GridView1.DataBind();

                }
            }
        }
        else
        {
            for (i = 0; i < Convert.ToInt32(txtQtdRotas.Text); i++)
            {
                DataRow row = dt.NewRow();
                row["Rota"] = i + 1;
                if (total % Convert.ToInt32(txtQtdRotas.Text) != 0)
                    if ((Convert.ToInt32(txtQtdRotas.Text)) - 1 == i)
                        row["Qtd"] = (total % Convert.ToInt32(txtQtdRotas.Text)) + total / Convert.ToInt32(txtQtdRotas.Text);
                    else
                    {
                        row["Qtd"] = total / Convert.ToInt32(txtQtdRotas.Text);
                    }
                else
                {
                    row["Qtd"] = total / Convert.ToInt32(txtQtdRotas.Text);
                }
                dt.Rows.Add(row);
                GridView1.DataSource = dt;
                GridView1.DataBind();

            }
        }


        String[] ruas;
        ruas = (String[])Session["ruas"];

        int aux = 0;
        int c = 0;
        for (int contador = 0; contador < Convert.ToInt32(txtQtdRotas.Text); contador++)
        {


            if (GridView1.Rows[contador].RowType == DataControlRowType.DataRow)
            {

                for (; c < Convert.ToInt32(((Label)GridView1.Rows[contador].FindControl("lblQtd")).Text) + aux; c++)
                {
                    ruasRotas[contador] += ruas[c] + "|";

                }

                aux += Convert.ToInt32(((Label)GridView1.Rows[contador].FindControl("lblQtd")).Text);


            }


            string[] vet = ruasRotas[contador].Split('|');

            for (int y = 0; y < vet.GroupBy(rua => rua).ToArray().Length; y++)
            {
                ((Label)GridView1.Rows[contador].FindControl("lblEndereco")).Text +=
                    vet.GroupBy(rua => rua).ToArray()[y].Key.ToString() + " - ";
            }


        }
        if (Request.Cookies["CliID"].Value.ToString().Trim() == "17")
            btEnviar.Enabled = true;
    }



    protected void ShowMessage(string msg)
    {
        String scriptString = "<script language = JavaScript>";
        scriptString += "confirm('" + msg + "');";
        scriptString += "</script>";
        Page.ClientScript.RegisterStartupScript(Page.GetType(), "scrt", scriptString);
    }

    protected void btEnviar_Click(object sender, EventArgs e)
    {
        try
        {
            Session["idFaixa"] = "";
            Session["cat"] = "";
            string file = "";
            if (cbxLeit.SelectedValue.ToString() != "0")
            {
                if (Request.Cookies["mes"].Value.ToString().Trim().ToString().Trim() != "")
                {
                    if ((Convert.ToInt32(Request.Cookies["CliID"].Value) != 10) || (Convert.ToInt32(Request.Cookies["CliID"].Value.ToString()) == 10 && txtLocal.Text.Trim().Length > 0))
                    {

                        //progresso.Value = 0;
                        ////////IMPORTAR TODOS OS ARQUIVOS RINOPOLIS de uma vez.
                        ////////RUBENS 27/04/2017
                        if (Convert.ToInt32(Request.Cookies["CliID"].Value.ToString()) == 2)
                        {
                            int i = 0;
                            foreach (var item in lsvArquivos.Items)
                            {
                                fileRinopolis[i] = item.ToString();
                                i++;
                            }
                        }
                        else
                        {
                            file = lsvArquivos.SelectedItem.ToString();
                        }

                        string path = Server.MapPath("~/IMPORT/" + Request.Cookies["CliID"].Value.ToString().PadLeft(2, '0'));
                        string file2 = string.Empty;
                        string obsMsg = string.Empty;

                        //pnlLeiturista.Visible = false;
                        //this.Refresh();

                        int respo = 999;
                        bool carneDoc = false;
                        bool fatura = false;
                        //progress("Início do processo", true);



                        if (Convert.ToInt32(Request.Cookies["CliID"].Value.ToString()) == 5)//CEBI - DAEM MARILIA
                        {
                            Marilia Marilia = new Marilia();

                            respo = Marilia.Import(path + @"\", file, txtLocal.Text, Request.Cookies["mes"].Value.ToString(), Request.Cookies["ano"].Value.ToString(), path, cbxLeit.SelectedValue.ToString(), carregaConexao(), txtStatus);
                        }


                        #region 4R - SAAE Pompeia
                        if (Convert.ToInt32(Request.Cookies["CliID"].Value.ToString()) == 1) //SAAE Pompeia
                        {
                            Pompeia pompeia = new Pompeia();

                            respo = pompeia.Import(path + @"\", file, txtLocal.Text, Request.Cookies["mes"].Value.ToString(), Request.Cookies["ano"].Value.ToString(), path, cbxLeit.SelectedValue.ToString(), carregaConexao(), txtSeparador, cbxLeit, lsvArquivos, txtStatus);

                        }
                        #endregion

                        #region Micromap - SAEC Chavantes
                        if (Convert.ToInt32(Request.Cookies["CliID"].Value.ToString()) == 9) //SAEC Chavantes
                        {
                            Chavantes chavantes = new Chavantes();

                            respo = chavantes.Import(path + @"\", file, txtLocal.Text, Request.Cookies["mes"].Value.ToString(), Request.Cookies["ano"].Value.ToString(), path, cbxLeit.SelectedValue.ToString(), carregaConexao());

                        }
                        #endregion

                        #region Cosmópolis - SAE Cosmópolis
                        else if (Convert.ToInt32(Request.Cookies["CliID"].Value.ToString()) == 16) //Cosmópolis - SAE Cosmópolis
                        {
                            Cosmopolis Cosmopolis = new Cosmopolis();

                            respo = Cosmopolis.Import(path + @"\", file, txtLocal.Text, Request.Cookies["mes"].Value.ToString(), Request.Cookies["ano"].Value.ToString(), path, cbxLeit.SelectedValue.ToString(), carregaConexao(), txtStatus);

                        }
                        #endregion Cosmópolis - SAE Cosmópolis

                        #region São Caetano - DAE São Caetano
                        else if (Convert.ToInt32(Request.Cookies["CliID"].Value.ToString()) == 17) //São Caetano - DAE São Caetano
                        {
                            SaoCaetano SaoCaetano = new SaoCaetano();

                            respo = SaoCaetano.Import(path + @"\", file, txtLocal.Text, Request.Cookies["mes"].Value.ToString(), Request.Cookies["ano"].Value.ToString(), path, cbxLeit.SelectedValue.ToString(), carregaConexao(), GridView1, txtStatus);
                            GridView1.DataSource = null;
                            GridView1.DataBind();

                        }
                        #endregion São Caetano - DAE São Caetano

                        #region Clementina - Prefeitura
                        else if (Convert.ToInt32(Request.Cookies["CliID"].Value.ToString()) == 14) //Prefeitura de Clementina
                        {
                            Clementina clementina = new Clementina();

                            respo = clementina.Import(path + @"\", file, txtLocal.Text, Request.Cookies["mes"].Value.ToString(), Request.Cookies["ano"].Value.ToString(), path, cbxLeit.SelectedValue.ToString(), carregaConexao(), txtStatus);

                        }
                        #endregion Clementina - Prefeitura

                        #region Promissão - Prefeitura
                        else if (Convert.ToInt32(Request.Cookies["CliID"].Value.ToString()) == 12) //Prefeitura de Promissao
                        {
                            Promissao promissao = new Promissao();

                            respo = promissao.Import(path + @"\", file, txtLocal.Text, Request.Cookies["mes"].Value.ToString(), Request.Cookies["ano"].Value.ToString(), path, cbxLeit.SelectedValue.ToString(), carregaConexao(), txtStatus);

                        }
                        #endregion Promissão - Prefeitura

                        #region Rio Pardo - DAEE
                        else if (Convert.ToInt32(Request.Cookies["CliID"].Value.ToString()) == 7) //DAEE Rio Pardo
                        {
                            RioPardo RioPardo = new RioPardo();

                            respo = RioPardo.Import(path + @"\", file, txtLocal.Text, Request.Cookies["mes"].Value.ToString(), Request.Cookies["ano"].Value.ToString(), path, cbxLeit.SelectedValue.ToString(), carregaConexao(), txtSeparador, cbxLeit, lsvArquivos, txtStatus);

                        }
                        #endregion Promissão - Prefeitura

                        #region Iracemápolis - DAE
                        else if (Convert.ToInt32(Request.Cookies["CliID"].Value.ToString()) == 18) //Iracemápolis - DAE
                        {
                            Iracemapolis Iracemapolis = new Iracemapolis();

                            respo = Iracemapolis.Import(path + @"\", file, txtLocal.Text, Request.Cookies["mes"].Value.ToString(), Request.Cookies["ano"].Value.ToString(), path, cbxLeit.SelectedValue.ToString(), carregaConexao(), txtStatus);

                        }
                        #endregion  Iracemápolis - DAE

                        #region Casa Branca - DAEMASA
                        else if (Convert.ToInt32(Request.Cookies["CliID"].Value.ToString()) == 19) //Casa Branca - DAE
                        {
                            CasaBranca casabranca = new CasaBranca();

                            respo = casabranca.Import(path + @"\", file, txtLocal.Text, Request.Cookies["mes"].Value.ToString(), Request.Cookies["ano"].Value.ToString(), path, cbxLeit.SelectedValue.ToString(), carregaConexao(), txtStatus);

                        }
                        #endregion Casa Branca - DAEMASA

                        #region RINÓPOLIS
                        else if (Convert.ToInt32(Request.Cookies["CliID"].Value.ToString()) == 2) //Rinopolis - DAE
                        {
                            Rinopolis rinopolis = new Rinopolis();
                            respo = rinopolis.Import(path + @"\", fileRinopolis, txtLocal.Text, Request.Cookies["mes"].Value.ToString(), Request.Cookies["ano"].Value.ToString(), path, cbxLeit.SelectedValue.ToString(), carregaConexao(), txtSeparador, cbxLeit, lsvArquivos, txtStatus);

                        }

                        #endregion




                        if (respo == 0)
                        {
                            try
                            {
                                localBkp = Server.MapPath("~/IMPORT/BKP/" + Request.Cookies["CliID"].Value.ToString().PadLeft(2, '0'));
                                //lstDirBkp.Items.Add(file);
                                //if (file2 != null)
                                //  lstDirBkp.Items.Add(file2);

                                lsvArquivos.Items.Remove(file);
                                if (file2 != null)
                                    lsvArquivos.Items.Remove(file2);
                            }
                            catch
                            {
                            }
                            //progresso.Value = progresso.Maximum;

                            ShowMessage("Arquivo: " + file.Trim() + " carregado com sucesso !");

                            if (file2 != null)
                            {
                                if (File.Exists(path + @"\" + file2))
                                {
                                    File.Move(path + @"\" + file2, localBkp.Trim() + @"\" + file2);
                                    ShowMessage("Arquivo: " + file2.Trim() + " carregado com sucesso!");
                                }
                            }

                            if (carneDoc)
                            {
                                File.Move(path + @"\" + file2, localBkp.Trim() + @"\" + file2);
                                ShowMessage("Arquivo: " + file2.Trim() + " carregado com sucesso!");
                            }

                            /*if ((fileFTP.Trim() != "") && (chkFTP.Checked))
                            {
                                Upload(fileFTP);
                            }
                            */
                            //Importar();

                            //cbxLeit.Text = "Selecione um leiturista...";
                            File.Move(path + @"\" + file, localBkp.Trim() + @"\" + file);
                        }
                        else
                        {
                            if (respo == -1)
                                ShowMessage("O arquivo " + file + " foi enviado ao banco de dados.");
                        }
                    }
                    else
                    {
                        Response.Write("Informe a Rota/Local.");
                    }
                }
                else
                {
                    Response.Write("Informe a etapa.");
                }
            }
            else
            {
                Response.Write("Selecione um leiturista.");
            }
            if (Request.Cookies["CliID"].Value.ToString().Trim() == "17")
                btEnviar.Enabled = false;
        }
        catch (Exception ex)
        {
            ShowMessage("Informação: " + ex.Message);
        }
    }
}