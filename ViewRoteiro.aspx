﻿<%@ Page Title="Resumo de Roteiros" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="ViewRoteiro.aspx.cs" Inherits="ViewRoteiro" %>

<%@ MasterType VirtualPath="~/Site.master" %>
<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="head">
    <style type="text/css">
        .grid {
            width: 80%;
            padding-left: 40px;
        }

        .style101024 {
            font-size: x-large;
            color: #000099;
        }

        .th {
            text-align: center;
        }

         @media print {
             .form-control {
    display: block;
    width: 100%;
    height: 38px;
    padding: 8px 12px;
    font-size: 14px;
    line-height: 1.428571429;
    color: #555;
    vertical-align: middle;
    background-color: #fff;
    border: 1px solid #ccc;
    border-radius: 4px;
    -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,0.075);
    box-shadow: inset 0 1px 1px rgba(0,0,0,0.075);
    -webkit-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
    transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
}

    .form-control:focus {
        border-color: rgba(82,168,236,0.8);
        outline: 0;
        -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,0.075),0 0 8px rgba(82,168,236,0.6);
        box-shadow: inset 0 1px 1px rgba(0,0,0,0.075),0 0 8px rgba(82,168,236,0.6);
    }

    .form-control[disabled], .form-control[readonly], fieldset[disabled] .form-control {
        cursor: not-allowed;
        background-color: #eee;
    }

textarea.form-control {
    height: auto;
}

.form-group {
    margin-bottom: 15px;
}

            .row {
                margin-right: -15px;
                margin-left: -15px;
                padding-top: 0px;
                font-size: x-small;
            }

            col-1, .col-2, .col-3, .col-4, .col-5, .col-6, .col-7, .col-8, .col-9, .col-10, .col-11, .col-12, .col-sm-1, .col-sm-2, .col-sm-3, .col-sm-4, .col-sm-5, .col-sm-6, .col-sm-7, .col-sm-8, .col-sm-9, .col-sm-10, .col-sm-11, .col-sm-12, .col-lg-1, .col-lg-2, .col-lg-3, .col-lg-4, .col-lg-5, .col-lg-6, .col-lg-7, .col-lg-8, .col-lg-9, .col-lg-10, .col-lg-11, .col-lg-12 {
                position: relative;
                min-height: 1px;
                padding-right: 15px;
                padding-left: 15px;
                top: 0px;
                left: 0px;
            }

            .col-1, .col-2, .col-3, .col-4, .col-5, .col-6, .col-7, .col-8, .col-9, .col-10, .col-11, .col-12 {
                float: left;
            }

            .col-1 {
                width: 8.333333333333332%;
            }

            .col-2 {
                width: 16.666666666666664%;
            }

            .col-3 {
                width: 25%;
            }

            .col-4 {
                width: 33.33333333333333%;
            }

            .col-5 {
                width: 41.66666666666667%;
            }

            .col-6 {
                width: 50%;
            }

            .col-7 {
                width: 58.333333333333336%;
            }

            .col-8 {
                width: 66.66666666666666%;
            }

            .col-9 {
                width: 75%;
            }

            .col-10 {
                width: 83.33333333333334%;
            }

            .col-11 {
                width: 91.66666666666666%;
            }

            .col-12 {
                width: 100%;
            }

            td {
                padding: 0px;
            }
        }

    </style>
    
    <script type="text/javascript">
        
        function printDiv(divName) {
            var printContents = document.getElementById(divName).innerHTML;
            var originalContents = document.body.innerHTML;

            document.body.innerHTML = printContents;

            window.print();

            document.body.innerHTML = originalContents;
        }

    </script>

</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="ContentPlaceHolder1">

    <form runat="server">

        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h4 class="panel-title"><span class="TitulosPanel">Consultar Roteiros</span></h4>
                    </div>
                    <div class="panel-body minha-classe">
                        <asp:Panel ID="PanelMsg" runat="server" Visible="false">
                            <div class="alert alert-info">
                                <strong>Atenção! </strong>
                                <asp:Label ID="lblMsg" runat="server" Text=""></asp:Label>
                            </div>
                        </asp:Panel>
                       
                        
                        
                        <div id="print" >
                             <div class="row" style="padding-top: 10px">
                                  <div class="row">
                            <div class="col-lg-12">
                                <div class="col-lg-2" style="padding-left: 0px">
                                    <div class="input-group">
                                        <span class="input-group-addon">Mês</span>
                                        <asp:DropDownList ID="cbxMes" runat="server" CssClass="form-control">
                                            <asp:ListItem>Selecione o mês</asp:ListItem>
                                            <asp:ListItem>Janeiro</asp:ListItem>
                                            <asp:ListItem>Fevereiro</asp:ListItem>
                                            <asp:ListItem>Março</asp:ListItem>
                                            <asp:ListItem>Abril</asp:ListItem>
                                            <asp:ListItem>Maio</asp:ListItem>
                                            <asp:ListItem>Junho</asp:ListItem>
                                            <asp:ListItem>Julho</asp:ListItem>
                                            <asp:ListItem>Agosto</asp:ListItem>
                                            <asp:ListItem>Setembro</asp:ListItem>
                                            <asp:ListItem>Outubro</asp:ListItem>
                                            <asp:ListItem>Novembro</asp:ListItem>
                                            <asp:ListItem>Dezembro</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>

                                <div class="col-lg-10">
                                    <div class="col-lg-2" style="padding-left: 0px;">

                                        <div class="input-group">
                                            <span class="input-group-addon">Ano</span>
                                            <asp:TextBox ID="txtAno" runat="server" CssClass="form-control bfh-number"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col-lg-2" style="padding-right: 0px;">
                                        <div class="input-group">
                                            <span class="input-group-addon">Grupo</span>
                                            <asp:TextBox ID="txtGrupo" runat="server" CssClass="form-control bfh-number"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col-lg-2" style="padding-right: 0px;">
                                        <div class="input-group">
                                            <span class="input-group-addon">Rota</span>
                                            <asp:TextBox ID="txtRota" runat="server" CssClass="form-control bfh-number"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                    <div class="input-group">
                                        <span class="input-group-addon">Leiturista</span>
                                        <asp:DropDownList ID="cbxLeit" runat="server" CssClass="form-control">
                                        </asp:DropDownList>
                                    </div>

                                </div>

                                <div class="col-lg-1">
                                    <asp:ImageButton ID="btPesquisar" runat="server" CssClass="btn btn-primary BotaoPesquisa" OnClick="btPesquisar_Click" ImageUrl="~/imagens/search.png" />
                                </div>
                                <div class="col-lg-1">
                                    <input type="image" onclick="printDiv('print')" class="btn btn-primary BotaoPesquisa" src="imagens/print.png" />
                                </div>

                                </div>

                                
                            </div>

                        </div>
                            <div class="col-lg-12">
                                <div id="Panelgrid" runat="server" class="">
                                    <div id="scrollDiv" style="overflow: auto; height: 100%; width: 100%">
                                        <asp:GridView ID="gridConsulta" runat="server" CellPadding="4" ForeColor="#333333" GridLines="None" EmptyDataText="Nenhum registro encontrado" BackColor="White"
                                            BorderColor="#999999" BorderStyle="None" BorderWidth="1px" Width="100%" AllowPaging="True" PageSize="100" HeaderStyle-CssClass="tblTitle">
                                            <AlternatingRowStyle BackColor="Gainsboro" />
                                            <EditRowStyle HorizontalAlign="Center" />
                                            <FooterStyle BackColor="#CCCCCC" ForeColor="Black" />
                                            <HeaderStyle BackColor="#4f5b69" Font-Bold="True" ForeColor="White" HorizontalAlign="Center" />
                                            <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Left" />
                                            <RowStyle BackColor="#EEEEEE" ForeColor="Black" HorizontalAlign="Center" />
                                            <SelectedRowStyle BackColor="#e1ffff" Font-Bold="True" ForeColor="White" />
                                            <SortedAscendingCellStyle BackColor="#F1F1F1"></SortedAscendingCellStyle>
                                            <SortedAscendingHeaderStyle BackColor="#0000A9"></SortedAscendingHeaderStyle>
                                            <SortedDescendingCellStyle BackColor="#CAC9C9"></SortedDescendingCellStyle>
                                            <SortedDescendingHeaderStyle BackColor="#000065"></SortedDescendingHeaderStyle>
                                        </asp:GridView>
                                    </div>
                                </div>
                            </div>

                        </div>
                        </div>
                       

                    </div>
                </div>
            </div>
        </div>
    </form>

</asp:Content>
