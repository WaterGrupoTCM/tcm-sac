﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site1.master" AutoEventWireup="true"
    CodeFile="Default.aspx.cs" Inherits="_Default" %>

<%@ MasterType VirtualPath="~/Site1.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="headLogin" runat="server">


    <script type="text/javascript">

        window.onload = function () {

            document.getElementById("ContentPlaceHolderLogin_txtUser").placeholder = "Usuário";
            document.getElementById("ContentPlaceHolderLogin_txtPwd").placeholder = "Senha";
            //document.getElementById("ContentPlaceHolderLogin_txtAno").type = "number";
        }



    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderLogin" runat="server">

    <div class="row">
        <div class="col-sm-4"></div>
        <div class="col-sm-4">
            <div class="row text-center Logocentralizado">
                <img src="imagens/tcmsac.png" />
            </div>
        </div>
        <div class="col-4"></div>
    </div>
    <div class="row">
        <div class="col-sm-4 col-2"></div>
        <div class="col-sm-4 col-8">
            <div class="col-sm-12">
                <form runat="server">
                    <div class="modal-body ">
                        <div class="form-group">
                            <asp:TextBox ID="txtUser" runat="server" CssClass="form-control"></asp:TextBox>
                        </div>
                        <div class="form-group">
                            <asp:TextBox ID="txtPwd" runat="server" TextMode="Password" CssClass="form-control"></asp:TextBox>
                        </div>

                        <div id="Panelreferencia" runat="server">
                            <div class="col-sm-6" style="padding-bottom: 10px; padding-left: 0px;">
                                <div class="input-group">
                                    <span class="input-group-addon">Mês</span>
                                    <asp:DropDownList ID="cbxMes" runat="server" CssClass="form-control">
                                        <asp:ListItem>Janeiro</asp:ListItem>
                                        <asp:ListItem>Fevereiro</asp:ListItem>
                                        <asp:ListItem>Março</asp:ListItem>
                                        <asp:ListItem>Abril</asp:ListItem>
                                        <asp:ListItem>Maio</asp:ListItem>
                                        <asp:ListItem>Junho</asp:ListItem>
                                        <asp:ListItem>Julho</asp:ListItem>
                                        <asp:ListItem>Agosto</asp:ListItem>
                                        <asp:ListItem>Setembro</asp:ListItem>
                                        <asp:ListItem>Outubro</asp:ListItem>
                                        <asp:ListItem>Novembro</asp:ListItem>
                                        <asp:ListItem>Dezembro</asp:ListItem>
                                    </asp:DropDownList>
                                </div>

                            </div>
                            <div class="col-sm-6" style="padding-right: 0px;">
                                <div class="input-group">
                                    <span class="input-group-addon" id="sizing-addon2">Ano</span>
                                    <asp:TextBox ID="txtAno" runat="server" CssClass="form-control bfh-number"></asp:TextBox>
                                </div>
                               
                            </div>
                        </div>
                         <div class="form-group" id="PanelEmpresa" runat="server">
                                    <div class="input-group">
                                        <span class="input-group-addon">Empresa</span>
                                        <asp:DropDownList ID="cbxEmpresa" runat="server" CssClass="form-control"></asp:DropDownList>
                                    </div>
                                </div>
                        
                        <div class="form-group">
                            &nbsp;
                            <asp:Button ID="btnLogin" runat="server" Text="Entrar" CssClass="btn btn-primary btn-lg btn-block" OnClick="btnLogin_Click" />
                        </div>

                    </div>
                    <asp:Panel ID="PanelMsg" runat="server" Visible="false">
                        <div class="alert alert-warning">
                            <strong>Atenção! </strong>
                            <asp:Label ID="lblMsg" runat="server" Text="Label"></asp:Label>
                        </div>
                    </asp:Panel>




                </form>
            </div>
        </div>
        <div class="col-sm-4 col-2"></div>
    </div>




</asp:Content>
