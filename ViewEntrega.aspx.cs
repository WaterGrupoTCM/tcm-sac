﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WaterSyncLite.Class;

public partial class ViewEntrega : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        this.Master.LoadHeader(this.Master, Request.Path.Substring(Request.Path.LastIndexOf("/") + 1));    
       
        SqlDataSourceLeituristas.ConnectionString = carregaConexao();
        SqlDataSourceLeituristas.SelectCommand = "select * from wtr_leituristas order by ltr_nome";
        SqlDataSourceLeituristas.DataBind();

        SqlDataSourceEntrega.ConnectionString = carregaConexao();
        SqlDataSourceEntrega.SelectCommand = "select * from wtr_entrega";
        SqlDataSourceEntrega.DataBind();



        if (!IsPostBack)
        {
            Session["chkHeader"] = null;
            txtAno.Text = DateTime.Now.Year.ToString();
            cbxMes.SelectedIndex = DateTime.Now.Month;

            Data db = new Data();
            #region LEITURISTAS
            DataTable dt = db.GetDt(carregaConexao(), "WTR_LEIT", "SELECT LTR_ID, LTR_NOME FROM "
                + " WTR_LEITURISTAS WHERE CLI_ID = " + Request.Cookies["CliID"].Value.ToString().Trim()
                + " AND EMP_ID = " + Request.Cookies["EmpID"].Value.ToString().Trim()
                + " ORDER BY LTR_NOME");

            DataRow dtr = dt.NewRow();
            dtr["LTR_NOME"] = "Selecione um leiturista...";
            dtr["LTR_ID"] = 0;
            ddlLtr.SelectedValue = "0";
            dt.Rows.Add(dtr);
            if (dt != null)
            {
                
                ddlLtr.DataSource = dt;
                ddlLtr.DataTextField = "LTR_NOME";
                ddlLtr.DataValueField = "LTR_ID";
                ddlLtr.DataBind();
            }
            #endregion
        }
    }
    protected String carregaConexao()
    {
        String StrCon = "";
        if (Request.Cookies["StrCon"] != null)
        {
            if (Request.Cookies["StrCon"].Value.ToString().Length > 0)
                StrCon = Request.Cookies["StrCon"].Value.ToString();
        }
        return StrCon.Replace('|', ';').Replace("usuario", xml.GetConexaoValue(Server.MapPath("~"), "LOCAL", "USER")).Replace("senha", xml.GetConexaoValue(Server.MapPath("~"), "LOCAL", "PASSWORD"));
    }
  

    protected void btPesquisar_Click(object sender, EventArgs e)
    {
        try
        {
            
            gridFormaEntrega.PageIndex = 0;
            CarregaGrid();
            Session["chkHeader"] = null;

            
        }
        catch (Exception ex)
        {

            ClientScript.RegisterStartupScript(GetType(), "Message", "<SCRIPT LANGUAGE='javascript'>alert('" + ex.Message.ToString().Replace("'", "") + "');</script>");
        }
    }

    protected void CarregaGrid()
    {
        String StrCon = carregaConexao();
        DataTable dt = new DataTable();

        //if (Convert.ToInt32(Request.Cookies["CliID"].Value.ToString().Trim()) == 17)
        //{
        //    SaoCaetano scs = new SaoCaetano();
        //    scs.Entrega(StrCon, ddlLtr, cbxMes, txtAno, ddlRelatorio, txtIdentificacao, txtGrupo, lblTOTAL, PanelQtdRegistros, gridFormaEntrega);

        //}

        Data db = new Data();
        String where = "";
        if (ddlLtr.SelectedValue.ToString() != "0" || cbxMes.SelectedValue.ToString() != "0" || txtAno.Text.Length > 0)
        {
            where += " where  wtr_leituras.cli_id = " + Request.Cookies["CliID"].Value.ToString().Trim();
            where += "  and wtr_leituras.emp_id = " + Request.Cookies["EmpID"].Value.ToString().Trim();
            if (txtAno.Text.Length > 0 && cbxMes.SelectedIndex.ToString() == "0")
            {
                if (ddlLtr.SelectedValue.ToString() != "0")
                    where += " and wtr_leituras.ltr_id =  " + ddlLtr.SelectedValue.ToString();
            }
            else
            {
                if (ddlLtr.SelectedValue.ToString() != "0")
                    where += " and wtr_leituras.ltr_id =  " + ddlLtr.SelectedValue.ToString();
                if (cbxMes.SelectedIndex.ToString() != "0")
                    where += " and wtr_leituras.liv_mes =  " + cbxMes.SelectedIndex.ToString();
                if (txtAno.Text.Length > 0)
                    where += " and wtr_leituras.liv_ano =  " + txtAno.Text;
                if (txtIdentificacao.Text.Length > 0)
                    where += " and wtr_leituras.wtr_ligacao =  " + txtIdentificacao.Text;
                if (txtGrupo.Text.Length > 0)
                    where += " and wtr_leituras.grupo_id =  " + txtGrupo.Text;
                if (ddlRelatorio.SelectedValue.ToString() == "Retidas")
                    where += " and wtr_leituras.lei_reterconta = 'S' and wtr_leituras.lei_tipobanco != '99' ";
                if (ddlRelatorio.SelectedValue.ToString() == "Protocoladas")
                    where += " and wtr_leituras.lei_tipobanco = '99' ";
            }
        }

        string sqlconsulta = "select wtr_leituras.lei_ltrentrega, ltr_nome,lei_consumidor,lei_endereco, lei_numero, liv_mes, liv_ano,lei_ligacao, loc_id, grupo_id, wtr_entrega.ent_descricao, lei_entregafatura, lei_dataentrega from wtr_leituras"
                        + " inner join wtr_leituristas on wtr_leituristas.ltr_id = wtr_leituras.lei_ltrentrega inner join wtr_entrega on wtr_entrega.ent_id = wtr_leituras.lei_entregafatura " + where;

        sqlconsulta += " order by wtr_leituras.liv_ano,wtr_leituras.liv_mes,wtr_leituras.loc_id";

        string sqlUpdate = "select lei_ltrentrega, lei_ligacao from wtr_leituras  "+ where;

        dt = db.GetDt(StrCon, "WTR_LEIT", sqlUpdate);
        DataTable dtUpdate = new DataTable();

        for (int i = 0; i < dt.Rows.Count; i++)// se o campo Lei_ltrentrega for vazio, ele insere o mesmo leiturista do campo ltr_id da tabela de leitura
        {
            if (dt.Rows[0][0].ToString().Trim() == " " || dt.Rows[0][0].ToString().Trim() == null || dt.Rows[0][0].ToString().Trim() == "")
            { 
                sqlUpdate = "update wtr_leituras set lei_ltrentrega = (select ltr_id from wtr_leituras " + where + " and lei_ligacao = " + dt.Rows[i][1].ToString().Trim()+") " + where + " and lei_ligacao = " + dt.Rows[i][1].ToString().Trim();
                dtUpdate = db.GetDt(StrCon, "WTR_LEIT", sqlUpdate);
            }
        }

        sqlUpdate = "select lei_entregafatura, lei_ligacao from wtr_leituras " + where;
        dt = db.GetDt(StrCon, "WTR_LEIT", sqlUpdate);

        for (int i = 0; i < dt.Rows.Count; i++)// se o campo Lei_entregafatura for vazio, ele insere o primeiro registro da tabela wtr_entrega
        {
            if (dt.Rows[0][0].ToString().Trim() == " " || dt.Rows[0][0].ToString().Trim() == null || dt.Rows[0][0].ToString().Trim() == "")
            {
                sqlUpdate = "update wtr_leituras set lei_entregafatura  = 0 " + where + " and lei_ligacao = " + dt.Rows[i][1].ToString().Trim();
                dtUpdate = db.GetDt(StrCon, "WTR_LEIT", sqlUpdate);
            }
        }

        dt = db.GetDt(StrCon, "WTR_LEIT", sqlconsulta);
        gridFormaEntrega.DataSource = dt;
        gridFormaEntrega.DataBind();
        gridFormaEntrega.Columns[5].Visible = false; //Esconde a Coluna
        gridFormaEntrega.Columns[13].Visible = false; //Esconde a Coluna
        gridFormaEntrega.Columns[14].Visible = false; //Esconde a Coluna
        Session["dt"] = dt;

        if (dt.DefaultView.Count > 0)
        {
            lblTOTAL.Text = string.Format("{0:C}", Convert.ToDecimal(dt.DefaultView.Count.ToString())).Replace("R$", "").Replace(" ", "").Split(',')[0];
            lblTOTAL.Text = string.Format("{0:C}", Convert.ToDecimal(dt.DefaultView.Count.ToString())).Replace("R$", "").Replace(" ", "").Split(',')[0];
            
            PanelQtdRegistros.Visible = true;
        }
        else PanelQtdRegistros.Visible = false;

       



    }

    protected void Cal1_SelectionChanged(object sender, EventArgs e)
    {
        Calendar cal = (Calendar)sender;
        TextBox text1 = (TextBox)((GridViewRow)cal.Parent.Parent).FindControl("text1");

        text1.Text = cal.SelectedDate.ToShortDateString();
    }

   

    protected void ShowMessage(string msg)
    {
        String scriptString = "<script language = JavaScript>";
        scriptString += "confirm('" + msg + "');";
        scriptString += "</script>";
        Page.ClientScript.RegisterStartupScript(Page.GetType(), "scrt", scriptString);
    }


    protected void gridFormaEntrega_RowDataBound(object sender, GridViewRowEventArgs e)
    {
    
        switch (e.Row.RowType)
        {           
            case DataControlRowType.DataRow:   

                ((DropDownList)e.Row.Cells[10].Controls[1]).SelectedValue = ((Label)e.Row.Cells[5].FindControl("lblMatricula")).Text;
                ((DropDownList)e.Row.Cells[11].Controls[1]).SelectedValue = ((Label)e.Row.Cells[13].FindControl("lblEntrega")).Text;
                break;

        }
       
    }   


    protected void ddlEntrega_SelectedIndexChanged(object sender, EventArgs e)
    {
        DropDownList entrega = (DropDownList)sender;
        GridViewRow grow = (GridViewRow)entrega.NamingContainer;


        if (gridFormaEntrega.Rows[grow.RowIndex].RowType == DataControlRowType.DataRow)
        {
            Label mes = (Label)gridFormaEntrega.Rows[grow.RowIndex].Cells[8].Controls[1];
            Label ano = (Label)gridFormaEntrega.Rows[grow.RowIndex].Cells[9].Controls[1];
            Label rota = (Label)gridFormaEntrega.Rows[grow.RowIndex].Cells[4].Controls[1];
            Label ligacao = (Label)gridFormaEntrega.Rows[grow.RowIndex].Cells[3].Controls[1];            

            String sqlUpdateLeitura = string.Format("update wtr_leituras set lei_entregafatura = '"+ entrega.SelectedIndex.ToString() + "' where  loc_id = {0} and liv_mes = {1} and liv_ano = {2} and cli_id = {3} and emp_id = {4} and lei_ligacao={5} ",
                 rota.Text,
                 mes.Text,
                 ano.Text,
                 Request.Cookies["CliId"].Value.ToString(),
                 Request.Cookies["EmpId"].Value.ToString(),
                 ligacao.Text);
            Data db = new Data();

            bool erro = db.Exe(carregaConexao(), "wtr_livros", sqlUpdateLeitura);
            if (!erro)
                ShowMessage("WTR_LIVROS - Erro ao atualizar Forma de Entrega da ligação " + ligacao.Text);
            CarregaGrid();
        }
    }   


    protected void txtDataEntrega_TextChanged(object sender, EventArgs e)
    {
        TextBox data = (TextBox)sender;
        GridViewRow grow = (GridViewRow)data.NamingContainer;
        TextBox txtValor = data.FindControl("txtData") as TextBox;
        if (gridFormaEntrega.Rows[grow.RowIndex].RowType == DataControlRowType.DataRow)
        {
            Label mes = (Label)gridFormaEntrega.Rows[grow.RowIndex].Cells[8].Controls[1];
            Label ano = (Label)gridFormaEntrega.Rows[grow.RowIndex].Cells[9].Controls[1];
            Label rota = (Label)gridFormaEntrega.Rows[grow.RowIndex].Cells[4].Controls[1];
            Label ligacao = (Label)gridFormaEntrega.Rows[grow.RowIndex].Cells[3].Controls[1];
            Label grupo = (Label)gridFormaEntrega.Rows[grow.RowIndex].Cells[7].Controls[1];


            String sqlUpdateLivros = string.Format("update wtr_leituras set lei_dataentrega = '" + data.Text + "' where  loc_id = {0} and liv_mes = {1} and liv_ano = {2} and cli_id = {3} and emp_id = {4} and lei_ligacao={5} and grupo_id={6}",
                 rota.Text,
                 mes.Text,
                 ano.Text,
                 Request.Cookies["CliId"].Value.ToString(),
                 Request.Cookies["EmpId"].Value.ToString(),
                 ligacao.Text,
                 grupo.Text);
            Data db = new Data();

            bool erro = db.Exe(carregaConexao(), "wtr_livros", sqlUpdateLivros);
            if (!erro)
                ShowMessage("WTR_LIVROS - Erro ao atualizar Forma de Entrega da ligação " + ligacao.Text);
            CarregaGrid();
        }
        try
        {

        }
        catch { }
    }

    protected void ddlLeiturista_SelectedIndexChanged(object sender, EventArgs e)
    {
        DropDownList leit = (DropDownList)sender;
        GridViewRow grow = (GridViewRow)leit.NamingContainer;


        if (gridFormaEntrega.Rows[grow.RowIndex].RowType == DataControlRowType.DataRow)
        {
            Label mes = (Label)gridFormaEntrega.Rows[grow.RowIndex].Cells[8].Controls[1];
            Label ano = (Label)gridFormaEntrega.Rows[grow.RowIndex].Cells[9].Controls[1];
            Label rota = (Label)gridFormaEntrega.Rows[grow.RowIndex].Cells[4].Controls[1];
            Label ligacao = (Label)gridFormaEntrega.Rows[grow.RowIndex].Cells[3].Controls[1];
            Data db = new Data();           
           
            String sqlUpdateLeituras = string.Format("update wtr_leituras set lei_ltrentrega = '" + leit.SelectedValue.ToString() + "' where loc_id = {0} and liv_mes = {1} and liv_ano = {2} and cli_id = {3} and emp_id = {4} and lei_ligacao = {5}",
               rota.Text,
                mes.Text,
                ano.Text,
                Request.Cookies["CliId"].Value.ToString(),
                Request.Cookies["EmpId"].Value.ToString(),
                ligacao.Text);

            bool erro = db.Exe(carregaConexao(), "wtr_leituras", sqlUpdateLeituras);
            if (!erro)
                ShowMessage("WTR_LEITURAS - Erro ao atualizar rota " + rota.Text);
            CarregaGrid();
        }
    }

    protected void imgExcel_Click(object sender, ImageClickEventArgs e)
    {
        Response.ClearContent();
        Response.Buffer = true;
        Response.AddHeader("content-disposition", string.Format("attachment; filename={0}", "Entrega.xls"));
        Response.ContentType = "application/ms-excel";
        StringWriter sw = new StringWriter();
        HtmlTextWriter htw = new HtmlTextWriter(sw);
        gridFormaEntrega.AllowPaging = false;
        CarregaGrid();
        //Change the Header Row back to white color
        gridFormaEntrega.HeaderRow.Style.Add("background-color", "#FFFFFF");
        //Applying stlye to gridview header cells
        for (int i = 0; i < gridFormaEntrega.HeaderRow.Cells.Count; i++)
        {
            gridFormaEntrega.HeaderRow.Cells[i].Style.Add("background-color", "#507CD1");
        }
        int j = 1;
        //This loop is used to apply stlye to cells based on particular row
        foreach (GridViewRow gvrow in gridFormaEntrega.Rows)
        {
            //gvrow.BackColor = Color.White;
            if (j <= gridFormaEntrega.Rows.Count)
            {
                if (j % 2 != 0)
                {
                    for (int k = 0; k < gvrow.Cells.Count; k++)
                    {
                        gvrow.Cells[k].Style.Add("background-color", "#EFF3FB");
                    }
                }
            }
            j++;
        }
        gridFormaEntrega.RenderControl(htw);
        Response.Write(sw.ToString());
        Response.End();
    }

    public override void VerifyRenderingInServerForm(Control control)
    {
        //Essencial para gerar planilha excel
    }

    protected void gridNormal_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gridFormaEntrega.PageIndex = e.NewPageIndex;
        CarregaGrid();
        gridFormaEntrega.DataBind();
    }

}