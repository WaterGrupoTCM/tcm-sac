﻿<%@ Page Title="Fechamentos municipais" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="ViewFechamentosMunicipais.aspx.cs" Inherits="ViewFechamentosMunicipais" %>

<%@ MasterType VirtualPath="~/Site.master" %>
<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="head">
    <style type="text/css">
        .grid {
            width: 80%;
            padding-left: 40px;
        }

        .style101024 {
            font-size: x-large;
            color: #000099;
        }
        th {
            text-align: center;
        }
    </style>

</asp:Content>
<asp:Content ID="BodyContentdsa" runat="server" ContentPlaceHolderID="ContentPlaceHolder1">
    <form runat="server">

        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h4 class="panel-title"><span class="TitulosPanel">Fechamentos Municipais</span></h4>
                    </div>
                    <div class="panel-body minha-classe">
                        <asp:Panel ID="PanelMsg" runat="server" Visible="false">
                            <div class="alert alert-info">
                                <strong>Atenção! </strong>
                                <asp:Label ID="lblMsg" runat="server" Text=""></asp:Label>
                            </div>
                        </asp:Panel>
                        <div class="row">
                                <div class="col-lg-2" >
                                    <div class="input-group">
                                        <span class="input-group-addon">Mês</span>
                                        <asp:DropDownList ID="cbxMes" runat="server" CssClass="form-control">
                                            <asp:ListItem>Selecione o mês</asp:ListItem>
                                            <asp:ListItem>Janeiro</asp:ListItem>
                                            <asp:ListItem>Fevereiro</asp:ListItem>
                                            <asp:ListItem>Março</asp:ListItem>
                                            <asp:ListItem>Abril</asp:ListItem>
                                            <asp:ListItem>Maio</asp:ListItem>
                                            <asp:ListItem>Junho</asp:ListItem>
                                            <asp:ListItem>Julho</asp:ListItem>
                                            <asp:ListItem>Agosto</asp:ListItem>
                                            <asp:ListItem>Setembro</asp:ListItem>
                                            <asp:ListItem>Outubro</asp:ListItem>
                                            <asp:ListItem>Novembro</asp:ListItem>
                                            <asp:ListItem>Dezembro</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>

                                <div class="col-lg-3" style="padding-left: 0px">

                                    <div class="col-lg-6">
                                        <div class="input-group">
                                            <span class="input-group-addon">Ano</span>
                                            <asp:TextBox ID="txtAno" runat="server" CssClass="form-control bfh-number"></asp:TextBox>
                                        </div>
                                    </div>

                                    <div class="col-lg-6">
                                        <asp:Button ID="Button1" runat="server" Text="Pesquisar" CssClass="btn btn-primary" OnClick="btPesquisar_Click" />
                                    </div>
                                </div>



                        </div>

                        <div class="row" style="padding-top: 10px">

                            <div class="col-lg-12">
                                <div class="col-lg-6" style="padding-left:0px">
                                    <div id="Panelgrid" runat="server" class="">
                                        <div id="scrollDiv" style="overflow: auto; height: 100%; width: 100%">
                                            <asp:GridView ID="gridConsulta" runat="server" CellPadding="4" ForeColor="#333333" GridLines="None" EmptyDataText="Nenhum registro encontrado" BackColor="White"
                                                BorderColor="#999999" BorderStyle="None" BorderWidth="1px" Width="100%" AllowPaging="True" PageSize="100" HeaderStyle-CssClass="tblTitle" OnRowCommand="gridConsulta_RowCommand" OnRowDataBound="gridConsulta_RowDataBound">
                                                <AlternatingRowStyle BackColor="Gainsboro" />


                                                <Columns>
                                                    <asp:ButtonField ButtonType="Image" CommandName="Detalhes" HeaderText="Detalhes" ImageUrl="~/imagens/detalhes.png" />
                                                </Columns>


                                                <EditRowStyle HorizontalAlign="Center" />
                                                <FooterStyle BackColor="#CCCCCC" ForeColor="Black" />
                                                <HeaderStyle BackColor="#4f5b69" Font-Bold="True" ForeColor="White" HorizontalAlign="Center" />
                                                <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Left" />
                                                <RowStyle BackColor="#EEEEEE" ForeColor="Black" HorizontalAlign="Center" />
                                                <SelectedRowStyle BackColor="#e1ffff" Font-Bold="True" ForeColor="White" />
                                                <SortedAscendingCellStyle BackColor="#F1F1F1"></SortedAscendingCellStyle>
                                                <SortedAscendingHeaderStyle BackColor="#0000A9"></SortedAscendingHeaderStyle>
                                                <SortedDescendingCellStyle BackColor="#CAC9C9"></SortedDescendingCellStyle>
                                                <SortedDescendingHeaderStyle BackColor="#000065"></SortedDescendingHeaderStyle>
                                            </asp:GridView>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-6">
                                    <div id="Div1" runat="server" class="">
                                        <div id="scrollDiv2" style="overflow: auto; height: 100%; width: 100%">
                                            <asp:GridView ID="GridView2" runat="server" CellPadding="4" ForeColor="#333333" GridLines="None" EmptyDataText="Nenhum registro encontrado" BackColor="White"
                                                BorderColor="#999999" BorderStyle="None" BorderWidth="1px" Width="100%" AllowPaging="True" PageSize="100" HeaderStyle-CssClass="tblTitle" OnRowDataBound="GridView2_RowDataBound">
                                                <AlternatingRowStyle BackColor="Gainsboro" />
                                                <EditRowStyle HorizontalAlign="Center" />
                                                <FooterStyle BackColor="#CCCCCC" ForeColor="Black" />
                                                <HeaderStyle BackColor="#4f5b69" Font-Bold="True" ForeColor="White" HorizontalAlign="Center" />
                                                <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Left" />
                                                <RowStyle BackColor="#EEEEEE" ForeColor="Black" HorizontalAlign="Center" />
                                                <SelectedRowStyle BackColor="#e1ffff" Font-Bold="True" ForeColor="White" />
                                                <SortedAscendingCellStyle BackColor="#F1F1F1"></SortedAscendingCellStyle>
                                                <SortedAscendingHeaderStyle BackColor="#0000A9"></SortedAscendingHeaderStyle>
                                                <SortedDescendingCellStyle BackColor="#CAC9C9"></SortedDescendingCellStyle>
                                                <SortedDescendingHeaderStyle BackColor="#000065"></SortedDescendingHeaderStyle>
                                            </asp:GridView>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>

                    </div>
                </div>
            </div>
        </div>
    </form>


</asp:Content>
