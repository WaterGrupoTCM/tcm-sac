﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WaterSyncLite.Class;

public partial class ViewFechamentosMunicipais : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        this.Master.LoadHeader(this.Master, Request.Path.Substring(Request.Path.LastIndexOf("/") + 1));
        if (!IsPostBack)
        {
            txtAno.Text = DateTime.Now.Date.Year.ToString();
            cbxMes.SelectedIndex = DateTime.Now.Date.Month;
        }
    }

    protected void CarregaFechamentos()
    {
        try
        {
            Data db = new Data();
            DataTable dt = new DataTable();
            dt.Columns.Add("Cliente");
            dt.Columns.Add("Ref.");
            dt.Columns.Add("Total");
            dt.Columns.Add("Impresso");
            dt.Columns.Add("Lidas");
            //dt.Columns.Add("Cliente");

            #region Pompéia
            ///////////////////////***********POMPÉIA*****//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            DataRow row = dt.NewRow();
            String sql = "select count(*) as qtde,(select count(*) as qtdeImpresso from  wtr_leituras where liv_mes = " +
                cbxMes.SelectedIndex + " and liv_ano = " + txtAno.Text + " and lei_impresso = 'S' and emp_id= 1 and cli_id = 1) as 'Qtd. Impresso',(select count(*) as qtdeLidas " +
                "from  wtr_leituras where liv_mes = " + cbxMes.SelectedIndex + " and liv_ano = " + txtAno.Text + " and lei_leitura!='' and emp_id= 1 and cli_id = 1) as 'Qtd. Lidas'" +
                " from  wtr_leituras where liv_mes = " + cbxMes.SelectedIndex + " and liv_ano = " + txtAno.Text + " and emp_id= 1 and cli_id = 1";

            DataTable dtCliente = new DataTable();
            String[] Strcon = carregaConexao().Split('=');
            Strcon[4] = "Pompeia";
            dtCliente = db.GetDt(Strcon[0] + "=" + Strcon[1] + "=" + Strcon[2] + "=" + Strcon[3] + "=" + Strcon[4], "", sql);

            if (dtCliente.DefaultView.Count > 0)
            {
                row[0] = Convert.ToString("SAAE Pompéia");
                row[1] = cbxMes.SelectedItem.Value + "/" + txtAno.Text;
                row[2] = dtCliente.DefaultView[0].Row["qtde"].ToString();
                row[3] = dtCliente.DefaultView[0].Row["Qtd. Impresso"].ToString();
                row[4] = dtCliente.DefaultView[0].Row["Qtd. Lidas"].ToString();
                dt.Rows.Add(row);
            }
            #endregion

            #region Marília
            ///////////////////////***********MARÍLIA*****//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            db = new Data();
            row = dt.NewRow();
            int proxMes = 0, proxAno = 0;
            if (cbxMes.SelectedIndex >= 12)
            {
                proxMes = 1;
                proxAno = Convert.ToInt32(txtAno.Text) + 1;
            }
            else
            {
                proxMes = cbxMes.SelectedIndex + 1;
                proxAno = Convert.ToInt32(txtAno.Text);
            }
            sql = "select (count(*)+(select count(*) from wtr_leituras where liv_mes = " + proxMes + " and liv_ano = " +
                  proxAno + " and loc_id < 200 and emp_id = 1 and cli_id = 5)) as qtde, " +
                  "(select (count(*)+(select count(*) from wtr_leituras where liv_mes = " + proxMes + " and liv_ano = " +
                  proxAno + " and loc_id < 200 and emp_id = 1 and cli_id = 5 and lei_impresso = 'S')) " +
                  "from wtr_leituras where liv_mes = " + cbxMes.SelectedIndex + " and liv_ano = " + txtAno.Text + " and loc_id > 199 and emp_id = 1 and cli_id = 5 and lei_impresso = 'S') as 'Qtd. Impresso', " +
                  "(select (count(*)+(select count(*) from wtr_leituras where liv_mes = " + proxMes + " and liv_ano = " +
                  proxAno + " and loc_id < 200 and emp_id = 1 and cli_id = 5 and lei_leitura != '')) " +
                  "from wtr_leituras where liv_mes = " + cbxMes.SelectedIndex + " and liv_ano = " + txtAno.Text + " and loc_id > 199 and emp_id = 1 and cli_id = 5 and lei_leitura != '') as 'Qtd. Lidas' " +

                  "from wtr_leituras where liv_mes = " + cbxMes.SelectedIndex + " and liv_ano = " + txtAno.Text + " and loc_id > 199 and emp_id = 1 and cli_id = 5";

            Strcon[4] = "grupotcm18";
            dtCliente = db.GetDt(Strcon[0] + "=" + Strcon[1] + "=" + Strcon[2] + "=" + Strcon[3] + "=" + Strcon[4], "", sql);

            if (dtCliente.DefaultView.Count > 0)
            {
                row[0] = Convert.ToString("DAEM Marília");
                row[1] = cbxMes.SelectedItem.Value + "/" + txtAno.Text;
                row[2] = dtCliente.DefaultView[0].Row["qtde"].ToString();
                row[3] = dtCliente.DefaultView[0].Row["Qtd. Impresso"].ToString();
                row[4] = dtCliente.DefaultView[0].Row["Qtd. Lidas"].ToString();
                dt.Rows.Add(row);
            }
            #endregion

            #region Clementina
            ///////////////////////***********CLEMENTINA*****//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            db = new Data();
            row = dt.NewRow();

            sql = "select count(*) as qtde,sum(case when lei_impresso = 'S' then 1 else 0 end) as 'Qtd. Impresso',sum(case when lei_leitura is not null then 1 else 0 end) as 'Qtd. Lidas'" +
                  "from  WTR_LEITURAS where liv_mes = " + cbxMes.SelectedIndex + " and liv_ano = " + txtAno.Text + " and emp_id= 1 and cli_id = 14";

            dtCliente = new DataTable();
            Strcon = carregaConexao().Split('=');
            Strcon[4] = "Clementina";
            dtCliente = db.GetDt(Strcon[0] + "=" + Strcon[1] + "=" + Strcon[2] + "=" + Strcon[3] + "=" + Strcon[4], "", sql);

            if (dtCliente.DefaultView.Count > 0)
            {
                row[0] = Convert.ToString("DAEC Clementina");
                row[1] = cbxMes.SelectedItem.Value + "/" + txtAno.Text;
                row[2] = dtCliente.DefaultView[0].Row["qtde"].ToString();
                row[3] = dtCliente.DefaultView[0].Row["Qtd. Impresso"].ToString();
                row[4] = dtCliente.DefaultView[0].Row["Qtd. Lidas"].ToString();
                dt.Rows.Add(row);
            }
            #endregion

            #region CHAVANTES
            ///////////////////////***********CHAVANTES*****//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            db = new Data();
            row = dt.NewRow();

            Strcon = carregaConexao().Split('=');


            if ((cbxMes.SelectedIndex < 2 && Convert.ToInt32(txtAno.Text.Trim()) == 2016) ||
                (Convert.ToInt32(txtAno.Text.Trim()) < 2016))
            {
                sql = "select count(*) as qtde,(select count(*) as qtdeImpresso from  wtr_fatura where fat_mes = " +
                      cbxMes.SelectedIndex + " and fat_ano = " + txtAno.Text +
                      " and fat_impresso = 'S' and emp_id= 1 and cli_id = 9) as 'Qtd. Impresso',(select count(*) as qtdeLidas " +
                      "from  wtr_fatura where fat_mes = " + cbxMes.SelectedIndex + " and fat_ano = " + txtAno.Text +
                      " and fat_leituraatual!='' and emp_id= 1 and cli_id = 9) as 'Qtd. Lidas'" +
                      " from  wtr_fatura where fat_mes = " + cbxMes.SelectedIndex + " and fat_ano = " + txtAno.Text +
                      " and emp_id= 1 and cli_id = 9";
                Strcon[4] = "grupotcm9";
            }
            else
            {
                sql = "select count(*) as qtde,(select count(*) as qtdeImpresso from  wtr_leituras where liv_mes = " +
                      cbxMes.SelectedIndex + " and liv_ano = " + txtAno.Text +
                      " and lei_impresso = 'S' and emp_id= 1 and cli_id = 9) as 'Qtd. Impresso',(select count(*) as qtdeLidas " +
                      "from  wtr_leituras where liv_mes = " + cbxMes.SelectedIndex + " and liv_ano = " + txtAno.Text +
                      " and lei_leitura!='' and emp_id= 1 and cli_id = 9) as 'Qtd. Lidas'" +
                      " from  wtr_leituras where liv_mes = " + cbxMes.SelectedIndex + " and liv_ano = " + txtAno.Text +
                      " and emp_id= 1 and cli_id = 9";
                Strcon[4] = "Chavantes";
            }

            dtCliente = new DataTable();
            dtCliente = db.GetDt(Strcon[0] + "=" + Strcon[1] + "=" + Strcon[2] + "=" + Strcon[3] + "=" + Strcon[4], "", sql);

            if (dtCliente.DefaultView.Count > 0)
            {
                row[0] = "SAEC Chavantes";
                row[1] = cbxMes.SelectedItem.Value + "/" + txtAno.Text;
                row[2] = dtCliente.DefaultView[0].Row["qtde"].ToString();
                row[3] = dtCliente.DefaultView[0].Row["Qtd. Impresso"].ToString();
                row[4] = dtCliente.DefaultView[0].Row["Qtd. Lidas"].ToString();
                dt.Rows.Add(row);
            }
            #endregion

            #region BILAC
            ///////////////////////***********BILAC*****//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            db = new Data();
            row = dt.NewRow();
            sql = "select count(*) as qtde,(select count(*) as qtdeImpresso from  wtr_fatura where fat_mes = " +
                cbxMes.SelectedIndex + " and fat_ano = " + txtAno.Text + " and fat_impresso = 'S' and emp_id= 1 and cli_id = 15) as 'Qtd. Impresso',(select count(*) as qtdeLidas " +
                "from  wtr_fatura where fat_mes = " + cbxMes.SelectedIndex + " and fat_ano = " + txtAno.Text + " and fat_leituraatual!='' and emp_id= 1 and cli_id = 15) as 'Qtd. Lidas'" +
                " from  wtr_fatura where fat_mes = " + cbxMes.SelectedIndex + " and fat_ano = " + txtAno.Text + " and emp_id= 1 and cli_id = 15";

            dtCliente = new DataTable();
            Strcon = carregaConexao().Split('=');
            Strcon[4] = "grupotcm15";
            dtCliente = db.GetDt(Strcon[0] + "=" + Strcon[1] + "=" + Strcon[2] + "=" + Strcon[3] + "=" + Strcon[4], "", sql);

            if (dtCliente.DefaultView.Count > 0)
            {
                row[0] = "Prefeitura de Bilac";
                row[1] = cbxMes.SelectedItem.Value + "/" + txtAno.Text;
                row[2] = dtCliente.DefaultView[0].Row["qtde"].ToString();
                row[3] = dtCliente.DefaultView[0].Row["Qtd. Impresso"].ToString();
                row[4] = dtCliente.DefaultView[0].Row["Qtd. Lidas"].ToString();
                dt.Rows.Add(row);
            }
            #endregion 

            #region Iracemápolis
            ///////////////////////***********Iracemápolis*****//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            db = new Data();
            row = dt.NewRow();
            sql = "select count(*) as qtde,(select count(*) as qtdeImpresso from  wtr_leituras where liv_mes = " +
                cbxMes.SelectedIndex + " and liv_ano = " + txtAno.Text + " and lei_impresso = 'S' and emp_id= 1 and cli_id = 18) as 'Qtd. Impresso',(select count(*) as qtdeLidas " +
                "from  wtr_leituras where liv_mes = " + cbxMes.SelectedIndex + " and liv_ano = " + txtAno.Text + " and lei_leitura!='' and emp_id= 1 and cli_id = 18) as 'Qtd. Lidas'" +
                " from  wtr_leituras where liv_mes = " + cbxMes.SelectedIndex + " and liv_ano = " + txtAno.Text + " and emp_id= 1 and cli_id = 18";

            dtCliente = new DataTable();
            Strcon = carregaConexao().Split('=');
            Strcon[4] = "Iracemapolis";
            dtCliente = db.GetDt(Strcon[0] + "=" + Strcon[1] + "=" + Strcon[2] + "=" + Strcon[3] + "=" + Strcon[4], "", sql);

            if (dtCliente.DefaultView.Count > 0)
            {
                row[0] = Convert.ToString("Prefitura Iracemápolis");
                row[1] = cbxMes.SelectedItem.Value + "/" + txtAno.Text;
                row[2] = dtCliente.DefaultView[0].Row["qtde"].ToString();
                row[3] = dtCliente.DefaultView[0].Row["Qtd. Impresso"].ToString();
                row[4] = dtCliente.DefaultView[0].Row["Qtd. Lidas"].ToString();
                dt.Rows.Add(row);
            }
            #endregion

            #region Rio Pardo
            ///////////////////////***********RIO PARDO*****//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            db = new Data();
            row = dt.NewRow();

            sql = "select count(*) as qtde,sum(case when lei_impresso = 'S' then 1 else 0 end) as 'Qtd. Impresso',sum(case when lei_leitura !='' then 1 else 0 end) as 'Qtd. Lidas'" +
                  "from  WTR_LEITURAS where liv_mes = " + cbxMes.SelectedIndex + " and liv_ano = " + txtAno.Text + " and emp_id= 1 and cli_id = 7";

            dtCliente = new DataTable();
            Strcon = carregaConexao().Split('=');
            Strcon[4] = "RioPardo";
            dtCliente = db.GetDt(Strcon[0] + "=" + Strcon[1] + "=" + Strcon[2] + "=" + Strcon[3] + "=" + Strcon[4], "", sql);

            if (dtCliente.DefaultView.Count > 0)
            {
                row[0] = Convert.ToString("SAERP Rio Pardo");
                row[1] = cbxMes.SelectedItem.Value + "/" + txtAno.Text;
                row[2] = dtCliente.DefaultView[0].Row["qtde"].ToString();
                row[3] = dtCliente.DefaultView[0].Row["Qtd. Impresso"].ToString();
                row[4] = dtCliente.DefaultView[0].Row["Qtd. Lidas"].ToString();
                dt.Rows.Add(row);
            }
            #endregion

            #region SÃO CAETANO
            ///////////////////////***********SÃO CAETANO*****//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            db = new Data();
            row = dt.NewRow();
            int antMes = 0, antAno = 0;

            if (cbxMes.SelectedIndex == 1)
            {
                antMes = 12;
                antAno = Convert.ToInt32(txtAno.Text) - 1;
            }
            else
            {
                antMes = cbxMes.SelectedIndex - 1;
                antAno = Convert.ToInt32(txtAno.Text);
            }

            sql = "select (count(*)+(select count(*) from wtr_leituras where liv_mes = " + antMes + " and liv_ano = " +
                  antAno + " and grupo_id = 88 and emp_id = 1 and cli_id = 17)) as qtde, " +
                  "(select (count(*)+(select count(*) from wtr_leituras where liv_mes = " + antMes + " and liv_ano = " +
                  antAno + " and grupo_id = 88 and emp_id = 1 and cli_id = 17 and lei_impresso = 'S')) " +
                  "from wtr_leituras where liv_mes = " + cbxMes.SelectedIndex + " and liv_ano = " + txtAno.Text + "  and emp_id = 1 AND GRUPO_ID != 88 and cli_id = 17 and lei_impresso = 'S') as 'Qtd. Impresso', " +
                  "(select (count(*)+(select count(*) from wtr_leituras where liv_mes = " + antMes + " and liv_ano = " +
                  antAno + " and grupo_id = 88 and emp_id = 1 and cli_id = 17 and lei_leitura != '')) " +
                  "from wtr_leituras where liv_mes = " + cbxMes.SelectedIndex + " and liv_ano = " + txtAno.Text + "  AND GRUPO_ID != 88 and emp_id = 1 and cli_id = 17 and lei_leitura != '') as 'Qtd. Lidas' " +

                  "from wtr_leituras where liv_mes = " + cbxMes.SelectedIndex + " and liv_ano = " + txtAno.Text + "  AND GRUPO_ID != 88 and emp_id = 1 and cli_id = 17";

            dtCliente = new DataTable();
            Strcon = carregaConexao().Split('=');
            Strcon[4] = "grupotcm17";
            dtCliente = db.GetDt(Strcon[0] + "=" + Strcon[1] + "=" + Strcon[2] + "=" + Strcon[3] + "=" + Strcon[4], "", sql);

            if (dtCliente.DefaultView.Count > 0)
            {
                row[0] = Convert.ToString("DAE São Caetano");
                row[1] = cbxMes.SelectedItem.Value + "/" + txtAno.Text;
                row[2] = dtCliente.DefaultView[0].Row["qtde"].ToString();
                row[3] = dtCliente.DefaultView[0].Row["Qtd. Impresso"].ToString();
                row[4] = dtCliente.DefaultView[0].Row["Qtd. Lidas"].ToString();
                dt.Rows.Add(row);
            }
            #endregion

            #region Casa Branca
            ///////////////////////***********Casa Branca*****//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            db = new Data();
            row = dt.NewRow();

            sql = "select count(*) as qtde,sum(case when lei_impresso = 'S' then 1 else 0 end) as 'Qtd. Impresso',sum(case when lei_leitura !='' then 1 else 0 end) as 'Qtd. Lidas'" +
                  "from  WTR_LEITURAS where liv_mes = " + cbxMes.SelectedIndex + " and liv_ano = " + txtAno.Text + " and emp_id= 1 and cli_id = 19";

            dtCliente = new DataTable();
            Strcon = carregaConexao().Split('=');
            Strcon[4] = "CasaBranca";
            dtCliente = db.GetDt(Strcon[0] + "=" + Strcon[1] + "=" + Strcon[2] + "=" + Strcon[3] + "=" + Strcon[4], "", sql);

            if (dtCliente.DefaultView.Count > 0)
            {
                row[0] = Convert.ToString("SAERP Rio Pardo");
                row[1] = cbxMes.SelectedItem.Value + "/" + txtAno.Text;
                row[2] = dtCliente.DefaultView[0].Row["qtde"].ToString();
                row[3] = dtCliente.DefaultView[0].Row["Qtd. Impresso"].ToString();
                row[4] = dtCliente.DefaultView[0].Row["Qtd. Lidas"].ToString();
                dt.Rows.Add(row);
            }
            #endregion

            gridConsulta.DataSource = dt;
            gridConsulta.DataBind();
        }
        catch (Exception ex)
        {
            msg("Erro - "+ex.Message);
        }

    }

    protected void msg(string msg)
    {
        lblMsg.Text = msg;
        PanelMsg.Visible = true;
    }



    protected String carregaConexao()
    {
        String StrCon = "";
        if (Request.Cookies["StrCon"] != null)
        {
            if (Request.Cookies["StrCon"].Value.ToString().Length > 0)
                StrCon = Request.Cookies["StrCon"].Value.ToString();
        }
        return StrCon.Replace('|', ';').Replace("usuario", xml.GetConexaoValue(Server.MapPath("~"), "LOCAL", "USER")).Replace("senha", xml.GetConexaoValue(Server.MapPath("~"), "LOCAL", "PASSWORD"));
    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        if(cbxMes.SelectedIndex!=0)
            if (txtAno.Text.ToString().Trim().Length > 0)
                CarregaFechamentos();
            else
            {
                ShowMessage("Por favor, informe o ano");
            }
        else
        {
            ShowMessage("Por favor, selecione o mês");
        }
    }

    protected void ShowMessage(string msg)
    {
        String scriptString = "<script language = JavaScript>";
        scriptString += "confirm('" + msg + "');";
        scriptString += "</script>";
        Page.ClientScript.RegisterStartupScript(Page.GetType(), "scrt", scriptString);
    }
    protected void gridConsulta_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        escondeColunaGrid(e);
    }

    private int Totalretidas = 0;
    private int TotalAlternativas = 0;
    private int Total2Via = 0;
    private int retidas = 0;
    private int Alternativas = 0;
    private int SegundaVia = 0;
    public void escondeColunaGrid( GridViewRowEventArgs e)
    {
        switch (e.Row.RowType)
        {
            case DataControlRowType.Header:
                //e.Row.Cells[coluna].Visible = false;
                break;
            case DataControlRowType.DataRow:
                if (e.Row.Cells[1].Text.ToString().Trim() != "DAE S&#227;o Caetano")
                    e.Row.Cells[0].Controls[0].Visible = false;


                break;
            case DataControlRowType.Footer:
                
                break;
        }
    }
    protected void gridConsulta_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Detalhes")
        {

            #region SÃO CAETANO
            ///////////////////////***********SÃO CAETANO*****//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            DataTable dt = new DataTable();
            Data db = new Data();
            
            db = new Data();
           
            int antMes = 0, antAno = 0;

            if (cbxMes.SelectedIndex == 1)
            {
                antMes = 12;
                antAno = Convert.ToInt32(txtAno.Text) - 1;
            }
            else
            {
                antMes = cbxMes.SelectedIndex - 1;
                antAno = Convert.ToInt32(txtAno.Text);
            }

           /*String sql = "select (count(*)+(select count(*) from wtr_leituras where liv_mes = " + antMes + " and liv_ano = " +
                  antAno + " and grupo_id = 88 and emp_id = 1 and cli_id = 17 and lei_reterconta = 'S')) as qtdeRetidas, " +
                  
                  "(select (count(*)+(select count(*) from wtr_leituras where liv_mes = " + antMes + " and liv_ano = " +
                  antAno + " and grupo_id = 88 and emp_id = 1 and cli_id = 17 and lei_tipobanco = 99)) as qtde " +
                  "from wtr_leituras where liv_mes = " + cbxMes.SelectedIndex + " and liv_ano = " + txtAno.Text + "  AND GRUPO_ID != 88 and emp_id = 1 and cli_id = 17 c) as qtdeAlternativa,"+

                  "(select count(*) from wtr_2via where wtr_2via.lei_impresso = 'S') as qtde2Via" +

                  " from wtr_leituras where liv_mes = " + cbxMes.SelectedIndex + " and liv_ano = " + txtAno.Text + "  AND GRUPO_ID != 88 and emp_id = 1 and cli_id = 17 and lei_reterconta = 'S'";
            */

            String sql = "select grupo_id as Grupo,count(*) as Qtde," +
                         "(select count(*) as 'Contas Retidas' from wtr_leituras l where liv_mes = " + cbxMes.SelectedIndex + " and liv_ano = " + txtAno.Text + "  AND GRUPO_ID != 88 and emp_id = 1 and cli_id = 17 and lei_reterconta = 'S' and lei.grupo_id = l.grupo_id) as 'Contas Retidas' ," +
                         "(select count(*) as 'End. Alternativo' from wtr_leituras l where liv_mes = " + cbxMes.SelectedIndex + " and liv_ano = " + txtAno.Text + "  AND GRUPO_ID != 88 and emp_id = 1 and cli_id = 17 and lei_tipobanco = 99 and lei.grupo_id = l.grupo_id) as 'End. Alternativo' ," +
                         "(select count(*) from wtr_2via where emp_id = 1 and cli_id = 17 and lei_impresso = 'S' and lei.grupo_id = wtr_2via.grupo_id) as '2ª Via' " +
                         "from wtr_leituras lei where liv_mes = " + cbxMes.SelectedIndex + " and liv_ano = " + txtAno.Text + "  AND GRUPO_ID != 88 and emp_id = 1 and cli_id = 17  group by grupo_id";


            dt = new DataTable();
            DataTable dtMesAnterior = new DataTable();
            String[] Strcon = carregaConexao().Split('=');
            Strcon[4] = "grupotcm17";
            dt = db.GetDt(Strcon[0] + "=" + Strcon[1] + "=" + Strcon[2] + "=" + Strcon[3] + "=" + Strcon[4], "", sql);
            sql = "select grupo_id as Grupo,count(*) as Qtde," +
                "(select count(*) as 'Contas Retidas' from wtr_leituras l where liv_mes = " + antMes + " and liv_ano = " + antAno + "  AND GRUPO_ID = 88 and emp_id = 1 and cli_id = 17 and lei_reterconta = 'S' and lei.grupo_id = l.grupo_id) as 'Contas Retidas' ," +
                         "(select count(*) as 'End. Alternativo' from wtr_leituras l where liv_mes = " + antMes + " and liv_ano = " + antAno + "  AND GRUPO_ID = 88 and emp_id = 1 and cli_id = 17 and lei_tipobanco = 99 and lei.grupo_id = l.grupo_id) as 'End. Alternativo' ," +
                         "(select count(*) from wtr_2via where emp_id = 1 and cli_id = 17 and lei_impresso = 'S' and lei.grupo_id = wtr_2via.grupo_id) as '2ª Via' " +
                         "from wtr_leituras lei where liv_mes = " + antMes + " and liv_ano = " + antAno + "  AND GRUPO_ID = 88 and emp_id = 1 and cli_id = 17 group by grupo_id";
            dtMesAnterior = db.GetDt(Strcon[0] + "=" + Strcon[1] + "=" + Strcon[2] + "=" + Strcon[3] + "=" + Strcon[4], "", sql);
            if (dt.DefaultView.Count > 0)
            {

                DataRow row = dt.NewRow();
                row["Grupo"] = dtMesAnterior.DefaultView[0].Row["Grupo"].ToString();
                row["Qtde"] = dtMesAnterior.DefaultView[0].Row["Qtde"].ToString();
                row["Contas Retidas"] = dtMesAnterior.DefaultView[0].Row["Contas Retidas"].ToString();
                row["End. Alternativo"] = dtMesAnterior.DefaultView[0].Row["End. Alternativo"].ToString();
                row["2ª Via"] = dtMesAnterior.DefaultView[0].Row["2ª Via"].ToString();
                dt.Rows.Add(row);
                dt.DefaultView.Sort = "Grupo";
                dt = dt.DefaultView.ToTable(true);
                

                GridView2.DataSource = dt;
                GridView2.DataBind();

            }
            #endregion
        }
    }
    protected void GridView2_RowDataBound(object sender, GridViewRowEventArgs e)
    {

        switch (e.Row.RowType)
        {
            case DataControlRowType.Header:
                //e.Row.Cells[coluna].Visible = false;
                break;
            case DataControlRowType.DataRow:
                if (int.TryParse(e.Row.Cells[1].Text, out retidas))
                {
                    Totalretidas += retidas;
                }
                if (int.TryParse(e.Row.Cells[2].Text, out Alternativas))
                {
                    TotalAlternativas += Alternativas;
                }
                if (int.TryParse(e.Row.Cells[3].Text, out SegundaVia))
                {
                    Total2Via += SegundaVia;
                }

                break;
            case DataControlRowType.Footer:
                e.Row.Cells[0].Text = "TOTAL";
                e.Row.Cells[1].Text = Totalretidas.ToString();
                e.Row.Cells[2].Text = TotalAlternativas.ToString();
                e.Row.Cells[3].Text = Total2Via.ToString();
                e.Row.Cells[0].ForeColor = Color.White;
                e.Row.Cells[1].ForeColor = Color.White;
                e.Row.Cells[2].ForeColor = Color.White;
                e.Row.Cells[3].ForeColor = Color.White;
                break;
        }
        
    }
    protected void btPesquisar_Click(object sender, EventArgs e)
    {
        CarregaFechamentos();
    }
}