﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="View2Via.aspx.cs" Inherits="View2Via" %>
<%@ MasterType VirtualPath="~/Site.master" %>

<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
    

    <script type="text/javascript">

        function printform() {
            var printContent = document.getElementById('<%= Panel6.ClientID %>');
    var printContent2 = document.getElementById('<%= GridView1.ClientID %>');
    var windowUrl = 'about:blank';
    var uniqueName = new Date();
    var windowName = 'Print' + uniqueName.getTime();
    var printWindow = window.open(windowUrl, windowName, 'left=50000,top=50000,width=0,height=0');

    printWindow.document.write(printContent.innerHTML + printContent2.innerHTML);
    printWindow.document.close();
    printWindow.focus();
    printWindow.print();
    printWindow.close();
}

</script>
    
    <style type="text/css">
        body{ font-family:Arial, Helvetica, sans-serif }

         .PadLeft{ margin-left: 20px; }
#conteudo{ 

}
#banner{
 
}
#banner a{ color:#FFFFFF;text-decoration:none }
#banner p { padding: 5px 10px 0; }
p.link{ text-align:center;clear:both }
#fechar{ 
 position:relative;
 float:right; 
 width:20px;
 height:20px;
 background-color:#000000;
 color:#FFFFFF;
 text-align:center; 
}
        .style17
        {
            width: 188px;
            height: 29px;
            font-size: x-large;
            font-weight: bold;
            color: #003399;
            text-align: left;
        }
       
        .style101014
        {
            width: 78px;
        }
        .style101016
        {
            width: 36px;
        }
        .style101019
        {
            width: 73px;
        }
        .style101021
        {
        width: 140px;
    }

        #map {
        height: 70%;
      }
       
    .style101022
    {
        width: 78px;
        height: 24px;
    }
    .style101023
    {
        width: 73px;
        height: 24px;
    }
    .style101024
    {
        width: 36px;
        height: 24px;
    }
    .style101025
    {
        width: 140px;
        height: 24px;
    }
       
        .auto-style1 {
            color: #000099;
            font-size: medium;
        }
       
        .auto-style2 {
            width: 184px;
        }
       
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">

        <asp:Panel ID="PanelImpressao" runat="server">
        <div><span class="style17">Análise e Crítica de Leitura</span><br /></div>
    

    <asp:CheckBox ID="chkEsconder" runat="server" AutoPostBack="True" OnCheckedChanged="chkEsconder_CheckedChanged" Text="Painel Consulta" Font-Bold="True" Font-Size="12pt" />
    <asp:Panel ID="Panel6" runat="server">
        
        <table style="border: 2px outset #003399; width: 100%; ">
            <tr>
                
                <td class="style101014"><strong>Identificação: </strong></td>
                <td class="style101019"><strong>Ano:</strong></td>
                <td class="style101016"><strong>Mês:</strong></td>
                <td class="style101021">
                    <strong>Pesquisar</strong></td>
                <td rowspan="3" class="auto-style2"></td>
                <td rowspan="3" >
                    
                </td>
            </tr>
            <tr>
                <td class="style101014">
                    <asp:TextBox ID="txtIdentificacao" runat="server" Height="23px" Width="119px" 
                    style="margin-left: 0px"></asp:TextBox>
                </td>
                <td class="style101019">
                    <asp:TextBox ID="txtAno" runat="server" Width="62px" Height="23px"></asp:TextBox>
                </td>
                <td class="style101016">
                    <asp:DropDownList ID="cbxMes" runat="server" Height="25px" Width="130px" 
                    style="margin-left: 0px">
                        <asp:ListItem>&lt; TODOS &gt;</asp:ListItem>
                        <asp:ListItem>Janeiro</asp:ListItem>
                        <asp:ListItem>Fevereiro</asp:ListItem>
                        <asp:ListItem>Março</asp:ListItem>
                        <asp:ListItem>Abril</asp:ListItem>
                        <asp:ListItem>Maio</asp:ListItem>
                        <asp:ListItem>Junho</asp:ListItem>
                        <asp:ListItem>Julho</asp:ListItem>
                        <asp:ListItem>Agosto</asp:ListItem>
                        <asp:ListItem>Setembro</asp:ListItem>
                        <asp:ListItem>Outubro</asp:ListItem>
                        <asp:ListItem>Novembro</asp:ListItem>
                        <asp:ListItem>Dezembro</asp:ListItem>
                    </asp:DropDownList>
                </td>
                <td class="style101021">
                    <strong>
                    <asp:ImageButton ID="btnSelect" runat="server" Height="26px"  ImageUrl="~/imagens/1362163260_Search.png" onclick="ImageButton1_Click" Width="36px" CssClass="PadLeft" />
                    </strong>
                </td>
            </tr>
            <tr>
                <td class="style101022">
                    <asp:Panel ID="PanelGrupo" runat="server">
                        <strong>Grupo</strong><br />
                        <asp:TextBox ID="txtGrupo" runat="server" Width="121px" Height="23px"></asp:TextBox>
                    </asp:Panel>
                </td>
                <td class="style101023"><strong>Medidor:<br />
                    <asp:TextBox ID="txtMedidor" runat="server" Height="23px" Width="117px"></asp:TextBox>
                    </strong></td>
                <td class="">
                    <strong>Livro:</strong><br />
                    <asp:TextBox ID="txtLivro" runat="server" Height="23px" Width="60px"></asp:TextBox>
                </td>
                <td class="style101025">
                    &nbsp;<strong>Ocorrencia:</strong><br /> 
                    <asp:DropDownList ID="cbxMsg" runat="server" Height="23px" Width="146px">
                    </asp:DropDownList>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <br />
    
    
    <div style="text-align: right">
        
        
        <span class="auto-style1"><strong>Imprimir Consulta</strong></span><br />
          <asp:Button ID="Button1" runat="server" OnClick="PrintAllPages" Text="Imprimir" />
        <%--<asp:Button ID="Button1" runat="server" OnClick="PrintAllPages" Text="Todas as páginas" />--%>
        &nbsp;<%--<asp:Button ID="Button1" runat="server" OnClick="PrintAllPages" Text="Todas as páginas" />--%><br /></div>
    <div style="text-align: left">
        
        Ordenar por:
        <asp:RadioButton ID="rbSequencia" runat="server" GroupName="1" Text="Sequência" />
        <asp:RadioButton ID="rbLigacao" runat="server" GroupName="1" Text="N° Ligação" />
        <asp:RadioButton ID="rbEndereço" runat="server" GroupName="1" Text="Endereço" />
        <asp:RadioButton ID="rbRota" runat="server" GroupName="1" Text="Rota" />
        <asp:RadioButton ID="rbNomeContribuinte" runat="server" GroupName="1" Text="Nome contribuinte" />
        
    </div>
    <br/>
    <div style="width: 100%; overflow: scroll">
        <asp:GridView ID="GridView1" runat="server" CellPadding="0" 
        GridLines="Vertical" 
        BackColor="White" BorderColor="#999999" BorderStyle="None" 
        BorderWidth="2px" CaptionAlign="Top" HorizontalAlign="Justify" 
        onrowcommand="GridView1_RowCommand" 
        onpageindexchanging="GridView1_PageIndexChanging" 
        style="text-align: center" PageSize="100" OnRowDataBound="GridView1_RowDataBound" Font-Size="10pt" AllowPaging="True">
            <%-- <Columns>
            <asp:ButtonField CommandName="Select" Text="Select" />
        </Columns>--%>
            <AlternatingRowStyle BackColor="Gainsboro" BorderStyle="Inset" />
            <Columns>
                <asp:ButtonField CommandName="Boleto" HeaderImageUrl="~/imagens/fotos.png" Text="Boleto" />
            </Columns>
            <FooterStyle BackColor="#CCCCCC" ForeColor="Black" />
            <HeaderStyle BackColor="#000084" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
            <RowStyle BackColor="#EEEEEE" ForeColor="Black" Font-Size="9pt" />
            <SelectedRowStyle BackColor="#008A8C" Font-Bold="True" ForeColor="White" />
            <SortedAscendingCellStyle BackColor="#F1F1F1" />
            <SortedAscendingHeaderStyle BackColor="#0000A9" />
            <SortedDescendingCellStyle BackColor="#CAC9C9" />
            <SortedDescendingHeaderStyle BackColor="#000065" />
        </asp:GridView>
    </div>
    <br />
    <br />
    <br />
    
    </asp:Panel>
    
</asp:Content>

