﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="ViewImport.aspx.cs" Inherits="ViewImport" %>
<%@ MasterType VirtualPath="~/Site.master" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <link href="inputFile/css/basic.css" rel="stylesheet" />

    <style type="text/css">
        .minha-classe {
            min-height: 200px;
            height: 350px;
            max-height: 350px;
        }
        /* altura mínima de 100px, altura normal de 150px altura máxima de 200px*/

        
        .FileUpload {
            width: 85%;
            text-align: left;
            
        }

        .Titulos {
            font-size: large;
            color: #428bca;
            font-weight: bold;
        }

        .top {
            padding-top: 10px;
        }


        .file-upload {
            display: inline-block;
            overflow: hidden;
            text-align: center;
            vertical-align: middle;
            font-family: Arial;
            border: 1px solid #124d77;
            background: #007dc1;
            color: #fff;
            border-radius: 6px;
            -moz-border-radius: 6px;
            cursor: pointer;
            text-shadow: #000 1px 1px 2px;
            -webkit-border-radius: 6px;
        }

            .file-upload:hover {
                background: -webkit-gradient(linear, left top, left bottom, color-stop(0.05, #0061a7), color-stop(1, #007dc1));
                background: -moz-linear-gradient(top, #0061a7 5%, #007dc1 100%);
                background: -webkit-linear-gradient(top, #0061a7 5%, #007dc1 100%);
                background: -o-linear-gradient(top, #0061a7 5%, #007dc1 100%);
                background: -ms-linear-gradient(top, #0061a7 5%, #007dc1 100%);
                background: linear-gradient(to bottom, #0061a7 5%, #007dc1 100%);
                filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#0061a7', endColorstr='#007dc1',GradientType=0);
                background-color: #0061a7;
            }

        /* The button size */
        .file-upload {
            height: 30px;
        }

            .file-upload, .file-upload span {
                width: 90px;
            }

                .file-upload input {
                    top: 0;
                    left: 0;
                    margin: 0;
                    font-size: 11px;
                    font-weight: bold;
                    /* Loses tab index in webkit if width is set to 0 */
                    opacity: 0;
                    filter: alpha(opacity=0);
                }

                .file-upload strong {
                    font: normal 12px Tahoma,sans-serif;
                    text-align: center;
                    vertical-align: middle;
                }

                .file-upload span {
                    top: 0;
                    left: 0;
                    display: inline-block;
                    /* Adjust button text vertical alignment */
                    padding-top: 5px;
                }
                .centralizarImagem {
            display: flex;
            display: -webkit-flex; /* Garante compatibilidade com navegador Safari. */
            justify-content: center;
            align-items: center;
        }

        invisivel{
            visibility:hidden;
        }

        .modal {
            position: fixed;
            top: 0;
            left: 0;
            background-color: #000000;
            z-index: 99;
            opacity: 0.8;
            filter: alpha(opacity=80);
            -moz-opacity: 0.8;
            min-height: 100%;
            width: 100%;
        }
        .loading
        {
            font-family: Arial;
            font-size: 10pt;
            border: 5px solid #000099;
            width: 200px;
            height: 150px;
            display: none;
            position: fixed;
            background-color: White;
            z-index: 999;
        }

        .imagemCentral {
        }

    </style>

    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
    <script type="text/javascript">
        function ShowProgress() {
            setTimeout(function () {
                var modal = $('<div id="progress"/>');
                modal.addClass("modal");
                modal.addClass("centralizarImagem");
                $('body').append(modal);
                $('<img  src="logo.gif"   alt=""   />').addClass("imagemCentral").appendTo(modal);

                var loading = $(".loading");
                loading.show();
                var top = Math.max($(window).height() / 2 - loading[0].offsetHeight / 2, 0);
                var left = Math.max($(window).width() / 2 - loading[0].offsetWidth / 2, 0);
                loading.css({ top: top, left: left });
            }, 200);
        }
        $('form').live("submit", function () {
            ShowProgress();
        });
    </script>


</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <form runat="server">

           <div class="col-lg-6 col-sm-12" style="padding-left:0px">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h4 class="panel-title"><span class="TitulosPanel">Upload de Arquivos (Passo 1)</span></h4>
                    </div>
                    <div class="panel-body minha-classe">


                        <div class="row">

                            <div class="col-lg-12">
                                <%--<asp:FileUpload ID="FileUpload1" runat="server" CssClass="fileUpload btn btn-primary" />--%>
                                <div class="input-group">                                   
                                    
                                     <asp:FileUpload ID="EnviarArq" CssClass="FileUpload btn btn-primary" runat="server" AllowMultiple="true" />
                                    <div class="input-group-btn">
                                        <!-- Buttons -->

                                        <asp:Button ID="btnEnviarArq" runat="server" Text="Upload" CssClass="form-control btn btn-primary" OnClick="btnEnviarArq_Click" />

                                    </div>


                                </div>

                            </div>

                        </div>
                        <br />
                        <div class="row">
                            <div class="col-lg-12">
                                <h4 class="panel-title" style="padding-bottom: 5px;"><span class="Titulos">Arquivos Disponíveis</span></h4>
                                <asp:ListBox ID="lsvArquivos" runat="server" Height="100%" Width="100%" CssClass="form-control minha-classe" AutoPostBack="True" OnSelectedIndexChanged="lsvArquivos_SelectedIndexChanged"></asp:ListBox>
                            </div>
                        </div>
                        <br />
                        <asp:Panel ID="PanelMsg" runat="server" Visible="false">
                            <div class="alert alert-info">
                                <strong>Atenção! </strong>
                                <asp:Label ID="lblMsg" runat="server" Text=""></asp:Label>
                            </div>
                        </asp:Panel>

                    </div>
                </div>


            </div>

           <div class="col-lg-6 col-sm-12" style="padding-right:10px">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h4 class="panel-title"><span class="TitulosPanel">Importação de Arquivos (Passo 2)</span></h4>
                    </div>
                    <div class="panel-body minha-classe">


                        <div class="row">

                            <div class="col-lg-12">


                                    <div class="col-lg-4" style="padding-left:0px;">
                                        <h4 class="panel-title" style="padding-bottom: 5px;"><span class="Titulos">Rota</span></h4>
                                        <asp:TextBox ID="txtLocal" runat="server" CssClass="form-control"></asp:TextBox>

                                        <h4 class="panel-title" style="padding-bottom: 5px; padding-top: 5px"><span class="Titulos">Selecione o Leiturista</span></h4>
                                        <asp:DropDownList ID="cbxLeit" runat="server" CssClass="form-control"></asp:DropDownList>
                                        <div class="top">
                                            <asp:Button ID="btEnviar" runat="server" Text="Enviar" CssClass="form-control btn btn-primary" OnClick="btEnviar_Click" />
                                        </div>

                                    </div>

                                    <div class="col-lg-3" id="PanelQtde" runat="server">

                                        <h4 class="panel-title" style="padding-bottom: 5px;"><span class="Titulos">Qtde.</span></h4>
                                        <asp:TextBox ID="txtQtdRotas" runat="server" CssClass="form-control"></asp:TextBox>

                                        <div class="top">
                                            <asp:Button ID="btGerar" runat="server" Text="Dividir" CssClass="form-control btn btn-primary" OnClick="btGerar_Click" />
                                        </div>
                                    </div>



                                <div class="row">
                                    <asp:CheckBox ID="chkGeraLivro" runat="server" CssClass="style15"
                                        Text="Gerar arquivo" Visible="False" />
                                    <asp:CheckBox ID="chkUnico" runat="server" CssClass="style13"
                                        Text="Livro + Leitura" Visible="False" />
                                    <asp:CheckBox ID="chkUpdate" runat="server" CssClass="style15"
                                        Text="Atualizar Dados" Visible="False" />
                                    <asp:TextBox ID="txtSeparador" runat="server" Width="74px" Visible="False"></asp:TextBox>
                                    <asp:Label ID="lblDataRef" runat="server" Style="font-weight: 700"></asp:Label>
                                    <asp:Label ID="lblQtdLigacoes" runat="server" Style="font-weight: 700"></asp:Label>
                                </div>


                            </div>

                        </div>

                        <div class="row" style="padding-top: 10px">

                            <div class="col-lg-12" style="padding-bottom:10px">
                                <asp:TextBox ID="txtStatus" runat="server" TextMode="MultiLine"
                                    Width="100%" Enabled="False" Rows="6"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                </div>


            </div>


        <div class="row">
            <div class="col-sm-12">
                <asp:GridView ID="GridView1" runat="server" CellPadding="4" ForeColor="#333333" Width="100%" AutoGenerateColumns="False" OnRowDataBound="GridView1_RowDataBound">
                    <AlternatingRowStyle BackColor="White" />
                    <Columns>
                        <asp:TemplateField HeaderText="Rota" SortExpression="Rota">
                            <ItemTemplate>
                                <asp:Label ID="lblRota" Text='<%# Eval("Rota") %>' runat="server"></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtRota" Text='<%# Eval("rota") %>'
                                    runat="server" Width="50px" ForeColor="#FFA500" Style="font-weight: 700; text-align: center;"></asp:TextBox>
                            </EditItemTemplate>
                            <HeaderStyle BackColor="#4f5b69" />
                            <ItemStyle HorizontalAlign="Center" />
                            <ItemStyle ForeColor="#4f5b69" />
                            <FooterStyle HorizontalAlign="Center" />
                        </asp:TemplateField>


                        <asp:TemplateField HeaderText="Endereço" SortExpression="Endereco">
                            <ItemTemplate>
                                <asp:Label ID="lblEndereco" Text='<%# Eval("Endereco") %>' runat="server"></asp:Label>
                            </ItemTemplate>

                            <HeaderStyle BackColor="#4f5b69" />
                            <ItemStyle HorizontalAlign="Center" />
                            <ItemStyle ForeColor="#4f5b69" />
                            <FooterStyle HorizontalAlign="Center" />
                        </asp:TemplateField>


                        <asp:TemplateField HeaderText="Qtd" SortExpression="Qtd">
                            <ItemTemplate>
                                <asp:Label ID="lblQtd" Text='<%# Eval("Qtd") %>' runat="server"></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtQtd" Text='<%# Eval("Qtd") %>'
                                    runat="server" Width="50px" ForeColor="#FFA500" Style="font-weight: 700; text-align: center;"></asp:TextBox>
                            </EditItemTemplate>
                            <HeaderStyle BackColor="#4f5b69" />
                            <ItemStyle HorizontalAlign="Center" />
                            <ItemStyle ForeColor="#4f5b69" />
                            <FooterStyle HorizontalAlign="Center" />
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Leiturista" SortExpression="Leiturista">
                            <ItemTemplate>
                                <asp:DropDownList ID="ddlLeiturista" Width="130px"
                                    runat="server" DataTextField="LTR_NOME"
                                    DataValueField="LTR_ID" OnDataBound="ddlddlLeiturista_DataBound" CssClass="form-control">
                                </asp:DropDownList>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtleiturista" Text='<%# Eval("leiturista") %>'
                                    runat="server" Width="50px" ForeColor="#FFA500" Style="font-weight: 700; text-align: center;" CssClass="form-control"></asp:TextBox>
                            </EditItemTemplate>
                            <HeaderStyle BackColor="#4f5b69" />
                            <ItemStyle HorizontalAlign="Center" />
                            <ItemStyle ForeColor="#4f5b69" />
                            <FooterStyle HorizontalAlign="Center" />
                        </asp:TemplateField>
                    </Columns>
                    <EditRowStyle BackColor="#2461BF" />
                    <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                    <HeaderStyle BackColor="#4f5b69" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                    <RowStyle BackColor="#EFF3FB" />
                    <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                    <SortedAscendingCellStyle BackColor="#F5F7FB" />
                    <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                    <SortedDescendingCellStyle BackColor="#E9EBEF" />
                    <SortedDescendingHeaderStyle BackColor="#4870BE" />
                </asp:GridView>
            </div>
        </div>
    </form>



</asp:Content>
