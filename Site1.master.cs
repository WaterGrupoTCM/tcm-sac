﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Site1 : System.Web.UI.MasterPage
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    public void CarregaCampos(string cliId, string mes, string ano, string usuario, string empID)
    {

        whriteCookie("CliID", cliId);
        whriteCookie("EmpID", empID);
        whriteCookie("mes", mes);
        whriteCookie("ano", ano);
        whriteCookie("user", usuario);



    }

    public void whriteCookie(string name, string value)
    {
        if (Request.Cookies[name] != null)
        {
            Request.Cookies[name].Values.Clear();
        }
        var newCookie = new HttpCookie(name);
        newCookie.Value = value;
        newCookie.Expires = DateTime.Now.AddMinutes(60); //Expira em 20 minutos. 
        //newCookie.Domain = "187.45.225.14"; //Comente Para funcionar Local 
        //newCookie.Path = "/WaterSyncSqlite"; //Comente Para funcionar Local 
        //newCookie.Secure = true; //Comente Para funcionar Local 
        Response.Cookies.Add(newCookie);
    }
}
