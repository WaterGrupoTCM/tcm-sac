﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WaterSyncLite.Class;

public partial class teste : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        this.Master.LoadHeader(this.Master, Request.Path.Substring(Request.Path.LastIndexOf("/") + 1));
        if (!IsPostBack)
        {

            txtAno.Text = DateTime.Now.Date.Year.ToString();
            cbxMes.SelectedIndex = DateTime.Now.Date.Month;

            Session["latitude"] = "";
            Session["longitude"] = "";
            Session["local"] = "";
            Session["consumidor"] = "";
            Session["leitura"] = "";
            Session["leituraAnterior"] = "";


            Data db = new Data();

            #region LEITURISTAS
            DataTable dt = db.GetDt(carregaConexao(), "WTR_LEIT", "SELECT LTR_ID, LTR_NOME FROM "
                + " WTR_LEITURISTAS WHERE CLI_ID = " + Request.Cookies["CliID"].Value.ToString().Trim()
                + " AND EMP_ID = " + Request.Cookies["EmpID"].Value.ToString().Trim()
                + " ORDER BY LTR_NOME");

            DataRow dtr = dt.NewRow();
            dtr["LTR_NOME"] = "Selecione um leiturista...";
            dtr["LTR_ID"] = 0;
            dt.Rows.Add(dtr);
            if (dt != null)
            {
                cbxLeit.DataSource = dt;
                cbxLeit.DataTextField = "LTR_NOME";
                cbxLeit.DataValueField = "LTR_ID";
                cbxLeit.DataBind();
                cbxLeit.SelectedValue = "0";

            }
            #endregion


            if (Request.QueryString["Mes"] != null)
                cbxMes.SelectedIndex = Convert.ToInt32(Request.QueryString["Mes"].ToString());
            if (Request.QueryString["Ano"] != null)
                txtAno.Text = Request.QueryString["Ano"].ToString();
            if (Request.QueryString["Rota"] != null)
                txtRota.Text = Request.QueryString["Rota"].ToString();
            if (Request.QueryString["Ltr"] != null)
                cbxLeit.SelectedValue = Request.QueryString["Ltr"].ToString();

            if (Session["latitude"] != null)
            {
                //lblLatitudes.Text = Session["latitude"].ToString();
                //lblLongitudes.Text = Session["longitude"].ToString();
                //lblLongitudes.Visible = false;
                //lblLatitudes.Visible = false;

            }


        }


    }

    protected String carregaConexao()
    {
        String StrCon = "";
        if (Request.Cookies["StrCon"] != null)
        {
            if (Request.Cookies["StrCon"].Value.ToString().Length > 0)
                StrCon = Request.Cookies["StrCon"].Value.ToString();
        }
        return StrCon.Replace('|', ';').Replace("usuario", xml.GetConexaoValue(Server.MapPath("~"), "LOCAL", "USER")).Replace("senha", xml.GetConexaoValue(Server.MapPath("~"), "LOCAL", "PASSWORD"));
    }
    protected void btPesquisar_Click(object sender, EventArgs e)
    {
        if (chkMapeamento.Checked)
        {
            Data db = new Data();
            string sql = "select lei_ligacao AS LIGACAO,(SELECT TOP 1 (LEI_LATITUDE + '%'+LEI_LONGITUDE+'%'+LEI_CONSUMIDOR+'%'+LEI_ENDERECO+'%'+cast(LEI_NUMERO as varchar)+" +
                "'%'+cast(LEI_LEITURA as varchar)+'%'+cast(LEI_ANTERIOR as varchar))" + "FROM WTR_LEITURAS T WHERE T.LEI_LIGACAO = S.LEI_LIGACAO " +
                "AND T.LEI_LATITUDE IS NOT NULL AND T.LEI_LATITUDE != '')  AS RESULTADO from wtr_leituras S where S.lei_latitude != ''  ";
            sql += " and S.CLI_ID = " + Request.Cookies["CliID"].Value.ToString().Trim()
                + " AND S.EMP_ID = " + Request.Cookies["EmpID"].Value.ToString().Trim();

            sql += " group by S.lei_ligacao ";
            DataTable dt = db.GetDt(carregaConexao(), "WTR_LEITURAS", sql);

            string latitude = "", longitude = "", local = "", consumidor = "", leitura = "", leituraAnterior = "";


            for (int i = 0; i < dt.DefaultView.Count; i++)
            {



                latitude += dt.DefaultView[i].Row["RESULTADO"].ToString().Split('%')[0] + ",";
                longitude += dt.DefaultView[i].Row["RESULTADO"].ToString().Split('%')[1] + ",";
                consumidor += dt.DefaultView[i].Row["RESULTADO"].ToString().Split('%')[2] + "%";
                leitura += dt.DefaultView[i].Row["RESULTADO"].ToString().Split('%')[5] + ",";
                leituraAnterior += dt.DefaultView[i].Row["RESULTADO"].ToString().Split('%')[6] + ",";
                local += dt.DefaultView[i].Row["RESULTADO"].ToString().Split('%')[3] + "," +
                         dt.DefaultView[i].Row["RESULTADO"].ToString().Split('%')[4] + "%";


            }

            Session["latitude"] = latitude;
            Session["longitude"] = longitude;
            Session["local"] = local;
            Session["consumidor"] = consumidor;
            Session["leitura"] = leitura;
            Session["leituraAnterior"] = leituraAnterior;


            //this.Response.Redirect("teste.aspx?Mes=" + txtMes.Text + "&Ano=" + txtAno.Text + "&Rota=" + txtRota.Text + "&Ltr=" + ddlLeituristas.SelectedValue + "&Qtd");
            //this.Response.Close();
            //this.Response.Redirect("teste.aspx");
        }


        if (txtAno.Text.Trim() != "")
        {
            Data db = new Data();
            string sql = "SELECT LEI_LATITUDE,LEI_LONGITUDE,LEI_ENDERECO AS ENDEREÇO,LEI_NUMERO AS NUMERO,LEI_CONSUMIDOR,LEI_LEITURA,LEI_ANTERIOR FROM "
                + " WTR_LEITURAS WHERE CLI_ID = " + Request.Cookies["CliID"].Value.ToString().Trim()
                + " AND EMP_ID = " + Request.Cookies["EmpID"].Value.ToString().Trim()
                + " AND LIV_ANO =" + txtAno.Text+ "AND LEI_LATITUDE != '' AND LEI_LATITUDE != 'null'";
            if (cbxMes.SelectedIndex.ToString().Trim() != "0")
            {

                sql += " AND LIV_MES = " + cbxMes.SelectedIndex;
            }
            if (txtRota.Text.Trim() != "")
            {
                sql += " AND LOC_ID = " + txtRota.Text;
            }
            if (cbxLeit.SelectedValue.ToString() != "0")
            {
                sql += " AND LTR_ID = " + cbxLeit.SelectedValue;
            }


            sql += " ORDER BY LEI_DATA";

            DataTable dt = db.GetDt(carregaConexao(), "WTR_LEITURAS", sql);

            string latitude = "", longitude = "", local = "", consumidor = "", leitura = "", leituraAnterior = "";


            for (int i = 0; i < dt.DefaultView.Count; i++)
            {
                if (dt.DefaultView[i].Row["LEI_LATITUDE"].ToString().Trim() != "")
                {


                    latitude += dt.DefaultView[i].Row["LEI_LATITUDE"].ToString() + ",";
                    longitude += dt.DefaultView[i].Row["LEI_LONGITUDE"].ToString() + ",";
                    consumidor += dt.DefaultView[i].Row["LEI_CONSUMIDOR"].ToString() + "%";
                    leitura += dt.DefaultView[i].Row["LEI_LEITURA"].ToString() + ",";
                    leituraAnterior += dt.DefaultView[i].Row["LEI_ANTERIOR"].ToString() + ",";
                    local += dt.DefaultView[i].Row["ENDEREÇO"].ToString() + "," +
                             dt.DefaultView[i].Row["NUMERO"].ToString() + "%";

                }
            }

            Session["latitude"] = latitude;
            Session["longitude"] = longitude;
            Session["local"] = local;
            Session["consumidor"] = consumidor;
            Session["leitura"] = leitura;
            Session["leituraAnterior"] = leituraAnterior;



            //this.Response.Redirect("teste.aspx?Mes=" + txtMes.Text + "&Ano=" + txtAno.Text + "&Rota=" + txtRota.Text + "&Ltr=" + ddlLeituristas.SelectedValue);
            // this.Response.Close();
            //this.Response.Redirect("teste.aspx");
        }

    }
}