﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="ViewAdm.aspx.cs" Inherits="ViewAdm" %>
<%@ MasterType VirtualPath="~/Site.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <form id="form1" runat="server">


        <div class="panel panel-primary">
            <div class="panel-heading">
                <h4 class="panel-title"><span class="TitulosPanel">Gerenciar Usuários</span></h4>
            </div>
            <div class="panel-body">
                <asp:Panel ID="PanelMsg" runat="server" Visible="false">
                            <div class="alert alert-info">
                                <strong>Atenção! </strong>
                                <asp:Label ID="lblMsg" runat="server" Text=""></asp:Label>
                            </div>
                        </asp:Panel>
                <div class="row">
                    <div class="col-4">
                        <div class="input-group">
                            <span class="input-group-addon">Empresa</span>
                            <asp:DropDownList ID="ddlEmp" runat="server" CssClass="form-control">
                            </asp:DropDownList>
                        </div>
                    </div>

                   
                    

                    <div class="col-2">
                            <asp:Button ID="btPesquisar" runat="server" Text="Pesquisar" CssClass="btn btn-primary" OnClick="btPesquisar_Click" />
                    </div>


                </div>


                <div class="row" style="padding-top: 15px">
                    <div class="col-12">
                        <div id="scrollDiv" style="overflow: auto; height: 100%; width: 100%">
                            <asp:GridView ID="gridUsuarios" runat="server" AutoGenerateColumns="False"
                                DataKeyNames="usr_id"
                                ShowFooter="True" EmptyDataText="Nenhuma rota encontrada" CellPadding="3" GridLines="Vertical" BackColor="White"
                                BorderColor="#999999" BorderStyle="None" BorderWidth="1px">
                                <AlternatingRowStyle BackColor="Gainsboro" />
                                <Columns>

                                    <asp:TemplateField HeaderText="Código" SortExpression="usr_id" ItemStyle-CssClass="text-center" HeaderStyle-CssClass="text-center" Visible="false">
                                        <EditItemTemplate>
                                            <asp:Label ID="lblCodigo" Text='<%# Eval("usr_id") %>' runat="server"></asp:Label>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblCodigo" Text='<%# Eval("usr_id") %>' runat="server"></asp:Label>
                                        </ItemTemplate>

                                        <HeaderStyle CssClass="text-center"></HeaderStyle>

                                        <ItemStyle CssClass="text-center"></ItemStyle>
                                    </asp:TemplateField>


                                    <asp:TemplateField HeaderText="Usuário" ItemStyle-CssClass="text-center" HeaderStyle-CssClass="text-center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblNomeUser" Text='<%# Eval("usr_name") %>' runat="server"></asp:Label>
                                        </ItemTemplate>

                                        <HeaderStyle CssClass="text-center"></HeaderStyle>

                                        <ItemStyle CssClass="text-center"></ItemStyle>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Empresa" ItemStyle-CssClass="text-center" HeaderStyle-CssClass="text-center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblEmpresa" Text='<%# Eval("emp_razao") %>' runat="server"></asp:Label>
                                        </ItemTemplate>

                                        <HeaderStyle CssClass="text-center"></HeaderStyle>

                                        <ItemStyle CssClass="text-center"></ItemStyle>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Senha" ItemStyle-CssClass="text-center" HeaderStyle-CssClass="text-center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblSenha" Text='<%# Eval("USR_PWD") %>' runat="server"></asp:Label>
                                        </ItemTemplate>

                                        <HeaderStyle CssClass="text-center"></HeaderStyle>

                                        <ItemStyle CssClass="text-center"></ItemStyle>
                                    </asp:TemplateField>


                                    <asp:TemplateField HeaderText="Import" ItemStyle-CssClass="text-center" HeaderStyle-CssClass="text-center">
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkTelaImport" runat="server" Checked='<%# Bind("USR_TELAIMPORT") %>' OnCheckedChanged="chkTelaImport_CheckedChanged" AutoPostBack="true"/>
                                        </ItemTemplate>

                                        <HeaderStyle CssClass="text-center"></HeaderStyle>

                                        <ItemStyle CssClass="text-center"></ItemStyle>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Export" ItemStyle-CssClass="text-center" HeaderStyle-CssClass="text-center">
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkTelaExport" runat="server" Checked='<%# Bind("USR_TELAEXPORT") %>' OnCheckedChanged="chkTelaImport_CheckedChanged" AutoPostBack="true"/>
                                        </ItemTemplate>

                                        <HeaderStyle CssClass="text-center"></HeaderStyle>

                                        <ItemStyle CssClass="text-center"></ItemStyle>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Sync" ItemStyle-CssClass="text-center" HeaderStyle-CssClass="text-center">
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkTelaISync" runat="server" Checked='<%# Bind("USR_TELASYNC") %>' OnCheckedChanged="chkTelaImport_CheckedChanged" AutoPostBack="true"/>
                                        </ItemTemplate>

                                        <HeaderStyle CssClass="text-center"></HeaderStyle>

                                        <ItemStyle CssClass="text-center"></ItemStyle>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Repasse" ItemStyle-CssClass="text-center" HeaderStyle-CssClass="text-center">
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkTelaRepasse" runat="server" Checked='<%# Bind("USR_TELAREPASSE") %>' OnCheckedChanged="chkTelaImport_CheckedChanged" AutoPostBack="true"/>
                                        </ItemTemplate>

                                        <HeaderStyle CssClass="text-center"></HeaderStyle>

                                        <ItemStyle CssClass="text-center"></ItemStyle>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Liberar Rotas" ItemStyle-CssClass="text-center" HeaderStyle-CssClass="text-center">
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkTelaLiberarRotas" runat="server" Checked='<%# Bind("USR_TELALIBERARROTAS") %>' OnCheckedChanged="chkTelaImport_CheckedChanged" AutoPostBack="true"/>
                                        </ItemTemplate>

                                        <HeaderStyle CssClass="text-center"></HeaderStyle>

                                        <ItemStyle CssClass="text-center"></ItemStyle>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Alteração Leituras" ItemStyle-CssClass="text-center" HeaderStyle-CssClass="text-center">
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkTelaAlteracaoLeituras" runat="server" Checked='<%# Bind("USR_TELAALTERACAOLEITURA") %>' OnCheckedChanged="chkTelaImport_CheckedChanged" AutoPostBack="true" />
                                        </ItemTemplate>

                                        <HeaderStyle CssClass="text-center"></HeaderStyle>

                                        <ItemStyle CssClass="text-center"></ItemStyle>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Análise Crítica" ItemStyle-CssClass="text-center" HeaderStyle-CssClass="text-center">
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkTelaAnaliseCritica" runat="server" Checked='<%# Bind("USR_TELAANALISECRITICA") %>' OnCheckedChanged="chkTelaImport_CheckedChanged" AutoPostBack="true"/>
                                        </ItemTemplate>

                                        <HeaderStyle CssClass="text-center"></HeaderStyle>

                                        <ItemStyle CssClass="text-center"></ItemStyle>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Fechamentos" ItemStyle-CssClass="text-center" HeaderStyle-CssClass="text-center">
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkTelaFechamentos" runat="server" Checked='<%# Bind("USR_TELAFECHAMENTO") %>' OnCheckedChanged="chkTelaImport_CheckedChanged" AutoPostBack="true"/>
                                        </ItemTemplate>

                                        <HeaderStyle CssClass="text-center"></HeaderStyle>

                                        <ItemStyle CssClass="text-center"></ItemStyle>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Data Leituras" ItemStyle-CssClass="text-center" HeaderStyle-CssClass="text-center">
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkTelaDataLeituras" runat="server" Checked='<%# Bind("USR_TELADATALEITURA") %>' OnCheckedChanged="chkTelaImport_CheckedChanged" AutoPostBack="true"/>
                                        </ItemTemplate>

                                        <HeaderStyle CssClass="text-center"></HeaderStyle>

                                        <ItemStyle CssClass="text-center"></ItemStyle>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Ocorrências" ItemStyle-CssClass="text-center" HeaderStyle-CssClass="text-center">
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkTelaOcorrencias" runat="server" Checked='<%# Bind("USR_TELAOCORRENCIAS") %>' OnCheckedChanged="chkTelaImport_CheckedChanged" AutoPostBack="true"/>
                                        </ItemTemplate>

                                        <HeaderStyle CssClass="text-center"></HeaderStyle>

                                        <ItemStyle CssClass="text-center"></ItemStyle>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Roteiros" ItemStyle-CssClass="text-center" HeaderStyle-CssClass="text-center">
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkTelaRoteiros" runat="server" Checked='<%# Bind("USR_TELAROTEIROS") %>' OnCheckedChanged="chkTelaImport_CheckedChanged" AutoPostBack="true"/>
                                        </ItemTemplate>

                                        <HeaderStyle CssClass="text-center"></HeaderStyle>

                                        <ItemStyle CssClass="text-center"></ItemStyle>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Georeferenciamento" ItemStyle-CssClass="text-center" HeaderStyle-CssClass="text-center">
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkTelaGeoreferenciamento" runat="server" Checked='<%# Bind("USR_TELAGEOREFERENCIAMENTO") %>' OnCheckedChanged="chkTelaImport_CheckedChanged" AutoPostBack="true" />
                                        </ItemTemplate>

                                        <HeaderStyle CssClass="text-center"></HeaderStyle>

                                        <ItemStyle CssClass="text-center"></ItemStyle>
                                    </asp:TemplateField>

                                   
                                    <%--<asp:TemplateField ItemStyle-CssClass="text-center" HeaderStyle-CssClass="text-center">
                                        <HeaderTemplate>

                                            <div class="checkbox">
                                                <asp:CheckBox ID="imgDesmarcar" runat="server" ToolTip="Desmarcar Todos" AutoPostBack="True" OnCheckedChanged="imgDesmarcar_CheckedChanged" />Marcar Todas
                                            </div>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <div class="checkbox">
                                                <asp:CheckBox ID="chkMarcar" runat="server" OnCheckedChanged="chkMarcar_CheckedChanged" AutoPostBack="True"></asp:CheckBox>
                                            </div>
                                            <asp:Label ID="lblMsg" runat="server" Text=""></asp:Label>

                                        </ItemTemplate>

                                        <HeaderStyle CssClass="text-center"></HeaderStyle>

                                        <ItemStyle CssClass="text-center"></ItemStyle>
                                    </asp:TemplateField>--%>
                                </Columns>
                                <FooterStyle BackColor="#CCCCCC" ForeColor="Black" />
                                <HeaderStyle BackColor="#4f5b69" Font-Bold="True" ForeColor="White" />
                                <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Left" />
                                <RowStyle BackColor="#EEEEEE" ForeColor="Black" />
                                <SelectedRowStyle BackColor="#e1ffff" Font-Bold="True" ForeColor="White" />
                                <SortedAscendingCellStyle BackColor="#F1F1F1"></SortedAscendingCellStyle>
                                <SortedAscendingHeaderStyle BackColor="#0000A9"></SortedAscendingHeaderStyle>
                                <SortedDescendingCellStyle BackColor="#CAC9C9"></SortedDescendingCellStyle>
                                <SortedDescendingHeaderStyle BackColor="#000065"></SortedDescendingHeaderStyle>
                            </asp:GridView>

                        </div>
                    </div>
                </div>


            </div>
        </div>

    </form>

</asp:Content>

