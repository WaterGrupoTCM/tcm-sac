﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="ViewExport.aspx.cs" Inherits="ViewExport" %>

<%@ MasterType VirtualPath="~/Site.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <style type="text/css">
        .centralizarImagem {
            display: flex;
            display: -webkit-flex; /* Garante compatibilidade com navegador Safari. */
            justify-content: center;
            align-items: center;
        }

        .invisivel {
            visibility: hidden;
        }

        .visivel {
            visibility: visible;
        }

        .modal {
            position: fixed;
            top: 0;
            left: 0;
            background-color: #000000;
            z-index: 99;
            opacity: 0.8;
            filter: alpha(opacity=80);
            -moz-opacity: 0.8;
            min-height: 100%;
            width: 100%;
        }

        .loading {
            font-family: Arial;
            font-size: 10pt;
            border: 5px solid #000099;
            width: 200px;
            height: 150px;
            display: none;
            position: fixed;
            background-color: White;
            z-index: 999;
        }

        .imagemCentral {
        }
    </style>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
    <script type="text/javascript">
        //function ShowProgress() {
        //    setTimeout(function () {
        //        var modal = $('<div id="progress"/>');
        //        modal.addClass("modal");
        //        modal.addClass("centralizarImagem");
        //        $('body').append(modal);
        //        $('<img  src="logo.gif"   alt=""   />').addClass("imagemCentral").appendTo(modal);

        //        var loading = $(".loading");
        //        loading.show();
        //        var top = Math.max($(window).height() / 2 - loading[0].offsetHeight / 2, 0);
        //        var left = Math.max($(window).width() / 2 - loading[0].offsetWidth / 2, 0);
        //        loading.css({ top: top, left: left });
        //    }, 200);
        //}
        //$('form').live("submit", function () {
        //    ShowProgress();
        //});

        function ShowProgress() {
            // alert('ShowProgress - Entrou');
            document.getElementById("progress").className = "modal centralizarImagem visivel"
            //alert('ShowProgress - Saiu');

        }



        function fncsave() {
            ShowProgress();
            document.getElementById('<%= btnReceber.ClientID %>').click();
        }


        function HideProgress() {
            // alert('HideProgress - Entrou');

            document.getElementById("progress").className = "modal centralizarImagem invisivel"
            //alert(document.getElementById("progress").className);
            //alert('HideProgress - Saiu');
        }



    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="modal centralizarImagem invisivel" id="progress">
        <img class="imagemCentral" src="logo.gif" alt="" />

    </div>

    <form runat="server" id="form">
        <div class="row">

            <div class="col-xs-12 col-sm-12">

                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h4 class="panel-title"><span class="TitulosPanel">Exportação de Arquivos</span></h4>
                    </div>
                    <div class="panel-body">

                        <div runat="server" id="divComponentes">
                            <asp:TextBox ID="txtSeparador" runat="server" Width="68px" Visible="False"></asp:TextBox>
                            <asp:CheckBox ID="chkTodos" runat="server" Style="font-size: medium" Text="TODOS" Visible="False" />
                            <asp:CheckBox ID="chkUnico" runat="server" Style="font-size: medium" Text="Livro + Leitura" Visible="False" />
                            <asp:TextBox ID="txtDirExp" runat="server" Width="450px" Height="23px" Visible="False"></asp:TextBox>
                        </div>

                        <div class="row">
                            <div class="col-lg-6 col-12">
                                <h4 class="panel-title" style="padding-bottom: 5px;"><span class="Titulos">Arquivos Disponíveis</span></h4>
                                <asp:ListBox ID="lstDirExp" runat="server" Height="200px" Width="100%" CssClass="form-control"></asp:ListBox>
                            </div>
                            <div class="col-lg-6 col-12">
                                <h4 class="panel-title" style="padding-bottom: 5px;"><span class="Titulos">Status</span></h4>
                                <asp:TextBox ID="txtStatus" runat="server" TextMode="MultiLine" Height="200px"
                                    Width="100%" Enabled="False"></asp:TextBox>
                            </div>

                        </div>
                        <div class="row" style="padding-top: 10px">
                            <div class="col-lg-6 col-12">

                                <asp:Button ID="btnReceber" runat="server" Text="Exportar" Width="100%" CssClass="btn btn-primary"
                                    Style="font-weight: 700; font-size: medium" OnClick="btnReceber_Click" OnClientClick="document.getElementById('progress').className = 'modal centralizarImagem visivel';"/>
                            </div>
                            <div class="col-lg-6 col-12">
                                <asp:Button ID="btDownload" runat="server" Text="Download" Width="100%" CssClass="btn btn-primary"
                                    Style="font-weight: 700; font-size: medium" OnClick="btDownload_Click" Enabled="false"/>

                            </div>
                        </div>
                        <br />
                        <asp:Panel ID="PanelMsg" runat="server" Visible="false">
                            <div class="alert alert-info">
                                <strong>Atenção! </strong>
                                <asp:Label ID="lblMsg" runat="server" Text=""></asp:Label>
                            </div>
                        </asp:Panel>

                    </div>
                </div>


            </div>



        </div>


    </form>

</asp:Content>
