﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="ViewConsAnalise.aspx.cs" Inherits="ViewConsAnalise" enableEventValidation ="false" %> 


<%@ MasterType VirtualPath="~/Site.master" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">


    <link href="Content/lightbox.css" rel="stylesheet" />
    <link href="Content/prettyPhoto.css" rel="stylesheet" />
    <link href="Content/StyleSheet.css" rel="stylesheet" />




    <style type="text/css">

        .GridPager a, .GridPager span
        {
        display: block;
        height: 20px;
        width: 30px;
        font-weight: bold;
        text-align: center;
        text-decoration: none;
        }
        .GridPager a
        {
        background-color: #f5f5f5;
        color: #969696;
        border: 1px solid #969696;
        }
        .GridPager span
        {
        background-color: dodgerblue;
        color: #000;
        border: 1px solid orangered;
        }
        @media print {


            .row {
                margin-right: -15px;
                margin-left: -15px;
                padding-top: 0px;
                font-size: x-small;
            }

            col-1, .col-2, .col-3, .col-4, .col-5, .col-6, .col-7, .col-8, .col-9, .col-10, .col-11, .col-12, .col-sm-1, .col-sm-2, .col-sm-3, .col-sm-4, .col-sm-5, .col-sm-6, .col-sm-7, .col-sm-8, .col-sm-9, .col-sm-10, .col-sm-11, .col-sm-12, .col-lg-1, .col-lg-2, .col-lg-3, .col-lg-4, .col-lg-5, .col-lg-6, .col-lg-7, .col-lg-8, .col-lg-9, .col-lg-10, .col-lg-11, .col-lg-12 {
                position: relative;
                min-height: 1px;
                padding-right: 15px;
                padding-left: 15px;
                top: 0px;
                left: 0px;
            }

            .col-1, .col-2, .col-3, .col-4, .col-5, .col-6, .col-7, .col-8, .col-9, .col-10, .col-11, .col-12 {
                float: left;
            }

            .col-1 {
                width: 8.333333333333332%;
            }

            .col-2 {
                width: 16.666666666666664%;
            }

            .col-3 {
                width: 25%;
            }

            .col-4 {
                width: 33.33333333333333%;
            }

            .col-5 {
                width: 41.66666666666667%;
            }

            .col-6 {
                width: 50%;
            }

            .col-7 {
                width: 58.333333333333336%;
            }

            .col-8 {
                width: 66.66666666666666%;
            }

            .col-9 {
                width: 75%;
            }

            .col-10 {
                width: 83.33333333333334%;
            }

            .col-11 {
                width: 91.66666666666666%;
            }

            .col-12 {
                width: 100%;
            }

            td {
                padding: 0px;
            }
        }




        .divbutton {
            /*height: 100px;*/
        }

        label {
            margin-bottom: 0px;
            font-weight: inherit;
        }

        labelPesquisa {
            margin-bottom: 0px;
            font-weight: inherit;
        }

        th {
            text-align: center;
        }

        td {
            padding: 0px;
        }

        .FixedHeader {
            position: absolute;
            font-weight: bold;
            vertical-align: text-bottom;
            width: 100%;
        }

        .painelInfo {
            min-height: 120px;
        }


        a.fill-div {
            text-align: center;
            display: block;
            height: 100%;
            width: 100%;
            text-decoration: none;
        }

        .autocomplete_list {
            visibility: visible;
            margin: 0px !important;
            padding: 1px;
            background-color: GrayText;
            color: Black;
            border: buttonshadow;
            border-width: 1px;
            border-style: solid;
            cursor: default;
            text-align: left;
            list-style-type: none;
            font-weight: normal;
            font-family: Arial;
            font-size: 16px;
        }

        .autocomplete_highlighted_listitem {
            background-color: ButtonFace;
            color: Blue;
            padding: 3px;
        }

        .autocomplete_listitem {
            background-color: Window;
            color: Black;
            font-family: Arial;
            font-size: 12px;
            padding: 3px;
        }

        .carousel {
            width: 600px;
            padding-left: 21px;
        }

        /* Indicators list style */
        .article-slide .carousel-indicators {
            bottom: 0;
            left: 0;
            margin-left: 59px;
            width: 100%;
        }
            /* Indicators list style */
            .article-slide .carousel-indicators li {
                border: medium none;
                border-radius: 0;
                float: left;
                height: 54px;
                margin-bottom: 5px;
                margin-left: 0;
                margin-right: 5px !important;
                margin-top: 0;
                width: 100px;
            }
            /* Indicators images style */
            .article-slide .carousel-indicators img {
                border: 2px solid #FFFFFF;
                float: left;
                height: 54px;
                left: 0;
                width: 100px;
            }
            /* Indicators active image style */
            .article-slide .carousel-indicators .active img {
                border: 2px solid #428BCA;
                opacity: 0.7;
            }

        .ligacao {
        }

        .esconder {
            visibility: hidden;
        }
    </style>

    <script type="text/javascript">

        function printDiv(divName) {
            var printContents = document.getElementById(divName).innerHTML;
            var originalContents = document.body.innerHTML;

            document.body.innerHTML = printContents;

            window.print();

            document.body.innerHTML = originalContents;
        }

        

        function imprime() {
            //you can put your contentID which is you want to print.
            var contents = $("#conteudo").html();
            var frame1 = $('<iframe />');
            frame1[0].name = "frame1";
            frame1.css({ "position": "absolute", "top": "-1000000px" });
            $("body").append(frame1);
            var frameDoc = frame1[0].contentWindow ? frame1[0].contentWindow : frame1[0].contentDocument.document ? frame1[0].contentDocument.document : frame1[0].contentDocument;
            frameDoc.document.open();
            //Create a new HTML document.
            frameDoc.document.write('<html><head><title>DIV Contents</title>');
            frameDoc.document.write('</head><body>');
            //Append the external CSS file.
            frameDoc.document.write('<link href="style.css" rel="stylesheet" type="text/css" />');
            //Append the DIV contents.
            frameDoc.document.write(contents);
            frameDoc.document.write('</body></html>');
            frameDoc.document.close();
            setTimeout(function () {
                window.frames["frame1"].focus();
                window.frames["frame1"].print();
                frame1.remove();
            }, 500);
        }

        function printform() {
            var printContent = document.getElementById('<%= PanelDetalhado.ClientID %>');

            var windowUrl = 'about:blank';
            var uniqueName = new Date();
            var windowName = 'Print' + uniqueName.getTime();
            var printWindow = window.open(windowUrl, windowName, 'left=50000,top=50000,width=0,height=0');

            printWindow.document.write(printContent.innerHTML);
            printWindow.document.close();
            printWindow.focus();
            printWindow.print();
            printWindow.close();
        }
    </script>

    <style type="text/css">
        .left {
            padding-left: 10px;
            padding-top: 10px;
        }

        .EstiloGaleria {
            position: fixed;
            left: 29%;
            top: 58%;
            /*width: 300px;*/
            min-height: 250px;
            margin-left: -150px;
            margin-top: -125px;
            background-color: rgb(102, 102, 102);
            color: rgb(255, 255, 255);
            background-color: #666;
            text-align: center;
            z-index: 1000;
            overflow: scroll;
        }

        #gallery {
            background-color: #4f5b69;
            padding: 10px;
            width: auto;
            overflow: scroll;
        }

            #gallery ul {
                list-style: none;
            }

                #gallery ul li {
                    display: inline;
                }

                #gallery ul img {
                    border: 5px solid #444444;
                    border-width: 5px 5px 20px;
                }

                #gallery ul a:hover img {
                    border: 5px solid #fff;
                    border-width: 5px 5px 20px;
                    color: #fff;
                }

                #gallery ul a:hover {
                    color: #fff;
                }
    </style>



    <script type="text/javascript">




        jQuery(window).click(function () {
            //alert('click');
            if (document.getElementById("ContentPlaceHolder1_lblFileUpload").innerHTML.toString() != "teste") {
                //document.getElementById("ContentPlaceHolder1_gallery").style = "visibility: hidden;position: fixed;z-index: 9999;overflow:scroll";
                var self = jQuery(this).closest("tr");
                var col1_value = self.find(".ligacao").text();



            }


            //Carrega painel de informações da pesquisa
            if (document.getElementById("collapseOne").attributes.getNamedItem('aria-expanded').value == "false") {

                document.getElementById("InfoPesquisa").className = "row";
            } else {

                document.getElementById("InfoPesquisa").className = "row esconder";
            }


            document.getElementById("ContentPlaceHolder1_lblFileUpload").innerHTML = "";
            // alert(jQuery(this).text());
        });
        window.onload = function () {

            //esconde galeria
            //document.getElementById("ContentPlaceHolder1_gallery").style = "visibility: hidden;position: fixed;z-index: 9999;overflow:scroll";

            document.getElementById("ContentPlaceHolder1_rbEndereco").style = "margin-bottom:0px;";
            document.getElementById("ContentPlaceHolder1_rbEndereco").checked = true;
            document.getElementById("ContentPlaceHolder1_rbEnderecoEntrega").style = "margin-bottom:0px;";


            //document.getElementById("ContentPlaceHolderLogin_txtAno").type = "number";
        }


        function UploadFile(fileUpload) {
            document.getElementById("ContentPlaceHolder1_lblFileUpload").innerHTML = "teste";
        }


    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

  
    <form class="form-horizontal" role="form" runat="server">


        <asp:Label ID="lblFileUpload" runat="server" Text=""></asp:Label>
        <asp:Label ID="lblLigacaoSel" runat="server" Text="" Visible="false"></asp:Label>


        <div id="conteudo">
            <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>

            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                <div class="panel panel-primary">
                    <div class="panel-heading" role="tab" id="headingOne">

                        <div id="my-div">
                            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true"
                                aria-controls="collapseOne" class="fill-div TitulosPanel">Análise Crítica</a>
                        </div>

                    </div>
                    <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                        <div class="panel-body">

                            <div class="row">
                                <div class="col-12">
                                    <asp:Panel ID="PanelMsg" runat="server" Visible="false">
                                        <div class="alert alert-info">
                                            <strong>Atenção! </strong>
                                            <asp:Label ID="lblMsg" runat="server" Text="teste" Visible="false"></asp:Label>
                                        </div>
                                    </asp:Panel>
                                </div>
                            </div>


                            <div class="row">


                                <div class="col-sm-6 col-lg-6">
                                    <div class="col-sm-4 col-lg-6" style="padding-left: 0px">
                                        <div class="input-group">
                                            <span class="input-group-addon">Mês</span>
                                            <asp:DropDownList ID="cbxMes" runat="server" CssClass="form-control">
                                                <asp:ListItem>Selecione o mês</asp:ListItem>
                                                <asp:ListItem>Janeiro</asp:ListItem>
                                                <asp:ListItem>Fevereiro</asp:ListItem>
                                                <asp:ListItem>Março</asp:ListItem>
                                                <asp:ListItem>Abril</asp:ListItem>
                                                <asp:ListItem>Maio</asp:ListItem>
                                                <asp:ListItem>Junho</asp:ListItem>
                                                <asp:ListItem>Julho</asp:ListItem>
                                                <asp:ListItem>Agosto</asp:ListItem>
                                                <asp:ListItem>Setembro</asp:ListItem>
                                                <asp:ListItem>Outubro</asp:ListItem>
                                                <asp:ListItem>Novembro</asp:ListItem>
                                                <asp:ListItem>Dezembro</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-sm-4 col-lg-3" style="padding-right: 0px; padding-left: 0px;">
                                        <div class="input-group">
                                            <span class="input-group-addon" id="">Ano</span>
                                            <asp:TextBox ID="txtAno" runat="server" CssClass="form-control bfh-number"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col-sm-4 col-lg-3" style="padding-right: 0px">
                                        <div class="input-group">
                                            <span class="input-group-addon">Rota</span>
                                            <asp:TextBox ID="txtLivro" runat="server" CssClass="form-control bfh-number"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>




                                <div class="col-sm-6 col-lg-6" style="padding-right: 0px; padding-left: 0px">
                                    <div class="col-sm-4 col-lg-5" style="padding-right: 0px; padding-left: 0px">
                                        <div class="input-group">
                                            <span class="input-group-addon">Ligação</span>
                                            <asp:TextBox ID="txtIdentificacao" runat="server" CssClass="form-control bfh-number"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col-sm-4 col-lg-4">
                                        <div class="input-group">
                                            <span class="input-group-addon">HD</span>
                                            <asp:TextBox ID="txtMedidor" runat="server" CssClass="form-control bfh-number"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col-sm-4 col-lg-3" style="padding-right: 15px; padding-left: 0px">
                                        <div class="input-group" id="divGrupo" runat="server">
                                            <span class="input-group-addon">Grupo</span>
                                            <asp:TextBox ID="txtGrupo" runat="server" CssClass="form-control bfh-number"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>







                            </div>

                            <div class="row" style="padding-top: 10px">
                                <div class="col-sm-6 col-lg-3">
                                    <div class="input-group">
                                        <span class="input-group-addon">Leiturista</span>
                                        <asp:DropDownList ID="ddlLtr" runat="server" CssClass="form-control">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-lg-3">
                                    <div class="input-group">
                                        <span class="input-group-addon">Sit. Leitura</span>
                                        <asp:DropDownList ID="ddlSituacaoLeitura" runat="server" CssClass="form-control">
                                            <asp:ListItem>Com Leitura</asp:ListItem>
                                            <asp:ListItem>Sem Leitura</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-lg-3">
                                    <div class="input-group">
                                        <span class="input-group-addon">Relatório</span>
                                        <asp:DropDownList ID="ddlTipoRelatorio" runat="server" CssClass="form-control">
                                            <asp:ListItem>Normal</asp:ListItem>

                                        </asp:DropDownList>
                                    </div>

                                </div>

                                <div class="col-sm-6 col-lg-3" id="lancamentos" runat="server">
                                    <div class="input-group">
                                        <span class="input-group-addon">Lançamentos</span>
                                        <asp:DropDownList ID="ddlLancamentos" runat="server" CssClass="form-control">
                                            <asp:ListItem>Todos</asp:ListItem>
                                            <asp:ListItem>Fechado por débito</asp:ListItem>
                                            <asp:ListItem>Fechado a pedido</asp:ListItem>

                                        </asp:DropDownList>
                                    </div>

                                </div>
                            </div>




                            <div class="row" style="padding-top: 10px">

                                <div class="col-sm-6 col-lg-6">
                                    <div class="col-sm-6 col-lg-9" style="padding-left: 0px">
                                        <div class="input-group">
                                            <span class="input-group-addon" style="padding: 0px 2px; text-align: left;">
                                                <asp:RadioButton ID="rbEndereco" runat="server" Text="Endereço" Checked="True" GroupName="2" AutoPostBack="True" OnCheckedChanged="rbEndereco_CheckedChanged" CssClass="Endereco" />
                                                <br />
                                                <asp:RadioButton ID="rbEnderecoEntrega" runat="server" Text="End. de Entrega" GroupName="2" AutoPostBack="True" OnCheckedChanged="rbEnderecoEntrega_CheckedChanged" CssClass="Endereco" />
                                            </span>

                                            <asp:TextBox ID="txtEndereco" runat="server"
                                                AutoPostBack="true" CssClass="form-control" Height="46px"></asp:TextBox>
                                            <asp:AutoCompleteExtender ID="txtEndereco_AutoCompleteExtender"
                                                runat="server" DelimiterCharacters="" Enabled="True" ServicePath="" TargetControlID="txtEndereco"
                                                CompletionSetCount="10"
                                                CompletionInterval="10"
                                                MinimumPrefixLength="1"
                                                EnableCaching="true"
                                                CompletionListCssClass="autocomplete_list"
                                                CompletionListItemCssClass="autocomplete_listitem"
                                                CompletionListHighlightedItemCssClass="autocomplete_highlighted_listitem" OnLoad="txtEndereco_AutoCompleteExtender_Load">
                                            </asp:AutoCompleteExtender>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-lg-3" style="padding-right: 0px">
                                        <div class="input-group">
                                            <span class="input-group-addon">Nº</span>
                                            <asp:TextBox ID="txtNumero" runat="server" CssClass="form-control bfh-number"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-6 col-lg-2">
                                    <div class="input-group">
                                        <span style="padding: 4px 12px;" class="input-group-addon">
                                            <asp:CheckBox ID="chkCritica" runat="server" Text="%Crítica" ForeColor="ForestGreen" /></span>
                                        <asp:TextBox ID="txtPercent" runat="server" CssClass="form-control">30</asp:TextBox>

                                    </div>

                                </div>
                                <div class="col-sm-6 col-lg-4">
                                    <div class="input-group">
                                        <span class="input-group-addon">Ocorrência</span>
                                        <asp:DropDownList ID="cbxMsg" runat="server" CssClass="form-control">
                                        </asp:DropDownList>
                                    </div>

                                </div>
                            </div>
                            <!-- /.row this actually does not appear to be needed with the form-horizontal -->



                            <div class="row" style="padding-top: 10px">

                                <div class="col-sm-6 col-lg-5">
                                    <div class="well well-small painelInfo " style="background-color: white">

                                        <div style="background-color: #eee; text-align: center; padding-right: 0px;">Ordenar por:</div>
                                        <div class="col-3">
                                            <div class="radio">
                                                <asp:RadioButton ID="rbSequencia" runat="server" GroupName="1" Text="Sequência" Checked="True" />
                                            </div>
                                            <div class="radio">
                                                <asp:RadioButton ID="rbLigacao" runat="server" GroupName="1" Text="Ligação" />
                                            </div>


                                        </div>
                                        <div class="col-3">
                                            <div class="radio">
                                                <asp:RadioButton ID="rbRota" runat="server" GroupName="1" Text="Rota" />
                                            </div>
                                            <div class="radio">
                                                <asp:RadioButton ID="rbNomeContribuinte" runat="server" GroupName="1" Text="Contribuinte" />
                                            </div>

                                        </div>

                                        <div class="col-3">
                                            <div class="radio">
                                                <asp:RadioButton ID="rbEnderecoOrdem" runat="server" GroupName="1" Text="Endereço" />
                                            </div>
                                            <div class="radio">
                                                <asp:RadioButton ID="rbData" runat="server" GroupName="1" Text="Data" />
                                            </div>

                                        </div>
                                        <div class="col-3">
                                            <div class="input-group" style="margin-top: 10px">
                                                <asp:Button ID="btPesquisar" runat="server" Text="Pesquisar" CssClass="btn btn-primary" OnClick="btPesquisar_Click" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-lg-3">
                                    <div class="well well-small painelInfo" style="padding-right: 10px; background-color: white;">
                                        <div style="background-color: #eee; text-align: center">Legenda</div>
                                        <asp:Label ID="lbl50" runat="server" ForeColor="Blue" Text="50% fora da média"></asp:Label>
                                        <br />
                                        <asp:Label ID="lbl75" runat="server" ForeColor="BlueViolet" Text="75% fora da média"></asp:Label>
                                        <br />
                                        <asp:Label ID="lbl100" runat="server" ForeColor="Red" Text="100% fora da média"></asp:Label>
                                        <br />
                                        <asp:Label ID="lblPorc" runat="server" Font-Bold="True" ForeColor="ForestGreen" Text="% crítica fora da média"></asp:Label>

                                    </div>
                                </div>
                                <div class="col-sm-6 col-lg-4">
                                    <div class="well well-small painelInfo" style="padding-right: 10px; background-color: white">
                                        <div style="background-color: #eee; text-align: center">Informações da rota</div>

                                        <div style="text-align: left">
                                            <asp:Label ID="lblDataEnvio" runat="server" ForeColor="Blue" Style="font-weight: 700; text-align: left"></asp:Label>
                                        </div>

                                        <div style="text-align: left">
                                            <asp:Label ID="lblTotalLivros" runat="server" ForeColor="Blue" Style="font-weight: 700; text-align: left"></asp:Label>
                                        </div>

                                        <div style="text-align: left">
                                            <asp:Label ID="lblTotalLivrosLidas" runat="server" ForeColor="Blue" Style="font-weight: 700; text-align: left"></asp:Label>
                                        </div>

                                        <div style="text-align: left">
                                            <asp:Label ID="lblTotalLivrosNaoLidas" runat="server" ForeColor="Blue" Style="font-weight: 700; text-align: left"></asp:Label>
                                        </div>


                                    </div>
                                </div>

                            </div>



                        </div>
                    </div>
                </div>
            </div>


            <asp:Panel ID="PanelQtdRegistros" runat="server" Visible="false">
                <div class="alert alert-info text-center" style="margin-bottom: 0px;">

                    <div class="row">

                        <div class="col-3">
                        </div>
                        <div class="col-6">
                            <strong style="color: #4f5b69">Registros encontrados: </strong>
                            <asp:Label ID="lblTOTAL" runat="server" ForeColor="#4f5b69" Style="font-weight: 700"></asp:Label>
                        </div>
                        <div class="col-3 text-right">
                            <div class="col-6">
                            </div>

                            <div class="col-6">

                                <div class="col-6 " style="padding-right: 10px">
                                    <asp:ImageButton ID="imgExcel" runat="server" ImageUrl="~/imagens/excel.png" OnClick="imgExcel_Click" />
                                </div>
                                <div class="col-6 text-left" style="padding-right: 10px">
                                    <input type="image" onclick="printDiv('conteudo')" src="imagens/print.png" />
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="row esconder" id="InfoPesquisa">
                        <div class="col-lg-2 col-sm-4 labelPesquisa" id="leiturista" runat="server" visible="false">
                            Leiturista:
                            <asp:Label ID="lblLeiturista" runat="server" Text=""></asp:Label>
                        </div>
                        <div class="col-lg-3 col-sm-4 labelPesquisa" id="SitLeitura" runat="server" visible="false">
                            Situação leitura:
                            <asp:Label ID="lblSitLeitura" runat="server" Text=""></asp:Label>
                        </div>
                        <div class="col-lg-1 col-sm-4 labelPesquisa" id="Rota" runat="server" visible="false">
                            Rota:
                            <asp:Label ID="lblRota" runat="server" Text=""></asp:Label>
                        </div>
                        <div class="col-lg-1 col-sm-4 labelPesquisa" id="Grupo" runat="server" visible="false">
                            Grupo:
                            <asp:Label ID="lblGrupo" runat="server" Text=""></asp:Label>
                        </div>
                        <div class="col-lg-3 col-sm-4 labelPesquisa" id="Ocorrencia" runat="server" visible="false">
                            Ocorrência:
                            <asp:Label ID="lblOcorrencia" runat="server" Text=""></asp:Label>
                        </div>
                        <div class="col-lg-2 col-sm-4 labelPesquisa" id="referencia" runat="server" visible="false">
                            Referência:
                            <asp:Label ID="lblReferencia" runat="server" Text=""></asp:Label>
                        </div>
                    </div>

                </div>

            </asp:Panel>
            <div class="row">
                <div class="col-12">
                    <div class="row" style="padding-top: 15px">
                        <div class="col-12">
                            <asp:Panel ID="Panel1" runat="server">
                                <div id="Panelgrid" runat="server" class="">
                                    <div id="scrollDiv" style="overflow: auto; height: 100%; width: 100%">
                                        <div id="teste">
                                            <asp:GridView ID="gridNormal" runat="server" CellPadding="2" ForeColor="#333333" EmptyDataText="Nenhum registro encontrado" BackColor="White"
                                                BorderColor="#999999" BorderStyle="None" BorderWidth="1px" Width="100%" OnRowDataBound="gridNormal_RowDataBound" AllowPaging="True" PageSize="100"
                                                HeaderStyle-CssClass="tblTitle" OnRowCommand="gridNormal_RowCommand" OnPageIndexChanging="gridNormal_PageIndexChanging" CssClass="grid" GridLines="Horizontal">

                                                <AlternatingRowStyle BackColor="Gainsboro" />
                                                <Columns>
                                                    <asp:ButtonField CommandName="Foto" ButtonType="Image" ImageUrl="imagens/foto.png" ControlStyle-CssClass="divbutton" />
                                                </Columns>

                                                <EditRowStyle HorizontalAlign="Center" />

                                                <FooterStyle BackColor="#CCCCCC" ForeColor="Black" />
                                                <HeaderStyle BackColor="#4f5b69" Font-Bold="True" ForeColor="White" HorizontalAlign="Center" />
                                                <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Left" CssClass="GridPager" />
                                                <RowStyle BackColor="#EEEEEE" ForeColor="Black" HorizontalAlign="Center" />
                                                <SelectedRowStyle BackColor="#e1ffff" Font-Bold="True" ForeColor="White" />
                                                <SortedAscendingCellStyle BackColor="#F1F1F1"></SortedAscendingCellStyle>
                                                <SortedAscendingHeaderStyle BackColor="#0000A9"></SortedAscendingHeaderStyle>
                                                <SortedDescendingCellStyle BackColor="#CAC9C9"></SortedDescendingCellStyle>
                                                <SortedDescendingHeaderStyle BackColor="#000065"></SortedDescendingHeaderStyle>
                                            </asp:GridView>
                                        </div>

                                    </div>



                                </div>
                            </asp:Panel>


                            <div id="Panelrol" runat="server">
                                <table class="table table-bordered text-center" border="1">
                                    <thead>
                                        <tr style="background-color: #4f5b69; color: white">
                                            <td>Seq.</td>
                                            <td>Cod. DAE</td>
                                            <td>Nome/Endereço</td>
                                            <td>Bairro</td>
                                            <td>Categoria/HD</td>
                                            <td>Leitura Atual  </td>
                                            <td>Ocorrência</td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <asp:Repeater runat="server" ID="RepeaterRolLeitura">
                                            <ItemTemplate>
                                                <tr>
                                                    <td><%# DataBinder.Eval(Container.DataItem, "LEI_SEQUENCIA") %></td>
                                                    <td><%# DataBinder.Eval(Container.DataItem, "LEI_LIGACAO") %></td>
                                                    <td>
                                                        <div><%# DataBinder.Eval(Container.DataItem, "LEI_CONSUMIDOR") %></div>
                                                        <div><%# DataBinder.Eval(Container.DataItem, "ENDERECO") %></div>
                                                    </td>
                                                    </div>
                                                <td><%# DataBinder.Eval(Container.DataItem, "LEI_BAIRRO") %></td>
                                                    <td>
                                                        <div><%# DataBinder.Eval(Container.DataItem, "nomecategoria") %></div>
                                                        <div><%# DataBinder.Eval(Container.DataItem, "LEI_NUMMED") %></div>
                                                    </td>
                                                    <td><%# DataBinder.Eval(Container.DataItem, "LEI_LEITURA") %></td>
                                                    <td><%# DataBinder.Eval(Container.DataItem, "OCORRENCIA") %></td>
                                                </tr>


                                            </ItemTemplate>

                                        </asp:Repeater>
                                    </tbody>
                                </table>
                            </div>

                            <div id="PanelEntrega" runat="server">
                                <table class="table table-bordered text-center" border="1">
                                    <thead>
                                        <tr style="background-color: #4f5b69; color: white">
                                            <td>Ligação</td>
                                            <td>Grupo</td>
                                            <td>Nome/Endereço</td>
                                            <td>Forma de Entrega</td>
                                            <td>Nome do Entregador</td>
                                            <td>Data Entrega </td>                                            
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <asp:Repeater runat="server" ID="RepeaterFormaEntrega">
                                            <ItemTemplate>
                                                <tr>
                                                    <td><%# DataBinder.Eval(Container.DataItem, "LEI_LIGACAO") %></td>
                                                    <td><%# DataBinder.Eval(Container.DataItem, "GRUPO_ID") %></td>
                                                    <td>
                                                        <div><%# DataBinder.Eval(Container.DataItem, "LEI_CONSUMIDOR") %></div>
                                                        <div><%# DataBinder.Eval(Container.DataItem, "ENDERECO") %></div>
                                                    </td>
                                                    </div>
                                                <td><%# DataBinder.Eval(Container.DataItem, "LEI_ENTREGAFATURA") %></td>
                                                    <td>                                       
                                                        <div><%# DataBinder.Eval(Container.DataItem, "LTR_NOME") %></div>
                                                    </td>
                                                    <td><%# DataBinder.Eval(Container.DataItem, "LEI_DATAENTREGA") %></td>                                                    
                                                </tr>
                                            </ItemTemplate>

                                        </asp:Repeater>
                                    </tbody>
                                </table>
                            </div>

                            <asp:Panel ID="PanelDetalhado" runat="server" Visible="false">
                                <asp:Repeater runat="server" ID="RepeaterDetalhado">
                                    <ItemTemplate>
                                        <div class="well well-small" style="background-color: rgb(238, 238, 238); text-align: left; padding: 2px;">

                                            <div class="row">

                                                <div class="col-2">
                                                    <strong>Código DAE:</strong><%# DataBinder.Eval(Container.DataItem, "LEI_LIGACAO") %>
                                                </div>

                                                <div class="col-2">
                                                    <strong>Rota Leitura:</strong><%# DataBinder.Eval(Container.DataItem, "GRUPO_ID") %>-<%# DataBinder.Eval(Container.DataItem, "LEI_SEQUENCIA") %>
                                                </div>

                                                <div class="col-2">
                                                    <strong>Situação Leitura:</strong><%# DataBinder.Eval(Container.DataItem, "SITUACAO") %>
                                                </div>

                                                <div class="col-3">
                                                    <strong>Leiturista:</strong><%# DataBinder.Eval(Container.DataItem, "LTR_NOME") %>
                                                </div>

                                                <div class="col-3">
                                                    <strong>Hidrômetro:</strong><%# DataBinder.Eval(Container.DataItem, "HD") %>
                                                </div>



                                            </div>
                                            <div class="row" style="padding-top: 2px">
                                                <div class="col-4">
                                                    <strong>Ocorrência:</strong><%# DataBinder.Eval(Container.DataItem, "MSG_ID") %>-<%# DataBinder.Eval(Container.DataItem, "OCORRENCIA") %>
                                                </div>

                                                <div class="col-3">
                                                    <div>
                                                        <strong>Nome:</strong><%# DataBinder.Eval(Container.DataItem, "LEI_CONSUMIDOR") %>
                                                    </div>

                                                </div>

                                                <div class="col-3">
                                                    <strong>Forma de Entrega:</strong><%# DataBinder.Eval(Container.DataItem, "LEI_FORMAENTREGA") %>
                                                </div>


                                                <div class="col-2">
                                                    <div>
                                                        <strong>Categoria:</strong> <%# DataBinder.Eval(Container.DataItem, "CATEGORIA") %>
                                                    </div>

                                                </div>




                                            </div>
                                            <div class="row" style="padding-top: 2px">

                                                <div class="col-3">
                                                    <div>
                                                        <strong>Alteração Cadastral:</strong> <%# DataBinder.Eval(Container.DataItem, "NOVACATEGORIA") %>
                                                    </div>

                                                </div>

                                                <div class="col-2">
                                                    <div>
                                                        <strong>Economias:</strong><%# DataBinder.Eval(Container.DataItem, "LEI_ECONOMIA") %>
                                                    </div>

                                                </div>

                                                <div class="col-2">
                                                    <div>
                                                        <strong>TL:</strong><%# DataBinder.Eval(Container.DataItem, "LEI_LANCAMENTO") %>
                                                    </div>

                                                </div>
                                                <div class="col-2">
                                                    <div>
                                                        <strong>Conta retida:</strong><%# DataBinder.Eval(Container.DataItem, "LEI_RETERCONTA") %>
                                                    </div>

                                                </div>





                                            </div>
                                            <div class="row" style="padding-top: 2px">

                                                <div class="col-3">
                                                    <div>
                                                        <strong>Endereço:</strong><%# DataBinder.Eval(Container.DataItem, "ENDERECO") %>
                                                    </div>

                                                </div>
                                                <div class="col-2">
                                                    <div>
                                                        <strong>Leitura Anterior:</strong><%# DataBinder.Eval(Container.DataItem, "LEI_ANTERIOR") %>
                                                    </div>

                                                </div>


                                                <div class="col-2">
                                                    <div>
                                                        <strong>Leitura Atual:</strong><%# DataBinder.Eval(Container.DataItem, "LEI_LEITURA") %>
                                                    </div>

                                                </div>


                                                <div class="col-3">
                                                    <div>
                                                        <strong>Consumo Medido:</strong><%# DataBinder.Eval(Container.DataItem, "LEI_CONSUMO") %>
                                                    </div>

                                                </div>


                                                <div class="col-2">
                                                    <div>
                                                        <strong>Média:</strong><%# DataBinder.Eval(Container.DataItem, "MEDIA") %>
                                                    </div>

                                                </div>
                                            </div>

                                        </div>



                                    </ItemTemplate>

                                </asp:Repeater>
                            </asp:Panel>

                        </div>
                    </div>
                </div>
            </div>


        </div>

        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header text-center">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <h4 class="modal-title" id="myModalLabel">Galeria de Fotos</h4>
                    </div>
                    <div class="modal-body">
                        <div class="divbutton" id="divGaleria">
                            <div class="imageRow  " id="gallery" runat="server">
                                <div class="row">
                                    <div class="col-5">
                                        <asp:FileUpload ID="EnviarArq" runat="server" CssClass="FileUpload btn btn-primary" />
                                    </div>
                                    <div class="col-2">
                                        <div class="input-group">

                                            <div class="input-group-btn">
                                                <!-- Buttons -->

                                                <asp:Button ID="btnEnviarArq" runat="server" Text="Upload" CssClass="form-control btn btn-primary" OnClick="btnEnviarArq_Click" OnClientClick="openfileDialog();" />

                                            </div>


                                        </div>
                                    </div>

                                </div>


                                <div class="set">
                                    <asp:DataList ID="galeriaDataList" RepeatColumns="6" runat="server" ItemStyle-CssClass="left" HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <a href="<%# Eval("Name","Fotos/"+Request.Cookies["CliID"].Value.PadLeft(2,'0')+"/{0}")%>" rel="lightbox[plants]" title="Clique do lado direito para ir para a próxima imagem.">
                                                <img src='<%# Eval("Name","Fotos/"+Request.Cookies["CliID"].Value.PadLeft(2,'0')+"/{0}")%>' width="200" height="200" alt='<%# Eval("Name") %>' />
                                            </a>
                                            <br />

                                            <div style="text-align: center">
                                                <asp:Label ID="lblReferenciaFoto" runat="server" Text='<%#(Eval("Name").ToString().Length < 39 && Eval("Name").ToString().Length  < 18 ) ? 
                        Eval("Name").ToString().Substring(0, 2) + "/" + Eval("Name").ToString().Substring(2, 4):
                        (Eval("Name").ToString().Length ==26 ) ? 
                        Eval("Name").ToString().Substring(8, 2) + "/" + Eval("Name").ToString().Substring(10, 4):
                        (Eval("Name").ToString().Substring(4, 2) == "05" && Eval("Name").ToString().Length <=43) ? 
                        Eval("Name").ToString().Substring(28, 2) + "/" + Eval("Name").ToString().Substring(24, 4):
                        (Eval("Name").ToString().Length <=43 ) ? 
                        Eval("Name").ToString().Substring(6, 2) + "/" + Eval("Name").ToString().Substring(8, 4):
                        (Eval("Name").ToString().Length <=45 ) ? 
                        Eval("Name").ToString().Substring(33, 2)+"/"
                        +Eval("Name").ToString().Substring(31, 2) + "/" 
                        + Eval("Name").ToString().Substring(27, 4) + " "
                        + Eval("Name").ToString().Substring(35, 2)+":"
                        + Eval("Name").ToString().Substring(37, 2)+":"
                        + Eval("Name").ToString().Substring(39, 2):
                        Eval("Name").ToString().Substring(34, 2)+"/"
                        +Eval("Name").ToString().Substring(32, 2) + "/" 
                        + Eval("Name").ToString().Substring(28, 4) + " "
                        + Eval("Name").ToString().Substring(36, 2)+":"
                        + Eval("Name").ToString().Substring(38, 2)+":"
                        + Eval("Name").ToString().Substring(40, 2)%>'></asp:Label>
                                            </div>

                                        </ItemTemplate>
                                    </asp:DataList>

                                </div>

                            </div>


                        </div>


                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary btn-lg" data-dismiss="modal">
                            Concluído</button>

                    </div>
                </div>
            </div>
        </div>
    </form>



</asp:Content>
