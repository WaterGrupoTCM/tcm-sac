﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WaterSyncLite.Class;

public partial class ViewLiberarRotas : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        this.Master.LoadHeader(this.Master, Request.Path.Substring(Request.Path.LastIndexOf("/") + 1));

        SqlDataSourceLeituristas.ConnectionString = carregaConexao();
        SqlDataSourceLeituristas.SelectCommand = "select * from wtr_leituristas order by ltr_nome";
        SqlDataSourceLeituristas.DataBind();        

        if (!IsPostBack)
        {
            Session["chkHeader"] = null;
            txtAno.Text = DateTime.Now.Year.ToString();
            cbxMes.SelectedIndex = DateTime.Now.Month;

            Data db = new Data();
            #region LEITURISTAS
            DataTable dt = db.GetDt(carregaConexao(), "WTR_LEIT", "SELECT LTR_ID, LTR_NOME FROM "
                + " WTR_LEITURISTAS WHERE CLI_ID = " + Request.Cookies["CliID"].Value.ToString().Trim()
                + " AND EMP_ID = " + Request.Cookies["EmpID"].Value.ToString().Trim()
                + " ORDER BY LTR_NOME");

            DataRow dtr = dt.NewRow();
            dtr["LTR_NOME"] = "Selecione um leiturista...";
            dtr["LTR_ID"] = 0;
            dt.Rows.Add(dtr);
            if (dt != null)
            {
                ddlLtr.DataSource = dt;
                ddlLtr.DataTextField = "LTR_NOME";
                ddlLtr.DataValueField = "LTR_ID";
                ddlLtr.DataBind();
                ddlLtr.SelectedValue = "0";

            }
            #endregion



        }
    }
    protected String carregaConexao()
    {
        String StrCon = "";
        if (Request.Cookies["StrCon"] != null)
        {
            if (Request.Cookies["StrCon"].Value.ToString().Length > 0)
                StrCon = Request.Cookies["StrCon"].Value.ToString();
        }
        return StrCon.Replace('|', ';').Replace("usuario", xml.GetConexaoValue(Server.MapPath("~"), "LOCAL", "USER")).Replace("senha", xml.GetConexaoValue(Server.MapPath("~"), "LOCAL", "PASSWORD"));
    }

    protected void gridLiberarRotas_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        #region "Deixa invisível a coluna"
        switch (e.Row.RowType)
        {
            case DataControlRowType.Header:    //liberar rotas
                if (Session["chkHeader"] != null)
                    if (Session["chkHeader"].ToString() == "true")
                        ((CheckBox)e.Row.Cells[11].Controls[1]).Checked = true;
                    else
                        ((CheckBox)e.Row.Cells[11].Controls[1]).Checked = false;
                else ((CheckBox)e.Row.Cells[11].Controls[1]).Checked = false;

                break;

            case DataControlRowType.DataRow:    //liberar rotas
                if (((Label)e.Row.Cells[10].FindControl("lblLIV_FLAG")).Text == "S")
                {
                    ((CheckBox)e.Row.Cells[11].Controls[1]).Checked = false;
                    ((Image)e.Row.Cells[11].FindControl("imgFlag")).ImageUrl = "~/imagens/locked.png";
                    ((Label)e.Row.Cells[10].FindControl("lblLIV_FLAG")).Text = "Bloqueada";
                }

                else
                {
                    ((CheckBox)e.Row.Cells[11].Controls[1]).Checked = true;
                    ((Image)e.Row.Cells[11].FindControl("imgFlag")).ImageUrl = "~/imagens/unlocked.png";
                    ((Label)e.Row.Cells[10].FindControl("lblLIV_FLAG")).Text = "Liberada";
                }

                ((DropDownList)e.Row.Cells[9].Controls[1]).SelectedValue = ((Label)e.Row.Cells[5].FindControl("lblMatricula")).Text;


                break;

        }
        #endregion
    }
    protected void imgDesmarcar_CheckedChanged(object sender, EventArgs e)
    {
        CheckBox chkHeader = (CheckBox)sender;


        for (int i = 0; i < gridLiberarRotas.Rows.Count; i++)
        {
            if (gridLiberarRotas.Rows[i].RowType == DataControlRowType.DataRow)
            {
                string flag = "";
                Label mes = (Label)gridLiberarRotas.Rows[i].Cells[2].Controls[1];
                Label ano = (Label)gridLiberarRotas.Rows[i].Cells[3].Controls[1];
                Label rota = (Label)gridLiberarRotas.Rows[i].Cells[4].Controls[1];

                CheckBox checkbox = (CheckBox)gridLiberarRotas.Rows[i].Cells[11].Controls[1];
                if (chkHeader.Checked)
                {
                    checkbox.Checked = true;

                    flag = "N";

                    Session["chkHeader"] = "true";
                }
                else
                {
                    checkbox.Checked = false;
                    flag = "S";
                    Session["chkHeader"] = "false";
                }

                String sqlUpdateLivros = string.Format("update wtr_livros set liv_flag = '" + flag + "' where  loc_id = {0} and liv_mes = {1} and liv_ano = {2} and cli_id = {3} and emp_id = {4}",
                         rota.Text,
                         mes.Text,
                         ano.Text,
                         Request.Cookies["CliId"].Value.ToString(),
                         Request.Cookies["EmpId"].Value.ToString());
                Data db = new Data();

                bool erro = db.Exe(carregaConexao(), "wtr_livros", sqlUpdateLivros);
                if (!erro)
                    ShowMessage("WTR_LIVROS - Erro ao atualizar rota " + rota.Text);

                String sqlUpdateLeituras = string.Format("update wtr_leituras set lei_flag = '" + flag + "' where loc_id = {0} and liv_mes = {1} and liv_ano = {2} and cli_id = {3} and emp_id = {4}",
                   rota.Text,
                    mes.Text,
                    ano.Text,
                    Request.Cookies["CliId"].Value.ToString(),
                    Request.Cookies["EmpId"].Value.ToString());

                erro = db.Exe(carregaConexao(), "wtr_leituras", sqlUpdateLeituras);
                if (!erro)
                    ShowMessage("WTR_LEITURAS - Erro ao atualizar rota " + rota.Text);
            }

        }
        CarregaGrid();

    }

    protected void btPesquisar_Click(object sender, EventArgs e)
    {
        try
        {
            CarregaGrid();
            Session["chkHeader"] = null;
        }
        catch (Exception ex)
        {

            ClientScript.RegisterStartupScript(GetType(), "Message", "<SCRIPT LANGUAGE='javascript'>alert('" + ex.Message.ToString().Replace("'", "") + "');</script>");
        }
    }

    protected void CarregaGrid()
    {
        try
        {
            Data db = new Data();
            String where = "";
            if (ddlLtr.SelectedValue.ToString() != "0" || cbxMes.SelectedValue.ToString() != "0" || txtAno.Text.Length > 0)
            {
                where += " where ";
                if (txtAno.Text.Length > 0 && cbxMes.SelectedIndex.ToString() == "0")
                {
                    if (ddlLtr.SelectedValue.ToString() != "0")
                        where += " wtr_leituras.ltr_id =  " + ddlLtr.SelectedValue.ToString();
                }
                else
                {
                    if (ddlLtr.SelectedValue.ToString() != "0")
                        where += " wtr_leituras.ltr_id =  " + ddlLtr.SelectedValue.ToString() + " and";
                    if (cbxMes.SelectedIndex.ToString() != "0")
                        where += " wtr_leituras.liv_mes =  " + cbxMes.SelectedIndex.ToString() + " and";
                    if (txtAno.Text.Length > 0)
                        where += " wtr_leituras.liv_ano =  " + txtAno.Text;

                    where += " and wtr_leituras.cli_id = " + Request.Cookies["CliId"].Value.ToString();
                    where += " and wtr_leituras.emp_id = " + Request.Cookies["EmpId"].Value.ToString();
                }



            }

            string sql = " select wtr_leituras.ltr_id,ltr_nome ,wtr_leituras.Liv_mes ,wtr_leituras.Liv_ano ,wtr_leituras.loc_id,count(*) as liv_total,sum(case when lei_data != '' then 1 else 0 /*zero*/end) as liv_leitura," +
                         "sum(case when lei_data = '' or lei_data is NULL then 1 else 0 /*zero*/end) as liv_naolida, liv_datacarga,liv_flag " +
                         " from wtr_leituras inner join wtr_leituristas on (wtr_leituristas.ltr_id  = wtr_leituras.ltr_id and wtr_leituras.cli_id =wtr_leituristas.cli_id and wtr_leituristas.emp_id = wtr_leituras.emp_id )" +
                         " inner join wtr_livros on (wtr_livros.ltr_id  = wtr_leituras.ltr_id and wtr_leituras.cli_id =wtr_livros.cli_id and wtr_leituras.emp_id = wtr_livros.emp_id and wtr_leituras.liv_mes = wtr_livros.liv_mes and wtr_leituras.liv_ano = wtr_livros.liv_ano and wtr_leituras.loc_id = wtr_livros.loc_id ) " +
                         where + " group by wtr_leituras.ltr_id,ltr_nome ,wtr_leituras.Liv_mes ,wtr_leituras.Liv_ano ,wtr_leituras.loc_id,liv_datacarga,liv_flag ";




            sql += "order by wtr_leituras.liv_ano,wtr_leituras.liv_mes,wtr_leituras.loc_id";

            DataTable dt = db.GetDt(carregaConexao(), "WTR_LEIT", sql);
            gridLiberarRotas.DataSource = dt;
            gridLiberarRotas.DataBind();

            Session["dt"] = dt;
        }
        catch (Exception ex)
        {

            ShowMessage("Erro-" + ex.Message);
        }

    }

    protected void ShowMessage(string msg)
    {
        String scriptString = "<script language = JavaScript>";
        scriptString += "confirm('" + msg + "');";
        scriptString += "</script>";
        Page.ClientScript.RegisterStartupScript(Page.GetType(), "scrt", scriptString);
    }

    protected void gridLiberarRotas_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {

        }
        catch (Exception ex)
        {

            ShowMessage("Erro-" + ex.Message);
        }
    }

    protected void chkMarcar_CheckedChanged(object sender, EventArgs e)
    {

        CheckBox ck1 = (CheckBox)sender;
        GridViewRow grow = (GridViewRow)ck1.NamingContainer;


        if (gridLiberarRotas.Rows[grow.RowIndex].RowType == DataControlRowType.DataRow)
        {
            Label mes = (Label)gridLiberarRotas.Rows[grow.RowIndex].Cells[2].Controls[1];
            Label ano = (Label)gridLiberarRotas.Rows[grow.RowIndex].Cells[3].Controls[1];
            Label rota = (Label)gridLiberarRotas.Rows[grow.RowIndex].Cells[4].Controls[1];

            DataTable dtVerifica = new DataTable();
            string flag = "";
            if (ck1.Checked)
            {
                flag = "Liberada";
            }
            else
            {
                flag = "Bloqueada";
            }

            int i = grow.RowIndex;
            if (((Label)gridLiberarRotas.Rows[i].Cells[10].FindControl("lblLIV_FLAG")).Text != flag)
            {

                if (flag == "Liberada")
                {
                    flag = "N";
                }
                else flag = "S";

                String sqlUpdateLivros = string.Format("update wtr_livros set liv_flag = '" + flag + "' where  loc_id = {0} and liv_mes = {1} and liv_ano = {2} and cli_id = {3} and emp_id = {4}",
                     rota.Text,
                     mes.Text,
                     ano.Text,
                     Request.Cookies["CliId"].Value.ToString(),
                     Request.Cookies["EmpId"].Value.ToString());
                Data db = new Data();

                bool erro = db.Exe(carregaConexao(), "wtr_livros", sqlUpdateLivros);
                if (!erro)
                    ShowMessage("WTR_LIVROS - Erro ao atualizar rota " + rota.Text);

                String sqlUpdateLeituras = string.Format("update wtr_leituras set lei_flag = '" + flag + "' where loc_id = {0} and liv_mes = {1} and liv_ano = {2} and cli_id = {3} and emp_id = {4}",
                   rota.Text,
                    mes.Text,
                    ano.Text,
                    Request.Cookies["CliId"].Value.ToString(),
                    Request.Cookies["EmpId"].Value.ToString());

                erro = db.Exe(carregaConexao(), "wtr_leituras", sqlUpdateLeituras);
                if (!erro)
                    ShowMessage("WTR_LEITURAS - Erro ao atualizar rota " + rota.Text);


            }

            CarregaGrid();
        }
    }

    protected void ddlContrato_SelectedIndexChanged(object sender, EventArgs e)
    {
        DropDownList leit = (DropDownList)sender;
        GridViewRow grow = (GridViewRow)leit.NamingContainer;


        if (gridLiberarRotas.Rows[grow.RowIndex].RowType == DataControlRowType.DataRow)
        {
            Label mes = (Label)gridLiberarRotas.Rows[grow.RowIndex].Cells[2].Controls[1];
            Label ano = (Label)gridLiberarRotas.Rows[grow.RowIndex].Cells[3].Controls[1];
            Label rota = (Label)gridLiberarRotas.Rows[grow.RowIndex].Cells[4].Controls[1];


            String sqlUpdateLivros = string.Format("update wtr_livros set ltr_id = '" + leit.SelectedValue.ToString() + "' where  loc_id = {0} and liv_mes = {1} and liv_ano = {2} and cli_id = {3} and emp_id = {4}",
                 rota.Text,
                 mes.Text,
                 ano.Text,
                 Request.Cookies["CliId"].Value.ToString(),
                 Request.Cookies["EmpId"].Value.ToString());
            Data db = new Data();

            bool erro = db.Exe(carregaConexao(), "wtr_livros", sqlUpdateLivros);
            if (!erro)
                ShowMessage("WTR_LIVROS - Erro ao atualizar rota " + rota.Text);

            String sqlUpdateLeituras = string.Format("update wtr_leituras set ltr_id = '" + leit.SelectedValue.ToString() + "' where loc_id = {0} and liv_mes = {1} and liv_ano = {2} and cli_id = {3} and emp_id = {4}",
               rota.Text,
                mes.Text,
                ano.Text,
                Request.Cookies["CliId"].Value.ToString(),
                Request.Cookies["EmpId"].Value.ToString());

            erro = db.Exe(carregaConexao(), "wtr_leituras", sqlUpdateLeituras);
            if (!erro)
                ShowMessage("WTR_LEITURAS - Erro ao atualizar rota " + rota.Text);




            CarregaGrid();
        }
    }
}