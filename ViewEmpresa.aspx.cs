﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WaterSyncLite.MDL;
using WaterSyncLite.DAL;
using WaterSyncLite.Class;


public partial class ViewEmpresa : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        this.Master.LoadHeader(this.Master, Request.Path.Substring(Request.Path.LastIndexOf("/") + 1));
        BloquearCampos();
        EmpresaDAL empDAL = new EmpresaDAL();
        GridView1.DataSource = empDAL.TodasEmpresas();
        GridView1.DataBind();
    }


    protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if(e.CommandName == "Select")
        {
            BloquearCampos();
            int index = Convert.ToInt32(e.CommandArgument);
            GridViewRow row = GridView1.Rows[index]; 
            Empresa emp = new Empresa();
            EmpresaDAL empDAL = new EmpresaDAL();
            string aux = row.Cells[1].Text;

            emp = empDAL.getEmp(Convert.ToInt16(aux));

            txtBairro.Text = emp.Bairro;
            txtCEP.Text = emp.CEP;
            txtCidade.Text = emp.Cidade;
            txtCto1.Text = emp.Cont1;
            txtCto2.Text = emp.Cont2;
            txtFone1.Text = emp.Fone1;
            txtFone2.Text = emp.Fone2;
            txtHomePage.Text = emp.HomePage;
            txtId.Text = emp.CliId.ToString();
            txtRazao.Text = emp.RazaoSocial;
            txtUF.Text = emp.UF;
            txtEmail.Text = emp.Email;
            txtFantasia.Text = emp.NomeFantasia;
            txtEnd.Text = emp.Endereco;
            



        }
    }
    protected void btnAlterar_Click(object sender, EventArgs e)
    {
        try
        {
            desbloquearCampos();
            Empresa emp = new Empresa();
            EmpresaDAL empDAL = new EmpresaDAL();
            emp.EmpId = Dados.Company.EmpId;
            emp.Bairro = txtBairro.Text;
            emp.CEP = txtCEP.Text;
            emp.Cidade = txtCidade.Text;
            emp.Cont1 = txtCto1.Text;
            emp.Cont2 = txtCto2.Text;
            emp.Fone1 = txtFone1.Text;
            emp.Fone2 = txtFone2.Text;
            emp.HomePage = txtHomePage.Text;
            emp.CliId = Convert.ToInt32(txtId.Text);
            emp.RazaoSocial = txtRazao.Text;
            emp.UF = txtUF.Text;
            emp.Email = txtEmail.Text;
            emp.NomeFantasia = txtFantasia.Text;
            emp.Endereco = txtEnd.Text;

            bool check = empDAL.update(emp);
            if (check)
                Response.Write("Dados Atualizados com sucesso");
            else
                Response.Write("Dados não atualizados");
        }
        catch(Exception ex)
        {
            Response.Write("Erro: "+ex.Message);
        }

    }

    protected void BloquearCampos() 
    {
        txtBairro.Enabled = false;
        txtCEP.Enabled =  false;
        txtCidade.Enabled =  false;
        txtCto1.Enabled =  false;
        txtCto2.Enabled=  false;
        txtFone1.Enabled =  false;
        txtFone2.Enabled =  false; 
        txtHomePage.Enabled =  false;
        txtId.Enabled =  false; 
        txtRazao.Enabled =  false;
        txtUF.Enabled =  false;
        txtEmail.Enabled =  false;
        txtFantasia.Enabled =  false;
        txtEnd.Enabled = false;
    }

    protected void desbloquearCampos()
    {
        txtBairro.Enabled = true;
        txtCEP.Enabled = true;
        txtCidade.Enabled = true;
        txtCto1.Enabled = true;
        txtCto2.Enabled = true;
        txtFone1.Enabled = true;
        txtFone2.Enabled = true;
        txtHomePage.Enabled = true;
        txtRazao.Enabled = true;
        txtUF.Enabled = true;
        txtEmail.Enabled = true;
        txtFantasia.Enabled = true;
        txtEnd.Enabled = true;
    }
}