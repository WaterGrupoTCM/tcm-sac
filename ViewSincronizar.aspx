﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="ViewSincronizar.aspx.cs" Inherits="ViewSincronizar" %>

<%@ MasterType VirtualPath="~/Site.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="Content/css/controls/tab.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <form id="form1" runat="server">


        <asp:HiddenField ID="TabName" runat="server" />

        <div class="panel panel-primary">
            <div class="panel-heading">
                <h4 class="panel-title"><span class="TitulosPanel">Painel de Controle</span></h4>
            </div>
            <div class="panel-body">
                <div>
                    <div class="col-sm-4 col-lg-3" style="padding-left:0px">
                        <div class="input-group">
                            <span class="input-group-addon">Leiturista</span>
                            <asp:DropDownList ID="ddlLtr" runat="server" CssClass="form-control" AutoPostBack="True" OnSelectedIndexChanged="ddlLtr_SelectedIndexChanged">
                            </asp:DropDownList>
                        </div>
                    </div>

                </div>
                <br />
                <br />
                <br />
                <div id="Tabs" role="tabpanel">
                    <ul class="nav nav-tabs">
                        <li class="active"><a data-toggle="tab" href="#home">Conexão</a></li>
                        <li><a data-toggle="tab" href="#menu1">Ferramentas</a></li>
                        <li><a data-toggle="tab" href="#menu2">Localização</a></li>

                    </ul>

                    <div class="tab-content" style="padding-left: 20px">


                        <div id="home" class="tab-pane fade in active">
                            <h4>Envio de dados</h4>
                            <div class="radio">
                                <asp:RadioButton ID="rbLeituraLeitura" runat="server" GroupName="1" Text="Leitura a Leitura" CssClass="" AutoPostBack="True" OnCheckedChanged="rbLeituraLeitura_CheckedChanged" />
                            </div>
                            <div class="radio">
                                <asp:RadioButton ID="rbLeituraTempo" runat="server" GroupName="1" Text="Por tempo determinado" CssClass="Paragrafo" AutoPostBack="True" OnCheckedChanged="rbLeituraTempo_CheckedChanged" />
                            </div>
                            <asp:Panel ID="panelLeituraTempo" runat="server" CssClass="Paragrafo">
                                <asp:Label ID="Label2" runat="server" Text="Atualização a cada "></asp:Label>

                                <asp:TextBox ID="txtTempoLeitura" runat="server" Width="50px" AutoPostBack="True" OnTextChanged="txtTempoLeitura_TextChanged"></asp:TextBox>
                                <asp:Label ID="Label3" runat="server" Text=" minutos. " CssClass="Paragrafo"></asp:Label>
                            </asp:Panel>
                            <div class="radio">
                                <asp:RadioButton ID="rbFinalizarLeituras" runat="server" GroupName="1" Text="Ao finalizar leituras" CssClass="Paragrafo" AutoPostBack="True" OnCheckedChanged="rbFinalizarLeituras_CheckedChanged" />
                            </div>
                            <div class="radio">
                                <asp:RadioButton ID="rbManual" runat="server" GroupName="1" Text="Envio Manual" CssClass="Paragrafo" AutoPostBack="True" OnCheckedChanged="rbManual_CheckedChanged" />
                            </div>


                        </div>

                        <div id="menu1" class="tab-pane fade">
                            <div class="checkbox">
                                <asp:CheckBox ID="chkAlteracao" runat="server" Text="Permitir Alteração de Leitura" OnCheckedChanged="chkAlteracao_CheckedChanged" AutoPostBack="True" />

                            </div>
                            <div class="checkbox">
                                <asp:CheckBox ID="chkAtualizacaoCadastral" runat="server" Text="Atualização Cadastral" OnCheckedChanged="chkAtualizacaoCadastral_CheckedChanged" AutoPostBack="True" />
                            </div>
                            <div class="checkbox">
                                <asp:CheckBox ID="chkImpSimultanea" runat="server" Text="Impressão Simultânea" OnCheckedChanged="chkImpSimultanea_CheckedChanged" AutoPostBack="True" />
                            </div>
                            <div class="checkbox">
                                <asp:CheckBox ID="chkLeituraAnterior" runat="server" Text="Exibir Leitura Anterior" OnCheckedChanged="chkLeituraAnterior_CheckedChanged" AutoPostBack="True" />
                            </div>
                            <div class="checkbox">
                                <asp:CheckBox ID="chkReImpressao" runat="server" Text="Permitir reimpressão da fatura" AutoPostBack="True" OnCheckedChanged="chkReImpressao_CheckedChanged" />
                            </div>
                            <div class="checkbox">
                                <asp:CheckBox ID="chkTipoEntrega" runat="server" Text="Informar forma de entrega da fatura" AutoPostBack="True" OnCheckedChanged="chkTipoEntrega_CheckedChanged" />
                            </div>


                            <div class="checkbox">
                                <asp:CheckBox ID="chkBkpLeituras" runat="server" Text="Imprimir backup de leitura" OnCheckedChanged="chkTempoLocalizacao_CheckedChanged" AutoPostBack="True" />
                            </div>
                            <asp:Panel ID="panelBkpImp" runat="server" CssClass="Paragrafo">
                                <asp:Label ID="Label1" runat="server" Text="Impressão do backup a cada "></asp:Label>

                                <asp:TextBox ID="txtBkpImp" runat="server" Width="50px" AutoPostBack="True"></asp:TextBox>
                                <asp:Label ID="Label6" runat="server" Text=" leituras. " CssClass="Paragrafo"></asp:Label>
                            </asp:Panel>

                            <div class="checkbox">
                                <asp:CheckBox ID="chkTelaSimples" runat="server" Text="Tela Simples" AutoPostBack="True" />
                            </div>

                            <div class="checkbox">
                                <asp:CheckBox ID="chkImpLatLong" runat="server" Text="Imprimir latitude e longitude" AutoPostBack="True" />
                            </div>
                            <div class="checkbox">
                                <asp:CheckBox ID="chkCamera" runat="server" Text="Utilizar câmera" AutoPostBack="True" OnCheckedChanged="chkCamera_CheckedChanged" />
                            </div>
                        </div>


                        <div id="menu2" class="tab-pane fade">
                            <div class="checkbox">
                                <asp:CheckBox ID="chkLocalizacao" runat="server" Text="Exigir Localização" OnCheckedChanged="chkLocalizacao_CheckedChanged" AutoPostBack="True" />

                            </div>
                            <div class="checkbox">
                                <asp:CheckBox ID="chkTempoLocalizacao" runat="server" Text="Enviar Localização por tempo determinado" OnCheckedChanged="chkTempoLocalizacao_CheckedChanged" AutoPostBack="True" />
                            </div>
                            <asp:Panel ID="panelTempoLocalizacao" runat="server" CssClass="Paragrafo">
                                <asp:Label ID="Label4" runat="server" Text="Atualização a cada "></asp:Label>

                                <asp:TextBox ID="txtTempoLocalizacao" runat="server" Width="50px" AutoPostBack="True" OnTextChanged="txtTempoLocalizacao_TextChanged"></asp:TextBox>
                                <asp:Label ID="Label5" runat="server" Text=" minutos. " CssClass="Paragrafo"></asp:Label>
                            </asp:Panel>

                        </div>

                    </div>
                </div>
            </div>
        </div>



        <script type="text/javascript">
            jQuery(function () {
                var tabName = jQuery("[id*=TabName]").val() != "" ? jQuery("[id*=TabName]").val() : "personal";
                jQuery('#Tabs a[href="#' + tabName + '"]').tab('show');
                jQuery("#Tabs a").click(function () {
                    jQuery("[id*=TabName]").val(jQuery(this).attr("href").replace("#", ""));
                });
            });
        </script>
    </form>





</asp:Content>
