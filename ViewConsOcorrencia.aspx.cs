﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using WaterSyncLite.MDL;
using WaterSyncLite.DAL;
using WaterSyncLite.Class;
public partial class ViewConsOcorrencia : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        this.Master.LoadHeader(this.Master, Request.Path.Substring(Request.Path.LastIndexOf("/") + 1));
        if (!IsPostBack)
        {
            try
            {
                cbxMes.SelectedIndex = DateTime.Now.Month;
                txtAno.Text = DateTime.Now.Year.ToString();
                Data db = new Data();

                if(Request.Cookies["CliID"].Value.PadLeft(2, '0').ToString().Trim() != "05")
                {
                    panelVencimento.Visible = false;
                    rbVencimento.Checked = false;
                    rbVencimento.Visible = false;
                }



                #region LEITURISTAS
                DataTable dt = db.GetDt(carregaConexao(), "WTR_LEIT", "SELECT LTR_ID, LTR_NOME FROM "
                    + " WTR_LEITURISTAS WHERE CLI_ID = " + Request.Cookies["CliID"].Value.ToString().Trim()
                    + " AND EMP_ID = " + Request.Cookies["EmpID"].Value.ToString().Trim()
                    + " ORDER BY LTR_NOME");

                DataRow dtr = dt.NewRow();
                dtr["LTR_NOME"] = "Selecione um leiturista...";
                dtr["LTR_ID"] = 0;
                dt.Rows.Add(dtr);
                if (dt != null)
                {
                    cbxLeit.DataSource = dt;
                    cbxLeit.DataTextField = "LTR_NOME";
                    cbxLeit.DataValueField = "LTR_ID";
                    cbxLeit.DataBind();
                    cbxLeit.SelectedValue = "0";

                }
                #endregion

            }
            catch (Exception ex)
            {
                msg("Problemas no sistema: " + ex.Message);
                Response.Redirect("ViewHome.aspx");
            }

        }
    }

    protected bool VerificarLogin()
    {
        if (Dados.user.Name == null || Dados.user.Pwd == null)
        {
            return false;

        }
        else return true;
    }





    protected void msg(string msg)
    {
        lblMsg.Text = msg;
        PanelMsg.Visible = true;
    }





    protected String carregaConexao()
    {
        String StrCon = "";
        if (Request.Cookies["StrCon"] != null)
        {
            if (Request.Cookies["StrCon"].Value.ToString().Length > 0)
                StrCon = Request.Cookies["StrCon"].Value.ToString();
        }
        return StrCon.Replace('|', ';').Replace("usuario", xml.GetConexaoValue(Server.MapPath("~"), "LOCAL", "USER")).Replace("senha", xml.GetConexaoValue(Server.MapPath("~"), "LOCAL", "PASSWORD"));
    }
    protected void carregaGrid()
    {
        Data db = new Data();
        DataTable dt = new DataTable();
        string sql;


        try
        {
            if (rbMes.Checked)
            {
                sql =
                    "select wtr_mensagens.MSG_ID as 'CÓDIGO',wtr_mensagens.msg_descricao as DESCRIÇÃO, count(*) as QUANTIDADE " +
                    " FROM WTR_LEITURAS INNER JOIN WTR_MENSAGENS ON (WTR_LEITURAS.MSG_ID = WTR_MENSAGENS.MSG_ID) WHERE " +
                    " LIV_MES = " + cbxMes.SelectedIndex +
                    " AND LIV_ANO = " + txtAno.Text;
                if (txtRota.Text != "")
                    sql += " and wtr_leituras.loc_id = " + Convert.ToInt16(txtRota.Text);
                sql+=" GROUP BY  wtr_mensagens.MSG_ID ,wtr_mensagens.msg_descricao ";

                dt = db.GetDt(carregaConexao().Replace('|', ';'), "WTR_LEITURAS", sql);
                GridView1.DataSource = dt;
                GridView1.DataBind();
            }
            else
            {
                if (rbRota.Checked)
                {
                    sql = "select wtr_leituras.loc_id as ROTA, wtr_mensagens.MSG_ID  as 'CÓDIGO',wtr_mensagens.msg_descricao as DESCRIÇÃO, count(*) as QUANTIDADE " +
                   " FROM WTR_LEITURAS INNER JOIN WTR_MENSAGENS ON (WTR_LEITURAS.MSG_ID = WTR_MENSAGENS.MSG_ID) WHERE " +
                   " LIV_MES = " + cbxMes.SelectedIndex +
                   " AND LIV_ANO = " + txtAno.Text;
                    if (txtRota.Text != "")
                        sql += " and wtr_leituras.loc_id = " + Convert.ToInt16(txtRota.Text);


                    sql += " GROUP BY  wtr_leituras.loc_id,wtr_mensagens.MSG_ID ,wtr_mensagens.msg_descricao " +
                    " order by wtr_leituras.loc_id ";

                    dt = db.GetDt(carregaConexao().Replace('|', ';'), "WTR_LEITURAS", sql);
                    GridView1.DataSource = dt;
                    GridView1.DataBind();
                }
                else
                {
                    if (rbLtr.Checked)
                    {
                        if (cbxLeit.SelectedValue == "0")
                        {
                            sql =
                                "select LTR_NOME,wtr_leituras.LTR_ID2 as LEITURISTA, wtr_mensagens.MSG_ID  as 'CÓDIGO',wtr_mensagens.msg_descricao as DESCRIÇÃO, count(*) as QUANTIDADE " +
                                " FROM WTR_LEITURAS INNER JOIN WTR_MENSAGENS ON (WTR_LEITURAS.MSG_ID = WTR_MENSAGENS.MSG_ID and WTR_LEITURAS.EMP_ID = WTR_MENSAGENS.EMP_ID) INNER JOIN WTR_LEITURISTAS LTR ON (LTR.LTR_ID = WTR_LEITURAS.LTR_ID2) WHERE " +
                                " LIV_MES = " + cbxMes.SelectedIndex +
                                " AND LIV_ANO = " + txtAno.Text;
                            if (txtRota.Text != "")
                                sql += " and wtr_leituras.loc_id = " + Convert.ToInt16(txtRota.Text);
                        sql+=" GROUP BY  wtr_leituras.LTR_ID2,wtr_mensagens.MSG_ID ,wtr_mensagens.msg_descricao,LTR_NOME " +
                        " order by wtr_leituras.LTR_ID2 ";

                            dt = db.GetDt(carregaConexao().Replace('|', ';'), "WTR_LEITURAS", sql);
                            GridView1.DataSource = dt;
                            GridView1.DataBind();
                        }
                        else
                        {
                            sql =
                                "select wtr_leituras.LTR_ID2 as LEITURISTA, wtr_mensagens.MSG_ID  as 'CÓDIGO',wtr_mensagens.msg_descricao as DESCRIÇÃO, count(*) as QUANTIDADE " +
                                " FROM WTR_LEITURAS INNER JOIN WTR_MENSAGENS ON (WTR_LEITURAS.MSG_ID = WTR_MENSAGENS.MSG_ID) WHERE " +
                                " LIV_MES = " + cbxMes.SelectedIndex +
                                " AND LIV_ANO = " + txtAno.Text +
                                " AND WTR_LEITURAS.LTR_ID2 = " + Convert.ToInt16(cbxLeit.SelectedValue);
                            if (txtRota.Text != "")
                                sql += " and wtr_leituras.loc_id = " + Convert.ToInt16(txtRota.Text);
                       sql +=" GROUP BY  wtr_leituras.LTR_ID2,wtr_mensagens.MSG_ID ,wtr_mensagens.msg_descricao " +
                       " order by wtr_leituras.LTR_ID2 ";

                            dt = db.GetDt(carregaConexao().Replace('|', ';'), "WTR_LEITURAS", sql);
                            GridView1.DataSource = dt;
                            GridView1.DataBind();
                        }

                    }
                    else
                    {
                        if (rbVencimento.Checked)
                        {
                            sql = "select substring(convert(varchar,loc_id),1,1) as Vencimento,wtr_mensagens.MSG_ID as 'CÓDIGO',wtr_mensagens.msg_descricao as DESCRIÇÃO, count(*) as QUANTIDADE " +
                                   " FROM WTR_LEITURAS INNER JOIN WTR_MENSAGENS ON (WTR_LEITURAS.MSG_ID = WTR_MENSAGENS.MSG_ID) WHERE " +
                                   " LIV_MES = " + cbxMes.SelectedIndex +
                                   " AND LIV_ANO = " + txtAno.Text;
                            if (ddlVencimento.SelectedIndex.ToString() != "0")
                            {
                                sql += " and substring(convert(varchar,loc_id),1,1) = '" + ddlVencimento.SelectedValue + "'";
                            }

                            if (txtRota.Text != "")
                                sql += " and wtr_leituras.loc_id = " + Convert.ToInt16(txtRota.Text);

                            sql += " GROUP BY  wtr_mensagens.MSG_ID ,wtr_mensagens.msg_descricao,substring(convert(varchar,loc_id),1,1) order by substring(convert(varchar,loc_id),1,1) ";

                            dt = db.GetDt(carregaConexao().Replace('|', ';'), "WTR_LEITURAS", sql);
                            GridView1.DataSource = dt;
                            GridView1.DataBind();
                        }
                    }
                }
            }
        }
        catch (Exception ex)
        {
            msg("Problema no sistema:" + ex.Message);
        }
    }
    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            GridView1.PageIndex = e.NewPageIndex;
            carregaGrid();
            GridView1.DataBind();
        }
        catch { }
    }
   

    protected void btPesquisar_Click(object sender, EventArgs e)
    {
        PanelMsg.Visible = false;
        carregaGrid();
    }

}