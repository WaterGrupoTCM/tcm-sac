﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="ViewConsHoraLeitura.aspx.cs" Inherits="ViewConsHoraLeitura" %>
<%@ MasterType VirtualPath="~/Site.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
    <style type="text/css">
      
        .style17
        {
            width: 100%;
        }
        .style18
        {
            font-size: x-large;
            height: 1px;
        }
        .style101015
        {
            width: 159px;
        }
        .style101016
        {
            width: 120px;
        }
        .style101017
        {
            width: 254px;
        }
        .style101018
        {
            width: 103px;
        }
        .style101019
        {
            width: 189px;
        }
        .style101020
        {
            width: 69px;
        }
      
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
    <table class="style17">
        <tr>
            <td class="style18">
                Consulta dos Horários de Leituras</td>
        </tr>
        <tr>
            <td>
                <table class="style17">
                    <tr>
                        <td class="style101017">
                            Leiturista&nbsp;&nbsp;&nbsp;&nbsp;</td>
                        <td class="style101018">
                            Ano&nbsp;&nbsp;</td>
                        <td class="style101016">
                            Mês</td>
                        <td class="style101015">
                            Livro</td>
                        <td class="style101019">
                            Etapa</td>
                        <td class="style101020">
                            &nbsp;</td>
                        <td rowspan="2">
                <asp:ImageButton ID="btnConsultar" runat="server" Height="33px" 
                    ImageUrl="~/imagens/1362163260_Search.png" onclick="btnConsultar_Click" 
                    Width="36px" />
                        </td>
                    </tr>
                    <tr>
                        <td class="style101017">
                <asp:DropDownList ID="cbxLtr" runat="server" Height="25px" Width="242px" AutoPostBack="True" OnSelectedIndexChanged="cbxLtr_SelectedIndexChanged">
                </asp:DropDownList>
                        </td>
                        <td class="style101018">
                <asp:DropDownList ID="cbxAno" runat="server" Height="25px" Width="88px" AutoPostBack="True" OnSelectedIndexChanged="cbxAno_SelectedIndexChanged">
                    <asp:ListItem>Selecione o ano...</asp:ListItem>
                    <asp:ListItem>2011</asp:ListItem>
                    <asp:ListItem>2012</asp:ListItem>
                    <asp:ListItem>2013</asp:ListItem>
                    <asp:ListItem>2014</asp:ListItem>
                    <asp:ListItem>2015</asp:ListItem>
                    <asp:ListItem>2016</asp:ListItem>
                    <asp:ListItem>2017</asp:ListItem>
                    <asp:ListItem>2018</asp:ListItem>
                </asp:DropDownList>
                        </td>
                        <td class="style101016">
                <asp:DropDownList ID="cbxMes" runat="server" Height="25px" Width="102px" AutoPostBack="True" OnSelectedIndexChanged="cbxMes_SelectedIndexChanged">
                    <asp:ListItem>Selecione o mês...</asp:ListItem>
                    <asp:ListItem>Janeiro</asp:ListItem>
                    <asp:ListItem>Fevereiro</asp:ListItem>
                    <asp:ListItem>Março</asp:ListItem>
                    <asp:ListItem>Abril</asp:ListItem>
                    <asp:ListItem>Maio</asp:ListItem>
                    <asp:ListItem>Junho</asp:ListItem>
                    <asp:ListItem>Julho</asp:ListItem>
                    <asp:ListItem>Agosto</asp:ListItem>
                    <asp:ListItem>Setembro</asp:ListItem>
                    <asp:ListItem>Outubro</asp:ListItem>
                    <asp:ListItem>Novembro</asp:ListItem>
                    <asp:ListItem>Dezembro</asp:ListItem>
                </asp:DropDownList>
                        </td>
                        <td class="style101015">
                <asp:DropDownList ID="cbxLivro" runat="server" Height="25px" Width="144px">
                </asp:DropDownList>
                        </td>
                        <td class="style101019">
                <asp:DropDownList ID="cbxEtapa" runat="server" Height="25px" 
                    onselectedindexchanged="cbxEtapa_SelectedIndexChanged" Width="172px">
                    <asp:ListItem>Selecione uma etapa...</asp:ListItem>
                    <asp:ListItem>Etapa 01</asp:ListItem>
                    <asp:ListItem>Etapa 02</asp:ListItem>
                    <asp:ListItem>Etapa 03</asp:ListItem>
                    <asp:ListItem>Etapa 04</asp:ListItem>
                    <asp:ListItem>Etapa 05</asp:ListItem>
                    <asp:ListItem>Etapa 06</asp:ListItem>
                    <asp:ListItem>Etapa 07</asp:ListItem>
                    <asp:ListItem>Etapa 08</asp:ListItem>
                    <asp:ListItem>Etapa 09</asp:ListItem>
                    <asp:ListItem>Etapa 10</asp:ListItem>
                    <asp:ListItem>Etapa 11</asp:ListItem>
                    <asp:ListItem>Etapa 12</asp:ListItem>
                    <asp:ListItem>Etapa 13</asp:ListItem>
                </asp:DropDownList>
                        </td>
                        <td class="style101020">
                <asp:CheckBox ID="chkLocal" runat="server" Text="Local" />
                        </td>
                    </tr>
                </table>
                <br />
                <asp:GridView ID="GridView1" runat="server" AllowPaging="True" 
                    BackColor="White" BorderColor="#999999" BorderStyle="None" BorderWidth="1px" 
                    CellPadding="3" GridLines="Vertical" 
                    onpageindexchanged="GridView1_PageIndexChanged" 
                    onpageindexchanging="GridView1_PageIndexChanging" PageSize="100">
                    <AlternatingRowStyle BackColor="#DCDCDC" />
                    <FooterStyle BackColor="#CCCCCC" ForeColor="Black" />
                    <HeaderStyle BackColor="#000084" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
                    <RowStyle BackColor="#EEEEEE" ForeColor="Black" />
                    <SelectedRowStyle BackColor="#008A8C" Font-Bold="True" ForeColor="White" />
                    <SortedAscendingCellStyle BackColor="#F1F1F1" />
                    <SortedAscendingHeaderStyle BackColor="#0000A9" />
                    <SortedDescendingCellStyle BackColor="#CAC9C9" />
                    <SortedDescendingHeaderStyle BackColor="#000065" />
                </asp:GridView>
            </td>
        </tr>
    </table>
</asp:Content>

