﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WaterSyncLite.MDL;
using WaterSyncLite.DAL;
using WaterSyncLite.Class;
using System.Data;

public partial class ViewConsDataLeitura : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        this.Master.LoadHeader(this.Master, Request.Path.Substring(Request.Path.LastIndexOf("/") + 1));

        if (!IsPostBack)
        {
            cbxMes.SelectedIndex = DateTime.Now.Month;
            txtAno.Text = DateTime.Now.Year.ToString();
            //popMes();
        }

    }

    protected void btPesquisar_Click(object sender, EventArgs e)
    {
        carregaGrid();
    }

    protected void carregaGrid()
    {
        PanelMsg.Visible = false;

         Data db = new Data();

           
        String sql = "";
        if (TabName.Value == "referencia")
        {
            if (txtAno.Text != "")
            {
                sql = "SELECT LOC_ID AS ROTA,liv_mes as REF, substring( lei_data,1,10) AS 'Data Leitura', COUNT(*) AS 'Leituras Realizadas' FROM WTR_LEITURAS WHERE CLI_ID=" + Request.Cookies["CliID"].Value +
               " and liv_mes = " + cbxMes.SelectedIndex.ToString() + " and liv_ano = " + txtAno.Text +
               " GROUP BY substring( lei_data,1,10),LOC_ID,liv_mes " +
               " ORDER BY loc_id,substring( lei_data,1,10) desc ";
                //}

            }
            else
            {
                PanelMsg.Visible = true;
                lblMsg.Text = ("Necessário preencher o ano para realizar a consulta: ");
                return;
            }
        }
        else
        {
            if (txtDataInicial.Text != "" && txtDataFinal.Text != "")
            {
                sql =
                    " SELECT LOC_ID AS ROTA,liv_mes as REF, substring( lei_data,1,10) AS 'Data Leitura', COUNT(*) AS 'Leituras Realizadas' " +
                    "FROM WTR_LEITURAS WHERE CLI_ID=" + Request.Cookies["CliID"].Value + " AND SUBSTRING(LEI_DATA, 7,4)=" +
                    txtDataFinal.Text.Substring(6, 4) + " AND (((SUBSTRING(LEI_DATA, 1,2) >= " +
                    txtDataInicial.Text.Substring(0, 2) + ") and (SUBSTRING(LEI_DATA, 1,2) <= " +
                    txtDataFinal.Text.Substring(0, 2) + ")))  " +
                    "and ((SUBSTRING(LEI_DATA, 4,2) =" + txtDataFinal.Text.Substring(3, 2) +
                    ") OR (SUBSTRING(LEI_DATA, 4,2) =" + txtDataInicial.Text.Substring(3, 2) +
                    ")) GROUP BY substring( lei_data,1,10),LOC_ID,liv_mes  ORDER BY loc_id,substring( lei_data,1,10) desc ";
                //}
            }
            else
            {
                PanelMsg.Visible = true;
                lblMsg.Text = ("Necessário preencher as datas para realizar a consulta: ");
                return;
            }
        }
        DataTable dt = db.GetDt(carregaConexao(), "WTR_LEITURAS", sql);
        GridView1.DataSource = dt;
        GridView1.DataBind();

        if (TabName.Value == "referencia")
        {
            if (txtAno.Text != "")
            {
                if (Convert.ToInt32(Request.Cookies["CliID"].Value) == 9)
                {
                    sql =
                        "SELECT LOC_ID AS ROTA,liv_mes as REF, substring( lei_data,7,2)+'/'+substring( lei_data,9,2)+'/'+substring( lei_data,11,4) AS 'Data Leitura', COUNT(*) AS 'Leituras Realizadas' FROM WTR_LEITURAS WHERE LEI_IMPRESSO='S' AND CLI_ID=" +
                        Request.Cookies["CliID"].Value +
                        " and liv_mes = " + cbxMes.SelectedIndex.ToString() + " and liv_ano = " + txtAno.Text +
                        " GROUP BY substring( lei_data,7,2)+'/'+substring( lei_data,9,2)+'/'+substring( lei_data,11,4),LOC_ID,liv_mes " +
                        " ORDER BY loc_id,substring( lei_data,7,2)+'/'+substring( lei_data,9,2)+'/'+substring( lei_data,11,4) desc ";
                }
                else
                {
                    sql =
                       "SELECT LOC_ID AS ROTA,liv_mes as REF, substring( lei_data,1,10) AS 'Data Leitura', COUNT(*) AS 'Leituras Realizadas' FROM WTR_LEITURAS WHERE LEI_IMPRESSO='S' AND CLI_ID=" +
                       Request.Cookies["CliID"].Value +
                       " and liv_mes = " + cbxMes.SelectedIndex.ToString() + " and liv_ano = " + txtAno.Text +
                       " GROUP BY substring( lei_data,1,10),LOC_ID,liv_mes " +
                       " ORDER BY loc_id,substring( lei_data,1,10) desc ";
                }
            }
            else
            {
                PanelMsg.Visible = true;
                lblMsg.Text = ("Necessário preencher o ano para realizar a consulta: ");
                return;
            }
        }
        else
        {
            if (txtDataInicial.Text != "" && txtDataFinal.Text != "")
            {
                if (Request.Cookies["CliID"].Value == "9")
                {

                    sql =
                        " SELECT LOC_ID AS ROTA,liv_mes as REF, substring( lei_data,7,2)+'/'+substring( lei_data,9,2)+'/'+substring( lei_data,11,4) AS 'Data Leitura', COUNT(*) AS 'Leituras Realizadas' " +
                        "FROM WTR_LEITURAS WHERE lei_impresso= 'S' and CLI_ID=9 AND substring( lei_data,11,4)=" +
                        txtDataFinal.Text.Substring(6, 4) + " AND (((substring( lei_data,7,2) >= " +
                        txtDataInicial.Text.Substring(0, 2) + ") and (SUBSTRING(LEI_DATA, 7,2) <= " +
                        txtDataFinal.Text.Substring(0, 2) + ")))  " +
                        "and ((SUBSTRING(LEI_DATA, 9,2) =" + txtDataFinal.Text.Substring(3, 2) +
                        ") OR (SUBSTRING(LEI_DATA, 9,2) =" + txtDataInicial.Text.Substring(3, 2) +
                        ")) GROUP BY substring( lei_data,7,2)+'/'+substring( lei_data,9,2)+'/'+substring( lei_data,11,4),LOC_ID,liv_mes  ORDER BY loc_id,substring( lei_data,7,2)+'/'+substring( lei_data,9,2)+'/'+substring( lei_data,11,4) desc ";
                }
                else
                {
                    sql = " SELECT LOC_ID AS ROTA,liv_mes as REF, substring( lei_data,1,10) AS 'Data Leitura', COUNT(*) AS 'Leituras Realizadas' " +
                   "FROM WTR_LEITURAS WHERE lei_impresso= 'S' and CLI_ID=16 AND SUBSTRING(LEI_DATA, 7,4)=" + txtDataFinal.Text.Substring(6, 4) + " AND (((SUBSTRING(LEI_DATA, 1,2) >= " + txtDataInicial.Text.Substring(0, 2) + ") and (SUBSTRING(LEI_DATA, 1,2) <= " + txtDataFinal.Text.Substring(0, 2) + ")))  " +
                   "and ((SUBSTRING(LEI_DATA, 4,2) =" + txtDataFinal.Text.Substring(3, 2) + ") OR (SUBSTRING(LEI_DATA, 4,2) =" + txtDataInicial.Text.Substring(3, 2) + ")) GROUP BY substring( lei_data,1,10),LOC_ID,liv_mes  ORDER BY loc_id,substring( lei_data,1,10) desc ";
                }

            }
            else
            {
                PanelMsg.Visible = true;
                lblMsg.Text = ("Necessário preencher as datas para realizar a consulta: ");
                return;
            }
        }
       



    }

    protected String carregaConexao()
    {
        String StrCon = "";
        if (Request.Cookies["StrCon"] != null)
        {
            if (Request.Cookies["StrCon"].Value.ToString().Length > 0)
                StrCon = Request.Cookies["StrCon"].Value.ToString();
        }
        return StrCon.Replace('|', ';').Replace("usuario", xml.GetConexaoValue(Server.MapPath("~"), "LOCAL", "USER")).Replace("senha", xml.GetConexaoValue(Server.MapPath("~"), "LOCAL", "PASSWORD"));
    }



    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView1.PageIndex = e.NewPageIndex;
        carregaGrid();
        GridView1.DataBind();
    }
}