﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iTextSharp.text;
using iTextSharp.text.pdf;
using Microsoft.WindowsMobile.Samples.Location;
using WaterSyncLite.Class;

public partial class View2Via : System.Web.UI.Page
{
    public static bool analExists = false;
    Data db = new Data();

    protected void Page_Load(object sender, EventArgs e)
    {
        this.Master.LoadHeader(this.Master, Request.Path.Substring(Request.Path.LastIndexOf("/") + 1));
        if (!IsPostBack)
        {
            
            try
            {
                String StrCon = carregaConexao();

                string sql = "";

                //header();
                txtAno.Text = DateTime.Now.Year.ToString();
                cbxMes.SelectedIndex = DateTime.Now.Month;


                

                if (Request.Cookies["CliID"].Value.PadLeft(2, '0').ToString().Trim() != "17")
                {
                    PanelGrupo.Visible = false;
                }

                #region OCORRENCIAS
                DataTable dt = db.GetDt(StrCon, "WTR_MENSAGENS", "SELECT MSG_ID, MSG_DESCRICAO FROM "
                    + " WTR_MENSAGENS WHERE CLI_ID = " + Request.Cookies["CliID"].Value.PadLeft(2, '0').ToString().Trim()
                    + " AND EMP_ID = " + Request.Cookies["EmpID"].Value.ToString().Trim()
                    + " ORDER BY MSG_ID");

                DataRow dtr = dt.NewRow();
                dtr["MSG_DESCRICAO"] = "Todas";
                dtr["msg_id"] = 1000;
                dt.Rows.Add(dtr);
                if (dt != null)
                {
                    cbxMsg.DataSource = dt;

                    cbxMsg.DataTextField = "MSG_DESCRICAO";
                    cbxMsg.DataValueField = "msg_id";
                    cbxMsg.DataBind();
                    cbxMsg.SelectedValue = "1000";
                }
                #endregion


                

            }
            catch (Exception ex)
            {
                msg("Problemas no sistema: " + ex.Message);
            }
        }
    }

    protected string VerificaPermissao()
    {
        try
        {
            Data db = new Data();
            string sql = "select * from wtr_user where usr_id = " + Request.Cookies["user"].Value.ToString().Trim();

            DataTable dt = db.GetDt("Data Source=.\\SQLEXPRESS;User ID=" + xml.GetConexaoValue(Server.MapPath("~"), "LOCAL", "USER") + ";Password=" + xml.GetConexaoValue(Server.MapPath("~"), "LOCAL", "PASSWORD") + ";Initial Catalog=grupotcm", "WTR_USER", sql);

            return dt.DefaultView[0].Row["USR_ADM"].ToString().Trim();
        }
        catch { return ""; }
    }

    protected String carregaConexao()
    {
        String StrCon = "";
        if (Request.Cookies["StrCon"] != null)
        {
            if (Request.Cookies["StrCon"].Value.ToString().Length > 0)
                StrCon = Request.Cookies["StrCon"].Value.ToString();
        }
        return StrCon.Replace('|', ';').Replace("usuario", xml.GetConexaoValue(Server.MapPath("~"), "LOCAL", "USER")).Replace("senha", xml.GetConexaoValue(Server.MapPath("~"), "LOCAL", "PASSWORD"));
    }

    protected void msg(string texto)
    {
        Response.Write(texto);
    }
    


    private void makeLista(string ordeR)
    {
        try
        {
            String StrCon = carregaConexao();
            DataTable dt = carregaGrid();

              

            //lblTOTAL.Text = dt.Rows.Count + " leitura(s) encontrada(s).";

            if (dt.Rows.Count > 0)
            {
                if (Request.Cookies["CliID"].Value.PadLeft(2, '0').ToString().Trim() == "05")
                    GridView1.AllowPaging = false;

                GridView1.DataSource = dt;

            }
            else
            {
                msg("Nenhum histórico encontrado para esta identificação ou medidor informado.");
            }
            GridView1.DataBind();
        }
        catch (Exception ex)
        {
            msg("Informações do sistema: " + ex.Message);
        }
        finally
        {
            //Cursor.Current = Cursors.Default;
        }
    }

    protected void carregaInformacoesLivros()
    {
        /*String StrCon = carregaConexao();
        Data db = new Data();
        String where = "";
        if ((ddlLeituristas.SelectedValue != "1000" || txtLivro.Text.Length > 0) && cbxMes.SelectedIndex != 0 && txtAno.Text.Length > 0)
        {
            where += " where ";
            if (txtAno.Text.Length == 0 && cbxMes.SelectedIndex == 0)
            {
                if (ddlLeituristas.SelectedValue != "0")
                    where += " ltr_id =  " + ddlLeituristas.SelectedValue;
            }
            else
            {
                if (ddlLeituristas.SelectedValue != "1000")
                    where += " ltr_id =  " + ddlLeituristas.SelectedValue + " and";
                if (cbxMes.SelectedIndex != 0)
                    where += " liv_mes =  " + cbxMes.SelectedIndex + " and";
                if (txtAno.Text.Length > 0)
                    where += " liv_ano =  " + txtAno.Text;
            }



        }

        string sql = "Select LIV_DATALEIT,Liv_mes as Mês,Liv_ano as Ano,loc_id as Rota ,(select count(*) from wtr_leituras " + where + " and wtr_leituras.loc_id = wtr_livros.loc_id) as Total,(select count(*) from wtr_leituras " + where + " and lei_leitura != '' and wtr_leituras.loc_id = wtr_livros.loc_id) as Lidas ,(select count(*) from wtr_leituras " + where + " and lei_leitura = '' and wtr_leituras.loc_id = wtr_livros.loc_id) as 'Não Lidas',liv_dataCarga as 'Data Carga',liv_flag as Flag from wtr_livros " + where;
        sql += " and loc_id = " + txtLivro.Text;



        sql += "order by liv_ano,liv_mes,loc_id";

        DataTable dt = db.GetDt(carregaConexao(), "WTR_LEIT", sql);

        if (dt.DefaultView.Count > 0)
        {
            lblDataEnvio.Text = "Data da última atualização: ".PadRight(50, ' ') + dt.DefaultView[0].Row["LIV_DATALEIT"].ToString();
            lblTotalLivros.Text = "Quantidade Total de Leituras:".PadRight(50, ' ') + dt.DefaultView[0].Row["Total"].ToString();
            lblTotalLivrosNaoLidas.Text = "Quantidade Sem Leitura:".PadRight(50, ' ') + dt.DefaultView[0].Row["Não Lidas"].ToString();
            lblTotalLivrosLidas.Text = "Quantidade de leituras realizadas:".PadRight(50, ' ') + dt.DefaultView[0].Row["Lidas"].ToString();


        }*/

    }

    protected DataTable carregaGrid()
    {
        String StrCon = carregaConexao();
        
        Session["latitude"] = "";
        Session["longitude"] = "";
        Session["local"] = "";

       

        double porc = 0;
        string pMax = "";
        string pMin = "";

       
        db = new Data();

        string sql =
            "select LIV_ANO AS ANO, LIV_MES AS MES, LEI_LIGACAO AS LIGACAO,LEI_CONSUMIDOR AS CONSUMIDOR, LOC_ID AS ROTA, LEI_ENDERECO AS ENDEREÇO, LEI_NUMERO AS NUMERO, LEI_DATA ";
        if (Convert.ToInt16(Request.Cookies["CliID"].Value) == 5)
            sql += "+' '+LEI_HORA ";
        sql += "AS DATA, LTR_NOME AS LEITURISTA, "
                     +
                     " MSG_DESCRICAO AS OCORRENCIA, LEI_MEDIA AS MEDIA, LEI_ANTERIOR AS LEITURA_ANTERIOR, LEI_LEITURA AS LEITURA, "
                     + " LEI_NUMMED AS MEDIDOR,  LEI_CONSUMO,LEI_LATITUDE,LEI_LONGITUDE ";



        
        if (Request.Cookies["CliID"].Value.PadLeft(2, '0').ToString().Trim() == "17" && txtGrupo.Text.Length == 0)
            sql += ",GRUPO_ID ";


        sql += " FROM WTR_LEITURAS INNER JOIN WTR_MENSAGENS ON(WTR_LEITURAS.MSG_ID = WTR_MENSAGENS.MSG_ID and WTR_LEITURAS.cli_id = WTR_MENSAGENS.cli_id and WTR_MENSAGENS.emp_id = WTR_LEITURAS.emp_id) INNER JOIN WTR_LEITURISTAS ON (WTR_LEITURAS.LTR_ID = WTR_LEITURISTAS.LTR_ID) WHERE WTR_LEITURAS.emp_id =" +
                     Request.Cookies["EmpID"].Value.ToString().Trim() + " and WTR_LEITURAS.cid_id =" + Request.Cookies["EmpID"].Value.ToString().Trim() +
                     " and WTR_LEITURAS.CLI_ID = " + Request.Cookies["CliID"].Value.PadLeft(2, '0').ToString().Trim();// Data.cli.ToString();


        #region SQL E FILTROS

        if (txtLivro.Text.Trim().Length > 0)
        {
            if (Request.Cookies["CliID"].Value.PadLeft(2, '0').ToString().Trim() == "4")
                txtLivro.Text = txtLivro.Text.Trim().PadLeft(3, '0');
            else
                sql += " AND LOC_ID = '" + txtLivro.Text.Trim() + "' ";
        }

        if (cbxMsg.SelectedValue != "1000")
            sql += " AND WTR_LEITURAS.MSG_ID = '" + cbxMsg.SelectedValue.ToString() + "' ";

        if (cbxMes.SelectedIndex > 0)
            sql += " AND LIV_MES = '" + cbxMes.SelectedIndex.ToString() + "' ";

        if (txtAno.Text.Trim().Length > 0)
            sql += " AND LIV_ANO = '" + txtAno.Text.Trim() + "' ";
        


       

          
            #region Grupo
            if (txtGrupo.Text.Trim() != "" && Request.Cookies["CliID"].Value.PadLeft(2, '0').ToString().Trim() == "17")
            {
                sql += " AND GRUPO_ID = " + txtGrupo.Text;
            }
            #endregion

            


          

            #region Identificação e Medidor
            if ((txtIdentificacao.Text.Trim().Length > 0) || (txtMedidor.Text.Trim().Length > 0))
            {
                if (txtIdentificacao.Text.Trim().Length > 0)
                    sql += " AND LEI_LIGACAO LIKE '%" + txtIdentificacao.Text.Trim() + "%' ";

                if (txtMedidor.Text.Trim().Length > 0)
                    sql += " AND LEI_NUMMED LIKE '%" + txtMedidor.Text.Trim() + "%' ";

            }
        
       

      
            #endregion
            #endregion

       

        /*if (ordeR.Length > 0)
            sql += " ORDER BY " + ordeR;
        else
        {*/
        if (rbEndereço.Checked)
            sql += " ORDER BY LEI_ENDERECO,LEI_NUMERO,LIV_ANO, LIV_MES, LOC_ID,LEI_SEQ";
        else if (rbLigacao.Checked)
            sql += " ORDER BY lei_ligacao,LIV_ANO, LIV_MES, LOC_ID";
        else if (rbSequencia.Checked && Request.Cookies["CliID"].Value.PadLeft(2, '0').ToString().Trim() == "17")
            sql += " ORDER BY lei_sequencia,LIV_ANO, LIV_MES, LOC_ID";
        else if (rbNomeContribuinte.Checked)
            sql += " ORDER BY lei_consumidor,LIV_ANO, LIV_MES, LOC_ID";
        else if (rbRota.Checked)
            sql += " ORDER BY loc_id,LIV_ANO, LIV_MES,lei_seq";
        else
        {

            sql += " ORDER BY LIV_ANO, LIV_MES, ";
            if (Request.Cookies["CliID"].Value.PadLeft(2, '0').ToString().Trim() == "17" && txtGrupo.Text.Length == 0)
                sql += "GRUPO_ID, ";
            sql += "lei_seq";
        }

        //}


        DataTable dt = db.GetDt(StrCon, "WTR_LEITURAS", sql);

        return dt;

    }

   
    protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
    {
       
        if (e.CommandName == "Boleto")
        {

            int index = Convert.ToInt32(e.CommandArgument);
            //string leitura = "";
            //string msg2 = "";
            GridViewRow row = GridView1.Rows[index];
            string mes = row.Cells[2].Text;
            string ano = row.Cells[1].Text;
            string ligacao = row.Cells[3].Text;
            geraBoleto(mes, ano, ligacao);

        }
        

    }

    private void geraBoleto(String mes, String ano, String ligacao)
    {
        try
        {

           
            

            GerarPDF(@"C:\Inetpub\wwwroot\WatersyncSqlite\faturas\boleto.pdf",mes,ano,ligacao);





           
            

        }
        catch (Exception ex)
        {

        }
    }

    private void GerarPDF(string pCaminhoArquivoPDF,string mes,string ano,string ligacao)
    {
        String StrCon = carregaConexao();
        string sql = "select LEI_LIGACAO,LIV_MES,LIV_ANO,LEI_ENDERECO,LEI_NUMERO,LEI_COMPLEMENTO,LEI_BAIRRO,LEI_CONSUMO,LEI_CONSUMIDOR,LOC_ID,LEI_DATALEITURAANTERIOR,LEI_LEITURA,"+
            "LEI_ECONOMIA,LEI_NUMMED,CAT_NOME,LEI_DATA,LEI_ANTERIOR,LEI_PERIODO1,LEI_VALOR1,LEI_PERIODO2,LEI_VALOR2,LEI_PERIODO3,LEI_VALOR3,LEI_PERIODO4,LEI_VALOR4,LEI_PERIODO5,LEI_VALOR5,LEI_PERIODO6,LEI_VALOR6,LEI_MEDIA,LEI_TXLIGAGUA,LEI_ESGOTO,LEI_VENCIMENTO,LEI_VALORTOTAL " +
            "from wtr_leituras INNER JOIN WTR_MENSAGENS ON(WTR_LEITURAS.MSG_ID = WTR_MENSAGENS.MSG_ID and WTR_LEITURAS.cli_id = WTR_MENSAGENS.cli_id and WTR_MENSAGENS.emp_id = WTR_LEITURAS.emp_id) inner join wtr_categoria on (lei_categoria = wtr_categoria.cat_id ) where liv_mes = " + mes + " and liv_ano = " + ano + " and lei_ligacao like '" + ligacao + "'";
        DataTable dtLigacao = db.GetDt(StrCon, "WTR_LEITURAS", sql);




        if (dtLigacao.DefaultView.Count > 0)
        {

            File.Delete(pCaminhoArquivoPDF);

            Document documento = new Document();

            PdfWriter writer = PdfWriter.GetInstance(documento, new FileStream(pCaminhoArquivoPDF, FileMode.Append));

            try
            {




                documento.Open();
                // Paragraph p1 = new Paragraph("Nome: Lucas Alves Pereira");
                //documento.Add(p1);
                //Paragraph p = new Paragraph(new Chunk(new iTextSharp.text.pdf.draw.LineSeparator(0.0F, 100.0F, BaseColor.BLACK, Element.ALIGN_LEFT, 1)));
                //Esqueleto da fatura
                // documento.Add(p);
                PdfContentByte cb = writer.DirectContent;

                cb.MoveTo(documento.PageSize.Width - (documento.PageSize.Width/3), documento.PageSize.Height/4);
                    // x = 297.5 - y = 210.5
                cb.LineTo(documento.PageSize.Width - (documento.PageSize.Width/3), documento.PageSize.Height - 30);
                cb.Stroke();

                cb.MoveTo(documento.PageSize.Width - (documento.PageSize.Width/3), documento.PageSize.Height - 30);
                    // x = 297.5 - y = 210.5
                cb.LineTo(30, documento.PageSize.Height - 30);
                cb.Stroke();


                cb.MoveTo(30, documento.PageSize.Height/4); // x = 10 - y = 210.5
                cb.LineTo(documento.PageSize.Width - (documento.PageSize.Width/3), documento.PageSize.Height/4);
                cb.Stroke();

                cb.MoveTo(30, documento.PageSize.Height/4); // x = 10 - y = 210.5
                cb.LineTo(30, documento.PageSize.Height - 30);
                cb.Stroke();





                //ENDEREÇO
                cb.SetFontAndSize(BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.WINANSI, false), 10);
                cb.BeginText();
                cb.ShowTextAligned(Element.ALIGN_LEFT, "NOME/ENDEREÇO", 40f, 784f, 0f);
                cb.EndText();
                cb.Stroke();

                //LIGACAO
                cb.SetFontAndSize(BaseFont.CreateFont(BaseFont.TIMES_BOLD, BaseFont.WINANSI, false), 9);
                cb.BeginText();
                cb.ShowTextAligned(Element.ALIGN_LEFT, "CÓDIGO LIGAÇÃO", 160f, 800f, 0f);
                cb.EndText();
                cb.Stroke();

                //LIGACAO
                cb.SetFontAndSize(BaseFont.CreateFont(BaseFont.TIMES_BOLD, BaseFont.WINANSI, false), 9);
                cb.BeginText();
                cb.ShowTextAligned(Element.ALIGN_LEFT, dtLigacao.DefaultView[0].Row["LEI_LIGACAO"].ToString(), 173f, 788f, 0f);
                cb.EndText();
                cb.Stroke();

                cb.MoveTo(170, Convert.ToSingle(798)); // |
                cb.LineTo(Convert.ToSingle(170), Convert.ToSingle(785));
                cb.Stroke();

                cb.MoveTo(235, Convert.ToSingle(798)); // |
                cb.LineTo(235, Convert.ToSingle(785));
                cb.Stroke();

                cb.MoveTo(170, Convert.ToSingle(798)); // _
                cb.LineTo(235, Convert.ToSingle(798));
                cb.Stroke();

                cb.MoveTo(170, Convert.ToSingle(785)); // _
                cb.LineTo(235, Convert.ToSingle(785));

                


                cb.Stroke();

                //MÊS REFERÊNCIA
                cb.SetFontAndSize(BaseFont.CreateFont(BaseFont.TIMES_BOLD, BaseFont.WINANSI, false), 9);
                cb.BeginText();
                cb.ShowTextAligned(Element.ALIGN_LEFT, "MÊS REFERÊNCIA", 260f, 800f, 0f);
                cb.EndText();
                cb.Stroke();

                //LIGACAO
                cb.SetFontAndSize(BaseFont.CreateFont(BaseFont.TIMES_BOLD, BaseFont.WINANSI, false), 9);
                cb.BeginText();
                cb.ShowTextAligned(Element.ALIGN_LEFT, dtLigacao.DefaultView[0].Row["LIV_MES"].ToString() + "/" +dtLigacao.DefaultView[0].Row["LIV_ANO"].ToString(), 277f, 788f, 0f);
                cb.EndText();
                cb.Stroke();


                cb.MoveTo(270, Convert.ToSingle(798)); // |
                cb.LineTo(Convert.ToSingle(270), Convert.ToSingle(785));
                cb.Stroke();

                cb.MoveTo(330, Convert.ToSingle(798)); // |
                cb.LineTo(330, Convert.ToSingle(785));
                cb.Stroke();

                cb.MoveTo(270, Convert.ToSingle(798)); // _
                cb.LineTo(330, Convert.ToSingle(798));
                cb.Stroke();

                cb.MoveTo(270, Convert.ToSingle(785)); // _
                cb.LineTo(330, Convert.ToSingle(785));
                cb.Stroke();



                cb.MoveTo(30, Convert.ToSingle(782.75)); // 1º linha
                cb.LineTo(Convert.ToSingle(396.666656), Convert.ToSingle(782.75));
                cb.Stroke();

                //CONSUMIDOR
                cb.SetFontAndSize(BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.WINANSI, false), 9);
                cb.BeginText();
                cb.ShowTextAligned(Element.ALIGN_LEFT, dtLigacao.DefaultView[0].Row["LEI_CONSUMIDOR"].ToString() , 40f, 770f, 0f);
                cb.EndText();
                cb.Stroke();

                //ENDERECO
                cb.SetFontAndSize(BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.WINANSI, false), 9);
                cb.BeginText();
                cb.ShowTextAligned(Element.ALIGN_LEFT, dtLigacao.DefaultView[0].Row["LEI_ENDERECO"].ToString() + ", " + dtLigacao.DefaultView[0].Row["LEI_NUMERO"].ToString()+" - "+
                    dtLigacao.DefaultView[0].Row["LEI_BAIRRO"].ToString()+" "+ dtLigacao.DefaultView[0].Row["LEI_COMPLEMENTO"].ToString(), 40f, 760f, 0f);
                cb.EndText();
                cb.Stroke();




                cb.MoveTo(30, Convert.ToSingle(753.50)); // 2º linha
                cb.LineTo(Convert.ToSingle(396.666656), Convert.ToSingle(753.50));
                cb.Stroke();

                //ROTEIRO
                cb.SetFontAndSize(BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.WINANSI, false), 7);
                cb.BeginText();
                cb.ShowTextAligned(Element.ALIGN_LEFT, "ROTEIRO", 40f, 745f, 0f);
                cb.EndText();
                cb.Stroke();

                //ROTEIRO
                cb.SetFontAndSize(BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.WINANSI, false), 7);
                cb.BeginText();
                cb.ShowTextAligned(Element.ALIGN_LEFT, dtLigacao.DefaultView[0].Row["LOC_ID"].ToString(), 160f, 745f, 0f);
                cb.EndText();
                cb.Stroke();

                

                //DATA DE LEITURA
                cb.SetFontAndSize(BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.WINANSI, false), 7);
                cb.BeginText();
                cb.ShowTextAligned(Element.ALIGN_LEFT, "DATA DE LEITURA", 220f, 745f, 0f);
                cb.EndText();
                cb.Stroke();

                //DATA DE LEITURA
                cb.SetFontAndSize(BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.WINANSI, false), 7);
                cb.BeginText();
                cb.ShowTextAligned(Element.ALIGN_LEFT, dtLigacao.DefaultView[0].Row["LEI_DATA"].ToString(), 335f, 745f, 0f);
                cb.EndText();
                cb.Stroke();


                cb.MoveTo(30, Convert.ToSingle(743.75)); // 3º linha
                cb.LineTo(Convert.ToSingle(396.666656), Convert.ToSingle(743.75));
                cb.Stroke();

                //Nº DO HIDROMETRO
                cb.SetFontAndSize(BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.WINANSI, false), 7);
                cb.BeginText();
                cb.ShowTextAligned(Element.ALIGN_LEFT, "Nº DO HIDRÔMETRO", 40f, 735.25f, 0f);
                cb.EndText();
                cb.Stroke();

                //Nº DO HIDROMETRO
                cb.SetFontAndSize(BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.WINANSI, false), 7);
                cb.BeginText();
                cb.ShowTextAligned(Element.ALIGN_LEFT, dtLigacao.DefaultView[0].Row["LEI_NUMMED"].ToString(), 145f, 735.25f, 0f);
                cb.EndText();
                cb.Stroke();

                //DATA LEITURA ANTERIOR
                cb.SetFontAndSize(BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.WINANSI, false), 7);
                cb.BeginText();
                cb.ShowTextAligned(Element.ALIGN_LEFT, "DATA LEITURA ANTERIOR", 220f, 735.25f, 0f);
                cb.EndText();
                cb.Stroke();

                //DATA LEITURA ANTERIOR
                cb.SetFontAndSize(BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.WINANSI, false), 7);
                cb.BeginText();
                cb.ShowTextAligned(Element.ALIGN_LEFT, dtLigacao.DefaultView[0].Row["LEI_DATALEITURAANTERIOR"].ToString().Substring(0, 2) + "/" +
                    dtLigacao.DefaultView[0].Row["LEI_DATALEITURAANTERIOR"].ToString().Substring(2, 2) + "/" + 
                    dtLigacao.DefaultView[0].Row["LEI_DATALEITURAANTERIOR"].ToString().Substring(4, 4), 335f, 735.25f, 0f);
                cb.EndText();
                cb.Stroke();

                cb.MoveTo(30, Convert.ToSingle(734)); // 4º linha
                cb.LineTo(Convert.ToSingle(396.666656), Convert.ToSingle(734));
                cb.Stroke();

                //LIGAÇÃO
                cb.SetFontAndSize(BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.WINANSI, false), 7);
                cb.BeginText();
                cb.ShowTextAligned(Element.ALIGN_LEFT, "LIGAÇÃO", 40f, 725.5f, 0f);
                cb.EndText();
                cb.Stroke();

                //LIGAÇÃO
                cb.SetFontAndSize(BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.WINANSI, false), 7);
                cb.BeginText();
                cb.ShowTextAligned(Element.ALIGN_LEFT, dtLigacao.DefaultView[0].Row["LEI_LIGACAO"].ToString(), 145f, 725.5f, 0f);
                cb.EndText();
                cb.Stroke();

                //LEITURA ATUAL
                cb.SetFontAndSize(BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.WINANSI, false), 7);
                cb.BeginText();
                cb.ShowTextAligned(Element.ALIGN_LEFT, "LEITURA ATUAL", 220f, 725.5f, 0f);
                cb.EndText();
                cb.Stroke();

                //LEITURA ATUAL
                cb.SetFontAndSize(BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.WINANSI, false), 7);
                cb.BeginText();
                cb.ShowTextAligned(Element.ALIGN_LEFT, dtLigacao.DefaultView[0].Row["LEI_LEITURA"].ToString(), 345f, 725.5f, 0f);
                cb.EndText();
                cb.Stroke();

                cb.MoveTo(30, Convert.ToSingle(724.25)); // 5º linha
                cb.LineTo(Convert.ToSingle(396.666656), Convert.ToSingle(724.25));
                cb.Stroke();

                //CATEGORIA
                cb.SetFontAndSize(BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.WINANSI, false), 7);
                cb.BeginText();
                cb.ShowTextAligned(Element.ALIGN_LEFT, "CATEGORIA", 40f, 715.75f, 0f);
                cb.EndText();
                cb.Stroke();

                //CATEGORIA
                cb.SetFontAndSize(BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.WINANSI, false), 7);
                cb.BeginText();
                cb.ShowTextAligned(Element.ALIGN_LEFT, dtLigacao.DefaultView[0].Row["CAT_NOME"].ToString(), 140f, 715.75f, 0f);
                cb.EndText();
                cb.Stroke();

                //LEITURA ANTERIOR
                cb.SetFontAndSize(BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.WINANSI, false), 7);
                cb.BeginText();
                cb.ShowTextAligned(Element.ALIGN_LEFT, "LEITURA ANTERIOR", 220f, 715.75f, 0f);
                cb.EndText();
                cb.Stroke();

                //LEITURA ANTERIOR
                cb.SetFontAndSize(BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.WINANSI, false), 7);
                cb.BeginText();
                cb.ShowTextAligned(Element.ALIGN_LEFT, dtLigacao.DefaultView[0].Row["LEI_ANTERIOR"].ToString(), 345f, 715.75f, 0f);
                cb.EndText();
                cb.Stroke();

                cb.MoveTo(30, Convert.ToSingle(714.5)); // 6º linha
                cb.LineTo(Convert.ToSingle(396.666656), Convert.ToSingle(714.5));
                cb.Stroke();

                //Nº DE ECONOMIAS
                cb.SetFontAndSize(BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.WINANSI, false), 7);
                cb.BeginText();
                cb.ShowTextAligned(Element.ALIGN_LEFT, "Nº DE ECONOMIAS", 40f, 706f, 0f);
                cb.EndText();
                cb.Stroke();

                //Nº DE ECONOMIAS
                cb.SetFontAndSize(BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.WINANSI, false), 7);
                cb.BeginText();
                cb.ShowTextAligned(Element.ALIGN_LEFT, dtLigacao.DefaultView[0].Row["LEI_ECONOMIA"].ToString(), 155f, 706f, 0f);
                cb.EndText();
                cb.Stroke();

                //CONSUMO
                cb.SetFontAndSize(BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.WINANSI, false), 7);
                cb.BeginText();
                cb.ShowTextAligned(Element.ALIGN_LEFT, "CONSUMO", 220f, 706f, 0f);
                cb.EndText();
                cb.Stroke();

                //CONSUMO
                cb.SetFontAndSize(BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.WINANSI, false), 7);
                cb.BeginText();
                cb.ShowTextAligned(Element.ALIGN_LEFT, dtLigacao.DefaultView[0].Row["LEI_CONSUMO"].ToString(), 345f, 706f, 0f);
                cb.EndText();
                cb.Stroke();

                cb.MoveTo(30, Convert.ToSingle(704.75)); // 7º linha
                cb.LineTo(Convert.ToSingle(396.666656), Convert.ToSingle(704.75));
                cb.Stroke();

                //CONDIÇÃO DE LEITURA
                cb.SetFontAndSize(BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.WINANSI, false), 7);
                cb.BeginText();
                cb.ShowTextAligned(Element.ALIGN_LEFT, "CONDIÇÃO DE LEITURA", 40f, 696.25f, 0f);
                cb.EndText();
                cb.Stroke();

                //PREVISÃO PRÓX. LEITURA
                cb.SetFontAndSize(BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.WINANSI, false), 7);
                cb.BeginText();
                cb.ShowTextAligned(Element.ALIGN_LEFT, "PREVISÃO PRÓX. LEITURA", 220f, 696.25f, 0f);
                cb.EndText();
                cb.Stroke();


                cb.MoveTo(30, Convert.ToSingle(695)); // 8º linha
                cb.LineTo(Convert.ToSingle(396.666656), Convert.ToSingle(695));
                cb.Stroke();


                cb.MoveTo(Convert.ToSingle(121.66), Convert.ToSingle(753.50)); // 10º linha
                cb.LineTo(Convert.ToSingle(121.66), Convert.ToSingle(695));
                cb.Stroke();

                cb.MoveTo(Convert.ToSingle(213.32666), Convert.ToSingle(753.50)); // 10º linha
                cb.LineTo(Convert.ToSingle(213.32666), Convert.ToSingle(695));
                cb.Stroke();

                cb.MoveTo(Convert.ToSingle(320), Convert.ToSingle(753.50)); // 10º linha
                cb.LineTo(Convert.ToSingle(320), Convert.ToSingle(695));
                cb.Stroke();



                cb.MoveTo(30, Convert.ToSingle(680.5)); // HISTORICO 9º linha
                cb.LineTo(Convert.ToSingle(396.666656), Convert.ToSingle(680.5));
                cb.Stroke();

                cb.SetFontAndSize(BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.WINANSI, false), 9);
                cb.BeginText();
                cb.ShowTextAligned(Element.ALIGN_LEFT, "REF", 65f, 681.75f, 0f);
                cb.EndText();
                cb.Stroke();

                cb.SetFontAndSize(BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.WINANSI, false), 9);
                cb.BeginText();
                cb.ShowTextAligned(Element.ALIGN_LEFT, dtLigacao.DefaultView[0].Row["LEI_PERIODO1"].ToString(), 65f, 663.75f, 0f);
                cb.EndText();
                cb.Stroke();
                cb.SetFontAndSize(BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.WINANSI, false), 9);
                cb.BeginText();
                cb.ShowTextAligned(Element.ALIGN_LEFT, dtLigacao.DefaultView[0].Row["LEI_PERIODO2"].ToString(), 65f, 652.75f, 0f);
                cb.EndText();
                cb.Stroke();
                cb.SetFontAndSize(BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.WINANSI, false), 9);
                cb.BeginText();
                cb.ShowTextAligned(Element.ALIGN_LEFT, dtLigacao.DefaultView[0].Row["LEI_PERIODO3"].ToString(), 65f, 641.75f, 0f);
                cb.EndText();
                cb.Stroke();

                


                cb.SetFontAndSize(BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.WINANSI, false), 9);
                cb.BeginText();
                cb.ShowTextAligned(Element.ALIGN_LEFT, "CONSUMO", 115f, 681.75f, 0f);
                cb.EndText();
                cb.Stroke();

                cb.SetFontAndSize(BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.WINANSI, false), 9);
                cb.BeginText();
                cb.ShowTextAligned(Element.ALIGN_LEFT, dtLigacao.DefaultView[0].Row["LEI_VALOR1"].ToString(), 135f, 663.75f, 0f);
                cb.EndText();
                cb.Stroke();
                cb.SetFontAndSize(BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.WINANSI, false), 9);
                cb.BeginText();
                cb.ShowTextAligned(Element.ALIGN_LEFT, dtLigacao.DefaultView[0].Row["LEI_VALOR2"].ToString(), 135f, 652.75f, 0f);
                cb.EndText();
                cb.Stroke();
                cb.SetFontAndSize(BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.WINANSI, false), 9);
                cb.BeginText();
                cb.ShowTextAligned(Element.ALIGN_LEFT, dtLigacao.DefaultView[0].Row["LEI_VALOR3"].ToString(), 135f, 641.75f, 0f);
                cb.EndText();
                cb.Stroke();

                cb.SetFontAndSize(BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.WINANSI, false), 9);
                cb.BeginText();
                cb.ShowTextAligned(Element.ALIGN_LEFT, "REF", 195f, 681.75f, 0f);
                cb.EndText();
                cb.Stroke();

                cb.SetFontAndSize(BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.WINANSI, false), 9);
                cb.BeginText();
                cb.ShowTextAligned(Element.ALIGN_LEFT, dtLigacao.DefaultView[0].Row["LEI_PERIODO4"].ToString(), 195f, 663.75f, 0f);
                cb.EndText();
                cb.Stroke();
                cb.SetFontAndSize(BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.WINANSI, false), 9);
                cb.BeginText();
                cb.ShowTextAligned(Element.ALIGN_LEFT, dtLigacao.DefaultView[0].Row["LEI_PERIODO5"].ToString(), 195f, 652.75f, 0f);
                cb.EndText();
                cb.Stroke();
                cb.SetFontAndSize(BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.WINANSI, false), 9);
                cb.BeginText();
                cb.ShowTextAligned(Element.ALIGN_LEFT, dtLigacao.DefaultView[0].Row["LEI_PERIODO6"].ToString(), 195f, 641.75f, 0f);
                cb.EndText();
                cb.Stroke();


                cb.SetFontAndSize(BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.WINANSI, false), 9);
                cb.BeginText();
                cb.ShowTextAligned(Element.ALIGN_LEFT, "CONSUMO", 245f, 681.75f, 0f);
                cb.EndText();
                cb.Stroke();

                cb.SetFontAndSize(BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.WINANSI, false), 9);
                cb.BeginText();
                cb.ShowTextAligned(Element.ALIGN_LEFT, dtLigacao.DefaultView[0].Row["LEI_VALOR4"].ToString(), 265f, 663.75f, 0f);
                cb.EndText();
                cb.Stroke();
                cb.SetFontAndSize(BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.WINANSI, false), 9);
                cb.BeginText();
                cb.ShowTextAligned(Element.ALIGN_LEFT, dtLigacao.DefaultView[0].Row["LEI_VALOR5"].ToString(), 265f, 652.75f, 0f);
                cb.EndText();
                cb.Stroke();
                cb.SetFontAndSize(BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.WINANSI, false), 9);
                cb.BeginText();
                cb.ShowTextAligned(Element.ALIGN_LEFT, dtLigacao.DefaultView[0].Row["LEI_VALOR6"].ToString(), 265f, 641.75f, 0f);
                cb.EndText();
                cb.Stroke();

                cb.SetFontAndSize(BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.WINANSI, false), 9);
                cb.BeginText();
                cb.ShowTextAligned(Element.ALIGN_LEFT, "MEDIA", 330f, 681.75f, 0f);
                cb.EndText();
                cb.Stroke();

                cb.SetFontAndSize(BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.WINANSI, false), 9);
                cb.BeginText();
                cb.ShowTextAligned(Element.ALIGN_LEFT, dtLigacao.DefaultView[0].Row["LEI_MEDIA"].ToString(), 338f, 663.75f, 0f);
                cb.EndText();
                cb.Stroke();
                






                float posAux = Convert.ToSingle(55.5);

                //Descricao dos lancamentos
                cb.SetFontAndSize(BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.WINANSI, false), 12);
                cb.BeginText();
                cb.ShowTextAligned(Element.ALIGN_LEFT, "Descrição dos lançamentos", 183.333328f - 38f, 673.625f - posAux,0f);
                cb.EndText();
                cb.Stroke();

                cb.SetFontAndSize(BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.WINANSI, false), 9);
                cb.BeginText();
                cb.ShowTextAligned(Element.ALIGN_LEFT, "ÁGUA", 40f, 653.625f - posAux, 0f);
                cb.EndText();
                cb.Stroke();

                cb.SetFontAndSize(BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.WINANSI, false), 9);
                cb.BeginText();
                cb.ShowTextAligned(Element.ALIGN_LEFT, dtLigacao.DefaultView[0].Row["LEI_TXLIGAGUA"].ToString(), 200f, 653.625f - posAux, 0f);
                cb.EndText();
                cb.Stroke();

                cb.SetFontAndSize(BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.WINANSI, false), 9);
                cb.BeginText();
                cb.ShowTextAligned(Element.ALIGN_LEFT, "ESGOTO", 40f, 643.625f - posAux, 0f);
                cb.EndText();
                cb.Stroke();

                cb.SetFontAndSize(BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.WINANSI, false), 9);
                cb.BeginText();
                cb.ShowTextAligned(Element.ALIGN_LEFT, dtLigacao.DefaultView[0].Row["LEI_ESGOTO"].ToString(), 200f, 643.625f - posAux, 0f);
                cb.EndText();
                cb.Stroke();

                string sqlServicos = "select txa_mes,txa_ano,txa_ligacao,srv_id,srv_descricao,sum(txa_valor) AS VALOR,txa_tipo from wtr_tarifa inner join wtr_servico on(srv_id = txa_id) "+
                    "where txa_ligacao like '"+ligacao+"' and txa_mes = "+mes+" and txa_ano = "+ano+" group by txa_mes,txa_ano,txa_ligacao,srv_id,srv_descricao,txa_tipo";

                DataTable dtServicos = db.GetDt(StrCon, "WTR_LEITURAS", sqlServicos);
                float posLancamentos = 633.625f;
                for (int i = 0; i < dtServicos.DefaultView.Count; i++)
                {

                    if (dtServicos.DefaultView[i].Row["srv_descricao"].ToString().Trim().Length > 26)
                    {
                        cb.SetFontAndSize(BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.WINANSI, false), 9);
                        cb.BeginText();
                        cb.ShowTextAligned(Element.ALIGN_LEFT, dtServicos.DefaultView[i].Row["srv_descricao"].ToString().Trim().Substring(0, 26),
                            40f, posLancamentos - posAux, 0f);
                        cb.EndText();
                        cb.Stroke();
                        cb.SetFontAndSize(BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.WINANSI, false), 9);
                        cb.BeginText();
                        cb.ShowTextAligned(Element.ALIGN_LEFT, dtServicos.DefaultView[i].Row["VALOR"].ToString(), 200f, posLancamentos - posAux, 0f);
                        cb.EndText();
                        cb.Stroke();
                        posLancamentos -= 10;
                        cb.SetFontAndSize(BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.WINANSI, false), 9);
                        cb.BeginText();
                        cb.ShowTextAligned(Element.ALIGN_LEFT, dtServicos.DefaultView[i].Row["srv_descricao"].ToString().Trim().Substring(26, dtServicos.DefaultView[i].Row["srv_descricao"].ToString().Trim().Length - 26),
                            40f, posLancamentos - posAux, 0f);
                        cb.EndText();
                        cb.Stroke();
                        
                    }
                    else
                    {
                        cb.SetFontAndSize(BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.WINANSI, false), 9);
                        cb.BeginText();
                        cb.ShowTextAligned(Element.ALIGN_LEFT, dtServicos.DefaultView[i].Row["srv_descricao"].ToString().Trim(),
                            40f, posLancamentos - posAux, 0f);
                        cb.EndText();
                        cb.Stroke();
                        cb.SetFontAndSize(BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.WINANSI, false), 9);
                        cb.BeginText();
                        cb.ShowTextAligned(Element.ALIGN_LEFT, dtServicos.DefaultView[i].Row["VALOR"].ToString(), 200f, posLancamentos - posAux, 0f);
                        cb.EndText();
                        cb.Stroke();
                    }

                    
                    posLancamentos -= 10;
                }


                //////////VENCIMENTO E TOTAL
                cb.MoveTo(30, Convert.ToSingle(548.75) - posAux); // 10º linha
                cb.LineTo(Convert.ToSingle(396.666656), Convert.ToSingle(548.75) - posAux);
                cb.Stroke();

                cb.SetFontAndSize(BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.WINANSI, false), 12);
                cb.BeginText();
                cb.ShowTextAligned(Element.ALIGN_LEFT, "VENCIMENTO", 40f, 535f - posAux, 0f);
                cb.EndText();
                cb.Stroke();

                cb.SetFontAndSize(BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.WINANSI, false), 12);
                cb.BeginText();
                cb.ShowTextAligned(Element.ALIGN_LEFT, dtLigacao.DefaultView[0].Row["LEI_VENCIMENTO"].ToString().Substring(0, 2) + "/" +
                    dtLigacao.DefaultView[0].Row["LEI_VENCIMENTO"].ToString().Substring(2, 2) + "/" + 
                    dtLigacao.DefaultView[0].Row["LEI_VENCIMENTO"].ToString().Substring(4, 4), 142f, 534f - posAux, 0f);
                cb.EndText();
                cb.Stroke();

                cb.MoveTo(140, Convert.ToSingle(547) - posAux); // |
                cb.LineTo(Convert.ToSingle(140), Convert.ToSingle(531) - posAux);
                cb.Stroke();

                cb.MoveTo(200, Convert.ToSingle(547) - posAux); // |
                cb.LineTo(200, Convert.ToSingle(531) - posAux);
                cb.Stroke();

                cb.MoveTo(140, Convert.ToSingle(547) - posAux); // _
                cb.LineTo(200, Convert.ToSingle(547) - posAux);
                cb.Stroke();

                cb.MoveTo(140, Convert.ToSingle(531) - posAux); // _
                cb.LineTo(200, Convert.ToSingle(531) - posAux);
                cb.Stroke();





                cb.SetFontAndSize(BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.WINANSI, false), 12);
                cb.BeginText();
                cb.ShowTextAligned(Element.ALIGN_LEFT, "TOTAL", 230f, 535f - posAux, 0f);
                cb.EndText();
                cb.Stroke();

                cb.SetFontAndSize(BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.WINANSI, false), 12);
                cb.BeginText();
                cb.ShowTextAligned(Element.ALIGN_LEFT, dtLigacao.DefaultView[0].Row["LEI_VALORTOTAL"].ToString(), 287f, 534f - posAux, 0f);
                cb.EndText();
                cb.Stroke();


                cb.MoveTo(285, Convert.ToSingle(547) - posAux); // |
                cb.LineTo(Convert.ToSingle(285), Convert.ToSingle(531) - posAux);
                cb.Stroke();

                cb.MoveTo(365, Convert.ToSingle(547) - posAux); // |
                cb.LineTo(365, Convert.ToSingle(531) - posAux);
                cb.Stroke();

                cb.MoveTo(285, Convert.ToSingle(547) - posAux); // _
                cb.LineTo(365, Convert.ToSingle(547) - posAux);
                cb.Stroke();

                cb.MoveTo(285, Convert.ToSingle(531) - posAux); // _
                cb.LineTo(365, Convert.ToSingle(531) - posAux);
                cb.Stroke();

                ///////////////////////////////////////

                cb.MoveTo(30, Convert.ToSingle(529.25) - posAux); // 11º linha
                cb.LineTo(Convert.ToSingle(396.666656), Convert.ToSingle(529.25) - posAux);
                cb.Stroke();

                cb.SetFontAndSize(BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.WINANSI, false), 12);
                cb.BeginText();
                cb.ShowTextAligned(Element.ALIGN_LEFT, "***ATENÇÃO***", 183.333328f - 20f, 517f - posAux, 0f);
                cb.EndText();
                cb.Stroke();


                cb.MoveTo(30, Convert.ToSingle(470.75) - posAux); // 12º linha
                cb.LineTo(Convert.ToSingle(396.666656), Convert.ToSingle(470.75) - posAux);
                cb.Stroke();



                cb.MoveTo(30, Convert.ToSingle(412.25) - posAux); // 13º linha
                cb.LineTo(Convert.ToSingle(396.666656), Convert.ToSingle(412.25) - posAux);
                cb.Stroke();

                cb.SetFontAndSize(BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.WINANSI, false), 7);
                cb.BeginText();
                cb.ShowTextAligned(Element.ALIGN_LEFT, "Parâmetro", 40f, 405f - posAux, 0f);
                cb.EndText();
                cb.Stroke();

                cb.MoveTo(Convert.ToSingle(82.38095), Convert.ToSingle(412.25) - posAux); // |
                cb.LineTo(Convert.ToSingle(82.38095), Convert.ToSingle(383) - posAux);
                cb.Stroke();

                cb.SetFontAndSize(BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.WINANSI, false), 7);
                cb.BeginText();
                cb.ShowTextAligned(Element.ALIGN_LEFT, "Cloro", 98f, 405f - posAux, 0f);
                cb.EndText();
                cb.Stroke();


                cb.MoveTo(Convert.ToSingle(134.7619), Convert.ToSingle(412.25) - posAux); // |
                cb.LineTo(Convert.ToSingle(134.7619), Convert.ToSingle(383) - posAux);
                cb.Stroke();

                cb.SetFontAndSize(BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.WINANSI, false), 7);
                cb.BeginText();
                cb.ShowTextAligned(Element.ALIGN_LEFT, "Coliformes Totais", 136f, 405f - posAux, 0f);
                cb.EndText();
                cb.Stroke();

                cb.MoveTo(Convert.ToSingle(187.14285), Convert.ToSingle(412.25) - posAux); // |
                cb.LineTo(Convert.ToSingle(187.14285), Convert.ToSingle(383) - posAux);
                cb.Stroke();

                cb.SetFontAndSize(BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.WINANSI, false), 7);
                cb.BeginText();
                cb.ShowTextAligned(Element.ALIGN_LEFT, "Cor", 207f, 405f - posAux, 0f);
                cb.EndText();
                cb.Stroke();

                cb.MoveTo(Convert.ToSingle(239.5238), Convert.ToSingle(412.25) - posAux); // |
                cb.LineTo(Convert.ToSingle(239.5238), Convert.ToSingle(383) - posAux);
                cb.Stroke();

                cb.SetFontAndSize(BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.WINANSI, false), 7);
                cb.BeginText();
                cb.ShowTextAligned(Element.ALIGN_LEFT, "Fluoreto", 255f, 405f - posAux, 0f);
                cb.EndText();
                cb.Stroke();

                cb.MoveTo(Convert.ToSingle(291.90475), Convert.ToSingle(412.25) - posAux); // |
                cb.LineTo(Convert.ToSingle(291.90475), Convert.ToSingle(383) - posAux);
                cb.Stroke();

                cb.SetFontAndSize(BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.WINANSI, false), 7);
                cb.BeginText();
                cb.ShowTextAligned(Element.ALIGN_LEFT, "p.H", 315f, 405f - posAux, 0f);
                cb.EndText();
                cb.Stroke();

                cb.MoveTo(Convert.ToSingle(344.2857), Convert.ToSingle(412.25) - posAux); // |
                cb.LineTo(Convert.ToSingle(344.2857), Convert.ToSingle(383) - posAux);
                cb.Stroke();

                cb.SetFontAndSize(BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.WINANSI, false), 7);
                cb.BeginText();
                cb.ShowTextAligned(Element.ALIGN_LEFT, "Turbidez", 358f, 405f - posAux, 0f);
                cb.EndText();
                cb.Stroke();




                cb.MoveTo(30, Convert.ToSingle(402) - posAux); // 14º linha
                cb.LineTo(Convert.ToSingle(396.666656), Convert.ToSingle(402) - posAux);
                cb.Stroke();


                cb.MoveTo(30, Convert.ToSingle(383) - posAux); // 15º linha
                cb.LineTo(Convert.ToSingle(396.666656), Convert.ToSingle(383) - posAux);
                cb.Stroke();


                cb.SetFontAndSize(BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.WINANSI, false), 7);
                cb.BeginText();
                cb.ShowTextAligned(Element.ALIGN_LEFT,
                    "- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - ",
                    30f, 360f - posAux, 0f);
                cb.EndText();
                cb.Stroke();


                //CÓDIGO LIGAÇÃO
                cb.SetFontAndSize(BaseFont.CreateFont(BaseFont.TIMES_BOLD, BaseFont.WINANSI, false), 9);
                cb.BeginText();
                cb.ShowTextAligned(Element.ALIGN_LEFT, "CÓDIGO LIGAÇÃO", 40f, 350f - posAux, 0f);
                cb.EndText();
                cb.Stroke();

                cb.MoveTo(45, Convert.ToSingle(348) - posAux); // |
                cb.LineTo(Convert.ToSingle(45), Convert.ToSingle(330) - posAux);
                cb.Stroke();

                cb.MoveTo(120, Convert.ToSingle(348) - posAux); // |
                cb.LineTo(120, Convert.ToSingle(330) - posAux);
                cb.Stroke();

                cb.MoveTo(45, Convert.ToSingle(348) - posAux); // _
                cb.LineTo(120, Convert.ToSingle(348) - posAux);
                cb.Stroke();

                cb.MoveTo(45, Convert.ToSingle(330) - posAux); // _
                cb.LineTo(120, Convert.ToSingle(330) - posAux);
                cb.Stroke();

                //MÊS REFERÊNCIA
                cb.SetFontAndSize(BaseFont.CreateFont(BaseFont.TIMES_BOLD, BaseFont.WINANSI, false), 9);
                cb.BeginText();
                cb.ShowTextAligned(Element.ALIGN_LEFT, "MÊS REFERÊNCIA", 140f, 350f - posAux, 0f);
                cb.EndText();
                cb.Stroke();

                cb.MoveTo(155, Convert.ToSingle(348) - posAux); // |
                cb.LineTo(Convert.ToSingle(155), Convert.ToSingle(330) - posAux);
                cb.Stroke();

                cb.MoveTo(205, Convert.ToSingle(348) - posAux); // |
                cb.LineTo(205, Convert.ToSingle(330) - posAux);
                cb.Stroke();

                cb.MoveTo(155, Convert.ToSingle(348) - posAux); // _
                cb.LineTo(205, Convert.ToSingle(348) - posAux);
                cb.Stroke();

                cb.MoveTo(155, Convert.ToSingle(330) - posAux); // _
                cb.LineTo(205, Convert.ToSingle(330) - posAux);
                cb.Stroke();

                //VENCIMENTO
                cb.SetFontAndSize(BaseFont.CreateFont(BaseFont.TIMES_BOLD, BaseFont.WINANSI, false), 9);
                cb.BeginText();
                cb.ShowTextAligned(Element.ALIGN_LEFT, "VENCIMENTO", 245f, 350f - posAux, 0f);
                cb.EndText();
                cb.Stroke();

                cb.MoveTo(247, Convert.ToSingle(348) - posAux); // |
                cb.LineTo(Convert.ToSingle(247), Convert.ToSingle(330) - posAux);
                cb.Stroke();

                cb.MoveTo(305, Convert.ToSingle(348) - posAux); // |
                cb.LineTo(305, Convert.ToSingle(330) - posAux);
                cb.Stroke();

                cb.MoveTo(247, Convert.ToSingle(348) - posAux); // _
                cb.LineTo(305, Convert.ToSingle(348) - posAux);
                cb.Stroke();

                cb.MoveTo(247, Convert.ToSingle(330) - posAux); // _
                cb.LineTo(305, Convert.ToSingle(330) - posAux);
                cb.Stroke();

                //TOTAL
                cb.SetFontAndSize(BaseFont.CreateFont(BaseFont.TIMES_BOLD, BaseFont.WINANSI, false), 9);
                cb.BeginText();
                cb.ShowTextAligned(Element.ALIGN_LEFT, "TOTAL", 340f, 350f - posAux, 0f);
                cb.EndText();
                cb.Stroke();

                cb.MoveTo(326, Convert.ToSingle(348) - posAux); // |
                cb.LineTo(Convert.ToSingle(326), Convert.ToSingle(330) - posAux);
                cb.Stroke();

                cb.MoveTo(390, Convert.ToSingle(348) - posAux); // |
                cb.LineTo(390, Convert.ToSingle(330) - posAux);
                cb.Stroke();

                cb.MoveTo(326, Convert.ToSingle(348) - posAux); // _
                cb.LineTo(390, Convert.ToSingle(348) - posAux);
                cb.Stroke();

                cb.MoveTo(326, Convert.ToSingle(330) - posAux); // _
                cb.LineTo(390, Convert.ToSingle(330) - posAux);
                cb.Stroke();
                /////////////////////////////////////////////////////////////////////////////////

                Barcode128 code128 = new Barcode128();
                code128.CodeType = Barcode.CODE128;
                code128.ChecksumText = true;
                code128.GenerateChecksum = true;
                code128.Code = "82640000000318600322015040600041967042015470";
                System.Drawing.Bitmap bm =
                    new System.Drawing.Bitmap(code128.CreateDrawingImage(System.Drawing.Color.Black,
                        System.Drawing.Color.White));
                bm.Save(@"C:\Inetpub\wwwroot\WatersyncSqlite\faturas\barcod.jpg");
                System.Drawing.Image image =
                    System.Drawing.Image.FromFile(@"C:\Inetpub\wwwroot\WatersyncSqlite\faturas\barcod.jpg");
                iTextSharp.text.Image pdfImage = iTextSharp.text.Image.GetInstance(image,
                    System.Drawing.Imaging.ImageFormat.Jpeg);
                pdfImage.SetAbsolutePosition(65f, 280f - posAux);
                documento.Add(pdfImage);
                //Paragraph p4 = new Paragraph(new Chunk(new iTextSharp.text.pdf.draw.LineSeparator(0.0F, 100.0F, BaseColor.BLACK, Element.ALIGN_LEFT, 1))); // linha separadora

                //documento.Add(p4);
                /*PdfContentByte rectangulo = writer.DirectContent;

            rectangulo.SetColorFill(BaseColor.BLUE);//'dar color a rectángulo relleno
            rectangulo.Rectangle(350.0F, 580.0F, 200.0F, -100.0F);
            rectangulo.Fill();//'traza el rectangulo con relleno y sin linea de contorno

            /*cb.MoveTo(30, Convert.ToSingle(324.5) - posAux);// 17º linha
            cb.LineTo(Convert.ToSingle(396.666656), Convert.ToSingle(324.5) - posAux);
            cb.Stroke();

            cb.MoveTo(30, Convert.ToSingle(305) - posAux);// 18º linha
            cb.LineTo(Convert.ToSingle(396.666656), Convert.ToSingle(305) - posAux);
            cb.Stroke();

            cb.MoveTo(30, Convert.ToSingle(285.5) - posAux);// 19º linha
            cb.LineTo(Convert.ToSingle(396.666656), Convert.ToSingle(285.5) - posAux);
            cb.Stroke();*/

                /*cb.MoveTo(30, Convert.ToSingle(266) - posAux);// 20º linha
            cb.LineTo(Convert.ToSingle(396.666656), Convert.ToSingle(266) - posAux);
            cb.Stroke();*/

                /*//cb.MoveTo(302, documento.PageSize.Height / 2);
           */


                /* Paragraph p2 = new Paragraph("Endereço: Rua Padre Gusmões nº 311");
            documento.Add(p2);
            Paragraph p3 = new Paragraph("dados da leitura : 1020");
            documento.Add(p3);*/



                //documento.Close();

                /*Barcode128 code128 = new Barcode128();
            code128.CodeType = Barcode.CODE128;
            code128.ChecksumText = true;
            code128.GenerateChecksum = true;
            code128.Code = "82640000000318600322015040600041967042015470";
            System.Drawing.Bitmap bm = new System.Drawing.Bitmap(code128.CreateDrawingImage(System.Drawing.Color.Black, System.Drawing.Color.White));
            bm.Save(@"C:\Inetpub\wwwroot\WatersyncSqlite\faturas\barcode.jpg");
            System.Drawing.Image image = System.Drawing.Image.FromFile(@"C:\Inetpub\wwwroot\WatersyncSqlite\faturas\barcode.jpg");
            iTextSharp.text.Image pdfImage = iTextSharp.text.Image.GetInstance(image, System.Drawing.Imaging.ImageFormat.Jpeg);
            documento.Add(pdfImage);
            //Paragraph p4 = new Paragraph(new Chunk(new iTextSharp.text.pdf.draw.LineSeparator(0.0F, 100.0F, BaseColor.BLACK, Element.ALIGN_LEFT, 1))); // linha separadora

            //documento.Add(p4);*/
                documento.Close();

                writer.Close();

            }
            catch (Exception e)
            {
                //MessageBox.Show(e.StackTrace);
                if (documento.IsOpen())
                    documento.Close();
            }
        }
    }

    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {

        GridView1.PageIndex = e.NewPageIndex;
        makeLista("");
        GridView1.DataBind();
    }




    protected void ImageButton1_Click(object sender, ImageClickEventArgs e)
    {
        txtIdentificacao.Text = txtIdentificacao.Text.ToUpper();
        txtMedidor.Text = txtMedidor.Text.ToUpper();
        txtLivro.Text = txtLivro.Text.ToUpper();

        

        /*if ((txtIdentificacao.Text.Trim().Length < 1) && (txtMedidor.Text.Trim().Length < 1))
            msg("Favor informar IDENTIFICAÇÃO ou MEDIDOR.");
        else*/
        makeLista("");
    }
    protected void chkEsconder_CheckedChanged(object sender, EventArgs e)
    {
        if (chkEsconder.Checked)
            Panel6.Visible = false;
        else
        {
            Panel6.Visible = true;
        }
    }

    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        escondeColunaGrid(16, e);
        escondeColunaGrid(17, e);
        //escondelink(e);
        escondeColunaGrid(2, e);
        escondeColunaGrid(3, e);
        //corLinhaGrid(0, e);
    }
    public void escondeColunaGrid(int coluna, GridViewRowEventArgs e)
    {
        switch (e.Row.RowType)
        {
            case DataControlRowType.Header:
                e.Row.Cells[coluna].Visible = false;
                break;
            case DataControlRowType.DataRow:
                e.Row.Cells[coluna].Visible = false;
                break;
            case DataControlRowType.Footer:
                e.Row.Cells[coluna].Visible = false;
                break;
        }
    }
    public void categoria(GridViewRowEventArgs e)
    {
        switch (e.Row.RowType)
        {
            case DataControlRowType.DataRow:
                if (Convert.ToInt32(e.Row.Cells[2].Text) > 0)
                    e.Row.Cells[16].Text = "RES";
                else
                    if (Convert.ToInt32(e.Row.Cells[3].Text) > 0)
                        e.Row.Cells[16].Text = "COM";
                    else
                        if (Convert.ToInt32(e.Row.Cells[4].Text) > 0)
                            e.Row.Cells[16].Text = "IND";
                break;

        }
    }
  
    
    public void escondelinkRol(GridViewRowEventArgs e)
    {
        switch (e.Row.RowType)
        {
            case DataControlRowType.Header:
                //e.Row.Cells[coluna].Visible = false;
                break;
            case DataControlRowType.DataRow:
                e.Row.Cells[0].Controls[0].Visible = false;
                e.Row.Cells[1].Controls[0].Visible = false;
                break;
            case DataControlRowType.Footer:
                //e.Row.Cells[coluna].Visible = false;
                break;
        }
    }


    protected void btRotas_Click(object sender, EventArgs e)
    {
        //Response.Redirect("teste.aspx");

    }
    protected void PrintAllPages(object sender, EventArgs e)
    {
        btnSelect.Visible = false;
        GridView1.AllowPaging = false;
        GridView1.DataSource = carregaGrid();
        GridView1.DataBind();

        StringWriter sw = new StringWriter();
        HtmlTextWriter hw = new HtmlTextWriter(sw);
        GridView1.RenderBeginTag(hw);
        GridView1.HeaderRow.RenderControl(hw);
        foreach (GridViewRow row in GridView1.Rows)
        {
            row.RenderControl(hw);
        }
        GridView1.FooterRow.RenderControl(hw);
        GridView1.RenderEndTag(hw);

        StringBuilder sb = new StringBuilder();
        /*StringWriter tw = new StringWriter(sb);
        HtmlTextWriter hwpanel = new HtmlTextWriter(tw);
        Panel6.RenderControl(hw); 
        */
        string gridHTML = sb.ToString() + sw.ToString().Replace("\"", "'")
            .Replace(System.Environment.NewLine, "");

        sb.Append("<script type = 'text/javascript'>");
        sb.Append("window.onload = new function(){");
        sb.Append("var printWin = window.open('', '', 'left=0");
        sb.Append(",top=0,width=1000,height=600,status=0');");
        sb.Append("printWin.document.write(\"");
        sb.Append(gridHTML);
        sb.Append("\");");
        sb.Append("printWin.document.close();");
        sb.Append("printWin.focus();");
        sb.Append("printWin.print();");
        sb.Append("printWin.close();};");
        sb.Append("</script>");
        ClientScript.RegisterStartupScript(this.GetType(), "GridPrint", sb.ToString());
        GridView1.AllowPaging = true;
        GridView1.DataBind();
        btnSelect.Visible = true;
    }
    
}