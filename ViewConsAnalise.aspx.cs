﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Globalization;
using WaterSyncLite.MDL;
using WaterSyncLite.DAL;
using WaterSyncLite.Class;
using System.IO;
using System.ServiceModel.Channels;
using System.ServiceModel.Security;
using System.Text;
using AjaxControlToolkit;
using iTextSharp.text;
using Microsoft.WindowsMobile.Samples.Location;
using System.Drawing;
using System.Drawing.Imaging;
using System.Drawing.Drawing2D;

public partial class ViewConsAnalise : System.Web.UI.Page
{
    private DataTable dtLtr;
    protected void Page_Load(object sender, EventArgs e)
    {
        this.Master.LoadHeader(this.Master, Request.Path.Substring(Request.Path.LastIndexOf("/") + 1));

        EnviarArq.Attributes["onclick"] = "UploadFile(this)";
        if (!IsPostBack)
        {

            this.Form.DefaultButton = btPesquisar.UniqueID;

            String StrCon = carregaConexao();

            string sql = "";

            txtAno.Text = DateTime.Now.Year.ToString();
            cbxMes.SelectedIndex = DateTime.Now.Month;

            if (rbEndereco.Checked)
                txtEndereco_AutoCompleteExtender.ServiceMethod = "getEndereco";
            else txtEndereco_AutoCompleteExtender.ServiceMethod = "getEnderecoEntrega";


            Data db = new Data();

            #region LEITURISTAS
            dtLtr = db.GetDt(carregaConexao(), "WTR_LEIT", "SELECT LTR_ID, LTR_NOME FROM "
                + " WTR_LEITURISTAS WHERE CLI_ID = " + Request.Cookies["CliID"].Value.ToString().Trim()
                + " AND EMP_ID = " + Request.Cookies["EmpID"].Value.ToString().Trim()
                + " ORDER BY LTR_NOME");

            DataRow dtr = dtLtr.NewRow();
            dtr["LTR_NOME"] = "Selecione um leiturista...";
            dtr["LTR_ID"] = 0;
            dtLtr.Rows.Add(dtr);
            if (dtLtr != null)
            {
                ddlLtr.DataSource = dtLtr;
                ddlLtr.DataTextField = "LTR_NOME";
                ddlLtr.DataValueField = "LTR_ID";
                ddlLtr.DataBind();
                ddlLtr.SelectedValue = "0";

            }

            Session["dtLtr"] = dtLtr;
            #endregion

            #region OCORRENCIAS
            DataTable dtOcorrencia = db.GetDt(carregaConexao(), "WTR_MENSAGENS", "SELECT MSG_ID, MSG_DESCRICAO FROM "
                + " WTR_MENSAGENS WHERE CLI_ID = " + Request.Cookies["CliID"].Value.PadLeft(2, '0').ToString().Trim()
                + " AND EMP_ID = " + Request.Cookies["EmpID"].Value.ToString().Trim()
                + " ORDER BY MSG_ID");

            if(Request.Cookies["CliID"].Value.PadLeft(2, '0').ToString().Trim() == "19")
            {
                dtOcorrencia = db.GetDt(carregaConexao(), "WTR_MENSAGENS", "SELECT MSG_ID, MSG_DESCRICAO FROM "
                + " WTR_MENSAGENS WHERE CLI_ID = " + Request.Cookies["CliID"].Value.PadLeft(2, '0').ToString().Trim()
                + " AND EMP_ID = " + Request.Cookies["EmpID"].Value.ToString().Trim()
                + " AND MSG_DESCRICAO != 'desativado' ORDER BY MSG_ID");

            }

            DataRow dtrOcorrencia = dtOcorrencia.NewRow();
            dtrOcorrencia["MSG_DESCRICAO"] = "Todas";
            dtrOcorrencia["msg_id"] = 1000;
            dtOcorrencia.Rows.Add(dtrOcorrencia);
            if (dtOcorrencia != null)
            {
                cbxMsg.DataSource = dtOcorrencia;

                cbxMsg.DataTextField = "MSG_DESCRICAO";
                cbxMsg.DataValueField = "msg_id";
                cbxMsg.DataBind();
                cbxMsg.SelectedValue = "1000";
            }
            #endregion

            carregaInformacoes();


        }
    }

    protected void carregaInformacoes()
    {
        try
        {

            Panelrol.Visible = false;
            PanelEntrega.Visible = false;
            if (Request.Cookies["CliID"].Value.PadLeft(2, '0').ToString().Trim() != "17")
            {

                divGrupo.Visible = false;
                EnviarArq.Visible = false;
                btnEnviarArq.Visible = false;
                lancamentos.Visible = false;
            }

            if (Request.Cookies["CliID"].Value.PadLeft(2, '0').ToString().Trim() == "05")
            {
                ddlTipoRelatorio.Items.Add("Relatório comercial");
                ddlSituacaoLeitura.Items.Add("Alteração cadastral");
            }

            if (Request.Cookies["CliID"].Value.PadLeft(2, '0').ToString().Trim() == "18")
            {
                ddlTipoRelatorio.Items.Add("Credito de Consumo");
            }
            else
            {
                if (Request.Cookies["CliID"].Value.PadLeft(2, '0').ToString().Trim() == "17")
                {
                    ddlSituacaoLeitura.Items.Add("Acréscimo de consumo");
                    ddlSituacaoLeitura.Items.Add("Decréscimo de consumo");
                    ddlSituacaoLeitura.Items.Add("Contas retidas");
                    ddlSituacaoLeitura.Items.Add("Contas Protocoladas");
                    ddlSituacaoLeitura.Items.Add("Alteração cadastral");
                    ddlSituacaoLeitura.Items.Add("Hidro. inclinado");


                    ddlTipoRelatorio.Items.Add("Relatório detalhado");

                    ddlTipoRelatorio.Items.Add("Relatório grupos");

                    ddlTipoRelatorio.Items.Add("Rol de leituras");

                    ddlTipoRelatorio.Items.Add("Entrega de Contas");

                    if (VerificaPermissao() == "S")
                    {
                        ddlTipoRelatorio.Items.Add("Relatório de valores");
                    }

                }
            }

        }
        catch (Exception ex)
        {
            ShowMessage("Erro - " + ex.Message);

        }


    }

    protected String carregaConexao()
    {
        String StrCon = "";
        if (Request.Cookies["StrCon"] != null)
        {
            if (Request.Cookies["StrCon"].Value.ToString().Length > 0)
                StrCon = Request.Cookies["StrCon"].Value.ToString();
        }
        return StrCon.Replace('|', ';').Replace("usuario", xml.GetConexaoValue(Server.MapPath("~"), "LOCAL", "USER")).Replace("senha", xml.GetConexaoValue(Server.MapPath("~"), "LOCAL", "PASSWORD"));
    }

    protected void carregaGrid()
    {
        String StrCon = carregaConexao();
        DataTable dt = new DataTable();
        Session["latitude"] = "";
        Session["longitude"] = "";
        Session["local"] = "";


        if (Convert.ToInt32(Request.Cookies["CliID"].Value.ToString().Trim()) == 14)
        {
            Clementina clementina = new Clementina();

            clementina.AnaliseCritica(StrCon, txtLivro, cbxMsg, cbxMes, txtAno, txtEndereco, txtNumero, chkCritica, txtPercent, lblMsg, ddlSituacaoLeitura,
                ddlLtr, txtIdentificacao, txtMedidor, rbEnderecoOrdem, rbLigacao, rbSequencia, rbNomeContribuinte, rbRota, gridNormal, PanelMsg, PanelQtdRegistros, lblTOTAL);
            carregaInformacoesLivros();

        }
        else if (Convert.ToInt32(Request.Cookies["CliID"].Value.ToString().Trim()) == 5)
        {
            //gridNormal.AllowPaging = false;
            Marilia marilia = new Marilia();
            marilia.AnaliseCritica(StrCon, txtLivro, cbxMsg, cbxMes, txtAno, txtEndereco, txtNumero, chkCritica, txtPercent, lblMsg, ddlSituacaoLeitura,
                ddlLtr, txtIdentificacao, txtMedidor, rbEnderecoOrdem, rbEnderecoEntrega, rbLigacao, rbSequencia, rbNomeContribuinte, rbRota, gridNormal, PanelMsg, ddlTipoRelatorio, PanelQtdRegistros, lblTOTAL, rbData);
            carregaInformacoesLivros();
        }
        else if (Convert.ToInt32(Request.Cookies["CliID"].Value.ToString().Trim()) == 17)
        {
            SaoCaetano scs = new SaoCaetano();
            scs.AnaliseCritica(StrCon, txtLivro, cbxMsg, cbxMes, txtAno, txtEndereco, txtNumero, chkCritica, txtPercent, lblMsg, ddlSituacaoLeitura,
                ddlLtr, txtIdentificacao, txtMedidor, rbEnderecoOrdem, rbLigacao, rbSequencia, rbNomeContribuinte, rbRota, gridNormal, PanelMsg, ddlTipoRelatorio, 
                txtGrupo, RepeaterRolLeitura, Panelgrid, Panelrol, PanelDetalhado, RepeaterDetalhado, lblTOTAL, PanelQtdRegistros, ddlLancamentos, rbData, RepeaterFormaEntrega, PanelEntrega);
            carregaInformacoesLivros();
        }
        else if (Convert.ToInt32(Request.Cookies["CliID"].Value.ToString().Trim()) == 16)
        {
            Cosmopolis cosmopolis = new Cosmopolis();
            cosmopolis.AnaliseCritica(StrCon, txtLivro, cbxMsg, cbxMes, txtAno, txtEndereco, txtNumero, chkCritica, txtPercent, lblMsg, ddlSituacaoLeitura,
                 ddlLtr, txtIdentificacao, txtMedidor, rbEnderecoOrdem, rbLigacao, rbSequencia, rbNomeContribuinte, rbRota, gridNormal, PanelMsg, PanelQtdRegistros, lblTOTAL);
            carregaInformacoesLivros();
        }
        else if (Convert.ToInt32(Request.Cookies["CliID"].Value.ToString().Trim()) == 9)
        {
            Chavantes chavantes = new Chavantes();
            chavantes.AnaliseCritica(StrCon, txtLivro, cbxMsg, cbxMes, txtAno, txtEndereco, txtNumero, chkCritica, txtPercent, lblMsg, ddlSituacaoLeitura,
                 ddlLtr, txtIdentificacao, txtMedidor, rbEnderecoOrdem, rbLigacao, rbSequencia, rbNomeContribuinte, rbRota, gridNormal, PanelMsg, PanelQtdRegistros, lblTOTAL, rbData);
            carregaInformacoesLivros();
        }
        else if (Convert.ToInt32(Request.Cookies["CliID"].Value.ToString().Trim()) == 1)
        {
            Pompeia pompeia = new Pompeia();
            pompeia.AnaliseCritica(StrCon, txtLivro, cbxMsg, cbxMes, txtAno, txtEndereco, txtNumero, chkCritica, txtPercent, lblMsg, ddlSituacaoLeitura,
                ddlLtr, txtIdentificacao, txtMedidor, rbEnderecoOrdem, rbLigacao, rbSequencia, rbNomeContribuinte, rbRota, gridNormal, PanelMsg, PanelQtdRegistros, lblTOTAL);
            carregaInformacoesLivros();
        }

        else if (Convert.ToInt32(Request.Cookies["CliID"].Value.ToString().Trim()) == 7)
        {
            RioPardo RioPardo = new RioPardo();
            RioPardo.AnaliseCritica(StrCon, txtLivro, cbxMsg, cbxMes, txtAno, txtEndereco, txtNumero, chkCritica, txtPercent, lblMsg, ddlSituacaoLeitura,
                ddlLtr, txtIdentificacao, txtMedidor, rbEnderecoOrdem, rbLigacao, rbSequencia, rbNomeContribuinte, rbRota, gridNormal, PanelMsg, PanelQtdRegistros, lblTOTAL);
            carregaInformacoesLivros();
        }
        else if (Convert.ToInt32(Request.Cookies["CliID"].Value.ToString().Trim()) == 18)
        {
            Iracemapolis iracemapolis = new Iracemapolis();

            iracemapolis.AnaliseCritica(StrCon, txtLivro, cbxMsg, cbxMes, txtAno, txtEndereco, txtNumero, chkCritica, txtPercent, lblMsg, ddlSituacaoLeitura,
                ddlLtr, txtIdentificacao, txtMedidor, rbEnderecoOrdem, rbLigacao, rbSequencia, rbNomeContribuinte, rbRota, gridNormal, PanelMsg, PanelQtdRegistros, lblTOTAL, ddlTipoRelatorio);
            carregaInformacoesLivros();

        }

        if (Convert.ToInt32(Request.Cookies["CliID"].Value.ToString().Trim()) == 19)
        {
            CasaBranca casabranca = new CasaBranca();

            casabranca.AnaliseCritica(StrCon, txtLivro, cbxMsg, cbxMes, txtAno, txtEndereco, txtNumero, chkCritica, txtPercent, lblMsg, ddlSituacaoLeitura,
                ddlLtr, txtIdentificacao, txtMedidor, rbEnderecoOrdem, rbLigacao, rbSequencia, rbNomeContribuinte, rbRota, gridNormal, PanelMsg, PanelQtdRegistros, lblTOTAL);
            carregaInformacoesLivros();

        }

        if (Convert.ToInt32(Request.Cookies["CliID"].Value.ToString().Trim()) == 2)
        {
            Rinopolis rinopolis = new Rinopolis();

            rinopolis.AnaliseCritica(StrCon, txtLivro, cbxMsg, cbxMes, txtAno, txtEndereco, txtNumero, chkCritica, txtPercent, lblMsg, ddlSituacaoLeitura,
                ddlLtr, txtIdentificacao, txtMedidor, rbEnderecoOrdem, rbLigacao, rbSequencia, rbNomeContribuinte, rbRota, gridNormal, PanelMsg, PanelQtdRegistros, lblTOTAL);
            carregaInformacoesLivros();

        }
    }
    


    protected void carregaInformacoesLivros()
    {
        String StrCon = carregaConexao();
        Data db = new Data();
        String where = "";
        if ((ddlLtr.SelectedValue.ToString() != "0" || txtLivro.Text.Trim().Length > 0) && txtAno.Text.Length > 0)
        {
            where += " where ";
            if (txtAno.Text.Length > 0 && cbxMes.SelectedIndex.ToString() == "0")
            {
                if (ddlLtr.SelectedValue.ToString() != "0")
                    where += " wtr_leituras.ltr_id =  " + ddlLtr.SelectedValue.ToString();
            }
            else
            {
                if (ddlLtr.SelectedValue.ToString() != "0")
                    where += " wtr_leituras.ltr_id =  " + ddlLtr.SelectedValue.ToString() + " and";
                if (cbxMes.SelectedIndex > 0)
                    where += " wtr_leituras.liv_mes =  " + cbxMes.SelectedIndex.ToString() + " and";
                if (txtAno.Text.Length > 0)
                    where += " wtr_leituras.liv_ano =  " + txtAno.Text;
                if (txtLivro.Text.Length > 0)
                    where += "  and wtr_leituras.loc_id =  " + txtLivro.Text;

                if (Request.Cookies["CliId"].Value.ToString() == "17")
                {
                    if (txtGrupo.Text.Length > 0)
                        where += "  and wtr_leituras.grupo_id =  " + txtGrupo.Text;
                }

                where += " and wtr_leituras.cli_id = " + Request.Cookies["CliId"].Value.ToString();
                where += " and wtr_leituras.emp_id = " + Request.Cookies["EmpId"].Value.ToString();
            }

            string sql = " select wtr_leituras.ltr_id,ltr_nome ,wtr_leituras.Liv_mes ,wtr_leituras.Liv_ano ,wtr_leituras.loc_id,count(*) as Total,sum(case when lei_data != '' then 1 else 0 /*zero*/end) as Lidas," +
                            "sum(case when lei_data = '' or lei_data is NULL then 1 else 0 /*zero*/end) as 'Não Lidas', LIV_DATALEIT,liv_flag " +
                            " from wtr_leituras inner join wtr_leituristas on (wtr_leituristas.ltr_id  = wtr_leituras.ltr_id and wtr_leituras.cli_id =wtr_leituristas.cli_id and wtr_leituristas.emp_id = wtr_leituras.emp_id )" +
                            " inner join wtr_livros on (wtr_livros.ltr_id  = wtr_leituras.ltr_id and wtr_leituras.cli_id =wtr_livros.cli_id and wtr_leituras.emp_id = wtr_livros.emp_id and wtr_leituras.liv_mes = wtr_livros.liv_mes and wtr_leituras.liv_ano = wtr_livros.liv_ano and wtr_leituras.loc_id = wtr_livros.loc_id ) " +
                            where + " group by wtr_leituras.ltr_id,ltr_nome ,wtr_leituras.Liv_mes ,wtr_leituras.Liv_ano ,wtr_leituras.loc_id,LIV_DATALEIT,liv_flag ";

            sql += "order by wtr_leituras.liv_ano,wtr_leituras.liv_mes,wtr_leituras.loc_id";

            DataTable dt = db.GetDt(carregaConexao(), "WTR_LEIT", sql);

            lblDataEnvio.Visible = true;
            lblTotalLivros.Visible = true;
            lblTotalLivrosNaoLidas.Visible = true;
            lblTotalLivrosLidas.Visible = true;

            if (dt.DefaultView.Count > 0)
            {
                lblDataEnvio.Text = "Última atualização: ".PadRight(50, ' ') + dt.DefaultView[0].Row["LIV_DATALEIT"].ToString();
                lblTotalLivros.Text = "Total de Leituras:".PadRight(50, ' ') + dt.DefaultView[0].Row["Total"].ToString();
                lblTotalLivrosNaoLidas.Text = "Sem Leitura:".PadRight(50, ' ') + dt.DefaultView[0].Row["Não Lidas"].ToString();
                lblTotalLivrosLidas.Text = "Leituras realizadas:".PadRight(50, ' ') + dt.DefaultView[0].Row["Lidas"].ToString();
            }
            else
            {
                lblDataEnvio.Text = "";
                lblTotalLivros.Text = "";
                lblTotalLivrosNaoLidas.Text = "";
                lblTotalLivrosLidas.Text = "";
            }
        }
        else
        {

            lblDataEnvio.Visible = false;
            lblTotalLivros.Visible = false;
            lblTotalLivrosNaoLidas.Visible = false;
            lblTotalLivrosLidas.Visible = false;

        }

        

       
    }   


    protected void ShowMessage(string msg)
    {
        String scriptString = "<script language = JavaScript>";
        scriptString += "confirm('" + msg + "');";
        scriptString += "</script>";
        Page.ClientScript.RegisterStartupScript(Page.GetType(), "scrt", scriptString);
    }
    protected string VerificaPermissao()
    {
        try
        {
            Data db = new Data();
            string sql = "select * from wtr_user where usr_id = " + Request.Cookies["user"].Value.ToString().Trim();

            DataTable dt = db.GetDt(Conexao.StrCon, "WTR_USER", sql);

            return dt.DefaultView[0].Row["USR_ADM"].ToString().Trim();
        }
        catch { return ""; }
    }

    protected string VerificaConsultaValores()
    {
        try
        {
            Data db = new Data();
            string sql = "select consultaValores from WTR_CONFIG";

            DataTable dt = db.GetDt(Conexao.StrCon, "WTR_USER", sql);

            return dt.DefaultView[0].Row["CONSULTAVALORES"].ToString().Trim();
        }
        catch { return ""; }
    }

    protected string VerificaDiretor()
    {
        try
        {
            Data db = new Data();
            string sql = "select * from wtr_user where usr_id = " + Request.Cookies["user"].Value.ToString().Trim();

            DataTable dt = db.GetDt(Conexao.StrCon, "WTR_USER", sql);

            return dt.DefaultView[0].Row["USR_ESPECIAL"].ToString().Trim();
        }
        catch { return ""; }
    }   

    protected void txtEndereco_AutoCompleteExtender_Load(object sender, EventArgs e)
    {

    }

    [System.Web.Script.Services.ScriptMethod()]
    [System.Web.Services.WebMethod]
    public static List<string> getEndereco(string prefixText, int count)
    {


        HttpCookie objHTTPCk = HttpContext.Current.Request.Cookies.Get("user");
        List<string> nomes = new List<string>();
        String StrCon = "Data Source=.\\SQLEXPRESS;User ID=" + xml.GetConexaoValue(@"D:\Sites\tcmsac", "LOCAL", "USER") + ";Password=" + xml.GetConexaoValue(@"D:\Sites\tcmsac", "LOCAL", "PASSWORD") + ";" + "Initial Catalog=grupotcm";
        //String StrCon = "Data Source=186.202.136.10;User ID=" + xml.GetConexaoValue(@"E:\Codigo Fonte\Sites\TCM-SAC", "LOCAL", "USER") + ";Password=" + xml.GetConexaoValue(@"E:\Codigo Fonte\Sites\TCM-SAC", "LOCAL", "PASSWORD") + ";" + "Initial Catalog=grupotcm";
        Data db = new Data();
        DataTable dtUser = db.GetDt(StrCon, "wtr_user", "select * from wtr_user where usr_id = " + objHTTPCk.Value.ToString());
        if (dtUser.DefaultView.Count > 0)
        {
            SqlConnection con = new SqlConnection(dtUser.DefaultView[0].Row["USR_STRCON"].ToString().Replace("usuario", xml.GetConexaoValue(@"D:\Sites\tcmsac", "LOCAL", "USER")).Replace("senha", xml.GetConexaoValue(@"D:\Sites\tcmsac", "LOCAL", "PASSWORD")));
            //SqlConnection con = new SqlConnection(dtUser.DefaultView[0].Row["USR_STRCON"].ToString().Replace("usuario", xml.GetConexaoValue(@"E:\Codigo Fonte\Sites\TCM-SAC", "LOCAL", "USER")).Replace("senha", xml.GetConexaoValue(@"E:\Codigo Fonte\Sites\TCM-SAC", "LOCAL", "PASSWORD")));
            con.Open();
            SqlCommand cmd =
                new SqlCommand("SELECT LEI_ENDERECO from WTR_LEITURAS where LEI_ENDERECO like @Descricao and liv_ano = " +
                    HttpContext.Current.Request.Cookies.Get("ano").Value +
                    " and liv_mes = " + HttpContext.Current.Request.Cookies.Get("mes").Value +
                    " and cli_id = " +
                    HttpContext.Current.Request.Cookies.Get("cliID").Value + " and emp_id = " +
                    HttpContext.Current.Request.Cookies.Get("empID").Value + " GROUP BY LEI_ENDERECO order by LEI_ENDERECO", con);

            cmd.Parameters.AddWithValue("@Descricao", "%" + prefixText + "%");
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                nomes.Add(dt.Rows[i][0].ToString());
            }
        }
        return nomes;
    }

    [System.Web.Script.Services.ScriptMethod()]
    [System.Web.Services.WebMethod]
    public static List<string> getEnderecoEntrega(string prefixText, int count)
    {


        HttpCookie objHTTPCk = HttpContext.Current.Request.Cookies.Get("user");
        List<string> nomes = new List<string>();
        String StrCon = "Data Source=.\\SQLEXPRESS;User ID=" + xml.GetConexaoValue(@"D:\Sites\tcmsac", "LOCAL", "USER") + ";Password=" + xml.GetConexaoValue(@"D:\Sites\tcmsac", "LOCAL", "PASSWORD") + ";" + "Initial Catalog=grupotcm";
        //String StrCon = "Data Source=186.202.136.10;User ID=" + xml.GetConexaoValue(@"E:\Codigo Fonte\Sites\TCM-SAC", "LOCAL", "USER") + ";Password=" + xml.GetConexaoValue(@"E:\Codigo Fonte\Sites\TCM-SAC", "LOCAL", "PASSWORD") + ";" + "Initial Catalog=grupotcm";
        Data db = new Data();
        DataTable dtUser = db.GetDt(StrCon, "wtr_user", "select * from wtr_user where usr_id = " + objHTTPCk.Value.ToString());
        if (dtUser.DefaultView.Count > 0)
        {
            SqlConnection con = new SqlConnection(dtUser.DefaultView[0].Row["USR_STRCON"].ToString().Replace("usuario", xml.GetConexaoValue(@"D:\Sites\tcmsac", "LOCAL", "USER")).Replace("senha", xml.GetConexaoValue(@"D:\Sites\tcmsac", "LOCAL", "PASSWORD")));
            //SqlConnection con = new SqlConnection(dtUser.DefaultView[0].Row["USR_STRCON"].ToString().Replace("usuario", xml.GetConexaoValue(@"E:\Codigo Fonte\Sites\TCM-SAC", "LOCAL", "USER")).Replace("senha", xml.GetConexaoValue(@"E:\Codigo Fonte\Sites\TCM-SAC", "LOCAL", "PASSWORD")));
            con.Open();
            SqlCommand cmd =
                new SqlCommand("SELECT LEI_ENDERECOENTREGA from WTR_LEITURAS where LEI_ENDERECOENTREGA like @Descricao and liv_ano = " +
                     HttpContext.Current.Request.Cookies.Get("ano").Value +
                    " and liv_mes = " + HttpContext.Current.Request.Cookies.Get("mes").Value +
                    " and cli_id = " +
                    HttpContext.Current.Request.Cookies.Get("cliID").Value + " and emp_id = " +
                    HttpContext.Current.Request.Cookies.Get("empID").Value + " GROUP BY LEI_ENDERECOENTREGA order by LEI_ENDERECOENTREGA", con);

            cmd.Parameters.AddWithValue("@Descricao", "%" + prefixText + "%");
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                nomes.Add(dt.Rows[i][0].ToString());
            }
        }
        return nomes;
    }


    protected void rbEnderecoEntrega_CheckedChanged(object sender, EventArgs e)
    {
        if (rbEnderecoEntrega.Checked)
            txtEndereco_AutoCompleteExtender.ServiceMethod = "getEnderecoEntrega";
        else txtEndereco_AutoCompleteExtender.ServiceMethod = "getEndereco";
    }

    protected void rbEndereco_CheckedChanged(object sender, EventArgs e)
    {
        if (rbEndereco.Checked)
            txtEndereco_AutoCompleteExtender.ServiceMethod = "getEndereco";
        else txtEndereco_AutoCompleteExtender.ServiceMethod = "getEnderecoEntrega";
    }

    protected void btPesquisar_Click(object sender, EventArgs e)
    {
        PanelMsg.Visible = false;
        gridNormal.PageIndex = 0;
        carregaGrid();
        gallery.Style.Add("visibility", "hidden");
        carregaInfoPesquisa();
    }

    protected void gridNormal_RowDataBound(object sender, GridViewRowEventArgs e)
    {

        try
        {
            #region Pompeia
            if (Convert.ToInt32(Request.Cookies["CliID"].Value.ToString().Trim()) == 1)
            {
                escondeColunaGrid(17, e);
                escondeColunaGrid(18, e);
                escondeColunaGrid(19, e);
                escondeColunaGrid(20, e);
                corLinhaGrid(0, e);
                leituristas(9, e);

            }
            #endregion
            #region Clementina
            if (Convert.ToInt32(Request.Cookies["CliID"].Value.ToString().Trim()) == 14)
            {
                for (int i = 17; i < e.Row.Cells.Count; i++)
                {
                    escondeColunaGrid(i, e);
                    if (e.Row.RowType == DataControlRowType.DataRow)
                    {
                        e.Row.Cells[12].Text = buscaLeituraAnterior(e).ToString();
                        e.Row.Cells[11].Text = buscaMedia(e).ToString();
                    }

                }
                corLinhaGrid(0, e);
            }
            #endregion
            #region Iracemapolis
            if (Convert.ToInt32(Request.Cookies["CliID"].Value.ToString().Trim()) == 18)
            {
                if (ddlTipoRelatorio.SelectedValue.ToString() == "Credito de Consumo")
                {
                    escondeColunaGrid(0, e);                   
                    corLinhaGrid(0, e);
                }
                else
                {
                    for (int i = 17; i < e.Row.Cells.Count; i++)
                    {
                        escondeColunaGrid(i, e);
                        if (e.Row.RowType == DataControlRowType.DataRow)
                        {
                            e.Row.Cells[12].Text = buscaLeituraAnterior(e).ToString();
                            e.Row.Cells[11].Text = buscaMedia(e).ToString();
                        }                     

                    }
                    corLinhaGrid(0, e);

                }
            }
            #endregion
            #region Marilia
            else if (Convert.ToInt32(Request.Cookies["CliID"].Value.ToString().Trim()) == 5)
            {


                escondeColunaGrid(16, e);
                escondeColunaGrid(17, e);
                escondeColunaGrid(20, e);
                escondeColunaGrid(19, e);
                corLinhaGrid(0, e);
                leituristas(9, e);
                if (ddlTipoRelatorio.SelectedValue.ToString() == "Relatório comercial")
                {
                    RecalcularComercio(e);
                    escondeColunaGrid(8, e);
                    escondeColunaGrid(9, e);
                    escondeColunaGrid(10, e);
                    escondeColunaGrid(11, e);
                    escondeColunaGrid(12, e);
                    escondeColunaGrid(13, e);
                    escondeColunaGrid(14, e);
                    corColunaDiferenca(20, e);

                }

            }
            #endregion
            #region São Caetano
            else if (Convert.ToInt32(Request.Cookies["CliID"].Value.ToString().Trim()) == 17)
            {
                if (ddlTipoRelatorio.SelectedValue.ToString() != "Relatório grupos" && ddlTipoRelatorio.SelectedValue.ToString() != "Rol de leitura" && ddlTipoRelatorio.SelectedValue.ToString() != "Relatório de valores")
                {
                    escondeColunaGrid(16, e);
                    escondeColunaGrid(17, e);
                    escondeColunaGrid(1, e);
                    escondeColunaGrid(2, e);
                    corLinhaGrid(0, e);
                }
                else
                {
                    if (ddlTipoRelatorio.SelectedValue.ToString() == "Relatório grupos")
                    {
                        if (e.Row.RowType == DataControlRowType.DataRow)
                        {
                            e.Row.Cells[2].Text = string.Format("{0:C}", Convert.ToDecimal(e.Row.Cells[2].Text)).Replace("R$", "").Replace(" ", "");
                            e.Row.Cells[3].Text = string.Format("{0:C}", Convert.ToDecimal(e.Row.Cells[3].Text)).Replace("R$", "").Replace(" ", "").Split(',')[0];
                        }
                    }

                }



            }
            #endregion
            #region Cosmopolis
            else if (Convert.ToInt32(Request.Cookies["CliID"].Value.ToString().Trim()) == 16)
            {
                escondeColunaGrid(16, e);
                escondeColunaGrid(17, e);
                corLinhaGrid(0, e);
            }
            #endregion
            #region Chavantes
            else if (Convert.ToInt32(Request.Cookies["CliID"].Value.ToString().Trim()) == 9)
            {
                escondeColunaGrid(16, e);
                escondeColunaGrid(17, e);
                corLinhaGrid(0, e);
            }
            #endregion
            #region Rio Pardo
            if (Convert.ToInt32(Request.Cookies["CliID"].Value.ToString().Trim()) == 7)
            {
                escondeColunaGrid(17, e);
                escondeColunaGrid(18, e);
                corLinhaGrid(0, e);
            }
            #endregion
            #region Casa Branca
            if (Convert.ToInt32(Request.Cookies["CliID"].Value.ToString().Trim()) == 19)
            {
                escondeColunaGrid(18, e);
                escondeColunaGrid(19, e);
                corLinhaGrid(0, e);
            }
            #endregion
            #region Rinopolis
            if (Convert.ToInt32(Request.Cookies["CliID"].Value.ToString().Trim()) == 2)
            {
                escondeColunaGrid(16, e);
                escondeColunaGrid(17, e);
                corLinhaGrid(0, e);
            }
            #endregion


            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Cells[3].CssClass = "ligacao";
                escondelink(e);

            }

            if (ddlTipoRelatorio.SelectedValue.ToString() != "Normal")
                escondeColunaGrid(0, e);



        }
        catch (Exception ex)
        {
            lblMsg.Text += "Erro - " + ex.Message;
        }

    }

    public void escondelink(GridViewRowEventArgs e)
    {
        switch (e.Row.RowType)
        {
            case DataControlRowType.Header:
                //e.Row.Cells[coluna].Visible = false;
                break;
            case DataControlRowType.DataRow:
                string mes = e.Row.Cells[2].Text;
                string ano = e.Row.Cells[1].Text;
                string ligacao = e.Row.Cells[3].Text;

                DirectoryInfo minhaPastaImagens = new DirectoryInfo(Server.MapPath("~/Fotos/" + Request.Cookies["CliID"].Value.PadLeft(2, '0').PadLeft(2, '0').PadLeft(2, '0')));
                //DirectoryInfo minhaPastaImagens = new DirectoryInfo(Server.MapPath("~/Fotos/" + Request.Cookies["CliID"].Value.PadLeft(2, '0') + "/" + ano + "/" + mes.PadLeft(2, '0'))+"/");
                //FileInfo[] fotos = minhaPastaImagens.GetFiles("*" +mes.PadLeft(2,'0')+ano+ ligacao + ".jpg");
                if (!Directory.Exists(minhaPastaImagens.FullName))
                {
                    Directory.CreateDirectory(minhaPastaImagens.FullName);
                }

                FileInfo[] fotos = null;
                if (Request.Cookies["CliID"].Value.PadLeft(2, '0') == "05")
                    fotos = minhaPastaImagens.GetFiles("*" + ligacao.Replace("-", "") + "_*.jpg");
                //fotos = minhaPastaImagens.GetFiles("*" + ligacao.Replace("-", "") + "_"+ano+ mes.PadLeft(2,'0')+"*.jpg");
                else
                    if (Request.Cookies["CliID"].Value.PadLeft(2, '0') == "09")
                    fotos = minhaPastaImagens.GetFiles("*" + ligacao + "_" + ano + Convert.ToString(Convert.ToInt32(mes) + 1).PadLeft(2, '0') + "*.jpg");
                else
                    if (Request.Cookies["CliID"].Value.PadLeft(2, '0') == "14")
                    fotos = minhaPastaImagens.GetFiles("*" + mes.PadLeft(2, '0') + ano + ligacao + ".jpg");
                else
                    if (Request.Cookies["CliID"].Value.PadLeft(2, '0') == "14")
                    fotos = minhaPastaImagens.GetFiles("*" + mes.PadLeft(2, '0') + ano + ligacao + ".jpg");
                else
                    if (Request.Cookies["CliID"].Value.PadLeft(2, '0') == "17")
                    fotos = minhaPastaImagens.GetFiles("*" + ano + ligacao + ".jpg");
                else
                    if (Request.Cookies["CliID"].Value.PadLeft(2, '0') == "07")
                    fotos = minhaPastaImagens.GetFiles("*" + mes + ano + ligacao + ".jpg");
                else
                    if (Request.Cookies["CliID"].Value.PadLeft(2, '0') == "19")
                    fotos = minhaPastaImagens.GetFiles("*" + ano + ligacao + ".jpg");
                else
                    if (Request.Cookies["CliID"].Value.PadLeft(2, '0') == "18")
                    fotos = minhaPastaImagens.GetFiles("*" + mes.PadLeft(2, '0') + ano + ligacao + ".jpg");
                else
                    if (Request.Cookies["CliID"].Value.PadLeft(2, '0') == "02")
                    fotos = minhaPastaImagens.GetFiles("*" + mes.PadLeft(2, '0') + ano + ligacao.PadLeft(10,'0') + ".jpg");
                else
                    fotos = minhaPastaImagens.GetFiles("*" + ligacao + ".jpg");
              
                if (fotos.Length > 0)
                    e.Row.Cells[0].Controls[0].Visible = true;
                else
                {
                    e.Row.Cells[0].Controls[0].Visible = false;
                }
                //if (!(e.Row.Cells[17].Text != "" && e.Row.Cells[18].Text != "" && e.Row.Cells[17].Text != "null" && e.Row.Cells[18].Text != "null" && e.Row.Cells[17].Text.Trim() != "&nbsp;" && e.Row.Cells[18].Text.Trim() != "&nbsp;"))
                //    e.Row.Cells[1].Controls[0].Visible = false;
                break;
            case DataControlRowType.Footer:
                //e.Row.Cells[coluna].Visible = false;
                break;
        }
    }

    public void categoria(GridViewRowEventArgs e)
    {
        switch (e.Row.RowType)
        {
            case DataControlRowType.DataRow:
                if (Convert.ToInt32(e.Row.Cells[3].Text) > 0)
                    e.Row.Cells[17].Text = "RES";
                else
                    if (Convert.ToInt32(e.Row.Cells[4].Text) > 0)
                    e.Row.Cells[17].Text = "COM";
                else
                        if (Convert.ToInt32(e.Row.Cells[5].Text) > 0)
                    e.Row.Cells[17].Text = "IND";
                break;

        }
    }

    public void corLinhaGrid(int coluna, GridViewRowEventArgs e)
    {
        switch (e.Row.RowType)
        {

            case DataControlRowType.DataRow:

                if (e.Row.Cells[15].Text != "" && e.Row.Cells[11].Text.Trim() != "0" &&
                    e.Row.Cells[15].Text.Trim() != "0" && e.Row.Cells[15].Text.Trim() != "&nbsp;" &&
                    e.Row.Cells[11].Text.Trim() != "&nbsp;")
                {
                    int consumo = Convert.ToInt32(e.Row.Cells[15].Text);
                    int media = Convert.ToInt32(e.Row.Cells[11].Text);
                    float porc = 0;

                    if (txtPercent.Text.Trim() != "")
                        porc = Convert.ToSingle(txtPercent.Text.Trim());



                    int result = 0;

                    if (media > consumo)
                        result = (media * 100) / consumo;
                    else
                    {
                        result = (consumo * 100) / media;
                    }

                    if (txtPercent.Text.Trim() == "" || !chkCritica.Checked)
                    {

                        if ((result - 100) > 0)
                        {
                            if ((result - 100) >= 50 && ((result - 100) < 75))
                                e.Row.ForeColor = System.Drawing.Color.Blue;

                            else if ((result - 100) >= 75 && ((result - 100) < 100))
                                e.Row.ForeColor = System.Drawing.Color.BlueViolet;

                            else if (((result - 100) >= 100))
                                e.Row.ForeColor = System.Drawing.Color.Red;
                        }
                        else
                        {
                            if (((result - 100) <= -50) && ((result - 100) > -75))
                                e.Row.ForeColor = System.Drawing.Color.Blue;

                            else if (((result - 100) <= -75) && ((result - 100) > -100))
                                e.Row.ForeColor = System.Drawing.Color.BlueViolet;

                            else if (((result - 100) <= -100))
                                e.Row.ForeColor = System.Drawing.Color.Red;

                        }


                    }
                    else
                    {
                        if (result - 100 > porc)
                            e.Row.ForeColor = System.Drawing.Color.ForestGreen;
                        e.Row.Style.Add("font-weight", "bold");
                    }
                }
                break;

        }
    }

    public void corColunaDiferenca(int coluna, GridViewRowEventArgs e)
    {
        switch (e.Row.RowType)
        {

            case DataControlRowType.DataRow:

                e.Row.Cells[coluna].ForeColor = System.Drawing.Color.Red;
                break;

        }
    }

    public void leituristas(int coluna, GridViewRowEventArgs e)
    {
        switch (e.Row.RowType)
        {

            case DataControlRowType.DataRow:
                DataTable dt = (DataTable)Session["dtLtr"];
                DataRow[] row = dt.Select("LTR_ID = " + e.Row.Cells[19].Text + "");

                if (e.Row.Cells[20].Text != e.Row.Cells[19].Text &&
                    (e.Row.Cells[19].Text != "0" && e.Row.Cells[19].Text != "&nbsp;"))
                {
                    e.Row.Cells[coluna].ForeColor = System.Drawing.Color.LimeGreen;
                    e.Row.Cells[coluna].ToolTip = Context.Server.HtmlDecode(e.Row.Cells[coluna].Text);
                    e.Row.Cells[coluna].Text = row[0]["LTR_NOME"].ToString();
                }
                break;

        }
    }

    public void escondelinkRol(GridViewRowEventArgs e)
    {
        switch (e.Row.RowType)
        {
            case DataControlRowType.Header:
                //e.Row.Cells[coluna].Visible = false;
                break;
            case DataControlRowType.DataRow:
                e.Row.Cells[0].Controls[0].Visible = false;
                e.Row.Cells[1].Controls[0].Visible = false;
                break;
            case DataControlRowType.Footer:
                //e.Row.Cells[coluna].Visible = false;
                break;
        }
    }

    public void RecalcularComercio(GridViewRowEventArgs e)
    {
        switch (e.Row.RowType)
        {
            case DataControlRowType.Header:
                break;
            case DataControlRowType.DataRow:
                Data db = new Data();
                string ligacao = "0" + e.Row.Cells[3].Text.Split('-')[0].Substring(2);
                string mes = e.Row.Cells[2].Text;
                string ano = e.Row.Cells[1].Text;
                string rota = e.Row.Cells[5].Text;

                String sqlRes = "select * from wtr_faixa where fxa_mes = " + cbxMes.SelectedIndex + " and fxA_ano = " + txtAno.Text + " and cat_id = '01' and fxa_id = 2";
                String sqlCom = "select * from wtr_faixa where fxa_mes = " + cbxMes.SelectedIndex + " and fxA_ano = " + txtAno.Text + " and cat_id = '02' and fxa_id = 2";

                DataTable dtRes = db.GetDt(carregaConexao(),
                    "wtr_faixa", sqlRes);
                DataTable dtCom = db.GetDt(carregaConexao(),
                                    "wtr_faixa", sqlCom);

                string minRes = dtRes.DefaultView[0].Row["fxa_vragua"].ToString();
                string minCom = dtCom.DefaultView[0].Row["fxa_vragua"].ToString();

                e.Row.Cells[18].Text = CalculoGeral(ligacao, mes, ano, rota, "1").ToString();

                e.Row.Cells[19].Text = CalculoGeral(ligacao, mes, ano, rota, "2").ToString();

                e.Row.Cells[20].Text = Convert.ToString(Convert.ToDecimal(e.Row.Cells[19].Text) - Convert.ToDecimal(e.Row.Cells[18].Text));

                e.Row.Cells[21].Text = (Convert.ToSingle(e.Row.Cells[18].Text) + (Convert.ToSingle(minRes) * 1.50)).ToString("0.00");

                e.Row.Cells[22].Text = (Convert.ToSingle(e.Row.Cells[19].Text) + (Convert.ToSingle(minCom) * 1.50)).ToString("0.00");

                e.Row.Cells[23].Text = (Convert.ToDecimal(e.Row.Cells[22].Text) - Convert.ToDecimal(e.Row.Cells[21].Text)).ToString("0.00");


                break;
            case DataControlRowType.Footer:


                break;
        }
    }

    public void RecalcularResidencial(GridViewRowEventArgs e)
    {
        switch (e.Row.RowType)
        {
            case DataControlRowType.Header:
                break;
            case DataControlRowType.DataRow:
                string ligacao = e.Row.Cells[3].Text;
                string mes = e.Row.Cells[2].Text;
                string ano = e.Row.Cells[1].Text;
                string rota = e.Row.Cells[5].Text;




                break;
            case DataControlRowType.Footer:
                break;
        }
    }



    private int buscaLeituraAnterior(GridViewRowEventArgs e)
    {
        int leituraAnterior = 0;
        switch (e.Row.RowType)
        {
            case DataControlRowType.DataRow:

                for (int i = 20; i < e.Row.Cells.Count; i += 2)
                {
                    if (e.Row.Cells[i].Text != "" && e.Row.Cells[i].Text.Trim() != "&nbsp;")
                        leituraAnterior = Convert.ToInt32(e.Row.Cells[i].Text.Split('.')[0]);
                }

                break;

        }
        return leituraAnterior;

    }

    private int buscaMedia(GridViewRowEventArgs e)
    {
        double soma = 0;
        double tamanho = 0;
        int aux = 0;

        switch (e.Row.RowType)
        {
            case DataControlRowType.DataRow:

                for (int i = 41; i > 18; i -= 2)
                {

                    if (e.Row.Cells[i].Text != "" && e.Row.Cells[i].Text.Trim() != "&nbsp;" && tamanho < 6)
                    {
                        soma += Convert.ToInt32(e.Row.Cells[i].Text.Split('.')[0]);
                        tamanho += 1;
                    }
                }

                break;
        }
        if (tamanho > 0)
            return Convert.ToInt32(Math.Floor(soma / tamanho));
        else return 0;
    }

    public void escondeColunaGrid(int coluna, GridViewRowEventArgs e)
    {
        switch (e.Row.RowType)
        {
            case DataControlRowType.Header:
                e.Row.Cells[coluna].Visible = false;
                break;
            case DataControlRowType.DataRow:
                e.Row.Cells[coluna].Visible = false;
                break;
            case DataControlRowType.Footer:
                e.Row.Cells[coluna].Visible = false;
                break;
        }
    }

    private decimal CalculoGeral(string ligacao, string mes, string ano, string rota, string calculoCategoria)
    {
        try
        {
            Data db = new Data();

            string sqlLeituras = "SELECT * FROM WTR_LEITURAS WHERE LIV_MES = " + mes + " AND LIV_ANO = " + ano + " AND LEI_LIGACAO LIKE '" + ligacao + "' AND LOC_ID = " + rota + " AND CLI_ID = " + Request.Cookies["CliID"].Value.PadLeft(2, '0') + " AND EMP_ID = 1";
            DataTable dt_leituras = db.GetDt(carregaConexao(), "wtr_leituras", sqlLeituras);
            int consumo = Convert.ToInt32(dt_leituras.DefaultView[0].Row["LEI_MEDIA"]);
            string economia = dt_leituras.DefaultView[0].Row["LEI_ECONOMIA"].ToString();
            string categoria = dt_leituras.DefaultView[0].Row["LEI_CATEGORIA"].ToString().Replace("1", calculoCategoria).Replace("2", calculoCategoria);

            string dias = dt_leituras.DefaultView[0].Row["LEI_DIAS"].ToString();
            string vecto = dt_leituras.DefaultView[0].Row["LEI_VENCIMENTO"].ToString();
            string barCod = "";
            string idBanco = "0";
            string vencimento = dt_leituras.DefaultView[0].Row["LEI_VENCIMENTO"].ToString();
            string leitAtual = dt_leituras.DefaultView[0].Row["LEI_LEITURA"].ToString();
            string leituraAnterior = dt_leituras.DefaultView[0].Row["LEI_ANTERIOR"].ToString();
            string digitoligacao = dt_leituras.DefaultView[0].Row["LEI_DIGITOLIGACAO"].ToString();
            string condominioPrincipal = dt_leituras.DefaultView[0].Row["LEI_PRINCIPALCONDOMINIO"].ToString();
            string idCondominioPrincipal = dt_leituras.DefaultView[0].Row["LEI_CONDOMINIO"].ToString();
            string descontoTarifaSocial = dt_leituras.DefaultView[0].Row["LEI_DESCONTOTARIFASOCIAL"].ToString();
            string situacaoLeitura = dt_leituras.DefaultView[0].Row["LEI_SITUACAOLEIT"].ToString();
            string idSituacaoAgua = dt_leituras.DefaultView[0].Row["LEI_SITUACAOAGUA"].ToString();
            string idSituacaoEsgoto = dt_leituras.DefaultView[0].Row["LEI_SITUACAOESGOTO"].ToString();
            string idSituacaoCondominio = dt_leituras.DefaultView[0].Row["LEI_CONDOMINIO"].ToString();
            string idSituacaoAtividade = dt_leituras.DefaultView[0].Row["LEI_CODATIVIDADE"].ToString();
            string idSituacaoLancamentos = dt_leituras.DefaultView[0].Row["LEI_LANCAMENTO"].ToString();
            string idSituacaoLigacao = dt_leituras.DefaultView[0].Row["LEI_HIDROMETRACAO"].ToString();
            string consumoFixo = dt_leituras.DefaultView[0].Row["LEI_CONSUMOFIXO"].ToString();
            string minimoLigacao = dt_leituras.DefaultView[0].Row["LEI_MINIMO"].ToString();
            string debitoAuto = dt_leituras.DefaultView[0].Row["LEI_COBRANCA"].ToString();


            #region Variaveis

            int tipoConsumo = 0;
            decimal valorDesconto = 0;
            decimal vrAgua = 0; //Valor da Água
            decimal vrEsgoto = 0; //Valor do Esgoto
            decimal vrA = 0; //Cálculo temporário por economia Valor Água.
            decimal vrE = 0; //Cálculo temporário por economia Valor Esgoto.
            decimal excesso = consumo; //Excesso para calculo de faixa
            decimal totfaixa = 0; //Total de consumo por faixa
            string sql = ""; //
            string[] categorias = categoria.Split('/'); //Categoria para calculo
            int econom = 1; //Numero de economias for categoria
            bool fim = false; //Usado para finalizar Categoria
            int ecoTotal = 0; //Numero de economias TOTAL
            decimal vrExpediente = 0; //Valor da Taxa de Expediente da catetgoria
            decimal vrTarSocial = 0; //Valor de Dedução de Tarifa Social
            decimal residuo = 0; //Valor do Resíduo a ser abatido em próximas faturas
            decimal consumoFat = Convert.ToDecimal(consumo); //Valor do Consumo Faturado
            //decimal vrIsencao = 0;						//Valor de Isenção
            decimal vrAguaIsento = 0; //Valor de Isenção da Água
            decimal vrEsgotoIsento = 0; //valor de Isenção do Esgoto
            int ecoS = 1;
            //Controle de economias por Categorias. =1 é várias categorias ... !=1 é o num. de economias de uma categoria.
            string sqlServ98 = ""; //Sql para ajustar o valor de serviços 98
            decimal porc = Convert.ToDecimal(0.98);
            //Utilizado para calculo do valor da Água/Esgoto (SAMAR desconto de 2%)
            decimal imposto = 0; // Imposto para calcular caso Entidade for federal ou não
            decimal baseCalculo = 0; // Base para o cálculo caso Entidade for federal ou não
            string porcEsgoto = "0,50";
            int consumoMinimo2Categorias = 0;
            if (categorias.Length > 1)
            {
                sql = "SELECT FXA_CONSUMO FROM WTR_FAIXA WHERE (CAT_ID = " + categorias[0].ToString() + ") AND FXA_ID = 2";
                DataTable dt_minimoCat1 = db.GetDt(carregaConexao(), "wtr_leituras", sql);

                sql = "SELECT FXA_CONSUMO FROM WTR_FAIXA WHERE (CAT_ID = " + categorias[1].ToString() + ") AND FXA_ID = 2";
                DataTable dt_minimoCat2 = db.GetDt(carregaConexao(), "wtr_leituras", sql);

                consumoMinimo2Categorias = (Convert.ToInt32(dt_minimoCat1.DefaultView[0].Row[0].ToString())
                                             * Convert.ToInt32(economia.Split('/')[0].ToString()))
                                             + Convert.ToInt32(dt_minimoCat2.DefaultView[0].Row[0].ToString()) * Convert.ToInt32(economia.Split('/')[1].ToString());
            }

            #endregion

            #region CÁLCULO DE VALORES



            #endregion

            #region PARÂMETROS

            #region ###########     Busca de PARAMETROS     ###########

            string sqlParametros = "SELECT * FROM WTR_PARAMETROS "
                                   + " WHERE CLI_ID = " + dt_leituras.DefaultView[0].Row["CLI_ID"].ToString()
                                   + " AND EMP_ID = " + dt_leituras.DefaultView[0].Row["EMP_ID"].ToString()
                                   + " AND LOC_ID = " + Convert.ToInt32(dt_leituras.DefaultView[0].Row["LOC_ID"].ToString());
            DataTable dt_Param = db.GetDt(carregaConexao(), "wtr_leituras", sqlParametros);

            DataRow[] rowSituacaoAgua = dt_Param.Select("PAR_TIPO = 'A' AND PAR_ID = " + idSituacaoAgua);
            DataRow[] rowSituacaoEsgoto = dt_Param.Select("PAR_TIPO = 'E' AND PAR_ID = " + idSituacaoEsgoto);
            DataRow[] rowSituacaoCondominio = dt_Param.Select("PAR_TIPO = 'C' AND PAR_ID = " + idSituacaoCondominio);
            DataRow[] rowSituacaoAtividade = dt_Param.Select("PAR_TIPO = 'T' AND PAR_ID = " + idSituacaoAtividade);
            DataRow[] rowSituacaoLancamentos = dt_Param.Select("PAR_TIPO = 'L' AND PAR_ID = " + idSituacaoLancamentos);
            DataRow[] rowSituacaoLigacao = dt_Param.Select("PAR_TIPO = 'G' AND PAR_ID = " + idSituacaoLigacao);

            #endregion

            #endregion

            for (int cat = 0; cat < categorias.Length; cat++)
            {
                #region Busca de Faixas por categoria

                sql = "SELECT * FROM WTR_FAIXA F INNER JOIN WTR_CATEGORIA C ON (C.CAT_ID = F.CAT_ID AND C.CLI_ID = F.CLI_ID AND F.EMP_ID = C.EMP_ID) "
                      + " WHERE F.CLI_ID = " + dt_leituras.DefaultView[0].Row["CLI_ID"].ToString()
                      + " AND F.EMP_ID = " + dt_leituras.DefaultView[0].Row["EMP_ID"].ToString()
                      + " AND F.CAT_ID = " + categorias[cat].ToString()
                      + " AND F.FXA_MES = " + dt_leituras.DefaultView[0].Row["LIV_MES"].ToString()
                      + " AND F.FXA_ANO = " + dt_leituras.DefaultView[0].Row["LIV_ANO"].ToString();
                //+ " AND LOC_ID = " + Global.loc;
                DataTable dt_Calc = db.GetDt(carregaConexao(), "wtr_leituras", sql);

                #endregion

                if (dt_Calc.DefaultView.Count < 1)
                {
                    // MessageBox.Show("Problemas no cálculo da fatura! Entre em contato com a central.");
                }
                else
                {
                    #region Calculo pelo Consumo Faturado
                    econom = Convert.ToInt32(economia.Split('/')[cat].ToString());
                    if (economia.Split('/').Length <= 1)
                        ecoS = econom;
                    ecoTotal += econom;

                    if (categorias.Length > 1)
                    {
                        consumoFat = consumoFat * (Convert.ToDecimal(dt_Calc.DefaultView[0].Row["FXA_CONSUMO"].ToString()) * econom / consumoMinimo2Categorias);
                    }

                    fim = false;
                    consumoFat = consumoFat / Convert.ToInt32(econom);
                    excesso = consumoFat;
                    vrA = 0;
                    vrE = 0;


                    for (int reg = 0; reg < dt_Calc.DefaultView.Count; reg++)
                    {
                        if (consumoFat < (Convert.ToDecimal(dt_Calc.DefaultView[reg].Row["FXA_CONSUMO"])))
                        {
                            #region Dentro da Faixa
                            if (reg == 0)
                            {
                                vrA += Convert.ToDecimal(dt_Calc.DefaultView[reg].Row["FXA_VRAGUA"]);
                                totfaixa = consumoFat;
                            }
                            else
                            {
                                vrA += Convert.ToDecimal(dt_Calc.DefaultView[reg].Row["FXA_VRAGUA"]) * excesso;
                                totfaixa = excesso;
                            }
                            #endregion

                            fim = true;
                        }
                        else
                        {
                            #region Faixas abaixo
                            excesso = consumoFat - (Convert.ToDecimal(dt_Calc.DefaultView[reg].Row["FXA_CONSUMO"]));

                            if (reg == 0)
                            {
                                vrA += Convert.ToDecimal(dt_Calc.DefaultView[reg].Row["FXA_VRAGUA"]);
                                totfaixa = Convert.ToDecimal(dt_Calc.DefaultView[reg].Row["FXA_CONSUMO"]);
                            }
                            else
                            {
                                totfaixa = (Convert.ToDecimal(dt_Calc.DefaultView[reg].Row["FXA_CONSUMO"])) - (Convert.ToDecimal(dt_Calc.DefaultView[reg - 1].Row["FXA_CONSUMO"]));
                                vrA += Convert.ToDecimal(dt_Calc.DefaultView[reg].Row["FXA_VRAGUA"]) * totfaixa;

                            }
                            #endregion
                        }


                        if (fim)
                            reg = dt_Calc.DefaultView.Count;
                    }

                    if (consumoFat < (Convert.ToDecimal(dt_Calc.DefaultView[0].Row["FXA_CONSUMO"])))
                        consumoFat = (Convert.ToDecimal(dt_Calc.DefaultView[0].Row["FXA_CONSUMO"]));
                    vrAgua += vrA * econom;
                    vrEsgoto = Convert.ToDecimal(vrAgua) * Convert.ToDecimal(porcEsgoto);

                    //if (txtTipo.Text.ToUpper() == "T" || txtTipo.Text.ToUpper() == "A")
                    //{
                    //    if (Convert.ToInt32(categoria) != 5)
                    //        vrAgua += vrA * econom;
                    //    else vrAgua += vrA;
                    //}
                    //if (txtTipo.Text.ToUpper() == "T" || txtTipo.Text.ToUpper() == "E")
                    //{
                    //    vrEsgoto += Convert.ToDecimal(vrAgua) * Convert.ToDecimal(porcEsgoto);
                    //}

                    #endregion
                }
            }

            string data = DateTime.Now.ToString("MM/dd/yyyy");
            string hora = DateTime.Now.ToString("HH:mm:ss");


            vrAgua = Convert.ToDecimal(Convert.ToDecimal(vrAgua).ToString("0.00"));
            vrEsgoto = Convert.ToDecimal(Convert.ToDecimal(vrEsgoto).ToString("0.00"));


            if (Convert.ToInt32(descontoTarifaSocial) > 0)
            {
                descontoTarifaSocial = Convert.ToInt32(descontoTarifaSocial).ToString();
                descontoTarifaSocial = Convert.ToDecimal(descontoTarifaSocial.Substring(0, 2) + "," + descontoTarifaSocial.Substring(2, 4)).ToString();

                vrAgua = vrAgua * (Convert.ToDecimal(descontoTarifaSocial) / 100);
                vrEsgoto = vrEsgoto * (Convert.ToDecimal(descontoTarifaSocial) / 100);

            }


            //PARAMETROS ESGOTO
            if (rowSituacaoAgua.Length > 0)
            {
                if (rowSituacaoAgua[0]["PAR_ID"].ToString().Trim() == "8") // Sem ligacao de agua
                    vrAgua = 0;
            }
            //PARAMETROS ESGOTO
            if (rowSituacaoEsgoto.Length > 0)
            {
                if (rowSituacaoEsgoto[0]["PAR_ISENTOESGOTO"].ToString().Trim() == "S")
                    vrEsgoto = 0;
            }

            //PARAMETROS LANCAMENTOS
            if (rowSituacaoLancamentos.Length > 0)
            {

                //PARAMETROS LANCAMENTOS
                if (rowSituacaoLancamentos[0]["PAR_COBRAAGUA"].ToString().Trim() == "S")
                    vrAgua = 0;
                if (rowSituacaoLancamentos[0]["PAR_COBRAESGOTO"].ToString().Trim() == "S")
                    vrEsgoto = 0;
                //if (rowSituacaoLancamentos[0]["PAR_COBRASERV"].ToString().Trim() == "S")
                //    somaGeral = 0;
            }

            //PARAMETROS ATIVIDADE
            if (rowSituacaoAtividade.Length > 0)
            {
                if (rowSituacaoAtividade[0]["PAR_DESCONTO"].ToString().Trim() == "E")
                {
                    int desconto = Convert.ToInt32(rowSituacaoAtividade[0]["PAR_DESCONTOREAL"].ToString());
                    decimal descontoreal =
                        Convert.ToDecimal(desconto.ToString().Substring(0, desconto.ToString().Length - 4) + "," +
                                          desconto.ToString().Substring(desconto.ToString().Length - 4, 4));
                    valorDesconto = vrEsgoto - (vrEsgoto * (descontoreal / 100));
                    //vrEsgoto = vrEsgoto*(descontoreal/100);
                    //string insert =
                    //    "insert into wtr_tarifa (cli_id,emp_id,cid_id,txa_ligacao,txa_mes,txa_ano,txa_id,loc_id,txa_valor,txa_sinal,txa_referencia,txa_seq," +
                    //    "txa_refmulta,txa_tipo) values (5," + Global.emp + "," + Global.cid + ",'" + ligacao + "'," +
                    //    Global.liv + "," + Global.ano + "," +
                    //    rowSituacaoAtividade[0]["PAR_CODSERV"].ToString() + "," + Global.loc + ","+ aux.ToString().Replace(',','.') +",'-',' ',0,' ','S')";

                    //bool check = db.Exe(insert, "wtr_tarifa");
                    //if (!check)
                    //{
                    //    string insertServ = "update  wtr_tarifa set txa_valor = " + aux.ToString().Replace(',', '.') + " where txa_ligacao = '" + ligacao + "' and "
                    //        + " txa_mes = " + Global.liv + " and txa_ano = " + Global.ano + " and loc_id = " + Global.loc + " and txa_id = " + rowSituacaoAtividade[0]["PAR_CODSERV"].ToString().Trim();
                    //    db.Exe(insertServ, "wtr_tarifa");   
                    //}

                }
                else if (rowSituacaoAtividade[0]["PAR_COBRAESGOTO"].ToString().Trim() == "A")
                {
                    int desconto = Convert.ToInt32(rowSituacaoAtividade[0]["PAR_DESCONTOREAL"].ToString());
                    decimal descontoreal =
                        Convert.ToDecimal(desconto.ToString().Substring(0, desconto.ToString().Length - 4) + "," +
                                          desconto.ToString().Substring(desconto.ToString().Length - 4, 4));

                    valorDesconto = vrAgua - (vrAgua * (descontoreal / 100));

                }
            }



            //VALOR TOTAL DA FATURA
            decimal total = Convert.ToDecimal(vrAgua.ToString("0.00")) + Convert.ToDecimal(vrEsgoto.ToString("0.00")) - valorDesconto;
            decimal somaCondominio = 0;
            Decimal soma = 0, somaGeral = 0;

            try
            {
                //VALOR CONDOMINIOS
                if (condominioPrincipal == "S")
                {

                    string sqlCondominios =
                        "select SUM(convert(decimal(10,2),LEI_VALORTOTAL)) as soma from wtr_leituras  where LEI_CONDOMINIO = " + idCondominioPrincipal + " and LEI_PRINCIPALCONDOMINIO = 'N' and LIV_MES = " + dt_leituras.DefaultView[0].Row["LIV_MES"].ToString() + " and liv_ano = " + dt_leituras.DefaultView[0].Row["LIV_ANO"].ToString() + " and loc_id = " + dt_leituras.DefaultView[0].Row["LOC_ID"].ToString();
                    DataTable dtCondominios = db.GetDt(carregaConexao(), "wtr_leituras", sqlCondominios);


                    if (dtCondominios.DefaultView[0].Row["soma"].ToString() != "")
                    {
                        somaCondominio = Convert.ToDecimal(dtCondominios.DefaultView[0].Row["soma"].ToString());
                        //total += somaCondominio;

                        string insertServ = "update  wtr_tarifa set txa_valor = " + somaCondominio.ToString().Replace(',', '.') + " where txa_ligacao = '" + ligacao + "' and "
                            + " txa_mes = " + dt_leituras.DefaultView[0].Row["LIV_MES"].ToString() + " and txa_ano = " + dt_leituras.DefaultView[0].Row["LIV_ANO"].ToString() + " and loc_id = " + dt_leituras.DefaultView[0].Row["LOC_ID"].ToString() + " and txa_id = " + rowSituacaoCondominio[0]["PAR_CDMTAXA"].ToString().Trim();
                        db.Exe(carregaConexao(), "wtr_leituras", insertServ);

                    }
                }

            }
            catch (Exception)
            {
                // MessageBox.Show("Verifique se todos os poços deste condomímio foram lidos.", "Atenção");

            }

            // VALORES EXTRAS -- TARIFAS - Servicos
            string sqlLanc = "select * from wtr_tarifa t where  t.txa_ligacao = " + ligacao + " and t.txa_mes = " + dt_leituras.DefaultView[0].Row["LIV_MES"].ToString()
                    + " and t.txa_ano = " + dt_leituras.DefaultView[0].Row["LIV_ANO"].ToString() + " and txa_tipo = 'S'";
            DataTable dtLancto = db.GetDt(carregaConexao(), "wtr_leituras", sqlLanc);
            string negativo = "N";
            if (dtLancto.DefaultView.Count > 0)
            {

                for (int aux = 0; aux < dtLancto.DefaultView.Count; aux++)
                {
                    if (dtLancto.DefaultView[aux].Row["txa_sinal"].ToString() == "-")
                    {
                        //negativo = "S";
                        somaGeral -= Convert.ToDecimal(dtLancto.DefaultView[aux].Row["txa_valor"].ToString());
                    }
                    else somaGeral += Convert.ToDecimal(dtLancto.DefaultView[aux].Row["txa_valor"].ToString());

                    //somaGeral += Convert.ToDecimal(dtLancto.DefaultView[aux].Row["txa_valor"].ToString());
                }
            }

            // VALORES EXTRAS -- TARIFAS - Servicos
            string sqlTaxa = "select * from wtr_tarifa t where  t.txa_ligacao = " + ligacao +
                    " and t.txa_mes = " + dt_leituras.DefaultView[0].Row["LIV_MES"].ToString()
                    + " and t.txa_ano = " + dt_leituras.DefaultView[0].Row["LIV_ANO"].ToString() + " and txa_tipo = 'T'";
            DataTable dtTaxa = db.GetDt(carregaConexao(), "wtr_leituras", sqlTaxa);


            if (dtTaxa.DefaultView.Count > 0)
            {

                for (int aux = 0; aux < dtTaxa.DefaultView.Count; aux++)
                {
                    if (dtTaxa.DefaultView[aux].Row["txa_sinal"].ToString() == "-")
                    {
                        //negativo = "S";
                        somaGeral -= Convert.ToDecimal(dtTaxa.DefaultView[aux].Row["txa_valor"].ToString());
                    }
                    else somaGeral += Convert.ToDecimal(dtTaxa.DefaultView[aux].Row["txa_valor"].ToString());

                    //somaGeral += Convert.ToDecimal(dtLancto.DefaultView[aux].Row["txa_valor"].ToString());
                }
            }

            total += somaGeral;

            if (total < 0)
                total = 0;


            DateTime dtA = Convert.ToDateTime(
                vecto.Substring(0, 2) + "/" +
                vecto.Substring(2, 2) + "/" +
                vecto.Substring(4, 4));
            DateTime dtB = Convert.ToDateTime("07/10/1997");
            string fator = Convert.ToString(dtA - dtB).Split('.')[0];

            string totBarCod = total.ToString("0.00").Replace(",", "").Replace(".", "").Replace("-", "").PadLeft(10, '0');
            string tmpBarCode = "";



            string lineCod = "";
            string banco = idBanco.PadLeft(3, '0');
            string tipMoeda = "9";						//Tipo da moeda - 9=Real

            #region Inf. Bancárias (Cod.Barras)
            string idProd = "8";
            string idSeg = "2";
            string idVr = "6";
            string dvGeral = "";
            string idOrg = "0032";
            string ligacaoaux = Convert.ToInt32(ligacao).ToString();
            ligacaoaux = ligacaoaux.PadLeft(8, '0');
            //string venc = Global.ano.ToString().PadLeft(4, '0') + Global.liv.ToString().PadLeft(2, '0')+vencimento.PadLeft(2, '0');
            string venc = vencimento.Substring(4, 4) + vencimento.ToString().Substring(2, 2).PadLeft(2, '0') + vencimento.Substring(0, 2);
            string constante = "901";

            string line = "";

            //parcela = (Convert.ToDouble(parcela)).ToString();//remover zeros a esquerda
            line = idProd + idSeg + idVr + dvGeral + total.ToString("0.00").Replace(",", "").Replace(".", "").Replace("-", "").PadLeft(11, '0') + idOrg + venc + ligacaoaux + dt_leituras.DefaultView[0].Row["LIV_MES"].ToString().ToString().PadLeft(2, '0') + dt_leituras.DefaultView[0].Row["LIV_ANO"].ToString().PadLeft(4, '0') + digitoligacao.PadLeft(2, '0') + "0";
            //line = "8260000000400100322013121800000456112013850";

            if (debitoAuto.Trim() == "1")
            {
                barCod = "DÉBITO AUTOMÁTICO";
            }
            else
            {
                barCod = CalcDVGeral(line);
                lineCod = CalculaDigito(barCod);
            }
            #endregion


            //numArrecada = "";
            //numArrecada = numArrecada.Replace("-", "").Replace(" ", "");
            //numArrecada = numArrecada.PadLeft(13, '0');

            ////numArrecada = numArrecada.Substring(0, 13);	//Usar apenas os 13 caracteres sem os 3 digitos finais

            //if (numArrecada.Length == 0)
            //    numArrecada = "0000000000000";	//DAE Tupi Paulista

            //codCedente = "";
            //codCedente = codCedente.Replace(" ", "");
            //if (codCedente.Length == 0)
            //    codCedente = "2193450";				//DAE Tupi Paulista



            //if (tmpBarCode.Length > 0)
            //    barCod = tmpBarCode;		//Devolve o conteúdo Débito automático
            //Padovani - 20Mai2010
            if (consumo < 0)
                consumo = 0;
            if (consumoFat < 0)
                consumoFat = 0;
            //if (consumoFat > consumo)
            //    consumoFat = consumo;

            return total;

        }
        catch (Exception ex)
        {
            return 0;
            //MessageBox.Show("Informação: " + ex.Message + ". Entre em contato com a central.");
        }
        finally
        {
            //m_con.Close();
        }
    }

    private string CalculaDigito(string barcode)
    {
        try
        {
            int conta = 0;				// usado para contas
            int num = 0;				// número a ser trabalhado
            int digc = 0;				// Digitos por grupo
            string aux;					// auxiliar para cálculo
            int[] grupo = new int[4];	// Grupo de Dígitos
            int grup = 0;				// índice do Grupo.

            barcode = barcode.Replace("A", "");

            for (int dig = 0; dig < barcode.Length; dig++)
            {
                num = Convert.ToInt32(barcode.Substring(dig, 1));
                if (digc % 2 == 0)
                {
                    aux = Convert.ToString(num * 2);
                    conta += Convert.ToInt32(aux.Substring(0, 1));

                    if (aux.Length > 1)
                        conta += Convert.ToInt32(aux.Substring(1, 1));
                }
                else
                {
                    aux = Convert.ToString(num * 1);
                    conta += Convert.ToInt32(aux.Substring(0, 1));
                }

                digc++;

                if ((digc % 11 == 0) & (digc > 0))
                {
                    conta = conta % 10;

                    if (conta == 0)
                        grupo[grup] = conta;
                    else
                    {
                        conta = 10 - conta;
                        grupo[grup] = conta;
                    }

                    grup++;
                    conta = 0;
                    digc = 0;
                }
            }

            return barcode.Substring(0, 11) + "-" + grupo[0].ToString() + "  "
                    + barcode.Substring(11, 11) + "-" + grupo[1].ToString() + "  "
                    + barcode.Substring(22, 11) + "-" + grupo[2].ToString() + "  "
                    + barcode.Substring(33, 11) + "-" + grupo[3].ToString();
        }
        catch (Exception ex)
        {
            lblMsg.Text += "Informação: " + ex.Message;
            return barcode;
        }
    }

    private string CalcDVGeral(string line)
    {
        try
        {
            string dv = "0";
            string prod = "";
            int aux = 1;

            if (line.Length < 43)
                line = line.PadRight(43, '0');

            for (int x = 0; x < 43; x++)
            {
                if (aux == 1)
                    aux = 2;
                else
                    aux = 1;

                prod += (Convert.ToInt32(line.Substring(x, 1)) * aux).ToString();//Convert.ToInt32(sequencia.Substring(x, 1))).ToString();
            }
            for (int x = 0; x < prod.Length; x++)
            {
                dv = (Convert.ToInt32(dv) + Convert.ToInt32(prod.Substring(x, 1))).ToString();
            }
            dv = (Convert.ToInt32(dv) % 10).ToString();
            if (Convert.ToInt32(dv) != 0)
                dv = (10 - (Convert.ToInt32(dv))).ToString();
            line = line.Substring(0, 3) + dv + line.Substring(3, 40);
            return line;
        }
        catch (Exception ex)
        {
            lblMsg.Text += "Informação: " + ex.Message;
            return "";
        }
    }

    protected void gridNormal_RowCommand(object sender, GridViewCommandEventArgs e) //Carrega a foto no Grid
    {
        if (e.CommandName == "Foto")
        {

            Page.ClientScript.RegisterStartupScript(GetType(), "MyKey", "jQuery('#myModal').modal('show');", true);


            int index = Convert.ToInt32(e.CommandArgument);
            //string leitura = "";
            //string msg2 = "";
            GridViewRow row = gridNormal.Rows[index];
            string mes = row.Cells[2].Text;
            string ano = row.Cells[1].Text;
            string ligacao = row.Cells[3].Text;

            DirectoryInfo minhaPastaImagens = new DirectoryInfo(Server.MapPath("~/Fotos/" + Request.Cookies["CliID"].Value.PadLeft(2, '0').PadLeft(2, '0').PadLeft(2, '0')));
            //DirectoryInfo minhaPastaImagens = new DirectoryInfo(Server.MapPath("~/Fotos/" + Request.Cookies["CliID"].Value.PadLeft(2, '0').PadLeft(2, '0').PadLeft(2, '0') + "/" + ano + "/" + mes.PadLeft(2, '0')));


            //galeriaDataList.DataSource = minhaPastaImagens.GetFiles("*" + Session["mesfotos"].ToString().PadLeft(2,'0') + Session["anofotos"] + Session["fotos"].ToString() + ".jpg");

            if (Convert.ToInt32(Request.Cookies["CliID"].Value.PadLeft(2, '0')) == 5) {
                galeriaDataList.DataSource = minhaPastaImagens.GetFiles("*" + ligacao.Replace("-", "") + "_*.jpg");
                // galeriaDataList.DataSource = minhaPastaImagens.GetFiles("*" + ligacao.Replace("-", "") + "_" + ano + Convert.ToString((Convert.ToUInt32(mes)-1)).PadLeft(2, '0') + "*.jpg");
            }
            else
            if (Convert.ToInt32(Request.Cookies["CliID"].Value.PadLeft(2, '0')) == 09)                            
                galeriaDataList.DataSource = minhaPastaImagens.GetFiles("*" + ligacao + "_" + ano + Convert.ToString(Convert.ToInt32(mes) + 1).PadLeft(2, '0') + "*.jpg");
            else
                if (Convert.ToInt32(Request.Cookies["CliID"].Value.PadLeft(2, '0')) == 7)
                galeriaDataList.DataSource = minhaPastaImagens.GetFiles("*" + mes + ano + ligacao + ".jpg");
            else
                if (Convert.ToInt32(Request.Cookies["CliID"].Value.PadLeft(2, '0')) == 19)
                galeriaDataList.DataSource = minhaPastaImagens.GetFiles("*" +ano+ ligacao + ".jpg");
            else
                if (Convert.ToInt32(Request.Cookies["CliID"].Value.PadLeft(2, '0')) == 18)
                galeriaDataList.DataSource = minhaPastaImagens.GetFiles("*" +mes.PadLeft(2,'0')+ ano + ligacao + ".jpg");
            else
                if (Convert.ToInt32(Request.Cookies["CliID"].Value.PadLeft(2, '0')) ==02)
                galeriaDataList.DataSource = minhaPastaImagens. GetFiles("*" + mes.PadLeft(2, '0') + ano + ligacao.PadLeft(10,'0') + ".jpg");
           
            else
            {
                galeriaDataList.DataSource = minhaPastaImagens.GetFiles("*" + ligacao + "*.jpg").OrderByDescending(p => p.Name.Substring(8, 4)).ThenByDescending(p => p.Name.Substring(6, 2));
            }
            galeriaDataList.DataBind();

            gallery.Style.Add("visibility", "visible");

            lblLigacaoSel.Text = ligacao;



            //buscaFoto(mes, ano, ligacao);
        }
    }

    protected void gridNormal_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {        
        gridNormal.PageIndex = e.NewPageIndex;
        carregaGrid();
        gridNormal.DataBind();
    }


    public override void VerifyRenderingInServerForm(Control control)
    {

    }



    protected void imgExcel_Click(object sender, ImageClickEventArgs e)
    {
        Response.ClearContent();
        Response.Buffer = true;
        Response.AddHeader("content-disposition", string.Format("attachment; filename={0}", "GOMSNO747.xls"));
        Response.ContentType = "application/ms-excel";
        StringWriter sw = new StringWriter();
        HtmlTextWriter htw = new HtmlTextWriter(sw);
        gridNormal.AllowPaging = false;
        carregaGrid();
        //Change the Header Row back to white color
        gridNormal.HeaderRow.Style.Add("background-color", "#FFFFFF");
        //Applying stlye to gridview header cells
        for (int i = 0; i < gridNormal.HeaderRow.Cells.Count; i++)
        {
            gridNormal.HeaderRow.Cells[i].Style.Add("background-color", "#507CD1");
        }
        int j = 1;
        //This loop is used to apply stlye to cells based on particular row
        foreach (GridViewRow gvrow in gridNormal.Rows)
        {
            //gvrow.BackColor = Color.White;
            if (j <= gridNormal.Rows.Count)
            {
                if (j % 2 != 0)
                {
                    for (int k = 0; k < gvrow.Cells.Count; k++)
                    {
                        gvrow.Cells[k].Style.Add("background-color", "#EFF3FB");
                    }
                }
            }
            j++;
        }
        gridNormal.RenderControl(htw);
        Response.Write(sw.ToString());
        Response.End();
    }
    protected void btnEnviarArq_Click(object sender, EventArgs e)
    {

        if (EnviarArq.HasFile)
            try
            {
                if (EnviarArq.FileName.ToString().Substring(EnviarArq.FileName.ToString().Length - 3, 3).Equals("jpg") ||
                    EnviarArq.FileName.ToString().Substring(EnviarArq.FileName.ToString().Length - 4, 4).Equals("jpeg"))
                {
                    string localImport = Server.MapPath("~/Fotos/" + Request.Cookies["CliID"].Value.ToString().PadLeft(2, '0'));


                    string caminho = localImport + "\\";
                    string nomeArquivo =
                        DateTime.Now.Hour.ToString().PadLeft(2, '0') +
                        DateTime.Now.Minute.ToString().PadLeft(2, '0') +
                        DateTime.Now.Second.ToString().PadLeft(2, '0') +
                        DateTime.Now.Day.ToString().PadLeft(2, '0') +
                        DateTime.Now.Month.ToString().PadLeft(2, '0') +
                        DateTime.Now.Year.ToString().PadLeft(4, '0') +
                        lblLigacaoSel.Text + ".jpg";
                    string caminhoCompleto = caminho + nomeArquivo;
                    //Response.Write(caminhoCompleto + " MapPath = " + Server.MapPath("~") + @"\Configs\" + Request.Cookies["CliID"].Value.ToString().PadLeft(2, '0') + @"\" + @"Config.xml");
                    EnviarArq.PostedFile.SaveAs(caminhoCompleto);
                    ajustaFoto(caminhoCompleto);

                    DirectoryInfo minhaPastaImagens = new DirectoryInfo(Server.MapPath("~/Fotos/" + Request.Cookies["CliID"].Value.PadLeft(2, '0')));
                    //galeriaDataList.DataSource = minhaPastaImagens.GetFiles("*" + Session["mesfotos"].ToString().PadLeft(2,'0') + Session["anofotos"] + Session["fotos"].ToString() + ".jpg");

                    if (Convert.ToInt32(Request.Cookies["CliID"].Value.PadLeft(2, '0')) == 5)
                        galeriaDataList.DataSource = minhaPastaImagens.GetFiles("*" + lblLigacaoSel.Text.Split('-')[0] + "*_*.jpg");                    
                    else
                    {
                        galeriaDataList.DataSource = minhaPastaImagens.GetFiles("*" + lblLigacaoSel.Text + "*.jpg");
                    }
                    galeriaDataList.DataBind();

                    gallery.Style.Add("visibility", "visible");
                    PanelMsg.Visible = false;
                }
                else
                {
                    PanelMsg.Visible = true;
                    lblMsg.Text = "Por favor, escolha um arquivo no formato .jpg";
                }



            }
            catch (Exception ex)
            {

                lblMsg.Text = "Foto não enviada. Erro : " + ex.Message;
            }
        else
        {

            lblMsg.Text = "Por favor selecione uma foto.";
        }

    }

    protected void carregaInfoPesquisa()
    {

        if (txtLivro.Text.Trim().Length > 0)
        {
            Rota.Visible = true;
            lblRota.Text = txtLivro.Text;
        }
        else
        {
            Rota.Visible = false;
            lblRota.Text = "";
        }
        if (txtGrupo.Text.Trim().Length > 0)
        {
            Grupo.Visible = true;
            lblGrupo.Text = txtGrupo.Text;
        }
        else
        {
            Grupo.Visible = false;
            lblGrupo.Text = "";
        }
        if (ddlLtr.SelectedValue.ToString() != "0")
        {
            leiturista.Visible = true;
            lblLeiturista.Text = ddlLtr.SelectedItem.Text;
        }
        else
        {
            leiturista.Visible = false;
            lblLeiturista.Text = "";
        }

        SitLeitura.Visible = true;
        lblSitLeitura.Text = ddlSituacaoLeitura.SelectedItem.Text;

        if (cbxMsg.SelectedValue.ToString() != "1000")
        {
            Ocorrencia.Visible = true;
            lblOcorrencia.Text = cbxMsg.SelectedItem.Text;
        }
        else
        {
            Ocorrencia.Visible = false;
            lblOcorrencia.Text = "";
        }
        referencia.Visible = true;
        lblReferencia.Text = ((Label)Master.FindControl("lblRef")).Text;

    }

    protected void ajustaFoto(string caminho)
    {

        System.Drawing.Image imagem = System.Drawing.Image.FromFile(caminho);
        int altura = imagem.Height;
        int largura = 0;

        if (imagem.Width > 640)
            largura = 640;
        else
            largura = imagem.Width;

        altura = imagem.Height * largura / imagem.Width;
        largura = imagem.Width * altura / imagem.Height;

        imagem.Dispose();

        String initialPath = caminho;
        String finalPath = caminho;
        System.Drawing.Image originalImage = System.Drawing.Image.FromFile(initialPath);
        System.Drawing.Image thumbImage;
        Graphics graphic;

        ImageFormat format = originalImage.RawFormat;
        thumbImage = new Bitmap(largura, altura);

        graphic = Graphics.FromImage(thumbImage);
        graphic.CompositingQuality = CompositingQuality.HighQuality;
        graphic.SmoothingMode = SmoothingMode.HighQuality;
        graphic.InterpolationMode = InterpolationMode.HighQualityBicubic;
        graphic.DrawImage(originalImage, new System.Drawing.Rectangle(0, 0, largura, altura));

        originalImage.Dispose();
        thumbImage.Save(finalPath, format);
        thumbImage.Dispose();
        imagem.Dispose();
    }


    private String removeCE(String str)
    {
        try
        {
            str = str.Replace("Á", "A").Replace("Â", "A").Replace("À", "A")
                    .Replace("Ã", "A").Replace("Ä", "A");
            str = str.Replace("á", "a").Replace("â", "a").Replace("à", "a")
                    .Replace("ã", "a").Replace("ä", "a");
            str = str.Replace("É", "E").Replace("Ê", "E").Replace("È", "E");
            str = str.Replace("é", "e").Replace("ê", "e").Replace("è", "e");
            str = str.Replace("Í", "I").Replace("Î", "I").Replace("Ì", "I")
                    .Replace("Ï", "I");
            str = str.Replace("í", "i").Replace("î", "i").Replace("ì", "i")
                    .Replace("ï", "i");
            str = str.Replace("Ó", "O").Replace("Ô", "O").Replace("Ò", "O")
                    .Replace("Õ", "O");
            str = str.Replace("ó", "o").Replace("ô", "o").Replace("ò", "o")
                    .Replace("õ", "o");
            str = str.Replace("Ú", "U").Replace("Ü", "U");
            str = str.Replace("ú", "u").Replace("ü", "u");
            str = str.Replace("Ç", "C").Replace("ç", "c");

            return str;
        }
        catch (Exception ex)
        {
            return "";
            // MessageBox.Show(")
        }
    }

}