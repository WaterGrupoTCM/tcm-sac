﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WaterSyncLite.Class;

public partial class ViewConsAlteracoes : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        this.Master.LoadHeader(this.Master, Request.Path.Substring(Request.Path.LastIndexOf("/") + 1));
        if (!IsPostBack)
        {

            Data db = new Data();

            txtAno.Text = DateTime.Now.Year.ToString();
            cbxMes.SelectedIndex = DateTime.Now.Month;

            if (Request.Cookies["CliID"].Value.PadLeft(2, '0').ToString().Trim() == "17")
                panelGrupo.Visible = true;
            else
                panelGrupo.Visible = false;


            #region LEITURISTAS
            DataTable dt = db.GetDt(carregaConexao(), "WTR_LEIT", "SELECT LTR_ID, LTR_NOME FROM "
                + " WTR_LEITURISTAS WHERE CLI_ID = " + Request.Cookies["CliID"].Value.ToString().Trim()
                + " AND EMP_ID = " + Request.Cookies["EmpID"].Value.ToString().Trim()
                + " ORDER BY LTR_NOME");

            DataRow dtr = dt.NewRow();
            dtr["LTR_NOME"] = "Selecione um leiturista...";
            dtr["LTR_ID"] = 0;
            dt.Rows.Add(dtr);
            if (dt != null)
            {
                ddlLtr.DataSource = dt;
                ddlLtr.DataTextField = "LTR_NOME";
                ddlLtr.DataValueField = "LTR_ID";
                ddlLtr.DataBind();
                ddlLtr.SelectedValue = "0";

            }
            #endregion
        }

    }
    protected String carregaConexao()
    {
        String StrCon = "";
        if (Request.Cookies["StrCon"] != null)
        {
            if (Request.Cookies["StrCon"].Value.ToString().Length > 0)
                StrCon = Request.Cookies["StrCon"].Value.ToString();
        }
        return StrCon.Replace('|', ';').Replace("usuario", xml.GetConexaoValue(Server.MapPath("~"), "LOCAL", "USER")).Replace("senha", xml.GetConexaoValue(Server.MapPath("~"), "LOCAL", "PASSWORD"));
    }
    protected void btPesquisar_Click(object sender, EventArgs e)
    {

        GridView1.DataSource = carregaGrid();
        GridView1.DataBind();
    }

    protected DataTable carregaGrid()
    {
        String StrCon = carregaConexao();

       

        double porc = 0;
        string pMax = "";
        string pMin = "";


        if (txtAno.Text.Trim().Length <= 0)
        {
            lblMsg.Text = "Digite o ano antes de realizar a consulta.";
            PanelMsg.Visible = true;
            return null;
        }
       
        Data db = new Data();
        string sql =
            "select LIV_ANO AS ANO, LIV_MES AS MES, LEI_LIGACAO AS LIGACAO,LEI_CONSUMIDOR AS CONSUMIDOR, ";

        if (Request.Cookies["CliID"].Value.PadLeft(2, '0').ToString().Trim() == "17")
            sql += "GRUPO_ID AS GRUPO,";

        sql+=" LEI_ENDERECO AS ENDEREÇO, LEI_NUMERO AS NUMERO,leituraIncorreta AS ERRADA,LEI_LEITURA AS ATUAL,(select msg_descricao from wtr_mensagens where wtr_mensagens.msg_id = wtr_permissao.msg_id) as 'OCORR.ERRADA' ,MSG_DESCRICAO as 'OCORR.ATUAL'";

        sql += " FROM WTR_PERMISSAO INNER JOIN WTR_LEITURAS ON (LIV_MES = MES AND LIV_ANO = ANO AND CONVERT(INT,LEI_LIGACAO)  = CONVERT(INT,LIGACAO)) INNER JOIN WTR_MENSAGENS ON(WTR_LEITURAS.MSG_ID = WTR_MENSAGENS.MSG_ID and WTR_LEITURAS.cli_id = WTR_MENSAGENS.cli_id and WTR_MENSAGENS.emp_id = WTR_LEITURAS.emp_id) INNER JOIN WTR_LEITURISTAS ON (WTR_LEITURAS.LTR_ID = WTR_LEITURISTAS.LTR_ID) WHERE WTR_LEITURAS.emp_id =" +
                     Request.Cookies["EmpID"].Value.ToString().Trim() + " and WTR_LEITURAS.cid_id =" + Request.Cookies["EmpID"].Value.ToString().Trim() +
                     " and WTR_LEITURAS.CLI_ID = " + Request.Cookies["CliID"].Value.PadLeft(2, '0').ToString().Trim();// Data.cli.ToString();


        #region SQL E FILTROS

        if (txtLivro.Text.Trim().Length > 0)
        {
            if (Request.Cookies["CliID"].Value.PadLeft(2, '0').ToString().Trim() == "4")
                txtLivro.Text = txtLivro.Text.Trim().PadLeft(3, '0');
            else
                sql += " AND LOC_ID = '" + txtLivro.Text.Trim() + "' ";
        }


        if (cbxMes.SelectedIndex > 0)
            sql += " AND LIV_MES = '" + cbxMes.SelectedIndex.ToString() + "' ";

        if (txtAno.Text.Trim().Length > 0)
            sql += " AND LIV_ANO = '" + txtAno.Text.Trim() + "' ";

        #region Grupo
        if (txtGrupo.Text.Trim() != "" && Request.Cookies["CliID"].Value.PadLeft(2, '0').ToString().Trim() == "17")
        {
            sql += " AND GRUPO_ID = " + txtGrupo.Text;
        }
        #endregion
        #region Leiturista
        if (ddlLtr.SelectedValue.ToString() != "0")
        {
            sql += " AND LTR_ID = " + ddlLtr.SelectedValue.ToString();
        }
        #endregion

        #region Identificação e Medidor
        if ((txtIdentificacao.Text.Trim().Length > 0) || (txtMedidor.Text.Trim().Length > 0))
        {
            if (txtIdentificacao.Text.Trim().Length > 0)
                sql += " AND LEI_LIGACAO LIKE '%" + txtIdentificacao.Text.Trim() + "%' ";

            if (txtMedidor.Text.Trim().Length > 0)
                sql += " AND LEI_NUMMED LIKE '%" + txtMedidor.Text.Trim() + "%' ";

        }

        if (ddlSituacao.SelectedValue.ToString() == "Liberadas")
            sql += " AND FLAG = 'N'";
        else if (ddlSituacao.SelectedValue.ToString() == "Mofificadas")
            sql += " AND FLAG = 'S'";

        #endregion
        #endregion

        DataTable dt = db.GetDt(StrCon, "WTR_LEITURAS", sql);

        Session["dt"] = dt;
        PanelMsg.Visible = false;
        return dt;

    }


}