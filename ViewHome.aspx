﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="ViewHome.aspx.cs" Inherits="ViewHome" %>
<%@ MasterType VirtualPath="~/Site.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <style type="text/css">
        img.ca {
            opacity: 0.50;
            -moz-opacity: 0.50;
            filter: alpha(opacity=50);
        }

        #sidebar {
            background-color: #191919;
            padding: 10px;
        }

            #sidebar a:hover, a:focus {
                color: #FFF;
                text-decoration: none;
            }

            #sidebar.nav > li > a,
            #sidebar.nav > li > a:link {
                background-color: #191919;
                color: #FFF;
            }

                #sidebar.nav > li > a:hover,
                #sidebar.nav > li > a:focus {
                    background-color: #373737;
                }

            #sidebar.nav-pills > li.active > a,
            #sidebar.nav-pills > li.active > a:hover,
            #sidebar.nav-pills > li.active > a:focus {
                color: #FFF;
                background-color: #C72A25;
            }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="row">
        <div class="col-sm-3"></div>
        <div class="col-sm-6">
            <div class="row text-center Logocentralizado thumbnail">
                <img src="imagens/grupotcm.png" class="ca" />
            </div>
            <%--<div class="text-center ">
                <h2><span class="text-primary label">
                    <asp:Label ID="lblNomeCliente" runat="server" Text=""></asp:Label></span> </h2>
            </div>--%>
            <div class="text-center">
                <h2><span class="text-primary  label">Bem vindo
                        <asp:Label ID="lblUsuario" runat="server" Text=""></asp:Label></span> </h2>
            </div>

        </div>
        <div class="col-sm-3"></div>
    </div>


</asp:Content>
