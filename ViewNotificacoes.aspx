﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="ViewNotificacoes.aspx.cs" Inherits="ViewNotificacoes" %>

<%@ MasterType VirtualPath="~/Site.master" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

    <style type="text/css">
        .bold {
            font-weight: bold;
        }



        table td {
            padding-right: 50px;
            padding-left: 50px;
        }

        .btn-Msg {
            color: #fff;
            background-color: #D8DEDE;
            border-color: #474949;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <form runat="server">

        <div class="row">
            <div class="col-1">
                <asp:ImageButton ID="imgBtMsgTodos" runat="server" ImageUrl="~/imagens/msg_todos.png" OnClick="imgBtMsgTodos_Click" CssClass="img btn btn-Msg" />
            </div>
            <div class="col-1">
                <asp:ImageButton ID="imgBtMsgSelect" runat="server" ImageUrl="~/imagens/MsgSelect.png" OnClick="imgBtMsgSelect_Click" CssClass="img btn btn-Msg" />
            </div>
        </div>

        <div class="row">
            <div class="col-3">
            </div>
            <div class="col-6 text-center" id="divMsg" runat="server" visible="false">
                <asp:Label ID="lblMsg" runat="server" Text="" Font-Bold="true" />
            </div>
            <div class="col-3">
            </div>
        </div>

        <div class="row">
            <div class="col-12">
                <asp:GridView ID="GridDispositivos" runat="server" AutoGenerateColumns="False"
                    DataKeyNames="DIS_ID"
                    ShowFooter="True" EmptyDataText="Nenhuma rota encontrada" CellPadding="3" GridLines="Vertical" BackColor="White"
                    BorderColor="#999999" BorderStyle="None" BorderWidth="1px" Width="100%" OnRowCommand="GridDispositivos_RowCommand" OnRowDataBound="GridDispositivos_RowDataBound1">
                    <AlternatingRowStyle BackColor="Gainsboro" />
                    <Columns>
                        <asp:TemplateField HeaderText="Token" SortExpression="DIS_TOKEN" ItemStyle-CssClass="text-center" HeaderStyle-CssClass="text-center">
                            <EditItemTemplate>
                                <asp:Label ID="lblDIS_TOKEN" Text='<%# Eval("DIS_TOKEN") %>' runat="server"></asp:Label>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblDIS_TOKEN" Text='<%# Eval("DIS_TOKEN") %>' runat="server"></asp:Label>
                            </ItemTemplate>

                            <HeaderStyle CssClass="text-center"></HeaderStyle>

                            <ItemStyle CssClass="text-center"></ItemStyle>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="ID" SortExpression="DIS_ID" ItemStyle-CssClass="text-center" HeaderStyle-CssClass="text-center">
                            <EditItemTemplate>
                                <asp:Label ID="lblDIS_ID" Text='<%# Eval("DIS_ID") %>' runat="server"></asp:Label>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblDIS_ID" Text='<%# Eval("DIS_ID") %>' runat="server"></asp:Label>
                            </ItemTemplate>

                            <HeaderStyle CssClass="text-center"></HeaderStyle>

                            <ItemStyle CssClass="text-center"></ItemStyle>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="USUÁRIO" ItemStyle-CssClass="text-center" HeaderStyle-CssClass="text-center">
                            <ItemTemplate>
                                <asp:Label ID="lblDIS_USUARIO" Text='<%# Eval("DIS_USUARIO") %>' runat="server"></asp:Label>
                            </ItemTemplate>

                            <HeaderStyle CssClass="text-center"></HeaderStyle>

                            <ItemStyle CssClass="text-center"></ItemStyle>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="MODELO" ItemStyle-CssClass="text-center" HeaderStyle-CssClass="text-center">
                            <ItemTemplate>
                                <asp:Label ID="lblDIS_MODELO" Text='<%# Eval("DIS_MODELO") %>' runat="server"></asp:Label>
                            </ItemTemplate>

                            <HeaderStyle CssClass="text-center"></HeaderStyle>

                            <ItemStyle CssClass="text-center"></ItemStyle>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="DT.CADASTRO" ItemStyle-CssClass="text-center" HeaderStyle-CssClass="text-center">
                            <ItemTemplate>
                                <asp:Label ID="lblDIS_DATACADASTRO" Text='<%# Eval("DIS_DATACADASTRO") %>' runat="server"></asp:Label>
                            </ItemTemplate>

                            <HeaderStyle CssClass="text-center"></HeaderStyle>

                            <ItemStyle CssClass="text-center"></ItemStyle>
                        </asp:TemplateField>


                        <asp:TemplateField ItemStyle-CssClass="text-center" HeaderStyle-CssClass="text-center">
                            <HeaderTemplate>

                                <div class="checkbox">
                                    <asp:CheckBox ID="imgDesmarcar" runat="server" ToolTip="Desmarcar Todos" AutoPostBack="True" OnCheckedChanged="imgDesmarcar_CheckedChanged" />
                                </div>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <div class="checkbox">
                                    <asp:CheckBox ID="chkMarcar" runat="server" OnCheckedChanged="chkMarcar_CheckedChanged" AutoPostBack="True"></asp:CheckBox>
                                </div>
                                <asp:Label ID="lblMsg" runat="server" Text=""></asp:Label>

                            </ItemTemplate>

                            <HeaderStyle CssClass="text-center"></HeaderStyle>

                            <ItemStyle CssClass="text-center"></ItemStyle>
                        </asp:TemplateField>

                        <asp:ButtonField CommandName="Msg" Text="Msg" ButtonType="Image" HeaderText="Msg" ItemStyle-CssClass="text-center" ImageUrl="~/imagens/msg.png" />


                    </Columns>
                    <FooterStyle BackColor="#CCCCCC" ForeColor="Black" />
                    <HeaderStyle BackColor="#4f5b69" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Left" />
                    <RowStyle BackColor="#EEEEEE" ForeColor="Black" />
                    <SelectedRowStyle BackColor="#e1ffff" Font-Bold="True" ForeColor="White" />
                    <SortedAscendingCellStyle BackColor="#F1F1F1"></SortedAscendingCellStyle>
                    <SortedAscendingHeaderStyle BackColor="#0000A9"></SortedAscendingHeaderStyle>
                    <SortedDescendingCellStyle BackColor="#CAC9C9"></SortedDescendingCellStyle>
                    <SortedDescendingHeaderStyle BackColor="#000065"></SortedDescendingHeaderStyle>
                </asp:GridView>
            </div>

        </div>



        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document" style="padding: 8% 20% 20%">
                <div class="modal-content">
                    <div class="modal-header text-center">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <h4 class="modal-title" id="myModalLabel">
                            <img class="img-responsive" src="imagens/msg.png" />
                            Enviar Mensagem</h4>


                    </div>
                    <div class="modal-body">

                        <div class="row">
                            <div class="col-12">
                                <asp:Label ID="Label1" CssClass="bold" runat="server" Text="Token do Dispositivo:"></asp:Label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <asp:Label ID="lblToken" runat="server" Text=""></asp:Label>
                            </div>
                        </div>

                        <br />

                        <div class="row">
                            <div class="col-4">
                                <asp:Label ID="Label3" CssClass="bold" runat="server" Text="Modelo: "></asp:Label>
                            </div>
                            <div class="col-3">
                                <asp:Label ID="Label5" CssClass="bold" runat="server" Text="Data de Cadastro: "></asp:Label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-4">
                                <asp:Label ID="lblModelo" runat="server" Text=""></asp:Label>

                            </div>
                            <div class="col-3">
                                <asp:Label ID="lblData" runat="server" Text=""></asp:Label>

                            </div>
                        </div>

                        <br />

                        <div class="row">
                            <div class="col-12">
                                <asp:Label ID="label18" CssClass="bold" runat="server" Text="Mensagem"></asp:Label>

                            </div>

                        </div>
                        <div class="row">
                            <div class="col-12">
                                <asp:TextBox ID="txtMsg" class="form-control" Width="100%" runat="server" Text="" Rows="3" TextMode="MultiLine"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <asp:Button ID="btEnviarMsg" runat="server" Text="Enviar Mensagem" OnClick="btEnviarMsg_Click" />
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="myModalTodos" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document" style="padding: 8% 20% 20%">
                <div class="modal-content">
                    <div class="modal-header text-center">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <h4 class="modal-title" id="myModalLabel">
                            <img class="img-responsive" src="imagens/msg.png" />
                            Enviar Mensagem</h4>


                    </div>
                    <div class="modal-body">

                        <div class="row">
                            <div class="col-12 text-center">
                                <asp:Label ID="Label2" CssClass="bold" runat="server" Text="Todos os Dispositivos"></asp:Label>
                            </div>
                        </div>


                        <br />



                        <div class="row">
                            <div class="col-12">
                                <asp:Label ID="label10" CssClass="bold" runat="server" Text="Mensagem"></asp:Label>

                            </div>

                        </div>
                        <div class="row">
                            <div class="col-12">
                                <asp:TextBox ID="txtMsgTodos" class="form-control" Width="100%" runat="server" Text="" Rows="3" TextMode="MultiLine"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <asp:Button ID="btEnviarTodos" runat="server" Text="Enviar Mensagem" OnClick="btEnviarTodos_Click" />
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="myModalSelecionados" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document" style="padding: 8% 20% 20%">
                <div class="modal-content">
                    <div class="modal-header text-center">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <h4 class="modal-title" id="myModalLabel">
                            <img class="img-responsive" src="imagens/msg.png" />
                            Enviar Mensagem</h4>


                    </div>
                    <div class="modal-body">

                        <div class="row">
                            <div class="col-12 text-center">
                                <asp:Label ID="Label4" CssClass="bold" runat="server" Text="Dispositivos Selecionados"></asp:Label>
                            </div>
                        </div>


                        <br />



                        <div class="row">
                            <div class="col-12">
                                <asp:Label ID="label6" CssClass="bold" runat="server" Text="Mensagem"></asp:Label>

                            </div>

                        </div>
                        <div class="row">
                            <div class="col-12">
                                <asp:TextBox ID="txtMSgSelecionadas" class="form-control" Width="100%" runat="server" Text="" Rows="3" TextMode="MultiLine"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <asp:Button ID="btEnviarMsgSelecionadas" runat="server" Text="Enviar Mensagem" OnClick="btEnviarMsgSelecionadas_Click" />
                    </div>
                </div>
            </div>
        </div>

    </form>


</asp:Content>

