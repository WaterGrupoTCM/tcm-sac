﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WaterSyncLite.MDL;
using WaterSyncLite.DAL;
using WaterSyncLite.Class;
using System.Data;

public partial class ViewConsFechamento : System.Web.UI.Page
{
    public static bool fecExists = false;
    public bool inicio;
    Data db = new Data();
    private int qtdIrreg = 0;

    protected void Page_Load(object sender, EventArgs e)
    {
        this.Master.LoadHeader(this.Master, Request.Path.Substring(Request.Path.LastIndexOf("/") + 1));
        if (!IsPostBack)
        {
            

            try
            {
                //popAnos();
                popMes();

                cbxMes.SelectedIndex = Convert.ToInt32(Request.Cookies["mes"].Value.ToString().Trim());
                cbxAno.Text = Request.Cookies["ano"].Value.ToString().Trim();
            }
            catch (Exception ex)
            {
                msg("Problemas no sistema: " + ex.Message);
            }
        }
    }


    protected void msg(string texto)
    {
        Response.Write(texto);
    }



    private void popMes()
    {
        try
        {
            cbxMes.Items.Add("Selecione o mês...");
            cbxMes.Items.Add("01 - Janeiro");
            cbxMes.Items.Add("02 - Fevereiro");
            cbxMes.Items.Add("03 - Março");
            cbxMes.Items.Add("04 - Abril");
            cbxMes.Items.Add("05 - Maio");
            cbxMes.Items.Add("06 - Junho");
            cbxMes.Items.Add("07 - Julho");
            cbxMes.Items.Add("08 - Agosto");
            cbxMes.Items.Add("09 - Setembro");
            cbxMes.Items.Add("10 - Outubro");
            cbxMes.Items.Add("11 - Novembro");
            cbxMes.Items.Add("12 - Dezembro");
            cbxMes.Text = "Selecione o mês...";
        }
        catch (Exception ex)
        {
            msg("Problemas no sistema: " + ex.Message);
        }
    }
    //protected void btnFiltrar_Click(object sender, EventArgs e)
    //{
    /*//if (cbxEmpresa.SelectedValue.ToString() == "0")
    //    MessageBox.Show("Selecione uma empresa.", "WaterSync", MessageBoxButtons.OK,MessageBoxIcon.Information,MessageBoxDefaultButton.Button1);
    if (cbxMes.Text.Trim() == "Selecione um mês...")
        msg("Selecione um mês.");
    else
    {
        string sql, titulo;

        #region Separação de Etapas
        // Contador por Leiturista
        sql = " select EMP_ID, ETP_ID, LTR_ID, CID_ID, LIV_MES, SUM(LIV_TOTAL) AS Leituras, "
            + " SUM(LIV_ERROS) AS Erros, SUM(LIV_DCM) AS DCM, SUM(LIV_DSM) AS DSM, "
            + " SUM(LIV_MEDIA) AS MEDIA from WTR_LIVROS "
            + " where ( (LIV_MES = " + cbxMes.SelectedIndex.ToString()
            + " and ETP_ID < 13) or ( LIV_MES = " + Convert.ToString(cbxMes.SelectedIndex - 1)
            + " and ETP_ID > 12)) "
            + " and CLI_ID = " + Request.Cookies["CliID"].Value.ToString().Trim()
            + " and EMP_ID = " + Request.Cookies["EmpID"].Value.ToString().Trim()
            + " group by ETP_ID, LTR_ID, LIV_MES, EMP_ID, CID_ID "
            + " order by LIV_MES, ETP_ID ";

        titulo = "RESUMO DE LEITURA, MEDIA E REFATURA POR LEITURISTA";

        printEtapas(sql, titulo);
        #endregion

        #region Separação de Etapas por Cidade
        sql = " select EMP_ID, ETP_ID, CID_ID, LIV_MES, sum(LIV_TOTAL) AS Leituras, "
            + " SUM(LIV_ERROS) AS Erros, SUM(LIV_DCM) AS DCM, SUM(LIV_DSM) AS DSM, "
            + " SUM(LIV_MEDIA) AS MEDIA from WTR_LIVROS "
            + " where ( (LIV_MES = " + cbxMes.SelectedIndex.ToString()
            + " and ETP_ID < 13) or ( LIV_MES = " + Convert.ToString(cbxMes.SelectedIndex - 1)
            + " and ETP_ID > 12) ) "
            + " and CLI_ID = " + Request.Cookies["CliID"].Value.ToString().Trim()
            + " and EMP_ID = " + Request.Cookies["EmpID"].Value.ToString().Trim()
            + " GROUP BY ETP_ID, CID_ID, LIV_MES, EMP_ID"
            + " order by LIV_MES, ETP_ID ";

        titulo = "TOTAL DE LEITURA POR CIDADE E C.R.S.";

        printEtpCidades(sql, titulo);

        #endregion

        #region Separação de Etapas por CRS
        sql = "select R.CRS_ID, R.CRS_NOME, SUM(L.LIV_TOTAL) AS TOTAL, SUM(L.LIV_ERROS) AS ERROS,"
            + " SUM(L.LIV_MEDIA) AS MEDIA, SUM(L.LIV_DCM) AS DCM, SUM(L.LIV_DSM) AS DSM "
            + " from WTR_LIVROS L INNER JOIN WTR_CIDADES C ON L.CID_ID = C.CID_ID "
            + " AND L.LIV_MES = 7 AND L.ETP_ID < 13 AND L.EMP_ID = 2 OR L.LIV_MES = 8 "
            + " AND L.ETP_ID > 12 AND L.EMP_ID = 2 INNER JOIN WTR_CRS R ON C.CRS_ID = R.CRS_ID "
            + " GROUP BY R.CRS_ID, R.CRS_NOME "
            + " ORDER BY R.CRS_ID ";

        titulo = "TOTAL DE LEITURA POR CRS";

        printEtpCRS(sql, titulo);
        #endregion

        #region Total de Etapas
        // Contador por Leiturista
        sql = " select LTR_ID, CID_ID, LIV_MES, SUM(LIV_TOTAL) as Leituras, "
            + " SUM(LIV_ERROS) AS Erros, SUM(LIV_DCM) AS DCM, SUM(LIV_DSM) AS DSM, "
            + " SUM(LIV_MEDIA) AS MEDIA from WTR_LIVROS "
            + " where ( (LIV_MES = " + cbxMes.SelectedIndex.ToString()
            + " and ETP_ID < 13) or ( LIV_MES = " + Convert.ToString(cbxMes.SelectedIndex - 1)
            + " and ETP_ID > 12) ) "
            + " and CLI_ID = " + Request.Cookies["CliID"].Value.ToString().Trim()
            + " and EMP_ID = " + Request.Cookies["EmpID"].Value.ToString().Trim()
            + " group by LTR_ID, LIV_MES, CID_ID "
            + " order by LIV_MES ";

        titulo = "RESUMO DE LEITURA, MEDIA E REFATURA POR LEITURISTA";

        printTotalEtapas(sql, titulo);
        #endregion

        #region Total de Etapas por Cidade
        sql = "select CID_ID, LIV_MES, SUM(LIV_TOTAL) as Leituras, "
            + " SUM(LIV_ERROS) AS Erros, SUM(LIV_DCM) AS DCM, SUM(LIV_DSM) AS DSM, "
            + " SUM(LIV_MEDIA) AS MEDIA from WTR_LIVROS "
            + " where ( (LIV_MES = " + cbxMes.SelectedIndex.ToString()
            + " and ETP_ID < 13) or ( LIV_MES = " + Convert.ToString(cbxMes.SelectedIndex - 1)
            + " and ETP_ID > 12) ) "
            + " and CLI_ID = " + Request.Cookies["CliID"].Value.ToString().Trim()
            + " and EMP_ID = " + Request.Cookies["EmpID"].Value.ToString().Trim()
            + " group by LIV_MES, CID_ID "
            + " order by LIV_MES ";

        titulo = "TOTAL DE LEITURA POR CIDADE E C.R.S.";

        printTotalEtapasCidades(sql, titulo);
        #endregion

        #region ISS
        //Leituras
        //( (Total de Leituras da cidade + Leituras Irregulares) * Valor) * ( (Índice1 + Índice2) / 2)

        sql = "SELECT EMP_ID, CID_ID, SUM(LIV_TOTAL) AS Leituras, SUM(LIV_ERROS) AS Erros, "
            + " SUM(LIV_DCM) AS DCM, SUM(LIV_DSM) AS DSM, SUM(LIV_MEDIA) AS MEDIA, "
            + "	(SELECT FEC_VRLEITURA FROM WTR_FECHAMENTO) AS VrFechamento "
            + " FROM WTR_LIVROS "
            + " WHERE (LIV_MES = 7) AND (ETP_ID < 13) AND (EMP_ID = 2) "
            + " OR (LIV_MES = 8) AND (ETP_ID > 12) AND (EMP_ID = 2) "
            + " GROUP BY CID_ID, EMP_ID "
            + " ORDER BY CID_ID ";

        if (txtIrregular.Text.Trim() == "") txtIrregular.Text = "0";
        qtdIrreg = Convert.ToInt32(txtIrregular.Text.Trim());



        //Entregas
        //( (Total de Leituras da cidade + DCM + DSM) * Índice1) * Índice2

        //Alternadas
        //( (Total de Alternadas por cidade) * Índice1)

        //
        //
        //
        //
        //	sql = "select CID_NOME, CID_ISS from WTR_CIDADES order by CID_ID";

        titulo = "TOTAL DE SERVIÇOS EXECUTADOS POR CIDADES PARA PAGAMENTO DO ISS";

        printISS(sql, titulo);
        #endregion

          
    }*/
    //}

    private void printEtapas(string sql, string titulo)
    {
        try
        {
            if (sql.Trim().Length > 0)
            {
                int linha = 0;
                int numLinhas = Convert.ToInt32(64);
                DataTable dt = new DataTable();

                //Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;
                txtDados.Text = "";

                string nomeLtr = "";
                string nomeCid = "";
                string etapa = "";

                dt = db.GetDt(carregaConexao(), "PRINT", sql, "TCM");
                DataRow[] dtr = dt.Select();

                if (dtr.Length > 0)
                {
                    string line = titulo.PadLeft(67, ' ');

                    /*line += "Empresa: " + Data.nomeEmp.Trim() + "      " 
                        + "Ref: " + cbxMes.SelectedItem.ToString().Trim() 
                        + "/" + cbxAno.SelectedItem.ToString().Trim() + "\r\n";
                    */
                    string cabeca = "|-Refatura-|".PadLeft(65, ' ') + "\r\n";
                    cabeca += "Col".PadRight(3, ' ');
                    cabeca += "Leiturista".PadRight(22, ' ');
                    cabeca += "Cidade".PadRight(22, ' ');
                    cabeca += "Leitu".PadLeft(5, ' ');
                    cabeca += "Med".PadLeft(4, ' ');
                    cabeca += "Err".PadLeft(4, ' ');
                    cabeca += "Med".PadLeft(4, ' ');
                    cabeca += "Total".PadLeft(6, ' ');
                    cabeca += "DSM".PadLeft(4, ' ');
                    cabeca += "DCM".PadLeft(4, ' ');
                    cabeca += "\r\n";
                    //line += cabeca;

                    foreach (DataRow dr in dtr)
                    {
                        if (dr["ETP_ID"].ToString() != etapa)
                        {
                            line += "\r\n" + "Empresa: " + Request.Cookies["EmpID"].Value.ToString().Trim()/*Data.nomeEmp.Trim()*/ + "      "
                                + "Ref: " + db.NomeMes(Convert.ToInt32(dr["LIV_MES"].ToString()))
                                + "/" + cbxAno.SelectedItem.ToString().Trim()
                                + "  -  Etapa: " + dr["ETP_ID"].ToString();
                            line += "\r\n" + cabeca;
                        }

                        line += dr["LTR_ID"].ToString().PadRight(3, ' ');

                        // ** MELHORAR BUSCA - colocar no sql anterior...
                        nomeLtr = db.GetField(carregaConexao(), "select RTrim(LTR_NOME) from WTR_LEITURISTAS where LTR_ID = " + dr["LTR_ID"].ToString(), 0, "TCM");
                        line += nomeLtr.Trim().PadRight(22, ' ');

                        // ** MELHORAR BUSCA - colocar no sql anterior...
                        nomeCid = db.GetField(carregaConexao(), "select RTrim(CID_NOME) from WTR_CIDADES where CID_ID = " + dr["CID_ID"].ToString(), 0, "TCM");
                        line += nomeCid.Trim().PadRight(22, ' ');
                        //line += dr["CID_NOME"].ToString().PadRight(22,' ');

                        line += dr["LEITURAS"].ToString().PadLeft(5, ' ');
                        line += dr["MEDIA"].ToString().PadLeft(4, ' ');
                        line += dr["ERROS"].ToString().PadLeft(4, ' ');
                        line += dr["MEDIA"].ToString().PadLeft(4, ' ');
                        line += dr["LEITURAS"].ToString().PadLeft(6, ' ');
                        line += dr["DSM"].ToString().PadLeft(4, ' ');
                        line += dr["DCM"].ToString().PadLeft(4, ' ');
                        line += "\r\n";

                        /*if(linha > numLinhas)
                        {
                            linha = 0;
                            line += "\r\n" + "\r\n" + "\r\n" + cabeca;
                        }
                        */

                        linha++;
                        etapa = dr["ETP_ID"].ToString();
                    }
                    txtDados.Text = line;
                }

                //cbxAno.Enabled = false;
                //cbxMes.Enabled = false;
            }
        }
        catch (Exception ex)
        {
            msg("Informação: " + ex.Message);
        }
        finally
        {
            //Cursor.Current = System.Windows.Forms.Cursors.Default;
        }
    }


    protected String carregaConexao()
    {
        String StrCon = "";
        if (Request.Cookies["StrCon"] != null)
        {
            if (Request.Cookies["StrCon"].Value.ToString().Length > 0)
                StrCon = Request.Cookies["StrCon"].Value.ToString();
        }
        return StrCon.Replace('|', ';').Replace("usuario", xml.GetConexaoValue(Server.MapPath("~"), "LOCAL", "USER")).Replace("senha", xml.GetConexaoValue(Server.MapPath("~"), "LOCAL", "PASSWORD"));
    }

    private void printEtpCidades(string sql, string titulo)
    {
        try
        {
            if (sql.Trim().Length > 0)
            {
                int linha = 0;
                int numLinhas = Convert.ToInt32(64);
                DataTable dt = new DataTable();

                //Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;
                txtDados.Text = "";

                string nomeCid = "";
                string etapa = "";

                dt = db.GetDt(carregaConexao(), "PRINT", sql, "TCM");
                DataRow[] dtr = dt.Select();

                if (dtr.Length > 0)
                {
                    string line = titulo.PadLeft(65, ' ');

                    string cabeca = "|---- Refatura ----|".PadLeft(66, ' ') + "\r\n";
                    cabeca += "Cidade".PadRight(26, ' ');
                    cabeca += "Leituras".PadLeft(10, ' ');
                    cabeca += "Medias".PadLeft(8, ' ');
                    cabeca += "Erros".PadLeft(7, ' ');
                    cabeca += "Medias".PadLeft(8, ' ');
                    cabeca += "Total".PadLeft(7, ' ');
                    cabeca += "DSM".PadLeft(6, ' ');
                    cabeca += "DCM".PadLeft(6, ' ');
                    cabeca += "\r\n";
                    //line += cabeca;

                    foreach (DataRow dr in dtr)
                    {
                        if (dr["ETP_ID"].ToString() != etapa)
                        {
                            line += "\r\n" + "Empresa: " + Request.Cookies["EmpID"].Value.ToString().Trim()/*Data.nomeEmp.Trim()*/ + "      "
                                + "Ref: " + db.NomeMes(Convert.ToInt32(dr["LIV_MES"].ToString()))
                                + "/" + cbxAno.SelectedItem.ToString().Trim()
                                + "  -  Etapa: " + dr["ETP_ID"].ToString() + "\r\n";
                            line += "\r\n" + cabeca;
                        }

                        // ** MELHORAR BUSCA - colocar no sql anterior...
                        nomeCid = db.GetField(carregaConexao(), "select RTrim(CID_NOME) from WTR_CIDADES where CID_ID = " + dr["CID_ID"].ToString(), 0, "TCM");
                        line += nomeCid.Trim().PadRight(26, ' ');
                        //line += dr["CID_NOME"].ToString().PadRight(22,' ');

                        line += dr["LEITURAS"].ToString().PadLeft(10, ' ');
                        line += dr["MEDIA"].ToString().PadLeft(8, ' ');
                        line += dr["ERROS"].ToString().PadLeft(7, ' ');
                        line += dr["MEDIA"].ToString().PadLeft(8, ' ');
                        line += dr["LEITURAS"].ToString().PadLeft(7, ' ');
                        line += dr["DSM"].ToString().PadLeft(6, ' ');
                        line += dr["DCM"].ToString().PadLeft(6, ' ');
                        line += "\r\n";

                        /*if(linha > numLinhas)
                        {
                            linha = 0;
                            line += "\r\n" + "\r\n" + "\r\n" + cabeca;
                        }
                        */

                        linha++;
                        etapa = dr["ETP_ID"].ToString();
                    }
                    txtDados.Text = line;
                }

                //cbxAno.Enabled = false;
                //cbxMes.Enabled = false;
            }
        }
        catch (Exception ex)
        {
            msg("Informação: " + ex.Message);
        }
        finally
        {
            //Cursor.Current = System.Windows.Forms.Cursors.Default;
        }
    }


    private void printTotalEtapas(string sql, string titulo)
    {
        try
        {
            if (sql.Trim().Length > 0)
            {
                int linha = 0;
                int numLinhas = Convert.ToInt32(64);
                DataTable dt = new DataTable();

                //Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;
                txtDados.Text = "";

                string nomeLtr = "";
                string nomeCid = "";

                dt = db.GetDt(carregaConexao(), "PRINT", sql, "TCM");
                DataRow[] dtr = dt.Select();

                if (dtr.Length > 0)
                {
                    string line = titulo.PadLeft(67, ' ');

                    line += "\r\n" + "Empresa: " + Request.Cookies["EmpID"].Value.ToString().Trim()/*Data.nomeEmp.Trim()*/ + "      "
                        + "Ref: " + db.NomeMes(cbxMes.SelectedIndex)
                        + "/" + cbxAno.SelectedItem.ToString().Trim()
                        + "  -  Etapa: TOTAL" + "\r\n";


                    string cabeca = "|-Refatura-|".PadLeft(64, ' ') + "\r\n";
                    cabeca += "Col".PadRight(3, ' ');
                    cabeca += "Leiturista".PadRight(22, ' ');
                    cabeca += "Cidade".PadRight(22, ' ');
                    cabeca += "Leitu".PadLeft(5, ' ');
                    cabeca += "Med".PadLeft(4, ' ');
                    cabeca += "Err".PadLeft(4, ' ');
                    cabeca += "Med".PadLeft(4, ' ');
                    cabeca += "Total".PadLeft(6, ' ');
                    cabeca += "DSM".PadLeft(4, ' ');
                    cabeca += "DCM".PadLeft(4, ' ');
                    cabeca += "\r\n";
                    line += cabeca;

                    foreach (DataRow dr in dtr)
                    {
                        line += dr["LTR_ID"].ToString().PadRight(3, ' ');

                        // ** MELHORAR BUSCA - colocar no sql anterior...
                        nomeLtr = db.GetField(carregaConexao(), "select RTrim(LTR_NOME) from WTR_LEITURISTAS where LTR_ID = " + dr["LTR_ID"].ToString(), 0, "TCM");
                        line += nomeLtr.Trim().PadRight(22, ' ');

                        // ** MELHORAR BUSCA - colocar no sql anterior...
                        nomeCid = db.GetField(carregaConexao(), "select RTrim(CID_NOME) from WTR_CIDADES where CID_ID = " + dr["CID_ID"].ToString(), 0, "TCM");
                        line += nomeCid.Trim().PadRight(22, ' ');
                        //line += dr["CID_NOME"].ToString().PadRight(22,' ');

                        line += dr["LEITURAS"].ToString().PadLeft(5, ' ');
                        line += dr["MEDIA"].ToString().PadLeft(4, ' ');
                        line += dr["ERROS"].ToString().PadLeft(4, ' ');
                        line += dr["MEDIA"].ToString().PadLeft(4, ' ');
                        line += dr["LEITURAS"].ToString().PadLeft(6, ' ');
                        line += dr["DSM"].ToString().PadLeft(4, ' ');
                        line += dr["DCM"].ToString().PadLeft(4, ' ');
                        line += "\r\n";

                        /*if(linha > numLinhas)
                        {
                            linha = 0;
                            line += "\r\n" + "\r\n" + "\r\n" + cabeca;
                        }
                        */

                        linha++;
                        //etapa = dr["ETP_ID"].ToString();
                    }
                    txtDados.Text = line;
                }

                //cbxAno.Enabled = false;
                //cbxMes.Enabled = false;
            }
        }
        catch (Exception ex)
        {
            msg("Informação: " + ex.Message);
        }
        finally
        {
            //Cursor.Current = System.Windows.Forms.Cursors.Default;
        }
    }


    private void printTotalEtapasCidades(string sql, string titulo)
    {
        try
        {
            if (sql.Trim().Length > 0)
            {
                int linha = 0;
                int numLinhas = Convert.ToInt32(64);
                DataTable dt = new DataTable();

                //Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;
                txtDados.Text = "";

                string nomeCid = "";

                dt = db.GetDt(carregaConexao(), "PRINT", sql, "TCM");
                DataRow[] dtr = dt.Select();

                if (dtr.Length > 0)
                {
                    string line = titulo.PadLeft(67, ' ');

                    line += "\r\n" + "Empresa: " + Request.Cookies["EmpID"].Value.ToString().Trim()/*Data.nomeEmp.Trim()*/ + "      "
                        + "Ref: " + db.NomeMes(cbxMes.SelectedIndex)
                        + "/" + cbxAno.SelectedItem.ToString().Trim()
                        + "  -  Etapa: TOTAL" + "\r\n";

                    string cabeca = "|---- Refatura ----|".PadLeft(66, ' ') + "\r\n";
                    cabeca += "Cidade".PadRight(26, ' ');
                    cabeca += "Leituras".PadLeft(10, ' ');
                    cabeca += "Medias".PadLeft(8, ' ');
                    cabeca += "Erros".PadLeft(7, ' ');
                    cabeca += "Medias".PadLeft(8, ' ');
                    cabeca += "Total".PadLeft(7, ' ');
                    cabeca += "DSM".PadLeft(6, ' ');
                    cabeca += "DCM".PadLeft(6, ' ');
                    cabeca += "\r\n";
                    line += cabeca;

                    foreach (DataRow dr in dtr)
                    {
                        // ** MELHORAR BUSCA - colocar no sql anterior...
                        nomeCid = db.GetField(carregaConexao(), "select RTrim(CID_NOME) from WTR_CIDADES where CID_ID = " + dr["CID_ID"].ToString() + " and CLI_ID = " + Request.Cookies["CliID"].Value.ToString().Trim(), 0, "TCM");
                        line += nomeCid.Trim().PadRight(26, ' ');
                        //line += dr["CID_NOME"].ToString().PadRight(22,' ');

                        line += dr["LEITURAS"].ToString().PadLeft(10, ' ');
                        line += dr["MEDIA"].ToString().PadLeft(8, ' ');
                        line += dr["ERROS"].ToString().PadLeft(7, ' ');
                        line += dr["MEDIA"].ToString().PadLeft(8, ' ');
                        line += dr["LEITURAS"].ToString().PadLeft(7, ' ');
                        line += dr["DSM"].ToString().PadLeft(6, ' ');
                        line += dr["DCM"].ToString().PadLeft(6, ' ');
                        line += "\r\n";

                        /*if(linha > numLinhas)
                        {
                            linha = 0;
                            line += "\r\n" + "\r\n" + "\r\n" + cabeca;
                        }
                        */

                        linha++;
                    }
                    txtDados.Text = line;
                }

                //cbxAno.Enabled = false;
                //cbxMes.Enabled = false;
            }
        }
        catch (Exception ex)
        {
            msg("Informação: " + ex.Message);
        }
        finally
        {
            //Cursor.Current = System.Windows.Forms.Cursors.Default;
        }
    }


    private void printEtpCRS(string sql, string titulo)
    {
        try
        {

        }
        catch (Exception ex)
        {
            msg("Informação: " + ex.Message);
        }
        finally
        {
            //Cursor.Current = System.Windows.Forms.Cursors.Default;
        }
    }


    private void printISS(string sql, string titulo)
    {
        try
        {
            if (sql.Trim().Length > 0)
            {
                int linha = 0;
                int numLinhas = Convert.ToInt32(64);
                DataTable dt = new DataTable();

                //Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;
                txtDados.Text = "";

                string nomeCid = "";

                dt = db.GetDt(carregaConexao(), "PRINT", sql, "TCM");
                DataRow[] dtr = dt.Select();

                if (dtr.Length > 0)
                {
                    string line = titulo.PadLeft(67, ' ');

                    line += "\r\n" + "Empresa: " + Request.Cookies["EmpID"].Value.ToString().Trim() + "      "
                        + "Ref: " + db.NomeMes(cbxMes.SelectedIndex)
                        + "/" + cbxAno.SelectedItem.ToString().Trim()
                        + "  -  Etapa: TOTAL" + "\r\n";

                    string cabeca = "I.S.S.".PadLeft(55, ' ') + "\r\n";
                    cabeca += "Cidade".PadRight(20, ' ');
                    cabeca += "Leituras".PadLeft(10, ' ');
                    cabeca += "Entrega".PadLeft(8, ' ');
                    cabeca += "Alternadas".PadLeft(10, ' ');
                    cabeca += "Reaviso".PadLeft(8, ' ');
                    cabeca += "Total".PadLeft(10, ' ');
                    cabeca += "%".PadLeft(5, ' ');
                    cabeca += "Valor".PadLeft(10, ' ');
                    cabeca += "\r\n";
                    line += cabeca;

                    foreach (DataRow dr in dtr)
                    {
                        // ** MELHORAR BUSCA - colocar no sql anterior...
                        nomeCid = db.GetField(carregaConexao(), "select RTrim(CID_NOME) from WTR_CIDADES where CID_ID = " + dr["CID_ID"].ToString() + " and CLI_ID = " + Request.Cookies["CliID"].Value.ToString().Trim(), 0, "TCM");
                        line += nomeCid.Trim().PadRight(26, ' ');
                        //line += dr["CID_NOME"].ToString().PadRight(22,' ');

                        line += dr["LEITURAS"].ToString().PadLeft(10, ' ');
                        line += dr["MEDIA"].ToString().PadLeft(8, ' ');
                        line += dr["ERROS"].ToString().PadLeft(7, ' ');
                        line += dr["MEDIA"].ToString().PadLeft(8, ' ');
                        line += dr["LEITURAS"].ToString().PadLeft(7, ' ');
                        line += dr["DSM"].ToString().PadLeft(6, ' ');
                        line += dr["DCM"].ToString().PadLeft(6, ' ');
                        line += "\r\n";

                        /*if(linha > numLinhas)
                        {
                            linha = 0;
                            line += "\r\n" + "\r\n" + "\r\n" + cabeca;
                        }
                        */

                        linha++;
                    }
                    txtDados.Text = line;
                }

                // cbxAno.Enabled = false;
                //cbxMes.Enabled = false;
            }
        }
        catch (Exception ex)
        {
            msg("Informação: " + ex.Message);
        }
        finally
        {
            //Cursor.Current = System.Windows.Forms.Cursors.Default;
        }
    }


    /*private void btnPrint_Click(object sender, System.EventArgs e)
    {
        try
        {
            string s = "";
            

            // device-dependent string, need a FormFeed?
            //string s = txtEtapas.Text; // device-dependent string, need a FormFeed?

            if (s.Trim().Length > 0)
            {
                // Allow the user to select a printer.
                PrintDialog pd = new PrintDialog();
                pd.PrinterSettings = new PrinterSettings();
                if (DialogResult.OK == pd.ShowDialog(this))
                {
                    // Send a printer-specific to the printer.
                    Print.RawPrinterHelper.SendStringToPrinter(pd.PrinterSettings.PrinterName, s);
                }
            }
        }
        catch (Exception ex)
        {
            MessageBox.Show("Problemas no sistema: " + ex.Message);
        }
    }*/



    protected void linkEtapas_Click(object sender, EventArgs e)
    {
        if (cbxMes.Text.Trim() == "Selecione um mês...")
            msg("Selecione um mês.");
        else
        {
            string sql, titulo;
            #region Separação de Etapas
            // Contador por Leiturista
            sql = " select EMP_ID, ETP_ID, LTR_ID, CID_ID, LIV_MES, SUM(LIV_TOTAL) AS Leituras, "
                + " SUM(LIV_ERROS) AS Erros, SUM(LIV_DCM) AS DCM, SUM(LIV_DSM) AS DSM, "
                + " SUM(LIV_MEDIA) AS MEDIA from WTR_LIVROS "
                + " where ( (LIV_MES = " + cbxMes.SelectedIndex.ToString()
                + " and ETP_ID < 13) or ( LIV_MES = " + Convert.ToString(cbxMes.SelectedIndex - 1)
                + " and ETP_ID > 12)) "
                + " and CLI_ID = " + Request.Cookies["CliID"].Value.ToString().Trim()
                + " and EMP_ID = " + Request.Cookies["EmpID"].Value.ToString().Trim()
                + " group by ETP_ID, LTR_ID, LIV_MES, EMP_ID, CID_ID "
                + " order by LIV_MES, ETP_ID ";

            titulo = "RESUMO DE LEITURA, MEDIA E REFATURA POR LEITURISTA";

            printEtapas(sql, titulo);
            #endregion
        }
    }
    protected void linkEtapasCidades_Click(object sender, EventArgs e)
    {
        if (cbxMes.Text.Trim() == "Selecione um mês...")
            msg("Selecione um mês.");
        else
        {
            string sql, titulo;
            #region Separação de Etapas por Cidade
            sql = " select EMP_ID, ETP_ID, CID_ID, LIV_MES, sum(LIV_TOTAL) AS Leituras, "
                + " SUM(LIV_ERROS) AS Erros, SUM(LIV_DCM) AS DCM, SUM(LIV_DSM) AS DSM, "
                + " SUM(LIV_MEDIA) AS MEDIA from WTR_LIVROS "
                + " where ( (LIV_MES = " + cbxMes.SelectedIndex.ToString()
                + " and ETP_ID < 13) or ( LIV_MES = " + Convert.ToString(cbxMes.SelectedIndex - 1)
                + " and ETP_ID > 12) ) "
                + " and CLI_ID = " + Request.Cookies["CliID"].Value.ToString().Trim()
                + " and EMP_ID = " + Request.Cookies["EmpID"].Value.ToString().Trim()
                + " GROUP BY ETP_ID, CID_ID, LIV_MES, EMP_ID"
                + " order by LIV_MES, ETP_ID ";

            titulo = "TOTAL DE LEITURA POR CIDADE E C.R.S.";

            printEtpCidades(sql, titulo);

            #endregion
        }
    }
    protected void linkTotalEtapasCid_Click(object sender, EventArgs e)
    {
        if (cbxMes.Text.Trim() == "Selecione um mês...")
            msg("Selecione um mês.");
        else
        {
            string sql, titulo;
            #region Total de Etapas por Cidade
            sql = "select CID_ID, LIV_MES, SUM(LIV_TOTAL) as Leituras, "
                + " SUM(LIV_ERROS) AS Erros, SUM(LIV_DCM) AS DCM, SUM(LIV_DSM) AS DSM, "
                + " SUM(LIV_MEDIA) AS MEDIA from WTR_LIVROS "
                + " where ( (LIV_MES = " + cbxMes.SelectedIndex.ToString()
                + " and ETP_ID < 13) or ( LIV_MES = " + Convert.ToString(cbxMes.SelectedIndex - 1)
                + " and ETP_ID > 12) ) "
                + " and CLI_ID = " + Request.Cookies["CliID"].Value.ToString().Trim()
                + " and EMP_ID = " + Request.Cookies["EmpID"].Value.ToString().Trim()
                + " group by LIV_MES, CID_ID "
                + " order by LIV_MES ";

            titulo = "TOTAL DE LEITURA POR CIDADE E C.R.S.";

            printTotalEtapasCidades(sql, titulo);
            #endregion

        }
    }
    protected void linkResumoMed_Click(object sender, EventArgs e)
    {
        if (cbxMes.Text.Trim() == "Selecione um mês...")
            msg("Selecione um mês.");
        else
        {
            string sql, titulo;
            #region Total de Etapas
            // Contador por Leiturista
            sql = " select LTR_ID, CID_ID, LIV_MES, SUM(LIV_TOTAL) as Leituras, "
                + " SUM(LIV_ERROS) AS Erros, SUM(LIV_DCM) AS DCM, SUM(LIV_DSM) AS DSM, "
                + " SUM(LIV_MEDIA) AS MEDIA from WTR_LIVROS "
                + " where ( (LIV_MES = " + cbxMes.SelectedIndex.ToString()
                + " and ETP_ID < 13) or ( LIV_MES = " + Convert.ToString(cbxMes.SelectedIndex - 1)
                + " and ETP_ID > 12) ) "
                + " and CLI_ID = " + Request.Cookies["CliID"].Value.ToString().Trim()
                + " and EMP_ID = " + Request.Cookies["EmpID"].Value.ToString().Trim()
                + " group by LTR_ID, LIV_MES, CID_ID "
                + " order by LIV_MES ";

            titulo = "RESUMO DE LEITURA, MEDIA E REFATURA POR LEITURISTA";

            printTotalEtapas(sql, titulo);
            #endregion
        }
    }
    protected void linkISS_Click(object sender, EventArgs e)
    {
        if (cbxMes.Text.Trim() == "Selecione um mês...")
            msg("Selecione um mês.");
        else
        {
            string sql, titulo;
            #region ISS
            //Leituras
            //( (Total de Leituras da cidade + Leituras Irregulares) * Valor) * ( (Índice1 + Índice2) / 2)

            sql = "SELECT EMP_ID, CID_ID, SUM(LIV_TOTAL) AS Leituras, SUM(LIV_ERROS) AS Erros, "
                + " SUM(LIV_DCM) AS DCM, SUM(LIV_DSM) AS DSM, SUM(LIV_MEDIA) AS MEDIA "
                + " FROM WTR_LIVROS "
                + " WHERE (LIV_MES = " + cbxMes.SelectedValue.ToString() + ") AND (ETP_ID < 13) AND (EMP_ID = 1) "
                + " OR (LIV_MES = " + cbxMes.SelectedValue.ToString() + ") AND (ETP_ID > 12) AND (EMP_ID = 1) "
                + " GROUP BY CID_ID, EMP_ID "
                + " ORDER BY CID_ID ";

            if (txtIrregular.Text.Trim() == "") txtIrregular.Text = "0";
            qtdIrreg = Convert.ToInt32(txtIrregular.Text.Trim());



            //Entregas
            //( (Total de Leituras da cidade + DCM + DSM) * Índice1) * Índice2

            //Alternadas
            //( (Total de Alternadas por cidade) * Índice1)

            //
            //
            //
            //
            //	sql = "select CID_NOME, CID_ISS from WTR_CIDADES order by CID_ID";

            titulo = "TOTAL DE SERVIÇOS EXECUTADOS POR CIDADES PARA PAGAMENTO DO ISS";

            printISS(sql, titulo);
            #endregion
        }
    }
    protected void linkTotEtapas_Click(object sender, EventArgs e)
    {
        if (cbxMes.Text.Trim() == "Selecione um mês...")
            msg("Selecione um mês.");
        else
        {
            string sql, titulo;
            #region Total de Etapas
            // Contador por Leiturista
            sql = " select LTR_ID, CID_ID, LIV_MES, SUM(LIV_TOTAL) as Leituras, "
                + " SUM(LIV_ERROS) AS Erros, SUM(LIV_DCM) AS DCM, SUM(LIV_DSM) AS DSM, "
                + " SUM(LIV_MEDIA) AS MEDIA from WTR_LIVROS "
                + " where ( (LIV_MES = " + cbxMes.SelectedIndex.ToString()
                + " and ETP_ID < 13) or ( LIV_MES = " + Convert.ToString(cbxMes.SelectedIndex - 1)
                + " and ETP_ID > 12) ) "
                + " and CLI_ID = " + Request.Cookies["CliID"].Value.ToString().Trim()
                + " and EMP_ID = " + Request.Cookies["EmpID"].Value.ToString().Trim()
                + " group by LTR_ID, LIV_MES, CID_ID "
                + " order by LIV_MES ";

            titulo = "RESUMO DE LEITURA, MEDIA E REFATURA POR LEITURISTA";

            printTotalEtapas(sql, titulo);
            #endregion
        }
    }
    protected void linkDCertaRaviso_Click(object sender, EventArgs e)
    {

    }
}