﻿using System;
using System.Activities.Expressions;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WaterSyncLite.Class;

public partial class ViewRoteiro : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        this.Master.LoadHeader(this.Master, Request.Path.Substring(Request.Path.LastIndexOf("/") + 1));
        if (!IsPostBack)
        {
            cbxMes.SelectedIndex = DateTime.Now.Month;
            txtAno.Text = DateTime.Now.Year.ToString();
            Data db = new Data();



            #region LEITURISTAS
            DataTable dt = db.GetDt(carregaConexao(), "WTR_LEIT", "SELECT LTR_ID, LTR_NOME FROM "
                + " WTR_LEITURISTAS WHERE CLI_ID = " + Request.Cookies["CliID"].Value.ToString().Trim()
                + " AND EMP_ID = " + Request.Cookies["EmpID"].Value.ToString().Trim()
                + " ORDER BY LTR_NOME");

            DataRow dtr = dt.NewRow();
            dtr["LTR_NOME"] = "Selecione um leiturista...";
            dtr["LTR_ID"] = 0;
            dt.Rows.Add(dtr);
            if (dt != null)
            {
                cbxLeit.DataSource = dt;
                cbxLeit.DataTextField = "LTR_NOME";
                cbxLeit.DataValueField = "LTR_ID";
                cbxLeit.DataBind();
                cbxLeit.SelectedValue = "0";

            }
            #endregion
        }
    }


    private void CarregaDados()
    {
        try
        {
            if (cbxMes.SelectedIndex != 0 && txtAno.Text.Trim().Length > 0)
            {
                Data db = new Data();


                string where = "WHERE l.liv_mes = " + cbxMes.SelectedIndex + " and l.liv_ano = " + txtAno.Text;
                
                if (txtGrupo.Text.Trim().Length > 0 &&
                    Request.Cookies["CliID"].Value.PadLeft(2, '0').ToString().Trim() == "17")
                    where += " and grupo_id = " + txtGrupo.Text;
                if (txtRota.Text.Trim().Length > 0)
                    where += " and loc_id = " + txtRota.Text;
                string sql = "";
                if (Request.Cookies["CliID"].Value.PadLeft(2, '0').ToString().Trim() == "17")
                {

                    sql =
                        "SELECT l.grupo_id as Grupo,l.loc_id as rota,l.lei_sequencia as Sequência,ltr_nome as Leiturista,l.lei_endereco+','+convert(varchar,l.lei_numero) as Endereço " +
                        "FROM wtr_leituras l  inner join wtr_leituristas leit on (leit.ltr_id = l.ltr_id) " +
                        where +
                        " and l.lei_sequencia = (select min(lei_sequencia) from wtr_leituras sub where sub.grupo_id = l.grupo_id and sub.liv_mes = " +
                        cbxMes.SelectedIndex + " and sub.liv_ano = " + txtAno.Text + " and loc_id = l.loc_id) " +
                        "UNION " +
                        "SELECT l.grupo_id as Grupo,l.loc_id as rota,l.lei_sequencia as Sequência,ltr_nome as Leiturista,l.lei_endereco+','+convert(varchar,l.lei_numero) as Endereço " +
                        "FROM wtr_leituras l  inner join wtr_leituristas leit on (leit.ltr_id = l.ltr_id)" +
                        where +
                        " and l.lei_sequencia = (select max(lei_sequencia) from wtr_leituras sub where sub.grupo_id = l.grupo_id and sub.liv_mes = " + cbxMes.SelectedIndex + " and sub.liv_ano = " + txtAno.Text + " and loc_id = l.loc_id)";
                    sql += " order by l.grupo_id,loc_id,lei_sequencia";

                }
                else
                {
                    sql =
                        "SELECT l.loc_id as rota,l.lei_seq as Sequência,ltr_nome as Leiturista,l.lei_endereco+','+convert(varchar,l.lei_numero) as Endereço " +
                        "FROM wtr_leituras l  inner join wtr_leituristas leit on (leit.ltr_id = l.ltr_id) " +
                        where +
                        " and l.lei_seq = (select min(lei_seq) from wtr_leituras sub where sub.liv_mes = " +
                        cbxMes.SelectedIndex + " and sub.liv_ano = " + txtAno.Text + " and loc_id = l.loc_id) " +
                        "UNION " +
                        "SELECT l.loc_id as rota,l.lei_seq as Sequência,ltr_nome as Leiturista,l.lei_endereco+','+convert(varchar,l.lei_numero) as Endereço " +
                        "FROM wtr_leituras l  inner join wtr_leituristas leit on (leit.ltr_id = l.ltr_id)" +
                        where +
                        " and l.lei_seq = (select max(lei_seq) from wtr_leituras sub where sub.liv_mes = " + cbxMes.SelectedIndex + " and sub.liv_ano = "+txtAno.Text+" and loc_id = l.loc_id)";
                        sql += " order by loc_id,lei_seq";
                }


                

                DataTable dt = db.GetDt(carregaConexao(), "WTR_LEITURAS", sql);
                gridConsulta.DataSource = dt;
                gridConsulta.DataBind();

            }
            else
            {
                ShowMessage("Por favor, Preencha os campos mês e ano.");
            }

        }
        catch (Exception ex)
        {
            ShowMessage("Erro ao buscar dados -"+ex.Message);
            throw;
        }
        
    }

    protected void ShowMessage(string msg)
    {
        lblMsg.Text = msg;
        PanelMsg.Visible = true;
    }
    
    protected String carregaConexao()
    {
        String StrCon = "";
        if (Request.Cookies["StrCon"] != null)
        {
            if (Request.Cookies["StrCon"].Value.ToString().Length > 0)
                StrCon = Request.Cookies["StrCon"].Value.ToString();
        }
        return StrCon.Replace('|', ';').Replace("usuario", xml.GetConexaoValue(Server.MapPath("~"), "LOCAL", "USER")).Replace("senha", xml.GetConexaoValue(Server.MapPath("~"), "LOCAL", "PASSWORD"));
    }

    protected void btPesquisar_Click(object sender, EventArgs e)
    {
        PanelMsg.Visible = false;
        CarregaDados();
    }
}