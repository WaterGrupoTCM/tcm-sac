﻿using System.Data.SQLite;
using System.Data;
using System.Globalization;
using System.Data.SqlClient;
using System.IO;
using System.Xml;
using System.Xml.XPath;
using System.Data;
using System.Drawing;
using System.Text;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WaterSyncLite.Class;
using WaterSyncLite.MDL;
using WaterSyncLite.DAL;

using System.IO.Compression;
using OpenNETCF.Net.Ftp;
using System.Threading;




public partial class ViewSincronizar : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            this.Master.LoadHeader(this.Master, Request.Path.Substring(Request.Path.LastIndexOf("/") + 1));
            if (!IsPostBack)
            {
                TabName.Value = Request.Form[TabName.UniqueID];

                #region LEITURISTAS
                Data db = new Data();
                DataTable dt = db.GetDt(carregaConexao(), "WTR_LEIT", "SELECT LTR_ID, LTR_NOME FROM "
                    + " WTR_LEITURISTAS WHERE CLI_ID = " + Request.Cookies["CliID"].Value.ToString().Trim()
                    + " AND EMP_ID = " + Request.Cookies["EmpID"].Value.ToString().Trim()
                    + " ORDER BY LTR_NOME");

               
                if (dt != null)
                {
                    ddlLtr.DataSource = dt;
                    ddlLtr.DataTextField = "LTR_NOME";
                    ddlLtr.DataValueField = "LTR_ID";
                    ddlLtr.DataBind();

                }
                #endregion
               
                //incluirConfig();
                Empresa emp = new Empresa(Convert.ToInt32(Request.Cookies["CliID"].Value.ToString()), Convert.ToInt32(Request.Cookies["EmpID"].Value.ToString()), "");

                carregarConfiguracoes();

               
            }
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(GetType(), "Message", "<SCRIPT LANGUAGE='javascript'>alert('" + ex.Message.ToString().Replace("'", "") + "');</script>");

        }

    }

    protected void carregarConfiguracoes()
    {
        try
        {

            Data db = new Data();
            DataTable dt = db.GetDt(carregaConexao().Replace('|', ';'), "WTR_LEITURISTAS", "select * from wtr_leituristas where ltr_id = " + ddlLtr.SelectedValue.ToString() + " and cli_id = " + Request.Cookies["CliID"].Value.ToString());

            if (dt.DefaultView.Count > 0)
            {
                if (dt.DefaultView[0].Row["ATUCADASTRAL"].ToString() == "1")
                    chkAtualizacaoCadastral.Checked = true;
                else
                    chkAtualizacaoCadastral.Checked = false;

                if (dt.DefaultView[0].Row["ALTERACAO"].ToString() == "1")
                    chkAlteracao.Checked = true;
                else
                    chkAlteracao.Checked = false;

                if (dt.DefaultView[0].Row["SIMULTANEA"].ToString() == "1")
                    chkImpSimultanea.Checked = true;
                else
                    chkImpSimultanea.Checked = false;

                if (dt.DefaultView[0].Row["LEITURA_LEITURA"].ToString() == "1")
                    rbLeituraLeitura.Checked = true;
                else
                    rbLeituraLeitura.Checked = false;

                if (dt.DefaultView[0].Row["ENVIOMANUAL"].ToString() == "1")
                    rbManual.Checked = true;
                else
                    rbManual.Checked = false;

                if (dt.DefaultView[0].Row["LOCALIZACAO"].ToString() == "1")
                    chkLocalizacao.Checked = true;
                else
                    chkLocalizacao.Checked = false;

                if (dt.DefaultView[0].Row["LEITURA_ANTERIOR"].ToString() == "1")
                    chkLeituraAnterior.Checked = true;
                else
                    chkLeituraAnterior.Checked = false;

                if (dt.DefaultView[0].Row["LEITURA_TEMPO"].ToString() != "0")
                    txtTempoLeitura.Text = dt.DefaultView[0].Row["LEITURA_TEMPO"].ToString();
                else{
                    txtTempoLeitura.Text = "0";
                    panelLeituraTempo.Visible = false;
                }

                if (dt.DefaultView[0].Row["LOCALIZACAO_TEMPO"].ToString() != "0")
                    txtTempoLocalizacao.Text = dt.DefaultView[0].Row["LOCALIZACAO_TEMPO"].ToString();
                else{
                    txtTempoLocalizacao.Text = "0";
                    panelTempoLocalizacao.Visible = false;
                }

                if (dt.DefaultView[0].Row["IMPBKPLEITURA"].ToString() != "0")
                    txtBkpImp.Text = dt.DefaultView[0].Row["IMPBKPLEITURA"].ToString();
                else{
                    txtBkpImp.Text = "0";
                    panelBkpImp.Visible = false;
                }

                if (dt.DefaultView[0].Row["REIMPRESSAO"].ToString() == "1")
                    chkReImpressao.Checked = true;
                else
                    chkReImpressao.Checked = false;

                if (dt.DefaultView[0].Row["FORMA_ENTREGA"].ToString() == "1")
                    chkTipoEntrega.Checked = true;
                else
                    chkTipoEntrega.Checked = false;

                if (dt.DefaultView[0].Row["TELASIMPLES"].ToString() == "1")
                    chkTelaSimples.Checked = true;
                else
                    chkTelaSimples.Checked = false;

                if (dt.DefaultView[0].Row["IMPLATLONG"].ToString() == "1")
                    chkImpLatLong.Checked = true;
                else
                    chkImpLatLong.Checked = false;

                if (dt.DefaultView[0].Row["CAMERA"].ToString() == "1")
                    chkCamera.Checked = true;
                else
                    chkCamera.Checked = false;

            }


        }
        catch (Exception)
        {

            throw;
        }

    }

    protected String carregaConexao()
    {
        String StrCon = "";
        if (Request.Cookies["StrCon"] != null)
        {
            if (Request.Cookies["StrCon"].Value.ToString().Length > 0)
                StrCon = Request.Cookies["StrCon"].Value.ToString();
        }
        return StrCon.Replace('|', ';').Replace("usuario", xml.GetConexaoValue(Server.MapPath("~"), "LOCAL", "USER")).Replace("senha", xml.GetConexaoValue(Server.MapPath("~"), "LOCAL", "PASSWORD"));
    }
    protected void incluirConfig()
    {
        string sql = "insert into wtr_config(cli_id,emp_id,cfg_id,usr_id) values (" + Request.Cookies["CliID"].Value.ToString() + "," + Request.Cookies["EmpID"].Value.ToString() + ",1," + Request.Cookies["user"].Value.ToString() + ")";
        Data db = new Data();
        DataTable dt = db.GetDt(carregaConexao().Replace('|', ';'), "WTR_CONFIG", "select * from wtr_config where usr_id = " + Request.Cookies["user"].Value.ToString() + " and cli_id = " + Request.Cookies["CliID"].Value.ToString());
        if (dt.DefaultView.Count <= 0)
            db.Exe(carregaConexao().Replace('|', ';'), "wtr_config", sql);


    }

    protected void atualizarConfiguracoes()
    {
        try
        {
            Data db = new Data();

            if (rbLeituraTempo.Checked)
                panelLeituraTempo.Visible = true;
            else
                panelLeituraTempo.Visible = false;

            if (chkTempoLocalizacao.Checked)
                panelTempoLocalizacao.Visible = true;
            else
                panelTempoLocalizacao.Visible = false;

            if (chkBkpLeituras.Checked)
                panelBkpImp.Visible = true;
            else
                panelBkpImp.Visible = false;

            String sql = "update wtr_leituristas set ";

            if (chkAlteracao.Checked)
                sql += "ALTERACAO = 1";
            else
                sql += "ALTERACAO = 0";

            if (chkAtualizacaoCadastral.Checked)
                sql += ",ATUCADASTRAL = 1";
            else
                sql += ",ATUCADASTRAL = 0";

            if (chkImpSimultanea.Checked)
                sql += ",SIMULTANEA = 1";
            else
                sql += ",SIMULTANEA = 0";

            if (rbLeituraLeitura.Checked)
                sql += ",LEITURA_LEITURA = 1";
            else
                sql += ",LEITURA_LEITURA = 0";

            if (rbManual.Checked)
                sql += ",ENVIOMANUAL = 1";
            else
                sql += ",ENVIOMANUAL = 0";

            if (chkLocalizacao.Checked)
                sql += ",LOCALIZACAO = 1";
            else
                sql += ",LOCALIZACAO = 0";

            if (chkLeituraAnterior.Checked)
                sql += ",LEITURA_ANTERIOR = 1";
            else
                sql += ",LEITURA_ANTERIOR = 0";

            if (rbLeituraTempo.Checked)
                sql += ",LEITURA_TEMPO = " + txtTempoLeitura.Text;
            else
                sql += ",LEITURA_TEMPO = 0";

            if (chkTempoLocalizacao.Checked)
                sql += ",LOCALIZACAO_TEMPO = " + txtTempoLocalizacao.Text;
            else
                sql += ",LOCALIZACAO_TEMPO = 0";

            if (chkBkpLeituras.Checked)
                sql += ",IMPBKPLEITURA = " + txtBkpImp.Text;
            else
                sql += ",IMPBKPLEITURA = 0";

            if (chkReImpressao.Checked)
                sql += ",REIMPRESSAO = 1";
            else
                sql += ",REIMPRESSAO = 0";

            if (chkTipoEntrega.Checked) 
                sql += ",FORMA_ENTREGA = 1";
            else
                sql += ",FORMA_ENTREGA = 0";

            if (chkTelaSimples.Checked)
                sql += ",TELASIMPLES = 1";
            else
                sql += ",TELASIMPLES = 0";

            if (chkImpLatLong.Checked)
                sql += ",IMPLATLONG = 1";
            else
                sql += ",IMPLATLONG = 0";

            if (chkCamera.Checked)
                sql += ",CAMERA = 1";
            else
                sql += ",CAMERA = 0";



            sql += " where ltr_id = " + ddlLtr.SelectedValue.ToString() + " and cli_id = " +
                   Request.Cookies["CliID"].Value.ToString();


            db.Exe(carregaConexao().Replace('|', ';'), "WTR_CONFIG", sql);


        }
        catch (Exception)
        {

            throw;
        }

    }

    protected void rbLeituraLeitura_CheckedChanged(object sender, EventArgs e)
    {
        atualizarConfiguracoes();
    }

    protected void rbLeituraTempo_CheckedChanged(object sender, EventArgs e)
    {
        atualizarConfiguracoes();
    }

    protected void rbFinalizarLeituras_CheckedChanged(object sender, EventArgs e)
    {
        atualizarConfiguracoes();
    }

    protected void rbManual_CheckedChanged(object sender, EventArgs e)
    {
        atualizarConfiguracoes();
    }

    protected void txtTempoLeitura_TextChanged(object sender, EventArgs e)
    {
        atualizarConfiguracoes();
    }

    protected void chkAlteracao_CheckedChanged(object sender, EventArgs e)
    {
        atualizarConfiguracoes();
    }

    protected void chkAtualizacaoCadastral_CheckedChanged(object sender, EventArgs e)
    {
        atualizarConfiguracoes();
    }

    protected void chkImpSimultanea_CheckedChanged(object sender, EventArgs e)
    {
        atualizarConfiguracoes();
    }

    protected void chkLeituraAnterior_CheckedChanged(object sender, EventArgs e)
    {
        atualizarConfiguracoes();
    }

    protected void chkLocalizacao_CheckedChanged(object sender, EventArgs e)
    {
        atualizarConfiguracoes();
    }

    protected void chkTempoLocalizacao_CheckedChanged(object sender, EventArgs e)
    {
        atualizarConfiguracoes();
    }

    protected void txtTempoLocalizacao_TextChanged(object sender, EventArgs e)
    {
        atualizarConfiguracoes();
    }
    protected void ddlLtr_SelectedIndexChanged(object sender, EventArgs e)
    {
        carregarConfiguracoes();
    }
    protected void chkReImpressao_CheckedChanged(object sender, EventArgs e)
    {
        atualizarConfiguracoes();
    }
    protected void chkTipoEntrega_CheckedChanged(object sender, EventArgs e)
    {
        atualizarConfiguracoes();
    }
    protected void chkCamera_CheckedChanged(object sender, EventArgs e)
    {
        atualizarConfiguracoes();
    }
}

