﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="ViewLeiturista.aspx.cs" Inherits="ViewLeiturista" %>
<%@ MasterType VirtualPath="~/Site.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

    <link href="Content/lightbox.css" rel="stylesheet" />
    <link href="Content/prettyPhoto.css" rel="stylesheet" />
    <link href="Content/StyleSheet.css" rel="stylesheet" />




    <style type="text/css">
        @media print {


            .row {
                margin-right: -15px;
                margin-left: -15px;
                padding-top: 0px;
                font-size: x-small;
            }

            col-1, .col-2, .col-3, .col-4, .col-5, .col-6, .col-7, .col-8, .col-9, .col-10, .col-11, .col-12, .col-sm-1, .col-sm-2, .col-sm-3, .col-sm-4, .col-sm-5, .col-sm-6, .col-sm-7, .col-sm-8, .col-sm-9, .col-sm-10, .col-sm-11, .col-sm-12, .col-lg-1, .col-lg-2, .col-lg-3, .col-lg-4, .col-lg-5, .col-lg-6, .col-lg-7, .col-lg-8, .col-lg-9, .col-lg-10, .col-lg-11, .col-lg-12 {
                position: relative;
                min-height: 1px;
                padding-right: 15px;
                padding-left: 15px;
                top: 0px;
                left: 0px;
            }

            .col-1, .col-2, .col-3, .col-4, .col-5, .col-6, .col-7, .col-8, .col-9, .col-10, .col-11, .col-12 {
                float: left;
            }

            .col-1 {
                width: 8.333333333333332%;
            }

            .col-2 {
                width: 16.666666666666664%;
            }

            .col-3 {
                width: 25%;
            }

            .col-4 {
                width: 33.33333333333333%;
            }

            .col-5 {
                width: 41.66666666666667%;
            }

            .col-6 {
                width: 50%;
            }

            .col-7 {
                width: 58.333333333333336%;
            }

            .col-8 {
                width: 66.66666666666666%;
            }

            .col-9 {
                width: 75%;
            }

            .col-10 {
                width: 83.33333333333334%;
            }

            .col-11 {
                width: 91.66666666666666%;
            }

            .col-12 {
                width: 100%;
            }

            td {
                padding: 0px;
            }
        }


        .divbutton {
            /*height: 100px;*/
        }

        label {
            margin-bottom: 0px;
            font-weight: inherit;
        }

        labelPesquisa {
            margin-bottom: 0px;
            font-weight: inherit;
        }

        th {
            text-align: center;
        }

        td {
            padding: 0px;
        }

        .FixedHeader {
            position: absolute;
            font-weight: bold;
            vertical-align: text-bottom;
            width: 100%;
        }

        .painelInfo {
            min-height: 120px;
        }


        a.fill-div {
            text-align: center;
            display: block;
            height: 100%;
            width: 100%;
            text-decoration: none;
        }

        .autocomplete_list {
            visibility: visible;
            margin: 0px !important;
            padding: 1px;
            background-color: GrayText;
            color: Black;
            border: buttonshadow;
            border-width: 1px;
            border-style: solid;
            cursor: default;
            text-align: left;
            list-style-type: none;
            font-weight: normal;
            font-family: Arial;
            font-size: 16px;
        }

        .autocomplete_highlighted_listitem {
            background-color: ButtonFace;
            color: Blue;
            padding: 3px;
        }

        .autocomplete_listitem {
            background-color: Window;
            color: Black;
            font-family: Arial;
            font-size: 12px;
            padding: 3px;
        }

        .carousel {
            width: 600px;
            padding-left: 21px;
        }

        /* Indicators list style */
        .article-slide .carousel-indicators {
            bottom: 0;
            left: 0;
            margin-left: 59px;
            width: 100%;
        }
            /* Indicators list style */
            .article-slide .carousel-indicators li {
                border: medium none;
                border-radius: 0;
                float: left;
                height: 54px;
                margin-bottom: 5px;
                margin-left: 0;
                margin-right: 5px !important;
                margin-top: 0;
                width: 100px;
            }
            /* Indicators images style */
            .article-slide .carousel-indicators img {
                border: 2px solid #FFFFFF;
                float: left;
                height: 54px;
                left: 0;
                width: 100px;
            }
            /* Indicators active image style */
            .article-slide .carousel-indicators .active img {
                border: 2px solid #428BCA;
                opacity: 0.7;
            }

        .ligacao {
        }

        .esconder {
            visibility: hidden;
        }
    </style>

    <script type="text/javascript">

        function printDiv(divName) {
            var printContents = document.getElementById(divName).innerHTML;
            var originalContents = document.body.innerHTML;

            document.body.innerHTML = printContents;

            window.print();

            document.body.innerHTML = originalContents;
        }



        function imprime() {
            //you can put your contentID which is you want to print.
            var contents = $("#conteudo").html();
            var frame1 = $('<iframe />');
            frame1[0].name = "frame1";
            frame1.css({ "position": "absolute", "top": "-1000000px" });
            $("body").append(frame1);
            var frameDoc = frame1[0].contentWindow ? frame1[0].contentWindow : frame1[0].contentDocument.document ? frame1[0].contentDocument.document : frame1[0].contentDocument;
            frameDoc.document.open();
            //Create a new HTML document.
            frameDoc.document.write('<html><head><title>DIV Contents</title>');
            frameDoc.document.write('</head><body>');
            //Append the external CSS file.
            frameDoc.document.write('<link href="style.css" rel="stylesheet" type="text/css" />');
            //Append the DIV contents.
            frameDoc.document.write(contents);
            frameDoc.document.write('</body></html>');
            frameDoc.document.close();
            setTimeout(function () {
                window.frames["frame1"].focus();
                window.frames["frame1"].print();
                frame1.remove();
            }, 500);
        }

        function printform() {


            var windowUrl = 'about:blank';
            var uniqueName = new Date();
            var windowName = 'Print' + uniqueName.getTime();
            var printWindow = window.open(windowUrl, windowName, 'left=50000,top=50000,width=0,height=0');

            printWindow.document.write(printContent.innerHTML);
            printWindow.document.close();
            printWindow.focus();
            printWindow.print();
            printWindow.close();
        }
    </script>

    <style type="text/css">
        .left {
            padding-left: 10px;
            padding-top: 10px;
        }

        .EstiloGaleria {
            position: fixed;
            left: 29%;
            top: 58%;
            /*width: 300px;*/
            min-height: 250px;
            margin-left: -150px;
            margin-top: -125px;
            background-color: rgb(102, 102, 102);
            color: rgb(255, 255, 255);
            background-color: #666;
            text-align: center;
            z-index: 1000;
            overflow: scroll;
        }

        #gallery {
            background-color: #4f5b69;
            padding: 10px;
            width: auto;
            overflow: scroll;
        }

            #gallery ul {
                list-style: none;
            }

                #gallery ul li {
                    display: inline;
                }

                #gallery ul img {
                    border: 5px solid #444444;
                    border-width: 5px 5px 20px;
                }

                #gallery ul a:hover img {
                    border: 5px solid #fff;
                    border-width: 5px 5px 20px;
                    color: #fff;
                }

                #gallery ul a:hover {
                    color: #fff;
                }
    </style>



    <script type="text/javascript">




        jQuery(window).click(function () {
            //alert('click');
            if (document.getElementById("ContentPlaceHolder1_lblFileUpload").innerHTML.toString() != "teste") {
                //document.getElementById("ContentPlaceHolder1_gallery").style = "visibility: hidden;position: fixed;z-index: 9999;overflow:scroll";
                var self = jQuery(this).closest("tr");
                var col1_value = self.find(".ligacao").text();



            }


            //Carrega painel de informações da pesquisa
            if (document.getElementById("collapseOne").attributes.getNamedItem('aria-expanded').value == "false") {

                document.getElementById("InfoPesquisa").className = "row";
            } else {

                document.getElementById("InfoPesquisa").className = "row esconder";
            }


            document.getElementById("ContentPlaceHolder1_lblFileUpload").innerHTML = "";
            // alert(jQuery(this).text());
        });
        window.onload = function () {

            //esconde galeria
            //document.getElementById("ContentPlaceHolder1_gallery").style = "visibility: hidden;position: fixed;z-index: 9999;overflow:scroll";

            document.getElementById("ContentPlaceHolder1_rbEndereco").style = "margin-bottom:0px;";
            document.getElementById("ContentPlaceHolder1_rbEndereco").checked = true;
            document.getElementById("ContentPlaceHolder1_rbEnderecoEntrega").style = "margin-bottom:0px;";


            //document.getElementById("ContentPlaceHolderLogin_txtAno").type = "number";
        }


        function UploadFile(fileUpload) {
            document.getElementById("ContentPlaceHolder1_lblFileUpload").innerHTML = "teste";
        }


    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">


    <form class="form-horizontal" role="form" runat="server">


        <asp:Label ID="lblFileUpload" runat="server" Text=""></asp:Label>
        <asp:Label ID="lblLigacaoSel" runat="server" Text="" Visible="false"></asp:Label>


        <div id="conteudo">

            <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>


            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                <div class="panel panel-primary">
                    <div class="panel-heading" role="tab" id="headingOne">

                        <div id="my-div">
                            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true"
                                aria-controls="collapseOne" class="fill-div TitulosPanel">Cadastro Leiturista</a>
                        </div>

                    </div>
                    <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                        <div class="panel-body">

                            <div class="row">
                                <div class="col-12">
                                    <asp:Panel ID="PanelMsg" runat="server" Visible="false">
                                        <div class="alert alert-info">
                                            <strong>Atenção! </strong>
                                            <asp:Label ID="lblMsg" runat="server" Text="teste" Visible="false"></asp:Label>
                                        </div>
                                    </asp:Panel>
                                </div>
                            </div>
                            <div class="row" style="padding-top: 10px">
                                <div class="col-sm-6 col-lg-4">
                                    
                                        <div class="input-group">
                                            <span class="input-group-addon" id="">Nome     </span>
                                            <asp:TextBox ID="txtNome" runat="server" CssClass="form-control bfh-number"></asp:TextBox>
                                        </div>
                                </div>
                            

                          
                                <div class="col-sm-6 col-lg-4">
                                        <div class="input-group">
                                            <span class="input-group-addon" id="">Sobrenome</span>
                                            <asp:TextBox ID="txtSobreNome" runat="server" CssClass="form-control bfh-number"></asp:TextBox>
                                        </div>
                                </div>
                          
                                <div class="col-sm-6 col-lg-4">
                                        <div class="input-group">
                                            <span class="input-group-addon" id="">Usuario  </span>
                                            <asp:TextBox ID="txtUsuario" runat="server" CssClass="form-control bfh-number"></asp:TextBox>
                                        </div>
                                </div>
                            </div>

                            <div class="row" style="padding-top: 10px">
                                <div class="col-sm-6 col-lg 6">
                                    <div class="col-sm-4 col-lg-6" style="padding-right: 0px; padding-left: 0px;">
                                        <div class="input-group">
                                            <span class="input-group-addon" id="">Senha    </span>
                                            <asp:TextBox ID="txtSenha" runat="server" CssClass="form-control bfh-number"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="input-group" style="margin-top: 10px">
                                <asp:Button ID="btPesquisar" runat="server" Text="Adicionar" CssClass="btn btn-primary" OnClick="btSalvar_Click" />
                                <asp:Button ID="Button1" runat="server" Text="Cancelar" CssClass="btn btn-primary" OnClick="btCancelar_Click" />
                            </div>

                            <!-- /.row this actually does not appear to be needed with the form-horizontal -->
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <asp:GridView ID="GridLeiturista" runat="server" CellPadding="3" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" >
            <FooterStyle BackColor="White" ForeColor="#000066" />
            <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
            <RowStyle ForeColor="#000066" />
            <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
            <SortedAscendingCellStyle BackColor="#F1F1F1" />
            <SortedAscendingHeaderStyle BackColor="#007DBB" />
            <SortedDescendingCellStyle BackColor="#CAC9C9" />
            <SortedDescendingHeaderStyle BackColor="#00547E" />
        </asp:GridView>
    </form>



</asp:Content>

