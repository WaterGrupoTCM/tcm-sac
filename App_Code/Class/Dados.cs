﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WaterSyncLite.MDL;
namespace WaterSyncLite.Class
{
    public class Dados{
        public static User user;

        public static Empresa Company;

        public static Cidade Cidade;

        public static class Reference
        {
            private static int dia;

            public static int Dia
            {
                get { return Reference.dia; }
                set { Reference.dia = value; }
            }

            private static int mes;

            public static int Mes
            {
                get { return Reference.mes; }
                set { Reference.mes = value; }
            }
            private static int ano;

            public static int Ano
            {
                get { return Reference.ano; }
                set { Reference.ano = value; }
            }
        }

        public static Config config;
    }
}
