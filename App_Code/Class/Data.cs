﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.IO;

namespace WaterSyncLite.Class
{
   public class Data
    {
        //public string StrCon = Conexao.StrCon;
        SqlConnection m_con;

        public Data()
        {
        }

        public String read(String StrCon, String sql, String campo)
        {
            try
            {
                string codigo;
                SqlConnection conn = new SqlConnection(StrCon);
                SqlCommand cmd = new SqlCommand(sql, conn);
                conn.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                reader.Read();
                codigo = reader[campo].ToString();
                conn.Close();
                return codigo;
            }
            catch (Exception ex)
            {
                return null;
            }

        }

        public DataTable GetDt(String StrCon, string tabela, string sql)
        {
            try
            {
                DataSet ds = new DataSet();
                DataTable dt = new DataTable(tabela);
                try
                {
                    if (m_con == null)
                        m_con = new SqlConnection(StrCon);
                    
                    if (m_con.State == ConnectionState.Closed)
                        m_con.Open();

                    SqlDataAdapter da = new SqlDataAdapter(sql, m_con);

                    da.SelectCommand.CommandTimeout = TimeSpan.FromMinutes(60).Seconds;

                    da.Fill(dt);
                }
                catch (SqlException ex)
                {
                    //MessageBox.Show(ex.Message);
                    //Files.WriteLog(xml.GetConfigValue("LOCAL", "LOG"), "LOG.txt", DateTime.Now.ToString() + " - " + ex.Message + "\r\n");
                }
                finally
                {
                    m_con.Close();
                }
                return dt;

            }
            catch (Exception ex)
            {
                //MessageBox.Show("Informação - " + ex.Message);
                return null;
            }
        }

        public DataTable GetDt(String StrCon, string tabela, string sql, string local)
        {
            try
            {
                DataSet ds = new DataSet();
                DataTable dt;
                dt = new DataTable(tabela);
                try
                {
                    if (m_con == null)
                        m_con = new SqlConnection(StrCon);

                    if (m_con.State == ConnectionState.Closed)
                        m_con.Open();

                    SqlDataAdapter da = new SqlDataAdapter(sql, m_con);
                    da.SelectCommand.CommandTimeout = TimeSpan.FromMinutes(60).Seconds;

                    da.Fill(dt);
                }
                catch (SqlException ex)
                {
                    //MessageBox.Show(ex.Message);
                    //Files.WriteLog(xml.GetConfigValue("LOCAL", "LOG"), "LOG.txt", DateTime.Now.ToString() + " - " + ex.Message + "\r\n");
                }
                finally
                {
                    m_con.Close();
                }
                return dt;

            }
            catch (Exception ex)
            {
                //MessageBox.Show("Informação - " + ex.Message);
                //Files.WriteLog(xml.GetConfigValue("LOCAL", "LOG"), "LOG.txt", DateTime.Now.ToString() + " - " + ex.Message + "\r\n");
                return null;
            }
        }

        public bool Exe(String StrCon, string tabela, string sql)
        {
            try
            {
                if (m_con == null)
                    m_con = new SqlConnection(StrCon);

                if (m_con.State == ConnectionState.Closed)
                    m_con.Open();

                SqlDataReader dr;
                SqlCommand objcom = new SqlCommand(sql, m_con);
                dr = objcom.ExecuteReader();
                return true;
            }
            catch (SqlException ex)
            {
                //MessageBox.Show("Informação: " + ex.Message);
                //Files.WriteLog(xml.GetConfigValue("LOCAL", "LOG"), "LOG.txt", DateTime.Now.ToString() + " - " + ex.Message + "\r\n");
                return false;
            }
            finally
            {
                m_con.Close();
            }
        }

        public string Ins(String StrCon, string sql)
        {
            if (m_con == null)
                m_con = new SqlConnection(StrCon);

            if (m_con.State == ConnectionState.Closed)
                m_con.Open();

            SqlCommand objcom = new SqlCommand(sql, m_con);

            try
            {
                SqlDataReader objdtr;
                objdtr = objcom.ExecuteReader();
                objdtr.Close();
                return "";
            }
            catch (SqlException ex)
            {
                //Files.WriteLog(xml.GetConfigValue("LOCAL", "LOG"), "LOG.txt", DateTime.Now.ToString() + " - " + ex.Message + "\r\n");
                return ex.Message;
            }
            finally
            {
                m_con.Close();
            }
        }

        public string GetField(String StrCon, string sql, int campo)
        {
            try
            {
                if (m_con == null)
                    m_con = new SqlConnection(StrCon);

                if (m_con.State == ConnectionState.Closed)
                    m_con.Open();

                SqlCommand objcom = new SqlCommand(sql, m_con);
                SqlDataReader dr = objcom.ExecuteReader();
                if (dr.Read())
                    return dr[campo].ToString();
                else
                    return "";
            }
            catch (Exception ex)
            {
                //Files.WriteLog(xml.GetConfigValue("LOCAL", "LOG"), "LOG.txt", DateTime.Now.ToString() + " - " + ex.Message + "\r\n");
                string erro = ex.Message;
                return "";
            }
            finally
            {
                m_con.Close();
            }
        }

        public string GetField(String StrCon, string sql, int campo, string local)
        {
            try
            {
                if (m_con == null)
                    m_con = new SqlConnection(StrCon);

                if (m_con.State == ConnectionState.Closed)
                    m_con.Open();

                SqlCommand objcom = new SqlCommand(sql, m_con);
                SqlDataReader dr = objcom.ExecuteReader();
                if (dr.Read())
                    return dr[campo].ToString();
                else
                    return "";
            }
            catch (Exception ex)
            {
                //Files.WriteLog(xml.GetConfigValue("LOCAL", "LOG"), "LOG.txt", DateTime.Now.ToString() + " - " + ex.Message + "\r\n");
                string erro = ex.Message;
                return "";
            }
            finally
            {
                m_con.Close();
            }
        }

        public static void msg(string texto)
        {
            //MessageBox.Show(texto.Trim(), "WaterSync", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
        }

        public string NomeMes(int mes)
        {
            try
            {
                if ((mes < 0) || (mes > 12))
                {
                    return "";
                }
                else
                {
                    switch (mes)
                    {
                        case 0:
                            return "";
                            //break;
                        case 1:
                            return "Janeiro";
                            //break;
                        case 2:
                            return "Fevereiro";
                            //break;
                        case 3:
                            return "Março";
                            //break;
                        case 4:
                            return "Abril";
                            //break;
                        case 5:
                            return "Maio";
                            //break;
                        case 6:
                            return "Junho";
                            //break;
                        case 7:
                            return "Julho";
                            //break;
                        case 8:
                            return "Agosto";
                            //break;
                        case 9:
                            return "Setembro";
                            //break;
                        case 10:
                            return "Outubro";
                            //break;
                        case 11:
                            return "Novembro";
                            //break;
                        case 12:
                            return "Dezembro";
                            //break;
                        default:
                            return "";
                    }
                }
            }
            catch (Exception ex)
            {
                return "";
            }
        }
    }
}
