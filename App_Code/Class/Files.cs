﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WaterSyncLite;
using System.IO;

namespace WaterSyncLite.Class
{
    class Files
    {
        

        public static String checkFileExists(string cam, string arquivo, string ext)
        {
            try
            {
                //SaveFileDialog sfd = new System.Windows.Forms.SaveFileDialog();
                String file = "";
                if (System.IO.File.Exists(cam + @"\" + arquivo + "." + ext))
                {
                    //if (MessageBox.Show("Arquivo " + arquivo + " já existe.Deseja substittuí-lo?", "Atenção!", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) == DialogResult.Yes)
                   // {
                        try
                        {
                            System.IO.File.Delete(cam + @"\" + arquivo + "." + ext);
                            file = cam + @"\" + arquivo + "." + ext;
                        }
                        catch (System.IO.IOException e)
                        {
                            Console.WriteLine(e.Message);
                        }
                   // }
                    //else
                   // {
                        //sfd.ShowDialog();
                        //file = sfd.FileName;
                    //}
                }
                else
                {
                    file = cam + @"\" + arquivo + "." + ext;
                }
                return (file.Contains("." + ext) ? file : file + "." + ext);
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.Message);
                return "";
            }
        }

        public static bool checkDir(String diretorio)
        {
            //Verifica se Diretório existe
            try
            {
                DirectoryInfo dir = new DirectoryInfo(diretorio);
                if (!dir.Exists)
                {
                    System.IO.Directory.CreateDirectory(diretorio);
                }
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public static void WriteLog(string path, string file, string text)
        {
            if (!Directory.Exists(path))
                Directory.CreateDirectory(path);

            if (!File.Exists(path + @"\" + file))
            {
                FileStream fs = File.Create(path + @"\" + file);
                fs.Close();
            }
            StreamWriter sw = new StreamWriter(path + @"\" + file, true, Encoding.UTF8);
            text = text.Replace("',", "#");
            text = text.Replace(",", ".");
            text = text.Replace("#", "',");
            text = text.Replace("',.", "'0.");
            text = text.Replace("..", ".");
            sw.WriteLine(text.Trim());
            sw.Close();
        }
    }
}
