using System;
using System.Xml;
using System.Xml.Serialization;
using System.IO;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
namespace WaterSyncLite.Class
{
    /// <summary>
    /// Summary description for xml.
    /// </summary>
    public class xml
    {
        
        public xml()
        {

        }


        public static string GetConfigValue(string appPath, string tab, string element, string CliId)
        {
            string res = "";

            try
            {

                //string appPath = @"E:\bkpLucas\Sistema\Sistema\WebSites\WaterSyncWebSiteAjax\Configs\" + CliId.ToString().PadLeft(2, '0') + "\\";
                //appPath = "C:\\Inetpub\\wwwroot\\WaterSyncSQlite\\Configs\\" + CliId.ToString().PadLeft(2, '0') + "\\";
                //appPath =  "~/Configs/" + CliId.ToString().PadLeft(2, '0') + "//";
                //appPath += @"Configs\" + CliId.ToString().PadLeft(2, '0') + @"\";
                if (File.Exists(@appPath + @"\Configs\" + CliId.PadLeft(2,'0') +@"\Config.xml"))
                {

                    XmlTextReader xr = new XmlTextReader(@appPath + @"\Configs\" + CliId.PadLeft(2, '0') + @"\Config.xml");
                    string currTab = "";

                    while (xr.Read())
                    {
                        if (xr.Name == tab)
                            currTab = tab;
                        else if (currTab == tab && xr.Name == element)
                        {
                            res = xr.ReadString();
                            break;
                        }
                    }
                }
                return res;
            }
            catch (Exception ex)
            {
                log("GetConfigValue - Erro: " + ex.Message, CliId);
                return "";
            }
        }

        public static string GetConexaoValue(string appPath, string tab, string element)
        {
            string res = "";

            try
            {

                if (File.Exists(@appPath + @"\Conexao.xml"))
                {

                    XmlTextReader xr = new XmlTextReader(@appPath + @"\Conexao.xml");
                    string currTab = "";

                    while (xr.Read())
                    {
                        if (xr.Name == tab)
                            currTab = tab;
                        else if (currTab == tab && xr.Name == element)
                        {
                            res = xr.ReadString();
                            break;
                        }
                    }
                }
                return res;
            }
            catch (Exception ex)
            {
                //log("GetConexaoValue - Erro: " + ex.Message, CliId);
                return "";
            }
        }

        public static string GetSchemaValue(string appPath, string tab, string element, string CliId)
        {
            string res = "";

            try
            {
                //string appPath = @"E:\bkpLucas\Sistema\Sistema\WebSites\WaterSyncWebSiteAjax\Schemas\" + CliId.ToString().PadLeft(2, '0') + "\\";
                //appPath += @"Schemas\" + CliId.ToString().PadLeft(2, '0') + @"\"; 
                //appPath = "C:\\Inetpub\\wwwroot\\WaterSyncSQlite\\Schemas\\" + CliId.ToString().PadLeft(2, '0') + "\\";
                //appPath = "~/Schemas/" + CliId.ToString().PadLeft(2, '0') + "//";
                //string appPath = "C:\\Users\\Desenvolvimento\\Sistema\\Agua\\WaterSyncWEB";
                //string appPath = HttpContext.Current.Request.ApplicationPath; 
                if (File.Exists(@appPath + @"\Schemas\" + CliId.PadLeft(2, '0') + @"\Schema_Cfg.xml"))
                {
                    XmlTextReader xr = new XmlTextReader(@appPath + @"\Schemas\" + CliId.PadLeft(2, '0') + @"\Schema_Cfg.xml");
                    string currTab = "";

                    while (xr.Read())
                    {
                        if (xr.Name == tab)
                            currTab = tab;
                        else if (currTab == tab && xr.Name == element)
                        {
                            res = xr.ReadString();
                            break;
                        }
                    }
                }
                return res;
            }
            catch (Exception ex)
            {
                log("GetConfigValue - Erro: " + ex.Message, CliId);
                return "";
            }
        }

        public static string GetImportValue(string appPath, string tab, string element, string CliId)
        {
            string res = "";
            

            try
            {
                //string appPath = @"E:\bkpLucas\Sistema\Sistema\WebSites\WaterSyncWebSiteAjax\IMPORT\" + CliId.ToString().PadLeft(2, '0') + "\\";
                //appPath += @"IMPORT\" + CliId.ToString().PadLeft(2, '0') + @"\"; 
                //string appPath = "C:\\Users\\Desenvolvimento\\Sistema\\Agua\\WaterSyncWEB";
                // string appPath = HttpContext.Current.Request.ApplicationPath; 
                if (File.Exists(@appPath + @"\Import.xml"))
                {
                    XmlTextReader xr = new XmlTextReader(@appPath + @"\Import.xml");
                    string currTab = "";

                    while (xr.Read())
                    {
                        if (xr.Name == tab)
                            currTab = tab;
                        else if (currTab == tab && xr.Name == element.ToUpper())
                        {
                            res = xr.ReadString();
                            break;
                        }
                    }
                }
                return res;
            }
            catch (Exception ex)
            {
                log("GetImportValue - Erro: " + ex.Message, CliId);
                return "";
            }
        }

        public static string GetExportValue(string appPath, string tab, string element, string CliId)
        {
            string res = "";

            try
            {
                //string appPath = @"E:\bkpLucas\Sistema\Sistema\WebSites\WaterSyncWebSiteAjax\EXPORT\" + CliId.ToString().PadLeft(2, '0') + "\\";
                appPath += @"IMPORT\" + CliId.ToString().PadLeft(2, '0') + @"\"; 
                //string appPath = "C:\\Users\\Desenvolvimento\\Sistema\\Agua\\WaterSyncWEB";
                // string appPath = HttpContext.Current.Request.ApplicationPath; 
                if (File.Exists(@appPath + @"\Export.xml"))
                {
                    XmlTextReader xr = new XmlTextReader(@appPath + @"\Export.xml");
                    string currTab = "";

                    while (xr.Read())
                    {
                        if (xr.Name == tab)
                            currTab = tab;
                        else if (currTab == tab && xr.Name == element.ToUpper())
                        {
                            res = xr.ReadString();
                            break;
                        }
                    }
                }
                return res;
            }
            catch (Exception ex)
            {
                log("GetExportValue - Erro: " + ex.Message,CliId);
                return "";
            }
        }

        public static void log(string texto, string CliId)
        {
            try
            {
                //string appPath = @"E:\bkpLucas\Sistema\Sistema\WebSites\WaterSyncWebSiteAjax\LOG\" + CliId.ToString().PadLeft(2, '0') + "\\";
                string appPath = HttpContext.Current.Request.ApplicationPath + @"\\LOG\\" + CliId.ToString().PadLeft(2, '0') + @"\\"; ;
                //string appPath = "C:\\Users\\Desenvolvimento\\Sistema\\Agua\\WaterSyncWEB";
                //string appPath = HttpContext.Current.Request.ApplicationPath; 
                if (!File.Exists(appPath+ @"\Log.txt"))
                	File.Create(appPath+ @"\Log.txt");

                StreamWriter sw = new StreamWriter(appPath + @"\Log.txt", true);
                sw.WriteLine(texto + " - " + DateTime.Now);
                sw.Close();
            }
            catch (Exception ex)
            {
                msg(ex.Message);
            }
        }


        public static void msg(string texto)
        {
            try
            {
                //MessageBox.Show(texto, "WaterSync", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
            }
            catch (Exception ex)
            {
               // MessageBox.Show(ex.Message, "WaterSync", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
            }
        }

        internal static object GetConfigValue(string caminhoProjeto, string v1, string v2)
        {
            throw new NotImplementedException();
        }
    }
}
