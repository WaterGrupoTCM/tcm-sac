﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WaterSyncLite.MDL;
using WaterSyncLite.Class;
using System.Data;

namespace WaterSyncLite.DAL
{
    public class CidadeDAL
    {

        public Cidade getCid(int cliId,String StrCon)
        {
            Cidade cid = new Cidade();
            Data dt = new Data();
            DataTable dt_usr = dt.GetDt(StrCon, "WTR_CIDADES", "SELECT CLI_ID, EMP_ID,CID_ID,CID_NOME FROM WTR_CIDADES WHERE CLI_ID='" + cliId + "' ");

            try
            {
                if (dt_usr.Rows.Count > 0)
                {
                    cid.CliID = Convert.ToInt32(dt_usr.DefaultView[0].Row["CLI_ID"]);
                    cid.EmpID = Convert.ToInt32(dt_usr.DefaultView[0].Row["EMP_ID"]);
                    cid.Nome = dt_usr.DefaultView[0].Row["CID_NOME"].ToString();
                    cid.CidID = Convert.ToInt32(dt_usr.DefaultView[0].Row["CID_ID"]);
                }
                return cid;
            }
            catch (Exception ex)
            {
                return cid;
            }
        }
    }
}
