﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WaterSyncLite.MDL;
using WaterSyncLite.Class;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WaterSyncLite.DAL
{

    public class LeituristaDAL
    {
        
        SqlConnection conexao = null;



        public Boolean insert(Leiturista leiturista,String StrCon)
        {
            conexao = new SqlConnection(StrCon);
            String sql = "INSERT INTO WTR_LEITURISTAS (CLI_ID,EMP_ID,LTR_ID,LTR_NOME,LTR_SOBRENOME,LTR_TIPO,LTR_ENVIOFOTO,LTR_PRINT) VALUES(@CLI,@EMP,@ID,@NOME,@SOBRENOME,@TIPO,@ENVIOFOTO,@PRINT)";
            SqlCommand cmd = new SqlCommand(sql, conexao);
            cmd.Parameters.AddWithValue("@CLI", leiturista.Empresa.CliId);
            cmd.Parameters.AddWithValue("@EMP", leiturista.Empresa.EmpId);
            cmd.Parameters.AddWithValue("@ID", leiturista.Id);
            cmd.Parameters.AddWithValue("@NOME", leiturista.Nome);
            cmd.Parameters.AddWithValue("@SOBRENOME", leiturista.Sobrenome);
            cmd.Parameters.AddWithValue("@TIPO", leiturista.Tipo);
            cmd.Parameters.AddWithValue("@ENVIOFOTO", leiturista.EnvioFoto);
            cmd.Parameters.AddWithValue("@PRINT", leiturista.Print);
            conexao.Open();
            try
            {
                cmd.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
            finally
            {
                conexao.Close();
            }
        }

        public Boolean update(Leiturista leiturista, String StrCon)
        {
            conexao = new SqlConnection(StrCon);
            String sql = "UPDATE WTR_LEITURISTAS SET LTR_NOME = @NOME, LTR_SOBRENOME = @SOBRENOME, LTR_TIPO=@TIPO, LTR_ENVIOFOTO=@ENVIOFOTO, LTR_PRINT=@PRINT, WHERE CLI_ID=@CLI, EMP_ID=@EMP, LTR_ID=@ID";
            SqlCommand cmd = new SqlCommand(sql, conexao);
            cmd.Parameters.AddWithValue("@CLI", leiturista.Empresa.CliId);
            cmd.Parameters.AddWithValue("@EMP", leiturista.Empresa.EmpId);
            cmd.Parameters.AddWithValue("@ID", leiturista.Id);
            cmd.Parameters.AddWithValue("@NOME", leiturista.Nome);
            cmd.Parameters.AddWithValue("@SOBRENOME", leiturista.Sobrenome);
            cmd.Parameters.AddWithValue("@TIPO", leiturista.Tipo);
            cmd.Parameters.AddWithValue("@ENVIOFOTO", leiturista.EnvioFoto);
            cmd.Parameters.AddWithValue("@PRINT", leiturista.Print);
            conexao.Open();
            try
            {
                cmd.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
            finally
            {
                conexao.Close();
            }
        }


        public bool listarLeituritas(String cliID, String empID, DropDownList cbx, String StrCon)
        {
            try
            {
                Data db = new Data(); ;
                conexao = new SqlConnection(StrCon);
                DataTable dt = db.GetDt(StrCon, "WTR_LEIT", "SELECT LTR_ID, LTR_NOME FROM "
                        + " WTR_LEITURISTAS WHERE CLI_ID = " + cliID
                        + " AND EMP_ID = " + empID
                        + " ORDER BY LTR_NOME");

                DataRow dtr = dt.NewRow();
                dtr["LTR_NOME"] = "Selecione um leiturista...";
                dtr["LTR_ID"] = 0;
                dt.Rows.Add(dtr);
                if (dt != null)
                {
                    cbx.DataSource = dt;
                    cbx.DataTextField = "LTR_NOME";
                    cbx.DataValueField = "LTR_ID";
                    cbx.DataBind();
                    
                    cbx.Text = "Selecione um leiturista...";
                }
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
            finally
            {
                conexao.Close();
            }
        }



    }
}

