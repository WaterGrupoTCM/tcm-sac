﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using WaterSyncLite.Class;
using WaterSyncLite.MDL;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WaterSyncLite.DAL
{
    public class EmpresaDAL
    {
        private string StrCon = Conexao.StrCon;
        SqlConnection conexao = null;
        public EmpresaDAL()
        {
        }

        public Boolean insert(Empresa emp)
        {
            string StrCon2 = ""; 
            conexao = new SqlConnection(StrCon2);
            String sql = "";
            SqlCommand cmd = new SqlCommand(sql, conexao);
            cmd.Parameters.AddWithValue("@CLIID", emp.CliId);
            cmd.Parameters.AddWithValue("@EMPID", emp.EmpId);
            cmd.Parameters.AddWithValue("@RSOCIAL", emp.RazaoSocial);
            conexao.Open();
            try
            {
                cmd.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
            finally
            {
                conexao.Close();
            }
        }

        public Boolean update(Empresa emp)
        {
            string StrCon2 = ""; 
            conexao = new SqlConnection(StrCon2);
            String sql = "UPDATE WTR_EMPRESAS SET EMP_RAZAO = @RAZAO, EMP_BAIRRO = @BAIRRO, EMP_CEP = @CEP, EMP_CIDADE = @CIDADE,EMP_CONTATO1 = @CONT1, ";
            sql += " EMP_CONTATO2 = @CONT2, EMP_FONE1=@FONE1, EMP_FONE2 = @FONE2,EMP_HOMEPAGE=@HOMEPAGE, EMP_UF=@UF,EMP_EMAIL=@EMAIL,EMP_FANTASIA=@FANTASIA, ";
            sql += " EMP_ENDERECO =@ENDERECO WHERE CLI_ID = @ID AND EMP_ID = @EMP";
            SqlCommand cmd = new SqlCommand(sql, conexao);
            cmd.Parameters.AddWithValue("@ID", emp.CliId);
            cmd.Parameters.AddWithValue("@EMP", emp.EmpId);
            cmd.Parameters.AddWithValue("@RSOCIAL", emp.RazaoSocial);
            cmd.Parameters.AddWithValue("@RAZAO",emp.RazaoSocial);
            cmd.Parameters.AddWithValue("@BAIRRO", emp.Bairro);
            cmd.Parameters.AddWithValue("@CEP",emp.CEP);
            cmd.Parameters.AddWithValue("@CIDADE", emp.Cidade);
            cmd.Parameters.AddWithValue("@CONT1",emp.Cont1);
            cmd.Parameters.AddWithValue("@CONT2",emp.Cont2);
            cmd.Parameters.AddWithValue("@FONE1",emp.Fone1);
            cmd.Parameters.AddWithValue("@FONE2",emp.Fone2);
            cmd.Parameters.AddWithValue("@HOMEPAGE",emp.HomePage);
            cmd.Parameters.AddWithValue("@UF",emp.UF);
            cmd.Parameters.AddWithValue("@EMAIL",emp.Email);
            cmd.Parameters.AddWithValue("@FANTASIA",emp.NomeFantasia);
            cmd.Parameters.AddWithValue("@ENDERECO", emp.Endereco);
            conexao.Open();
            try
            {
                cmd.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
            finally
            {
                conexao.Close();
            }
        }

        public Boolean delete(Empresa emp)
        {
            string StrCon2 = ""; 
            conexao = new SqlConnection(StrCon2);
            String sql = "";
            SqlCommand cmd = new SqlCommand(sql, conexao);
            cmd.Parameters.AddWithValue("@CLIID", emp.CliId);
            cmd.Parameters.AddWithValue("@EMPID", emp.EmpId);
            conexao.Open();
            try
            {
                cmd.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
            finally
            {
                conexao.Close();
            }
        }

        public Empresa getEmp(int cliId)
        {
            
            Empresa emp = new Empresa();
            Data dt = new Data();
            DataTable dt_usr = dt.GetDt(StrCon,"WTR_EMPRESA", "SELECT * FROM WTR_EMPRESAS WHERE CLI_ID='" + cliId + "' ");

            try
            {
                if (dt_usr.Rows.Count > 0)
                {
                    emp.CliId = Convert.ToInt32(dt_usr.DefaultView[0].Row["CLI_ID"]);
                    emp.EmpId = Convert.ToInt32(dt_usr.DefaultView[0].Row["EMP_ID"]);
                    emp.RazaoSocial = dt_usr.DefaultView[0].Row["EMP_RAZAO"].ToString();
                    emp.Bairro = dt_usr.DefaultView[0].Row["EMP_BAIRRO"].ToString();
                    emp.CEP = dt_usr.DefaultView[0].Row["EMP_CEP"].ToString();
                    emp.Cidade= dt_usr.DefaultView[0].Row["EMP_CIDADE"].ToString();
                    emp.Cont1= dt_usr.DefaultView[0].Row["EMP_CONTATO1"].ToString();
                    emp.Cont2= dt_usr.DefaultView[0].Row["EMP_CONTATO2"].ToString();
                    emp.Fone1= dt_usr.DefaultView[0].Row["EMP_FONE1"].ToString();
                    emp.Fone2= dt_usr.DefaultView[0].Row["EMP_FONE2"].ToString();
                    emp.HomePage= dt_usr.DefaultView[0].Row["EMP_HOMEPAGE"].ToString();
                    emp.UF = dt_usr.DefaultView[0].Row["EMP_UF"].ToString();
                    emp.Email = dt_usr.DefaultView[0].Row["EMP_EMAIL"].ToString();
                    emp.NomeFantasia = dt_usr.DefaultView[0].Row["EMP_FANTASIA"].ToString();
                    emp.Endereco = dt_usr.DefaultView[0].Row["EMP_ENDERECO"].ToString();
                }
                return emp;
            }
            catch (Exception ex)
            {
                return emp;
            }
        }

        public DataTable TodasEmpresas()
        {
            string StrCon2 = ""; 
            Empresa emp = new Empresa();
            Data dt = new Data();
            DataTable dt_usr = dt.GetDt(StrCon2, "WTR_EMPRESA", "SELECT  CLI_ID, EMP_ID,EMP_RAZAO FROM WTR_EMPRESAS WHERE CLI_ID='" + "" + "' ");

            try
            {
                
                return dt_usr;
            }
            catch (Exception ex)
            {
                return dt_usr;
            }
        }

        public int getEmp(int usrId, DropDownList cbx)
        {
            conexao = new SqlConnection(StrCon);
            SqlDataReader dr;
            String sql = "SELECT E.CLI_ID, E.EMP_ID, E.EMP_RAZAO FROM WTR_EMPRESAS E INNER JOIN WTR_PERMISSAOACESSO P ON  E.EMP_ID=P.EMP_ID AND E.CLI_ID=P.CLI_ID WHERE USR_ID=@USR";
            SqlCommand cmd = new SqlCommand(sql, conexao);
            cmd.Parameters.AddWithValue("@USR", usrId);
            conexao.Open();
            int x = 0;
            try
            {
                cbx.Items.Clear();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    cbx.Items.Add(dr["EMP_RAZAO"].ToString());
                    x++;
                }
                return x;
            }
            catch (Exception ex)
            {
                return 0;
            }
            finally
            {
                conexao.Close();
            }
        }

        public Boolean getEmp(Empresa empresa, int usrId)
        {
            conexao = new SqlConnection(StrCon);
            SqlDataReader dr;
            String sql = "SELECT E.CLI_ID, E.EMP_ID, E.EMP_RAZAO FROM WTR_EMPRESAS E INNER JOIN WTR_PERMISSAOACESSO P ON  E.EMP_ID=P.EMP_ID AND E.CLI_ID=P.CLI_ID WHERE USR_ID=@USR";
            SqlCommand cmd = new SqlCommand(sql, conexao);
            cmd.Parameters.AddWithValue("@USR", usrId);
            conexao.Open();

            try
            {
                dr = cmd.ExecuteReader();
                if (dr.Read())
                {
                    empresa.CliId = Convert.ToInt32(dr["CLI_ID"].ToString());
                    empresa.EmpId = Convert.ToInt32(dr["EMP_ID"].ToString());
                    empresa.RazaoSocial = dr["EMP_RAZAO"].ToString();
                }
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
            finally
            {
                conexao.Close();
            }
        }

        public Empresa getEmp(Empresa empresa)
        {
            conexao = new SqlConnection(StrCon);
            SqlDataReader dr;
            String sql = "SELECT CLI_ID, EMP_ID, EMP_RAZAO FROM WTR_EMPRESAS WHERE EMP_RAZAO=@RAZAO";
            SqlCommand cmd = new SqlCommand(sql, conexao);
            cmd.Parameters.AddWithValue("@RAZAO", empresa.RazaoSocial);
            conexao.Open();

            try
            {
                dr = cmd.ExecuteReader();
                if (dr.Read())
                {
                    empresa.CliId = Convert.ToInt32(dr["CLI_ID"].ToString());
                    empresa.EmpId = Convert.ToInt32(dr["EMP_ID"].ToString());
                    empresa.RazaoSocial = dr["EMP_RAZAO"].ToString();
                }
                return empresa;
            }
            catch (Exception ex)
            {
                return empresa;
            }
            finally
            {
                conexao.Close();
            }
        }
    }
}
