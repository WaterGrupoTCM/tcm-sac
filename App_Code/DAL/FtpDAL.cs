﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WaterSyncLite.Class;
using WaterSyncLite.MDL;
using System.Data.SqlClient;
using System.Net.Sockets;
using System.Net;
using System.Diagnostics;
using System.Runtime.Remoting;
using System.Runtime.Remoting.Messaging;
using System.Security.Permissions;
using System.IO;

namespace WaterSyncLite.DAL
{
    public class FtpDAL
    {
        public Ftp getFtp()
        {
            SqlConnection conexao = new SqlConnection(Conexao.StrCon);
            try
            {
                String sql = "SELECT TOP 1 [FTP_SERVER],[FTP_USER],[FTP_PASSWORD],[FTP_PORT],[FTP_UPDATEPATH],[FTP_MANUALPATH],[FTP_DOWNLOADPATH] FROM [WTR_FTP]";
                SqlCommand cmd = new SqlCommand(sql, conexao);
                conexao.Open();
                SqlDataReader dr = cmd.ExecuteReader();
                Ftp ftp = new Ftp();
                if (dr.Read())
                {
                    ftp.Server = dr["FTP_SERVER"].ToString();
                    ftp.User = dr["FTP_USER"].ToString();
                    ftp.Password = dr["FTP_PASSWORD"].ToString();
                    ftp.Port = dr["FTP_PORT"].ToString();
                    ftp.UpdatePath = dr["FTP_UPDATEPATH"].ToString();
                    ftp.ManualPath = dr["FTP_MANUALPATH"].ToString();
                    ftp.DownloadPath = dr["FTP_DOWNLOADPATH"].ToString();
                }
                dr.Close();
                return ftp;
            }
            catch (Exception ex)
            {
                //Files.WriteLog(xml.GetConfigValue("LOCAL", "LOG"), "LOG.txt", DateTime.Now.ToString() + " - " + ex.Message + "\r\n");
                return null;
            }
            finally
            {
                conexao.Close();
            }
        }

        public Boolean checkUpdate(Ftp ftp, String OldVersion, String Diretorio)
        {
            try
            {
                cFtp c = new cFtp(ftp.Server, ftp.User, ftp.Password);
                String[] lista = c.directoryListSimple(ftp.UpdatePath);
                String newVersion = "";

                for (int x = 0; x < lista.Length; x++)
                {
                    if (lista[x].Contains("WaterSyncLite"))
                    {
                        newVersion = lista[x];
                        x = lista.Length;
                    }
                }

                if (Convert.ToInt32("0" + OldVersion.Replace(".", "")) < Convert.ToInt32("0" + newVersion.Replace("WaterSyncLite", "").Replace(".", "").Replace("_", "").Replace("exe", "")))
                {
                    Download(ftp, ftp.UpdatePath ,newVersion, Diretorio);
                }
                return true;
            }
            catch (Exception ex)
            {
                //Files.WriteLog(xml.GetConfigValue("LOCAL", "LOG"), "LOG.txt", DateTime.Now.ToString() + " - " + ex.Message + "\r\n");
                return false;
            }
        }

        public List<String> getList(Ftp ftp, String path, String ext)
        {
            cFtp c = new cFtp(ftp.Server, ftp.User, ftp.Password);
            String[] lista = c.directoryListSimple(path);
            List<String> newLista = new List<String>();

            for (int x = 0; x < lista.Length; x++)
            {
                if (lista[x].Contains(ext))
                {
                    newLista.Add(lista[x]);
                }
            }
            return newLista;
        }

        public void Download(Ftp ftp,String DirFtp ,String arquivo, String Diretorio)
        {
            // O ftpPath deve ser montado da seguinte maneira:
            // ftp://username:password@hostFtp:port

            Uri uri = new Uri(ftp.Server+"/"+DirFtp+"/"+arquivo);

            // Cria o FtpWebRequest com o Uri que montamos
            FtpWebRequest request = (FtpWebRequest)FtpWebRequest.Create(uri);

            request.Credentials = new NetworkCredential(ftp.User, ftp.Password);

            // Por padrão a conexão é mantida depois do comando ser executado, aqui alteramos para a conexão ser fechada.
            request.KeepAlive = false;

            // Especifica o comando para ser executado.
            request.Method = WebRequestMethods.Ftp.DownloadFile;

            // Utiliza conexão no modo passivo
            request.UsePassive = true;

            // Especifica o tipo de transferência de dados.
            request.UseBinary = true;

            try
            {
                FtpWebResponse response = (FtpWebResponse)request.GetResponse();
                Stream responseStream = response.GetResponseStream();
                FileStream fileStream = new FileStream(Diretorio + @"/" + arquivo, FileMode.Create);

                // Divide e baixa o arquivo por partes.
                int Length = 2048;
                Byte[] buffer = new Byte[Length];
                int bytesToSave = responseStream.Read(buffer, 0, Length);
                while (bytesToSave > 0)
                {
                    // Transfere o conteúdo do buffer para o arquivo
                    fileStream.Write(buffer, 0, bytesToSave);
                    // Baixa uma parte do arquivo e carrega o buffer
                    bytesToSave = responseStream.Read(buffer, 0, Length);
                }
                fileStream.Close();
                response.Close();
            }
            catch (Exception ex)
            {
                //Files.WriteLog(xml.GetConfigValue("LOCAL", "LOG"), "LOG.txt", DateTime.Now.ToString() + " - " + ex.Message + "\r\n");
                //MessageBox.Show(ex.Message, "Erro no Download");
            }
        }
    }
}
