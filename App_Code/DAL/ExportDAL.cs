﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WaterSyncLite.MDL;
using System.Data;
using WaterSyncLite.Class;


namespace WaterSyncLite.DAL
{
    public class ExportDAL
    {

        public Export getExp(int cliId, string tab)
        {
            Export exp = new Export();
            Data dt = new Data();
            DataTable dt_usr = dt.GetDt(Conexao.StrCon, "WTR_LAYOUT", "SELECT CLI_ID, EMP_ID,LAY_TABELA,LAY_CAMPOS FROM WTR_LAYOUT WHERE CLI_ID='" + cliId + "' AND LTP_ID=2 AND LAY_TABELA = 'WTR_" + tab + "'");

            try
            {
                if (dt_usr.Rows.Count > 0)
                {
                    exp.Cli_id = Convert.ToInt32(dt_usr.DefaultView[0].Row["CLI_ID"]);
                    exp.Emp_id = Convert.ToInt32(dt_usr.DefaultView[0].Row["EMP_ID"]);
                    exp.Tabela = dt_usr.DefaultView[0].Row["LAY_TABELA"].ToString();
                    exp.Campos = dt_usr.DefaultView[0].Row["LAY_CAMPOS"].ToString();
                }
                return exp;
            }
            catch (Exception ex)
            {
                return exp;
            }
        }
    }
}
