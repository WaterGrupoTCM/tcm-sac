﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WaterSyncLite.MDL;
using System.Data.SqlClient;
using WaterSyncLite.Class;

namespace WaterSyncLite.DAL
{
    public class MailDAL
    {
        public Mail getDados()
        {
            SqlConnection conexao = new SqlConnection(Conexao.StrCon);
            try
            {
                String sql = "SELECT TOP 1 [SUP_MAILTO],[SUP_MAILFROM],[SUP_PWD],[SUP_SMTP],[SUP_SMTPPORT] FROM [WTR_SUPORTE]";
                SqlCommand cmd = new SqlCommand(sql, conexao);
                conexao.Open();
                SqlDataReader dr = cmd.ExecuteReader();
                Mail mail = new Mail();
                if (dr.Read())
                {
                    mail.MailTo = dr["SUP_MAILTO"].ToString();
                    mail.MailFrom = dr["SUP_MAILFROM"].ToString();
                    mail.Pwd = dr["SUP_PWD"].ToString();
                    mail.Smtp = dr["SUP_SMTP"].ToString();
                    mail.Port = Convert.ToInt32(dr["SUP_SMTPPORT"].ToString());

                }
                dr.Close();
                return mail;
            }
            catch (Exception ex)
            {
                //Files.WriteLog(xml.GetConfigValue("LOCAL", "LOG"), "LOG.txt", DateTime.Now.ToString() + " - " + ex.Message + "\r\n");
                return null;
            }
            finally
            {
                conexao.Close();
            }
        }
    }
}
