﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using WaterSyncLite.Class;
using WaterSyncLite.MDL;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WaterSyncLite.DAL
{
    public class UserDAL
    {
        private string StrCon = Conexao.StrCon;
        SqlConnection conexao = null;

        public UserDAL()
        {
        }

        public Boolean insert(User user)
        {
            conexao = new SqlConnection(StrCon);
            String sql = "INSERT INTO WTR_USER(USR_ID,USR_NAME,USR_USERNAME,USR_PWD,USR_EMAIL)VALUES(@ID,@NAME,@USERNAME,@PWD,@EMAIL)";
            SqlCommand cmd = new SqlCommand(sql, conexao);
            cmd.Parameters.AddWithValue("@ID", user.Id);
            cmd.Parameters.AddWithValue("@NAME", user.Name);
            cmd.Parameters.AddWithValue("@USERNAME", user.UserName);
            cmd.Parameters.AddWithValue("@PWD", user.Pwd);
            cmd.Parameters.AddWithValue("@EMAIL", user.Email);
            conexao.Open();
            try
            {
                cmd.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
            finally
            {
                conexao.Close();
            }
        }

        public Boolean update(User user)
        {
            conexao = new SqlConnection(StrCon);
            String sql = "UPDATE WTR_USER SET USR_NAME = @NAME, USR_USERNAME = @USERNAME, USR_PWD = @PWD, USR_EMAIL = @EMAIL WHERE USR_ID = @ID";
            SqlCommand cmd = new SqlCommand(sql, conexao);
            cmd.Parameters.AddWithValue("@ID", user.Id);
            cmd.Parameters.AddWithValue("@NAME", user.Name);
            cmd.Parameters.AddWithValue("@USERNAME", user.UserName);
            cmd.Parameters.AddWithValue("@PWD", user.Pwd);
            cmd.Parameters.AddWithValue("@EMAIL", user.Email);
            conexao.Open();
            try
            {
                cmd.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
            finally
            {
                conexao.Close();
            }
        }

        public Boolean delete(User user)
        {
            conexao = new SqlConnection(StrCon);
            String sql = "DELETE WTR_USER WHERE USR_ID = @ID";
            SqlCommand cmd = new SqlCommand(sql, conexao);
            cmd.Parameters.AddWithValue("@ID", user.Id);
            conexao.Open();
            try
            {
                cmd.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
            finally
            {
                conexao.Close();
            }
        }

        public Boolean login(User user)
        {
            try
            {
                Data dt = new Data();
                DataTable dt_usr = dt.GetDt(Conexao.StrCon,"WTR_USER", "SELECT * FROM WTR_USER WHERE USR_USERNAME='" + user.UserName + "' ");

                if (dt_usr.Rows.Count == 0)
                {
                    //MessageBox.Show("Login Inválido!", "Atenção!!!");
                    return false;
                }
                else if (dt_usr.DefaultView[0].Row["USR_PWD"].ToString() != user.Pwd)
                {
                    //MessageBox.Show("Senha Inválida!", "Atenção!!!");
                    return false;
                }
                else
                {
                    user.Name = dt_usr.DefaultView[0].Row["USR_NAME"].ToString();
                    user.Id = Convert.ToInt32(dt_usr.DefaultView[0].Row["USR_ID"]);
                    user.Email = dt_usr.DefaultView[0].Row["USR_EMAIL"].ToString();
                    return true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public User login(User user, TextBox userName, string pwd)
        {
            try
            {
                Data dt = new Data();
                DataTable dt_usr = dt.GetDt(Conexao.StrCon,"WTR_USER", "SELECT * FROM WTR_USER WHERE USR_USERNAME='" + user.UserName + "' ");

                if (dt_usr.Rows.Count == 0)
                {
                    //MessageBox.Show("Login Inválido!", "Atenção!!!");
                    pwd = "";
                    userName.Text = "";
                    userName.Focus();
                    return user;
                }
                else if (dt_usr.DefaultView[0].Row["USR_PWD"].ToString() != pwd)
                {
                    //MessageBox.Show("Senha Inválida!", "Atenção!!!");
                    pwd = "";
                    
                    return user;
                }
                else
                {

                    user.Name = dt_usr.DefaultView[0].Row["USR_NAME"].ToString();
                    user.Id = Convert.ToInt32(dt_usr.DefaultView[0].Row["USR_ID"]);
                    user.Email = dt_usr.DefaultView[0].Row["USR_EMAIL"].ToString();
                    return user;
                }

            }
            catch (Exception ex)
            {
                return user;
            }
        }

        public Boolean getUser(User user)
        {
            conexao = new SqlConnection(StrCon);
            SqlDataReader dr;
            String sql = "SELECT * FROM WTR_USER WHERE USR_EMAIL = @EMAIL";
            SqlCommand cmd = new SqlCommand(sql, conexao);
            cmd.Parameters.AddWithValue("@EMAIL", user.Email);
            conexao.Open();

            try
            {
                dr = cmd.ExecuteReader();
                if (dr.Read())
                {
                    user.Id = Convert.ToInt32(dr["USR_ID"].ToString());
                    user.Name = dr["USR_NAME"].ToString();
                    user.Pwd = dr["USR_PWD"].ToString();
                    user.UserName = dr["USR_USERNAME"].ToString();
                    return true;
                }
                else
                {
                    //MessageBox.Show("O email informado não está cadastrado em nosso sistema!");
                    return false;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
            finally
            {
                conexao.Close();
            }
        }

        public User getUser(int id)
        {
            User user = new User();
            conexao = new SqlConnection(StrCon);
            
            String sql = "SELECT * FROM WTR_USER WHERE USR_ID = @ID";
            SqlCommand cmd = new SqlCommand(sql, conexao);
            cmd.Parameters.AddWithValue("@ID", id);

            try
            {
                conexao.Open();
                SqlDataReader dr = cmd.ExecuteReader();
                if (dr.Read())
                {
                    user.Id = Convert.ToInt32(dr["USR_ID"].ToString());
                    user.Name = dr["USR_NAME"].ToString();
                    user.Pwd = dr["USR_PWD"].ToString();
                    user.UserName = dr["USR_USERNAME"].ToString();
                }
                return user;
            }
            catch (Exception ex)
            {
                return user;
            }
        }

        public string GetField(String StrCon,string sql, int campo, string local)
        {
            try
            {
                if (conexao == null)
                    conexao = new SqlConnection(StrCon);

                if (conexao.State == ConnectionState.Closed)
                    conexao.Open();

                SqlCommand objcom = new SqlCommand(sql, conexao);
                SqlDataReader dr = objcom.ExecuteReader();
                if (dr.Read())
                    return dr[campo].ToString();
                else
                    return "";
            }
            catch (Exception ex)
            {
                string erro = ex.Message;
                return "";
            }
            finally
            {
                conexao.Close();
            }
        }
    }
}
