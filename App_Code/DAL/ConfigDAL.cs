﻿using System;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections.Generic;
using WaterSyncLite.MDL;
using WaterSyncLite.Class;

namespace WaterSyncLite.DAL
{
    public class ConfigDAL
    {
        private string StrCon = Conexao.StrCon;
        SqlConnection conexao = null;

        public Boolean getStrCon(Config cfg)
        {
            conexao = new SqlConnection(StrCon);
            SqlDataReader dr;
            String sql = "SELECT CFG_BANCOSTR, CFG_LINHASREL FROM WTR_CONFIG WHERE CLI_ID=@CLI AND EMP_ID=@EMP";
            SqlCommand cmd = new SqlCommand(sql, conexao);
            cmd.Parameters.AddWithValue("@CLI", cfg.Empresa.CliId);
            cmd.Parameters.AddWithValue("@EMP", cfg.Empresa.EmpId);
            conexao.Open();

            try
            {
                dr = cmd.ExecuteReader();
                if (dr.Read())
                {
                    cfg.StrCon = dr["CFG_BANCOSTR"].ToString();
                    

                    
                    cfg.LinhasRel = dr["CFG_LINHASREL"].ToString();
                    return true;
                }
                else
                    return false;
            }
            catch (Exception ex)
            {
                return false;
            }
            finally
            {
                conexao.Close();
            }

        }
    }
}
