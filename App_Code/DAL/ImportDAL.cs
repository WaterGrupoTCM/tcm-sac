﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WaterSyncLite.MDL;
using WaterSyncLite.Class;
using System.Data;

namespace WaterSyncLite.DAL
{
   public class ImportDAL
    {

        public Import getImp(int cliId,string tab)
        {
            Import imp = new Import();
            Data dt = new Data();
            DataTable dt_usr = dt.GetDt(Conexao.StrCon, "WTR_LAYOUT", "SELECT CLI_ID, EMP_ID,LAY_TABELA,LAY_CAMPOS FROM WTR_LAYOUT WHERE CLI_ID='" + cliId + "' AND LTP_ID=1 AND LAY_TABELA = 'WTR_" + tab+"'");

            try
            {
                if (dt_usr.Rows.Count > 0)
                {
                    imp.Cli_id = Convert.ToInt32(dt_usr.DefaultView[0].Row["CLI_ID"]);
                    imp.Emp_id = Convert.ToInt32(dt_usr.DefaultView[0].Row["EMP_ID"]);
                    imp.Tabela = dt_usr.DefaultView[0].Row["LAY_TABELA"].ToString();
                    imp.Campos = dt_usr.DefaultView[0].Row["LAY_CAMPOS"].ToString();
                }
                return imp;
            }
            catch (Exception ex)
            {
                return imp;
            }
        }
    }
}
