﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using WaterSyncLite.Class;
using WaterSyncLite.DAL;
using WaterSyncLite.MDL;

/// <summary>
/// Summary description for Promissao
/// </summary>
public class Promissao
{

    private int idFaixa;
    private int cat;


	public Promissao()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    String msg = "";
    string cliente = "12";
    string empresa = "01";
    string cid = "1";
    public int Import(string cam, string file, string rota, string mes, string ano, string caminhoProjeto, string leiturista, String strCon, TextBox txtStatus)      //NovoSis (Layout Reader 3.95) - SAAE Promissão
    {
        #region Variaveis
        ImportDAL impDAL = new ImportDAL();
        string command = "insert";      //
        string xvHist = "";             //Usado para verificar se mudou a chave (fat_numuc)
        int idxHist = 0;                //Variável de index de campo para WTR_FATURA (Histórico)
        int idxField = 1;               //Usado para inserir serviços e histórico nas faturas
        int countLeit = 0;              //Controle de leituras para geração do Livro
        int qTab = 5;                   //Nº Total de tabelas de importação
        Data db = new Data();
        

        Import importIndice = impDAL.getImp(Convert.ToInt32(cliente), "INDICE");           //Indice de potabilidade da água
       // Import importFat = impDAL.getImp(Convert.ToInt32(cliente), "FATURA");              //Faturas
        Import importLeit = impDAL.getImp(Convert.ToInt32(cliente), "LEITURAS");            //Leituras
        Import importServ = impDAL.getImp(Convert.ToInt32(cliente), "SERVICOS");           //Serviços
        //Import importCategoria = impDAL.getImp(Dados.Company.CliId, "CATEGORIA");           //CAtegorias
        Import importFaixa = impDAL.getImp(Convert.ToInt32(cliente), "FAIXA");      //Faixas de valores
        Import importHistorico = impDAL.getImp(Convert.ToInt32(cliente), "HISTORICO");            //Historico de consumo
        //Import importMensagem = impDAL.getImp(Convert.ToInt32(cliente), "MENSAGENS");           //Ocorrências de leitura



        FileStream ofs = File.Open(cam + file, FileMode.Open, FileAccess.Read, FileShare.None);
        StreamReader sr = new StreamReader(ofs, System.Text.Encoding.GetEncoding("ISO-8859-1"));	//Aceita caracteres especiais.
        string linha = string.Empty;
        int i = 0;
        string sql = "";
        string sql1 = "";
        string sqlMsg = "";
        #endregion

        #region Mensagens - OBS
        string obsMsg = string.Empty;

        if (File.Exists(cam + "Mensagens.txt"))
        {
            FileStream ofObs = File.Open(cam + "Mensagens.txt", FileMode.Open, FileAccess.Read, FileShare.None);
            StreamReader srObs = new StreamReader(ofObs, System.Text.Encoding.GetEncoding("ISO-8859-1"));	//Aceita caracteres especiais.
            obsMsg = srObs.ReadLine();
            obsMsg = obsMsg.Substring(3);
        }
        #endregion

        #region CAMPOS
        string[][] campos = new string[qTab][];
        campos[0] = importIndice.Campos.Split('|');
      //  campos[1] = importFat.Campos.Split('|');
        campos[1] = importLeit.Campos.Split('|');
        campos[2] = importServ.Campos.Split('|');
        //campos[4] = importCategoria.Campos.Split('|');
        campos[3] = importFaixa.Campos.Split('|');
        campos[4] = importHistorico.Campos.Split('|');
        //campos[5] = importMensagem.Campos.Split('|');


        deletarDadosAntigosPromissao(rota,strCon,mes, ano);

        #endregion
        DataTable dtFaturas = new DataTable();
        dtFaturas.Columns.Add(new DataColumn("ligacao", typeof(string)));
        dtFaturas.Columns.Add(new DataColumn("insert", typeof(string)));
        dtFaturas.Columns.Add(new DataColumn("values", typeof(string)));

        DataTable dtHist = new DataTable();
        dtHist.Columns.Add(new DataColumn("ligacao", typeof(string)));
        dtHist.Columns.Add(new DataColumn("insert", typeof(string)));
        dtHist.Columns.Add(new DataColumn("values", typeof(string)));

        idFaixa = 0;
        cat = 0;
        try
        {
            while (sr.Peek() >= 0)
            {
                #region Variáveis de arquivo

                i++;                
                linha = string.Empty;
                linha = sr.ReadLine();

                #region Tratamento da linha
                if (linha.Length < 141)
                    linha = linha.PadRight(141, ' ');

                linha = linha.Replace("'", " ");		//Evita erro de SQL com apóstofro - Padovani.
                linha = linha.Replace("\r\n", " ");	    //Evita erro com <Enter> - Padovani.

                linha = linha.Replace("S/N", "000");    //Evita inserir letras em campo numérico - Padovani.
                #endregion

                string[] field = null;
                string[] valor = null;
                string tabela = "";
                int p = 0;
                bool exec = false;

                #endregion

                for (int t = 0; t < qTab; t++)
                {
                    tabela = campos[t][0];

                    #region Identificador da Linha

                    if ((linha.Substring(0, 3) == "A07") && (tabela == "WTR_INDICE"))
                    {
                        exec = true;
                        command = "insert";
                    }
                  //  else if ((linha.Substring(0, 3) == "A11") && (tabela == "WTR_FATURA"))
                    //{
                      //  exec = true;
                        //command = "insert";
                    //}
                    //else if ((linha.Substring(0, 3) == "A03") && (tabela == "WTR_CATEGORIA"))
                    //{
                    //    exec = true;
                    //    command = "insert";
                    //}
                    else if ((linha.Substring(0, 3) == "A11") && (tabela == "WTR_LEITURAS"))
                    {
                        countLeit++;
                        exec = true;
                        command = "insert";
                    }
                    else if ((linha.Substring(0, 3) == "A04") && (tabela == "WTR_FAIXA"))
                    {
                        exec = true;
                        command = "insert";
                    }
                    else if ((linha.Substring(0, 3) == "A13") && (tabela == "WTR_HISTORICO"))
                    {
                        exec = true;
                        command = "insert";
                    }
                    else if ((linha.Substring(0, 3) == "A12") && (tabela == "WTR_SERVICOS"))
                    {
                        exec = true;
                        command = "insert";
                    }
                    else if ((linha.Substring(0, 3) == "A05") && (tabela == "WTR_MENSAGENS"))
                    {
                        exec = true;
                        command = "insert";
                    }
                    else
                    {
                        exec = false;
                        continue;
                    }
                    #endregion

                    if (exec)
                    {

                        field = new string[campos[t].Length - 1];
                        valor = new string[campos[t].Length - 1];

                        if ((tabela.Trim().Length > 0) || (field.Length > 0))
                        {
                            #region Leitura de Conteúdo do Arquivo
                            p = 0;
                            while (p < (campos[t].Length - 1))
                            {
                                field[p] = campos[t][p + 1].Split(';')[0].Trim();                               
                                
                                    if (campos[t][p + 1].Split(';').Length > 3)	//Valor fixo ou Campo Formatado
                                    {

                                        valor[p] = linha.Substring(Convert.ToInt32(campos[t][p + 1].Split(';')[1].Trim()), Convert.ToInt32(campos[t][p + 1].Split(';')[2].Trim()));
                                        valor[p] = TrataCampo(campos[t][p + 1].Split(';')[3].Trim(), valor[p], tabela, countLeit, i, rota, txtStatus, leiturista, mes, ano);
                                    }
                                    else
                                        valor[p ] = linha.Substring(Convert.ToInt32(campos[t][p + 1].Split(';')[1].Trim()), Convert.ToInt32(campos[t][p + 1].Split(';')[2].Trim()));
                                                             

                                p++;
                            }
                            #endregion

                            #region Comando SQL para o BANCO DE DADOS
                            string erro = "";
                            #region Command INSERT
                            if (command == "insert")
                            {
                                sql = "INSERT INTO " + tabela + " (";

                                p = 0;
                                while (p < field.Length)
                                {
                                    if (p > 0)
                                    {
                                        if (field[p - 1].Trim() != field[p].Trim())
                                        {
                                            if (field[p].IndexOf("#") > 0)
                                                field[p] = field[p].Substring(field[p].IndexOf("#") + 1);

                                            sql += field[p].Trim() + ",";
                                        }
                                    }
                                    else
                                    {
                                        if (field[p].IndexOf("#") > 0)
                                            field[p] = field[p].Substring(field[p].IndexOf("#") + 1);

                                        sql += field[p].Trim() + ",";
                                    }
                                    p++;
                                }

                                sql = sql.Remove(sql.Length - 1, 1);
                                sql += ") VALUES ('";

                                p = 0;
                                while (p < valor.Length)
                                {
                                    if (p > 0)
                                    {
                                        if (field[p - 1].Trim() == field[p].Trim())
                                            sql = sql.Remove(sql.Length - 3, 3);

                                        sql += valor[p].Trim() + "','";
                                    }
                                    else
                                    {
                                        sql += valor[p].Trim() + "','";
                                    }

                                    p++;
                                }

                                sql = sql.Remove(sql.Length - 2, 2);
                                sql += ")";
                                if (command.Length > 0)
                                    erro = db.Ins(strCon, sql);
                                if (erro.Length > 80)
                                    command = "update";
                            }
                            #endregion

                            if (erro.Trim().Length > 0)
                            {
                                txtStatus.Text += "\r\n" + "Arquivo: " + file.Trim() + " - Registro: " + i + " - Erro: " + erro;
                              
                            }

                            #endregion

                        }
                    }
                }
            }



            #region Mensagem
            if (sqlMsg.Trim().Length > 0)
            {
                sqlMsg = sqlMsg.Replace("@LOCAL", rota.Replace("@M", Dados.Reference.Mes.ToString().Trim()).Replace("@ANO", Dados.Reference.Ano.ToString().Trim()));
                string error = db.Ins(Dados.config.StrCon, sqlMsg);
            }
            #endregion

            #region Geração do LIVRO.
            if (countLeit > 0)
            {
                string sql2 = "INSERT WTR_LIVROS VALUES('"
                    + cliente + "','"
                    + "1" + "','"
                    + mes + "','"
                    + mes + "','"
                    + ano + "','"
                    + "1','"
                    + rota.Trim() + "','"
                    + "','"
                    +DateTime.Now.Date.Day.ToString().PadLeft(2, '0') + "/" + mes + "/" + ano + "','"
                    + countLeit.ToString() + "','"
                    + "0','"
                    + countLeit.ToString() + "','"
                    + "0','"
                    + DateTime.Now.ToShortDateString()+ "','"
                    + leiturista + "','"
                    + "0','"
                    + "0','"
                    + "U','"
                    + "0','"
                    + "N')";
                string erro2 = db.Ins(strCon, sql2);
                //fileFTP += sql2 + "\r\n";
            }
            #endregion

            return 0;
        }
        catch (Exception ex)
        {
            sr.Close();
            ofs.Close();
            string resp = (i + " #" + linha + " - " + ex.Message);
            return -1;
        }
        finally
        {
            txtStatus.Text += "\r\nArquivo: " + file.ToUpper().Trim() + " carregado com " + i.ToString() + " registros.\r\n";
            sr.Close();
            ofs.Close();         

        }
    }


    public void deletarDadosAntigosPromissao(string rota, String strCon, string mes, string ano)
    {
        //DELETAR DADOS DAS TABELAS
        //WTR_HISTORICO
        //WTR_SERVICOS
        //WTR_FAIXA
        //WTR_MENSAGENS

        Data db = new Data();
        db.Exe(strCon, "WTR_HISTORICO", "delete from wtr_historico where loc_id = " + rota);
        db.Exe(strCon, "WTR_TARIFA", "delete from WTR_TARIFA where loc_id = " + rota + " AND TAR_MES = " + mes + " AND TAR_ANO = " + ano);
        db.Exe(strCon, "WTR_FAIXA", "delete from WTR_FAIXA  where loc_id = " + rota);
        db.Exe(strCon, "WTR_INDICE", "delete from WTR_INDICE where loc_id = " + rota);
        //db.Exe(Dados.config.StrCon, "WTR_MENSAGENS", "delete from WTR_MENSAGENS");
    }

    private string TrataCampo(string tipo, string valor, string tabela, int countLeit, int i, string rota, TextBox txtStatus, string leiturista, string mes, string ano)
    {
        try
        {
            if (tipo.Equals("'Data'") && (valor.Length == 8))
            {
                valor = valor.Substring(0, 2) + "/" + valor.Substring(2, 2) + "/" + valor.Substring(4, 4);
            }
            else if (tipo.Equals("'ref'"))
            {
                valor = valor.Replace("/", "");
            }
            else if (tipo.Equals(".NumCasa"))
            {

                if (valor.Trim() != "")
                {
                    valor = String.Join("", System.Text.RegularExpressions.Regex.Split(valor, @"[^\d]"));
                    if (valor.Trim().Length > 0)
                        valor = Convert.ToInt32(valor).ToString().Trim();
                    else valor = "0";
                }
                else
                    valor = "0";
            }
            else if (tipo.Equals(".Num"))
            {
                if (valor.Trim() != "")
                    valor = Convert.ToInt32(valor).ToString().Trim();
                else
                    valor = "0";
            }
            else if (tipo.Equals(".Saae"))
            {
                if (rota.Trim() != "")
                {
                   
                    if (Convert.ToInt32(rota) > 100)
                    {
                        valor = "S";
                    }
                    else
                    {
                        valor = "N";
                    }
                }
            }
            else if (tipo.Equals(".repasse"))
            {
                if (rota.Trim() != "")
                {
                    if (Convert.ToInt32(rota) > 100)
                    {
                        valor = "N";
                    }
                    else
                    {
                        valor = "S";
                    }
                }
            }
            else if (tipo == "'decimal'")
            {
                valor = valor.Replace(",", ".");
            }
            else if (tipo == ".cat")
            {

                if (cat == 0)
                    cat = Convert.ToInt32(valor);
                else if (cat != Convert.ToInt32(valor))
                {
                    cat = Convert.ToInt32(valor);
                    idFaixa = 0;
                }

                return valor;
            }
            else if (tipo == ".idFaixa")
            {

                idFaixa = idFaixa + 1;
                valor = idFaixa.ToString();
            }
            else if (tipo == "'/10'")
            {
                valor = Convert.ToString(Convert.ToDecimal(valor) / 10).Replace(',', '.');
            }
            else if (tipo == "'/100'")
            {
                if (valor.Trim().Length > 0)
                    valor = Convert.ToString(Convert.ToDecimal(valor) / 100).Replace(',', '.');
            }
            else if (tipo == "'/1000'")
            {
                valor = Convert.ToString(Convert.ToDecimal(valor) / 1000).Replace(',', '.');
            }
            else if (tipo == "'/10000'")
            {
                valor = Convert.ToString(Convert.ToDecimal(valor) / 10000).Replace(',', '.');
            }
            else if (tipo == ".trim")
            {
                valor = valor.Trim().Replace(" ", "");
            }
            else if (tipo == "NumSN")
            {
                if (valor.Trim() == "N")
                    valor = "0";
                else if (valor.Trim() == "S")
                    valor = "1";
                else
                    valor = "";
            }
            else if (tipo == ".leiturista")
            {
                valor = leiturista;
            }
            else if (tipo == ".empresa")
            {
                valor = "1";
            }
            else if (tipo == ".cidade")
            {
                valor = cid;
            }
            else if (tipo == ".local")
            {
                if (tabela == "WTR_MSG")
                    valor = "@LOCAL";
                else
                    valor = rota.Trim();
            }
            else if (tipo == ".dia")
                valor = Dados.Reference.Dia.ToString();
            else if (tipo == ".mes")
            {
                if (tabela == "WTR_MSG")
                    valor = "@M";
                else
                    valor = mes.Trim();
            }
            else if (tipo == ".ano")
            {
                if (tabela == "WTR_MSG")
                    valor = "@ANO";
                else
                    valor = ano.Trim();
            }
            else if (tipo == ".Etapa")
            {
                valor = mes;
            }
            else if ((tipo == ".seq") || (tipo == "seq"))
            {
                valor = countLeit.ToString(); //valor = (i).ToString();
            }
            else if (tipo == ".hoje")
            {
                valor = DateTime.Now.Day.ToString() + "/" + mes + "/" + ano;
            }
            else if (tipo == ".amanha")
            {
                valor = DateTime.Now.Day + "/" + mes + "/" + ano;
                valor = Convert.ToDateTime(valor).AddDays(1).ToString("dd/MM/yyyy");
            }
            else
                valor = tipo.Replace('"', ' ').Trim();

            return valor;

        }
        catch (Exception ex)
        {
            txtStatus.Text += "\r\nRegistro " + i + ": Erro no Tratamento de valores - " + ex.Message;
          

            return "";
        }
    }

}