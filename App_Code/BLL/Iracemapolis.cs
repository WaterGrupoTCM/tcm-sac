﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI.WebControls;
using WaterSyncLite.Class;
using WaterSyncLite.DAL;
using WaterSyncLite.MDL;

/// <summary>
/// Summary description for Iracemapolis
/// </summary>
public class Iracemapolis
{
    public Iracemapolis()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    string cliente = "18";
    string empresa = "01";
    private object MessageBox;

    public int Import(string cam, string file, string rota, string mes, string ano, string caminhoProjeto, string leiturista, String strCon, TextBox txtStatus)
    {


        ImportDAL impDAL = new ImportDAL();


        //====================RUBENS=====================
        //Carregando Layout para importação do arquivo
        //==============================================

        String corrida = "";
        String dataGeracao = "";
        String horaGeracao = "";
        String vencimento = "";
        String empresaNome = "";

        Data db = new Data();

       // Import importMsg = impDAL.getImp(Convert.ToInt32(cliente), "MENSAGENS");
        Import importLeit = impDAL.getImp(Convert.ToInt32(cliente), "LEITURA");
        Import importBanco = impDAL.getImp(Convert.ToInt32(cliente), "BANCO");
        //Import importLivro = impDAL.getImp(Convert.ToInt32(cliente), "LIVRO");
        Import importFaixa = impDAL.getImp(Convert.ToInt32(cliente), "FAIXA");
        Import importBloco = impDAL.getImp(Convert.ToInt32(cliente), "BLOCO");
        Import importParam = impDAL.getImp(Convert.ToInt32(cliente), "PARAMETROS");
        Import importRua = impDAL.getImp(Convert.ToInt32(cliente), "RUA");
        Import importVenc = impDAL.getImp(Convert.ToInt32(cliente), "VENCIMENTO");
        Import importHist = impDAL.getImp(Convert.ToInt32(cliente), "HISTORICO");
        Import importParc = impDAL.getImp(Convert.ToInt32(cliente), "PARCELA");
        Import importBairro = impDAL.getImp(Convert.ToInt32(cliente), "BAIRRO");
        //Import importObs = impDAL.getImp(Convert.ToInt32(cliente), "OBSCOMP");
        //Import importIGPM = impDAL.getImp(Convert.ToInt32(cliente), "IGPM");
        Import importTPLANCTO = impDAL.getImp(Convert.ToInt32(cliente), "TPLANCTO");
        Import importTributacao = impDAL.getImp(Convert.ToInt32(cliente), "TRIBUTACAO");
        
       // Import importComplobs = impDAL.getImp(Convert.ToInt32(cliente), "OBSCOMP");
        #region Variaveis
        string command = "insert";
        string tabelaAux = ""; // Usado para import que tem a mesma descrição
        string xvBanco = ""; //Usado para verificar se mudou a chave (fat_numuc)
        string xvMsg = ""; //Usado para verificar se mudou a chave (fat_numuc)
        string xvFai = "";  //Usado para verificar se mudou a chave (fat_numuc)
        int idxBanco = 0;    //Variável de index de campo para WTR_FATURA (Serviços)
        int idxMsg = 0;    //Variável de index de campo para WTR_FATURA (Histórico)
        int idxFai = 0;     //Variável de index de campo para WTR_FATURA (Observação)
        int idxField = 1;   //Usado para inserir serviços e histórico nas faturas
        int countLeit = 0;  //Controle de leituras para geração do Livro


        //string importFatOBS = xml.GetImportValue("IMPORT", "FATURAOBS");

        FileStream ofs = File.Open(cam + file, FileMode.Open, FileAccess.Read, FileShare.None);
        StreamReader sr = new StreamReader(ofs, System.Text.Encoding.GetEncoding("ISO-8859-1"));	//Aceita caracteres especiais.
        string linha = string.Empty;
        int i = 0;
        string sql = "";

        string sqlHistorico = "";

        #endregion

        #region Mensagens - OBS
        string obsMsg = string.Empty;

        if (File.Exists(cam + "Mensagens.txt"))
        {
            FileStream ofObs = File.Open(cam + "Mensagens.txt", FileMode.Open, FileAccess.Read, FileShare.None);
            StreamReader srObs = new StreamReader(ofObs, System.Text.Encoding.GetEncoding("ISO-8859-1"));	//Aceita caracteres especiais.
            obsMsg = srObs.ReadLine();
            obsMsg = obsMsg.Substring(3);
        }
        #endregion

        #region CAMPOS
        int cont = 12;
        string[][] campos = new string[cont][];

        // campos[0] = importIGPM.Campos.Split('|');
        campos[0] = importTPLANCTO.Campos.Split('|');
        campos[1] = importBanco.Campos.Split('|');
        campos[2] = importLeit.Campos.Split('|');
        campos[3] = importTributacao.Campos.Split('|');
        campos[4] = importFaixa.Campos.Split('|');
        campos[5] = importBloco.Campos.Split('|');
        campos[6] = importParam.Campos.Split('|');
        campos[7] = importRua.Campos.Split('|');
        campos[8] = importVenc.Campos.Split('|');
        campos[9] = importHist.Campos.Split('|');
        campos[10] = importParc.Campos.Split('|');
        campos[11] = importBairro.Campos.Split('|');
        
        
        
      //  campos[18] = importComplobs.Campos.Split('|');
        
        bool check = false;

        //DELETAR DADOS DAS TABELAS
        //WTR_OBSCOMP
        //
        deletarDadosAntigos(strCon,rota,mes,ano);
        if (VerificarRota(strCon,file.ToString().Substring(0, 6)))
        {
            txtStatus.Text = ("Atenção! Essa rota já foi enviada.");
            return -1;
        }

        #endregion

        int seq = 0;
        try
        {

            while (sr.Peek() >= 0)
            {
                #region Variáveis de arquivo

                i++;
                //if (i == 6064)
                //  MessageBox.Show("");
                if (i == 3638)
                {
                    
                    
                }


                linha = string.Empty;
                linha = sr.ReadLine();

                #region Tratamento da linha

                if (linha.Length < 141)
                    linha = linha.PadRight(141, ' ');

                linha = linha.Replace("'", " ");		//Evita erro de SQL com apóstofro - Padovani.
                linha = linha.Replace("\r\n", " ");	    //Evita erro com <Enter> - Padovani.

                linha = linha.Replace("S/N", "000");    //Evita inserir letras em campo numérico - Padovani.
                #endregion

                string[] field = null;
                string[] valor = null;
                string tabela = "";
                int p = 0;
                bool exec = false;

                #endregion

                

                if (linha.Substring(0, 4).Trim() == "2RUA")
                    tabelaAux = linha.Substring(0, 4).Trim();
                else if (linha.Substring(0, 4).Trim() == "2CEP")
                    tabelaAux = linha.Substring(0, 4).Trim();
                else if (linha.Substring(0, 11).Trim() == "2OBSLEITURA")
                    tabelaAux = linha.Substring(0, 11).Trim();

                for (int t = 0; t < cont; t++)
                {
                    tabela = campos[t][0];

                    #region Identificador da Linha

                    if ((linha.Substring(0, 1) == "1"))
                    {
                        corrida = linha.Substring(66,2);
                        dataGeracao = linha.Substring(74, 8);
                        horaGeracao = linha.Substring(82, 6);
                        vencimento = linha.Substring(64, 2);
                        empresaNome = linha.Substring(14, 50);

                        command = "insert";
                        check = true;
                    }else
                    if ((linha.Substring(0, 10) == "3Inscricao") && (tabela == "WTR_LEITURAS" ) && linha.Length > 1000)
                    {
                        if (tabela == "WTR_LEITURAS")
                            seq++;
                        countLeit++;
                        exec = true;
                        command = "insert";
                        check = true;
                    }
                   
                    else if ((linha.Substring(0, 13) == "3ObsIrregular") && (tabela == "WTR_OBSCOMP") && linha.Length > 200)
                    {
                        exec = true;
                        command = "insert";
                        check = true;
                    }
                    else if ((linha.Substring(0, 11) == "3Vencimento") && (tabela == "WTR_BLOCO") && linha.Length < 250)
                    {
                        exec = true;
                        command = "insert";
                        check = true;
                    }
                    else if ((linha.Substring(0, 7) == "3CodRua") && (tabela == "WTR_RUA") && tabelaAux== "2RUA")
                    {
                        exec = true;
                        command = "insert";
                        check = true;
                    }

                    else if ((linha.Substring(0, 13) == "3ObsIrregular") && (tabela == "WTR_MENSAGENS") && tabelaAux == "2OBSLEITURA")
                    {
                        exec = true;
                        command = "insert";
                        check = false;
                    }
                    else if ((linha.Substring(0, 10) == "3CodBairro") && (tabela == "WTR_BAIRRO"))
                    {
                        exec = true;
                        command = "insert";
                        check = true;
                    }
                    else if ((linha.Substring(0, 1) == "A") && (tabela == "WTR_HISTORICO"))
                    {
                        exec = true;
                        command = "insert";
                        check = true;
                    }
                    else if ((linha.Substring(0, 1) == "C") && (tabela == "WTR_PARCELA"))
                    {
                        exec = true;
                        command = "insert";
                        check = true;
                    }
                    else if ((linha.Substring(0, 9) == "3AnoRefer") && (tabela == "WTR_PARAMETROS"))
                    {
                        exec = true;
                        command = "insert";
                        check = false;
                    }
                    else if ((linha.Substring(0, 9) == "3CodBanco") && (tabela == "WTR_BANCO"))
                    {
                        exec = true;
                        command = "insert";
                        check = false;
                    }
                    else if ((linha.Substring(0, 1) == "9"))
                    {
                        string sql2 = "UPDATE WTR_LIVROS SET LIV_TOTAL = " + linha.Substring(17, 8) + ",LIV_NAOLIDA = " + linha.Substring(17, 8) + ",LIV_NOMELTR = '" + leiturista + "' WHERE liv_mes = " + Dados.Reference.Mes + " and liv_ano = " + Dados.Reference.Ano
                + " and loc_id = " + file.ToString().Substring(0, 6);
                        string erro2 = db.Ins(strCon, sql2);
                        //fileFTP += sql2 + "\r\n";
                    }

                    else if ((linha.Substring(0, 11) == "3Vencimento") && (linha.Length > 280 ) && (tabela == "WTR_VENCIMENTO"))
                    {
                        exec = true;
                        command = "insert";
                    }
                    else if ((linha.Substring(0, 4) == "3Ano") && (tabela == "WTR_IGPM") )
                    {
                        exec = true;
                        command = "insert";
                    }
                    else if ((linha.Substring(0, 1) == "3") && (tabela == "WTR_FAIXA") && linha.Length > 175 && linha.Length < 179)
                    {
                        exec = true;
                        command = "insert";
                        check = false;
                    }
                    else if ((linha.Substring(0, 10) == "3CodLancto") && (tabela == "WTR_TPLANCTO"))
                    {
                        exec = true;
                        command = "insert";
                        check = false;
                    }

                    else if ((linha.Substring(0, 14) == "3CodTributacao") && (tabela == "WTR_TRIBUTACAO"))
                    {
                        exec = true;
                        command = "insert";
                        check = false;
                    }
                    
                    else
                    {
                        exec = false;
                        continue;
                    }

                    #endregion

                    if (exec)
                    {
                        field = new string[campos[t].Length - 1];
                        valor = new string[campos[t].Length - 1];

                        if ((tabela.Trim().Length > 0) || (field.Length > 0))
                        {   

                            #region Leitura de Conteúdo do Arquivo
                            p = 0;
                            int contador = 0;
                            while (p < (campos[t].Length - 1))
                            {
                                field[p] = campos[t][p + 1].Split(';')[0].Trim();

                                if ((campos[t][0].Trim().Equals("WTR_LEITURAS") || campos[t][0].Trim().Equals("WTR_BLOCO") 
                                    || campos[t][0].Trim().Equals("WTR_IGPM") || campos[t][0].Trim().Equals("WTR_VENCIMENTO") 
                                    || campos[t][0].Trim().Equals("WTR_PARAMETROS")|| campos[t][0].Trim().Equals("WTR_TPLANCTO")
                                    || campos[t][0].Trim().Equals("WTR_TRIBUTACAO") || campos[t][0].Trim().Equals("WTR_MENSAGENS")
                                    || campos[t][0].Trim().Equals("WTR_OBSCOMP") || campos[t][0].Trim().Equals("WTR_RUA")
                                    || campos[t][0].Trim().Equals("WTR_BANCO") || campos[t][0].Trim().Equals("WTR_BAIRRO")) && campos[t][p + 1].Split(';').Length <= 3)
                                {
                                    String separa = "\u001b";

                                    linha = linha.Replace(separa, "|");
                                    string[] dados = linha.Split('|');

                                    if (contador < dados.Length)
                                    {
                                        if (dados[contador].Trim() != "")
                                            valor[p] =
                                                dados[contador].Replace(dados[contador].Split(' ').First(), " ")
                                                    .Trim()
                                                    .Substring(4);
                                        else
                                            valor[p] = " ";

                                        contador++;

                                    }

                                    if (tabela == "WTR_PARAMETROS")
                                    {
                                        String sqlFaixas = "";

                                        for (int x = 0; x < 4; x++)
                                        {

                                            insertFaixas(dados, x, strCon,"Res",1);
                                            insertFaixas(dados, x, strCon, "Com", 2);
                                            insertFaixas(dados, x, strCon, "Ind", 3);
                                            insertFaixas(dados, x, strCon, "Rural", 4);
                                            insertFaixas(dados, x, strCon, "PubEst", 5);
                                            insertFaixas(dados, x, strCon, "Inst", 6);
                                            insertFaixas(dados, x, strCon, "Soc", 7);



                                            

                                        }
                                    }

                                        
                                    
                                    
                                }
                                else
                                {
                                if (campos[t][p + 1].Split(';').Length > 3)	//Valor fixo
                                {
                                    if (campos[t][p + 1].Split(';')[3].Trim() == "'Data'")
                                    {
                                        valor[p] = linha.Substring(Convert.ToInt32(campos[t][p + 1].Split(';')[1].Trim()), Convert.ToInt32(campos[t][p + 1].Split(';')[2].Trim()));
                                        valor[p] = valor[p].Substring(0, 2) + "/" + valor[p].Substring(2, 2) + "/" + valor[p].Substring(4, 4);
                                    }
                                    else if (campos[t][p + 1].Split(';')[3].Trim() == "'ref'")
                                    {
                                        valor[p] = linha.Substring(Convert.ToInt32(campos[t][p + 1].Split(';')[1].Trim()), Convert.ToInt32(campos[t][p + 1].Split(';')[2].Trim()));
                                        valor[p] = valor[p].Replace("/", "");
                                    }
                                    else if (campos[t][p + 1].Split(';')[3].Trim() == "'decimal'")
                                    {
                                        valor[p] = linha.Substring(Convert.ToInt32(campos[t][p + 1].Split(';')[1].Trim()), Convert.ToInt32(campos[t][p + 1].Split(';')[2].Trim()));
                                        valor[p] = valor[p].Replace(",", ".");
                                    }

                                    //else if (campos[t][p + 1].Split(';')[3].Trim() == "'decimal4'")
                                    //{
                                    //    valor[p] = linha.Substring(Convert.ToInt32(campos[t][p + 1].Split(';')[1].Trim()), Convert.ToInt32(campos[t][p + 1].Split(';')[2].Trim()));
                                    //    valor[p] = valor[p].Replace(",", ".");
                                    //}
                                    else if (campos[t][p + 1].Split(';')[3].Trim() == "'/10'")
                                    {
                                        valor[p] = linha.Substring(Convert.ToInt32(campos[t][p + 1].Split(';')[1].Trim()), Convert.ToInt32(campos[t][p + 1].Split(';')[2].Trim()));
                                        valor[p] = Convert.ToString(Convert.ToDecimal(valor[p]) / 10).Replace(',', '.');
                                    }
                                    else if (campos[t][p + 1].Split(';')[3].Trim() == "'/100'")
                                    {
                                        valor[p] = linha.Substring(Convert.ToInt32(campos[t][p + 1].Split(';')[1].Trim()), Convert.ToInt32(campos[t][p + 1].Split(';')[2].Trim()));
                                        if (valor[p].Trim().Length > 0)
                                            valor[p] = Convert.ToString(Convert.ToDecimal(valor[p]) / 100).Replace(',', '.');
                                    }
                                    else if (campos[t][p + 1].Split(';')[3].Trim() == "'/1000'")
                                    {
                                        valor[p] = linha.Substring(Convert.ToInt32(campos[t][p + 1].Split(';')[1].Trim()), Convert.ToInt32(campos[t][p + 1].Split(';')[2].Trim()));
                                        valor[p] = Convert.ToString(Convert.ToDecimal(valor[p]) / 1000).Replace(',', '.');
                                    }
                                    else if (campos[t][p + 1].Split(';')[3].Trim() == "'/10000'")
                                    {
                                        valor[p] = linha.Substring(Convert.ToInt32(campos[t][p + 1].Split(';')[1].Trim()), Convert.ToInt32(campos[t][p + 1].Split(';')[2].Trim()));
                                        valor[p] = Convert.ToString(Convert.ToDecimal(valor[p]) / 10000).Replace(',', '.');
                                    }
                                    else if (campos[t][p + 1].Split(';')[3].Trim() == ".trim")
                                    {
                                        valor[p] = linha.Substring(Convert.ToInt32(campos[t][p + 1].Split(';')[1].Trim()), Convert.ToInt32(campos[t][p + 1].Split(';')[2].Trim()));
                                        valor[p] = valor[p].Trim().Replace(" ", "");
                                    }
                                    else if (campos[t][p + 1].Split(';')[3].Trim() == ".tipo10")
                                    {
                                        valor[p] = linha.Substring(Convert.ToInt32(campos[t][p + 1].Split(';')[1].Trim()), Convert.ToInt32(campos[t][p + 1].Split(';')[2].Trim()));
                                        valor[p] = tipo10(valor[p]);
                                    }
                                    else if (campos[t][p + 1].Split(';')[3].Trim() == ".fase10")
                                    {
                                        valor[p] = linha.Substring(Convert.ToInt32(campos[t][p + 1].Split(';')[1].Trim()), Convert.ToInt32(campos[t][p + 1].Split(';')[2].Trim()));
                                        valor[p] = fase10(valor[p]);
                                    }
                                    else if (campos[t][p + 1].Split(';')[3].Trim() == ".obs10")
                                    {
                                        if (linha.Substring(20, 2) != "01")
                                        {
                                            command = "";
                                            break;
                                        }
                                        else
                                            valor[p] = linha.Substring(Convert.ToInt32(campos[t][p + 1].Split(';')[1].Trim()), Convert.ToInt32(campos[t][p + 1].Split(';')[2].Trim()));
                                    }
                                    else if (campos[t][p + 1].Split(';')[3].Trim() == ".leiturista")
                                        valor[p] = leiturista;
                                    else if (campos[t][p + 1].Split(';')[3].Trim() == ".empresa")
                                        valor[p] = Dados.Company.EmpId.ToString();
                                    //else if (campos[t][p + 1].Split(';')[3].Trim() == ".cidade")
                                    //    valor[p] = txtCidade.Text.Trim();
                                    else if (campos[t][p + 1].Split(';')[3].Trim() == ".local")
                                        valor[p] = rota.PadLeft(2,'0');
                                    else if (campos[t][p + 1].Split(';')[3].Trim() == ".dia")
                                        valor[p] = Dados.Reference.Dia.ToString();
                                    else if (campos[t][p + 1].Split(';')[3].Trim() == ".mes")
                                        valor[p] = mes;
                                    else if (campos[t][p + 1].Split(';')[3].Trim() == ".ano")
                                        valor[p] = ano;
                                    else if ((campos[t][p + 1].Split(';')[3].Trim() == ".seq") || (campos[t][p + 1].Split(';')[3].Trim() == "seq"))
                                        valor[p] = seq.ToString();//countLeit.ToString();  //valor[p] = (i).ToString();
                                    else if (campos[t][p + 1].Split(';')[3].Trim() == ".hoje")
                                        valor[p] = DateTime.Now.Day.ToString().PadLeft(2, '0') + DateTime.Now.Month.ToString().PadLeft(2, '0') + DateTime.Now.Year.ToString();
                                    else if (campos[t][p + 1].Split(';')[3].Trim() == ".decimal2")
                                    {
                                        valor[p] = linha.Substring(Convert.ToInt32(campos[t][p + 1].Split(';')[1].Trim()), Convert.ToInt32(campos[t][p + 1].Split(';')[2].Trim())).Trim();
                                        valor[p] = valor[p].Substring(0, valor[p].Length - 2) + "." + valor[p].Substring(valor[p].Length - 2);
                                    }
                                    else if (campos[t][p + 1].Split(';')[3].Trim() == ".decimal4")
                                    {
                                        valor[p] = linha.Substring(Convert.ToInt32(campos[t][p + 1].Split(';')[1].Trim()), Convert.ToInt32(campos[t][p + 1].Split(';')[2].Trim())).Trim();
                                        valor[p] = valor[p].Substring(0, valor[p].Length - 4) + "." + valor[p].Substring(valor[p].Length - 4);
                                    }
                                    else if (campos[t][p + 1].Split(';')[3].Trim() == ".Num")
                                    {
                                        valor[p] = linha.Substring(Convert.ToInt32(campos[t][p + 1].Split(';')[1].Trim()), Convert.ToInt32(campos[t][p + 1].Split(';')[2].Trim()));
                                        if (valor[p].Trim().Length < 1)
                                            valor[p] = "0";

                                        valor[p] = valor[p].Replace(".", "");   //Retirar ponto de milhar
                                        valor[p] = valor[p].Replace(",", ".");  //Substituir vírgula de centavos

                                        if (valor[p].IndexOf("-") > 0)
                                        {
                                            valor[p] = valor[p].Replace("-", "");
                                            valor[p] = "-" + valor[p];
                                        }
                                    }
                                    else if (campos[t][p + 1].Split(';')[3].Trim() == ".amanha")
                                    {
                                        valor[p] = DateTime.Now.Day + "/" + DateTime.Now.Month.ToString() + "/" + DateTime.Now.Year.ToString();
                                        valor[p] = Convert.ToDateTime(valor[p]).AddDays(1).ToString("dd/MM/yyyy");
                                    }
                                    else if (campos[t][p + 1].Split(';')[3].Trim() == ".categoria")
                                    {

                                        if (linha.Substring(81, 20).Trim() == "Residencial")
                                            valor[p] = "1";
                                        else if (linha.Substring(81, 20).Trim() == "Comercial")
                                            valor[p] = "2";
                                        else if (linha.Substring(81, 20).Trim() == "Industrial")
                                            valor[p] = "3";
                                        else if (linha.Substring(81, 20).Trim() == "Publica")
                                            valor[p] = "4";
                                        else if (linha.Substring(81, 20).Trim() == "Mista")
                                            valor[p] = "5";
                                    }
                                    else
                                        valor[p] = campos[t][p + 1].Split(';')[3].Replace('"', ' ').Trim();
                                }
                                else
                                    valor[p] = linha.Substring(Convert.ToInt32(campos[t][p + 1].Split(';')[1].Trim()), Convert.ToInt32(campos[t][p + 1].Split(';')[2].Trim()));
                                }

                                /*
                                 * #region Colocando valores nos textBox
                                if (field[p].ToString() == "liv_mes")
                                {
                                    txtMes.Text = valor[p].ToString();
                                    txtEtapa.Text = valor[p].ToString();
                                }
                                else if (field[p].ToString() == "liv_ano")
                                    txtAno.Text = valor[p].ToString();
                                else if (field[p].ToString() == "loc_id")
                                    txtLocal.Text = valor[p].ToString();
                                else if (field[p].ToString() == "emp_id")
                                    txtEmpresa.Text = valor[p].ToString();
                                #endregion

                                #region MENSAGEM - OBS
                                if ((field[p].Trim() == "FAT_OBS") && (obsMsg.Length > 0))
                                    valor[p] = obsMsg;
                                #endregion
                                */
                                p++;

                            }

                            #endregion

                            #region Comando SQL para o BANCO DE DADOS
                            string erro = "";
                            #region Command INSERT
                            if (command == "insert")
                            {
                                sql = "INSERT INTO " + tabela + " (";

                                p = 0;
                                while (p < field.Length)
                                {
                                    if (p > 0)
                                    {
                                        if (field[p - 1].Trim() != field[p].Trim())
                                        {
                                            if (field[p].IndexOf("#") > 0)
                                                field[p] = field[p].Substring(field[p].IndexOf("#") + 1);

                                            sql += field[p].Trim() + ",";
                                        }
                                    }
                                    else
                                    {
                                        if (field[p].IndexOf("#") > 0)
                                            field[p] = field[p].Substring(field[p].IndexOf("#") + 1);

                                        sql += field[p].Trim() + ",";
                                    }
                                    p++;
                                }

                                sql = sql.Remove(sql.Length - 1, 1);
                                sql += ") VALUES ('";

                                p = 0;
                                while (p < valor.Length)
                                {
                                    if (p > 0)
                                    {
                                        if (field[p - 1].Trim() == field[p].Trim())
                                            sql = sql.Remove(sql.Length - 3, 3);

                                        sql += valor[p].Trim() + "','";
                                    }
                                    else
                                    {
                                        sql += valor[p].Trim() + "','";
                                    }

                                    p++;
                                }

                                sql = sql.Remove(sql.Length - 2, 2);
                                sql += ")";
                                if (command.Length > 0 && tabela != "WTR_HISTORICO")
                                    erro = db.Ins(strCon, sql);
                                if (erro.Length > 80 && tabela != "WTR_CEP")
                                    command = "update";
                                if (erro.Trim().Contains("Violation of PRIMARY KEY constraint 'PK_WTR_PARCELA_1'"))
                                {
                                    sql = "update wtr_parcela set parc_valor = parc_valor + " +
                                          Convert.ToDecimal(valor[5].Replace('.', ',')).ToString().Replace(',', '.') +
                                          " where parc_mes = " + valor[8] + " and parc_ano = " + valor[7] +
                                          " and parc_idlanc = " + valor[4] + " and parc_ligacao = " + valor[3] +
                                          " and loc_id = " + valor[2];
                                    erro = db.Ins(strCon, sql);
                                }
                                else
                                {
                                    if (erro.Trim().Contains("Violation of PRIMARY KEY constraint 'PK_WTR_CEP'"))
                                    {
                                        erro = "";
                                    }
                                }

                                if (tabela == "WTR_HISTORICO")
                                {
                                    sqlHistorico += sql + "|";
                                }

                                else if (tabela == "WTR_LEITURAS" && sqlHistorico.Trim().Length > 0)
                                {
                                    string con = strCon;

                                    String sqlHistoricoCompleto = TrataHistorico(sqlHistorico, mes, ano);

                                    db.Ins(con, sqlHistoricoCompleto);

                                    sqlHistorico = "";

                                }
                            }
                            #endregion

                            #region Command UPDATE

                            if (command == "update")
                            {
                                #region Chaves dados Update
                                if (field[0].Substring(0, 1) == "@")
                                {
                                    if (tabela.ToUpper() == "WTR_BANCO")
                                    {
                                        if (xvBanco != valor[0])
                                        {
                                            xvBanco = valor[0];
                                            idxBanco = 1;
                                        }
                                        else
                                            idxBanco++;

                                        idxField = idxBanco;
                                    }
                                    else if (tabela.ToUpper() == "WTR_MENSAGENS")
                                    {
                                        if (xvMsg != valor[0])
                                        {
                                            xvMsg = valor[0];
                                            idxMsg = 1;
                                        }
                                        else
                                            idxMsg++;

                                        idxField = idxMsg;
                                    }
                                    else if (tabela.ToUpper() == "WTR_FAIXA")
                                    {
                                        if (xvFai != valor[0])
                                        {
                                            xvFai = valor[0];
                                            idxFai = 1;
                                        }
                                        else
                                            idxFai++;

                                        idxField = idxMsg;
                                    }
                                }
                                #endregion


                                string sqlWhere = "";

                                sql = "UPDATE " + tabela.Replace("SERV", "").Replace("HIST", "").Replace("OBS", "") + " set ";

                                p = 0;
                                while (p < field.Length)
                                {
                                    if (field[p] != null)
                                    {
                                        if (field[p].Substring(0, 1) == "@")
                                        {
                                            field[p] = field[p].Replace("@", "");

                                            if (sqlWhere.Length == 0)
                                                sqlWhere += " where " + field[p] + "='" + valor[p].Trim() + "'";
                                            else
                                                sqlWhere += " and " + field[p] + "='" + valor[p].Trim() + "'";
                                        }
                                        else
                                        {
                                            field[p] = field[p].Replace("!", idxField.ToString());
                                            sql += field[p].Trim() + "='" + valor[p].Trim() + "',";
                                        }
                                    }
                                    p++;
                                }

                                sql = sql.Remove(sql.Length - 1, 1);
                                sql = sql + sqlWhere;
                                if (command.Length > 0)
                                    erro = db.Ins(strCon, sql);
                            }
                            #endregion





                            if (erro.Trim().Length > 0 && check == true)
                            {
                                txtStatus.Text += "\r\n" + "Arquivo: " + file.Trim() + " - Registro: " + i + " - Erro: " + erro;

                            }

                            #endregion

                        }
                    }
                }
            }

            String sqlHist = TrataHistorico(sqlHistorico, mes, ano);
            if (sqlHistorico.Trim().Length > 0)
                db.Ins(strCon, sqlHist);

            #region Geração do LIVRO.
            if (countLeit > 0)
            {
                string sql2 = "INSERT WTR_LIVROS VALUES('"
                    + cliente + "','"
                    + "1" + "','"
                    + mes + "','"
                    + mes + "','"
                    + ano + "','"
                    + "1','"
                    + rota.Trim() + "','"
                    + "','"
                    + DateTime.Now.Date.Day.ToString().PadLeft(2, '0') + "/" + mes + "/" + ano + "','"
                    + countLeit.ToString() + "','"
                    + "0','"
                    + countLeit.ToString() + "','"
                    + "0','"
                    + DateTime.Now.ToShortDateString() + "','"
                    + leiturista + "','"
                    + "0','"
                    + "0','"
                    + "U','"
                    + "0','"
                    + "N','"
                    + dataGeracao+"','"
                    + horaGeracao+"','"
                    + corrida +"','"
                    + vencimento + "','"
                    + empresaNome + "')";
                string erro2 = db.Ins(strCon, sql2);
                //fileFTP += sql2 + "\r\n";
            }
            #endregion
            return 0;
        }
        catch (Exception ex)
        {
           
            //fileFTP += sql2 + "\r\n";
            string resp = (i + " #" + linha + " - " + ex.Message);
            return -1;

        }
        finally
        {
           
            //fileFTP += sql2 + "\r\n";
            txtStatus.Text += "\r\nArquivo: " + file.ToUpper().Trim() + " carregado com " + i.ToString() + " registros.\r\n";
            sr.Close();
            ofs.Close();

        }

    }


    public String TrataHistorico(String sqlHistorico, String mes, String ano)
    {
        try
        {
            String sqlHistoricoCompleto = "INSERT INTO WTR_HISTORICO VALUES ('" +cliente + "','" + "1".PadLeft(3, '0') + "'," + sqlHistorico.Substring(193, 2) + ",'" + sqlHistorico.Substring(198, 8) + "',";



            for (int k = 0; k < 12; k++)
            {
                if (sqlHistorico.Split('|').Length - 1 >= (k + 1))
                    sqlHistoricoCompleto += sqlHistorico.Split('|')[k].Substring(208, sqlHistorico.Split('|')[k].Length - 209) + ",";
                else sqlHistoricoCompleto += "'','','','','','','','','',";

            }
            sqlHistoricoCompleto = sqlHistoricoCompleto + mes +","+ano+")";

            return sqlHistoricoCompleto;
        }
        catch { return ""; }

    }

    public void deletarDadosAntigos(string strCon, string rota,string mes,string ano)
    {
        //DELETAR DADOS DAS TABELAS
        //WTR_HISTORICO
        //WTR_CEP
        //WTR_BAIRRO
        //WTR_RUA
        Data db = new Data();
        //db.Exe(strCon, "WTR_PARCELA", "delete from wtr_historico where loc_id = " + rota);
        db.Exe(strCon, "WTR_FAIXA", "delete from WTR_FAIXA where fxa_mes = " + mes + " and fxa_ano = " + ano);
        db.Exe(strCon, "WTR_OBSCOMP", "delete from WTR_OBSCOMP where obs_mes = " + mes + " and obs_ano = " + ano);
        db.Exe(strCon, "WTR_PARAMETROS", "delete from WTR_PARAMETROS where PAR_MES = " + mes + " and PAR_ANO = " + ano);
        db.Exe(strCon, "WTR_BAIRRO", "delete from WTR_BAIRRO where loc_id = " + rota);
        db.Exe(strCon, "WTR_RUA", "delete from WTR_RUA where loc_id = " + rota);
        //db.Exe(strCon, "WTR_HISTORICO", "delete from WTR_HISTORICO where loc_id = " + rota);
        db.Exe(strCon, "WTR_vencimento", "delete from WTR_vencimento where loc_id = " + rota);
        db.Exe(strCon, "wtr_bloco", "delete from wtr_bloco where loc_id = " + rota);
        db.Exe(strCon, "WTR_tplancto", "delete from WTR_tplancto where planc_codlancto != 14 && planc_codlancto != 13");
        db.Exe(strCon, "WTR_TRIBUTACAO", "delete from WTR_TRIBUTACAO where loc_id = " + rota);



    }
    public bool VerificarRota(string strCon,string rota)
    {
        //verificar se rota ja existe no banco de dados
        Data db = new Data();
        DataTable dt = db.GetDt(strCon, "", "select * from wtr_livros where loc_id = " + rota + " and liv_mes  = " + Dados.Reference.Mes +
            "and liv_ano = " + Dados.Reference.Ano);
        if (dt.DefaultView.Count > 0)
            return true;
        else return false;

    }

    private string tipo10(string cont)
    {
        try
        {
            string tipo10 = "RES";

            if (cont.Length > 31)
            {
                if (Convert.ToInt32(cont.Substring(0, 4)) > 0)
                    tipo10 = "RES";
                else if (Convert.ToInt32(cont.Substring(7, 4)) > 0)
                    tipo10 = "COM";
                else if (Convert.ToInt32(cont.Substring(14, 4)) > 0)
                    tipo10 = "IND";
                else if (Convert.ToInt32(cont.Substring(21, 4)) > 0)
                    tipo10 = "PUB";
                else if (Convert.ToInt32(cont.Substring(28, 4)) > 0)
                    tipo10 = "SOC";
            }

            return tipo10;
        }
        catch (Exception ex)
        {
            return "";
        }
    }

    private string fase10(string cont)
    {
        try
        {
            string fase10 = "1";

            if (cont.Length > 31)
            {
                if (Convert.ToInt32(cont.Substring(4, 3)) > 0)
                    fase10 = cont.Substring(4, 3);
                else if (Convert.ToInt32(cont.Substring(11, 3)) > 0)
                    fase10 = cont.Substring(11, 3);
                else if (Convert.ToInt32(cont.Substring(18, 4)) > 0)
                    fase10 = cont.Substring(18, 4);
                else if (Convert.ToInt32(cont.Substring(25, 4)) > 0)
                    fase10 = cont.Substring(25, 4);
                else if (Convert.ToInt32(cont.Substring(32, 4)) > 0)
                    fase10 = cont.Substring(32, 4);
            }

            return fase10;
        }
        catch (Exception ex)
        {
            return "";
        }
    }


    private void insertFaixas(String[] dados, int x, string strCon,string nomeCategoria,int CatID)
    {
        #region Residencial

        String sqlFaixas = "insert into wtr_faixa (CLI_ID,EMP_ID,CAT_ID,FXA_ANO,FXA_MES,FXA_ID,FXA_CONSUMO,FXA_VALORAGUA,FXA_DEDUCAO) VALUES (18,1,"+CatID+"," +
            dados[0].Replace(dados[0].Split(' ').First(), " ")
                    .Trim()
                    .Substring(4) + "," + dados[1].Replace(dados[1].Split(' ').First(), " ")
                    .Trim()
                    .Substring(4) + "," + (x + 1) + ",";


        for (int h = 29; h < dados.Length; h++)
        {
            if (dados[h].ToUpper().Contains(("Cons" + nomeCategoria + (x + 1).ToString().PadLeft(2, '0')).ToUpper()))
            {
                sqlFaixas += dados[h].Replace(dados[h].Split(' ').First(), " ")
                    .Trim()
                    .Substring(4) + ",";
            }
            else if (dados[h].ToUpper().Contains(("Val"+nomeCategoria+"m3" + (x + 1).ToString().PadLeft(2, '0')).ToUpper()))
            {
                sqlFaixas += (Convert.ToDecimal(dados[h].Replace(dados[h].Split(' ').First(), " ")
                    .Trim()
                    .Substring(4)) / 100).ToString().Replace(',', '.') + ",";
            }
            else if (dados[h].ToUpper().Contains(("Val" + nomeCategoria + "Ded" + (x + 1).ToString().PadLeft(2, '0')).ToUpper()))
            {
                sqlFaixas += (Convert.ToDecimal(dados[h].Replace(dados[h].Split(' ').First(), " ")
                    .Trim()
                    .Substring(4)) / 100).ToString().Replace(',', '.') + ")";
                h = dados.Length;
            }
        }

        Data db = new Data();
        bool result = db.Exe(strCon, "WTR_FAIXA", sqlFaixas);

        #endregion
    }

    public void AnaliseCritica(String strCon, TextBox txtLivro, DropDownList cbxMsg, DropDownList cbxMes, TextBox txtAno, TextBox txtEndereco, TextBox txtNumero,
    CheckBox chkCritica, TextBox txtPercent, Label lblMsg, DropDownList ddlSituacaoLeitura, DropDownList ddlLeituristas, TextBox txtIdentificacao,
    TextBox txtMedidor, RadioButton rbEndereço, RadioButton rbLigacao, RadioButton rbSequencia, RadioButton rbNomeContribuinte, RadioButton rbRota, GridView gridView, Panel panelMsg, Panel PanelQtdRegistros, Label lblTOTAL,
     DropDownList ddlTipoRelatorio)
    {
        DataTable dt = new DataTable();

        try
        {


            double porc = 0;
            string pMax = "";
            string pMin = "";

            #region Porcentagem
            if (txtPercent.Text.Trim().Length < 1)
                porc = 1;
            else
            {
                try
                {
                    porc = Convert.ToInt32(txtPercent.Text.Trim());
                    pMax = Convert.ToDecimal(1 + (porc / 100)).ToString().Replace(",", ".");
                    pMin = Convert.ToDecimal(1 - (porc / 100)).ToString().Replace(",", ".");
                }
                catch (Exception ee)
                {
                    lblMsg.Text = "Informe o percentual correto da crítica";
                }
            }
            #endregion
            Data db = new Data();
            string sql = "";
            if (ddlTipoRelatorio.SelectedValue.ToString() == "Credito de Consumo")
            {

               sql = "select LOC_ID AS ROTA, LEI_CONSUMIDOR as CONSUMIDOR, LEI_LIGACAO as LIGACAO, CALCULO = CASE MSG_CALCULO  when '0' THEN 'NORMAL' WHEN '1' THEN 'MEDIA' END, LIV_MES AS MES, LIV_ANO AS ANO," +
                    "LEI_ANTERIOR as 'LEIT. ANT', LEI_LEITURA as 'LEIT. ATUAL', LEI_CONSUMO as CONSUMO, LEI_CONSUMOFAT as'CONSUMO FAT.', LEI_CREDITOCONSUMO as 'CRED. CONSUMO' , LEI_SALDOCONSUMO as 'SALDO CONSUMO'"
                        +"from wtr_leituras INNER JOIN WTR_MENSAGENS ON WTR_MENSAGENS.MSG_ID = WTR_LEITURAS.MSG_ID"
                        + " WHERE lei_creditoconsumo != '' AND WTR_LEITURAS.emp_id = " + empresa + " and WTR_LEITURAS.cid_id = " + empresa + " and WTR_LEITURAS.CLI_ID = " + cliente;

            }
            else
            {
                sql = "select LIV_ANO AS ANO, LIV_MES AS MES,CAST(lei_ligacao AS VARCHAR) AS LIGACAO,LEI_CONSUMIDOR AS CONSUMIDOR, WTR_LEITURAS.LOC_ID AS ROTA," +
               " (SELECT RUA_NOME FROM WTR_RUA WHERE RUA_ID = LEI_RUA group by rua_nome) AS ENDEREÇO, LEI_NUMERO AS NUMERO, LEI_DATA AS DATA, LTR_NOME AS LEITURISTA, " +
                        " MSG_DESCRICAO AS OCORRENCIA, LEI_MEDIA AS MEDIA, LEI_ANTERIOR AS 'LEIT. ANT', LEI_LEITURA AS 'LEIT. ATUAL', " +
                        " LEI_NUMMED AS MEDIDOR,  LEI_CONSUMO AS CONSUMO,LEI_FORMAENTREGA as ENTREGA,LEI_LATITUDE,LEI_LONGITUDE ,HIS_CONSUMO12,HIS_LEITURA12,HIS_CONSUMO11,HIS_LEITURA11,HIS_CONSUMO10," +
                        "HIS_LEITURA10,HIS_CONSUMO9,HIS_LEITURA9,HIS_CONSUMO8,HIS_LEITURA8,HIS_CONSUMO7,HIS_LEITURA7,HIS_CONSUMO6,HIS_LEITURA6,HIS_CONSUMO5,HIS_LEITURA5," +
                        "HIS_CONSUMO4,HIS_LEITURA4,HIS_CONSUMO3,HIS_LEITURA3,HIS_CONSUMO2,HIS_LEITURA2,HIS_CONSUMO1,HIS_LEITURA1 ";


                sql += " FROM WTR_LEITURAS INNER JOIN WTR_MENSAGENS ON(WTR_LEITURAS.MSG_ID = WTR_MENSAGENS.MSG_ID and WTR_LEITURAS.cli_id = WTR_MENSAGENS.cli_id and WTR_MENSAGENS.emp_id = WTR_LEITURAS.emp_id) INNER JOIN WTR_LEITURISTAS ON (WTR_LEITURAS.LTR_ID = WTR_LEITURISTAS.LTR_ID  and WTR_LEITURAS.cli_id = WTR_LEITURISTAS.cli_id and WTR_LEITURISTAS.emp_id = WTR_LEITURAS.emp_id) ";
                sql += " INNER JOIN WTR_HISTORICO ON (CONVERT(INT,HIS_LIGACAO) = CONVERT(INT,LEI_LIGACAO) AND WTR_HISTORICO.LOC_ID = WTR_LEITURAS.LOC_ID) ";
                sql += " WHERE WTR_LEITURAS.emp_id =" + empresa + " and WTR_LEITURAS.cid_id =" + empresa + " and WTR_LEITURAS.CLI_ID = " + cliente +" AND WTR_HISTORICO.HIS_MESREF = " + cbxMes.SelectedIndex.ToString();
            }
            #region SQL E FILTROS

            if (txtLivro.Text.Trim().Length > 0)
            {
                sql += " AND WTR_LEITURAS.LOC_ID = '" + txtLivro.Text.Trim() + "' ";
            }

            if (cbxMsg.SelectedValue != "1000")
                sql += " AND WTR_LEITURAS.MSG_ID = '" + cbxMsg.SelectedValue.ToString() + "' ";

            if (cbxMes.SelectedIndex > 0)
                sql += " AND LIV_MES = '" + cbxMes.SelectedIndex.ToString() + "' ";

            if (txtAno.Text.Trim().Length > 0)
                sql += " AND LIV_ANO = '" + txtAno.Text.Trim() + "' ";

            if (txtEndereco.Text.Trim().Length > 0)
                sql += " AND (LEI_ENDERECO LIKE '" + txtEndereco.Text + "' or LEI_ENDERECOENTREGA LIKE '" + txtEndereco.Text + "') ";

            if (txtNumero.Text.Trim().Length > 0)
                sql += " AND (LEI_NUMERO = " + txtNumero.Text + " or LEI_NUMEROENTREGA LIKE '" + txtNumero.Text + "') ";

            #region Somente critica
            if (chkCritica.Checked)
            {
                sql += " AND (((LEI_LEITURA - LEI_ANTERIOR) > (LEI_MEDIA * " + pMax + ")) OR ((LEI_LEITURA - LEI_ANTERIOR) < (LEI_MEDIA * " + pMin + ")))";
            }
            #endregion

            #region Sem Leitura
            if (ddlSituacaoLeitura.SelectedValue.ToString() == "Com Leitura")
            {
                sql += " AND LEI_LEITURA <> ''";
            }
            else
            {
                if (ddlSituacaoLeitura.SelectedValue.ToString() == "Sem Leitura")
                {
                    sql += " AND (LEI_LEITURA = '' or lei_leitura is null)";
                }
            }
            #endregion
            #region SITUACAO LEITURA
            if (ddlSituacaoLeitura.SelectedValue.ToString() == "Alteração cadastral")
                sql += " AND LEI_NOVACATEGORIA > 0";

            #endregion


            #region Leituristas
            if (ddlLeituristas.SelectedValue.ToString() != "0")
            {
                sql += " AND WTR_LEITURAS.LTR_ID  =  " + ddlLeituristas.SelectedValue;
            }
            #endregion

            #region Identificação e Medidor
            if ((txtIdentificacao.Text.Trim().Length > 0) || (txtMedidor.Text.Trim().Length > 0))
            {
                if (txtIdentificacao.Text.Trim().Length > 0)
                    sql += " AND LEI_LIGACAO LIKE '%" + txtIdentificacao.Text.Trim() + "%' ";

                if (txtMedidor.Text.Trim().Length > 0)
                    sql += " AND LEI_NUMMED LIKE '%" + txtMedidor.Text.Trim() + "%' ";
            }

            #endregion

            #endregion
           if (rbEndereço.Checked)
                sql += " ORDER BY LEI_ENDERECO,LEI_NUMERO,LIV_ANO, LIV_MES, WTR_LEITURAS.LOC_ID,LEI_SEQ";
            else if (rbLigacao.Checked)
                sql += " ORDER BY lei_ligacao,LIV_ANO, LIV_MES, WTR_LEITURAS.LOC_ID";
            else if (rbSequencia.Checked)
                sql += " ORDER BY lei_seq,LIV_ANO, LIV_MES, WTR_LEITURAS.LOC_ID";
            else if (rbNomeContribuinte.Checked)
                sql += " ORDER BY lei_consumidor,LIV_ANO, LIV_MES, WTR_LEITURAS.LOC_ID";
            else if (rbRota.Checked)
                sql += " ORDER BY WTR_LEITURAS.LOC_ID,LIV_ANO, LIV_MES,lei_seq";
            else
            {

                sql += " ORDER BY LIV_ANO, LIV_MES, lei_seq";
            }            

            dt = db.GetDt(strCon, "WTR_LEITURAS", sql);

            if (dt.DefaultView.Count > 0)
            {
                lblTOTAL.Text = string.Format("{0:C}", Convert.ToDecimal(dt.DefaultView.Count.ToString())).Replace("R$", "").Replace(" ", "").Split(',')[0];
                PanelQtdRegistros.Visible = true;
            }
            else PanelQtdRegistros.Visible = false;

            gridView.DataSource = dt;
            gridView.DataBind();


        }
        catch (Exception ex)
        {
            lblMsg.Text = "Erro - " + ex.Message;
            panelMsg.Visible = true;

        }

    }

    public void Export(string cam, string file, string cliente, string caminhoProjeto, String strCon, HttpResponse response, int mes, string ano, Button btDownload)
    {
        DataTable dt;
        DataTable dt2 = null;
        DataTable dtParc = null;
        DataTable dtVencimento = null;
        string ext = "";
        Data db = new Data();
        string separador = xml.GetConfigValue(caminhoProjeto, "LOCAL", "EXPSEPARADOR", Convert.ToInt32(cliente).ToString());
        if (Convert.ToInt32(cliente) == 3)
            ext = "s";
        else if ((Convert.ToInt32(cliente) == 4) || (Convert.ToInt32(cliente) == 10))
            ext = "RET";
        else if ((Convert.ToInt32(cliente) == 5))
            ext = "SIM";
        else
            ext = "txt";
        string sql = "";
        string sql2 = "";//Busca Qtd Ocorrências

        int p = 0;
        int i = 0;
        string fileNew = "";
        Export export = new Export();
        ExportDAL expDAL = new ExportDAL();
        StreamWriter sw = StreamWriter.Null;
        int qtd1 = 0, qtd2 = 0, qtd3 = 0;

        try
        {

            int cid = Convert.ToInt32(file.Substring(3, 3));
            int loc = Convert.ToInt32(file.Substring(6, 3));


            #region Buscando informações no arquivo Import.xml

            //Verificar quais tabelas a serem exportadas (Config.xml : <TABEXPORT>)

            string[] tabExport = xml.GetConfigValue(caminhoProjeto, "LOCAL", "TABEXPORT", Convert.ToInt32(cliente).ToString()).Split(',');

            #region MESMO ARQUIVO DE EXPORTAÇÃO PARA DUAS OU MAIS TABELAS
            if (fileNew.Length == 0)
            {


                if (Convert.ToInt32(cliente) == 18) //Iracemapolis
                {
                    DateTime data = DateTime.Now;
                    sw = new StreamWriter(cam + @"\" + DateTime.Now.Date.ToShortDateString().Replace("/", "") + "_" + DateTime.Now.Date.ToShortTimeString().Replace(":", "") + "RO" + loc.ToString().PadLeft(2, '0') + "." + ext);
                }
            }
            else
            {
                if (sw.Equals(StreamWriter.Null))
                {
                    if (fileNew.Length > 0)
                        sw = new StreamWriter(cam + @"\" + fileNew.Trim() + "." + ext);

                    else
                        sw = new StreamWriter(cam + @"\" + tabExport[i].Trim() + "_" + file + "." + ext);
                }
            }
            #endregion

            while (i < tabExport.Length)
            {
                export = expDAL.getExp(Convert.ToInt32(cliente), tabExport[i].Trim());

                #region Leitura de Conteúdo do Arquivo de Exportação
                string[] campos = export.Campos.Split('|');
                string tabela = campos[0];
                string[] field = new string[campos.Length - 1];
                string[] tamanho = new string[campos.Length - 1];
                string[] formato = new string[campos.Length - 1];
                string[] valor = new string[campos.Length - 1];

                p = 0;
                while (p < (campos.Length - 1))
                {
                    field[p] = campos[p + 1].Split(';')[0].Trim();
                    formato[p] = campos[p + 1].Split(';')[1].Trim();
                    tamanho[p] = campos[p + 1].Split(';')[2].Trim();

                    if (campos[p + 1].Split(';').Length > 3)	//Valor fixo
                    {
                        valor[p] = campos[p + 1].Split(';')[3].Replace('"', ' ').Trim();
                    }

                    p++;
                }
                #endregion

                mes = Convert.ToInt32(file.Substring(0, 3));
               

                #region Comando SQL para o BANCO DE DADOS
                sql = "SELECT ";

                p = 0;
                while (p < field.Length)
                {
                    sql += field[p].Trim() + ",";
                    p++;
                }

                sql = sql.Remove(sql.Length - 1, 1);
                sql += " FROM " + tabela;

                if ((tabela == "WTR_LIVROS") || (tabela == "WTR_LEITURAS"))
                {
                    sql += " WHERE LIV_MES = " + mes
                        + " AND LIV_ANO = " + ano
                        + " AND CID_ID = " + "1"
                        + " AND LOC_ID = " + loc
                        + " AND CLI_ID = " + Convert.ToInt32(cliente);
                    if (empresa != "0")
                        sql += " AND EMP_ID = " + empresa;
                    if (cliente == "14")
                        sql += " AND LEI_DATA != ''";

                    #region Verifica Qtd Ocorrências
                    if (Convert.ToInt32(cliente) == 5 || Convert.ToInt32(cliente) == 14)
                    {
                        if ((tabela == "WTR_LEITURAS"))//&&(Convert.ToInt32(cliente) == 5)
                        {
                            sql2 = "SELECT LTR_ID, MSG_ID, COUNT(*) AS QTD FROM WTR_LEITURAS WHERE LIV_MES = '" + mes + "'"
                            + " AND CID_ID = '" + cid + "'"
                            + " AND LIV_ANO = " + ano
                            + " AND LOC_ID = '" + loc + "'"
                            + " AND CLI_ID = '" + Convert.ToInt32(cliente) + "'"
                            + " GROUP BY LTR_ID, MSG_ID "
                            + " ORDER BY MSG_ID ";

                            dt2 = db.GetDt(strCon, tabela, sql2);
                        }
                    }
                    if (Convert.ToInt32(cliente) == 2)
                    {
                        if ((tabela == "WTR_LEITURAS"))//&&(Convert.ToInt32(cliente) == 5)
                        {
                            sql2 = "SELECT LTR_ID, MSG_ID, COUNT(*) AS QTD FROM WTR_LEITURAS WHERE LIV_MES = '" + mes + "'"
                            + " AND CID_ID = '" + cid + "'"
                            + " AND LIV_ANO = " + ano
                            + " AND LOC_ID = '" + loc + "'"
                            + " AND CLI_ID = '" + Convert.ToInt32(cliente) + "'"
                            + " GROUP BY LTR_ID, MSG_ID "
                            + " ORDER BY MSG_ID ";

                            dt2 = db.GetDt(strCon, tabela, sql2);
                        }
                    }
                    #endregion
                }
                else if (tabela.IndexOf("WTR_FATURA") >= 0)
                {
                    sql += " WHERE WTR_FATURA.FAT_MES = " + mes.ToString().PadLeft(2, '0')
                        + " AND WTR_FATURA.FAT_ANO = '" + ano.PadLeft(2, '0') + "'"
                       + " AND WTR_FATURA.CLI_ID = " + Convert.ToInt32(cliente)
                       + " AND WTR_FATURA.FAT_PASTA = " + loc.ToString();
                    if (empresa != "0")
                        sql += " AND WTR_FATURA.EMP_ID = " + empresa;

                    if (Convert.ToInt32(cliente) != 5)
                    {
                        sql2 = "SELECT LTR_ID, MSG_ID, COUNT(*) AS QTD FROM WTR_FATURA WHERE FAT_MES = " + mes
                        + " AND FAT_PASTA = '" + loc + "'"
                        + " AND FAT_ANO = " + ano + ""
                        + " AND CLI_ID = '" + Convert.ToInt32(cliente) + "'"
                        + " GROUP BY LTR_ID, MSG_ID "
                        + " ORDER BY MSG_ID ";

                        dt2 = db.GetDt(strCon, tabela, sql2);
                    }
                    else
                    {
                        sql2 = "SELECT LTR_ID, MSG_ID, COUNT(*) AS QTD FROM WTR_FATURA WHERE FAT_MES = " + mes
                        + " AND FAT_PASTA = '" + loc + "'"
                        + " AND FAT_ANO = " + ano + ""
                        + " AND CLI_ID = '" + Convert.ToInt32(cliente) + "'"
                        + " GROUP BY LTR_ID, MSG_ID "
                        + " ORDER BY MSG_ID ";

                        dt2 = db.GetDt(strCon, tabela, sql2);
                    }
                }
                else if (tabela == "WTR_SERVICOS")
                {
                    sql += " WHERE SRV_MES = " + mes
                        + " AND SRV_LOCAL = " + loc
                        + " AND CLI_ID = " + Convert.ToInt32(cliente);
                    if (empresa != "0")
                        sql += " AND EMP_ID = " + empresa;
                }
                else if (tabela == "WTR_TARIFA" && Convert.ToInt32(cliente) == 5)
                {
                    sql += " WHERE TXA_MES = " + mes
                        + " AND LOC_ID = " + loc
                        + " AND TXA_ANO = " + ano
                        + " AND CLI_ID = " + Convert.ToInt32(cliente);
                    if (empresa != "0")
                        sql += " AND EMP_ID = " + empresa;
                }
                else if (tabela == "WTR_REGISTROLOG")
                {
                    sql += " WHERE LOG_MES = " + mes
                        + " AND LOG_LOCAL = " + loc
                        + " AND CLI_ID = " + Convert.ToInt32(cliente);
                    if (empresa != "0")
                        sql += " AND EMP_ID = " + empresa;
                }   



                dt = db.GetDt(strCon, tabExport[i].Trim(), sql);

                if (dt.Rows.Count > 0)
                {
                   // msg += ("Exportando dados : " + tabExport[i].Trim().ToUpper());

                    

                    


                    for (int a = 0; a < dt.Rows.Count; a++)
                    {
                        //lblCount.Text = a.ToString();
                        //lblCount.Refresh();
                        string linha = string.Empty;                        

                        #region Leitura de Campos na Tabela
                        for (int n = 0; n < campos.Length - 1; n++)
                        {
                            if (Convert.ToInt32(tamanho[n].Trim()) > 0)
                            {
                                if (valor[n] == null)
                                {
                                    if (formato[n] == "N")
                                    {
                                        #region "N"
                                        decimal vlr = 0;
                                        if (dt.Rows[a][field[n]].ToString().Trim().Length > 0)
                                            vlr = Convert.ToDecimal(dt.Rows[a][field[n]].ToString().Replace('.', ','));
                                        if (vlr >= 0)
                                            linha += vlr.ToString().PadLeft(Convert.ToInt32(tamanho[n].Trim()), '0');
                                        else
                                        {
                                            vlr = vlr * (-1);
                                            linha += "-" + vlr.ToString().PadLeft(Convert.ToInt32(tamanho[n].Trim()) - 1, '0');
                                        }
                                        #endregion
                                    }
                                    else if (formato[n] == "T")
                                        linha += dt.Rows[a][field[n]].ToString().PadRight(Convert.ToInt32(tamanho[n].Trim()), ' ');
                                    else if (formato[n] == "TD")
                                    {
                                        if (!dt.Rows[a][field[n]].ToString().Trim().Equals(""))
                                            linha +=Convert.ToDecimal(dt.Rows[a][field[n]].ToString().Replace(".",",")).ToString("0.00").PadRight(Convert.ToInt32(tamanho[n].Trim()), ' ').Replace(",",".");
                                        else linha += dt.Rows[a][field[n]].ToString().PadRight(Convert.ToInt32(tamanho[n].Trim()), ' ').Replace(",", ".");
                                    }
                                    else if (formato[n] == "msg")
                                    {
                                        string msg = dt.Rows[a][field[n]].ToString();
                                        if (msg == "")
                                            msg = "0";
                                        linha += dt.Rows[a][field[n]].ToString().PadRight(Convert.ToInt32(tamanho[n].Trim()), ' ');
                                    }
                                    else if (formato[n] == "msg")
                                    {
                                        string msg = dt.Rows[a][field[n]].ToString();
                                        if (msg == "")
                                            msg = "0";
                                        linha += dt.Rows[a][field[n]].ToString().PadRight(Convert.ToInt32(tamanho[n].Trim()), ' ');
                                    }
                                    else if (formato[n] == "situacao")
                                    {
                                        string sit = " ";
                                        if (dt.Rows[a][field[n]].ToString().Trim() == "L")
                                        {
                                            sit = "N";
                                        }
                                        else if (dt.Rows[a][field[n]].ToString().Trim() == "C" || dt.Rows[a][field[n]].ToString().Trim() == "E")
                                        {
                                            sit = "S";
                                        }

                                        linha += sit.PadRight(Convert.ToInt32(tamanho[n].Trim()), ' ');
                                    }
                                    else if (formato[n] == "vencimento")
                                    {
                                        string venc = "";

                                        String sqlVen = "select VEN_DATA" + mes + " from WTR_VENCIMENTO WHERE LOC_ID = " + loc + " AND VEN_ID = " + dt.Rows[a][field[n]].ToString().Trim() + " AND VEN_ANO = " + ano;

                                        dtVencimento = db.GetDt(strCon, "WTR_VENCIMENTO", sqlVen);


                                        linha += dtVencimento.Rows[0][0].ToString().Replace("/", "");
                                    }
                                    else if (formato[n] == "credito")
                                    {
                                        string cred = dt.Rows[a][field[n]].ToString();
                                        if (cred == "")
                                            cred = "0";
                                        linha += cred.ToString().PadRight(Convert.ToInt32(tamanho[n].Trim()), ' ');
                                    }


                                    else if (formato[n] == "Decimal")
                                    {
                                        #region "Decimal"
                                        decimal vlr = 0;
                                        if (dt.Rows[a][field[n]].ToString().Trim().Length > 0)
                                            vlr = Convert.ToDecimal(dt.Rows[a][field[n]].ToString().Replace('.', ','));
                                        if (vlr >= 0)
                                            if (vlr > 0)
                                                linha += (vlr.ToString().Split(',')[0] + "," + vlr.ToString().Split(',')[1].PadRight(2, ' ')).PadLeft(Convert.ToInt32(tamanho[n].Trim()), ' ').Replace(',', '.');
                                            else
                                                linha += vlr.ToString("0.00").PadRight(Convert.ToInt32(tamanho[n].Trim()), ' ').Replace(',', '.');

                                        else
                                        {
                                            vlr = vlr * (-1);
                                            linha += "-" + vlr.ToString().PadLeft(Convert.ToInt32(tamanho[n].Trim()) - 1, '0').Replace(',', '.');
                                        }
                                        #endregion

                                        /* #region "Decimal"
                                        decimal vlr = 0;
                                        if (dt.Rows[a][field[n]].ToString().Trim().Length > 0)
                                            vlr = Convert.ToDecimal(dt.Rows[a][field[n]].ToString().Replace('.', ','));
                                        if (vlr >= 0)
                                            linha += vlr.ToString().PadLeft(Convert.ToInt32(tamanho[n].Trim()), '0').Replace(',', '.');
                                        else
                                        {
                                            vlr = vlr * (-1);
                                            linha += "-" + vlr.ToString().PadLeft(Convert.ToInt32(tamanho[n].Trim()) - 1, '0').Replace(',', '.');
                                        }
                                        #endregion*/
                                    }

                                    else if (formato[n] == "Versao")
                                    {
                                        #region "Versao"
                                        linha += "4.9".ToString().PadRight(Convert.ToInt32(tamanho[n].Trim()), ' ');


                                        #endregion
                                    }
                                    else if (formato[n] == "Null")
                                    {
                                        #region "Null"
                                        if (dt.Rows[a][field[n]].ToString().Trim() != "")
                                        {
                                            if (Convert.ToInt32(dt.Rows[a][field[n]].ToString().Trim()) == 0)
                                                linha += " ".PadRight(Convert.ToInt32(tamanho[n].Trim()), ' ');
                                            else
                                            {
                                                decimal vlr = 0;
                                                if (dt.Rows[a][field[n]].ToString().Trim().Length > 0)
                                                    vlr = Convert.ToDecimal(dt.Rows[a][field[n]].ToString().Replace('.', ','));
                                                if (vlr >= 0)
                                                    linha += vlr.ToString().PadLeft(Convert.ToInt32(tamanho[n].Trim()), '0');
                                                else
                                                {
                                                    vlr = vlr * (-1);
                                                    linha += "-" + vlr.ToString().PadLeft(Convert.ToInt32(tamanho[n].Trim()) - 1, '0');
                                                }
                                            }
                                        }
                                        else
                                            linha += " ".PadRight(Convert.ToInt32(tamanho[n].Trim()), ' ');


                                        #endregion
                                    }
                                    else if (formato[n] == "num")
                                    {
                                        #region "num"
                                        decimal vlr = 0;
                                        if (dt.Rows[a][field[n]].ToString().Contains("."))
                                            vlr =
                                                Convert.ToInt32(
                                                    dt.Rows[a][field[n]].ToString().Split('.')[0].ToString());
                                        else
                                        {
                                            if (dt.Rows[a][field[n]].ToString().Trim().Length > 0)
                                                vlr = Convert.ToInt32(dt.Rows[a][field[n]].ToString());
                                        }

                                        if (vlr >= 0)
                                            linha += vlr.ToString().PadLeft(Convert.ToInt32(tamanho[n].Trim()), '0').Replace(',', '.');
                                        else
                                        {
                                            vlr = vlr * (-1);
                                            linha += "-" + vlr.ToString().PadLeft(Convert.ToInt32(tamanho[n].Trim()) - 1, '0').Replace(',', '.');
                                        }
                                        #endregion
                                    }
                                    else if (formato[n] == "D")
                                    {
                                        #region "D"
                                        if (Convert.ToString(dt.Rows[a][field[n]].ToString().Replace("/", "").PadRight(Convert.ToInt32(tamanho[n].Trim()), ' ')).Trim().Length <= 0)
                                        {
                                            string dataLeit = DateTime.Now.Day.ToString().PadLeft(2, '0')
                                                            + DateTime.Now.Month.ToString().PadLeft(2, '0')
                                                            + DateTime.Now.Year.ToString().PadLeft(4, '0');
                                            linha += dataLeit;
                                        }
                                        else
                                            linha += dt.Rows[a][field[n]].ToString().Replace("/", "").PadRight(Convert.ToInt32(tamanho[n].Trim()), ' ');
                                        #endregion
                                    }
                                    else if (formato[n] == "Data")
                                    {
                                        #region "Data"
                                        if (dt.Rows[a][field[n]].ToString().Trim().Length > 0)
                                        {
                                            linha += dt.Rows[a][field[n]].ToString().Trim().Replace("-", "").Replace("/", "").Substring(2, 2) + dt.Rows[a][field[n]].ToString().Trim().Replace("-", "").Replace("/", "").Substring(0, 2) + dt.Rows[a][field[n]].ToString().Trim().Replace("-", "").Replace("/", "").Substring(4, 4);
                                        }
                                        else
                                            linha += dt.Rows[a][field[n]].ToString().Replace("/", "").PadRight(Convert.ToInt32(tamanho[n].Trim()), ' ');
                                        #endregion
                                    }
                                    else if (formato[n] == "DataDAEM")
                                    {
                                        #region "Data"
                                        if (dt.Rows[a][field[n]].ToString().Trim().Length > 0)
                                        {
                                            linha += dt.Rows[a][field[n]].ToString().Trim().Replace("-", "").Replace("/", "");
                                        }
                                        else
                                            linha += dt.Rows[a][field[n]].ToString().Replace("/", "").PadRight(Convert.ToInt32(tamanho[n].Trim()), ' ');
                                        #endregion
                                    }

                                    else if (formato[n] == "dataClementina")
                                    {
                                        #region "Data"
                                        if (dt.Rows[a][field[n]].ToString().Trim().Length > 0)
                                        {
                                            linha += dt.Rows[a][field[n]].ToString().Trim().Replace("/", "").Replace(" ", "").Replace(":", "").Substring(0, 12).PadRight(Convert.ToInt32(tamanho[n].Trim()), ' ');
                                        }
                                        else
                                            linha += dt.Rows[a][field[n]].ToString().Replace("/", "").PadRight(Convert.ToInt32(tamanho[n].Trim()), ' ');
                                        #endregion
                                    }

                                    else if (formato[n] == "NLigacao")
                                    {
                                        #region "N"
                                        decimal vlr = 0;
                                        if (dt.Rows[a][field[n]].ToString().Trim().Length > 0)
                                            vlr = Convert.ToDecimal(dt.Rows[a][field[n]].ToString().Replace('.', ',').Substring(0, dt.Rows[a][field[n]].ToString().Replace('.', ',').Length - 1));
                                        if (vlr >= 0)
                                            linha += vlr.ToString().PadLeft(Convert.ToInt32(tamanho[n].Trim()), '0');
                                        else
                                        {
                                            vlr = vlr * (-1);
                                            linha += "-" + vlr.ToString().PadLeft(Convert.ToInt32(tamanho[n].Trim()) - 1, '0');
                                        }
                                        #endregion
                                    }

                                    else if (formato[n] == "Hora")
                                    {
                                        #region "Data"
                                        if (dt.Rows[a][field[n]].ToString().Trim().Length > 0)
                                        {
                                            linha += dt.Rows[a][field[n]].ToString().Trim().Replace(":", "").Replace(" ", "");
                                        }
                                        else
                                            linha += dt.Rows[a][field[n]].ToString().Replace(":", "").Replace(" ", "").PadRight(Convert.ToInt32(tamanho[n].Trim()), ' ');
                                        #endregion
                                    }
                                    else if (formato[n] == "/100")
                                    {
                                        #region "/100"
                                        decimal ttmp = 0;

                                        if (dt.Rows[a][field[n]].ToString().Trim().Length > 0)
                                            ttmp = Convert.ToDecimal(dt.Rows[a][field[n]].ToString()) / 100;

                                        if (ttmp >= 0)
                                            linha += ttmp.ToString().PadLeft(Convert.ToInt32(tamanho[n].Trim()), '0');
                                        else
                                        {
                                            ttmp = ttmp * (-1);
                                            linha += "-" + ttmp.ToString().PadLeft(Convert.ToInt32(tamanho[n].Trim()) - 1, '0');
                                        }
                                        #endregion
                                    }
                                    else if (formato[n] == "*100")
                                    {
                                        #region "*100"
                                        string ttmp = "0";

                                        if (dt.Rows[a][field[n]].ToString().Trim().Length > 0)
                                            ttmp = (Convert.ToDecimal(dt.Rows[a][field[n]].ToString()) * 100).ToString().Replace(',', '.').Split('.')[0];

                                        if (Convert.ToDecimal(ttmp) >= 0)
                                            linha += ttmp.ToString().PadLeft(Convert.ToInt32(tamanho[n].Trim()), '0');
                                        else
                                        {
                                            ttmp = (Convert.ToDecimal(ttmp) * (-1)).ToString();
                                            linha += "-" + ttmp.ToString().PadLeft(Convert.ToInt32(tamanho[n].Trim()) - 1, '0');
                                        }
                                        #endregion
                                    }
                                    else if (formato[n] == "Right")
                                    {
                                        #region "Right"
                                        if (dt.Rows[a][field[n]].ToString().Length > 0)
                                        {
                                            int dife = dt.Rows[a][field[n]].ToString().Length - Convert.ToInt32(tamanho[n].Trim());
                                            linha += dt.Rows[a][field[n]].ToString().Substring(dife, dt.Rows[a][field[n]].ToString().Length - dife);
                                        }
                                        else
                                            linha += dt.Rows[a][field[n]].ToString().PadLeft(Convert.ToInt32(tamanho[n].Trim(), ' '));
                                        #endregion
                                    }
                                    else if (formato[n].Contains("Left-"))
                                    {
                                        #region "Left-"
                                        if (dt.Rows[a][field[n]].ToString().Length > 0)
                                        {
                                            int ret = Convert.ToInt32(formato[n].Split('-')[1].ToString());
                                            int dife = dt.Rows[a][field[n]].ToString().Length - ret;

                                            if (formato[n].Contains("N"))
                                                linha += dt.Rows[a][field[n]].ToString().Substring(0, dife).PadLeft(Convert.ToInt32(tamanho[n].Trim()), '0');
                                            else
                                                linha += dt.Rows[a][field[n]].ToString().Substring(0, dife).PadRight(Convert.ToInt32(tamanho[n].Trim()), ' ');
                                        }
                                        else
                                        {
                                            if (formato[n].Contains("N"))
                                                linha += dt.Rows[a][field[n]].ToString().PadLeft(Convert.ToInt32(tamanho[n].Trim(), '0'));
                                            else
                                                linha += dt.Rows[a][field[n]].ToString().PadRight(Convert.ToInt32(tamanho[n].Trim(), ' '));
                                        }
                                        #endregion
                                    }
                                    else if ((formato[n] == "RightDate") && (tamanho[n] == "6"))
                                    {
                                        #region "RightDate"
                                        if (dt.Rows[a][field[n]].ToString().Length > 0)
                                        {
                                            int dife = dt.Rows[a][field[n]].ToString().Length - 8;
                                            string tpm = dt.Rows[a][field[n]].ToString().Substring(dife, dt.Rows[a][field[n]].ToString().Length - dife);
                                            tpm = tpm.Substring(0, 4) + tpm.Substring(6, 2);
                                            linha += tpm;
                                        }
                                        else
                                            linha += dt.Rows[a][field[n]].ToString().PadLeft(Convert.ToInt32(tamanho[n].Trim()), ' ');
                                        #endregion
                                    }
                                    else if (formato[n].Contains("Right"))
                                    {
                                        #region "Right"
                                        if (dt.Rows[a][field[n]].ToString().Length > 0)
                                        {
                                            int ctr = dt.Rows[a][field[n]].ToString().Length;

                                            if (formato[n].Contains("("))
                                                ctr = Convert.ToInt32(formato[n].Split('(')[1].ToString().Replace(")", ""));

                                            if (dt.Rows[a][field[n]].ToString().Length > ctr)
                                                ctr = dt.Rows[a][field[n]].ToString().Length - ctr;
                                            else
                                                ctr = dt.Rows[a][field[n]].ToString().Length;

                                            if (ctr > Convert.ToInt32(tamanho[n].Trim()))
                                                ctr = Convert.ToInt32(tamanho[n].Trim());

                                            if ((formato[n].Contains("N")) && (Convert.ToInt32(cliente) == 4))//SAAE PB
                                                linha += dt.Rows[a][field[n]].ToString().Substring(0, ctr).PadLeft(Convert.ToInt32(tamanho[n].Trim()), '0');
                                            else if (formato[n].Contains("N"))
                                                linha += dt.Rows[a][field[n]].ToString().Substring(ctr).PadRight(Convert.ToInt32(tamanho[n].Trim()), '0');
                                            else
                                                linha += dt.Rows[a][field[n]].ToString().Substring(ctr).PadLeft(Convert.ToInt32(tamanho[n].Trim()), ' ');

                                        }
                                        else
                                        {
                                            if (formato[n].Contains("N"))
                                                linha += dt.Rows[a][field[n]].ToString().PadLeft(Convert.ToInt32(tamanho[n].Trim()), '0');
                                            else
                                                linha += dt.Rows[a][field[n]].ToString().PadRight(Convert.ToInt32(tamanho[n].Trim()), ' ');
                                        }
                                        #endregion
                                    }
                                    else if (formato[n].Contains("Left"))
                                    {
                                        #region "Left"
                                        if (dt.Rows[a][field[n]].ToString().Length > 0)
                                        {
                                            int ctr = dt.Rows[a][field[n]].ToString().Length;

                                            if (formato[n].Contains("("))
                                                ctr = Convert.ToInt32(formato[n].Split('(')[1].ToString().Replace(")", ""));

                                            if (dt.Rows[a][field[n]].ToString().Length < ctr)
                                                ctr = dt.Rows[a][field[n]].ToString().Length;

                                            if (ctr > Convert.ToInt32(tamanho[n].Trim()))
                                                ctr = Convert.ToInt32(tamanho[n].Trim());

                                            string dtLeit = dt.Rows[a][field[n]].ToString().Substring(0, ctr).PadLeft(Convert.ToInt32(tamanho[n].Trim()), ' ');
                                            if (formato[n].Contains("N"))
                                                dtLeit = dt.Rows[a][field[n]].ToString().Substring(0, ctr).PadRight(Convert.ToInt32(tamanho[n].Trim()), '0');

                                            if (formato[n].Contains("Date"))
                                            {
                                                if (dtLeit.Substring(dtLeit.Length - 2, 2) == "00")
                                                    dtLeit = dtLeit.Substring(0, 4) + "20" + dtLeit.Substring(4, 2);
                                            }
                                            linha += dtLeit;
                                        }
                                        else
                                        {
                                            if (formato[n].Contains("N"))
                                                linha += dt.Rows[a][field[n]].ToString().PadLeft(Convert.ToInt32(tamanho[n].Trim()), '0');
                                            else
                                                linha += dt.Rows[a][field[n]].ToString().PadRight(Convert.ToInt32(tamanho[n].Trim()), ' ');
                                        }
                                        #endregion
                                    }
                                    else if (formato[n].Contains("Seq"))
                                    {
                                        #region "Seq"
                                        int b = a + 1;
                                        linha += b.ToString().PadLeft(Convert.ToInt32(tamanho[n]), '0');

                                        #endregion
                                    }
                                    else if (formato[n].Contains("Lado"))
                                    {
                                        #region "Lado"
                                        linha += "1";

                                        #endregion
                                    }
                                    else if (formato[n] == "S/N")
                                    {
                                        #region "S/N"
                                        if (dt.Rows[a][field[n]].ToString().Trim().Length == 0)
                                            linha += "N";
                                        else
                                            linha += dt.Rows[a][field[n]].ToString().Trim();
                                        #endregion
                                    }
                                    else if ((formato[n].Substring(0, 1) == "P") && (formato[n].Split('.').Length > 1))
                                        linha += dt.Rows[a][field[n]].ToString().Substring(Convert.ToInt32(formato[n].Split('.')[1]), Convert.ToInt32(tamanho[n]));
                                    else if (formato[n] == "SetRota")
                                    {
                                        #region "SetRota"
                                        if (Convert.ToInt32(cliente) == 6)
                                        {
                                            if (dt.Rows[a][field[n]].ToString().Substring(0, 2) == "14")
                                            {
                                                linha += Convert.ToString("14" + dt.Rows[a][field[n]].ToString().Substring(2).PadLeft(4, '0')).PadLeft(10, '0');
                                            }
                                            else
                                            {
                                                linha += Convert.ToString(dt.Rows[a][field[n]].ToString().Substring(0, 1) + dt.Rows[a][field[n]].ToString().Substring(1).PadLeft(4, '0')).PadLeft(10, '0');
                                            }
                                        }
                                        #endregion
                                    }

                                    else
                                        linha += dt.Rows[a][field[n]].ToString();

                                    if (formato[n] == "num")
                                    {
                                        if (Convert.ToInt32(tamanho[n].Trim()) < dt.Rows[a][field[n]].ToString().Split('.')[0].Trim().Length)
                                        {
                                            if ((!formato[n].Contains("Hora")) && (!formato[n].Contains("Data")) && (!formato[n].Contains("Right")) && (formato[n].Substring(0, 1) != "P") && (!formato[n].Contains("Left")) && (!formato[n].Contains("dataClementina")))
                                            {
                                                int difff = dt.Rows[a][field[n]].ToString().Length - Convert.ToInt32(tamanho[n].Trim());
                                                if (linha.Length > difff)
                                                    linha = linha.Substring(0, (linha.Length - difff));
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (Convert.ToInt32(tamanho[n].Trim()) < dt.Rows[a][field[n]].ToString().Trim().Length)
                                        {
                                            if ((!formato[n].Contains("Hora")) && (!formato[n].Contains("Data")) && (!formato[n].Contains("Right")) && (formato[n].Substring(0, 1) != "P") && (!formato[n].Contains("Left")) && (!formato[n].Contains("dataClementina")))
                                            {
                                                int difff = dt.Rows[a][field[n]].ToString().Length - Convert.ToInt32(tamanho[n].Trim());
                                                if (linha.Length > difff)
                                                    linha = linha.Substring(0, (linha.Length - difff));
                                            }
                                        }
                                    }



                                }
                                else
                                {
                                    if (formato[n] == "N")
                                        linha += valor[n].ToString().PadLeft(Convert.ToInt32(tamanho[n].Trim()), '0');
                                    else
                                        linha += valor[n].ToString().PadRight(Convert.ToInt32(tamanho[n].Trim()), ' ');
                                }
                            }

                            if (separador.Length > 0)
                            {
                                if (Convert.ToInt32(cliente) == 1)
                                {
                                    if (tabela == "WTR_LEITURAS")
                                    {
                                        linha += separador;
                                    }
                                }
                                else
                                {
                                    linha += separador;
                                }
                            }
                        }

                        if (linha.IndexOf("#") > 0)
                        {
                            for (int s = 0; s < linha.Split('#').Length - 1; s++)
                            {
                                sw.WriteLine(linha.Split(Convert.ToChar("#"))[s] + "#");
                            }
                        }
                        else
                        {
                            sw.WriteLine(linha);
                        }

                        #region Busca Dados Parcelas

                        if (tabela == "WTR_LEITURAS")
                        {
                            qtd1++;
                            sql = "SELECT CLI_ID,EMP_ID,PARC_ANO,PARC_MES,PARC_LIGACAO,LOC_ID,PARC_IDLANC,PARC_NUMERO,PARC_VALOR,PARC_DATAPARC,PARC_MULTA,PARC_SINALNEGATIVO ";


                            sql += " FROM WTR_PARCELA";
                            sql += " WHERE LOC_ID = " + loc
                                    + " AND CLI_ID = " + cliente
                                    + " AND PARC_LIGACAO = " + dt.Rows[a][field[1]].ToString();


                            if (empresa != "0")
                                sql += " AND EMP_ID = " + empresa;
                            dtParc = db.GetDt(strCon, "WTR_PARCELA", sql);


                            
                            for (int h = 0; h < dtParc.DefaultView.Count; h++)
                            {
                                linha = "B";
                                linha += dtParc.DefaultView[h].Row["PARC_LIGACAO"].ToString().PadRight(8,' ');
                                linha += dtParc.DefaultView[h].Row["PARC_IDLANC"].ToString().PadRight(4, ' ');
                                linha += dtParc.DefaultView[h].Row["PARC_VALOR"].ToString().PadRight(15, ' ').Replace(",",".");
                                linha += dtParc.DefaultView[h].Row["PARC_DATAPARC"].ToString().PadRight(8, ' ');
                                linha += dtParc.DefaultView[h].Row["PARC_NUMERO"].ToString().PadRight(4, ' ');
                                linha += "0.00".PadRight(15, ' ');
                                sw.WriteLine(linha);

                            }

                            qtd2 += dtParc.DefaultView.Count;

                        }

                        //registro tipo B
                        
                        #endregion


                        #endregion

                        
                        
                    }


                }
               
                #endregion


                i++;
            }

            String linhaFim = "9" + "00000001" + qtd1.ToString().PadRight(8, ' ') + qtd2.ToString().PadRight(8, ' ')+ "0".ToString().PadRight(8, ' ')+ "0".ToString().PadRight(8, ' '); ;
            //String linhaFim = "9" + qtd1.ToString().PadRight(8, '0');
            sw.WriteLine(linhaFim);

            if (!sw.Equals(StreamWriter.Null))
                sw.Close();


            btDownload.Enabled = true;
          //  btDownload.Text = "Download: " + mes.ToString().Trim().PadLeft(2, '0') + ano + file.Substring(file.Length - 2, 2) + "." + ext;
            btDownload.Text = "Download: " + DateTime.Now.Date.ToShortDateString().Replace("/", "") + "_" 
                + DateTime.Now.Date.ToShortTimeString().Replace(":", "") + "RO" + loc.ToString().PadLeft(2, '0') + "." + ext;
            //Move nome arquivo para o outro list de backup




            if (!sw.Equals(StreamWriter.Null))
                sw.Close();
            #endregion


        }
        catch (Exception ex)
        {
            string resp = ("Erro :" + " - " + ex.Message);
           
        }
        finally
        {
            sw.Close();
        }

    }

    private void downloadTxt(string arquivo, string nomeArq, HttpResponse response)
    {
        try
        {

            //Response.ContentType = "application/octet-stream";
            //Response.AppendHeader("Content-Disposition", String.Format("attachment; filename={0}", nomeArq));
            //Response.TransmitFile(arquivo);
            //HttpContext.Current.ApplicationInstance.CompleteRequest();

            // Get the physical Path of the file
            string filepath = arquivo;

            // Create New instance of FileInfo class to get the properties of the file being downloaded
            FileInfo file = new FileInfo(filepath);

            // Checking if file exists
            if (file.Exists)
            {
                // Clear the content of the response
                response.ClearContent();

                // LINE1: Add the file name and attachment, which will force the open/cance/save dialog to show, to the header
                response.AddHeader("Content-Disposition", String.Format("attachment; filename={0}", file.Name));

                // Add the file size into the response header
                response.AddHeader("Content-Length", file.Length.ToString());

                // Set the ContentType
                response.ContentType = ReturnFiletype(file.Extension.ToLower());

                // Write the file into the response (TransmitFile is for ASP.NET 2.0. In ASP.NET 1.1 you have to use WriteFile instead)
                response.TransmitFile(file.FullName);

                // End the response
                response.End();

                //send statistics to the class
            }
        }
        catch (Exception ex)
        {
            // msg("Erro na download do arquivo: " + ex.Message);
        }
    }
    

    public static string ReturnFiletype(string fileExtension)
    {
        switch (fileExtension)
        {
            case ".htm":
            case ".html":
            case ".log":
                return "text/HTML";
            case ".txt":
                return "text/plain";
            case ".doc":
                return "application/ms-word";
            case ".tiff":
            case ".tif":
                return "image/tiff";
            case ".asf":
                return "video/x-ms-asf";
            case ".avi":
                return "video/avi";
            case ".zip":
                return "application/zip";
            case ".xls":
            case ".csv":
                return "application/vnd.ms-excel";
            case ".gif":
                return "image/gif";
            case ".jpg":
            case "jpeg":
                return "image/jpeg";
            case ".bmp":
                return "image/bmp";
            case ".wav":
                return "audio/wav";
            case ".mp3":
                return "audio/mpeg3";
            case ".mpg":
            case "mpeg":
                return "video/mpeg";
            case ".rtf":
                return "application/rtf";
            case ".asp":
                return "text/asp";
            case ".pdf":
                return "application/pdf";
            case ".fdf":
                return "application/vnd.fdf";
            case ".ppt":
                return "application/mspowerpoint";
            case ".dwg":
                return "image/vnd.dwg";
            case ".msg":
                return "application/msoutlook";
            case ".xml":
            case ".sdxl":
                return "application/xml";
            case ".xdp":
                return "application/vnd.adobe.xdp+xml";
            default:
                return "application/octet-stream";
        }
    }

}