﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using WaterSyncLite.Class;
using WaterSyncLite.DAL;
using WaterSyncLite.MDL;

/// <summary>
/// Summary description for RioPardo
/// </summary>
public class RioPardo
{
    public RioPardo()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    String msg = "";
    string cliente = "07";
    string empresa = "01";

    public int Import(string cam, string file, string rota, string mes, string ano, string caminhoProjeto, string leiturista, String strCon,
       TextBox txtSeparador, DropDownList cbxLeit, ListBox lsvArquivos, TextBox txtStatus)
    {
        Data db = new Data();
        #region Buscando informações no arquivo Import.xml
        //string import;
        Import import2;
        ImportDAL impDAL = new ImportDAL();
        Import import = new Import();


        int countUnder = file.Split('_').Length;

        //Tratamento de houver underline
        if (countUnder > 1)
            import = impDAL.getImp(Convert.ToInt32(Convert.ToInt32(cliente)), file.Split('_')[0].Trim());
        else
        {
            if (file.Length > 4)
                import = impDAL.getImp(Convert.ToInt32(Convert.ToInt32(cliente)), file.Remove(file.Length - 4, 4));
            else
                import = impDAL.getImp(Convert.ToInt32(Convert.ToInt32(cliente)), file);
        }
       
        #endregion

        try
        {
            string check = file;

            DataTable dtLeitura = new DataTable();
            dtLeitura.Columns.Add("Ligacao");
            dtLeitura.Columns.Add("Insert");
            dtLeitura.Columns.Add("Valores");

            DataTable dtFatura = new DataTable();
            dtFatura.Columns.Add("Ligacao");
            dtFatura.Columns.Add("Insert");
            dtFatura.Columns.Add("Values");
            dtFatura.Columns.Add("Valores");


            for (int f = 0; f < 2; f++)
            {

                if (f > 0)
                {
                    if (file.Split('_')[0] == "Fatura")
                        file = "Leitura_" + file.Split('_')[1];
                    else
                        file = "Fatura_" + file.Split('_')[1];

                }

                if ((import.Campos.Length > 1) && file.Split('_')[0] == "Leitura")
                    import = impDAL.getImp(Convert.ToInt32(Convert.ToInt32(cliente)), file.Split('_')[0].Trim());
                else
                    import = impDAL.getImp(Convert.ToInt32(Convert.ToInt32(cliente)), file.Split('_')[0].Trim());

                FileStream ofs = File.Open(cam + file, FileMode.Open, FileAccess.Read, FileShare.None);


                StreamReader sr = new StreamReader(ofs, System.Text.Encoding.GetEncoding("ISO-8859-1"));	//Aceita caracteres especiais.

                string linha = string.Empty;
                int i = 0;
                //string empresa = Request.Cookies["EmpID"].Value.ToString().Trim();
                string sql = "";
                string sqlLeitura = "";
                string Ligacao = "";
                string tabela = "";
                string insert = "";
                int p = 0;
                string[] campos = null;
                string[] field = null;
                string[] valor = null;
                string idLinha = "";
                string erro = "";

                string tipdebRef = "";
                string mesRef = DateTime.Now.Month.ToString().PadLeft(2, '0');
                string anoRef = DateTime.Now.Year.ToString().PadLeft(4, '0'); ;

                int linhaC = 0;     //CETIL - Controle de linhas de Registro C (discriminação dos serviços)


                try
                {


                    while (sr.Peek() >= 0)
                    {
                        i++;

                        ////lblCount.Refresh();
                        linha = string.Empty;
                        linha = sr.ReadLine();

                        //Retira o separador de campo - Na importação não deve ter.
                        if (txtSeparador.Text.Trim().Length < 1)
                            linha = linha.Replace("|", "").Replace(";", "");
                        //linha = linha.Replace("|"," ").Replace(";"," ");

                        linha = linha.PadRight(141, ' ');
                        linha = linha.Replace("'", " ");		//Alteração feita em 07Dez2004 - Evita erro de SQL com apóstofro - Padovani.
                        linha = linha.Replace("\r\n", " ");	//Alteração feita em 24Mar2005 - Evita erro com <Enter> - Padovani.
                        int len = linha.Length;
                        string reg = linha.Substring(0, 1);


                        #region Leitura de Conteúdo do Arquivo


                        campos = import.Campos.Split('|');
                        tabela = campos[0];
                    
                    field = new string[campos.Length - 1];
                    valor = new string[campos.Length - 1];

                    p = 0;
                    while (p < (campos.Length - 1))
                    {

                        field[p] = campos[p + 1].Split(';')[0].Trim();

                        if (txtSeparador.Text.Trim().Length > 0)
                        {
                            char separa = Convert.ToChar(txtSeparador.Text.Trim());

                            if (campos[p + 1].Split(';').Length > 3)
                            {
                                valor[p] = campos[p + 1].Split(';')[3].Trim().Replace('"', ' ');
                                valor[p] = valor[p].Trim();

                                if (valor[p].Trim() == "seq")
                                    valor[p] = i.ToString();
                                else if (valor[p].Trim() == ".leiturista")
                                    valor[p] = cbxLeit.SelectedValue.ToString();

                            }
                            else
                                valor[p] = linha.Split(separa)[p].ToString();

                            if (campos[p + 1].Split(';').Length > 2)
                            {
                                if (Convert.ToInt32(campos[p + 1].Split(';')[2].ToString().Trim()) > 0)
                                    valor[p] = valor[p].Substring(0, Convert.ToInt32(campos[p + 1].Split(';')[2].ToString().Trim()));
                            }
                        }
                        else
                        {
                            if (campos[p + 1].Split(';').Length > 3)    //Valor fixo
                            {
                                if (campos[p + 1].Split(';')[3].Trim() == "'Data'")
                                {
                                    valor[p] = linha.Substring(Convert.ToInt32(campos[p + 1].Split(';')[1].Trim()), Convert.ToInt32(campos[p + 1].Split(';')[2].Trim())).Trim();
                                    if (valor[p].ToString().Length > 0)
                                        valor[p] = valor[p].Substring(0, 2) + "/" + valor[p].Substring(2, 2) + "/" + valor[p].Substring(4, 4);
                                }
                                else if (campos[p + 1].Split(';')[3].Trim() == ".cliente")
                                    valor[p] = cliente;
                                else if (campos[p + 1].Split(';')[3].Trim() == ".empresa")
                                    valor[p] = empresa;
                                //else if (campos[p + 1].Split(';')[3].Trim() == ".cidade")
                                //    valor[p] = txtCidade.Text.Trim();
                                else if (campos[p + 1].Split(';')[3].Trim() == ".leiturista")
                                    valor[p] = cbxLeit.SelectedValue.ToString();
                                else if (campos[p + 1].Split(';')[3].Trim() == ".local")
                                    valor[p] = rota;
                                else if (campos[p + 1].Split(';')[3].Trim() == ".mes")
                                    valor[p] = mes;
                                else if (campos[p + 1].Split(';')[3].Trim() == ".ano")
                                    valor[p] = ano;
                                else if (campos[p + 1].Split(';')[3].Trim() == "'/100'")
                                {
                                    valor[p] = linha.Substring(Convert.ToInt32(campos[p + 1].Split(';')[1].Trim()), Convert.ToInt32(campos[p + 1].Split(';')[2].Trim()));
                                    valor[p] = Convert.ToString(Convert.ToDecimal(valor[p]) / 100).Replace(',', '.');
                                }
                                else if (campos[p + 1].Split(';')[3].Trim() == "'/10'")
                                {
                                    valor[p] = linha.Substring(Convert.ToInt32(campos[p + 1].Split(';')[1].Trim()), Convert.ToInt32(campos[p + 1].Split(';')[2].Trim()));
                                    valor[p] = Convert.ToString(Convert.ToDecimal(valor[p]) / 10).Replace(',', '.');
                                }
                                else if (campos[p + 1].Split(';')[3].Trim() == ".Trim")
                                {
                                    valor[p] = linha.Substring(Convert.ToInt32(campos[p + 1].Split(';')[1].Trim()), Convert.ToInt32(campos[p + 1].Split(';')[2].Trim()));
                                    valor[p] = valor[p].Replace(" ", "");
                                }
                                else if (campos[p + 1].Split(';')[3].Trim() == ".Num")
                                {
                                    valor[p] = linha.Substring(Convert.ToInt32(campos[p + 1].Split(';')[1].Trim()), Convert.ToInt32(campos[p + 1].Split(';')[2].Trim()));
                                    if (valor[p].Trim().Length < 1)
                                        valor[p] = "0";

                                    valor[p] = valor[p].Replace(".", "");   //Retirar ponto de milhar
                                    valor[p] = valor[p].Replace(",", ".");  //Substituir vírgula de centavos

                                    if (valor[p].IndexOf("-") > 0)
                                    {
                                        valor[p] = valor[p].Replace("-", "");
                                        valor[p] = "-" + valor[p];
                                    }
                                }
                                else if (campos[p + 1].Split(';')[3].Trim() == ".Txt/daea")
                                {
                                    valor[p] = linha.Substring(Convert.ToInt32(campos[p + 1].Split(';')[1].Trim()), Convert.ToInt32(campos[p + 1].Split(';')[2].Trim()));
                                    valor[p] = valor[p].Replace("LV1", "LVW").Replace("LV2", "LVY").Replace("LV3", "LVX");
                                    valor[p] = valor[p].Replace("0", "").Replace("1", "").Replace("2", "").Replace("3", "").Replace("4", "").Replace("5", "").Replace("6", "").Replace("7", "").Replace("8", "").Replace("9", "");
                                    valor[p] = valor[p].Replace("LVW", "LV1").Replace("LVY", "LV2").Replace("LVX", "LV3");
                                    valor[p] = valor[p].Trim().Replace(" ", "/");
                                }
                                else if (campos[p + 1].Split(';')[3].Trim() == ".Num/daea")
                                {
                                    valor[p] = linha.Substring(Convert.ToInt32(campos[p + 1].Split(';')[1].Trim()), Convert.ToInt32(campos[p + 1].Split(';')[2].Trim()));
                                    valor[p] = valor[p].ToUpper().Replace("RES", "").Replace("COM", "").Replace("IND", "").Replace("PUB", "").Replace("SOC", "").Replace("CIV", "").Replace("PAR", "").Replace("LV1", "").Replace("LV2", "").Replace("LV3", "").Replace("NES", "");
                                    valor[p] = valor[p].Trim().Replace(" ", "/");
                                }
                                else if (campos[p + 1].Split(';')[3].Trim() == ".stsDAEA")
                                {
                                    valor[p] = linha.Substring(Convert.ToInt32(campos[p + 1].Split(';')[1].Trim()), Convert.ToInt32(campos[p + 1].Split(';')[2].Trim()));
                                    valor[p] = valor[p].ToUpper().Replace("T", "1").Replace("F", "2").Replace("D", "3").Replace("E", "4");
                                }
                                else if (campos[p + 1].Split(';')[3].Trim() == ".N/0,S/1")
                                {
                                    valor[p] = linha.Substring(Convert.ToInt32(campos[p + 1].Split(';')[1].Trim()), Convert.ToInt32(campos[p + 1].Split(';')[2].Trim()));
                                    valor[p] = valor[p].ToUpper().Replace("S", "1").Replace("N", "0");
                                }
                                else if ((campos[p + 1].Split(';')[3].Trim().Length > 5) && (campos[p + 1].Split(';')[3].Trim().Substring(0, 6) == ".Split"))
                                {
                                    valor[p] = linha.Substring(Convert.ToInt32(campos[p + 1].Split(';')[1].Trim()), Convert.ToInt32(campos[p + 1].Split(';')[2].Trim()));
                                    if ((Convert.ToInt32(cliente) == 6) && (field[p].ToUpper() == "FAT_MES"))
                                    {
                                        valor[p] = valor[p].ToUpper();
                                        valor[p] = valor[p].Replace("JANEIRO", "01").Replace("FEVEREIRO", "02").Replace("MARÇO", "03").Replace("ABRIL", "04").Replace("MAIO", "05").Replace("JUNHO", "06").Replace("JULHO", "07").Replace("AGOSTO", "08").Replace("SETEMBRO", "09").Replace("OUTUBRO", "10").Replace("NOVEMBRO", "11").Replace("DEZEMBRO", "12").Replace("MARCO", "03");
                                    }

                                    char separ = Convert.ToChar(campos[p + 1].Split(';')[3].Trim().Substring(8, 1));
                                    int posic = Convert.ToInt32(campos[p + 1].Split(';')[3].Trim().Substring(12, 1));
                                    valor[p] = valor[p].Split(separ)[posic].Trim();
                                }
                                else if (campos[p + 1].Split(';')[3].Trim() == ".seq")
                                    valor[p] = i.ToString();
                                else
                                    valor[p] = campos[p + 1].Split(';')[3].Replace('"', ' ').Trim();
                            }
                            else
                                valor[p] = linha.Substring(Convert.ToInt32(campos[p + 1].Split(';')[1].Trim()), Convert.ToInt32(campos[p + 1].Split(';')[2].Trim()));
                        }



                        p++;
                    }

                    #endregion


                    #region Comando SQL para o BANCO DE DADOS LEITURA

                    if (file.Split('_')[0] == "Leitura")
                    {
                        DataRow Leitura = dtLeitura.NewRow();
                        Leitura["Ligacao"] = linha.Substring(218, 10).Trim();
                        sql = "";
                        sqlLeitura = "";
                        p = 0;
                        sql = ",";
                        while (p < field.Length)
                        {
                            if (p > 0)
                            {
                                if (field[p - 1].Trim() != field[p].Trim())
                                    sql += field[p].Trim() + ",";
                            }
                            else
                                sql += field[p].Trim() + ",";
                            p++;
                        }

                        sql = sql.Replace("A#", "");
                        sql = sql.Replace("B#", "");
                        sql = sql.Replace("C#", "");
                        sql = sql.Replace("D#", "");
                        sql = sql.Replace("E#", "");

                        sql = sql.Remove(sql.Length - 1, 1);
                        Leitura["Insert"] = sql;
                        p = 0;
                        sqlLeitura = ",'";
                        while (p < valor.Length)
                        {
                            if (valor[p] != null)
                            {
                                if (p > 0)
                                {
                                    if (field[p - 1].Trim() == field[p].Trim())
                                        sqlLeitura = sql.Remove(sql.Length - 3, 3);

                                    sqlLeitura += valor[p].Trim() + "','";
                                }
                                else
                                {
                                    sqlLeitura += valor[p].Trim() + "','";
                                }
                            }
                            else
                                sqlLeitura += "','";
                            p++;
                        }

                        sqlLeitura = sqlLeitura.Remove(sqlLeitura.Length - 2, 2);
                        sqlLeitura += ")";
                        Leitura["Valores"] = sqlLeitura;
                        dtLeitura.Rows.Add(Leitura);

                    }

                    #endregion

                    #region Comando SQL para o BANCO DE DADOS FATURA

                    if (file.Split('_')[0] == "Fatura")
                    {
                        DataRow Fatura = dtFatura.NewRow();
                        sql = "";
                        sqlLeitura = "";
                        sql = "INSERT INTO " + tabela + "(";
                        p = 0;

                        while (p < field.Length)
                        {
                            if (p > 0)
                            {
                                if (field[p - 1].Trim() != field[p].Trim())
                                    sql += field[p].Trim() + ",";
                            }
                            else
                                sql += field[p].Trim() + ",";
                            p++;
                        }

                        sql = sql.Replace("A#", "");
                        sql = sql.Replace("B#", "");
                        sql = sql.Replace("C#", "");
                        sql = sql.Replace("D#", "");
                        sql = sql.Replace("E#", "");

                        sql = sql.Remove(sql.Length - 1, 1);
                        Fatura["Insert"] = sql;
                        Fatura["Values"] = " ) VALUES ('";

                        p = 0;

                        while (p < valor.Length)
                        {
                            if (valor[p] != null)
                            {
                                if (p > 0)
                                {
                                    if (field[p - 1].Trim() == field[p].Trim())
                                        sqlLeitura = sql.Remove(sql.Length - 3, 3);

                                    sqlLeitura += valor[p].Trim() + "','";
                                }
                                else
                                {
                                    sqlLeitura += valor[p].Trim() + "','";
                                }
                            }
                            else
                                sql += "','";
                            p++;
                        }
                        sqlLeitura = sqlLeitura.Remove(sqlLeitura.Length - 2, 2);
                        //sqlLeitura += ")";
                        Fatura["Valores"] = sqlLeitura;
                        Fatura["Ligacao"] = valor[2].ToString().Trim();
                        dtFatura.Rows.Add(Fatura);
                    }

                    // erro = db.Ins(strCon, sql);

                    if (erro.Trim().Length > 0)
                    {
                        if ((Convert.ToInt32(cliente) != 11) || ((Convert.ToInt32(cliente) == 11) && (tabela != "WTR_FAIXA")))
                        {
                            msg += "\r\n" + "Arquivo: " + file.Trim() + " - Registro: " + i + " - Erro: " + erro;
                            //txtStatus.Refresh();
                        }
                    }
                    #endregion

                }
                    if (f > 0)
                    {
                        string tipo = "U";
                        //if (chkRural.Checked)
                        //    tipo = "R";

                        if ((Convert.ToInt32(cliente) == 6) && (file.Substring(0, 1) != "A"))
                            return 0;

                        if ((Convert.ToInt32(cliente) == 7) && (file.Substring(0, 6).ToUpper() == "FATURA"))
                            return 0;

                        #region Geração do LIVRO.


                        if (Convert.ToInt32(cliente) == 1)//Se o campo local não estiver preenchido, preenche automaticamente (POMPÉIA)
                        {
                            if ((rota.Length == 0) && (tabela == "WTR_LEITURAS"))
                            {
                                p = 0;
                                while ((field[p].Trim().ToUpper() != "LOC_ID") && (p < field.Length))
                                    p++;
                                rota = valor[p];
                            }
                            if (tabela == "WTR_FATURA")
                                return 0;
                        }


                        if (f > 0)
                        {
                            foreach (DataRow row2 in dtLeitura.Rows)
                            {
                                foreach (DataRow row in dtFatura.Rows)
                                {
                                    if (row["Ligacao"].ToString() == row2["Ligacao"].ToString())
                                    {
                                        insert = row["Insert"].ToString() + row2["Insert"].ToString() + row["Values"].ToString() + row["Valores"].ToString() + row2["Valores"].ToString();
                                        erro = db.Ins(strCon, insert);
                                    }
                                }
                            }

                            string sql2 = "INSERT WTR_LIVROS VALUES('"
                                + cliente + "','"
                                + empresa + "','"
                                + rota + "','"
                                + mes + "','"
                                + ano + "','"
                                + empresa + "','"
                                + rota + "','"
                                + "','"
                                + DateTime.Now.Day.ToString() + "/" + mes + "/" + ano + "','"
                                + i + "','"
                                + "0','"
                                + i + "','"
                                + "0','"
                                + DateTime.Now.ToShortDateString() + "','"
                                + cbxLeit.SelectedValue.ToString() + "','"
                                + "0','"
                                + "0','"
                                + tipo + "','"
                                + "0','"
                                + "N')";
                            string erro2 = db.Ins(strCon, sql2);
                            //fileFTP += sql2 + "\r\n";                           

                        }
                        #endregion
                    }

                }
                catch (Exception ex)
                {
                    msg += (i + " #" + linha + " - " + ex.Message) + "\n";

                }

                finally
                {
                    sr.Close();
                    ofs.Close();
                    if (f > 0)
                    {

                        string fileLeitura = lsvArquivos.SelectedItem.ToString().Replace("Fatura", "Leitura");//Import arquivo leitura Pompeia Douglas 08/07/2016

                        if (file.Split('_')[0] == "Leitura")
                            file = file.Replace("Leitura", "Fatura");

                        string path = caminhoProjeto;
                        string localBkp = caminhoProjeto.Substring(0, caminhoProjeto.Length - 3) + @"\BKP\" + cliente.PadLeft(2, '0');
                        string localFatura = caminhoProjeto.Substring(0, caminhoProjeto.Length - 3) + @"\BKP\" + cliente.PadLeft(2, '0') + @"\" +file;
                        if(File.Exists(localFatura))
                        {
                            File.Delete(localFatura);
                        }

                        File.Move(path + @"\" + file, localBkp.Trim() + @"\" + file);

                        localBkp = caminhoProjeto.Substring(0, caminhoProjeto.Length - 3) + @"\BKP\" + cliente.PadLeft(2, '0');
                        string localLeitura = caminhoProjeto.Substring(0, caminhoProjeto.Length - 3) + @"\BKP\" + cliente.PadLeft(2, '0') + @"\" + fileLeitura;
                        if (File.Exists(localLeitura))
                        {
                            File.Delete(localLeitura);
                        }
                        File.Move(path + @"\" + fileLeitura, localBkp.Trim() + @"\" + fileLeitura);
                        lsvArquivos.Items.Remove(file);
                        lsvArquivos.Items.Remove(fileLeitura);
                        msg += "Arquivo: " + file.Trim() + " carregado com sucesso !\n";
                        msg += ("Arquivo: " + fileLeitura.Trim() + " carregado com sucesso !\n");
                    }

                }
            }

            txtStatus.Text = msg;

            return 0;

        }
        catch (Exception ex)
        {
            msg += (ex.Message);
            txtStatus.Text = msg;
            return -1;
        }

    }

    public void AnaliseCritica(String strCon, TextBox txtLivro, DropDownList cbxMsg, DropDownList cbxMes, TextBox txtAno, TextBox txtEndereco, TextBox txtNumero,
       CheckBox chkCritica, TextBox txtPercent, Label lblMsg, DropDownList ddlSituacaoLeitura, DropDownList ddlLeituristas, TextBox txtIdentificacao,
       TextBox txtMedidor, RadioButton rbEndereço, RadioButton rbLigacao, RadioButton rbSequencia, RadioButton rbNomeContribuinte, RadioButton rbRota, GridView gridView, Panel panelMsg, Panel PanelQtdRegistros, Label lblTOTAL)
    {
        DataTable dt = new DataTable();

        try
        {


            double porc = 0;
            string pMax = "";
            string pMin = "";

            #region Porcentagem
            if (txtPercent.Text.Trim().Length < 1)
                porc = 1;
            else
            {
                try
                {
                    porc = Convert.ToInt32(txtPercent.Text.Trim());
                    pMax = Convert.ToDecimal(1 + (porc / 100)).ToString().Replace(",", ".");
                    pMin = Convert.ToDecimal(1 - (porc / 100)).ToString().Replace(",", ".");
                }
                catch (Exception ee)
                {
                    lblMsg.Text = "Informe o percentual correto da crítica";
                }
            }
            #endregion

            string sql = "select LIV_ANO AS ANO, LIV_MES AS MES,CAST(lei_ligacao AS VARCHAR) AS LIGACAO,LEI_CONSUMIDOR AS CONSUMIDOR," +
                         "WTR_LEITURAS.LOC_ID AS ROTA, LEI_ENDERECO AS ENDEREÇO, LEI_NUMERO AS NUMERO, LEI_DATA AS DATA," +
                         "LTR_NOME AS LEITURISTA,  MSG_DESCRICAO AS OCORRENCIA, LEI_MEDIA AS MEDIA, LEI_ANTERIOR AS 'LEIT. ANT'," +
                         "LEI_LEITURA AS 'LEIT. ATUAL',  LEI_NUMMED AS MEDIDOR,  LEI_CONSUMO AS CONSUMO,LEI_FORMAENTREGA as ENTREGA," +
                         "LEI_LATITUDE,LEI_LONGITUDE ";


            sql += " FROM WTR_LEITURAS INNER JOIN WTR_MENSAGENS ON(WTR_LEITURAS.MSG_ID = WTR_MENSAGENS.MSG_ID and WTR_LEITURAS.cli_id = WTR_MENSAGENS." +
            "cli_id and WTR_MENSAGENS.emp_id = WTR_LEITURAS.emp_id) INNER JOIN WTR_LEITURISTAS ON (WTR_LEITURAS.LTR_ID = WTR_LEITURISTAS.LTR_ID " +
            "and WTR_LEITURAS.cli_id = WTR_LEITURISTAS.cli_id and WTR_LEITURISTAS.emp_id = WTR_LEITURAS.emp_id) ";

            sql += "WHERE WTR_LEITURAS.emp_id =" +
                          empresa + " and WTR_LEITURAS.cid_id =" + empresa +
                          " and WTR_LEITURAS.CLI_ID = " + cliente;

            #region SQL E FILTROS

            if (txtLivro.Text.Trim().Length > 0)
            {
                sql += " AND WTR_LEITURAS.LOC_ID = '" + txtLivro.Text.Trim() + "' ";
            }

            if (cbxMsg.SelectedValue != "1000")
                sql += " AND WTR_LEITURAS.MSG_ID = '" + cbxMsg.SelectedValue.ToString() + "' ";

            if (cbxMes.SelectedIndex > 0)
                sql += " AND LIV_MES = '" + cbxMes.SelectedIndex.ToString() + "' ";

            if (txtAno.Text.Trim().Length > 0)
                sql += " AND LIV_ANO = '" + txtAno.Text.Trim() + "' ";

            if (txtEndereco.Text.Trim().Length > 0)
                sql += " AND (replace (LEI_ENDERECO,'É','E') LIKE '" + txtEndereco.Text + "' or LEI_ENDERECOENTREGA LIKE '" + txtEndereco.Text + "') ";

            if (txtNumero.Text.Trim().Length > 0)
                sql += " AND (LEI_NUMERO = " + txtNumero.Text + " or LEI_NUMEROENTREGA LIKE '" + txtNumero.Text + "') ";

            #region Somente critica
            if (chkCritica.Checked)
            {
                sql += " AND (((LEI_LEITURA - LEI_ANTERIOR) > (LEI_MEDIA * " + pMax + ")) OR ((LEI_LEITURA - LEI_ANTERIOR) < (LEI_MEDIA * " + pMin + ")))";
            }
            #endregion

            #region Sem Leitura
            if (ddlSituacaoLeitura.SelectedValue.ToString() == "Com Leitura")
            {
                sql += " AND LEI_LEITURA <> ''";
            }
            else
            {
                if (ddlSituacaoLeitura.SelectedValue.ToString() == "Sem Leitura")
                {
                    sql += " AND (LEI_LEITURA = '' or lei_leitura is null)";
                }
            }
            #endregion
            #region SITUACAO LEITURA
            if (ddlSituacaoLeitura.SelectedValue.ToString() == "Alteração cadastral")
                sql += " AND LEI_NOVACATEGORIA > 0";

            #endregion


            #region Leituristas
            if (ddlLeituristas.SelectedValue.ToString() != "0")
            {
                sql += " AND WTR_LEITURAS.LTR_ID  =  " + ddlLeituristas.SelectedValue;
            }
            #endregion

            #region Identificação e Medidor
            if ((txtIdentificacao.Text.Trim().Length > 0) || (txtMedidor.Text.Trim().Length > 0))
            {
                if (txtIdentificacao.Text.Trim().Length > 0)
                    sql += " AND LEI_LIGACAO LIKE '%" + txtIdentificacao.Text.Trim() + "%' ";

                if (txtMedidor.Text.Trim().Length > 0)
                    sql += " AND replace(LEI_NUMMED,'.','') LIKE '%" + txtMedidor.Text.Trim() + "%' ";
            }

            #endregion

            #endregion

            if (rbEndereço.Checked)
                sql += " ORDER BY LEI_ENDERECO,LEI_NUMERO,LIV_ANO, LIV_MES, WTR_LEITURAS.LOC_ID,LEI_SEQ";
            else if (rbLigacao.Checked)
                sql += " ORDER BY lei_ligacao,LIV_ANO, LIV_MES, WTR_LEITURAS.LOC_ID";
            else if (rbSequencia.Checked)
                sql += " ORDER BY lei_seq,LIV_ANO, LIV_MES, WTR_LEITURAS.LOC_ID";
            else if (rbNomeContribuinte.Checked)
                sql += " ORDER BY lei_consumidor,LIV_ANO, LIV_MES, WTR_LEITURAS.LOC_ID";
            else if (rbRota.Checked)
                sql += " ORDER BY WTR_LEITURAS.LOC_ID,LIV_ANO, LIV_MES,lei_seq";
            else
            {

                sql += " ORDER BY LIV_ANO, LIV_MES, lei_seq";
            }

            Data db = new Data();

            dt = db.GetDt(strCon, "WTR_LEITURAS", sql);

            if (dt.DefaultView.Count > 0)
            {
                lblTOTAL.Text = string.Format("{0:C}", Convert.ToDecimal(dt.DefaultView.Count.ToString())).Replace("R$", "").Replace(" ", "").Split(',')[0];
                PanelQtdRegistros.Visible = true;
            }
            else PanelQtdRegistros.Visible = false;

            gridView.DataSource = dt;
            gridView.DataBind();


        }
        catch (Exception ex)
        {
            lblMsg.Text = "Erro - " + ex.Message;
            panelMsg.Visible = true;

        }

    }

    private void downloadTxt(string arquivo, string nomeArq, HttpResponse response)
    {
        try
        {

            //Response.ContentType = "application/octet-stream";
            //Response.AppendHeader("Content-Disposition", String.Format("attachment; filename={0}", nomeArq));
            //Response.TransmitFile(arquivo);
            //HttpContext.Current.ApplicationInstance.CompleteRequest();

            // Get the physical Path of the file
            string filepath = arquivo;

            // Create New instance of FileInfo class to get the properties of the file being downloaded
            FileInfo file = new FileInfo(filepath);

            // Checking if file exists
            if (file.Exists)
            {
                // Clear the content of the response
                response.ClearContent();

                // LINE1: Add the file name and attachment, which will force the open/cance/save dialog to show, to the header
                response.AddHeader("Content-Disposition", String.Format("attachment; filename={0}", file.Name));

                // Add the file size into the response header
                response.AddHeader("Content-Length", file.Length.ToString());

                // Set the ContentType
                response.ContentType = ReturnFiletype(file.Extension.ToLower());

                // Write the file into the response (TransmitFile is for ASP.NET 2.0. In ASP.NET 1.1 you have to use WriteFile instead)
                response.TransmitFile(file.FullName);

                // End the response
                response.End();

                //send statistics to the class
            }
        }
        catch (Exception ex)
        {
            // msg("Erro na download do arquivo: " + ex.Message);
        }
    }

    public static string ReturnFiletype(string fileExtension)
    {
        switch (fileExtension)
        {
            case ".htm":
            case ".html":
            case ".log":
                return "text/HTML";
            case ".txt":
                return "text/plain";
            case ".doc":
                return "application/ms-word";
            case ".tiff":
            case ".tif":
                return "image/tiff";
            case ".asf":
                return "video/x-ms-asf";
            case ".avi":
                return "video/avi";
            case ".zip":
                return "application/zip";
            case ".xls":
            case ".csv":
                return "application/vnd.ms-excel";
            case ".gif":
                return "image/gif";
            case ".jpg":
            case "jpeg":
                return "image/jpeg";
            case ".bmp":
                return "image/bmp";
            case ".wav":
                return "audio/wav";
            case ".mp3":
                return "audio/mpeg3";
            case ".mpg":
            case "mpeg":
                return "video/mpeg";
            case ".rtf":
                return "application/rtf";
            case ".asp":
                return "text/asp";
            case ".pdf":
                return "application/pdf";
            case ".fdf":
                return "application/vnd.fdf";
            case ".ppt":
                return "application/mspowerpoint";
            case ".dwg":
                return "image/vnd.dwg";
            case ".msg":
                return "application/msoutlook";
            case ".xml":
            case ".sdxl":
                return "application/xml";
            case ".xdp":
                return "application/vnd.adobe.xdp+xml";
            default:
                return "application/octet-stream";
        }
    }

    public void Export(string cam, string file, string cliente, string caminhoProjeto, String strCon, HttpResponse response, int mes, string ano, Button btDownload)
    {
        DataTable dt;
        DataTable dt2 = null;
        string ext = "";
        Data db = new Data();
        string separador = xml.GetConfigValue(caminhoProjeto, "LOCAL", "EXPSEPARADOR", Convert.ToInt32(cliente).ToString());
        if (Convert.ToInt32(cliente) == 3)
            ext = "s";
        else if ((Convert.ToInt32(cliente) == 4) || (Convert.ToInt32(cliente) == 10))
            ext = "RET";
        else if ((Convert.ToInt32(cliente) == 5))
            ext = "SIM";
        else
            ext = "txt";
        string sql = "";
        string sql2 = "";//Busca Qtd Ocorrências

        int p = 0;
        int i = 0;
        string fileNew = "";
        Export export = new Export();
        ExportDAL expDAL = new ExportDAL();
        StreamWriter sw = StreamWriter.Null;
        int qtd1 = 0, qtd2 = 0, qtd3 = 0;

        try
        {


            #region Buscando informações no arquivo Import.xml

            //Verificar quais tabelas a serem exportadas (Config.xml : <TABEXPORT>)

            string[] tabExport = xml.GetConfigValue(caminhoProjeto, "LOCAL", "TABEXPORT", Convert.ToInt32(cliente).ToString()).Split(',');

            while (i < tabExport.Length)
            {
                export = expDAL.getExp(Convert.ToInt32(cliente), tabExport[i].Trim());

                #region Leitura de Conteúdo do Arquivo de Exportação
                string[] campos = export.Campos.Split('|');
                string tabela = campos[0];
                string[] field = new string[campos.Length - 1];
                string[] tamanho = new string[campos.Length - 1];
                string[] formato = new string[campos.Length - 1];
                string[] valor = new string[campos.Length - 1];

                p = 0;
                while (p < (campos.Length - 1))
                {
                    field[p] = campos[p + 1].Split(';')[0].Trim();
                    formato[p] = campos[p + 1].Split(';')[1].Trim();
                    tamanho[p] = campos[p + 1].Split(';')[2].Trim();

                    if (campos[p + 1].Split(';').Length > 3)	//Valor fixo
                    {
                        valor[p] = campos[p + 1].Split(';')[3].Replace('"', ' ').Trim();
                    }

                    p++;
                }
                #endregion

                mes = Convert.ToInt32(file.Substring(0, 3));
                int cid = Convert.ToInt32(file.Substring(3, 3));
                int loc = Convert.ToInt32(file.Substring(6, 3));

                #region Comando SQL para o BANCO DE DADOS
                sql = "SELECT ";

                p = 0;
                while (p < field.Length)
                {
                    sql += field[p].Trim() + ",";
                    p++;
                }

                sql = sql.Remove(sql.Length - 1, 1);
                sql += " FROM " + tabela;

                if ((tabela == "WTR_LIVROS") || (tabela == "WTR_LEITURAS"))
                {
                    sql += " WHERE LIV_MES = " + mes
                        + " AND LIV_ANO = " + ano
                        + " AND CID_ID = " + "1"
                        + " AND LOC_ID = " + loc
                        + " AND CLI_ID = " + Convert.ToInt32(cliente);
                    if (empresa != "0")
                        sql += " AND EMP_ID = " + empresa;
                    if (cliente == "14")
                        sql += " AND LEI_DATA != ''";

                    #region Verifica Qtd Ocorrências
                    if (Convert.ToInt32(cliente) == 5 || Convert.ToInt32(cliente) == 14 || Convert.ToInt32(cliente) == 7)
                    {
                        if ((tabela == "WTR_LEITURAS"))//&&(Convert.ToInt32(cliente) == 5)
                        {
                            sql2 = "SELECT LTR_ID, MSG_ID, COUNT(*) AS QTD FROM WTR_LEITURAS WHERE LIV_MES = '" + mes + "'"
                            + " AND CID_ID = '" + cid + "'"
                            + " AND LIV_ANO = " + ano
                            + " AND LOC_ID = '" + loc + "'"
                            + " AND CLI_ID = '" + Convert.ToInt32(cliente) + "'"
                            + " GROUP BY LTR_ID, MSG_ID "
                            + " ORDER BY MSG_ID ";

                            dt2 = db.GetDt(strCon, tabela, sql2);
                        }
                    }
                    if (Convert.ToInt32(cliente) == 2)
                    {
                        if ((tabela == "WTR_LEITURAS"))//&&(Convert.ToInt32(cliente) == 5)
                        {
                            sql2 = "SELECT LTR_ID, MSG_ID, COUNT(*) AS QTD FROM WTR_LEITURAS WHERE LIV_MES = '" + mes + "'"
                            + " AND CID_ID = '" + cid + "'"
                            + " AND LIV_ANO = " + ano
                            + " AND LOC_ID = '" + loc + "'"
                            + " AND CLI_ID = '" + Convert.ToInt32(cliente) + "'"
                            + " GROUP BY LTR_ID, MSG_ID "
                            + " ORDER BY MSG_ID ";

                            dt2 = db.GetDt(strCon, tabela, sql2);
                        }
                    }
                    #endregion
                }
                else if (tabela.IndexOf("WTR_FATURA") >= 0)
                {
                    sql += " WHERE WTR_FATURA.FAT_MES = " + mes.ToString().PadLeft(2, '0')
                        + " AND WTR_FATURA.FAT_ANO = '" + ano.PadLeft(2, '0') + "'"
                       + " AND WTR_FATURA.CLI_ID = " + Convert.ToInt32(cliente)
                       + " AND WTR_FATURA.FAT_PASTA = " + loc.ToString();
                    if (empresa != "0")
                        sql += " AND WTR_FATURA.EMP_ID = " + empresa;

                    if (Convert.ToInt32(cliente) != 5)
                    {
                        sql2 = "SELECT LTR_ID, MSG_ID, COUNT(*) AS QTD FROM WTR_FATURA WHERE FAT_MES = " + mes
                        + " AND FAT_PASTA = '" + loc + "'"
                        + " AND FAT_ANO = " + ano + ""
                        + " AND CLI_ID = '" + Convert.ToInt32(cliente) + "'"
                        + " GROUP BY LTR_ID, MSG_ID "
                        + " ORDER BY MSG_ID ";

                        dt2 = db.GetDt(strCon, tabela, sql2);
                    }
                    else
                    {
                        sql2 = "SELECT LTR_ID, MSG_ID, COUNT(*) AS QTD FROM WTR_FATURA WHERE FAT_MES = " + mes
                        + " AND FAT_PASTA = '" + loc + "'"
                        + " AND FAT_ANO = " + ano + ""
                        + " AND CLI_ID = '" + Convert.ToInt32(cliente) + "'"
                        + " GROUP BY LTR_ID, MSG_ID "
                        + " ORDER BY MSG_ID ";

                        dt2 = db.GetDt(strCon, tabela, sql2);
                    }
                }
                else if (tabela == "WTR_SERVICOS")
                {
                    sql += " WHERE SRV_MES = " + mes
                        + " AND SRV_LOCAL = " + loc
                        + " AND CLI_ID = " + Convert.ToInt32(cliente);
                    if (empresa != "0")
                        sql += " AND EMP_ID = " + empresa;
                }
                else if (tabela == "WTR_TARIFA" && Convert.ToInt32(cliente) == 5)
                {
                    sql += " WHERE TXA_MES = " + mes
                        + " AND LOC_ID = " + loc
                        + " AND TXA_ANO = " + ano
                        + " AND CLI_ID = " + Convert.ToInt32(cliente);
                    if (empresa != "0")
                        sql += " AND EMP_ID = " + empresa;
                }
                else if (tabela == "WTR_REGISTROLOG")
                {
                    sql += " WHERE LOG_MES = " + mes
                        + " AND LOG_LOCAL = " + loc
                        + " AND CLI_ID = " + Convert.ToInt32(cliente);
                    if (empresa != "0")
                        sql += " AND EMP_ID = " + empresa;
                }



                dt = db.GetDt(strCon, tabExport[i].Trim(), sql);

                if (dt.Rows.Count > 0)
                {
                    msg += ("Exportando dados : " + tabExport[i].Trim().ToUpper());

                    if (i == 0)
                        qtd1 = dt.Rows.Count;
                    else if (i == 1) qtd2 = dt.Rows.Count;
                    else if (i == 2) qtd3 = dt.Rows.Count;

                    #region MESMO ARQUIVO DE EXPORTAÇÃO PARA DUAS OU MAIS TABELAS
                    if (fileNew.Length == 0)
                    {


                        if (Convert.ToInt32(cliente) == 14) //Clementina
                        {
                            sw = new StreamWriter(cam + @"\" + mes.ToString().Trim().PadLeft(2, '0') + ano + file.Substring(file.Length - 2, 2) + "." + ext);
                        }

                        if (Convert.ToInt32(cliente) == 7) //Rio Pardo
                        {
                            sw = new StreamWriter(cam + @"\" + mes.ToString().Trim().PadLeft(2, '0')  + file.Substring(file.Length - 2, 2).PadLeft(4,'0') + "." + ext);
                        }
                    }
                    else
                    {
                        if (sw.Equals(StreamWriter.Null))
                        {
                            if (fileNew.Length > 0)
                                sw = new StreamWriter(cam + @"\" + fileNew.Trim() + "." + ext);

                            else
                                sw = new StreamWriter(cam + @"\" + tabExport[i].Trim() + "_" + file + "." + ext);
                        }
                    }
                    #endregion


                    for (int a = 0; a < dt.Rows.Count; a++)
                    {
                        //lblCount.Text = a.ToString();
                        //lblCount.Refresh();
                        string linha = string.Empty;

                        #region Leitura de Campos na Tabela
                        for (int n = 0; n < campos.Length - 1; n++)
                        {
                            if (Convert.ToInt32(tamanho[n].Trim()) > 0)
                            {
                                if (valor[n] == null)
                                {
                                    if (formato[n] == "N")
                                    {
                                        #region "N"
                                        decimal vlr = 0;
                                        if (dt.Rows[a][field[n]].ToString().Trim().Length > 0)
                                            vlr = Convert.ToDecimal(dt.Rows[a][field[n]].ToString().Replace('.', ','));
                                        if (vlr >= 0)
                                            linha += vlr.ToString().PadLeft(Convert.ToInt32(tamanho[n].Trim()), '0');
                                        else
                                        {
                                            vlr = vlr * (-1);
                                            linha += "-" + vlr.ToString().PadLeft(Convert.ToInt32(tamanho[n].Trim()) - 1, '0');
                                        }
                                        #endregion
                                    }
                                    else if (formato[n] == "T") { 
                                    int tam = Convert.ToInt32(tamanho[n].Trim());
                                        linha += dt.Rows[a][field[n]].ToString().PadRight(tam, ' ');
                                    }

                                else if (formato[n] == "DecimalClementina")
                                {
                                    #region "Decimal"
                                    decimal vlr = 0;
                                    if (dt.Rows[a][field[n]].ToString().Trim().Length > 0)
                                        vlr = Convert.ToDecimal(dt.Rows[a][field[n]].ToString().Replace('.', ','));
                                    if (vlr >= 0)
                                        if (vlr > 0)
                                            linha += (vlr.ToString().Split(',')[0] + "," + vlr.ToString().Split(',')[1].PadRight(2, '0')).PadLeft(Convert.ToInt32(tamanho[n].Trim()), '0').Replace(',', '.');
                                        else
                                            linha += vlr.ToString("0.00").PadLeft(Convert.ToInt32(tamanho[n].Trim()), '0').Replace(',', '.');

                                    else
                                    {
                                        vlr = vlr * (-1);
                                        linha += "-" + vlr.ToString().PadLeft(Convert.ToInt32(tamanho[n].Trim()) - 1, '0').Replace(',', '.');
                                    }
                                    #endregion

                                    /* #region "Decimal"
                                    decimal vlr = 0;
                                    if (dt.Rows[a][field[n]].ToString().Trim().Length > 0)
                                        vlr = Convert.ToDecimal(dt.Rows[a][field[n]].ToString().Replace('.', ','));
                                    if (vlr >= 0)
                                        linha += vlr.ToString().PadLeft(Convert.ToInt32(tamanho[n].Trim()), '0').Replace(',', '.');
                                    else
                                    {
                                        vlr = vlr * (-1);
                                        linha += "-" + vlr.ToString().PadLeft(Convert.ToInt32(tamanho[n].Trim()) - 1, '0').Replace(',', '.');
                                    }
                                    #endregion*/
                                }
                                else if (formato[n] == "Decimal")
                                {
                                    #region "Decimal"
                                    decimal vlr = 0;
                                    if (dt.Rows[a][field[n]].ToString().Trim().Length > 0)
                                        vlr = Convert.ToDecimal(dt.Rows[a][field[n]].ToString().Replace('.', ','));
                                    if (vlr >= 0)
                                        linha += vlr.ToString().PadLeft(Convert.ToInt32(tamanho[n].Trim()), '0').Replace(',', '.');
                                    else
                                    {
                                        vlr = vlr * (-1);
                                        linha += "-" + vlr.ToString().PadLeft(Convert.ToInt32(tamanho[n].Trim()) - 1, '0').Replace(',', '.');
                                    }
                                    #endregion
                                }
                                else if (formato[n] == "Versao")
                                {
                                    #region "Versao"
                                    linha += "4.9".ToString().PadRight(Convert.ToInt32(tamanho[n].Trim()), ' ');


                                    #endregion
                                }
                                else if (formato[n] == "Null")
                                {
                                    #region "Null"
                                    if (dt.Rows[a][field[n]].ToString().Trim() != "")
                                    {
                                        if (Convert.ToInt32(dt.Rows[a][field[n]].ToString().Trim()) == 0)
                                            linha += " ".PadRight(Convert.ToInt32(tamanho[n].Trim()), ' ');
                                        else
                                        {
                                            decimal vlr = 0;
                                            if (dt.Rows[a][field[n]].ToString().Trim().Length > 0)
                                                vlr = Convert.ToDecimal(dt.Rows[a][field[n]].ToString().Replace('.', ','));
                                            if (vlr >= 0)
                                                linha += vlr.ToString().PadLeft(Convert.ToInt32(tamanho[n].Trim()), '0');
                                            else
                                            {
                                                vlr = vlr * (-1);
                                                linha += "-" + vlr.ToString().PadLeft(Convert.ToInt32(tamanho[n].Trim()) - 1, '0');
                                            }
                                        }
                                    }
                                    else
                                        linha += " ".PadRight(Convert.ToInt32(tamanho[n].Trim()), ' ');


                                    #endregion
                                }
                                else if (formato[n] == "num")
                                {
                                    #region "num"
                                    decimal vlr = 0;
                                    if (dt.Rows[a][field[n]].ToString().Contains("."))
                                        vlr =
                                            Convert.ToInt32(
                                                dt.Rows[a][field[n]].ToString().Split('.')[0].ToString());
                                    else
                                    {
                                        if (dt.Rows[a][field[n]].ToString().Trim().Length > 0)
                                            vlr = Convert.ToInt32(dt.Rows[a][field[n]].ToString());
                                    }

                                    if (vlr >= 0)
                                        linha += vlr.ToString().PadLeft(Convert.ToInt32(tamanho[n].Trim()), '0').Replace(',', '.');
                                    else
                                    {
                                        vlr = vlr * (-1);
                                        linha += "-" + vlr.ToString().PadLeft(Convert.ToInt32(tamanho[n].Trim()) - 1, '0').Replace(',', '.');
                                    }
                                    #endregion
                                }
                                else if (formato[n] == "D")
                                {
                                    #region "D"
                                    if (Convert.ToString(dt.Rows[a][field[n]].ToString().Replace("/", "").PadRight(Convert.ToInt32(tamanho[n].Trim()), ' ')).Trim().Length <= 0)
                                    {
                                        string dataLeit = DateTime.Now.Day.ToString().PadLeft(2, '0')
                                                        + DateTime.Now.Month.ToString().PadLeft(2, '0')
                                                        + DateTime.Now.Year.ToString().PadLeft(4, '0');
                                        linha += dataLeit.PadRight(14, '0');
                                    }
                                    else
                                        linha += dt.Rows[a][field[n]].ToString().Replace("/", "").Replace(":", "").Replace(" ", "").PadRight(Convert.ToInt32(tamanho[n].Trim()), ' ');
                                    #endregion
                                }
                                else if (formato[n] == "Data")
                                {
                                    #region "Data"
                                    if (dt.Rows[a][field[n]].ToString().Trim().Length > 0)
                                    {
                                        linha += (dt.Rows[a][field[n]].ToString().Trim().Replace("-", "").Replace("/", "").Substring(0, 2) + dt.Rows[a][field[n]].ToString().Trim().Replace("-", "").Replace("/", "").Substring(2, 2) + dt.Rows[a][field[n]].ToString().Trim().Replace("-", "").Replace("/", "").Substring(4, 4)).PadRight(10, ' ');
                                    }
                                    else
                                        linha += dt.Rows[a][field[n]].ToString().Replace("/", "").PadRight(Convert.ToInt32(tamanho[n].Trim()), ' ');
                                    #endregion
                                }
                                else if (formato[n] == "DataDAEM")
                                {
                                    #region "Data"
                                    if (dt.Rows[a][field[n]].ToString().Trim().Length > 0)
                                    {
                                        linha += dt.Rows[a][field[n]].ToString().Trim().Replace("-", "").Replace("/", "");
                                    }
                                    else
                                        linha += dt.Rows[a][field[n]].ToString().Replace("/", "").PadRight(Convert.ToInt32(tamanho[n].Trim()), ' ');
                                    #endregion
                                }

                                else if (formato[n] == "dataClementina")
                                {
                                    #region "Data"
                                    if (dt.Rows[a][field[n]].ToString().Trim().Length > 0)
                                    {
                                        linha += dt.Rows[a][field[n]].ToString().Trim().Replace("/", "").Replace(" ", "").Replace(":", "").Substring(0, 12).PadRight(Convert.ToInt32(tamanho[n].Trim()), ' ');
                                    }
                                    else
                                        linha += dt.Rows[a][field[n]].ToString().Replace("/", "").PadRight(Convert.ToInt32(tamanho[n].Trim()), ' ');
                                    #endregion
                                }
                                else if (formato[n] == "dataRioPardo")
                                {
                                        #region "Data"
                                        int tam =  Convert.ToInt32(tamanho[n]);
                                    if (Convert.ToString(dt.Rows[a][field[n]].ToString().Replace("/", "").PadRight(tam, ' ')).Trim().Length <= 0)
                                    {

                                            linha += dt.Rows[a][field[n]].ToString().PadRight(14, ' ');
                                    }
                                    else
                                        //linha += dt.Rows[a][field[n]].ToString().Replace("/", "").Replace(":", "").Replace(" ", "").PadRight(tam, ' ');
                                        linha += dt.Rows[a][field[n]].ToString().Replace(" ", "-").Split('-')[0].Replace("/", "").PadRight(tam - 4, ' ') 
                                            + dt.Rows[a][field[n]].ToString().Replace(" ", "-").Split('-')[1].Replace(":", "").Substring(0,4).PadRight(4,' ');
                                        #endregion
                                    }

                                    else if (formato[n] == "Valor")
                                    {
                                        #region "Valor"
                                        decimal vlr = 0;
                                        if (dt.Rows[a][field[n]].ToString().Trim().Length > 0)
                                            vlr = Convert.ToDecimal(dt.Rows[a][field[n]].ToString().Replace('.', ','));
                                        if (vlr == 0)
                                            linha += vlr.ToString("0.00").PadLeft(Convert.ToInt32(tamanho[n].Trim()), '0');
                                        else if (vlr >= 0)
                                            linha += vlr.ToString("0.00").PadLeft(Convert.ToInt32(tamanho[n].Trim()), '0');
                                        else
                                        {
                                            vlr = vlr * (-1);
                                            linha += "-" + vlr.ToString("0.00").PadLeft(Convert.ToInt32(tamanho[n].Trim()) - 1, '0');
                                        }
                                        #endregion
                                    }

                                    else if (formato[n] == "NLigacao")
                                {
                                    #region "N"
                                    decimal vlr = 0;
                                    if (dt.Rows[a][field[n]].ToString().Trim().Length > 0)
                                        vlr = Convert.ToDecimal(dt.Rows[a][field[n]].ToString().Replace('.', ',').Substring(0, dt.Rows[a][field[n]].ToString().Replace('.', ',').Length - 1));
                                    if (vlr >= 0)
                                        linha += vlr.ToString().PadLeft(Convert.ToInt32(tamanho[n].Trim()), '0');
                                    else
                                    {
                                        vlr = vlr * (-1);
                                        linha += "-" + vlr.ToString().PadLeft(Convert.ToInt32(tamanho[n].Trim()) - 1, '0');
                                    }
                                    #endregion
                                }

                                else if (formato[n] == "Hora")
                                {
                                    #region "Data"
                                    if (dt.Rows[a][field[n]].ToString().Trim().Length > 0)
                                    {
                                        linha += dt.Rows[a][field[n]].ToString().Trim().Replace(":", "").Replace(" ", "");
                                    }
                                    else
                                        linha += dt.Rows[a][field[n]].ToString().Replace(":", "").Replace(" ", "").PadRight(Convert.ToInt32(tamanho[n].Trim()), ' ');
                                    #endregion
                                }
                                else if (formato[n] == "/100")
                                {
                                    #region "/100"
                                    decimal ttmp = 0;

                                    if (dt.Rows[a][field[n]].ToString().Trim().Length > 0)
                                        ttmp = Convert.ToDecimal(dt.Rows[a][field[n]].ToString()) / 100;

                                    if (ttmp >= 0)
                                        linha += ttmp.ToString().PadLeft(Convert.ToInt32(tamanho[n].Trim()), '0');
                                    else
                                    {
                                        ttmp = ttmp * (-1);
                                        linha += "-" + ttmp.ToString().PadLeft(Convert.ToInt32(tamanho[n].Trim()) - 1, '0');
                                    }
                                    #endregion
                                }
                                else if (formato[n] == "*100")
                                {
                                    #region "*100"
                                    string ttmp = "0";

                                    if (dt.Rows[a][field[n]].ToString().Trim().Length > 0)
                                        ttmp = (Convert.ToDecimal(dt.Rows[a][field[n]].ToString()) * 100).ToString().Replace(',', '.').Split('.')[0];

                                    if (Convert.ToDecimal(ttmp) >= 0)
                                        linha += ttmp.ToString().PadLeft(Convert.ToInt32(tamanho[n].Trim()), '0');
                                    else
                                    {
                                        ttmp = (Convert.ToDecimal(ttmp) * (-1)).ToString();
                                        linha += "-" + ttmp.ToString().PadLeft(Convert.ToInt32(tamanho[n].Trim()) - 1, '0');
                                    }
                                    #endregion
                                }
                                else if (formato[n] == "Right")
                                {
                                    #region "Right"
                                    if (dt.Rows[a][field[n]].ToString().Length > 0)
                                    {
                                        int dife = dt.Rows[a][field[n]].ToString().Length - Convert.ToInt32(tamanho[n].Trim());
                                        linha += dt.Rows[a][field[n]].ToString().Substring(dife, dt.Rows[a][field[n]].ToString().Length - dife);
                                    }
                                    else
                                        linha += dt.Rows[a][field[n]].ToString().PadLeft(Convert.ToInt32(tamanho[n].Trim(), ' '));
                                    #endregion
                                }
                                else if (formato[n].Contains("Left-"))
                                {
                                    #region "Left-"
                                    if (dt.Rows[a][field[n]].ToString().Length > 0)
                                    {
                                        int ret = Convert.ToInt32(formato[n].Split('-')[1].ToString());
                                        int dife = dt.Rows[a][field[n]].ToString().Length - ret;

                                        if (formato[n].Contains("N"))
                                            linha += dt.Rows[a][field[n]].ToString().Substring(0, dife).PadLeft(Convert.ToInt32(tamanho[n].Trim()), '0');
                                        else
                                            linha += dt.Rows[a][field[n]].ToString().Substring(0, dife).PadRight(Convert.ToInt32(tamanho[n].Trim()), ' ');
                                    }
                                    else
                                    {
                                        if (formato[n].Contains("N"))
                                            linha += dt.Rows[a][field[n]].ToString().PadLeft(Convert.ToInt32(tamanho[n].Trim(), '0'));
                                        else
                                            linha += dt.Rows[a][field[n]].ToString().PadRight(Convert.ToInt32(tamanho[n].Trim(), ' '));
                                    }
                                    #endregion
                                }
                                else if ((formato[n] == "RightDate") && (tamanho[n] == "6"))
                                {
                                    #region "RightDate"
                                    if (dt.Rows[a][field[n]].ToString().Length > 0)
                                    {
                                        int dife = dt.Rows[a][field[n]].ToString().Length - 8;
                                        string tpm = dt.Rows[a][field[n]].ToString().Substring(dife, dt.Rows[a][field[n]].ToString().Length - dife);
                                        tpm = tpm.Substring(0, 4) + tpm.Substring(6, 2);
                                        linha += tpm;
                                    }
                                    else
                                        linha += dt.Rows[a][field[n]].ToString().PadLeft(Convert.ToInt32(tamanho[n].Trim()), ' ');
                                    #endregion
                                }
                                else if (formato[n].Contains("Right"))
                                {
                                    #region "Right"
                                    if (dt.Rows[a][field[n]].ToString().Length > 0)
                                    {
                                        int ctr = dt.Rows[a][field[n]].ToString().Length;

                                        if (formato[n].Contains("("))
                                            ctr = Convert.ToInt32(formato[n].Split('(')[1].ToString().Replace(")", ""));

                                        if (dt.Rows[a][field[n]].ToString().Length > ctr)
                                            ctr = dt.Rows[a][field[n]].ToString().Length - ctr;
                                        else
                                            ctr = dt.Rows[a][field[n]].ToString().Length;

                                        if (ctr > Convert.ToInt32(tamanho[n].Trim()))
                                            ctr = Convert.ToInt32(tamanho[n].Trim());

                                        if ((formato[n].Contains("N")) && (Convert.ToInt32(cliente) == 4))//SAAE PB
                                            linha += dt.Rows[a][field[n]].ToString().Substring(0, ctr).PadLeft(Convert.ToInt32(tamanho[n].Trim()), '0');
                                        else if (formato[n].Contains("N"))
                                            linha += dt.Rows[a][field[n]].ToString().Substring(ctr).PadRight(Convert.ToInt32(tamanho[n].Trim()), '0');
                                        else
                                            linha += dt.Rows[a][field[n]].ToString().Substring(ctr).PadLeft(Convert.ToInt32(tamanho[n].Trim()), ' ');

                                    }
                                    else
                                    {
                                        if (formato[n].Contains("N"))
                                            linha += dt.Rows[a][field[n]].ToString().PadLeft(Convert.ToInt32(tamanho[n].Trim()), '0');
                                        else
                                            linha += dt.Rows[a][field[n]].ToString().PadRight(Convert.ToInt32(tamanho[n].Trim()), ' ');
                                    }
                                    #endregion
                                }
                                else if (formato[n].Contains("Left"))
                                {
                                    #region "Left"
                                    if (dt.Rows[a][field[n]].ToString().Length > 0)
                                    {
                                        int ctr = dt.Rows[a][field[n]].ToString().Length;

                                        if (formato[n].Contains("("))
                                            ctr = Convert.ToInt32(formato[n].Split('(')[1].ToString().Replace(")", ""));

                                        if (dt.Rows[a][field[n]].ToString().Length < ctr)
                                            ctr = dt.Rows[a][field[n]].ToString().Length;

                                        if (ctr > Convert.ToInt32(tamanho[n].Trim()))
                                            ctr = Convert.ToInt32(tamanho[n].Trim());

                                        string dtLeit = dt.Rows[a][field[n]].ToString().Substring(0, ctr).PadLeft(Convert.ToInt32(tamanho[n].Trim()), ' ');
                                        if (formato[n].Contains("N"))
                                            dtLeit = dt.Rows[a][field[n]].ToString().Substring(0, ctr).PadRight(Convert.ToInt32(tamanho[n].Trim()), '0');

                                        if (formato[n].Contains("Date"))
                                        {
                                            if (dtLeit.Substring(dtLeit.Length - 2, 2) == "00")
                                                dtLeit = dtLeit.Substring(0, 4) + "20" + dtLeit.Substring(4, 2);
                                        }
                                        linha += dtLeit;
                                    }
                                    else
                                    {
                                        if (formato[n].Contains("N"))
                                            linha += dt.Rows[a][field[n]].ToString().PadLeft(Convert.ToInt32(tamanho[n].Trim()), '0');
                                        else
                                            linha += dt.Rows[a][field[n]].ToString().PadRight(Convert.ToInt32(tamanho[n].Trim()), ' ');
                                    }
                                    #endregion
                                }
                                else if (formato[n].Contains("Seq"))
                                {
                                    #region "Seq"
                                    int b = a + 1;
                                    linha += b.ToString().PadLeft(Convert.ToInt32(tamanho[n]), '0');

                                    #endregion
                                }
                                else if (formato[n].Contains("Lado"))
                                {
                                    #region "Lado"
                                    linha += "1";

                                    #endregion
                                }
                                else if (formato[n] == "S/N")
                                {
                                    #region "S/N"
                                    if (dt.Rows[a][field[n]].ToString().Trim().Length == 0)
                                        linha += "N";
                                    else
                                        linha += dt.Rows[a][field[n]].ToString().Trim();
                                    #endregion
                                }
                                else if (formato[n] == "An")
                                {
                                    #region "Analise Agua"
                                    decimal vlr = 0;


                                    if (dt.Rows[a][field[n]].ToString().Trim().Length > 0)
                                        vlr = Convert.ToDecimal(dt.Rows[a][field[n]].ToString());

                                    if (vlr >= 0)
                                        linha += vlr.ToString().PadLeft(Convert.ToInt32(tamanho[n].Trim()), ' ');
                                    else
                                    {
                                        vlr = vlr * (-1);
                                        linha += "-" + vlr.ToString().PadLeft(Convert.ToInt32(tamanho[n].Trim()) - 1, '0');
                                    }
                                    #endregion
                                }
                                else if ((formato[n].Substring(0, 1) == "P") && (formato[n].Split('.').Length > 1))
                                    linha += dt.Rows[a][field[n]].ToString().Substring(Convert.ToInt32(formato[n].Split('.')[1]), Convert.ToInt32(tamanho[n]));
                                else if (formato[n] == "SetRota")
                                {
                                    #region "SetRota"
                                    if (Convert.ToInt32(cliente) == 6)
                                    {
                                        if (dt.Rows[a][field[n]].ToString().Substring(0, 2) == "14")
                                        {
                                            linha += Convert.ToString("14" + dt.Rows[a][field[n]].ToString().Substring(2).PadLeft(4, '0')).PadLeft(10, '0');
                                        }
                                        else
                                        {
                                            linha += Convert.ToString(dt.Rows[a][field[n]].ToString().Substring(0, 1) + dt.Rows[a][field[n]].ToString().Substring(1).PadLeft(4, '0')).PadLeft(10, '0');
                                        }
                                    }
                                    #endregion
                                }

                                else
                                    linha += dt.Rows[a][field[n]].ToString();

                                    if (formato[n] == "num")
                                    {
                                        if (Convert.ToInt32(tamanho[n].Trim()) < dt.Rows[a][field[n]].ToString().Split('.')[0].Trim().Length)
                                        {
                                            if ((!formato[n].Contains("Hora")) && (!formato[n].Contains("Data")) && (!formato[n].Contains("Right")) && (formato[n].Substring(0, 1) != "P") && (!formato[n].Contains("Left")) && (!formato[n].Contains("dataClementina")))
                                            {
                                                int difff = dt.Rows[a][field[n]].ToString().Length - Convert.ToInt32(tamanho[n].Trim());
                                                if (linha.Length > difff)
                                                    linha = linha.Substring(0, (linha.Length - difff));
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (Convert.ToInt32(tamanho[n].Trim()) < dt.Rows[a][field[n]].ToString().Trim().Length)
                                        {
                                            if ((!formato[n].Contains("Hora")) && (!formato[n].Contains("Data")) && (!formato[n].Contains("Right")) && (formato[n].Substring(0, 1) != "P") && (!formato[n].Contains("Left")) && (!formato[n].Contains("dataClementina")) && (!formato[n].Contains("dataRioPardo")))
                                            {
                                                int difff = dt.Rows[a][field[n]].ToString().Length - Convert.ToInt32(tamanho[n].Trim());
                                                if (linha.Length > difff)
                                                    linha = linha.Substring(0, (linha.Length - difff));
                                            }
                                        }
                                    }



                                }
                                else
                                {
                                    if (formato[n] == "N")
                                        linha += valor[n].ToString().PadLeft(Convert.ToInt32(tamanho[n].Trim()), '0');
                                    else
                                        linha += valor[n].ToString().PadRight(Convert.ToInt32(tamanho[n].Trim()), ' ');
                                }
                            }

                            if (separador.Length > 0)
                            {
                                if (Convert.ToInt32(cliente) == 1)
                                {
                                    if (tabela == "WTR_LEITURAS")
                                    {
                                        linha += separador;
                                    }
                                }
                                else
                                {
                                    linha += separador;
                                }
                            }
                        }

                        #endregion

                        linha = linha.ToUpper();
                        linha = linha.Replace("À", "A").Replace("Á", "A").Replace("Ã", "A").Replace("Â", "A").Replace("È", "E").Replace("É", "E").Replace("Ê", "E").Replace("Ì", "I").Replace("Í", "I").Replace("Ò", "O").Replace("Ó", "O").Replace("Õ", "O").Replace("Ô", "O").Replace("Ù", "U").Replace("Ú", "U").Replace("Û", "U").Replace("Ç", "C");

                        if (linha.IndexOf("#") > 0)
                        {
                            for (int s = 0; s < linha.Split('#').Length - 1; s++)
                            {
                                sw.WriteLine(linha.Split(Convert.ToChar("#"))[s] + "#");
                            }
                        }
                        else
                        {
                            sw.WriteLine(linha);
                        }
                    }


                }
                else { }
                #endregion


                i++;
            }


            if (!sw.Equals(StreamWriter.Null))
                sw.Close();


            btDownload.Enabled = true;
            btDownload.Text = "Download: " + mes.ToString().Trim().PadLeft(2, '0') + file.Substring(file.Length - 2, 2).PadLeft(4, '0') + "." + ext;
            //Move nome arquivo para o outro list de backup




            if (!sw.Equals(StreamWriter.Null))
                sw.Close();
            #endregion


        }
        catch (Exception ex)
        {
            string resp = ("Erro :" + " - " + ex.Message);
            msg += (resp);
        }
        finally
        {
            sw.Close();
        }

    }

  


}