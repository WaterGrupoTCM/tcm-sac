﻿using Ionic.Zip;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using WaterSyncLite.Class;
using WaterSyncLite.DAL;
using WaterSyncLite.MDL;

/// <summary>
/// Summary description for Rinopolis
/// </summary>
public class Rinopolis
{

    int cat = 0;
    int idFaixa = 0;
    String msg = "";
    string cliente = "02";
    string empresa = "01";
    int[,] Rotas = new int[9,2];
    public Rinopolis()
    {

    }

    public int Import(string cam, string[] file, string rota, string mes, string ano, string caminhoProjeto, string leiturista, String strCon,
        TextBox txtSeparador, DropDownList cbxLeit, ListBox lsvArquivos, TextBox txtStatus)
    {
        Data db = new Data();
        #region Buscando informações no arquivo Import.xml        
        Import import2;
        ImportDAL impDAL = new ImportDAL();
        Import import = new Import();

        try ///GERAL
        {
            
            for (int f = 0; f < 3; f++)
            {
                string check = file[f];
                int countUnder = file[f].Split('_').Length;

                //Tratamento de houver underline
                if (countUnder > 1)
                    import = impDAL.getImp(Convert.ToInt32(Convert.ToInt32(cliente)), file[f].Split('_')[0].Trim());
                else
                {
                    if (file[f].Length > 4)
                        import = impDAL.getImp(Convert.ToInt32(Convert.ToInt32(cliente)), file[f].Remove(file[f].Length - 4, 4));
                    else
                        import = impDAL.getImp(Convert.ToInt32(Convert.ToInt32(cliente)), file[f]);
                }

        #endregion




                //if ((import.Campos.Length > 1) && file.Split('_')[0] == "LEITURA")
                //    import = impDAL.getImp(Convert.ToInt32(Convert.ToInt32(cliente)), file.Split('_')[0].Trim());
                //else if ((import.Campos.Length > 1) && file.Split('_')[0] == "FATURAS")
                //    import = impDAL.getImp(Convert.ToInt32(Convert.ToInt32(cliente)), file.Split('_')[0].Trim());
                //else
                import = impDAL.getImp(Convert.ToInt32(Convert.ToInt32(cliente)), file[f].Split('_')[0].Trim());
                FileStream ofs = File.Open(cam + file[f], FileMode.Open, FileAccess.Read, FileShare.None);


                StreamReader sr = new StreamReader(ofs, System.Text.Encoding.GetEncoding("ISO-8859-1"));	//Aceita caracteres especiais.

                string linha = string.Empty;
                int i = 0;
                //string empresa = Request.Cookies["EmpID"].Value.ToString().Trim();
                string sql = "";
                string sqlLeitura = "";
                string Ligacao = "";
                string tabela = "";
                string insert = "";
                int p = 0;
                string[] campos = null;
                string[] field = null;
                string[] valor = null;
                string idLinha = "";
                string erro = "";

                string tipdebRef = "";
                string mesRef = DateTime.Now.Month.ToString().PadLeft(2, '0');
                string anoRef = DateTime.Now.Year.ToString().PadLeft(4, '0'); ;

                int linhaC = 0;		//CETIL - Controle de linhas de Registro C (discriminação dos serviços)



                try
                {


                    while (sr.Peek() >= 0)
                    {
                        i++;

                        ////lblCount.Refresh();
                        linha = string.Empty;
                        linha = sr.ReadLine();

                        //Retira o separador de campo - Na importação não deve ter.
                        if (txtSeparador.Text.Trim().Length < 1)
                            linha = linha.Replace("|", "").Replace(";", "");
                        //linha = linha.Replace("|"," ").Replace(";"," ");

                        linha = linha.PadRight(141, ' ');
                        linha = linha.Replace("'", " ");		//Alteração feita em 07Dez2004 - Evita erro de SQL com apóstofro - Padovani.
                        linha = linha.Replace("\r\n", " ");	//Alteração feita em 24Mar2005 - Evita erro com <Enter> - Padovani.
                        int len = linha.Length;
                        string reg = linha.Substring(0, 1);
                                    
                        //Demais Sistemas (MMR / NovoSis / Assessor Público) - Pompéia / Rinópolis / Araçatuba.                        
                        #region Leitura de Conteúdo do Arquivo

                        if ((Convert.ToInt32(cliente) == 6) && (linha.Substring(0, 3) == "FAI"))
                        {
                            import2 = impDAL.getImp(Convert.ToInt32(Convert.ToInt32(cliente)), "FAIXA");
                            campos = import2.Campos.Split('|');
                            tabela = import2.Tabela;
                        }
                        else if ((Convert.ToInt32(cliente) == 11) && (linha.Substring(0, 3) == "FAI"))
                        {
                            import2 = impDAL.getImp(Convert.ToInt32(Convert.ToInt32(cliente)), "FAIXA");
                            campos = import2.Campos.Split('|');
                            tabela = import2.Tabela;

                        }
                        else
                        {
                            campos = import.Campos.Split('|');
                            tabela = campos[0];

                        }
                        field = new string[campos.Length - 1];
                        valor = new string[campos.Length - 1];

                        p = 0;
                        while (p < (campos.Length - 1))
                        {

                            field[p] = campos[p + 1].Split(';')[0].Trim();

                            if (txtSeparador.Text.Trim().Length > 0)
                            {
                                char separa = Convert.ToChar(txtSeparador.Text.Trim());

                                if (campos[p + 1].Split(';').Length > 3)
                                {
                                    valor[p] = campos[p + 1].Split(';')[3].Trim().Replace('"', ' ');
                                    valor[p] = valor[p].Trim();

                                    if (valor[p].Trim() == "seq")
                                        valor[p] = i.ToString();
                                    else if (valor[p].Trim() == ".leiturista")
                                        valor[p] = cbxLeit.SelectedValue.ToString();

                                }
                                else
                                    valor[p] = linha.Split(separa)[p].ToString();

                                if (campos[p + 1].Split(';').Length > 2)
                                {
                                    if (Convert.ToInt32(campos[p + 1].Split(';')[2].ToString().Trim()) > 0)
                                        valor[p] = valor[p].Substring(0, Convert.ToInt32(campos[p + 1].Split(';')[2].ToString().Trim()));
                                }
                            }
                            else
                            {
                                if (campos[p + 1].Split(';').Length > 3)	//Valor fixo
                                {
                                    if (campos[p + 1].Split(';')[3].Trim() == "'Data'")
                                    {
                                        valor[p] = linha.Substring(Convert.ToInt32(campos[p + 1].Split(';')[1].Trim()), Convert.ToInt32(campos[p + 1].Split(';')[2].Trim())).Trim();
                                        if (valor[p].ToString().Length > 0)
                                            valor[p] = valor[p].Substring(0, 2) + "/" + valor[p].Substring(2, 2) + "/" + valor[p].Substring(4, 4);
                                    }
                                    else if (campos[p + 1].Split(';')[3].Trim() == ".cliente")
                                        valor[p] = cliente;
                                    else if (campos[p + 1].Split(';')[3].Trim() == ".empresa")
                                        valor[p] = empresa;
                                    //else if (campos[p + 1].Split(';')[3].Trim() == ".cidade")
                                    //    valor[p] = txtCidade.Text.Trim();
                                    else if (campos[p + 1].Split(';')[3].Trim() == ".leiturista")
                                        valor[p] = cbxLeit.SelectedValue.ToString();
                                    else if (campos[p + 1].Split(';')[3].Trim() == ".local")
                                        valor[p] = rota;
                                    else if (campos[p + 1].Split(';')[3].Trim() == ".mes")
                                        valor[p] = mes;
                                    else if (campos[p + 1].Split(';')[3].Trim() == ".ano")
                                        valor[p] = ano;
                                    else if (campos[p + 1].Split(';')[3].Trim() == "'/100'")
                                    {
                                        valor[p] = linha.Substring(Convert.ToInt32(campos[p + 1].Split(';')[1].Trim()), Convert.ToInt32(campos[p + 1].Split(';')[2].Trim()));
                                        valor[p] = Convert.ToString(Convert.ToDecimal(valor[p]) / 100).Replace(',', '.');
                                    }
                                    else if (campos[p + 1].Split(';')[3].Trim() == "'/10'")
                                    {
                                        valor[p] = linha.Substring(Convert.ToInt32(campos[p + 1].Split(';')[1].Trim()), Convert.ToInt32(campos[p + 1].Split(';')[2].Trim()));
                                        valor[p] = Convert.ToString(Convert.ToDecimal(valor[p]) / 10).Replace(',', '.');
                                    }
                                    else if (campos[p + 1].Split(';')[3].Trim() == ".Trim")
                                    {
                                        valor[p] = linha.Substring(Convert.ToInt32(campos[p + 1].Split(';')[1].Trim()), Convert.ToInt32(campos[p + 1].Split(';')[2].Trim()));
                                        valor[p] = valor[p].Replace(" ", "");
                                    }
                                    else if (campos[p + 1].Split(';')[3].Trim() == ".Num")
                                    {
                                        valor[p] = linha.Substring(Convert.ToInt32(campos[p + 1].Split(';')[1].Trim()), Convert.ToInt32(campos[p + 1].Split(';')[2].Trim()));
                                        if (valor[p].Trim().Length < 1)
                                            valor[p] = "0";

                                        valor[p] = valor[p].Replace(".", "");   //Retirar ponto de milhar
                                        valor[p] = valor[p].Replace(",", ".");  //Substituir vírgula de centavos

                                        if (valor[p].IndexOf("-") > 0)
                                        {
                                            valor[p] = valor[p].Replace("-", "");
                                            valor[p] = "-" + valor[p];
                                        }
                                    }
                                    else if (campos[p + 1].Split(';')[3].Trim() == ".Txt/daea")
                                    {
                                        valor[p] = linha.Substring(Convert.ToInt32(campos[p + 1].Split(';')[1].Trim()), Convert.ToInt32(campos[p + 1].Split(';')[2].Trim()));
                                        valor[p] = valor[p].Replace("LV1", "LVW").Replace("LV2", "LVY").Replace("LV3", "LVX");
                                        valor[p] = valor[p].Replace("0", "").Replace("1", "").Replace("2", "").Replace("3", "").Replace("4", "").Replace("5", "").Replace("6", "").Replace("7", "").Replace("8", "").Replace("9", "");
                                        valor[p] = valor[p].Replace("LVW", "LV1").Replace("LVY", "LV2").Replace("LVX", "LV3");
                                        valor[p] = valor[p].Trim().Replace(" ", "/");
                                    }
                                    else if (campos[p + 1].Split(';')[3].Trim() == ".Num/daea")
                                    {
                                        valor[p] = linha.Substring(Convert.ToInt32(campos[p + 1].Split(';')[1].Trim()), Convert.ToInt32(campos[p + 1].Split(';')[2].Trim()));
                                        valor[p] = valor[p].ToUpper().Replace("RES", "").Replace("COM", "").Replace("IND", "").Replace("PUB", "").Replace("SOC", "").Replace("CIV", "").Replace("PAR", "").Replace("LV1", "").Replace("LV2", "").Replace("LV3", "").Replace("NES", "");
                                        valor[p] = valor[p].Trim().Replace(" ", "/");
                                    }
                                    else if (campos[p + 1].Split(';')[3].Trim() == ".stsDAEA")
                                    {
                                        valor[p] = linha.Substring(Convert.ToInt32(campos[p + 1].Split(';')[1].Trim()), Convert.ToInt32(campos[p + 1].Split(';')[2].Trim()));
                                        valor[p] = valor[p].ToUpper().Replace("T", "1").Replace("F", "2").Replace("D", "3").Replace("E", "4");
                                    }
                                    else if (campos[p + 1].Split(';')[3].Trim() == ".N/0,S/1")
                                    {
                                        valor[p] = linha.Substring(Convert.ToInt32(campos[p + 1].Split(';')[1].Trim()), Convert.ToInt32(campos[p + 1].Split(';')[2].Trim()));
                                        valor[p] = valor[p].ToUpper().Replace("S", "1").Replace("N", "0");
                                    }
                                    else if ((campos[p + 1].Split(';')[3].Trim().Length > 5) && (campos[p + 1].Split(';')[3].Trim().Substring(0, 6) == ".Split"))
                                    {
                                        valor[p] = linha.Substring(Convert.ToInt32(campos[p + 1].Split(';')[1].Trim()), Convert.ToInt32(campos[p + 1].Split(';')[2].Trim()));
                                        if ((Convert.ToInt32(cliente) == 6) && (field[p].ToUpper() == "FAT_MES"))
                                        {
                                            valor[p] = valor[p].ToUpper();
                                            valor[p] = valor[p].Replace("JANEIRO", "01").Replace("FEVEREIRO", "02").Replace("MARÇO", "03").Replace("ABRIL", "04").Replace("MAIO", "05").Replace("JUNHO", "06").Replace("JULHO", "07").Replace("AGOSTO", "08").Replace("SETEMBRO", "09").Replace("OUTUBRO", "10").Replace("NOVEMBRO", "11").Replace("DEZEMBRO", "12").Replace("MARCO", "03");
                                        }

                                        char separ = Convert.ToChar(campos[p + 1].Split(';')[3].Trim().Substring(8, 1));
                                        int posic = Convert.ToInt32(campos[p + 1].Split(';')[3].Trim().Substring(12, 1));
                                        valor[p] = valor[p].Split(separ)[posic].Trim();
                                    }
                                    else if (campos[p + 1].Split(';')[3].Trim() == ".seq")
                                        valor[p] = i.ToString();
                                    else
                                        valor[p] = campos[p + 1].Split(';')[3].Replace('"', ' ').Trim();
                                }
                                else
                                    valor[p] = linha.Substring(Convert.ToInt32(campos[p + 1].Split(';')[1].Trim()), Convert.ToInt32(campos[p + 1].Split(';')[2].Trim()));
                            }



                            p++;
                        }

                        if(tabela=="WTR_LEITURAS")
                        {
                            valor[5] = BuscaRota(valor[5]);//Procura a rota que a ligação pertence

                            for(int k=0;k< 9; k++)
                            {
                                if(Convert.ToUInt32(Rotas[k,0])== Convert.ToUInt32(valor[5]))
                                {
                                    valor[6] = Convert.ToString(Rotas[k, 1]);//adiciona a sequencia da ligação na tabela de Leitura
                                }
                                    
                            }
                            
                        }

                        #endregion
                        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////                     
                        ///////////////////////////////////////////////////
                        #region Comando SQL para o BANCO DE DADOS LEITURA

                        if (tabela != "WTR_LIVROS")
                        {
                            sql = "INSERT INTO " + tabela + " (";

                            p = 0;
                            while (p < field.Length)
                            {
                                if (p > 0)
                                {
                                    if (field[p - 1].Trim() != field[p].Trim())
                                        sql += field[p].Trim() + ",";
                                }
                                else
                                    sql += field[p].Trim() + ",";
                                p++;
                            }

                            sql = sql.Replace("A#", "");
                            sql = sql.Replace("B#", "");
                            sql = sql.Replace("C#", "");
                            sql = sql.Replace("D#", "");
                            sql = sql.Replace("E#", "");

                            sql = sql.Remove(sql.Length - 1, 1);
                            sql += ") VALUES ('";

                            p = 0;
                            while (p < valor.Length)
                            {
                                if (valor[p] != null)
                                {
                                    if (p > 0)
                                    {
                                        if (field[p - 1].Trim() == field[p].Trim())
                                            sql = sql.Remove(sql.Length - 3, 3);
                                        sql += valor[p].Trim() + "','";

                                    }
                                    else
                                    {
                                        sql += valor[p].Trim() + "','";
                                    }
                                }
                                else
                                    sql += "','";
                                p++;
                            }

                            sql = sql.Remove(sql.Length - 2, 2);
                            sql += ")";

                            erro = db.Ins(strCon, sql);                            
                        }
                        else
                        {
                            for (int j = 0; j <9 ; j++)
                            {
                                sql = "INSERT INTO " + tabela + " (";
                                
                                p = 0;
                                while (p < field.Length)
                                {
                                    if (p > 0)
                                    {
                                        if (field[p - 1].Trim() != field[p].Trim())
                                            sql += field[p].Trim() + ",";
                                    }
                                    else
                                        sql += field[p].Trim() + ",";
                                    p++;
                                }

                                sql = sql.Replace("A#", "");
                                sql = sql.Replace("B#", "");
                                sql = sql.Replace("C#", "");
                                sql = sql.Replace("D#", "");
                                sql = sql.Replace("E#", "");

                                sql = sql.Remove(sql.Length - 1, 1);
                                sql += ") VALUES ('";

                                p = 0;
                                
                                while (p < valor.Length)
                                {
                                    if (valor[p] != null)
                                    {
                                        if (p > 0)
                                        {
                                            if (p == 6)
                                            {
                                                sql += Convert.ToString(Rotas[j,0]) + "','";
                                            }
                                            else if (p == 9)
                                            {
                                                sql += Convert.ToString(Rotas[j, 1]) + "','";
                                            }
                                            else if(field[p - 1].Trim() == field[p].Trim())
                                            { 
                                                sql = sql.Remove(sql.Length - 3, 3);                                               
                                            }
                                            else
                                            sql += valor[p].Trim() + "','";


                                        }
                                        else
                                        {
                                            sql += valor[p].Trim() + "','";
                                        }
                                    }
                                    else
                                        sql += "','";
                                    p++;
                                }

                                sql = sql.Remove(sql.Length - 2, 2);
                                sql += ")";
                                
                                erro = db.Ins(strCon, sql);
                               
                            }
                        }                     

                        #endregion
                    }//fim while sr.peek

                    if (f > 0)
                    {
                        string tipo = "U";
                    }

                }
                catch (Exception ex)
                {
                    msg += (i + " #" + linha + " - " + ex.Message) + "\n";
                }

                finally
                {
                    sr.Close();
                    ofs.Close();

                    string path = caminhoProjeto;
                    string localBkp = caminhoProjeto.Substring(0, caminhoProjeto.Length - 3) + @"\BKP\" + cliente.PadLeft(2, '0');
                    File.Move(path + @"\" + file[f], localBkp.Trim() + @"\" + file[f]);
                    lsvArquivos.Items.Remove(file[f]);
                    msg += "Arquivo: " + file[f].Trim() + " carregado com sucesso !\n";

                   

                }
            }//fim do for

            txtStatus.Text = msg;

            return 0;

        }////try geral
        catch (Exception ex)
        {
            msg += (ex.Message);
            txtStatus.Text = msg;
            return -1;
        }

    }

    public String BuscaRota(String rota)
    {
        String Aux = "";

        if (rota.ToString() == "0059" || rota.ToString() == "0058" || rota.ToString() == "0005" || rota.ToString() == "0109" ||
            rota.ToString() == "0009" || rota.ToString() == "0006" || rota.ToString() == "0011")
        {
            if(Rotas[0, 0]==0)
                Rotas[0,0] = 1;            
            Rotas[0,1] = Rotas[0,1] + 1;
            Aux = "1";
        }

        else if (rota.ToString() == "0015" || rota.ToString() == "0018" || rota.ToString() == "0019" || rota.ToString() == "0020" || rota.ToString() == "0022" ||
            rota.ToString() == "0023" || rota.ToString() == "0024" || rota.ToString() == "0025" || rota.ToString() == "0026")
        {
            if(Rotas[1, 0]==0)
                Rotas[1,0] = 2;
            Rotas[1,1] = Rotas[1,1] + 1;
            Aux = "2";
        }

        else if (rota.ToString() == "0028" || rota.ToString() == "0036" || rota.ToString() == "0030" || rota.ToString() == "0031" || rota.ToString() == "0033" ||
            rota.ToString() == "0038" || rota.ToString() == "0039" || rota.ToString() == "0088" || rota.ToString() == "0089" || rota.ToString() == "0090" ||
            rota.ToString() == "0092" || rota.ToString() == "0091" || rota.ToString() == "0105" || rota.ToString() == "0010")
        {
            if (Rotas[2, 0] == 0)
                Rotas[2, 0] = 3;
            Rotas[2,1] = Rotas[2,1] + 1;
            Aux = "3";
        }

        else if (rota.ToString() == "0004" || rota.ToString() == "0003" || rota.ToString() == "0001")
        {
            if(Rotas[3, 0] == 0)
               Rotas[3,0] = 4;
            Rotas[3,1] = Rotas[3,1] + 1;
            Aux = "4";
        }
        else if (rota.ToString() == "0007" || rota.ToString() == "0008" || rota.ToString() == "0040" || rota.ToString() == "0110" || rota.ToString() == "0012" ||
            rota.ToString() == "0016" || rota.ToString() == "0041" || rota.ToString() == "0042")
        {
            if(Rotas[4, 0] == 0)
                Rotas[4,0] = 5;
            Rotas[4,1] = Rotas[4,1] + 1;
            Aux = "5";
        }
        else if (rota.ToString() == "0043" || rota.ToString() == "0044" || rota.ToString() == "0118" || rota.ToString() == "0017" || rota.ToString() == "0120" ||
            rota.ToString() == "0121" || rota.ToString() == "0131" || rota.ToString() == "0161" || rota.ToString() == "0158" || rota.ToString() == "0159" ||
            rota.ToString() == "0160" || rota.ToString() == "0157" || rota.ToString() == "0152" || rota.ToString() == "0149" || rota.ToString() == "0150" ||
            rota.ToString() == "0147" || rota.ToString() == "0148" || rota.ToString() == "0151" || rota.ToString() == "0153" || rota.ToString() == "0154" ||
            rota.ToString() == "0085" || rota.ToString() == "0071" || rota.ToString() == "0069" || rota.ToString() == "0068" || rota.ToString() == "0067" ||
            rota.ToString() == "0063" || rota.ToString() == "0061" || rota.ToString() == "0060" || rota.ToString() == "0126" || rota.ToString() == "0184" ||
            rota.ToString() == "0180" || rota.ToString() == "0177" || rota.ToString() == "0173" || rota.ToString() == "0174")
        {
            if(Rotas[5, 0] == 0)
                Rotas[5, 0] = 6;
            Rotas[5, 1] = Rotas[5, 1] + 1;
            Aux = "6";
        }
        else if (rota.ToString() == "0062" || rota.ToString() == "0064" || rota.ToString() == "0065" || rota.ToString() == "0066" || rota.ToString() == "0072" ||
            rota.ToString() == "0074" || rota.ToString() == "0086" || rota.ToString() == "0073" || rota.ToString() == "0080" || rota.ToString() == "0081")
        {
            if(Rotas[6, 0] == 0)
                Rotas[6,0] = 7;
            Rotas[6,1] = Rotas[6,1] + 1;
            Aux = "7";
        }
        else if (rota.ToString() == "0082" || rota.ToString() == "0083" || rota.ToString() == "0122" || rota.ToString() == "0130" || rota.ToString() == "0129" ||
        rota.ToString() == "0094" || rota.ToString() == "0095" || rota.ToString() == "0096" || rota.ToString() == "0097" || rota.ToString() == "0098" || rota.ToString() == "0099" ||
        rota.ToString() == "0100" || rota.ToString() == "0101" || rota.ToString() == "0102" || rota.ToString() == "0136" || rota.ToString() == "0137" || rota.ToString() == "0138" ||
        rota.ToString() == "0139" || rota.ToString() == "0140")
        {
            if(Rotas[7, 0] == 0)
                Rotas[7,0] = 8;
            Rotas[7,1] = Rotas[7,1] + 1;
            Aux = "8";
        }
        else if (rota.ToString() == "0141" || rota.ToString() == "0142" || rota.ToString() == "0143" || rota.ToString() == "0144" || rota.ToString() == "0145" || rota.ToString() == "0052" ||
            rota.ToString() == "0050" || rota.ToString() == "0054" || rota.ToString() == "0055" || rota.ToString() == "0056" || rota.ToString() == "0053" || rota.ToString() == "0049" ||
            rota.ToString() == "0116" || rota.ToString() == "0117" || rota.ToString() == "0164")
        {
            if(Rotas[8, 0] == 0)
                Rotas[8,0] = 9;
            Rotas[8,1] = Rotas[8,1] + 1;
            Aux = "9";
        }

        return Aux;
    }


    public void AnaliseCritica(String strCon, TextBox txtLivro, DropDownList cbxMsg, DropDownList cbxMes, TextBox txtAno, TextBox txtEndereco, TextBox txtNumero,
     CheckBox chkCritica, TextBox txtPercent, Label lblMsg, DropDownList ddlSituacaoLeitura, DropDownList ddlLeituristas, TextBox txtIdentificacao,
     TextBox txtMedidor, RadioButton rbEndereço, RadioButton rbLigacao, RadioButton rbSequencia, RadioButton rbNomeContribuinte, RadioButton rbRota, GridView gridView, Panel panelMsg, Panel PanelQtdRegistros, Label lblTOTAL)
    {
        DataTable dt = new DataTable();

        try
        {


            double porc = 0;
            string pMax = "";
            string pMin = "";

            #region Porcentagem
            if (txtPercent.Text.Trim().Length < 1)
                porc = 1;
            else
            {
                try
                {
                    porc = Convert.ToInt32(txtPercent.Text.Trim());
                    pMax = Convert.ToDecimal(1 + (porc / 100)).ToString().Replace(",", ".");
                    pMin = Convert.ToDecimal(1 - (porc / 100)).ToString().Replace(",", ".");
                }
                catch (Exception ee)
                {
                    lblMsg.Text = "Informe o percentual correto da crítica";
                }
            }
            #endregion

            string sql = "select LIV_ANO AS ANO, LIV_MES AS MES,CAST(lei_ligacao AS INT) AS LIGACAO,LEI_CONSUMIDOR AS CONSUMIDOR," +
                         "WTR_LEITURAS.LOC_ID AS ROTA, LEI_ENDERECO AS ENDEREÇO, LEI_NUMERO AS NUMERO, LEI_DATA AS DATA," +
                         "LTR_NOME AS LEITURISTA,  MSG_DESCRICAO AS OCORRENCIA, LEI_ANTERIOR AS 'LEIT. ANT'," +
                         "LEI_LEITURA AS 'LEIT. ATUAL',  LEI_NUMMED AS MEDIDOR,  SUM(case when (lei_leitura - lei_anterior) < 0 Then 0 ELSE (lei_leitura - lei_anterior) end) AS CONSUMO,LEI_FORMAENTREGA as ENTREGA," +
                         "LEI_LATITUDE,LEI_LONGITUDE ";


            sql += " FROM WTR_LEITURAS INNER JOIN WTR_MENSAGENS ON(WTR_LEITURAS.MSG_ID = WTR_MENSAGENS.MSG_ID and WTR_LEITURAS.cli_id = WTR_MENSAGENS." +
            "cli_id and WTR_MENSAGENS.emp_id = WTR_LEITURAS.emp_id) INNER JOIN WTR_LEITURISTAS ON (WTR_LEITURAS.LTR_ID = WTR_LEITURISTAS.LTR_ID " +
            "and WTR_LEITURAS.cli_id = WTR_LEITURISTAS.cli_id and WTR_LEITURISTAS.emp_id = WTR_LEITURAS.emp_id) ";

            sql += "WHERE WTR_LEITURAS.emp_id =" +
                          empresa + " and WTR_LEITURAS.cid_id =" + empresa +
                          " and WTR_LEITURAS.CLI_ID = " + cliente;

            #region SQL E FILTROS

            if (txtLivro.Text.Trim().Length > 0)
            {
                sql += " AND WTR_LEITURAS.LOC_ID = '" + txtLivro.Text.Trim() + "' ";
            }

            if (cbxMsg.SelectedValue != "1000")
                sql += " AND WTR_LEITURAS.MSG_ID = '" + cbxMsg.SelectedValue.ToString() + "' ";

            if (cbxMes.SelectedIndex > 0)
                sql += " AND LIV_MES = '" + cbxMes.SelectedIndex.ToString() + "' ";

            if (txtAno.Text.Trim().Length > 0)
                sql += " AND LIV_ANO = '" + txtAno.Text.Trim() + "' ";

            if (txtEndereco.Text.Trim().Length > 0)
                sql += " AND (replace (LEI_ENDERECO,'É','E') LIKE '" + txtEndereco.Text + "' or LEI_ENDERECOENTREGA LIKE '" + txtEndereco.Text + "') ";

            if (txtNumero.Text.Trim().Length > 0)
                sql += " AND (LEI_NUMERO = " + txtNumero.Text + " or LEI_NUMEROENTREGA LIKE '" + txtNumero.Text + "') ";

            #region Somente critica
            if (chkCritica.Checked)
            {
                sql += " AND (((LEI_LEITURA - LEI_ANTERIOR) > (LEI_MEDIA * " + pMax + ")) OR ((LEI_LEITURA - LEI_ANTERIOR) < (LEI_MEDIA * " + pMin + ")))";
            }
            #endregion

            #region Sem Leitura
            if (ddlSituacaoLeitura.SelectedValue.ToString() == "Com Leitura")
            {
                sql += " AND LEI_LEITURA <> ''";
            }
            else
            {
                if (ddlSituacaoLeitura.SelectedValue.ToString() == "Sem Leitura")
                {
                    sql += " AND (LEI_LEITURA = '' or lei_leitura is null)";
                }
            }
            #endregion
            #region SITUACAO LEITURA
            if (ddlSituacaoLeitura.SelectedValue.ToString() == "Alteração cadastral")
                sql += " AND LEI_NOVACATEGORIA > 0";

            #endregion


            #region Leituristas
            if (ddlLeituristas.SelectedValue.ToString() != "0")
            {
                sql += " AND WTR_LEITURAS.LTR_ID  =  " + ddlLeituristas.SelectedValue;
            }
            #endregion

            #region Identificação e Medidor
            if ((txtIdentificacao.Text.Trim().Length > 0) || (txtMedidor.Text.Trim().Length > 0))
            {
                if (txtIdentificacao.Text.Trim().Length > 0)
                    sql += " AND LEI_LIGACAO LIKE '%" + txtIdentificacao.Text.Trim() + "%' ";

                if (txtMedidor.Text.Trim().Length > 0)
                    sql += " AND replace(LEI_NUMMED,'.','') LIKE '%" + txtMedidor.Text.Trim() + "%' ";
            }

            #endregion

            #endregion

            sql += " GROUP BY LIV_ANO, LIV_MES, LEI_LIGACAO, LEI_CONSUMIDOR, LOC_ID, LEI_ENDERECO, LEI_NUMERO, LEI_DATA, WTR_LEITURISTAS.LTR_NOME,WTR_MENSAGENS.MSG_DESCRICAO," +
                "WTR_LEITURAS.LEI_ANTERIOR,WTR_LEITURAS.LEI_LEITURA,WTR_LEITURAS.LEI_NUMMED,WTR_LEITURAS.LEI_FORMAENTREGA,WTR_LEITURAS.LEI_LATITUDE," +
                "WTR_LEITURAS.LEI_LONGITUDE,WTR_LEITURAS.LEI_SEQ";

            if (rbEndereço.Checked)
                sql += " ORDER BY LEI_ENDERECO,LEI_NUMERO,LIV_ANO, LIV_MES, WTR_LEITURAS.LOC_ID,LEI_SEQ";
            else if (rbLigacao.Checked)
                sql += " ORDER BY lei_ligacao,LIV_ANO, LIV_MES, WTR_LEITURAS.LOC_ID";
            else if (rbSequencia.Checked)
                sql += " ORDER BY lei_seq,LIV_ANO, LIV_MES, WTR_LEITURAS.LOC_ID";
            else if (rbNomeContribuinte.Checked)
                sql += " ORDER BY lei_consumidor,LIV_ANO, LIV_MES, WTR_LEITURAS.LOC_ID";
            else if (rbRota.Checked)
                sql += " ORDER BY WTR_LEITURAS.LOC_ID,LIV_ANO, LIV_MES,lei_seq";
            else
            {

                sql += " ORDER BY LIV_ANO, LIV_MES, lei_seq";
            }

            Data db = new Data();

            dt = db.GetDt(strCon, "WTR_LEITURAS", sql);

            if (dt.DefaultView.Count > 0)
            {
                lblTOTAL.Text = string.Format("{0:C}", Convert.ToDecimal(dt.DefaultView.Count.ToString())).Replace("R$", "").Replace(" ", "").Split(',')[0];
                PanelQtdRegistros.Visible = true;
            }
            else PanelQtdRegistros.Visible = false;

            gridView.DataSource = dt;
            gridView.DataBind();


        }
        catch (Exception ex)
        {
            lblMsg.Text = "Erro - " + ex.Message;
            panelMsg.Visible = true;

        }

    }

    public string Export(string cam, string file, string cliente, string caminhoProjeto, String strCon, HttpResponse response, int mes, string ano, Button btDownload)
    {
        DataTable dt;
        DataTable dt2 = null;
        Data db = new Data();
        string separador = xml.GetConfigValue(caminhoProjeto, "LOCAL", "EXPSEPARADOR", Convert.ToInt32(cliente).ToString());
        string ext = "";
        if (Convert.ToInt32(cliente) == 3)
            ext = "s";
        else if ((Convert.ToInt32(cliente) == 4) || (Convert.ToInt32(cliente) == 10))
            ext = "RET";
        else if ((Convert.ToInt32(cliente) == 5))
            ext = "SIM";
        else
            ext = "txt";
        string sql = "";
        string sql2 = "";//Busca Qtd Ocorrências
        int total = 0;
        int p = 0;
        int i = 0;
        string fileNew = "";
        Export export = new Export();
        ExportDAL expDAL = new ExportDAL();
        StreamWriter sw = StreamWriter.Null;
        int qtd1 = 0, qtd2 = 0, qtd3 = 0;

        try
        {


            #region Buscando informações no arquivo Import.xml

            //Verificar quais tabelas a serem exportadas (Config.xml : <TABEXPORT>)

            string[] tabExport = xml.GetConfigValue(caminhoProjeto, "LOCAL", "TABEXPORT", Convert.ToInt32(cliente).ToString()).Split(',');

            while (i < tabExport.Length)
            {
                export = expDAL.getExp(Convert.ToInt32(cliente), tabExport[i].Trim());

                #region Leitura de Conteúdo do Arquivo de Exportação
                string[] campos = export.Campos.Split('|');
                string tabela = campos[0];
                string[] field = new string[campos.Length - 1];
                string[] tamanho = new string[campos.Length - 1];
                string[] formato = new string[campos.Length - 1];
                string[] valor = new string[campos.Length - 1];

                p = 0;
                while (p < (campos.Length - 1))
                {
                    field[p] = campos[p + 1].Split(';')[0].Trim();
                    formato[p] = campos[p + 1].Split(';')[1].Trim();
                    tamanho[p] = campos[p + 1].Split(';')[2].Trim();

                    if (campos[p + 1].Split(';').Length > 3)	//Valor fixo
                    {
                        valor[p] = campos[p + 1].Split(';')[3].Replace('"', ' ').Trim();
                    }

                    p++;
                }
                #endregion

                mes = Convert.ToInt32(file.Substring(0, 3));
                int cid = Convert.ToInt32(file.Substring(3, 3));
                int loc = Convert.ToInt32(file.Substring(6, 3));

                #region Comando SQL para o BANCO DE DADOS
                sql = "SELECT ";

                p = 0;
                while (p < field.Length)
                {
                    sql += field[p].Trim() + ",";
                    p++;
                }                

                sql = sql.Remove(sql.Length - 1, 1);
                sql += " FROM " + tabela;
                

                if ((tabela == "WTR_LIVROS") || (tabela == "WTR_LEITURAS"))
                {
                    sql += " WHERE LIV_MES = " + mes
                        + " AND LIV_ANO = " + ano
                        + " AND CID_ID = " + cid
                        //+ " AND LOC_ID = " + loc
                        + " AND CLI_ID = " + cliente
                        + " AND EMP_ID = 1 ";                    

                    #region Verifica Qtd Ocorrências                                       
                        if ((tabela == "WTR_LEITURAS"))//&&(Data.cli == 5)
                        {
                        sql2 = "SELECT LTR_ID, MSG_ID, COUNT(*) AS QTD FROM WTR_LEITURAS WHERE LIV_MES = '" + mes + "'"
                        + " AND CID_ID = '" + cid + "'"
                        + " AND LIV_ANO = " + ano
                          //  + " AND LOC_ID = '" + loc + "'"
                            + " AND CLI_ID = '" +cliente + "'"
                            + " GROUP BY LTR_ID, MSG_ID "
                            + " ORDER BY MSG_ID ";

                            dt2 = db.GetDt(strCon, tabela, sql2);
                        }
                    
                    #endregion
                }
                else if (tabela.IndexOf("WTR_FATURA") >= 0)
                {
                    sql += " WHERE WTR_FATURA.FAT_MES = " + mes.ToString().PadLeft(2, '0')
                        + " AND WTR_FATURA.FAT_ANO = '" + ano + "'"
                       + " AND WTR_FATURA.CLI_ID = " + cliente
                       + " AND WTR_FATURA.FAT_PASTA = " + loc             
                       + " AND WTR_FATURA.EMP_ID = 1 " ;

                    
                        sql2 = "SELECT LTR_ID, MSG_ID, COUNT(*) AS QTD FROM WTR_FATURA WHERE FAT_MES = " + mes
                        + " AND FAT_PASTA = '" + loc + "'"
                        + " AND FAT_ANO = " + ano+ ""
                        + " AND CLI_ID = '" + cliente + "'"
                        + " GROUP BY LTR_ID, MSG_ID "
                        + " ORDER BY MSG_ID ";

                        dt2 = db.GetDt(strCon, tabela, sql2);
                   
                   
                }
                else if (tabela == "WTR_SERVICOS")
                {
                    sql += " WHERE SRV_MES = " + mes
                        + " AND SRV_LOCAL = " + loc
                        + " AND CLI_ID = " + cliente                  
                        + " AND EMP_ID = 1 " ;
                }               
                else if (tabela == "WTR_REGISTROLOG")
                {
                    sql += " WHERE LOG_MES = " + mes
                        + " AND LOG_LOCAL = " + loc
                        + " AND CLI_ID = " + cliente                   
                        + " AND EMP_ID = 1 " ;
                }             

                dt = db.GetDt(strCon, tabExport[i].Trim(), sql);

                if (dt.Rows.Count > 0)
                {
                    msg += ("Exportando dados : " + tabExport[i].Trim().ToUpper());

                    if (i == 0)
                        qtd1 = dt.Rows.Count;
                    else if (i == 1) qtd2 = dt.Rows.Count;
                    else if (i == 2) qtd3 = dt.Rows.Count;

                    #region MESMO ARQUIVO DE EXPORTAÇÃO PARA DUAS OU MAIS TABELAS
                    if (fileNew.Length == 0)
                    {
                        if (File.Exists(cam + @"\" + tabExport[i].Trim() + "_" + file + "." + ext))
                            File.Delete(cam + @"\" + tabExport[i].Trim() + "_" + file + "." + ext);

                        if (Convert.ToInt32(cliente) == 2)
                            sw = new StreamWriter(cam + @"\" + tabExport[i].Trim() + "_" + file + "." + ext, true, System.Text.Encoding.GetEncoding("ISO8859-1"));

                        //sw = new StreamWriter(cam + @"\" + tabExport[i].Trim() + "_" + file + "." + ext, true, System.Text.Encoding.GetEncoding("ISO8859-1"));

                    }
                    else
                    {
                        if (sw.Equals(StreamWriter.Null))
                        {
                            if (fileNew.Length > 0)
                                sw = new StreamWriter(cam + @"\" + fileNew.Trim() + "." + ext);
                            else
                                sw = new StreamWriter(cam + @"\" + tabExport[i].Trim() + "_" + file + "." + ext);
                        }
                    }
                    #endregion

                    if(tabela=="WTR_LIVROS")
                    {
                        for(int j = 0; j< dt.Rows.Count;j++)
                        {                            
                            total +=  Convert.ToInt32(dt.Rows[j][field[9]].ToString().Trim());
                        }
                    }

                    for (int a = 0; a < dt.Rows.Count; a++)
                    {
                        //lblCount.Text = a.ToString();
                        //lblCount.Refresh();
                        string linha = string.Empty;

                        #region Leitura de Campos na Tabela
                        for (int n = 0; n < campos.Length - 1; n++)
                        {
                            if (Convert.ToInt32(tamanho[n].Trim()) > 0)
                            {
                                if (valor[n] == null)
                                {
                                    if (formato[n] == "N")
                                    {
                                        #region "N"
                                        decimal vlr = 0;
                                        if (dt.Rows[a][field[n]].ToString().Trim().Length > 0)
                                            vlr = Convert.ToDecimal(dt.Rows[a][field[n]].ToString().Replace('.', ','));
                                        if (vlr >= 0)
                                            linha += vlr.ToString().PadLeft(Convert.ToInt32(tamanho[n].Trim()), '0');
                                        else
                                        {
                                            vlr = vlr * (-1);
                                            linha += "-" + vlr.ToString().PadLeft(Convert.ToInt32(tamanho[n].Trim()) - 1, '0');
                                        }
                                        #endregion
                                    }
                                    else if (formato[n] == "T")
                                        linha += dt.Rows[a][field[n]].ToString().PadRight(Convert.ToInt32(tamanho[n].Trim()), ' ');

                                    else if (formato[n] == "total")
                                        linha +=total.ToString().PadLeft(Convert.ToInt32(tamanho[n].Trim()), '0');

                                    else if (formato[n] == "Decimal")
                                    {
                                        #region "Decimal"
                                        decimal vlr = 0;
                                        if (dt.Rows[a][field[n]].ToString().Trim().Length > 0)
                                            vlr = Convert.ToDecimal(dt.Rows[a][field[n]].ToString().Replace('.', ','));
                                        if (vlr >= 0)
                                            linha += vlr.ToString().PadLeft(Convert.ToInt32(tamanho[n].Trim()), '0').Replace(',', '.');
                                        else
                                        {
                                            vlr = vlr * (-1);
                                            linha += "-" + vlr.ToString().PadLeft(Convert.ToInt32(tamanho[n].Trim()) - 1, '0').Replace(',', '.');
                                        }
                                        #endregion
                                    }
                                    else if (formato[n] == "Null")
                                    {
                                        #region "Null"
                                        if (dt.Rows[a][field[n]].ToString().Trim() != "")
                                        {
                                            if (Convert.ToInt32(dt.Rows[a][field[n]].ToString().Trim()) == 0)
                                                linha += " ".PadRight(Convert.ToInt32(tamanho[n].Trim()), ' ');
                                            else
                                            {
                                                decimal vlr = 0;
                                                if (dt.Rows[a][field[n]].ToString().Trim().Length > 0)
                                                    vlr = Convert.ToDecimal(dt.Rows[a][field[n]].ToString().Replace('.', ','));
                                                if (vlr >= 0)
                                                    linha += vlr.ToString().PadLeft(Convert.ToInt32(tamanho[n].Trim()), '0');
                                                else
                                                {
                                                    vlr = vlr * (-1);
                                                    linha += "-" + vlr.ToString().PadLeft(Convert.ToInt32(tamanho[n].Trim()) - 1, '0');
                                                }
                                            }
                                        }
                                        else
                                            linha += " ".PadRight(Convert.ToInt32(tamanho[n].Trim()), ' ');


                                        #endregion
                                    }
                                    else if (formato[n] == "num")
                                    {
                                        #region "num"
                                        decimal vlr = 0;
                                        if (dt.Rows[a][field[n]].ToString().Contains("."))
                                            vlr =
                                                Convert.ToInt32(
                                                    dt.Rows[a][field[n]].ToString().Split('.')[0].ToString());
                                        else
                                        {
                                            if (dt.Rows[a][field[n]].ToString().Trim().Length > 0)
                                                vlr = Convert.ToInt32(dt.Rows[a][field[n]].ToString());
                                        }

                                        if (vlr >= 0)
                                            linha += vlr.ToString().PadLeft(Convert.ToInt32(tamanho[n].Trim()), '0').Replace(',', '.');
                                        else
                                        {
                                            vlr = vlr * (-1);
                                            linha += "-" + vlr.ToString().PadLeft(Convert.ToInt32(tamanho[n].Trim()) - 1, '0').Replace(',', '.');
                                        }
                                        #endregion
                                    }
                                    else if (formato[n] == "D")
                                    {
                                        #region "D"
                                        if (Convert.ToString(dt.Rows[a][field[n]].ToString().Replace("/", "").PadRight(Convert.ToInt32(tamanho[n].Trim()), ' ')).Trim().Length <= 0)
                                        {
                                            string dataLeit = DateTime.Now.Day.ToString().PadLeft(2, '0')
                                                            + DateTime.Now.Month.ToString().PadLeft(2, '0')
                                                            + DateTime.Now.Year.ToString().PadLeft(4, '0');
                                            linha += dataLeit.PadRight(Convert.ToInt32(tamanho[n].Trim().Replace(":", "").Replace(" ", "")), ' ');
                                        }
                                        else if (dt.Rows[a][field[n]].ToString().Length > 10)
                                            linha += dt.Rows[a][field[n]].ToString().Split(' ')[1].Replace("/", "").Replace(":", "").Replace(" ", "") + dt.Rows[a][field[n]].ToString().Split(' ')[0].Replace("/", "").Replace(":", "").Replace(" ", "");
                                        else
                                              linha += dt.Rows[a][field[n]].ToString().Replace("/", "").Replace(":", "").Replace(" ", "").PadRight(Convert.ToInt32(tamanho[n].Trim()), ' ');
                                        #endregion
                                    }
                                    else if (formato[n] == "Data")
                                    {
                                        #region "Data"
                                        if (dt.Rows[a][field[n]].ToString().Trim().Length > 0)
                                        {
                                            linha += dt.Rows[a][field[n]].ToString().Trim().Replace("-", "").Replace("/", "").Substring(2, 2) + dt.Rows[a][field[n]].ToString().Trim().Replace("-", "").Replace("/", "").Substring(0, 2) + dt.Rows[a][field[n]].ToString().Trim().Replace("-", "").Replace("/", "").Substring(4, 4);
                                        }
                                        else
                                            linha += dt.Rows[a][field[n]].ToString().Replace("/", "").PadRight(Convert.ToInt32(tamanho[n].Trim()), ' ');
                                        #endregion
                                    }
                                    else if (formato[n] == "Hora")
                                    {
                                        #region "Data"
                                        if (dt.Rows[a][field[n]].ToString().Trim().Length > 0)
                                        {
                                            linha += dt.Rows[a][field[n]].ToString().Trim().Replace(":", "").Replace(" ", "");
                                        }
                                        else
                                            linha += dt.Rows[a][field[n]].ToString().Replace(":", "").Replace(" ", "").PadRight(Convert.ToInt32(tamanho[n].Trim()), ' ');
                                        #endregion
                                    }
                                    else if (formato[n] == "/100")
                                    {
                                        #region "/100"
                                        decimal ttmp = 0;

                                        if (dt.Rows[a][field[n]].ToString().Trim().Length > 0)
                                            ttmp = Convert.ToDecimal(dt.Rows[a][field[n]].ToString()) / 100;

                                        if (ttmp >= 0)
                                            linha += ttmp.ToString().PadLeft(Convert.ToInt32(tamanho[n].Trim()), '0');
                                        else
                                        {
                                            ttmp = ttmp * (-1);
                                            linha += "-" + ttmp.ToString().PadLeft(Convert.ToInt32(tamanho[n].Trim()) - 1, '0');
                                        }
                                        #endregion
                                    }
                                    else if (formato[n] == "*100")
                                    {
                                        #region "*100"
                                        string ttmp = "0";

                                        if (dt.Rows[a][field[n]].ToString().Trim().Length > 0)
                                            ttmp = (Convert.ToDecimal(dt.Rows[a][field[n]].ToString()) * 100).ToString().Replace(',', '.').Split('.')[0];

                                        if (Convert.ToDecimal(ttmp) >= 0)
                                            linha += ttmp.ToString().PadLeft(Convert.ToInt32(tamanho[n].Trim()), '0');
                                        else
                                        {
                                            ttmp = (Convert.ToDecimal(ttmp) * (-1)).ToString();
                                            linha += "-" + ttmp.ToString().PadLeft(Convert.ToInt32(tamanho[n].Trim()) - 1, '0');
                                        }
                                        #endregion
                                    }
                                    else if (formato[n] == "Right")
                                    {
                                        #region "Right"
                                        if (dt.Rows[a][field[n]].ToString().Length > 0)
                                        {
                                            int dife = dt.Rows[a][field[n]].ToString().Length - Convert.ToInt32(tamanho[n].Trim());
                                            linha += dt.Rows[a][field[n]].ToString().Substring(dife, dt.Rows[a][field[n]].ToString().Length - dife);
                                        }
                                        else
                                            linha += dt.Rows[a][field[n]].ToString().PadLeft(Convert.ToInt32(tamanho[n].Trim(), ' '));
                                        #endregion
                                    }
                                    else if (formato[n].Contains("Left-"))
                                    {
                                        #region "Left-"
                                        if (dt.Rows[a][field[n]].ToString().Length > 0)
                                        {
                                            int ret = Convert.ToInt32(formato[n].Split('-')[1].ToString());
                                            int dife = dt.Rows[a][field[n]].ToString().Length - ret;

                                            if (formato[n].Contains("N"))
                                                linha += dt.Rows[a][field[n]].ToString().Substring(0, dife).PadLeft(Convert.ToInt32(tamanho[n].Trim()), '0');
                                            else
                                                linha += dt.Rows[a][field[n]].ToString().Substring(0, dife).PadRight(Convert.ToInt32(tamanho[n].Trim()), ' ');
                                        }
                                        else
                                        {
                                            if (formato[n].Contains("N"))
                                                linha += dt.Rows[a][field[n]].ToString().PadLeft(Convert.ToInt32(tamanho[n].Trim(), '0'));
                                            else
                                                linha += dt.Rows[a][field[n]].ToString().PadRight(Convert.ToInt32(tamanho[n].Trim(), ' '));
                                        }
                                        #endregion
                                    }
                                    else if ((formato[n] == "RightDate") && (tamanho[n] == "6"))
                                    {
                                        #region "RightDate"
                                        if (dt.Rows[a][field[n]].ToString().Length > 0)
                                        {
                                            int dife = dt.Rows[a][field[n]].ToString().Length - 8;
                                            string tpm = dt.Rows[a][field[n]].ToString().Substring(dife, dt.Rows[a][field[n]].ToString().Length - dife);
                                            tpm = tpm.Substring(0, 4) + tpm.Substring(6, 2);
                                            linha += tpm;
                                        }
                                        else
                                            linha += dt.Rows[a][field[n]].ToString().PadLeft(Convert.ToInt32(tamanho[n].Trim()), ' ');
                                        #endregion
                                    }
                                    else if (formato[n].Contains("Right"))
                                    {
                                        #region "Right"
                                        if (dt.Rows[a][field[n]].ToString().Length > 0)
                                        {
                                            int ctr = dt.Rows[a][field[n]].ToString().Length;

                                            if (formato[n].Contains("("))
                                                ctr = Convert.ToInt32(formato[n].Split('(')[1].ToString().Replace(")", ""));

                                            if (dt.Rows[a][field[n]].ToString().Length > ctr)
                                                ctr = dt.Rows[a][field[n]].ToString().Length - ctr;
                                            else
                                                ctr = dt.Rows[a][field[n]].ToString().Length;

                                            if (ctr > Convert.ToInt32(tamanho[n].Trim()))
                                                ctr = Convert.ToInt32(tamanho[n].Trim());

                                            if (formato[n].Contains("N"))
                                                linha += dt.Rows[a][field[n]].ToString().Substring(ctr).PadRight(Convert.ToInt32(tamanho[n].Trim()), '0');
                                            else
                                                linha += dt.Rows[a][field[n]].ToString().Substring(ctr).PadLeft(Convert.ToInt32(tamanho[n].Trim()), ' ');

                                        }
                                        else
                                        {
                                            if (formato[n].Contains("N"))
                                                linha += dt.Rows[a][field[n]].ToString().PadLeft(Convert.ToInt32(tamanho[n].Trim()), '0');
                                            else
                                                linha += dt.Rows[a][field[n]].ToString().PadRight(Convert.ToInt32(tamanho[n].Trim()), ' ');
                                        }
                                        #endregion
                                    }
                                    else if (formato[n].Contains("Left"))
                                    {
                                        #region "Left"
                                        if (dt.Rows[a][field[n]].ToString().Length > 0)
                                        {
                                            int ctr = dt.Rows[a][field[n]].ToString().Length;

                                            if (formato[n].Contains("("))
                                                ctr = Convert.ToInt32(formato[n].Split('(')[1].ToString().Replace(")", ""));

                                            if (dt.Rows[a][field[n]].ToString().Length < ctr)
                                                ctr = dt.Rows[a][field[n]].ToString().Length;

                                            if (ctr > Convert.ToInt32(tamanho[n].Trim()))
                                                ctr = Convert.ToInt32(tamanho[n].Trim());

                                            string dtLeit = dt.Rows[a][field[n]].ToString().Substring(0, ctr).PadLeft(Convert.ToInt32(tamanho[n].Trim()), ' ');
                                            if (formato[n].Contains("N"))
                                                dtLeit = dt.Rows[a][field[n]].ToString().Substring(0, ctr).PadRight(Convert.ToInt32(tamanho[n].Trim()), '0');

                                            if (formato[n].Contains("Date"))
                                            {
                                                if (dtLeit.Substring(dtLeit.Length - 2, 2) == "00")
                                                    dtLeit = dtLeit.Substring(0, 4) + "20" + dtLeit.Substring(4, 2);
                                            }
                                            linha += dtLeit;
                                        }
                                        else
                                        {
                                            if (formato[n].Contains("N"))
                                                linha += dt.Rows[a][field[n]].ToString().PadLeft(Convert.ToInt32(tamanho[n].Trim()), '0');
                                            else
                                                linha += dt.Rows[a][field[n]].ToString().PadRight(Convert.ToInt32(tamanho[n].Trim()), ' ');
                                        }
                                        #endregion
                                    }
                                    else if (formato[n].Contains("Seq"))
                                    {
                                        #region "Seq"
                                        int b = a + 1;
                                        linha += b.ToString().PadLeft(Convert.ToInt32(tamanho[n]), '0');

                                        #endregion
                                    }
                                    else if (formato[n].Contains("Lado"))
                                    {
                                        #region "Lado"
                                        linha += "1";

                                        #endregion
                                    }
                                    else if (formato[n] == "S/N")
                                    {
                                        #region "S/N"
                                        if (dt.Rows[a][field[n]].ToString().Trim().Length == 0)
                                            linha += "N";
                                        else
                                            linha += dt.Rows[a][field[n]].ToString().Trim();
                                        #endregion
                                    }
                                    else if ((formato[n].Substring(0, 1) == "P") && (formato[n].Split('.').Length > 1))
                                        linha += dt.Rows[a][field[n]].ToString().Substring(Convert.ToInt32(formato[n].Split('.')[1]), Convert.ToInt32(tamanho[n]));
                                    else
                                        linha += dt.Rows[a][field[n]].ToString();

                                    if (formato[n] == "num")
                                    {
                                        if (Convert.ToInt32(tamanho[n].Trim()) < dt.Rows[a][field[n]].ToString().Split('.')[0].Trim().Length)
                                        {
                                            if ((!formato[n].Contains("Hora")) && (!formato[n].Contains("Data")) && (!formato[n].Contains("Right")) && (formato[n].Substring(0, 1) != "P") && (!formato[n].Contains("Left")))
                                            {
                                                int difff = dt.Rows[a][field[n]].ToString().Length - Convert.ToInt32(tamanho[n].Trim());
                                                if (linha.Length > difff)
                                                    linha = linha.Substring(0, (linha.Length - difff));
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (Convert.ToInt32(tamanho[n].Trim()) < dt.Rows[a][field[n]].ToString().Trim().Length)
                                        {
                                            if ((!formato[n].Contains("Hora")) && (!formato[n].Contains("Data")) && (!formato[n].Contains("Right")) && (formato[n].Substring(0, 1) != "P") && (!formato[n].Contains("Left")) && (!formato[n].Contains("D")))
                                            {
                                                int difff = dt.Rows[a][field[n]].ToString().Length - Convert.ToInt32(tamanho[n].Trim());
                                                if (linha.Length > difff)
                                                    linha = linha.Substring(0, (linha.Length - difff));
                                            }
                                        }
                                    }



                                }
                                else
                                {
                                    if (formato[n] == "N")
                                        linha += valor[n].ToString().PadLeft(Convert.ToInt32(tamanho[n].Trim()), '0');
                                    else
                                        linha += valor[n].ToString().PadRight(Convert.ToInt32(tamanho[n].Trim()), ' ');
                                }
                            }                                
                            
                        }

                        #endregion

                        linha = linha.ToUpper();
                        linha = linha.Replace("À", "A").Replace("Á", "A").Replace("Ã", "A").Replace("Â", "A").Replace("È", "E").Replace("É", "E").Replace("Ê", "E").Replace("Ì", "I").Replace("Í", "I").Replace("Ò", "O").Replace("Ó", "O").Replace("Õ", "O").Replace("Ô", "O").Replace("Ù", "U").Replace("Ú", "U").Replace("Û", "U").Replace("Ç", "C");

                        if (linha.IndexOf("#") > 0)
                        {
                            for (int s = 0; s < linha.Split('#').Length - 1; s++)
                            {
                                sw.WriteLine(linha.Split(Convert.ToChar("#"))[s] + "#");
                            }
                        }
                        else
                        {
                            sw.WriteLine(linha);
                        }

                        if(tabela == "WTR_LIVROS")
                            a =dt.Rows.Count;
                    }

                    sw.Close();
                }
                #endregion


                i++;

            }

            if (!sw.Equals(StreamWriter.Null))
                sw.Close();

            //zip files           
            CriarArquivoZip(cam, file);

            btDownload.Enabled = true;
            btDownload.Text = "Download: " + file + ".zip";

            //Move nome arquivo para o outro list de backup


            if (!sw.Equals(StreamWriter.Null))
                sw.Close();
            #endregion
            #region Exibe Quantidade de Ocorrências
            if (dt2.DefaultView.Count > 0)
            {
                string msgOcorrencia = "Cod.Ocorrência\tQuantidade\r\n";
                for (int i2 = 0; i2 < dt2.DefaultView.Count; i2++)
                {
                    msgOcorrencia += "\r\n" + dt2.DefaultView.Table.DefaultView[i2].Row["MSG_ID"].ToString()
                        + "\t\t" + dt2.DefaultView.Table.DefaultView[i2].Row["QTD"].ToString();
                }
                msg += (msgOcorrencia + "Leiturista: " + dt2.DefaultView.Table.DefaultView[0].Row["LTR_ID"].ToString());
            }
            #endregion
            return msg;
        }
        catch (Exception ex)
        {
            string resp = ("Erro :" + " - " + ex.Message);
            msg += resp;
            return msg;
        }
        finally
        {
            sw.Close();
        }

    }

    public static void CriarArquivoZip(string ArquivoDestino, string file)
    {
        string filepath = "";
        string filezip = ArquivoDestino + @"\" + "zip";

        using (ZipFile zip = new ZipFile())
        {
            // percorre todos os arquivos da lista
            for (int i = 0; i < 2; i++)
            {
                if (i == 0)
                    filepath = ArquivoDestino + @"\" + "LIVRO_" + file + ".txt";
                else
                    filepath = ArquivoDestino + @"\" + "LEITURA_" + file + ".txt";
                // se o item é um arquivo
                if (File.Exists(filepath))
                {
                    try
                    {
                        // Adiciona o arquivo na pasta raiz dentro do arquivo zip
                        zip.AddFile(filepath, "");
                    }
                    catch
                    {
                        throw;
                    }
                }
                // se o item é uma pasta
                else if (Directory.Exists(filepath))
                {
                    try
                    {
                        // Adiciona a pasta no arquivo zip com o nome da pasta 
                        zip.AddDirectory(filepath, new DirectoryInfo(filepath).Name);
                    }
                    catch
                    {
                        throw;
                    }
                }
            }
            // Salva o arquivo zip para o destino
            try
            {
                zip.Save(ArquivoDestino + @"\" + file + ".zip");
            }
            catch
            {
                throw;
            }
        }
    }


    public static string ReturnFiletype(string fileExtension)
    {
        switch (fileExtension)
        {
            case ".htm":
            case ".html":
            case ".log":
                return "text/HTML";
            case ".txt":
                return "text/plain";
            case ".doc":
                return "application/ms-word";
            case ".tiff":
            case ".tif":
                return "image/tiff";
            case ".asf":
                return "video/x-ms-asf";
            case ".avi":
                return "video/avi";
            case ".zip":
                return "application/zip";
            case ".xls":
            case ".csv":
                return "application/vnd.ms-excel";
            case ".gif":
                return "image/gif";
            case ".jpg":
            case "jpeg":
                return "image/jpeg";
            case ".bmp":
                return "image/bmp";
            case ".wav":
                return "audio/wav";
            case ".mp3":
                return "audio/mpeg3";
            case ".mpg":
            case "mpeg":
                return "video/mpeg";
            case ".rtf":
                return "application/rtf";
            case ".asp":
                return "text/asp";
            case ".pdf":
                return "application/pdf";
            case ".fdf":
                return "application/vnd.fdf";
            case ".ppt":
                return "application/mspowerpoint";
            case ".dwg":
                return "image/vnd.dwg";
            case ".msg":
                return "application/msoutlook";
            case ".xml":
            case ".sdxl":
                return "application/xml";
            case ".xdp":
                return "application/vnd.adobe.xdp+xml";
            default:
                return "application/octet-stream";
        }
    }

    private void downloadTxt(string arquivo, string nomeArq, HttpResponse response)
    {
        try
        {

            //Response.ContentType = "application/octet-stream";
            //Response.AppendHeader("Content-Disposition", String.Format("attachment; filename={0}", nomeArq));
            //Response.TransmitFile(arquivo);
            //HttpContext.Current.ApplicationInstance.CompleteRequest();

            // Get the physical Path of the file
            string filepath = arquivo;

            // Create New instance of FileInfo class to get the properties of the file being downloaded
            FileInfo file = new FileInfo(filepath);

            // Checking if file exists
            if (file.Exists)
            {
                // Clear the content of the response
                response.ClearContent();

                // LINE1: Add the file name and attachment, which will force the open/cance/save dialog to show, to the header
                response.AddHeader("Content-Disposition", String.Format("attachment; filename={0}", file.Name));

                // Add the file size into the response header
                response.AddHeader("Content-Length", file.Length.ToString());

                // Set the ContentType
                response.ContentType = ReturnFiletype(file.Extension.ToLower());

                // Write the file into the response (TransmitFile is for ASP.NET 2.0. In ASP.NET 1.1 you have to use WriteFile instead)
                response.TransmitFile(file.FullName);

                // End the response
                response.End();

                //send statistics to the class
            }
        }
        catch (Exception ex)
        {
            // msg("Erro na download do arquivo: " + ex.Message);
        }
    }

}





