﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using WaterSyncLite.Class;
using WaterSyncLite.DAL;
using WaterSyncLite.MDL;

/// <summary>
/// Summary description for Marilia
/// </summary>
public class Marilia
{
    public Marilia()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    int cat = 0;
    int idFaixa = 0;

    String msg = "";
    string cliente = "05";
    string empresa = "01";

    public int Import(string cam, string file, string rota, string mes, string ano, string caminhoProjeto, string leiturista, String strCon, TextBox txtStatus)
    {

        #region Variaveis
        string command = "insert";
        string separador = xml.GetConfigValue(caminhoProjeto, "LOCAL", "SEPARADOR", cliente);
        Data db = new Data();
        int countLeit = 0;              //Controle de leituras para geração do Livro
        Import import2;
        ImportDAL impDAL = new ImportDAL();
        Import import = new Import();
        String conexaoStr = strCon;

        //string importLivro = xml.GetImportValue("IMPORT", "LIVRO");
        Import importLeitura = impDAL.getImp(Convert.ToInt32(Convert.ToInt32(cliente)), "LEITURA");
        //string importTarifa = xml.GetImportValue("IMPORT", "TARIFA");
        Import importServico = impDAL.getImp(Convert.ToInt32(Convert.ToInt32(cliente)), "SERVICO");
        Import importMsg = impDAL.getImp(Convert.ToInt32(Convert.ToInt32(cliente)), "MENSAGENS");
        Import importFaixa = impDAL.getImp(Convert.ToInt32(Convert.ToInt32(cliente)), "FAIXA");
        Import importCategoria = impDAL.getImp(Convert.ToInt32(Convert.ToInt32(cliente)), "CATEGORIA");
        Import importFatura = impDAL.getImp(Convert.ToInt32(Convert.ToInt32(cliente)), "FATURA");
        Import importTarifa = impDAL.getImp(Convert.ToInt32(Convert.ToInt32(cliente)), "TARIFA");
        Import importParametros = impDAL.getImp(Convert.ToInt32(Convert.ToInt32(cliente)), "PARAMETROS");
        //string importUpdLivro = xml.GetImportValue("IMPORT", "LIVRO");
        //string importCarne = xml.GetImportValue("IMPORT", "CARNE");
        Import importIndice = impDAL.getImp(Convert.ToInt32(Convert.ToInt32(cliente)), "INDICE");




        string FAT_CATEGORIA = "";
        string FAT_ECONOMIA = "";
        string FAT_SETOR = "";
        string FAT_QTDDEBITOSABERTO = "";
        //string FAT_SITUACAOLEIT="";
        string FAT_CAT1 = "";
        string FAT_ECON1 = "";
        string FAT_VENCIMENTO = "";
        string FAT_DIGITOCONTA = "";
        string FAT_DIGITOLIGACAO = "";
        string FAT_DESCONTOTARIFASOCIAL = "";
        string FAT_SITUACAOAGUA = "";
        string FAT_ANOEXTRATO = "";
        string FAT_SITUACAOESGOTO = "";
        string FAT_CONDOMINIO = "";
        string FAT_CODATIVIDADE = "";
        string FAT_LANCAMENTO = "";
        string FAT_HIDROMETRACAO = "";
        string FAT_CONSUMOFIXO = "";
        string FAT_PROPRIETARIO = "";
        string FAT_EXCESSO = "";
        string FAT_LOTE = "";
        string FAT_PRINCIPALCONDOMINIO = "";
        string FAT_COBRANCA = "";
        string FAT_NUMMED = "";
        string FAT_CONSUMIDOR = "";
        string FAT_ENDERECO = "";
        string FAT_NUMERO = "";
        string FAT_COMPLEMENTO = "";
        string FAT_BAIRRO = "";
        string FAT_ENDERECOENTREGA = "";
        string FAT_NUMEROENTREGA = "";
        string FAT_COMPLEMENTOENTREGA = "";
        string FAT_BAIRROENTREGA = "";
        string FAT_BANCO = "";
        string FAT_CONTACORRENTE = "";
        string FAT_AGENCIA = "";
        string FAT_ZONA = "";
        string FAT_QUADRA = "";
        string FAT_CEP = "";
        string FAT_CEPENTREGA = "";

        string lei_anterior = "";
        string lei_maximo = "";
        string lei_minimo = "";
        string lei_media = "";
        string lei_residuotroca = "";
        string lei_dataleituraanterior = "";
        string lei_periodo1 = "";
        string lei_valor1 = "";
        string lei_periodo2 = "";
        string lei_valor2 = "";
        string lei_periodo3 = "";
        string lei_valor3 = "";
        string lei_periodo4 = "";
        string lei_valor4 = "";
        string lei_periodo5 = "";
        string lei_valor5 = "";
        string lei_periodo6 = "";
        string lei_valor6 = "";
        string lei_periodo7 = "";
        string lei_valor7 = "";
        string lei_periodo8 = "";
        string lei_valor8 = "";



        FileStream ofs = File.Open(cam + file, FileMode.Open, FileAccess.Read, FileShare.None);
        StreamReader sr = new StreamReader(ofs, System.Text.Encoding.GetEncoding("ISO-8859-1"));	//Aceita caracteres especiais.
        string linha = string.Empty;
        int i = 0;
        //string empresa = cbxEmpresa.SelectedValue.ToString().ToString();
        string sql = "";

        #endregion

        #region Mensagens - OBS - DAEM
        string obsMsg = string.Empty;

        if (File.Exists(cam + "Mensagens.txt"))
        {
            FileStream ofObs = File.Open(cam + "Mensagens.txt", FileMode.Open, FileAccess.Read, FileShare.None);
            StreamReader srObs = new StreamReader(ofObs, System.Text.Encoding.GetEncoding("ISO-8859-1"));	//Aceita caracteres especiais.
            obsMsg = srObs.ReadLine();
            obsMsg = obsMsg.Substring(3);
        }
        #endregion

        //DELETAR DADOS DAS TABELAS
        //WTR_HISTORICO
        //WTR_CEP
        //WTR_BAIRRO
        //WTR_RUA
        deletarDadosAntigosMarilia(file.ToString().Substring(file.ToString().Length - 7, 3), ano, mes, strCon);
        string sqlAux = "";
        try
        {
            //deleta indices 
            if (file.Substring(0, 3).ToString().Trim().ToUpper() == "QUA")
                db.Exe(conexaoStr, "WTR_INDICE", "DELETE FROM WTR_INDICE");
            string msg1 = "", msg2 = "", msg3 = "", febraban = "", livSeq = "", grupo = "";

            while (sr.Peek() >= 0)
            {
                i++;

                //lblCount.Refresh();
                linha = string.Empty;
                linha = sr.ReadLine();

                #region Tratamento da linha
                //Retira o separador de campo - Na importação não deve ter.
                if (separador.Trim().Length < 1)
                    linha = linha.Replace("|", " ").Replace(";", " ");    //Alterado para permitir ponto e vírgula do Cadastro. 10/12/2009

                if (linha.Length < 141)
                    linha = linha.PadRight(141, ' ');

                linha = linha.Replace("'", " ");		//Alteração feita em 07Dez2004 - Evita erro de SQL com apóstofro - Padovani.
                linha = linha.Replace("\r\n", " ");	//Alteração feita em 24Mar2005 - Evita erro com <Enter> - Padovani.

                if (Convert.ToInt32(cliente) == 5)
                    linha = linha.Replace("S/N", "000");
                #endregion

                int len = linha.Length;
                string reg = linha.Substring(0, 1);

                #region Leitura de Conteúdo do Arquivo
                string[] campos;
                command = "insert";
                campos = null;
                #region Identificador da Linha
                //if ((file.Substring(0, 5) == "CARNE") || (file.Substring(0, 5) == "Carne") || (file.Substring(0, 5) == "carne"))
                //{
                //    campos = importCarne.Split('|');
                //}
                //else
                //{


                if (linha.Substring(0, 1) == "6")
                    campos = importTarifa.Campos.Split('|');
                else
                    if (linha.Substring(0, 1) == "3")
                    {
                        db.Exe(conexaoStr, "WTR_FAIXA", "DELETE FROM WTR_FAIXA WHERE FXA_MES = " + mes + " AND FXA_ANO = " + ano + " AND FXA_ID= 1");
                        campos = importServico.Campos.Split('|');
                    }
                    //else
                    //    if (linha.Substring(0, 1) == "D")
                    //        campos = importCategoria.Split('|');
                    else if (linha.Substring(0, 1) == "7")
                    {
                        campos = importFatura.Campos.Split('|');
                        lei_anterior = linha.Substring(10, 7).Trim();
                        lei_maximo = linha.Substring(24, 7).Trim();
                        lei_minimo = linha.Substring(17, 7).Trim();
                        lei_media = linha.Substring(31, 7).Trim();
                        lei_residuotroca = linha.Substring(54, 7).Trim();
                        lei_dataleituraanterior = linha.Substring(38, 8).Trim();
                        lei_periodo1 = linha.Substring(77, 7).Trim();
                        lei_valor1 = linha.Substring(68, 7).Trim();
                        lei_periodo2 = linha.Substring(93, 7).Trim();
                        lei_valor2 = linha.Substring(84, 7).Trim();
                        lei_periodo3 = linha.Substring(109, 7).Trim();
                        lei_valor3 = linha.Substring(100, 7).Trim();
                        lei_periodo4 = linha.Substring(125, 7).Trim();
                        lei_valor4 = linha.Substring(116, 7).Trim();
                        lei_periodo5 = linha.Substring(141, 7).Trim();
                        lei_valor5 = linha.Substring(132, 7).Trim();
                        lei_periodo6 = linha.Substring(157, 7).Trim();
                        lei_valor6 = linha.Substring(148, 7).Trim();
                        lei_periodo7 = linha.Substring(173, 7).Trim();
                        lei_valor7 = linha.Substring(164, 7).Trim();
                        lei_periodo8 = linha.Substring(189, 7).Trim();
                        lei_valor8 = linha.Substring(180, 7).Trim();

                    }
                    //else
                    //    if (linha.Substring(0, 1) == "4")
                    //        campos = importMsg.Split('|');
                    else if (linha.Substring(0, 1) == "5")
                    {
                        rota = linha.Substring(540, 4).Trim();
                        countLeit++;
                        campos = importLeitura.Campos.Split('|');

                        FAT_CATEGORIA = linha.Substring(246, 2).Trim();
                        FAT_ECONOMIA = linha.Substring(248, 4).Trim();
                        FAT_SETOR = linha.Substring(233, 4).Trim().Replace("SN", "0").Replace("S/N", "0").Replace("sn", "0").Replace("s/n", "0");
                        FAT_QUADRA = linha.Substring(562, 6).Trim().Replace("SN", "0").Replace("S/N", "0").Replace("sn", "0").Replace("s/n", "0");
                        FAT_QTDDEBITOSABERTO = linha.Substring(493, 4).Trim();
                        //FAT_SITUACAOLEIT = linha.Substring(540, 4).Trim();
                        FAT_DIGITOLIGACAO = linha.Substring(10, 2).Trim();
                        FAT_CAT1 = linha.Substring(252, 2).Trim();
                        FAT_ECON1 = linha.Substring(254, 4).Trim();
                        FAT_VENCIMENTO = linha.Substring(409, 8).Trim();
                        FAT_DIGITOCONTA = linha.Substring(497, 2).Trim();
                        FAT_DESCONTOTARIFASOCIAL = linha.Substring(672, 10).Trim();
                        FAT_SITUACAOAGUA = linha.Substring(244, 1).Trim();
                        FAT_ANOEXTRATO = linha.Substring(742, 4).Trim();
                        FAT_SITUACAOESGOTO = linha.Substring(245, 1).Trim();
                        FAT_CONDOMINIO = linha.Substring(631, 4).Trim();
                        FAT_CODATIVIDADE = linha.Substring(737, 3).Trim();
                        FAT_LANCAMENTO = linha.Substring(431, 2).Trim();
                        FAT_HIDROMETRACAO = linha.Substring(408, 1).Trim();
                        FAT_CONSUMOFIXO = linha.Substring(417, 7).Trim();
                        FAT_PROPRIETARIO = linha.Substring(166, 50).Trim();
                        FAT_EXCESSO = linha.Substring(424, 7).Trim();
                        FAT_LOTE = linha.Substring(556, 6).Trim().Replace("SN", "0").Replace("S/N", "0").Replace("sn", "0").Replace("s/n", "0");
                        FAT_PRINCIPALCONDOMINIO = linha.Substring(630, 1).Trim();
                        FAT_COBRANCA = linha.Substring(524, 1).Trim();
                        FAT_NUMMED = linha.Substring(216, 16).Trim();
                        FAT_CONSUMIDOR = linha.Substring(116, 50).Trim();
                        FAT_ENDERECO = linha.Substring(12, 54).Trim();
                        FAT_NUMERO = linha.Substring(237, 5).Trim().Replace("SN", "0").Replace("S/N", "0").Replace("sn", "0").Replace("s/n", "0");
                        FAT_COMPLEMENTO = linha.Substring(96, 20).Trim();
                        FAT_BAIRRO = linha.Substring(66, 30).Trim();
                        FAT_ENDERECOENTREGA = linha.Substring(273, 54).Trim();
                        FAT_NUMEROENTREGA = linha.Substring(357, 5).Trim();
                        FAT_COMPLEMENTOENTREGA = linha.Substring(362, 20).Trim();
                        FAT_BAIRROENTREGA = linha.Substring(327, 30).Trim();
                        FAT_BANCO = linha.Substring(270, 3).Trim();
                        FAT_CONTACORRENTE = linha.Substring(525, 15).Trim();
                        FAT_AGENCIA = linha.Substring(404, 4).Trim();
                        FAT_ZONA = linha.Substring(626, 4).Trim();
                        FAT_CEP = linha.Substring(656, 8).Trim();
                        FAT_CEPENTREGA = linha.Substring(664, 8).Trim();

                    }
                    else if (linha.Substring(0, 1) == "F")
                    {
                        campos = importIndice.Campos.Split('|');
                    }
                    else if (linha.Substring(0, 1) == "2")
                        campos = importFaixa.Campos.Split('|');
                    else if (linha.Substring(0, 1) == "B")
                        campos = importParametros.Campos.Split('|');
                    else if (linha.Substring(0, 1) == "1")
                    {
                        msg1 = linha.Substring(43, 70).Trim();
                        msg2 = linha.Substring(113, 70).Trim();
                        msg3 = linha.Substring(183, 70).Trim();
                        febraban = linha.Substring(15, 4).Trim();
                        livSeq = linha.Substring(1, 4).Trim();
                        grupo = linha.Substring(5, 4).Trim();
                        campos = null;
                        continue;

                    }
                    //else if (linha.Substring(0, 1) == "7")
                    //{
                    //    campos = importLeitura.Split('|');
                    //    command = "update";
                    //}
                    else
                    {
                        campos = null;
                        continue;
                    }
                //}
                #endregion

                string tabela = campos[0];

                string[] field = new string[campos.Length - 1];

                string[] valor = new string[campos.Length - 1];

                int p = 0;
                while (p < (campos.Length - 1))
                {
                    field[p] = campos[p + 1].Split(';')[0].Trim();

                    if (separador.Trim().Length > 0)
                    {
                        char separa = Convert.ToChar(separador.Trim());

                        if (campos[p + 1].Split(';').Length > 3)
                        {
                            valor[p] = campos[p + 1].Split(';')[3].Trim().Replace('"', ' ');
                            valor[p] = valor[p].Trim();

                            if (valor[p].Trim() == "seq")
                                valor[p] = i.ToString();
                            else if (valor[p].Trim() == ".leiturista")
                                valor[p] = leiturista;
                            else if (campos[p + 1].Split(';')[3].Trim() == ".mes")
                                valor[p] = mes;
                            else if (campos[p + 1].Split(';')[3].Trim() == ".ano")
                                valor[p] = ano;

                        }
                        else
                            valor[p] = linha.Split(separa)[p].ToString();

                        if (campos[p + 1].Split(';').Length > 2)
                        {
                            if (Convert.ToInt32(campos[p + 1].Split(';')[2].ToString().Trim()) > 0)
                                valor[p] = valor[p].Substring(0, Convert.ToInt32(campos[p + 1].Split(';')[2].ToString().Trim()));
                        }
                    }
                    else
                    {
                        if (campos[p + 1].Split(';').Length > 3)	//Valor fixo ou Campo Formatado
                        {

                            valor[p] = linha.Substring(Convert.ToInt32(campos[p + 1].Split(';')[1].Trim()), Convert.ToInt32(campos[p + 1].Split(';')[2].Trim()));
                            valor[p] = TrataCampoMarilia(campos[p + 1].Split(';')[3].Trim(), valor[p], tabela, countLeit, i, rota, leiturista, mes, ano);
                        }
                        else
                            valor[p] = linha.Substring(Convert.ToInt32(campos[p + 1].Split(';')[1].Trim()), Convert.ToInt32(campos[p + 1].Split(';')[2].Trim()));
                    }



                    p++;
                }

                #endregion



                #region Comando SQL para o BANCO DE DADOS

                #region Command INSERT
                if (command == "insert")
                {
                    sql = "INSERT INTO " + tabela + " (";

                    p = 0;
                    while (p < field.Length)
                    {
                        if (p > 0)
                        {
                            if (field[p - 1].Trim() != field[p].Trim())
                            {
                                if (field[p].IndexOf("#") > 0)
                                    field[p] = field[p].Substring(field[p].IndexOf("#") + 1);

                                sql += field[p].Trim() + ",";
                            }
                        }
                        else
                        {
                            if (field[p].IndexOf("#") > 0)
                                field[p] = field[p].Substring(field[p].IndexOf("#") + 1);

                            sql += field[p].Trim() + ",";
                        }
                        p++;
                    }

                    sql = sql.Remove(sql.Length - 1, 1);
                    if (tabela == "WTR_FATURA")
                        sql += ",FAT_CATEGORIA,FAT_ECONOMIA,FAT_SETOR,FAT_QTDDEBITOSABERTO,FAT_CAT1,FAT_ECON1,FAT_VENCIMENTO,FAT_DIGITOCONTA,FAT_DESCONTOTARIFASOCIAL,FAT_SITUACAOAGUA,FAT_SITUACAOESGOTO,FAT_CONDOMINIO,FAT_CODATIVIDADE,FAT_LANCAMENTO,FAT_HIDROMETRACAO,FAT_CONSUMOFIXO,FAT_PROPRIETARIO,FAT_EXCESSO,FAT_LOTE,FAT_PRINCIPALCONDOMINIO,FAT_COBRANCA,FAT_NUMMED,FAT_CONSUMIDOR,FAT_ENDERECO,FAT_NUMERO,FAT_COMPLEMENTO,FAT_BAIRRO,FAT_ENDERECOENTREGA,FAT_NUMEROENTREGA,FAT_COMPLEMENTOENTREGA,FAT_BAIRROENTREGA,FAT_BANCO,FAT_CONTACORRENTE,FAT_AGENCIA,FAT_ZONA,FAT_QUADRA,FAT_CEP,FAT_CEPENTREGA,FAT_DIGITOLIGACAO,FAT_ANOEXTRATO";
                    else if (tabela == "WTR_LEITURAS")
                        sql += ",LEI_ANTERIOR,LEI_MEDIA,LEI_MAXIMO,LEI_MINIMO,LEI_DATALEITURAANTERIOR,LEI_RESIDUOTROCA,lei_periodo1,lei_valor1,lei_periodo2,lei_valor2,lei_periodo3,lei_valor3,lei_periodo4,lei_valor4,lei_periodo5,lei_valor5,lei_periodo6,lei_valor6,lei_periodo7,lei_valor7,lei_periodo8,lei_valor8";

                    sql += ") VALUES ('";

                    p = 0;
                    while (p < valor.Length)
                    {
                        if (p > 0)
                        {
                            if (field[p - 1].Trim() == field[p].Trim())
                                sql = sql.Remove(sql.Length - 3, 3);

                            sql += valor[p].Trim() + "','";
                        }
                        else
                        {
                            sql += valor[p].Trim() + "','";
                        }

                        p++;
                    }

                    sql = sql.Remove(sql.Length - 2, 2);
                    if (tabela == "WTR_FATURA")
                        sql += "," + "'" + FAT_CATEGORIA + "'" + ",'" + FAT_ECONOMIA + "'" + ",'" + FAT_SETOR + "'" + ",'" + FAT_QTDDEBITOSABERTO + "'" + ",'" + FAT_CAT1 + "'" + ",'" + FAT_ECON1 + "'" + ",'" + FAT_VENCIMENTO + "'" + ",'" + FAT_DIGITOCONTA + "'" + ",'" + FAT_DESCONTOTARIFASOCIAL + "'" + ",'" + FAT_SITUACAOAGUA + "'" + ",'" + FAT_SITUACAOESGOTO + "'" + ",'" + FAT_CONDOMINIO + "'" + ",'" + FAT_CODATIVIDADE + "'" + ",'" + FAT_LANCAMENTO + "'" + ",'" + FAT_HIDROMETRACAO + "'" + ",'" + FAT_CONSUMOFIXO + "'" + ",'" + FAT_PROPRIETARIO + "'" + ",'" + FAT_EXCESSO + "'" + ",'" + FAT_LOTE + "'" + ",'" + FAT_PRINCIPALCONDOMINIO + "'" + ",'" + FAT_COBRANCA + "'" + ",'" + FAT_NUMMED + "'" + ",'" + FAT_CONSUMIDOR + "'" + ",'" + FAT_ENDERECO + "'" + ",'" + FAT_NUMERO + "'" + ",'" + FAT_COMPLEMENTO + "'" + ",'" + FAT_BAIRRO + "'" + ",'" + FAT_ENDERECOENTREGA + "'" + ",'" + FAT_NUMEROENTREGA + "'" + ",'" + FAT_COMPLEMENTOENTREGA + "'" + ",'" + FAT_BAIRROENTREGA + "'" + ",'" + FAT_BANCO + "'" + ",'" + FAT_CONTACORRENTE + "'" + ",'" + FAT_AGENCIA + "'" + ",'" + FAT_ZONA + "'" + ",'" + FAT_QUADRA + "'" + ",'" + FAT_CEP + "'" + ",'" + FAT_CEPENTREGA + "'" + ",'" + FAT_DIGITOLIGACAO + "'" + ",'" + FAT_ANOEXTRATO + "'";


                    sql += ")";
                }
                #endregion

                #region Command UPDATE
                if (command == "update")
                {
                    string sqlWhere = "";

                    sql = "UPDATE " + tabela + " set ";

                    p = 0;
                    while (p < field.Length)
                    {
                        if (field[p] != null)
                        {
                            if (field[p].Substring(0, 1) == linha.Substring(0, 1))
                            {
                                if (field[p].IndexOf("#") > 0)
                                    field[p] = field[p].Substring(field[p].IndexOf("#") + 1);

                                sql += field[p].Trim() + "='" + valor[p].Trim() + "',";
                            }
                            else
                            {
                                if (field[p].IndexOf("#") > 0)
                                    field[p] = field[p].Substring(field[p].IndexOf("#") + 1);

                                if (sqlWhere.Length == 0)
                                    sqlWhere += " where " + field[p] + "='" + valor[p] + "'";
                                else
                                    sqlWhere += " and " + field[p] + "='" + valor[p] + "'";
                            }
                        }
                        p++;
                    }

                    sql = sql.Remove(sql.Length - 1, 1);
                    sql = sql + sqlWhere;
                }
                #endregion


                string erro = "";

                if (tabela == "WTR_LEITURAS")
                {
                    sqlAux = sql;
                }
                else if (tabela == "WTR_FATURA")
                {
                    sqlAux = sqlAux.Substring(0, sqlAux.Length - 1) + "," + "'" + lei_anterior + "'" + ",'" +
                             lei_media + "'" + ",'" + lei_maximo + "'" + ",'" + lei_minimo + "','" + lei_dataleituraanterior + "','" + lei_residuotroca + "','" +
                             lei_periodo1 + "','" + lei_valor1 + "','" + lei_periodo2 + "','" + lei_valor2 + "','" + lei_periodo3 + "','" + lei_valor3 + "','" + lei_periodo4 + "','"
                             + lei_valor4 + "','" + lei_periodo5 + "','" + lei_valor5 + "','" + lei_periodo6 + "','" + lei_valor6 + "','" + lei_periodo7 + "','" + lei_valor7 + "','"
                             + lei_periodo8 + "','" + lei_valor8 + "')";
                    //erro = db.Ins(conexaoStr, sql);
                    erro = db.Ins(conexaoStr, sqlAux);
                }
                else
                {
                    erro = db.Ins(conexaoStr, sql);
                }

                if (erro.Trim().Length > 0)
                {
                    txtStatus.Text += "\r\n" + "Arquivo: " + file.Trim() + " - Registro: " + i + " - Erro: " + erro;
                    //txtStatus.Refresh();
                }

                #endregion

            }

            db.Exe(conexaoStr, "WTR_FAIXA", "DELETE FROM WTR_FAIXA WHERE FXA_MES = " + mes + " AND FXA_ANO = " + ano + " AND FXA_ID= 1");


            sql = " INSERT INTO WTR_LIVROS (CLI_ID, EMP_ID, ETP_ID, LIV_MES, LIV_ANO, CID_ID, "
                              +
                              " LOC_ID, LIV_DATALEIT, LIV_TOTAL, LIV_LEITURA, LIV_NAOLIDA, LIV_DATACARGA, LTR_ID, "
                              + " LIV_FLAG,LIV_MSG1,LIV_MSG2,LIV_MSG3,LIV_SEQ,LIV_GRUPO,LIV_FEBRABAN,LIV_NOMEARQ,LTR_ID2)VALUES('"
                              + Convert.ToInt32(cliente) + "','"
                              + "1" + "','"
                              + mes + "','"
                              + mes + "','"
                              + ano + "','"
                              + "1" + "','"
                              + rota + "','"
                              + DateTime.Now.ToShortDateString() + "','"
                              + countLeit.ToString() + "','0','" + countLeit.ToString() + "','"
                              + DateTime.Now.ToShortDateString() + "','"
                              + leiturista + "','N','"
                              + msg1 + "','"
                              + msg2 + "','"
                              + msg3 + "',"
                              + livSeq + ","
                              + grupo + ",'"
                              + febraban + "','"
                              + file.Split('.')[0] + "','"+leiturista+"')";


            string errLiv = db.Ins(conexaoStr, sql);

            if (errLiv.Trim().Length > 0)
                txtStatus.Text += "\r\n" + "Arquivo: Geração de Livros - Erro: " + errLiv;

            //txtStatus.Refresh();


            return 0;
        }
        catch (Exception ex)
        {
            string resp = (i + " #" + linha + " - " + ex.Message);
            return -1;
        }
        finally
        {
            txtStatus.Text += "\r\nArquivo: " + file.ToUpper().Trim() + " carregado com " + i.ToString() + " registros.\r\n";
            sr.Close();
            ofs.Close();
        }
    }

    private string TrataCampoMarilia(string tipo, string valor, string tabela, int countLeit, int i, string rota, string leiturista, string mes, string ano)
    {
        try
        {


            if (tipo.Equals("'Data'") && (valor.Length == 8))
            {
                valor = valor.Substring(0, 2) + "/" + valor.Substring(2, 2) + "/" + valor.Substring(4, 4);
            }
            else if (tipo.Equals(".Num"))
            {
                valor = Convert.ToInt32(valor.ToString().Trim().Replace("SN", "0").Replace("S/N", "0")).ToString().Trim();
            }
            else if (tipo.Equals(".Rota"))
            {
                valor = Convert.ToInt32(rota).ToString();
            }
            else if (tipo == ".cat")
            {
                if (cat.ToString() != "")
                {
                    if (Convert.ToInt32(cat) == 0)
                        cat = Convert.ToInt32(valor);
                    else if (Convert.ToInt32(cat) != Convert.ToInt32(valor))
                    {
                        cat = Convert.ToInt32(valor);
                        idFaixa = 0;
                    }
                }
                else cat = Convert.ToInt32(valor);

                return valor;
            }
            else if (tipo == ".idFaixa")
            {
                if (idFaixa.ToString() != "")
                {
                    idFaixa = Convert.ToInt32(idFaixa) + 1;
                    valor = idFaixa.ToString();
                }
                else
                {
                    idFaixa = Convert.ToInt32("0");
                    idFaixa = Convert.ToInt32(idFaixa) + 1;
                    valor = idFaixa.ToString();
                }
            }
            else if (tipo.Equals("'ref'"))
            {
                valor = valor.Replace("/", "");
            }
            else if (tipo == "'decimal'")
            {
                valor = valor.Replace(",", ".");
                valor = valor.Substring(0, valor.Length - 2) + "." + valor.Substring(valor.Length - 2, 2);
            }
            else if (tipo == "'decimal4'")
            {
                valor = valor.Replace(",", ".");
                valor = valor.Substring(0, valor.Length - 4) + "." + valor.Substring(valor.Length - 4, 4);
            }
            else if (tipo == "'/10'")
            {
                valor = Convert.ToString(Convert.ToDecimal(valor) / 10).Replace(',', '.');
            }
            else if (tipo == "'/100'")
            {
                if (valor.Trim().Length > 0)
                    valor = Convert.ToString(Convert.ToDecimal(valor) / 100).Replace(',', '.');
            }
            else if (tipo == "'/1000'")
            {
                valor = Convert.ToString(Convert.ToDecimal(valor) / 1000).Replace(',', '.');
            }
            else if (tipo == "'/10000'")
            {
                valor = Convert.ToString(Convert.ToDecimal(valor) / 10000).Replace(',', '.');
            }
            else if (tipo == ".trim")
            {
                valor = valor.Trim().Replace(" ", "");
            }
            else if (tipo == "NumSN")
            {
                if (valor.Trim() == "N")
                    valor = "0";
                else if (valor.Trim() == "S")
                    valor = "1";
                else
                    valor = "";
            }
            else if (tipo == ".leiturista")
            {
                valor = leiturista;
            }
            else if (tipo == ".empresa")
            {
                valor = "1";
            }
            else if (tipo == ".cidade")
            {
                valor = "1";
            }
            else if (tipo == ".local")
            {
                if (tabela == "WTR_MSG")
                    valor = "@LOCAL";
                else
                    valor = rota;
            }
            //else if (tipo == ".dia")
            //    valor = txtDia.Text.Trim();
            else if (tipo == ".mes")
            {
                if (tabela == "WTR_MSG")
                    valor = "@M";
                else
                    valor = mes;
            }
            else if (tipo == ".ano")
            {
                if (tabela == "WTR_MSG")
                    valor = "@ANO";
                else
                    valor = ano;
            }
            else if (tipo == ".Etapa")
            {
                valor = mes;
            }
            else if ((tipo == ".seq") || (tipo == "seq"))
            {
                valor = countLeit.ToString(); //valor = (i).ToString();
            }
            else if (tipo == ".hoje")
            {
                valor = DateTime.Now.Day.ToString() + "/" + DateTime.Now.Month.ToString() + "/" + DateTime.Now.Year.ToString();
            }
            else if (tipo == ".amanha")
            {
                valor = DateTime.Now.Day + "/" + DateTime.Now.Month.ToString() + "/" + DateTime.Now.Year.ToString();
                valor = Convert.ToDateTime(valor).AddDays(1).ToString("dd/MM/yyyy");
            }
            else
                valor = tipo.Replace('"', ' ').Trim();

            return valor;

        }
        catch (Exception ex)
        {
            msg += "\r\nRegistro " + i + ": Erro no Tratamento de valores - " + ex.Message;
            return "";
        }
    }

    public void deletarDadosAntigosMarilia(string rota, string ano, string mes, string strCon)
    {
        //DELETAR DADOS DAS TABELAS
        //wtr_TARIFA
        //WTR_SERVICOS
        //WTR_RUA
        Data db = new Data();

        db.Exe(strCon, "WTR_TARIFA", "delete from WTR_TARIFA where loc_id = " + rota + " AND TXA_MES = " + mes + " and txa_ano = " + ano);
        db.Exe(strCon, "WTR_PARAMETROS", "delete from WTR_PARAMETROS where loc_id = " + rota);
        db.Exe(strCon, "wtr_servico", "delete from wtr_servico");
        db.Exe(strCon, "wtr_indice", "delete from wtr_indice");
        db.Exe(strCon, "wtr_faixa", "delete from wtr_faixa where fxa_ano=" + ano + " and fxa_mes = " + mes);


    }


    public string Export(string cam, string file, string cliente, string caminhoProjeto, String strCon, HttpResponse response, int mes, string ano, Button btDownload)
    {
        DataTable dt;
        DataTable dt2 = null;
        Data db = new Data();

        string separador = xml.GetConfigValue(caminhoProjeto, "LOCAL", "EXPSEPARADOR", Convert.ToInt32(cliente).ToString());
        bool chkUnico = true;
        string ext = "";
        if (Convert.ToInt32(cliente) == 3)
            ext = "s";
        else if ((Convert.ToInt32(cliente) == 4) || (Convert.ToInt32(cliente) == 10))
            ext = "RET";
        else if ((Convert.ToInt32(cliente) == 5))
            ext = "SIM";
        else
            ext = "txt";
        string sql = "";
        string sql2 = "";//Busca Qtd Ocorrências

        int p = 0;
        int i = 0;
        string fileNew = "";
        Export export = new Export();
        ExportDAL expDAL = new ExportDAL();
        StreamWriter sw = StreamWriter.Null;
        int qtd1 = 0, qtd2 = 0, qtd3 = 0;

        try
        {


            #region Buscando informações no arquivo Import.xml

            //Verificar quais tabelas a serem exportadas (Config.xml : <TABEXPORT>)

            string[] tabExport = xml.GetConfigValue(caminhoProjeto, "LOCAL", "TABEXPORT", Convert.ToInt32(cliente).ToString()).Split(',');

            while (i < tabExport.Length)
            {
                export = expDAL.getExp(Convert.ToInt32(cliente), tabExport[i].Trim());

                #region Leitura de Conteúdo do Arquivo de Exportação
                string[] campos = export.Campos.Split('|');
                string tabela = campos[0];
                string[] field = new string[campos.Length - 1];
                string[] tamanho = new string[campos.Length - 1];
                string[] formato = new string[campos.Length - 1];
                string[] valor = new string[campos.Length - 1];

                p = 0;
                while (p < (campos.Length - 1))
                {
                    field[p] = campos[p + 1].Split(';')[0].Trim();
                    formato[p] = campos[p + 1].Split(';')[1].Trim();
                    tamanho[p] = campos[p + 1].Split(';')[2].Trim();

                    if (campos[p + 1].Split(';').Length > 3)	//Valor fixo
                    {
                        valor[p] = campos[p + 1].Split(';')[3].Replace('"', ' ').Trim();
                    }

                    p++;
                }
                #endregion

                mes = Convert.ToInt32(file.Substring(0, 3));
                int cid = Convert.ToInt32(file.Substring(3, 3));
                int loc = Convert.ToInt32(file.Substring(6, 3));

                #region Comando SQL para o BANCO DE DADOS
                sql = "SELECT ";

                p = 0;
                while (p < field.Length)
                {
                    sql += field[p].Trim() + ",";
                    p++;
                }

                sql = sql.Remove(sql.Length - 1, 1);
                sql += " FROM " + tabela;

                if ((tabela == "WTR_LIVROS") || (tabela == "WTR_LEITURAS"))
                {
                    sql += " WHERE LIV_MES = " + mes
                        + " AND LIV_ANO = " + ano
                        + " AND CID_ID = " + "1"
                        + " AND LOC_ID = " + loc
                        + " AND CLI_ID = " + Convert.ToInt32(cliente);
                    if (empresa != "0")
                        sql += " AND EMP_ID = " + empresa;
                    if (cliente == "14")
                        sql += " AND LEI_DATA != ''";

                    #region Verifica Qtd Ocorrências
                    if (Convert.ToInt32(cliente) == 5 || Convert.ToInt32(cliente) == 14)
                    {
                        if ((tabela == "WTR_LEITURAS"))//&&(Convert.ToInt32(cliente) == 5)
                        {
                            sql2 = "SELECT LTR_ID, MSG_ID, COUNT(*) AS QTD FROM WTR_LEITURAS WHERE LIV_MES = '" + mes + "'"
                            + " AND CID_ID = '" + cid + "'"
                            + " AND LIV_ANO = " + ano
                            + " AND LOC_ID = '" + loc + "'"
                            + " AND CLI_ID = '" + Convert.ToInt32(cliente) + "'"
                            + " AND EMP_ID = " + empresa
                            + " GROUP BY LTR_ID, MSG_ID "
                            + " ORDER BY MSG_ID ";

                            dt2 = db.GetDt(strCon, tabela, sql2);
                        }
                    }
                    if (Convert.ToInt32(cliente) == 2)
                    {
                        if ((tabela == "WTR_LEITURAS"))//&&(Convert.ToInt32(cliente) == 5)
                        {
                            sql2 = "SELECT LTR_ID, MSG_ID, COUNT(*) AS QTD FROM WTR_LEITURAS WHERE LIV_MES = '" + mes + "'"
                            + " AND CID_ID = '" + cid + "'"
                            + " AND LIV_ANO = " + ano
                            + " AND LOC_ID = '" + loc + "'"
                            + " AND CLI_ID = '" + Convert.ToInt32(cliente) + "'"
                            + " GROUP BY LTR_ID, MSG_ID "
                            + " ORDER BY MSG_ID ";

                            dt2 = db.GetDt(strCon, tabela, sql2);
                        }
                    }
                    #endregion
                }
                else if (tabela.IndexOf("WTR_FATURA") >= 0)
                {
                    sql += " WHERE WTR_FATURA.FAT_MES = " + mes.ToString().PadLeft(2, '0')
                        + " AND WTR_FATURA.FAT_ANO = '" + ano.PadLeft(2, '0') + "'"
                       + " AND WTR_FATURA.CLI_ID = " + Convert.ToInt32(cliente)
                       + " AND WTR_FATURA.FAT_PASTA = " + loc.ToString();
                    if (empresa != "0")
                        sql += " AND WTR_FATURA.EMP_ID = " + empresa;

                    if (Convert.ToInt32(cliente) != 5)
                    {
                        sql2 = "SELECT LTR_ID, MSG_ID, COUNT(*) AS QTD FROM WTR_FATURA WHERE FAT_MES = " + mes
                        + " AND FAT_PASTA = '" + loc + "'"
                        + " AND FAT_ANO = " + ano + ""
                        + " AND CLI_ID = '" + Convert.ToInt32(cliente) + "'"
                        + " GROUP BY LTR_ID, MSG_ID "
                        + " ORDER BY MSG_ID ";

                        dt2 = db.GetDt(strCon, tabela, sql2);
                    }
                    else
                    {
                        sql2 = "SELECT LTR_ID, MSG_ID, COUNT(*) AS QTD FROM WTR_FATURA WHERE FAT_MES = " + mes
                        + " AND FAT_PASTA = '" + loc + "'"
                        + " AND FAT_ANO = " + ano + ""
                        + " AND CLI_ID = '" + Convert.ToInt32(cliente) + "'"
                        + " GROUP BY LTR_ID, MSG_ID "
                        + " ORDER BY MSG_ID ";

                        dt2 = db.GetDt(strCon, tabela, sql2);
                    }
                }
                else if (tabela == "WTR_SERVICOS")
                {
                    sql += " WHERE SRV_MES = " + mes
                        + " AND SRV_LOCAL = " + loc
                        + " AND CLI_ID = " + Convert.ToInt32(cliente);
                    if (empresa != "0")
                        sql += " AND EMP_ID = " + empresa;
                }
                else if (tabela == "WTR_TARIFA" && Convert.ToInt32(cliente) == 5)
                {
                    sql += " WHERE TXA_MES = " + mes
                        + " AND LOC_ID = " + loc
                        + " AND TXA_ANO = " + ano
                        + " AND CLI_ID = " + Convert.ToInt32(cliente);
                    if (empresa != "0")
                        sql += " AND EMP_ID = " + empresa;
                }
                else if (tabela == "WTR_REGISTROLOG")
                {
                    sql += " WHERE LOG_MES = " + mes
                        + " AND LOG_LOCAL = " + loc
                        + " AND CLI_ID = " + Convert.ToInt32(cliente);
                    if (empresa != "0")
                        sql += " AND EMP_ID = " + empresa;
                }



                dt = db.GetDt(strCon, tabExport[i].Trim(), sql);

                if (dt.Rows.Count > 0)
                {
                    msg += "Exportando dados : " + tabExport[i].Trim().ToUpper();

                    if (i == 0)
                        qtd1 = dt.Rows.Count;
                    else if (i == 1) qtd2 = dt.Rows.Count;
                    else if (i == 2) qtd3 = dt.Rows.Count;

                    #region MESMO ARQUIVO DE EXPORTAÇÃO PARA DUAS OU MAIS TABELAS
                    if (fileNew.Length == 0)
                    {
                        if (chkUnico)
                        {
                            string nomeArq = xml.GetConfigValue(caminhoProjeto, "LOCAL", "ARQEXPORT", Convert.ToInt32(cliente).ToString());
                            string[] pedacos = nomeArq.Split(';');
                            int tamCampo = 0;

                            for (int pp = 0; pp < pedacos.Length; pp++)
                            {
                                tamCampo = dt.Rows[0][pedacos[pp].Split(',')[0].Trim()].ToString().Length;

                                if (tamCampo < (Convert.ToInt32(pedacos[pp].Split(',')[1])))
                                    fileNew += dt.Rows[0][pedacos[pp].Split(',')[0].Trim()].ToString().PadLeft(Convert.ToInt32(pedacos[pp].Split(',')[1]), '0');
                                else
                                    fileNew += dt.Rows[0][pedacos[pp].Split(',')[0].Trim()].ToString().Substring(tamCampo - Convert.ToInt32(pedacos[pp].Split(',')[1]), Convert.ToInt32(pedacos[pp].Split(',')[1]));
                            }
                        }

                        if (fileNew.Length > 0)
                            sw = new StreamWriter(checkFileExists(cam, fileNew.Trim(), ext));
                        //sw = new StreamWriter(cam + @"\" + fileNew.Trim() + "." + ext);
                        else
                        {

                            if (cliente == "5") //DAEM MARILIA - CEBI
                                sw = new StreamWriter(cam + @"\ROTA" + loc.ToString().Substring(0, 1).PadLeft(2, '0') + loc.ToString().PadLeft(4, '0') + "." + ext);
                            else
                                if (Convert.ToInt32(cliente) == 14) //Clementina
                                {
                                    sw = new StreamWriter(cam + @"\" + mes.ToString().PadLeft(2, '0') + ano + file.Substring(file.Length - 2, 2) + "." + ext);
                                }
                                else
                                    if (Convert.ToInt32(cliente) == 17)
                                        sw = new StreamWriter(checkFileExists(cam, "T" + tabExport[i].Trim() + "_" + file, ext), true, System.Text.Encoding.GetEncoding("ISO8859-1"));
                                    else
                                        sw = new StreamWriter(checkFileExists(cam, tabExport[i].Trim().Replace("LEITURAS", "FATURA") + "_" + file, ext), true, System.Text.Encoding.GetEncoding("ISO8859-1"));
                            //sw = new StreamWriter(cam + @"\" + tabExport[i].Trim() + "_" + file + "." + ext, true, System.Text.Encoding.GetEncoding("ISO8859-1"));
                        }
                    }
                    else
                    {
                        if (sw.Equals(StreamWriter.Null))
                        {
                            if (fileNew.Length > 0)
                                sw = new StreamWriter(cam + @"\" + fileNew.Trim() + "." + ext);

                            else
                                sw = new StreamWriter(cam + @"\" + tabExport[i].Trim() + "_" + file + "." + ext);
                        }
                    }
                    #endregion


                    for (int a = 0; a < dt.Rows.Count; a++)
                    {
                        //lblCount.Text = a.ToString();
                        //lblCount.Refresh();
                        string linha = string.Empty;

                        #region Leitura de Campos na Tabela
                        for (int n = 0; n < campos.Length - 1; n++)
                        {
                            if (Convert.ToInt32(tamanho[n].Trim()) > 0)
                            {
                                if (valor[n] == null)
                                {
                                    if (formato[n] == "N")
                                    {
                                        #region "N"
                                        decimal vlr = 0;
                                        if (dt.Rows[a][field[n]].ToString().Trim().Length > 0)
                                            vlr = Convert.ToDecimal(dt.Rows[a][field[n]].ToString().Replace('.', ','));
                                        if (vlr >= 0)
                                            linha += vlr.ToString().PadLeft(Convert.ToInt32(tamanho[n].Trim()), '0');
                                        else
                                        {
                                            vlr = vlr * (-1);
                                            linha += "-" + vlr.ToString().PadLeft(Convert.ToInt32(tamanho[n].Trim()) - 1, '0');
                                        }
                                        #endregion
                                    }
                                    else if (formato[n] == "T")
                                        linha += dt.Rows[a][field[n]].ToString().PadRight(Convert.ToInt32(tamanho[n].Trim()), ' ');

                                    else if (formato[n] == "DecimalClementina")
                                    {
                                        #region "Decimal"
                                        decimal vlr = 0;
                                        if (dt.Rows[a][field[n]].ToString().Trim().Length > 0)
                                            vlr = Convert.ToDecimal(dt.Rows[a][field[n]].ToString().Replace('.', ','));
                                        if (vlr >= 0)
                                            if (vlr > 0)
                                                linha += (vlr.ToString().Split(',')[0] + "," + vlr.ToString().Split(',')[1].PadRight(2, '0')).PadLeft(Convert.ToInt32(tamanho[n].Trim()), '0').Replace(',', '.');
                                            else
                                                linha += vlr.ToString("0.00").PadLeft(Convert.ToInt32(tamanho[n].Trim()), '0').Replace(',', '.');

                                        else
                                        {
                                            vlr = vlr * (-1);
                                            linha += "-" + vlr.ToString().PadLeft(Convert.ToInt32(tamanho[n].Trim()) - 1, '0').Replace(',', '.');
                                        }
                                        #endregion

                                        /* #region "Decimal"
                                        decimal vlr = 0;
                                        if (dt.Rows[a][field[n]].ToString().Trim().Length > 0)
                                            vlr = Convert.ToDecimal(dt.Rows[a][field[n]].ToString().Replace('.', ','));
                                        if (vlr >= 0)
                                            linha += vlr.ToString().PadLeft(Convert.ToInt32(tamanho[n].Trim()), '0').Replace(',', '.');
                                        else
                                        {
                                            vlr = vlr * (-1);
                                            linha += "-" + vlr.ToString().PadLeft(Convert.ToInt32(tamanho[n].Trim()) - 1, '0').Replace(',', '.');
                                        }
                                        #endregion*/
                                    }
                                    else if (formato[n] == "Decimal")
                                    {
                                        #region "Decimal"
                                        decimal vlr = 0;
                                        if (dt.Rows[a][field[n]].ToString().Trim().Length > 0)
                                            vlr = Convert.ToDecimal(dt.Rows[a][field[n]].ToString().Replace('.', ','));
                                        if (vlr >= 0)
                                            linha += vlr.ToString().PadLeft(Convert.ToInt32(tamanho[n].Trim()), '0').Replace(',', '.');
                                        else
                                        {
                                            vlr = vlr * (-1);
                                            linha += "-" + vlr.ToString().PadLeft(Convert.ToInt32(tamanho[n].Trim()) - 1, '0').Replace(',', '.');
                                        }
                                        #endregion
                                    }
                                    else if (formato[n] == "Versao")
                                    {
                                        #region "Versao"
                                        linha += "4.9".ToString().PadRight(Convert.ToInt32(tamanho[n].Trim()), ' ');


                                        #endregion
                                    }
                                    else if (formato[n] == "Null")
                                    {
                                        #region "Null"
                                        if (dt.Rows[a][field[n]].ToString().Trim() != "")
                                        {
                                            if (Convert.ToInt32(dt.Rows[a][field[n]].ToString().Trim()) == 0)
                                                linha += " ".PadRight(Convert.ToInt32(tamanho[n].Trim()), ' ');
                                            else
                                            {
                                                decimal vlr = 0;
                                                if (dt.Rows[a][field[n]].ToString().Trim().Length > 0)
                                                    vlr = Convert.ToDecimal(dt.Rows[a][field[n]].ToString().Replace('.', ','));
                                                if (vlr >= 0)
                                                    linha += vlr.ToString().PadLeft(Convert.ToInt32(tamanho[n].Trim()), '0');
                                                else
                                                {
                                                    vlr = vlr * (-1);
                                                    linha += "-" + vlr.ToString().PadLeft(Convert.ToInt32(tamanho[n].Trim()) - 1, '0');
                                                }
                                            }
                                        }
                                        else
                                            linha += " ".PadRight(Convert.ToInt32(tamanho[n].Trim()), ' ');


                                        #endregion
                                    }
                                    else if (formato[n] == "num")
                                    {
                                        #region "num"
                                        decimal vlr = 0;
                                        if (dt.Rows[a][field[n]].ToString().Contains("."))
                                            vlr =
                                                Convert.ToInt32(
                                                    dt.Rows[a][field[n]].ToString().Split('.')[0].ToString());
                                        else
                                        {
                                            if (dt.Rows[a][field[n]].ToString().Trim().Length > 0)
                                                vlr = Convert.ToInt32(dt.Rows[a][field[n]].ToString());
                                        }

                                        if (vlr >= 0)
                                            linha += vlr.ToString().PadLeft(Convert.ToInt32(tamanho[n].Trim()), '0').Replace(',', '.');
                                        else
                                        {
                                            vlr = vlr * (-1);
                                            linha += "-" + vlr.ToString().PadLeft(Convert.ToInt32(tamanho[n].Trim()) - 1, '0').Replace(',', '.');
                                        }
                                        #endregion
                                    }
                                    else if (formato[n] == "D")
                                    {
                                        #region "D"
                                        if (Convert.ToString(dt.Rows[a][field[n]].ToString().Replace("/", "").PadRight(Convert.ToInt32(tamanho[n].Trim()), ' ')).Trim().Length <= 0)
                                        {
                                            string dataLeit = DateTime.Now.Day.ToString().PadLeft(2, '0')
                                                            + DateTime.Now.Month.ToString().PadLeft(2, '0')
                                                            + DateTime.Now.Year.ToString().PadLeft(4, '0');
                                            linha += dataLeit;
                                        }
                                        else
                                            linha += dt.Rows[a][field[n]].ToString().Replace("/", "").PadRight(Convert.ToInt32(tamanho[n].Trim()), ' ');
                                        #endregion
                                    }
                                    else if (formato[n] == "Data")
                                    {
                                        #region "Data"
                                        if (dt.Rows[a][field[n]].ToString().Trim().Length > 0)
                                        {
                                            linha += dt.Rows[a][field[n]].ToString().Trim().Replace("-", "").Replace("/", "").Substring(2, 2) + dt.Rows[a][field[n]].ToString().Trim().Replace("-", "").Replace("/", "").Substring(0, 2) + dt.Rows[a][field[n]].ToString().Trim().Replace("-", "").Replace("/", "").Substring(4, 4);
                                        }
                                        else
                                            linha += dt.Rows[a][field[n]].ToString().Replace("/", "").PadRight(Convert.ToInt32(tamanho[n].Trim()), ' ');
                                        #endregion
                                    }
                                    else if (formato[n] == "DataDAEM")
                                    {
                                        #region "Data"
                                        if (dt.Rows[a][field[n]].ToString().Trim().Length > 0)
                                        {
                                            linha += dt.Rows[a][field[n]].ToString().Trim().Replace("-", "").Replace("/", "");
                                        }
                                        else
                                            linha += dt.Rows[a][field[n]].ToString().Replace("/", "").PadRight(Convert.ToInt32(tamanho[n].Trim()), ' ');
                                        #endregion
                                    }

                                    else if (formato[n] == "dataClementina")
                                    {
                                        #region "Data"
                                        if (dt.Rows[a][field[n]].ToString().Trim().Length > 0)
                                        {
                                            linha += dt.Rows[a][field[n]].ToString().Trim().Replace("/", "").Replace(" ", "").Replace(":", "").Substring(0, 12).PadRight(Convert.ToInt32(tamanho[n].Trim()), ' ');
                                        }
                                        else
                                            linha += dt.Rows[a][field[n]].ToString().Replace("/", "").PadRight(Convert.ToInt32(tamanho[n].Trim()), ' ');
                                        #endregion
                                    }

                                    else if (formato[n] == "NLigacao")
                                    {
                                        #region "N"
                                        decimal vlr = 0;
                                        if (dt.Rows[a][field[n]].ToString().Trim().Length > 0)
                                            vlr = Convert.ToDecimal(dt.Rows[a][field[n]].ToString().Replace('.', ',').Substring(0, dt.Rows[a][field[n]].ToString().Replace('.', ',').Length - 1));
                                        if (vlr >= 0)
                                            linha += vlr.ToString().PadLeft(Convert.ToInt32(tamanho[n].Trim()), '0');
                                        else
                                        {
                                            vlr = vlr * (-1);
                                            linha += "-" + vlr.ToString().PadLeft(Convert.ToInt32(tamanho[n].Trim()) - 1, '0');
                                        }
                                        #endregion
                                    }

                                    else if (formato[n] == "Hora")
                                    {
                                        #region "Data"
                                        if (dt.Rows[a][field[n]].ToString().Trim().Length > 0)
                                        {
                                            linha += dt.Rows[a][field[n]].ToString().Trim().Replace(":", "").Replace(" ", "");
                                        }
                                        else
                                            linha += dt.Rows[a][field[n]].ToString().Replace(":", "").Replace(" ", "").PadRight(Convert.ToInt32(tamanho[n].Trim()), ' ');
                                        #endregion
                                    }
                                    else if (formato[n] == "/100")
                                    {
                                        #region "/100"
                                        decimal ttmp = 0;

                                        if (dt.Rows[a][field[n]].ToString().Trim().Length > 0)
                                            ttmp = Convert.ToDecimal(dt.Rows[a][field[n]].ToString()) / 100;

                                        if (ttmp >= 0)
                                            linha += ttmp.ToString().PadLeft(Convert.ToInt32(tamanho[n].Trim()), '0');
                                        else
                                        {
                                            ttmp = ttmp * (-1);
                                            linha += "-" + ttmp.ToString().PadLeft(Convert.ToInt32(tamanho[n].Trim()) - 1, '0');
                                        }
                                        #endregion
                                    }
                                    else if (formato[n] == "*100")
                                    {
                                        #region "*100"
                                        string ttmp = "0";

                                        if (dt.Rows[a][field[n]].ToString().Trim().Length > 0)
                                            ttmp = (Convert.ToDecimal(dt.Rows[a][field[n]].ToString()) * 100).ToString().Replace(',', '.').Split('.')[0];

                                        if (Convert.ToDecimal(ttmp) >= 0)
                                            linha += ttmp.ToString().PadLeft(Convert.ToInt32(tamanho[n].Trim()), '0');
                                        else
                                        {
                                            ttmp = (Convert.ToDecimal(ttmp) * (-1)).ToString();
                                            linha += "-" + ttmp.ToString().PadLeft(Convert.ToInt32(tamanho[n].Trim()) - 1, '0');
                                        }
                                        #endregion
                                    }
                                    else if (formato[n] == "Right")
                                    {
                                        #region "Right"
                                        if (dt.Rows[a][field[n]].ToString().Length > 0)
                                        {
                                            int dife = dt.Rows[a][field[n]].ToString().Length - Convert.ToInt32(tamanho[n].Trim());
                                            linha += dt.Rows[a][field[n]].ToString().Substring(dife, dt.Rows[a][field[n]].ToString().Length - dife);
                                        }
                                        else
                                            linha += dt.Rows[a][field[n]].ToString().PadLeft(Convert.ToInt32(tamanho[n].Trim(), ' '));
                                        #endregion
                                    }
                                    else if (formato[n].Contains("Left-"))
                                    {
                                        #region "Left-"
                                        if (dt.Rows[a][field[n]].ToString().Length > 0)
                                        {
                                            int ret = Convert.ToInt32(formato[n].Split('-')[1].ToString());
                                            int dife = dt.Rows[a][field[n]].ToString().Length - ret;

                                            if (formato[n].Contains("N"))
                                                linha += dt.Rows[a][field[n]].ToString().Substring(0, dife).PadLeft(Convert.ToInt32(tamanho[n].Trim()), '0');
                                            else
                                                linha += dt.Rows[a][field[n]].ToString().Substring(0, dife).PadRight(Convert.ToInt32(tamanho[n].Trim()), ' ');
                                        }
                                        else
                                        {
                                            if (formato[n].Contains("N"))
                                                linha += dt.Rows[a][field[n]].ToString().PadLeft(Convert.ToInt32(tamanho[n].Trim(), '0'));
                                            else
                                                linha += dt.Rows[a][field[n]].ToString().PadRight(Convert.ToInt32(tamanho[n].Trim(), ' '));
                                        }
                                        #endregion
                                    }
                                    else if ((formato[n] == "RightDate") && (tamanho[n] == "6"))
                                    {
                                        #region "RightDate"
                                        if (dt.Rows[a][field[n]].ToString().Length > 0)
                                        {
                                            int dife = dt.Rows[a][field[n]].ToString().Length - 8;
                                            string tpm = dt.Rows[a][field[n]].ToString().Substring(dife, dt.Rows[a][field[n]].ToString().Length - dife);
                                            tpm = tpm.Substring(0, 4) + tpm.Substring(6, 2);
                                            linha += tpm;
                                        }
                                        else
                                            linha += dt.Rows[a][field[n]].ToString().PadLeft(Convert.ToInt32(tamanho[n].Trim()), ' ');
                                        #endregion
                                    }
                                    else if (formato[n].Contains("Right"))
                                    {
                                        #region "Right"
                                        if (dt.Rows[a][field[n]].ToString().Length > 0)
                                        {
                                            int ctr = dt.Rows[a][field[n]].ToString().Length;

                                            if (formato[n].Contains("("))
                                                ctr = Convert.ToInt32(formato[n].Split('(')[1].ToString().Replace(")", ""));

                                            if (dt.Rows[a][field[n]].ToString().Length > ctr)
                                                ctr = dt.Rows[a][field[n]].ToString().Length - ctr;
                                            else
                                                ctr = dt.Rows[a][field[n]].ToString().Length;

                                            if (ctr > Convert.ToInt32(tamanho[n].Trim()))
                                                ctr = Convert.ToInt32(tamanho[n].Trim());

                                            if ((formato[n].Contains("N")) && (Convert.ToInt32(cliente) == 4))//SAAE PB
                                                linha += dt.Rows[a][field[n]].ToString().Substring(0, ctr).PadLeft(Convert.ToInt32(tamanho[n].Trim()), '0');
                                            else if (formato[n].Contains("N"))
                                                linha += dt.Rows[a][field[n]].ToString().Substring(ctr).PadRight(Convert.ToInt32(tamanho[n].Trim()), '0');
                                            else
                                                linha += dt.Rows[a][field[n]].ToString().Substring(ctr).PadLeft(Convert.ToInt32(tamanho[n].Trim()), ' ');

                                        }
                                        else
                                        {
                                            if (formato[n].Contains("N"))
                                                linha += dt.Rows[a][field[n]].ToString().PadLeft(Convert.ToInt32(tamanho[n].Trim()), '0');
                                            else
                                                linha += dt.Rows[a][field[n]].ToString().PadRight(Convert.ToInt32(tamanho[n].Trim()), ' ');
                                        }
                                        #endregion
                                    }
                                    else if (formato[n].Contains("Left"))
                                    {
                                        #region "Left"
                                        if (dt.Rows[a][field[n]].ToString().Length > 0)
                                        {
                                            int ctr = dt.Rows[a][field[n]].ToString().Length;

                                            if (formato[n].Contains("("))
                                                ctr = Convert.ToInt32(formato[n].Split('(')[1].ToString().Replace(")", ""));

                                            if (dt.Rows[a][field[n]].ToString().Length < ctr)
                                                ctr = dt.Rows[a][field[n]].ToString().Length;

                                            if (ctr > Convert.ToInt32(tamanho[n].Trim()))
                                                ctr = Convert.ToInt32(tamanho[n].Trim());

                                            string dtLeit = dt.Rows[a][field[n]].ToString().Substring(0, ctr).PadLeft(Convert.ToInt32(tamanho[n].Trim()), ' ');
                                            if (formato[n].Contains("N"))
                                                dtLeit = dt.Rows[a][field[n]].ToString().Substring(0, ctr).PadRight(Convert.ToInt32(tamanho[n].Trim()), '0');

                                            if (formato[n].Contains("Date"))
                                            {
                                                if (dtLeit.Substring(dtLeit.Length - 2, 2) == "00")
                                                    dtLeit = dtLeit.Substring(0, 4) + "20" + dtLeit.Substring(4, 2);
                                            }
                                            linha += dtLeit;
                                        }
                                        else
                                        {
                                            if (formato[n].Contains("N"))
                                                linha += dt.Rows[a][field[n]].ToString().PadLeft(Convert.ToInt32(tamanho[n].Trim()), '0');
                                            else
                                                linha += dt.Rows[a][field[n]].ToString().PadRight(Convert.ToInt32(tamanho[n].Trim()), ' ');
                                        }
                                        #endregion
                                    }
                                    else if (formato[n].Contains("Seq"))
                                    {
                                        #region "Seq"
                                        int b = a + 1;
                                        linha += b.ToString().PadLeft(Convert.ToInt32(tamanho[n]), '0');

                                        #endregion
                                    }
                                    else if (formato[n].Contains("Lado"))
                                    {
                                        #region "Lado"
                                        linha += "1";

                                        #endregion
                                    }
                                    else if (formato[n] == "S/N")
                                    {
                                        #region "S/N"
                                        if (dt.Rows[a][field[n]].ToString().Trim().Length == 0)
                                            linha += "N";
                                        else
                                            linha += dt.Rows[a][field[n]].ToString().Trim();
                                        #endregion
                                    }
                                    else if ((formato[n].Substring(0, 1) == "P") && (formato[n].Split('.').Length > 1))
                                        linha += dt.Rows[a][field[n]].ToString().Substring(Convert.ToInt32(formato[n].Split('.')[1]), Convert.ToInt32(tamanho[n]));
                                    else if (formato[n] == "SetRota")
                                    {
                                        #region "SetRota"
                                        if (Convert.ToInt32(cliente) == 6)
                                        {
                                            if (dt.Rows[a][field[n]].ToString().Substring(0, 2) == "14")
                                            {
                                                linha += Convert.ToString("14" + dt.Rows[a][field[n]].ToString().Substring(2).PadLeft(4, '0')).PadLeft(10, '0');
                                            }
                                            else
                                            {
                                                linha += Convert.ToString(dt.Rows[a][field[n]].ToString().Substring(0, 1) + dt.Rows[a][field[n]].ToString().Substring(1).PadLeft(4, '0')).PadLeft(10, '0');
                                            }
                                        }
                                        #endregion
                                    }

                                    else
                                        linha += dt.Rows[a][field[n]].ToString();

                                    if (formato[n] == "num")
                                    {
                                        if (Convert.ToInt32(tamanho[n].Trim()) < dt.Rows[a][field[n]].ToString().Split('.')[0].Trim().Length)
                                        {
                                            if ((!formato[n].Contains("Hora")) && (!formato[n].Contains("Data")) && (!formato[n].Contains("Right")) && (formato[n].Substring(0, 1) != "P") && (!formato[n].Contains("Left")) && (!formato[n].Contains("dataClementina")))
                                            {
                                                int difff = dt.Rows[a][field[n]].ToString().Length - Convert.ToInt32(tamanho[n].Trim());
                                                if (linha.Length > difff)
                                                    linha = linha.Substring(0, (linha.Length - difff));
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (Convert.ToInt32(tamanho[n].Trim()) < dt.Rows[a][field[n]].ToString().Trim().Length)
                                        {
                                            if ((!formato[n].Contains("Hora")) && (!formato[n].Contains("Data")) && (!formato[n].Contains("Right")) && (formato[n].Substring(0, 1) != "P") && (!formato[n].Contains("Left")) && (!formato[n].Contains("dataClementina")))
                                            {
                                                int difff = dt.Rows[a][field[n]].ToString().Length - Convert.ToInt32(tamanho[n].Trim());
                                                if (linha.Length > difff)
                                                    linha = linha.Substring(0, (linha.Length - difff));
                                            }
                                        }
                                    }



                                }
                                else
                                {
                                    if (formato[n] == "N")
                                        linha += valor[n].ToString().PadLeft(Convert.ToInt32(tamanho[n].Trim()), '0');
                                    else
                                        linha += valor[n].ToString().PadRight(Convert.ToInt32(tamanho[n].Trim()), ' ');
                                }
                            }

                            if (separador.Length > 0)
                            {
                                if (Convert.ToInt32(cliente) == 1)
                                {
                                    if (tabela == "WTR_LEITURAS")
                                    {
                                        linha += separador;
                                    }
                                }
                                else
                                {
                                    linha += separador;
                                }
                            }
                        }

                #endregion

                        linha = linha.ToUpper();
                        linha = linha.Replace("À", "A").Replace("Á", "A").Replace("Ã", "A").Replace("Â", "A").Replace("È", "E").Replace("É", "E").Replace("Ê", "E").Replace("Ì", "I").Replace("Í", "I").Replace("Ò", "O").Replace("Ó", "O").Replace("Õ", "O").Replace("Ô", "O").Replace("Ù", "U").Replace("Ú", "U").Replace("Û", "U").Replace("Ç", "C");

                        if (linha.IndexOf("#") > 0)
                        {
                            for (int s = 0; s < linha.Split('#').Length - 1; s++)
                            {
                                sw.WriteLine(linha.Split(Convert.ToChar("#"))[s] + "#");
                            }
                        }
                        else
                        {
                            sw.WriteLine(linha);
                        }
                    }

                    if (!chkUnico)
                        sw.Close();
                }
            #endregion


                i++;
            }


            if (Convert.ToInt32(cliente) == 5 && chkUnico)
                sw.WriteLine("9" + qtd1.ToString().PadLeft(5, '0') + qtd2.ToString().PadLeft(5, '0') + qtd3.ToString().PadLeft(5, '0') + "00000" + " ".PadLeft(268, ' ') + "#");



            if (!sw.Equals(StreamWriter.Null))
                sw.Close();


           

            btDownload.Enabled = true;
            btDownload.Text = "Download: " + fileNew.Trim() + "." + ext;



            //Move nome arquivo para o outro list de backup


            #endregion





            #region Exibe Quantidade de Ocorrências
            if (dt2.DefaultView.Count > 0)
            {
                string msgOcorrencia = "Cod.Ocorrência\tQuantidade\r\n";
                for (int i2 = 0; i2 < dt2.DefaultView.Count; i2++)
                {
                    msgOcorrencia += "\r\n" + dt2.DefaultView.Table.DefaultView[i2].Row["MSG_ID"].ToString()
                        + "\t\t" + dt2.DefaultView.Table.DefaultView[i2].Row["QTD"].ToString();
                }
                msg += (msgOcorrencia + "Leiturista: " + dt2.DefaultView.Table.DefaultView[0].Row["LTR_ID"].ToString());
            }
            #endregion
            return msg;

        }
        catch (Exception ex)
        {
            string resp = ("Erro :" + " - " + ex.Message);
            msg += resp;
            return msg;
        }
        finally
        {
            sw.Close();
        }

    }



    private void downloadTxt(string arquivo, string nomeArq, HttpResponse response)
    {
        try
        {

            //Response.ContentType = "application/octet-stream";
            //Response.AppendHeader("Content-Disposition", String.Format("attachment; filename={0}", nomeArq));
            //Response.TransmitFile(arquivo);
            //HttpContext.Current.ApplicationInstance.CompleteRequest();

            // Get the physical Path of the file
            string filepath = arquivo;

            // Create New instance of FileInfo class to get the properties of the file being downloaded
            FileInfo file = new FileInfo(filepath);

            // Checking if file exists
            if (file.Exists)
            {
                // Clear the content of the response
                response.ClearContent();

                // LINE1: Add the file name and attachment, which will force the open/cance/save dialog to show, to the header
                response.AddHeader("Content-Disposition", String.Format("attachment; filename={0}", file.Name));

                // Add the file size into the response header
                response.AddHeader("Content-Length", file.Length.ToString());

                // Set the ContentType
                response.ContentType = ReturnFiletype(file.Extension.ToLower());

                // Write the file into the response (TransmitFile is for ASP.NET 2.0. In ASP.NET 1.1 you have to use WriteFile instead)
                response.TransmitFile(file.FullName);

                // End the response
                response.End();

                //send statistics to the class
            }
        }
        catch (Exception ex)
        {
            // msg("Erro na download do arquivo: " + ex.Message);
        }
    }

    public static string ReturnFiletype(string fileExtension)
    {
        switch (fileExtension)
        {
            case ".htm":
            case ".html":
            case ".log":
                return "text/HTML";
            case ".txt":
                return "text/plain";
            case ".doc":
                return "application/ms-word";
            case ".tiff":
            case ".tif":
                return "image/tiff";
            case ".asf":
                return "video/x-ms-asf";
            case ".avi":
                return "video/avi";
            case ".zip":
                return "application/zip";
            case ".xls":
            case ".csv":
                return "application/vnd.ms-excel";
            case ".gif":
                return "image/gif";
            case ".jpg":
            case "jpeg":
                return "image/jpeg";
            case ".bmp":
                return "image/bmp";
            case ".wav":
                return "audio/wav";
            case ".mp3":
                return "audio/mpeg3";
            case ".mpg":
            case "mpeg":
                return "video/mpeg";
            case ".rtf":
                return "application/rtf";
            case ".asp":
                return "text/asp";
            case ".pdf":
                return "application/pdf";
            case ".fdf":
                return "application/vnd.fdf";
            case ".ppt":
                return "application/mspowerpoint";
            case ".dwg":
                return "image/vnd.dwg";
            case ".msg":
                return "application/msoutlook";
            case ".xml":
            case ".sdxl":
                return "application/xml";
            case ".xdp":
                return "application/vnd.adobe.xdp+xml";
            default:
                return "application/octet-stream";
        }
    }

    private string checkFileExists(string cam, string arquivo, string ext)
    {
        try
        {
            string file = null;
            if (System.IO.File.Exists(cam + @"\" + arquivo + "." + ext))
            {
                //Panel1.Visible = true;
                //Panel3.Enabled = false;
                //Panel4.Enabled = false;
                //lblMensagem.Text = "Arquivo ja existente. Deseja substituí-lo ?";

                //if ()//ShowMessage("Arquivo " + arquivo + " já existe. Deseja substituí-lo?"))//, "Atenção!", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) == DialogResult.Yes)
                //  {
                try
                {

                    System.IO.File.Delete(cam + @"\" + arquivo + "." + ext);
                    file = cam + @"\" + arquivo + "." + ext;
                }
                catch (System.IO.IOException e)
                {
                    Console.WriteLine(e.Message);
                }
                //}
                //else
                //{
                //InputBox input = new InputBox();
                //file = checkFileExists(cam, input.Show(arquivo, "Informe o nome do arquivo: "), ext);
                //}
            }
            else
            {
                file = cam + @"\" + arquivo + "." + ext;
            }
            return (file.Contains("." + ext) ? file : file + "." + ext);
        }
        catch (Exception ex)
        {
            msg += ex.Message;
            return "";
        }
    }

    public void AnaliseCritica(String strCon, TextBox txtLivro, DropDownList cbxMsg, DropDownList cbxMes, TextBox txtAno, TextBox txtEndereco, TextBox txtNumero,
       CheckBox chkCritica, TextBox txtPercent, Label lblMsg, DropDownList ddlSituacaoLeitura, DropDownList ddlLeituristas, TextBox txtIdentificacao,
       TextBox txtMedidor, RadioButton rbEndereço,RadioButton rbEnderecoEntrega, RadioButton rbLigacao, RadioButton rbSequencia, RadioButton rbNomeContribuinte,
        RadioButton rbRota, GridView gridView, Panel panelMsg,DropDownList ddlTipoRelatorio,Panel PanelQtdRegistros, Label lblTOTAL,RadioButton rbData)
    {
        DataTable dt = new DataTable();

        try
        {


            double porc = 0;
            string pMax = "";
            string pMin = "";

            #region Porcentagem
            if (txtPercent.Text.Trim().Length < 1)
                porc = 1;
            else
            {
                try
                {
                    porc = Convert.ToInt32(txtPercent.Text.Trim());
                    pMax = Convert.ToDecimal(1 + (porc / 100)).ToString().Replace(",", ".");
                    pMin = Convert.ToDecimal(1 - (porc / 100)).ToString().Replace(",", ".");
                }
                catch (Exception ee)
                {
                    lblMsg.Text = ("Informe o percentual correto da crítica");
                }
            }
            #endregion

            string sql =
                "select LIV_ANO AS ANO, LIV_MES AS MES, '09'+CAST(substring(lei_ligacao,2,len(lei_ligacao)) AS VARCHAR) +'-'+CAST(LEI_DIGITOLIGACAO AS VARCHAR)  AS " +
                "LIGACAO,LEI_CONSUMIDOR AS CONSUMIDOR, WTR_LEITURAS.LOC_ID AS ROTA, LEI_ENDERECO +','+convert(varchar,LEI_NUMERO) +' " +
                "Entrega: '+ LEI_ENDERECOENTREGA+','+CONVERT(varchar,LEI_NUMEROENTREGA) AS ENDEREÇO, LEI_NUMERO AS NUMERO, LEI_DATA +' '+LEI_HORA AS DATA," +
                " LTR_NOME AS LEITURISTA, MSG_DESCRICAO AS OCORRENCIA, LEI_MEDIA AS MEDIA, LEI_ANTERIOR AS 'LEIT. ANT', LEI_LEITURA AS LEITURA, " +
                " LEI_NUMMED AS MEDIDOR,  LEI_CONSUMO as CONSUMO,LEI_LATITUDE,LEI_LONGITUDE ";
            if (ddlTipoRelatorio.SelectedValue.ToString() != "Relatório comercial")
                sql += ",LEI_FORMAENTREGA AS ENTREGA,LTR_ID2,WTR_LEITURAS.LTR_ID   ";

            sql += " FROM WTR_LEITURAS INNER JOIN WTR_MENSAGENS ON(WTR_LEITURAS.MSG_ID = WTR_MENSAGENS.MSG_ID and WTR_LEITURAS.cli_id = WTR_MENSAGENS.cli_id and" +
                " WTR_MENSAGENS.emp_id = WTR_LEITURAS.emp_id) INNER JOIN WTR_LEITURISTAS ON (WTR_LEITURAS.LTR_ID = WTR_LEITURISTAS.LTR_ID  " +
                "and WTR_LEITURAS.cli_id = WTR_LEITURISTAS.cli_id and WTR_LEITURISTAS.emp_id = WTR_LEITURAS.emp_id) WHERE WTR_LEITURAS.emp_id =" +
                empresa + " and WTR_LEITURAS.cid_id =" + empresa + " and WTR_LEITURAS.CLI_ID = " + cliente;

            #region SQL E FILTROS

            if (txtLivro.Text.Trim().Length > 0)
            {
                sql += " AND WTR_LEITURAS.LOC_ID = '" + txtLivro.Text.Trim() + "' ";
            }

            if (cbxMsg.SelectedValue.ToString() != "1000")
                sql += " AND WTR_LEITURAS.MSG_ID = '" + cbxMsg.SelectedValue.ToString().ToString() + "' ";

            if (cbxMes.SelectedIndex > 0)
                sql += " AND LIV_MES = '" + cbxMes.SelectedIndex.ToString() + "' ";

            if (txtAno.Text.Trim().Length > 0)
                sql += " AND LIV_ANO = '" + txtAno.Text.Trim() + "' ";

            if (txtEndereco.Text.Trim().Length > 0)
                sql += " AND (LEI_ENDERECO LIKE '" + txtEndereco.Text + "' or LEI_ENDERECOENTREGA LIKE '" + txtEndereco.Text + "') ";
            if (txtNumero.Text.Trim().Length > 0)
                sql += " AND (LEI_NUMERO = " + txtNumero.Text + " or LEI_NUMEROENTREGA LIKE '" + txtNumero.Text + "') ";

            if (rbEnderecoEntrega.Checked)
            {
                sql += "AND (LEI_ENDERECO != LEI_ENDERECOENTREGA OR LEI_NUMERO != dbo.TiraLetras(LEI_NUMEROENTREGA))";
            }


            #region Somente critica
            if (chkCritica.Checked)
            {
                sql += " AND (((LEI_LEITURA - LEI_ANTERIOR) > (LEI_MEDIA * " + pMax + ")) OR ((LEI_LEITURA - LEI_ANTERIOR) < (LEI_MEDIA * " + pMin + ")))";
            }
            #endregion


            #region Sem Leitura
            if (ddlSituacaoLeitura.SelectedValue.ToString() == "Com Leitura")
            {
                sql += " AND LEI_LEITURA <> ''";
            }
            else
            {
                if (ddlSituacaoLeitura.SelectedValue.ToString() == "Sem Leitura")
                {
                    sql += " AND (LEI_LEITURA = '' or lei_leitura is null)";
                }
            }
            #endregion

            #region SITUACAO LEITURA
            if (ddlSituacaoLeitura.SelectedValue.ToString().ToString().ToString() == "Alteração cadastral")
                sql += " AND LEI_NOVACATEGORIA > 0";

            #endregion


            #region Leituristas
            if (ddlLeituristas.SelectedValue.ToString() != "0")
            {
                sql += " AND WTR_LEITURAS.LTR_ID  =  " + ddlLeituristas.SelectedValue.ToString();
            }
            #endregion

            #region Identificação e Medidor
            if ((txtIdentificacao.Text.Trim().Length > 0) || (txtMedidor.Text.Trim().Length > 0))
            {
                if (txtIdentificacao.Text.Trim().Length > 0)
                    sql += " AND LEI_LIGACAO LIKE '%" + txtIdentificacao.Text.Trim() + "%' ";

                if (txtMedidor.Text.Trim().Length > 0)
                    sql += " AND LEI_NUMMED LIKE '%" + txtMedidor.Text.Trim() + "%' ";

            }




            #endregion

            #endregion

            /*if (ordeR.Length > 0)
            sql += " ORDER BY " + ordeR;
        else
        {*/

            if (rbLigacao.Checked)
                sql += " ORDER BY lei_ligacao,LIV_ANO, LIV_MES, WTR_LEITURAS.LOC_ID";
            else if (rbNomeContribuinte.Checked)
                sql += " ORDER BY lei_consumidor,LIV_ANO, LIV_MES, WTR_LEITURAS.LOC_ID";
            else if (rbData.Checked)
                sql += " ORDER BY LEI_DATA,LEI_HORA,LIV_ANO, LIV_MES, WTR_LEITURAS.LOC_ID";
            else if (rbRota.Checked)
                sql += " ORDER BY WTR_LEITURAS.LOC_ID,LIV_ANO, LIV_MES,lei_seq";
            else
            {

                sql += " ORDER BY LIV_ANO, LIV_MES, ";
                
                sql += "lei_seq";
            }

            Data db = new Data();
            dt = db.GetDt(strCon, "WTR_LEITURAS", sql);

            if (dt.DefaultView.Count > 0)
            {
                lblTOTAL.Text = string.Format("{0:C}", Convert.ToDecimal(dt.DefaultView.Count.ToString())).Replace("R$", "").Replace(" ", "").Split(',')[0];
                PanelQtdRegistros.Visible = true;
            }
            else PanelQtdRegistros.Visible = false;


            if (ddlTipoRelatorio.SelectedValue.ToString() == "Relatório comercial")
            {
                dt.Columns.Add("VR RES");
                dt.Columns.Add("VR COM");
                dt.Columns.Add("Diferença");
                dt.Columns.Add("VR RES + Min");
                dt.Columns.Add("VR COM + Min");
                dt.Columns.Add("Diferença Min");
            }

            gridView.DataSource = dt;
            gridView.DataBind();


        }
        catch (Exception ex)
        {
            lblMsg.Text = "Erro - " + ex.Message;
            panelMsg.Visible = true;

        }

    }


}