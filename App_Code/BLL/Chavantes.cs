﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI.WebControls;
using WaterSyncLite.Class;
using WaterSyncLite.DAL;
using WaterSyncLite.MDL;

/// <summary>
/// Summary description for Chavantes
/// </summary>
public class Chavantes
{
	public Chavantes()
	{
		//
		// TODO: Add constructor logic here
		//
	}
    int cat = 0;
    int idFaixa = 0;
    String msg = "";
    string cliente = "09";
    string empresa = "01";
    public int Import(string cam, string file, string rota, string mes, string ano, string caminhoProjeto, string leiturista, String strCon)
    {
        #region Variaveis
        Data db = new Data();
        string separador = xml.GetConfigValue(caminhoProjeto, "LOCAL", "SEPARADOR", cliente);
        string command = "insert";
        ImportDAL impDAL = new ImportDAL();

        int tt = 1;           //Qtd de tabelas a serem importadas
        int countLeit = 0;    //Controle de leituras para geração do Livro

        Import importLeit = impDAL.getImp(Convert.ToInt32(Convert.ToInt32(cliente)), "LEITURA");
        //Import importFat = impDAL.getImp(Convert.ToInt32(Convert.ToInt32(cliente)), "FATURA");

        FileStream ofs = File.Open(cam + file, FileMode.Open, FileAccess.Read, FileShare.None);
        StreamReader sr = new StreamReader(ofs, System.Text.Encoding.GetEncoding("ISO-8859-1"));	//Aceita caracteres especiais.
        string linha = string.Empty;
        int i = 0;
        string sql = "";
        #endregion

        #region CAMPOS
        string[][] campos = new string[tt][];

        campos[0] = importLeit.Campos.Split('|');
        //campos[1] = importFat.Campos.Split('|');
        #endregion

        try
        {
            while (sr.Peek() >= 0)
            {
                #region Variáveis de arquivo

                i++;
                //lblCount.Text = i.ToString();
                ////lblCount.Refresh();
                linha = string.Empty;
                linha = sr.ReadLine();

                #region Tratamento da linha
                //Retira o separador de campo - Na importação não deve ter.
                if (separador.Trim().Length < 1)
                    linha = linha.Replace("|", " ").Replace(";", " ");    //Alterado para permitir ponto e vírgula do Cadastro. 10/12/2009

                //if (linha.Length < 141)
                //linha = linha.PadRight(141, ' ');

                linha = linha.Replace("'", " ");		//Evita erro de SQL com apóstofro - Padovani.
                linha = linha.Replace("\r\n", " ");	    //Evita erro com <Enter> - Padovani.

                linha = linha.Replace("S/N", "000");    //Evita inserir letras em campo numérico - Padovani.
                #endregion

                string[] field = null;
                string[] valor = null;
                string tabela = "";
                int p = 0;
                bool exec = false;

                #endregion

                for (int t = 0; t < tt; t++)
                {
                    tabela = campos[t][0];

                    #region Identificador da Linha
                    //if (tabela == "WTR_LEITURAS")
                    //{
                    //    countLeit++;
                    //    exec = true;
                    //    command = "insert";
                    //}
                    //else if (tabela == "WTR_FATURA")
                    //{
                    //    exec = true;
                    //    command = "insert";
                    //}

                    if ((tabela == "WTR_LEITURAS") || (tabela == "WTR_FATURA"))
                    {
                        exec = true;
                        command = "insert";
                        if (tabela == "WTR_LEITURAS")
                            countLeit++;
                    }

                    else
                    {
                        exec = false;
                        continue;
                    }
                    #endregion

                    if (exec)
                    {
                        field = new string[campos[t].Length - 1];
                        valor = new string[campos[t].Length - 1];

                        if ((tabela.Trim().Length > 0) || (field.Length > 0))
                        {

                            #region Leitura de Conteúdo do Arquivo
                            p = 0;
                            while (p < (campos[t].Length - 1))
                            {
                                field[p] = campos[t][p + 1].Split(';')[0].Trim();

                                if (separador.Trim().Length > 0)
                                {
                                    char separa = Convert.ToChar(separador.Trim());

                                    if (campos[t][p + 1].Split(';').Length > 3)
                                    {
                                        valor[p] = campos[t][p + 1].Split(';')[3].Trim().Replace('"', ' ');
                                        valor[p] = valor[p].Trim();

                                        if (valor[p].Trim() == "seq")
                                            valor[p] = i.ToString();
                                        else if (valor[p].Trim() == ".leiturista")
                                            valor[p] = leiturista;
                                    }
                                    else
                                        valor[p] = linha.Split(separa)[p].ToString();

                                    if (campos[t][p + 1].Split(';').Length > 2)
                                    {
                                        if (Convert.ToInt32(campos[t][p + 1].Split(';')[2].ToString().Trim()) > 0)
                                            valor[p] = valor[p].Substring(0, Convert.ToInt32(campos[t][p + 1].Split(';')[2].ToString().Trim()));
                                    }
                                }

                                else
                                {
                                    if (campos[t][p + 1].Split(';').Length > 3)	//Valor fixo ou Campo Formatado
                                    {
                                        valor[p] = linha.Substring(Convert.ToInt32(campos[t][p + 1].Split(';')[1].Trim()), Convert.ToInt32(campos[t][p + 1].Split(';')[2].Trim()));
                                        valor[p] = TrataCampo(campos[t][p + 1].Split(';')[3].Trim(), valor[p], tabela, countLeit, i,rota,leiturista,mes,ano);
                                    }
                                    else
                                        valor[p] = linha.Substring(Convert.ToInt32(campos[t][p + 1].Split(';')[1].Trim()), Convert.ToInt32(campos[t][p + 1].Split(';')[2].Trim()));
                                }
                                /*
                                #region Colocando valores nos textBox
                                if (field[p].ToString() == "liv_mes")
                                {
                                    txtMes.Text = valor[p].ToString();
                                    txtEtapa.Text = valor[p].ToString();
                                }
                                else if (field[p].ToString() == "liv_ano")
                                    txtAno.Text = valor[p].ToString();
                                else if (field[p].ToString() == "loc_id")
                                    txtLocal.Text = valor[p].ToString();
                                else if (field[p].ToString() == "emp_id")
                                    txtEmpresa.Text = valor[p].ToString();
                                #endregion
                                */

                       
                                p++;
                            }
                            #endregion

                            #region Comando SQL para o BANCO DE DADOS
                            #region Command INSERT
                            if (command == "insert")
                            {
                                sql = "INSERT INTO " + tabela + " (";

                                p = 0;
                                while (p < field.Length)
                                {
                                    if (p > 0)
                                    {
                                        if (field[p - 1].Trim() != field[p].Trim())
                                        {
                                            if (field[p].IndexOf("#") > 0)
                                                field[p] = field[p].Substring(field[p].IndexOf("#") + 1);

                                            sql += field[p].Trim() + ",";
                                        }
                                    }
                                    else
                                    {
                                        if (field[p].IndexOf("#") > 0)
                                            field[p] = field[p].Substring(field[p].IndexOf("#") + 1);

                                        sql += field[p].Trim() + ",";
                                    }
                                    p++;
                                }
                                sql = sql.Remove(sql.Length - 1, 1);
                                sql += ") VALUES ('";

                                p = 0;
                                while (p < valor.Length)
                                {
                                    if (p > 0)
                                    {
                                        if (field[p - 1].Trim() == field[p].Trim())
                                            sql = sql.Remove(sql.Length - 3, 3);

                                        sql += valor[p].Trim() + "','";
                                    }
                                    else
                                    {
                                        sql += valor[p].Trim() + "','";
                                    }

                                    p++;
                                }
                                sql = sql.Remove(sql.Length - 2, 2);
                                sql += ")";
                            }
                            #endregion



                            #region UPDATE
                            if (command == "update")
                            {
                                sql = " UPDATE " + tabela + " SET ";
                                string sqlWhere = " WHERE ";

                                p = 0;
                                while (p < field.Length)
                                {
                                    if (!((tabela.Equals("WTR_LEITURAS")) && (field[p].Trim().ToUpper().Equals("LEI_SEQ"))))
                                    {
                                        if (p > 0)
                                        {
                                            if (field[p - 1].Trim() != field[p].Trim())
                                            {
                                                if (field[p].IndexOf("#") > 0)
                                                    field[p] = field[p].Substring(field[p].IndexOf("#") + 1);

                                                if (field[p - 1].Trim() == field[p].Trim())
                                                    sql = sql.Remove(sql.Length - 3, 3);
                                            }
                                        }
                                        else
                                        {
                                            if (field[p].IndexOf("#") > 0)
                                                field[p] = field[p].Substring(field[p].IndexOf("#") + 1);
                                        }
                                        sql += field[p].Trim() + " = '" + valor[p].Trim() + "',";
                                        if ((tabela.Equals("WTR_LEITURAS")) || (tabela.Equals("WTR_FATURA")))
                                            if ((field[p].Trim().ToUpper().Equals("LEI_LIGACAO")) || (field[p].Trim().ToUpper().Equals("LOC_ID")) || (field[p].Trim().ToUpper().Equals("LIV_MES")) || (field[p].Trim().ToUpper().Equals("LIV_ANO")) ||
                                                (field[p].Trim().ToUpper().Equals("FAT_LIGACAO")) || (field[p].Trim().ToUpper().Equals("FAT_PASTA")) || (field[p].Trim().ToUpper().Equals("FAT_MES")) || (field[p].Trim().ToUpper().Equals("FAT_ANO")))
                                                sqlWhere += " " + field[p].Trim() + " = '" + valor[p].Trim() + "' AND";
                                    }
                                    p++;
                                }
                                sql = sql.Remove(sql.Length - 1, 1);
                                sqlWhere = (sqlWhere.Length > 7 ? sqlWhere.Remove(sqlWhere.Length - 3, 3) : ""); //+" AND " 
                                //+(tabela.Equals("WTR_LEITURAS")?" LEI_FLAG":(tabela.Equals("WTR_FATURA")? " FAT_FLAG":"")) + "='N'";
                                sql += sqlWhere;

                            }
                            #endregion

                            string erro = "";
                            if (command.Length > 0)
                            {
                                erro = db.Ins(strCon, sql);
                            }

                            if (erro.Trim().Length > 0)
                            {
                                msg += "\r\n" + "Arquivo: " + file.Trim() + " - Registro: " + i + " - Erro: " + erro;
                                //txtStatus.Refresh();
                            }


                            #endregion
                        }
                    }
                }
            }

            #region Geração do LIVRO.
            if (countLeit > 0)
            {
                string sql2 = "INSERT WTR_LIVROS VALUES('"
                    + cliente + "','"
                    + empresa + "','"
                    + mes + "','"
                    + mes + "','"
                    + ano + "','"
                    + "1','"
                    + rota + "','"
                    + "','"
                    + DateTime.Now.Day.ToString().ToString().Trim() + "/" + mes + "/" + ano + "','"
                    + countLeit.ToString() + "','"
                    + "0','"
                    + countLeit.ToString() + "','"
                    + "0','"
                    + DateTime.Now.ToShortDateString() + "','"
                    + leiturista + "','"
                    + "0','"
                    + "0','"
                    + "U','"
                    + "0','"
                    + "N')";
                string erro2 = db.Ins(strCon, sql2);
                msg += "Arquivo carregado com sucesso.\n"+countLeit.ToString()+" registros. ";
                ////fileFTP += sql2 + "\r\n";
            }
            #endregion

            return 0;
        }
        catch (Exception ex)
        {
            string resp = (i + " #" + linha + " - " + ex.Message);
            return -1;
        }
        finally
        {
            msg+= "\r\nArquivo: " + file.ToUpper().Trim() + " carregado com " + i.ToString() + " registros.\r\n";
            sr.Close();
            ofs.Close();
            
        }
    }

    private string TrataCampo(string tipo, string valor, string tabela, int countLeit, int i,string rota,string leiturista,string mes, string ano)
    {
        try
        {
            if (tipo.Equals("'Data'") && (valor.Length == 8))
            {
                valor = valor.Substring(0, 2) + "/" + valor.Substring(2, 2) + "/" + valor.Substring(4, 4);
            }
            else if (tipo.Equals("'ref'"))
            {
                valor = valor.Replace("/", "");
            }
            else if (tipo.Equals(".TrataNumero"))
            {


                if (valor.Contains('-'))
                {
                    valor = valor.Split('-')[0];
                }
                valor = SomenteNumeros(valor);

            }
            else if (tipo == "'decimal'")
            {
                valor = valor.Replace(",", ".");
            }
            else if (tipo == "'/10'")
            {
                valor = Convert.ToString(Convert.ToDecimal(valor) / 10).Replace(',', '.');
            }
            else if (tipo == "'/100'")
            {
                if (valor.Trim().Length > 0)
                    valor = Convert.ToString(Convert.ToDecimal(valor) / 100).Replace(',', '.');
            }
            else if (tipo == "'/1000'")
            {
                valor = Convert.ToString(Convert.ToDecimal(valor) / 1000).Replace(',', '.');
            }
            else if (tipo == "'/10000'")
            {
                valor = Convert.ToString(Convert.ToDecimal(valor) / 10000).Replace(',', '.');
            }
            else if (tipo == ".trim")
            {
                valor = valor.Trim().Replace(" ", "");
            }
            else if (tipo == "NumSN")
            {
                if (valor.Trim() == "N")
                    valor = "0";
                else if (valor.Trim() == "S")
                    valor = "1";
                else
                    valor = "";
            }
            else if (tipo == ".leiturista")
            {
                valor = leiturista;
            }
            else if (tipo == ".empresa")
            {
                valor = empresa;
            }
            else if (tipo == ".cidade")
            {
                valor = "1";
            }
            else if (tipo == ".local")
            {
                if (tabela == "WTR_MSG")
                    valor = "@LOCAL";
                else
                    valor = rota;
            }
            else if (tipo == ".dia")
                valor = DateTime.Now.Day.ToString().ToString();
            else if (tipo == ".mes")
            {
                if (tabela == "WTR_MSG")
                    valor = "@M";
                else
                    valor = mes;
            }
            else if (tipo == ".ano")
            {
                if (tabela == "WTR_MSG")
                    valor = "@ANO";
                else
                    valor = ano;
            }
            else if (tipo == ".Etapa")
            {
                valor = mes;
            }
            else if ((tipo == ".seq") || (tipo == "seq"))
            {
                valor = countLeit.ToString(); //valor = (i).ToString();
            }
            else if (tipo == ".hoje")
            {
                valor = DateTime.Now.Day.ToString() + "/" + DateTime.Now.Month.ToString() + "/" + DateTime.Now.Year.ToString();
            }
            else if (tipo == ".amanha")
            {
                valor = DateTime.Now.Day + "/" + DateTime.Now.Month.ToString() + "/" + DateTime.Now.Year.ToString();
                valor = Convert.ToDateTime(valor).AddDays(1).ToString("dd/MM/yyyy");
            }
            else if (tipo == ".cat")
            {
                if (cat.ToString() != "")
                {
                    if (Convert.ToInt32(cat) == 0)
                        cat = Convert.ToInt32(valor);
                    else if (Convert.ToInt32(cat) != Convert.ToInt32(valor))
                    {
                        cat = Convert.ToInt32(valor);
                        idFaixa = 0;
                    }
                }
                else cat = Convert.ToInt32(valor);

                return valor;
            }
            else if (tipo == ".idFaixa")
            {
                if (idFaixa.ToString() != "")
                {
                    idFaixa = Convert.ToInt32(idFaixa) + 1;
                    valor = idFaixa.ToString();
                }
                else
                {
                    idFaixa = Convert.ToInt32("0");
                    idFaixa = Convert.ToInt32(idFaixa) + 1;
                    valor = idFaixa.ToString();
                }
            }
            else
                valor = tipo.Replace('"', ' ').Trim();

            return valor;

        }
        catch (Exception ex)
        {
            msg += "\r\nRegistro " + i + ": Erro no Tratamento de valores - " + ex.Message;
            return "";
        }
    }

    //FUNÇÃO QUE RETORNA SOMENTE NÚMEROS DE UMA STRING
    public string SomenteNumeros(string toNormalize)
    {
        string resultString = string.Empty;
        Regex regexObj = new Regex(@"[^\d]");
        resultString = regexObj.Replace(toNormalize, "");
        return resultString;
    }


    public void Export(string cam, string file, string cliente, string caminhoProjeto, String strCon, HttpResponse response, int mes, string ano, Button btDownload)
    {

        DataTable dt;
        DataTable dt2 = null;
        Data db = new Data();
        string separador = xml.GetConfigValue(caminhoProjeto, "LOCAL", "EXPSEPARADOR", Convert.ToInt32(cliente).ToString());
        string ext = "";
        if (Convert.ToInt32(cliente) == 3)
            ext = "s";
        else if ((Convert.ToInt32(cliente) == 4) || (Convert.ToInt32(cliente) == 10))
            ext = "RET";
        else
            ext = "txt";
        string sql = "";
        string sql2 = "";//Busca Qtd Ocorrências

        bool chkUnico = false;

        Export export = new Export();
        ExportDAL expDAL = new ExportDAL();
        int p = 0;
        int i = 0;
        string fileNew = "";
        StreamWriter sw = StreamWriter.Null;

        try
        {

            #region Buscando informações no arquivo Import.xml

            //Verificar quais tabelas a serem exportadas (Config.xml : <TABEXPORT>)

            string[] tabExport = xml.GetConfigValue(caminhoProjeto, "LOCAL", "TABEXPORT", Convert.ToInt32(cliente).ToString()).Split(',');

            while (i < tabExport.Length)
            {
                export = expDAL.getExp(Convert.ToInt32(cliente), tabExport[i].Trim());

                #region Leitura de Conteúdo do Arquivo de Exportação
                string[] campos = export.Campos.Split('|');
                string tabela = campos[0];
                string[] field = new string[campos.Length - 1];
                string[] tamanho = new string[campos.Length - 1];
                string[] formato = new string[campos.Length - 1];
                string[] valor = new string[campos.Length - 1];

                p = 0;
                while (p < (campos.Length - 1))
                {
                    field[p] = campos[p + 1].Split(';')[0].Trim();
                    formato[p] = campos[p + 1].Split(';')[1].Trim();
                    tamanho[p] = campos[p + 1].Split(';')[2].Trim();

                    if (campos[p + 1].Split(';').Length > 3)	//Valor fixo
                    {
                        valor[p] = campos[p + 1].Split(';')[3].Replace('"', ' ').Trim();
                    }

                    p++;
                }
                #endregion

                mes = Convert.ToInt32(file.Substring(0, 3));
                int cid = Convert.ToInt32(Convert.ToInt32(empresa));
                int loc = Convert.ToInt32(file.Substring(6, 3));

                #region Comando SQL para o BANCO DE DADOS
                sql = "SELECT ";

                p = 0;
                while (p < field.Length)
                {
                    sql += field[p].Trim() + ",";
                    p++;
                }

                sql = sql.Remove(sql.Length - 1, 1);
                sql += " FROM " + tabela;

                if ((tabela == "WTR_LIVROS") || (tabela == "WTR_LEITURAS"))
                {
                    sql += " WHERE LIV_MES = " + mes
                        + " AND CID_ID = " + cid
                        + " AND LIV_ANO = " + ano
                        + " AND LOC_ID = " + loc
                        + " AND CLI_ID = " + Convert.ToInt32(cliente);
                    if (Convert.ToInt32(empresa).ToString() != "0")
                        sql += " AND EMP_ID = " + Convert.ToInt32(empresa).ToString();

                    #region Verifica Qtd Ocorrências
                    if (Convert.ToInt32(cliente) == 5)
                    {
                        if ((tabela == "WTR_LEITURAS"))//&&(Convert.ToInt32(cliente) == 5)
                        {
                            sql2 = "SELECT LTR_ID, MSG_ID, COUNT(*) AS QTD FROM WTR_LEITURAS WHERE LIV_MES = '" + mes + "'"
                            + " AND CID_ID = '" + cid + "'"
                            + " AND LOC_ID = '" + loc + "'"
                            + " AND CLI_ID = '" + Convert.ToInt32(cliente) + "'"
                            + " GROUP BY LTR_ID, MSG_ID "
                            + " ORDER BY MSG_ID ";

                            dt2 = db.GetDt(strCon, tabela, sql2);
                        }
                    }
                    #endregion
                }
                else if (tabela.IndexOf("WTR_FATURA") >= 0)
                {
                    sql += " WHERE WTR_FATURA.FAT_MES = " + mes
                       + " AND WTR_FATURA.CLI_ID = " + Convert.ToInt32(cliente)
                       + " AND WTR_FATURA.FAT_PASTA = " + loc.ToString();
                    if (Convert.ToInt32(empresa).ToString() != "0")
                        sql += " AND WTR_FATURA.EMP_ID = " + Convert.ToInt32(empresa).ToString();

                    if (Convert.ToInt32(cliente) != 5)
                    {
                        sql2 = "SELECT LTR_ID, MSG_ID, COUNT(*) AS QTD FROM WTR_FATURA WHERE FAT_MES = " + mes
                        + " AND FAT_PASTA = '" + loc + "'"
                        + " AND CLI_ID = '" + Convert.ToInt32(cliente) + "'"
                        + " GROUP BY LTR_ID, MSG_ID "
                        + " ORDER BY MSG_ID ";

                        dt2 = db.GetDt(strCon, tabela, sql2);
                    }
                }
                else if (tabela == "WTR_SERVICOS")
                {
                    sql += " WHERE SRV_MES = " + mes
                        + " AND SRV_LOCAL = " + loc
                        + " AND CLI_ID = " + Convert.ToInt32(cliente);
                    if (Convert.ToInt32(empresa).ToString() != "0")
                        sql += " AND EMP_ID = " + Convert.ToInt32(empresa).ToString();
                }
                else if (tabela == "WTR_REGISTROLOG")
                {
                    sql += " WHERE LOG_MES = " + mes
                        + " AND LOG_LOCAL = " + loc
                        + " AND CLI_ID = " + Convert.ToInt32(cliente);
                    if (Convert.ToInt32(empresa).ToString() != "0")
                        sql += " AND EMP_ID = " + Convert.ToInt32(empresa).ToString();
                }



                dt = db.GetDt(strCon, tabExport[i].Trim(), sql);

                if (dt.Rows.Count > 0)
                {
                    msg += ("Exportando dados : " + tabExport[i].Trim().ToUpper());

                    #region MESMO ARQUIVO DE EXPORTAÇÃO PARA DUAS OU MAIS TABELAS
                    if (fileNew.Length == 0)
                    {
                        if (chkUnico)
                        {
                            string nomeArq = xml.GetConfigValue(caminhoProjeto, "LOCAL", "ARQEXPORT", Convert.ToInt32(cliente).ToString());
                            string[] pedacos = nomeArq.Split(';');
                            int tamCampo = 0;

                            for (int pp = 0; pp < pedacos.Length; pp++)
                            {
                                tamCampo = dt.Rows[0][pedacos[pp].Split(',')[0].Trim()].ToString().Length;

                                if (tamCampo < (Convert.ToInt32(pedacos[pp].Split(',')[1])))
                                    fileNew += dt.Rows[0][pedacos[pp].Split(',')[0].Trim()].ToString().PadLeft(Convert.ToInt32(pedacos[pp].Split(',')[1]), '0');
                                else
                                    fileNew += dt.Rows[0][pedacos[pp].Split(',')[0].Trim()].ToString().Substring(tamCampo - Convert.ToInt32(pedacos[pp].Split(',')[1]), Convert.ToInt32(pedacos[pp].Split(',')[1]));
                            }
                        }

                        if (fileNew.Length > 0)
                            sw = new StreamWriter(checkFileExists(cam, fileNew.Trim(), ext));
                        //sw = new StreamWriter(cam + @"\" + fileNew.Trim() + "." + ext);
                        else
                        {
                            if (cliente.Trim() == "5") //DAEM MARILIA - CEBI
                                sw = new StreamWriter(cam + @"\ROTA" + loc.ToString().Substring(0, 1).PadLeft(2, '0') + loc.ToString().PadLeft(4, '0') + "." + ext);
                            else
                                if (Convert.ToInt32(cliente) == 17)
                                    sw = new StreamWriter(checkFileExists(cam, "T" + tabExport[i].Trim() + "_" + file, ext), true, System.Text.Encoding.GetEncoding("ISO8859-1"));
                                else
                                    sw = new StreamWriter(checkFileExists(cam, tabExport[i].Trim() + "_" + file, ext), true, System.Text.Encoding.GetEncoding("ISO8859-1"));
                            //sw = new StreamWriter(cam + @"\" + tabExport[i].Trim() + "_" + file + "." + ext, true, System.Text.Encoding.GetEncoding("ISO8859-1"));
                        }
                    }
                    else
                    {
                        if (sw.Equals(StreamWriter.Null))
                        {
                            if (fileNew.Length > 0)
                                sw = new StreamWriter(cam + @"\" + fileNew.Trim() + "." + ext);
                            else
                                sw = new StreamWriter(cam + @"\" + tabExport[i].Trim() + "_" + file + "." + ext);
                        }
                    }
                    #endregion

                    for (int a = 0; a < dt.Rows.Count; a++)
                    {

                        //lblCount.Refresh();
                        string linha = string.Empty;

                        #region Leitura de Campos na Tabela
                        for (int n = 0; n < campos.Length - 1; n++)
                        {
                            if (Convert.ToInt32(tamanho[n].Trim()) > 0)
                            {
                                if (valor[n] == null)
                                {
                                    if (formato[n] == "N")
                                    {
                                        #region "N"
                                        decimal vlr = 0;
                                        if (dt.Rows[a][field[n]].ToString().Trim().Length > 0)
                                            vlr = Convert.ToDecimal(dt.Rows[a][field[n]].ToString().Replace('.', ','));
                                        if (vlr >= 0)
                                            linha += vlr.ToString().PadLeft(Convert.ToInt32(tamanho[n].Trim()), '0');
                                        else
                                        {
                                            vlr = vlr * (-1);
                                            linha += "-" + vlr.ToString().PadLeft(Convert.ToInt32(tamanho[n].Trim()) - 1, '0');
                                        }
                                        #endregion
                                    }
                                    else if (formato[n] == "T")
                                        linha += dt.Rows[a][field[n]].ToString().PadRight(Convert.ToInt32(tamanho[n].Trim()), ' ');
                                    else if (formato[n] == "Decimal")
                                    {
                                        #region "Decimal"
                                        decimal vlr = 0;
                                        if (dt.Rows[a][field[n]].ToString().Trim().Length > 0)
                                            vlr = Convert.ToDecimal(dt.Rows[a][field[n]].ToString().Replace('.', ','));
                                        if (vlr >= 0)
                                            linha += vlr.ToString().PadLeft(Convert.ToInt32(tamanho[n].Trim()), '0').Replace(',', '.');
                                        else
                                        {
                                            vlr = vlr * (-1);
                                            linha += "-" + vlr.ToString().PadLeft(Convert.ToInt32(tamanho[n].Trim()) - 1, '0').Replace(',', '.');
                                        }
                                        #endregion
                                    }
                                    else if (formato[n] == "D")
                                    {
                                        #region "D"
                                        if (Convert.ToString(dt.Rows[a][field[n]].ToString().Replace("/", "").PadRight(Convert.ToInt32(tamanho[n].Trim()), ' ')).Trim().Length <= 0)
                                        {
                                            string dataLeit = DateTime.Now.Day.ToString().PadLeft(2, '0')
                                                            + DateTime.Now.Month.ToString().PadLeft(2, '0')
                                                            + DateTime.Now.Year.ToString().PadLeft(4, '0');
                                            linha += dataLeit;
                                        }
                                        else
                                            linha += dt.Rows[a][field[n]].ToString().Replace("/", "").PadRight(Convert.ToInt32(tamanho[n].Trim()), ' ');
                                        #endregion
                                    }
                                    else if (formato[n] == "Data")
                                    {
                                        #region "Data"
                                        if (dt.Rows[a][field[n]].ToString().Length >= 10)
                                            linha += dt.Rows[a][field[n]].ToString().Substring(0, 10).PadRight(Convert.ToInt32(tamanho[n].Trim()), ' ');
                                        else
                                        {
                                            linha += dt.Rows[a][field[n]].ToString().PadRight(Convert.ToInt32(tamanho[n].Trim()), ' ');
                                        }
                                        #endregion
                                    }
                                    else if (formato[n] == "/100")
                                    {
                                        #region "/100"
                                        decimal ttmp = 0;

                                        if (dt.Rows[a][field[n]].ToString().Trim().Length > 0)
                                            ttmp = Convert.ToDecimal(dt.Rows[a][field[n]].ToString()) / 100;

                                        if (ttmp >= 0)
                                            linha += ttmp.ToString().PadLeft(Convert.ToInt32(tamanho[n].Trim()), '0');
                                        else
                                        {
                                            ttmp = ttmp * (-1);
                                            linha += "-" + ttmp.ToString().PadLeft(Convert.ToInt32(tamanho[n].Trim()) - 1, '0');
                                        }
                                        #endregion
                                    }
                                    else if (formato[n] == "Right")
                                    {
                                        #region "Right"
                                        if (dt.Rows[a][field[n]].ToString().Length > 0)
                                        {
                                            int dife = dt.Rows[a][field[n]].ToString().Length - Convert.ToInt32(tamanho[n].Trim());
                                            linha += dt.Rows[a][field[n]].ToString().Substring(dife, dt.Rows[a][field[n]].ToString().Length - dife);
                                        }
                                        else
                                            linha += dt.Rows[a][field[n]].ToString().PadLeft(Convert.ToInt32(tamanho[n].Trim(), ' '));
                                        #endregion
                                    }
                                    else if (formato[n].Contains("Left-"))
                                    {
                                        #region "Left-"
                                        if (dt.Rows[a][field[n]].ToString().Length > 0)
                                        {
                                            int ret = Convert.ToInt32(formato[n].Split('-')[1].ToString());
                                            int dife = dt.Rows[a][field[n]].ToString().Length - ret;

                                            if (formato[n].Contains("N"))
                                                linha += dt.Rows[a][field[n]].ToString().Substring(0, dife).PadLeft(Convert.ToInt32(tamanho[n].Trim()), '0');
                                            else
                                                linha += dt.Rows[a][field[n]].ToString().Substring(0, dife).PadRight(Convert.ToInt32(tamanho[n].Trim()), ' ');
                                        }
                                        else
                                        {
                                            if (formato[n].Contains("N"))
                                                linha += dt.Rows[a][field[n]].ToString().PadLeft(Convert.ToInt32(tamanho[n].Trim(), '0'));
                                            else
                                                linha += dt.Rows[a][field[n]].ToString().PadRight(Convert.ToInt32(tamanho[n].Trim(), ' '));
                                        }
                                        #endregion
                                    }
                                    else if ((formato[n] == "RightDate") && (tamanho[n] == "6"))
                                    {
                                        #region "RightDate"
                                        if (dt.Rows[a][field[n]].ToString().Length > 0)
                                        {
                                            int dife = dt.Rows[a][field[n]].ToString().Length - 8;
                                            string tpm = dt.Rows[a][field[n]].ToString().Substring(dife, dt.Rows[a][field[n]].ToString().Length - dife);
                                            tpm = tpm.Substring(0, 4) + tpm.Substring(6, 2);
                                            linha += tpm;
                                        }
                                        else
                                            linha += dt.Rows[a][field[n]].ToString().PadLeft(Convert.ToInt32(tamanho[n].Trim()), ' ');
                                        #endregion
                                    }
                                    else if (formato[n].Contains("Right"))
                                    {
                                        #region "Right"
                                        if (dt.Rows[a][field[n]].ToString().Length > 0)
                                        {
                                            int ctr = dt.Rows[a][field[n]].ToString().Length;

                                            if (formato[n].Contains("("))
                                                ctr = Convert.ToInt32(formato[n].Split('(')[1].ToString().Replace(")", ""));

                                            if (dt.Rows[a][field[n]].ToString().Length > ctr)
                                                ctr = dt.Rows[a][field[n]].ToString().Length - ctr;
                                            else
                                                ctr = dt.Rows[a][field[n]].ToString().Length;

                                            if (ctr > Convert.ToInt32(tamanho[n].Trim()))
                                                ctr = Convert.ToInt32(tamanho[n].Trim());

                                            if ((formato[n].Contains("N")) && (Convert.ToInt32(cliente) == 4))//SAAE PB
                                                linha += dt.Rows[a][field[n]].ToString().Substring(0, ctr).PadLeft(Convert.ToInt32(tamanho[n].Trim()), '0');
                                            else if (formato[n].Contains("N"))
                                                linha += dt.Rows[a][field[n]].ToString().Substring(ctr).PadRight(Convert.ToInt32(tamanho[n].Trim()), '0');
                                            else
                                                linha += dt.Rows[a][field[n]].ToString().Substring(ctr).PadLeft(Convert.ToInt32(tamanho[n].Trim()), ' ');

                                        }
                                        else
                                        {
                                            if (formato[n].Contains("N"))
                                                linha += dt.Rows[a][field[n]].ToString().PadLeft(Convert.ToInt32(tamanho[n].Trim()), '0');
                                            else
                                                linha += dt.Rows[a][field[n]].ToString().PadRight(Convert.ToInt32(tamanho[n].Trim()), ' ');
                                        }
                                        #endregion
                                    }
                                    else if (formato[n].Contains("Left"))
                                    {
                                        #region "Left"
                                        if (dt.Rows[a][field[n]].ToString().Length > 0)
                                        {
                                            int ctr = dt.Rows[a][field[n]].ToString().Length;

                                            if (formato[n].Contains("("))
                                                ctr = Convert.ToInt32(formato[n].Split('(')[1].ToString().Replace(")", ""));

                                            if (dt.Rows[a][field[n]].ToString().Length < ctr)
                                                ctr = dt.Rows[a][field[n]].ToString().Length;

                                            if (ctr > Convert.ToInt32(tamanho[n].Trim()))
                                                ctr = Convert.ToInt32(tamanho[n].Trim());

                                            string dtLeit = dt.Rows[a][field[n]].ToString().Substring(0, ctr).PadLeft(Convert.ToInt32(tamanho[n].Trim()), ' ');
                                            if (formato[n].Contains("N"))
                                                dtLeit = dt.Rows[a][field[n]].ToString().Substring(0, ctr).PadRight(Convert.ToInt32(tamanho[n].Trim()), '0');

                                            if (formato[n].Contains("Date"))
                                            {
                                                if (dtLeit.Substring(dtLeit.Length - 2, 2) == "00")
                                                    dtLeit = dtLeit.Substring(0, 4) + "20" + dtLeit.Substring(4, 2);
                                            }
                                            linha += dtLeit;
                                        }
                                        else
                                        {
                                            if (formato[n].Contains("N"))
                                                linha += dt.Rows[a][field[n]].ToString().PadLeft(Convert.ToInt32(tamanho[n].Trim()), '0');
                                            else
                                                linha += dt.Rows[a][field[n]].ToString().PadRight(Convert.ToInt32(tamanho[n].Trim()), ' ');
                                        }
                                        #endregion
                                    }
                                    else if (formato[n].Contains("Seq"))
                                    {
                                        #region "Seq"
                                        int b = a + 1;
                                        linha += b.ToString().PadLeft(Convert.ToInt32(tamanho[n]), '0');

                                        #endregion
                                    }
                                    else if (formato[n].Contains("Lado"))
                                    {
                                        #region "Lado"
                                        linha += "1";

                                        #endregion
                                    }
                                    else if (formato[n] == "S/N")
                                    {
                                        #region "S/N"
                                        if (dt.Rows[a][field[n]].ToString().Trim().Length == 0)
                                            linha += "N";
                                        else
                                            linha += dt.Rows[a][field[n]].ToString().Trim();
                                        #endregion
                                    }
                                    else if ((formato[n].Substring(0, 1) == "P") && (formato[n].Split('.').Length > 1))
                                        linha += dt.Rows[a][field[n]].ToString().Substring(Convert.ToInt32(formato[n].Split('.')[1]), Convert.ToInt32(tamanho[n]));
                                    else if (formato[n] == "SetRota")
                                    {
                                        #region "SetRota"
                                        if (Convert.ToInt32(cliente) == 6)
                                        {
                                            if (dt.Rows[a][field[n]].ToString().Substring(0, 2) == "14")
                                            {
                                                linha += Convert.ToString("14" + dt.Rows[a][field[n]].ToString().Substring(2).PadLeft(4, '0')).PadLeft(10, '0');
                                            }
                                            else
                                            {
                                                linha += Convert.ToString(dt.Rows[a][field[n]].ToString().Substring(0, 1) + dt.Rows[a][field[n]].ToString().Substring(1).PadLeft(4, '0')).PadLeft(10, '0');
                                            }
                                        }
                                        #endregion
                                    }
                                    else
                                        linha += dt.Rows[a][field[n]].ToString();

                                    if (Convert.ToInt32(tamanho[n].Trim()) < dt.Rows[a][field[n]].ToString().Trim().Length && (field[n] != "lei_data"))
                                    {
                                        if ((!formato[n].Contains("Right")) && (formato[n].Substring(0, 1) != "P") && (!formato[n].Contains("Left")))
                                        {
                                            int difff = dt.Rows[a][field[n]].ToString().Length - Convert.ToInt32(tamanho[n].Trim());
                                            if (linha.Length > difff)
                                                linha = linha.Substring(0, (linha.Length - difff));
                                        }
                                    }

                                }
                                else
                                {
                                    if (formato[n] == "N")
                                        linha += valor[n].ToString().PadLeft(Convert.ToInt32(tamanho[n].Trim()), '0');
                                    else
                                        linha += valor[n].ToString().PadRight(Convert.ToInt32(tamanho[n].Trim()), ' ');
                                }
                            }

                            if (separador.Length > 0)
                            {
                                if (Convert.ToInt32(cliente) == 1)
                                {
                                    if (tabela == "WTR_LEITURAS")
                                    {
                                        linha += separador;
                                    }
                                }
                                else
                                {
                                    linha += separador;
                                }
                            }
                        }

                        #endregion

                        linha = linha.ToUpper();
                        linha = linha.Replace("À", "A").Replace("Á", "A").Replace("Ã", "A").Replace("Â", "A").Replace("È", "E").Replace("É", "E").Replace("Ê", "E").Replace("Ì", "I").Replace("Í", "I").Replace("Ò", "O").Replace("Ó", "O").Replace("Õ", "O").Replace("Ô", "O").Replace("Ù", "U").Replace("Ú", "U").Replace("Û", "U").Replace("Ç", "C");

                        if (linha.IndexOf("#") > 0)
                        {
                            for (int s = 0; s < linha.Split('#').Length - 1; s++)
                            {
                                sw.WriteLine(linha.Split(Convert.ToChar("#"))[s] + "#");
                            }
                        }
                        else
                        {
                            sw.WriteLine(linha);
                        }
                    }

                    if (!chkUnico)
                        sw.Close();
                }
                #endregion

                msg += ("Dados da tabela " + tabExport[i].Trim().ToUpper() + " exportados com sucesso: ");

                if (!sw.Equals(StreamWriter.Null))
                    sw.Close();


                
                btDownload.Enabled = true;
                btDownload.Text = "Download: " + tabExport[i].Trim() + "_" + file + "." + ext;

                i++;


            }

            //Move nome arquivo para o outro list de backup
            //lstDirBkp.Items.Add(file);
            //lstDirExp.Items.Remove(file);


            #endregion


            #region Exibe Quantidade de Ocorrências
            if (dt2 != null)
            {
                if (dt2.DefaultView.Count > 0)
                {
                    string msgOcorrencia = "Cod.Ocorrência\tQuantidade\r\n";
                    for (int i2 = 0; i2 < dt2.DefaultView.Count; i2++)
                    {
                        msgOcorrencia += "\r\n" + dt2.DefaultView.Table.DefaultView[i2].Row["MSG_ID"].ToString()
                            + "\t\t" + dt2.DefaultView.Table.DefaultView[i2].Row["QTD"].ToString();
                    }
                    msg += (msgOcorrencia + "          Leiturista: " + dt2.DefaultView.Table.DefaultView[0].Row["LTR_ID"].ToString());
                }
            }
            #endregion



            //FileInfo arquivo;
            //String nomeArquivo;
            //nomeArquivo = Server.MapPath("EXPORT/" + Convert.ToInt32(cliente)) + "\\FATURA_" + lstDirExp.SelectedItem.Text;
            //downloadTxt(lstDirExp.SelectedItem.Text);



        }
        catch (Exception ex)
        {
            string resp = ("Erro :" + " - " + ex.Message);
            msg += (resp);
        }
        finally
        {
            sw.Close();
            //lstDirExp.Items.Remove(file);
        }
    }

    private void downloadTxt(string arquivo, string nomeArq, HttpResponse response)
    {
        try
        {

            //Response.ContentType = "application/octet-stream";
            //Response.AppendHeader("Content-Disposition", String.Format("attachment; filename={0}", nomeArq));
            //Response.TransmitFile(arquivo);
            //HttpContext.Current.ApplicationInstance.CompleteRequest();

            // Get the physical Path of the file
            string filepath = arquivo;

            // Create New instance of FileInfo class to get the properties of the file being downloaded
            FileInfo file = new FileInfo(filepath);

            // Checking if file exists
            if (file.Exists)
            {
                // Clear the content of the response
                response.ClearContent();

                // LINE1: Add the file name and attachment, which will force the open/cance/save dialog to show, to the header
                response.AddHeader("Content-Disposition", String.Format("attachment; filename={0}", file.Name));

                // Add the file size into the response header
                response.AddHeader("Content-Length", file.Length.ToString());

                // Set the ContentType
                response.ContentType = ReturnFiletype(file.Extension.ToLower());

                // Write the file into the response (TransmitFile is for ASP.NET 2.0. In ASP.NET 1.1 you have to use WriteFile instead)
                response.TransmitFile(file.FullName);

                // End the response
                response.End();

                //send statistics to the class
            }
        }
        catch (Exception ex)
        {
            // msg("Erro na download do arquivo: " + ex.Message);
        }
    }

    public static string ReturnFiletype(string fileExtension)
    {
        switch (fileExtension)
        {
            case ".htm":
            case ".html":
            case ".log":
                return "text/HTML";
            case ".txt":
                return "text/plain";
            case ".doc":
                return "application/ms-word";
            case ".tiff":
            case ".tif":
                return "image/tiff";
            case ".asf":
                return "video/x-ms-asf";
            case ".avi":
                return "video/avi";
            case ".zip":
                return "application/zip";
            case ".xls":
            case ".csv":
                return "application/vnd.ms-excel";
            case ".gif":
                return "image/gif";
            case ".jpg":
            case "jpeg":
                return "image/jpeg";
            case ".bmp":
                return "image/bmp";
            case ".wav":
                return "audio/wav";
            case ".mp3":
                return "audio/mpeg3";
            case ".mpg":
            case "mpeg":
                return "video/mpeg";
            case ".rtf":
                return "application/rtf";
            case ".asp":
                return "text/asp";
            case ".pdf":
                return "application/pdf";
            case ".fdf":
                return "application/vnd.fdf";
            case ".ppt":
                return "application/mspowerpoint";
            case ".dwg":
                return "image/vnd.dwg";
            case ".msg":
                return "application/msoutlook";
            case ".xml":
            case ".sdxl":
                return "application/xml";
            case ".xdp":
                return "application/vnd.adobe.xdp+xml";
            default:
                return "application/octet-stream";
        }
    }

    private string checkFileExists(string cam, string arquivo, string ext)
    {
        try
        {
            string file = null;
            if (System.IO.File.Exists(cam + @"\" + arquivo + "." + ext))
            {
                //Panel1.Visible = true;
                //Panel3.Enabled = false;
                //Panel4.Enabled = false;
                //lblMensagem.Text = "Arquivo ja existente. Deseja substituí-lo ?";

                //if ()//ShowMessage("Arquivo " + arquivo + " já existe. Deseja substituí-lo?"))//, "Atenção!", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) == DialogResult.Yes)
                //  {
                try
                {

                    System.IO.File.Delete(cam + @"\" + arquivo + "." + ext);
                    file = cam + @"\" + arquivo + "." + ext;
                }
                catch (System.IO.IOException e)
                {
                    Console.WriteLine(e.Message);
                }
                //}
                //else
                //{
                //InputBox input = new InputBox();
                //file = checkFileExists(cam, input.Show(arquivo, "Informe o nome do arquivo: "), ext);
                //}
            }
            else
            {
                file = cam + @"\" + arquivo + "." + ext;
            }
            return (file.Contains("." + ext) ? file : file + "." + ext);
        }
        catch (Exception ex)
        {
            msg += ex.Message;
            return "";
        }
    }

    public void AnaliseCritica(String strCon, TextBox txtLivro, DropDownList cbxMsg, DropDownList cbxMes, TextBox txtAno, TextBox txtEndereco, TextBox txtNumero,
     CheckBox chkCritica, TextBox txtPercent, Label lblMsg, DropDownList ddlSituacaoLeitura, DropDownList ddlLeituristas, TextBox txtIdentificacao,
     TextBox txtMedidor, RadioButton rbEndereço, RadioButton rbLigacao, RadioButton rbSequencia, RadioButton rbNomeContribuinte, RadioButton rbRota, GridView gridView, Panel panelMsg, Panel PanelQtdRegistros, Label lblTOTAL, RadioButton rbData)
    {
        DataTable dt = new DataTable();

        try
        {


            double porc = 0;
            string pMax = "";
            string pMin = "";

            #region Porcentagem
            if (txtPercent.Text.Trim().Length < 1)
                porc = 1;
            else
            {
                try
                {
                    porc = Convert.ToInt32(txtPercent.Text.Trim());
                    pMax = Convert.ToDecimal(1 + (porc / 100)).ToString().Replace(",", ".");
                    pMin = Convert.ToDecimal(1 - (porc / 100)).ToString().Replace(",", ".");
                }
                catch (Exception ee)
                {
                    lblMsg.Text = "Informe o percentual correto da crítica";
                }
            }
            #endregion

            string sql =
            "select LIV_ANO AS ANO, LIV_MES AS MES, CAST(lei_ligacao AS VARCHAR) AS LIGACAO,LEI_CONSUMIDOR AS CONSUMIDOR, WTR_LEITURAS.LOC_ID AS ROTA, LEI_ENDERECO AS ENDEREÇO, LEI_NUMERO AS NUMERO, LEI_DATA + ' ' +LEI_HORA ";
            sql += "AS DATA, LTR_NOME AS LEITURISTA, "
                         +
                         " MSG_DESCRICAO AS OCORRENCIA, LEI_MEDIA AS MEDIA, LEI_ANTERIOR AS 'LEIT. ANT', LEI_LEITURA AS LEITURA, "
                         + " LEI_NUMMED AS MEDIDOR,  LEI_CONSUMO,LEI_LATITUDE,LEI_LONGITUDE ";


            sql += " FROM WTR_LEITURAS INNER JOIN WTR_MENSAGENS ON(WTR_LEITURAS.MSG_ID = WTR_MENSAGENS.MSG_ID and WTR_LEITURAS.cli_id = WTR_MENSAGENS.cli_id and WTR_MENSAGENS.emp_id = WTR_LEITURAS.emp_id) INNER JOIN WTR_LEITURISTAS ON (WTR_LEITURAS.LTR_ID = WTR_LEITURISTAS.LTR_ID  and WTR_LEITURAS.cli_id = WTR_LEITURISTAS.cli_id and WTR_LEITURISTAS.emp_id = WTR_LEITURAS.emp_id) ";


            sql += "WHERE WTR_LEITURAS.emp_id =" +
                          empresa + " and WTR_LEITURAS.cid_id =" + empresa +
                          " and WTR_LEITURAS.CLI_ID = " + cliente;// Data.cli.ToString();





            #region SQL E FILTROS

            if (txtLivro.Text.Trim().Length > 0)
            {
                sql += " AND WTR_LEITURAS.LOC_ID = '" + txtLivro.Text.Trim() + "' ";
            }

            if (cbxMsg.SelectedValue.ToString() != "1000")
                sql += " AND WTR_LEITURAS.MSG_ID = '" + cbxMsg.SelectedValue.ToString() + "' ";

            if (cbxMes.SelectedIndex > 0)
                sql += " AND LIV_MES = '" + cbxMes.SelectedIndex.ToString() + "' ";

            if (txtAno.Text.Trim().Length > 0)
                sql += " AND LIV_ANO = '" + txtAno.Text.Trim() + "' ";

            if (txtEndereco.Text.Trim().Length > 0)
                sql += " AND (LEI_ENDERECO LIKE '" + txtEndereco.Text + "' or LEI_ENDERECOENTREGA LIKE '" + txtEndereco.Text + "') ";
            if (txtNumero.Text.Trim().Length > 0)
                sql += " AND (LEI_NUMERO = " + txtNumero.Text + " or LEI_NUMEROENTREGA LIKE '" + txtNumero.Text + "') ";



            #region Somente critica
            if (chkCritica.Checked)
            {
                sql += " AND (((LEI_LEITURA - LEI_ANTERIOR) > (LEI_MEDIA * " + pMax + ")) OR ((LEI_LEITURA - LEI_ANTERIOR) < (LEI_MEDIA * " + pMin + ")))";
            }
            #endregion


            #region Sem Leitura
            if (ddlSituacaoLeitura.SelectedValue.ToString() == "Com Leitura")
            {
                sql += " AND LEI_LEITURA <> ''";
            }
            else
            {
                if (ddlSituacaoLeitura.SelectedValue.ToString() == "Sem Leitura")
                {
                    sql += " AND (LEI_LEITURA = '' or lei_leitura is null)";
                }
            }
            #endregion


            if (ddlSituacaoLeitura.SelectedValue.ToString() == "Alteração cadastral")
                sql += " AND LEI_NOVACATEGORIA > 0";
            else if (ddlSituacaoLeitura.SelectedValue.ToString() == "Hidro. torto")
                sql += " AND LEI_HIDROTORTO = 'S'";

            #endregion


            #region Leituristas
            if (ddlLeituristas.SelectedValue.ToString() != "0")
            {
                sql += " AND WTR_LEITURAS.LTR_ID  =  " + ddlLeituristas.SelectedValue;
            }
            #endregion

            #region Identificação e Medidor
            if ((txtIdentificacao.Text.Trim().Length > 0) || (txtMedidor.Text.Trim().Length > 0))
            {
                if (txtIdentificacao.Text.Trim().Length > 0)
                    sql += " AND LEI_LIGACAO LIKE '%" + txtIdentificacao.Text.Trim() + "%' ";

                if (txtMedidor.Text.Trim().Length > 0)
                    sql += " AND LEI_NUMMED LIKE '%" + txtMedidor.Text.Trim() + "%' ";

            }


            #endregion

            /*if (ordeR.Length > 0)
            sql += " ORDER BY " + ordeR;
        else
        {*/
            if (rbLigacao.Checked)
                sql += " ORDER BY lei_ligacao,LIV_ANO, LIV_MES, WTR_LEITURAS.LOC_ID";
            else if (rbNomeContribuinte.Checked)
                sql += " ORDER BY lei_consumidor,LIV_ANO, LIV_MES, WTR_LEITURAS.LOC_ID";
            else if (rbData.Checked)
                sql += " ORDER BY lei_data,lei_hora,LIV_ANO, LIV_MES, WTR_LEITURAS.LOC_ID";
            else if (rbRota.Checked)
                sql += " ORDER BY WTR_LEITURAS.LOC_ID,LIV_ANO, LIV_MES,lei_seq";
            else
            {

                sql += " ORDER BY LIV_ANO, LIV_MES, ";
                sql += "lei_seq";
            }

            Data db = new Data();

            dt = db.GetDt(strCon, "WTR_LEITURAS", sql);

            if (dt.DefaultView.Count > 0)
            {
                lblTOTAL.Text = string.Format("{0:C}", Convert.ToDecimal(dt.DefaultView.Count.ToString())).Replace("R$", "").Replace(" ", "").Split(',')[0];
                PanelQtdRegistros.Visible = true;
            }
            else PanelQtdRegistros.Visible = false;

            gridView.DataSource = dt;
            gridView.DataBind();


        }
        catch (Exception ex)
        {
            lblMsg.Text = "Erro - " + ex.Message;
            panelMsg.Visible = true;

        }

    }





}