﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WaterSyncLite.Class;
using WaterSyncLite.DAL;
using WaterSyncLite.MDL;

/// <summary>
/// Summary description for SaoCaetano
/// </summary>
public class SaoCaetano
{
    public SaoCaetano()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    String msg = "";
    string cliente = "17";
    string empresa = "01";
    public int Import(string cam, string file, string rota, string mes, string ano, string caminhoProjeto, string leiturista, String strCon, GridView GridView1, TextBox txtStatus)
    {

        #region Buscando informações no arquivo Import.xml
        Import import2;
        ImportDAL impDAL = new ImportDAL();
        Import import = new Import();
        string separador = xml.GetConfigValue(caminhoProjeto, "LOCAL", "SEPARADOR", cliente);
        String conexaoStr = strCon;
        Data db = new Data();
        int countUnder = file.Split('_').Length;

        //Tratamento de houver underline
        /*if (countUnder > 1)
            import = impDAL.getImp(Convert.ToInt32(Convert.ToInt32(cliente)), file.Split('_')[0].Trim());
        else
        {
            if (file.Length > 4)
                    import = impDAL.getImp(Convert.ToInt32(Convert.ToInt32(cliente)), file.Remove(file.Length - 4, 4));
            else
                import = impDAL.getImp(Convert.ToInt32(Convert.ToInt32(cliente)), file);
        }*/

        #endregion



        FileStream ofs = File.Open(cam + file, FileMode.Open, FileAccess.Read, FileShare.None);
        StreamReader sr = new StreamReader(ofs, System.Text.Encoding.GetEncoding("ISO-8859-1"));	//Aceita caracteres especiais.
        string linha = string.Empty;
        int i = 0;
        string sql;
        string tabela = "";
        int p = 0;
        string[] campos = null;
        string[] field = null;
        string[] valor = null;
        string idLinha = "";
        string erro = "";
        int seq = 0;
        string tipdebRef = "";
        string mesRef = DateTime.Now.Month.ToString().PadLeft(2, '0');
        string anoRef = DateTime.Now.Year.ToString().PadLeft(4, '0');


        string[] valoresRes = new string[6];
        string[] valoresCom = new string[6];
        string[] valoresInd = new string[6];

        string[] consumoRes = new string[6];
        string[] consumoCom = new string[6];
        string[] consumoInd = new string[6];
        string roteiro = "";
        string ltrid = "";
        string qtdRoteiro = "";
        int contRota = 0;
        string msg1 = "";
        string msg2 = "";
        string msg3 = "";
        string porcEsgoto = "";
        string versaoArquivo = "";
        string seqArquivo = "";

        #region Verifica se grupo ja foi enviado

        String sqlVerifica = "select * from wtr_livros where liv_mes = " + file.Substring(3, 2) + " and liv_ano = " + file.Substring(5, 4) + " and grupo_id = " + file.Substring(1, 2);
        DataTable dtLivro = db.GetDt(strCon, "wtr_livros", sqlVerifica);
        if (dtLivro.DefaultView.Count > 0)
        {
            txtStatus.Text += "O roteiro selecionado ja existe.";
            return -1;
        }

        #endregion


        int linhaC = 0;		//CETIL - Controle de linhas de Registro C (discriminação dos serviços)

        #region Variáveis antigas DESABILITADO
        /*
			string mes="";
			string cid="";
			string loc="";
			string seq="";
			string dtleitura="";
			string leiturista = "";
			int contLeituras = 0;
			int DSM = 0;
			int DCM = 0;
			*/
        #endregion

        try
        {

            deletarDadosAntigosSaoCaetano(rota, mes, ano, strCon);
            while (sr.Peek() >= 0)
            {
                i++;

                //lblCount.Refresh();
                linha = string.Empty;
                linha = sr.ReadLine();

                //Retira o separador de campo - Na importação não deve ter.
                if (separador.Trim().Length < 1)
                    linha = linha.Replace("|", " ").Replace(";", " ");

                linha = linha.PadRight(141, ' ');
                linha = linha.Replace("'", " ");		//Alteração feita em 07Dez2004 - Evita erro de SQL com apóstofro - Padovani.
                linha = linha.Replace("\r\n", " ");	//Alteração feita em 24Mar2005 - Evita erro com <Enter> - Padovani.
                int len = linha.Length;
                string reg = linha.Substring(0, 1);
                bool check = false;


                //#region Leitura de Conteúdo do Arquivo

                /*if ((linha.Substring(0, 2) == "03"))
                {
                    import = impDAL.getImp(Convert.ToInt32(Convert.ToInt32(cliente)), "MENSAGENS");
                    campos = import.Campos.Split('|');
                    check = true;
                }
                else*/
                if ((linha.Substring(0, 1) == "0")) // WTR_FAIXA
                {
                    ano = linha.Substring(1, 4);
                    mes = linha.Substring(5, 2);
                    string seqLivro = linha.Substring(7, 6);

                    int pos = 13;

                    for (int f = 0; f < 6; f++, pos += 8)
                    {
                        valoresRes[f] = linha.Substring(pos, 8);

                    }
                    for (int f = 0; f < 6; f++, pos += 8)
                    {
                        valoresCom[f] = linha.Substring(pos, 8);

                    }
                    for (int f = 0; f < 6; f++, pos += 8)
                    {
                        valoresInd[f] = linha.Substring(pos, 8);

                    }


                    string tarifaSocial = linha.Substring(pos, 8);
                    pos += 8;

                    for (int f = 0; f < 6; f++, pos += 4)
                    {
                        consumoRes[f] = linha.Substring(pos, 4);

                    }
                    for (int f = 0; f < 6; f++, pos += 4)
                    {
                        consumoCom[f] = linha.Substring(pos, 4);

                    }
                    for (int f = 0; f < 6; f++, pos += 4)
                    {
                        consumoInd[f] = linha.Substring(pos, 4);

                    }
                    porcEsgoto = linha.Substring(pos, 4);
                    string grupo = linha.Substring(241, 2);
                    versaoArquivo = linha.Substring(243, 2);
                    seqArquivo = linha.Substring(7, 6);





                    string sqlFaixas = "";
                    for (int f = 0; f < 6; f++)
                    {
                        #region Residencial
                        if (f == 0)
                            sqlFaixas =
                                "insert into wtr_faixa(cli_id,emp_id,cat_id,fxa_id,fxa_inicio,fxa_fim,fxa_vragua) values (17,1,1," +
                                f + "," + 0 + "," + consumoRes[f] + "," +
                                valoresRes[f].Substring(0, valoresRes[f].Length - 2) + "." +
                                valoresRes[f].Substring(valoresRes[f].Length - 2, 2) + ")";
                        else
                        {
                            sqlFaixas =
                               "insert into wtr_faixa(cli_id,emp_id,cat_id,fxa_id,fxa_inicio,fxa_fim,fxa_vragua) values (17,1,1," +
                               f + "," + (Convert.ToInt32(consumoRes[f - 1]) + 1) + "," + (Convert.ToInt32(consumoRes[f])) + "," +
                               valoresRes[f].Substring(0, valoresRes[f].Length - 2) + "." +
                               valoresRes[f].Substring(valoresRes[f].Length - 2, 2) + ")";

                        }
                        bool resultado = db.Exe(strCon, "wtr_faixa", sqlFaixas);

                        if (!resultado)
                            msg += "Erro ao inserir faixa de valor.";

                        #endregion Residencial

                        #region Comercial
                        if (f == 0)
                            sqlFaixas =
                                "insert into wtr_faixa(cli_id,emp_id,cat_id,fxa_id,fxa_inicio,fxa_fim,fxa_vragua) values (17,1,2," +
                                f + "," + 0 + "," + consumoCom[f] + "," +
                                valoresCom[f].Substring(0, valoresCom[f].Length - 2) + "." +
                                valoresCom[f].Substring(valoresCom[f].Length - 2, 2) + ")";
                        else
                        {
                            sqlFaixas =
                               "insert into wtr_faixa(cli_id,emp_id,cat_id,fxa_id,fxa_inicio,fxa_fim,fxa_vragua) values (17,1,2," +
                               f + "," + (Convert.ToInt32(consumoCom[f - 1]) + 1) + "," + (Convert.ToInt32(consumoCom[f])) + "," +
                               valoresCom[f].Substring(0, valoresCom[f].Length - 2) + "." +
                               valoresCom[f].Substring(valoresCom[f].Length - 2, 2) + ")";

                        }
                        resultado = db.Exe(strCon, "wtr_faixa", sqlFaixas);

                        if (!resultado)
                            msg += "Erro ao inserir faixa de valor.";

                        #endregion Comercial

                        #region Industrial
                        if (f == 0)
                            sqlFaixas =
                                "insert into wtr_faixa(cli_id,emp_id,cat_id,fxa_id,fxa_inicio,fxa_fim,fxa_vragua) values (17,1,3," +
                                f + "," + 0 + "," + consumoInd[f] + "," +
                                valoresInd[f].Substring(0, valoresInd[f].Length - 2) + "." +
                                valoresInd[f].Substring(valoresInd[f].Length - 2, 2) + ")";
                        else
                        {
                            sqlFaixas =
                               "insert into wtr_faixa(cli_id,emp_id,cat_id,fxa_id,fxa_inicio,fxa_fim,fxa_vragua) values (17,1,3," +
                               f + "," + (Convert.ToInt32(consumoInd[f - 1]) + 1) + "," + (Convert.ToInt32(consumoInd[f])) + "," +
                               valoresInd[f].Substring(0, valoresInd[f].Length - 2) + "." +
                               valoresInd[f].Substring(valoresInd[f].Length - 2, 2) + ")";

                        }
                        resultado = db.Exe(strCon, "wtr_faixa", sqlFaixas);

                        if (!resultado)
                            msg += "Erro ao inserir faixa de valor.";

                        #endregion Industrial
                    }

                }

                else if ((linha.Substring(0, 1) == "1"))
                {

                    import = impDAL.getImp(Convert.ToInt32(Convert.ToInt32(cliente)), "LEITURAS");
                    campos = import.Campos.Split('|');
                    seq++;



                    int total = GridView1.Rows.Count;
                    int contador;



                    for (contador = 0; contador < total; contador++)
                    {
                        if (GridView1.Rows[contador].RowType == DataControlRowType.DataRow)
                        {

                            if (contador == contRota)
                            {
                                qtdRoteiro = ((Label)GridView1.Rows[contador].FindControl("lblQtd")).Text;
                                //[contador].Cells[1].Text;
                                roteiro = ((Label)GridView1.Rows[contador].FindControl("lblRota")).Text;
                                ltrid = ((DropDownList)GridView1.Rows[contador].FindControl("ddlLeiturista")).SelectedValue;

                                if (Convert.ToInt32(qtdRoteiro) < seq)
                                {

                                    seq = 1;



                                }
                                else
                                {
                                    if (Convert.ToInt32(qtdRoteiro) == seq)
                                    {
                                        contRota++;
                                    }

                                }
                                break;

                            }




                        }

                    }

                    check = true;


                }
                else if ((linha.Substring(0, 1) == "6"))
                {
                    msg1 = linha.Substring(3, 62);
                    msg2 = linha.Substring(65, 62);
                    msg3 = linha.Substring(127, 62);

                    check = false;


                }
                else if ((linha.Substring(0, 1) == "7"))
                {
                    import = impDAL.getImp(Convert.ToInt32(Convert.ToInt32(cliente)), "INDICE");
                    campos = import.Campos.Split('|');
                    check = true;


                }
                else if ((linha.Substring(0, 1) == "8"))
                {
                    import = impDAL.getImp(Convert.ToInt32(Convert.ToInt32(cliente)), "QUITACAO");
                    campos = import.Campos.Split('|');
                    check = true;


                }
                else if ((linha.Substring(0, 1) == "2"))
                {
                    import = impDAL.getImp(Convert.ToInt32(Convert.ToInt32(cliente)), "SERVICO");
                    campos = import.Campos.Split('|');
                    check = true;
                }
                else if ((linha.Substring(0, 1) == "3"))
                {
                    import = impDAL.getImp(Convert.ToInt32(Convert.ToInt32(cliente)), "2VIA");
                    campos = import.Campos.Split('|');
                    check = true;
                }/*
                else if ((linha.Substring(0, 2) == "08"))
                {
                    seq++;
                    import = impDAL.getImp(Convert.ToInt32(Convert.ToInt32(cliente)), "LEITURAS");
                    campos = import.Campos.Split('|');
                    check = true;
                }
                else if ((linha.Substring(0, 2) == "02"))
                {

                    import = impDAL.getImp(Convert.ToInt32(Convert.ToInt32(cliente)), "INDICE");
                    campos = import.Campos.Split('|');
                    check = true;
                }
                */

                if (check)
                {
                    field = new string[campos.Length - 1];
                    valor = new string[campos.Length - 1];
                    tabela = campos[0];

                    p = 0;
                    try
                    {

                        while (p < (campos.Length - 1))
                        {
                            field[p] = campos[p + 1].Split(';')[0].Trim();
                            /*if (field[p].ToString() == "LEI_BAIRRO" || field[p].ToString() == "LEI_PERIODO1" || field[p].ToString() == "LEI_ANTERIOR" || field[p].ToString().ToUpper() == "IND_COR")
                            {
                                linha = string.Empty;
                                linha = sr.ReadLine();

                                //Retira o separador de campo - Na importação não deve ter.
                                if (separador.Trim().Length < 1)
                                    linha = linha.Replace("|", " ").Replace(";", " ");

                                linha = linha.PadRight(141, ' ');
                                linha = linha.Replace("'", " ");		//Alteração feita em 07Dez2004 - Evita erro de SQL com apóstofro - Padovani.
                                linha = linha.Replace("\r\n", " ");	//Alteração feita em 24Mar2005 - Evita erro com <Enter> - Padovani.
                                len = linha.Length;

                            }*/

                            if (separador.Trim().Length > 0)
                            {
                                char separa = Convert.ToChar(separador.Trim());

                                if (campos[p + 1].Split(';').Length > 3)
                                {
                                    valor[p] = campos[p + 1].Split(';')[3].Trim().Replace('"', ' ');
                                    valor[p] = valor[p].Trim();

                                    if (valor[p].Trim() == "seq")
                                        valor[p] = seq.ToString();
                                    else if (valor[p].Trim() == ".leiturista")
                                        valor[p] = ltrid;

                                }
                                else
                                    valor[p] = linha.Split(separa)[p].ToString();

                                if (campos[p + 1].Split(';').Length > 2)
                                {
                                    if (Convert.ToInt32(campos[p + 1].Split(';')[2].ToString().Trim()) > 0)
                                        valor[p] = valor[p].Substring(0, Convert.ToInt32(campos[p + 1].Split(';')[2].ToString().Trim()));
                                }
                            }
                            else
                            {
                                if (campos[p + 1].Split(';').Length > 3)	//Valor fixo
                                {
                                    if (campos[p + 1].Split(';')[3].Trim() == "'Data'")
                                    {
                                        valor[p] = linha.Substring(Convert.ToInt32(campos[p + 1].Split(';')[1].Trim()), Convert.ToInt32(campos[p + 1].Split(';')[2].Trim()));
                                        valor[p] = valor[p].Substring(0, 2) + "/" + valor[p].Substring(2, 2) + "/" + valor[p].Substring(4, 4);
                                    }
                                    else if (campos[p + 1].Split(';')[3].Trim() == ".leiturista")
                                        valor[p] = ltrid;//cbxLeit.SelectedValue.ToString();
                                    else if (campos[p + 1].Split(';')[3].Trim() == ".local")
                                        valor[p] = rota.Trim();
                                    else if (campos[p + 1].Split(';')[3].Trim() == "'/100'")
                                    {
                                        valor[p] = linha.Substring(Convert.ToInt32(campos[p + 1].Split(';')[1].Trim()), Convert.ToInt32(campos[p + 1].Split(';')[2].Trim()));
                                        if (valor[p].Trim() != "")
                                            if (valor[p].Trim().Contains("-"))
                                                valor[p] = "-" + Convert.ToString(Convert.ToDecimal(valor[p].Replace("-", "")) / 100).Replace(',', '.');
                                            else valor[p] = Convert.ToString(Convert.ToDecimal(valor[p]) / 100).Replace(',', '.');
                                    }
                                    else if (campos[p + 1].Split(';')[3].Trim() == ".seq")
                                    {
                                        valor[p] = seq.ToString();

                                    }
                                    else if (campos[p + 1].Split(';')[3].Trim() == ".Trim")
                                    {
                                        valor[p] = linha.Substring(Convert.ToInt32(campos[p + 1].Split(';')[1].Trim()), Convert.ToInt32(campos[p + 1].Split(';')[2].Trim()));
                                        valor[p] = valor[p].Replace(" ", "");
                                    }
                                    else if (campos[p + 1].Split(';')[3].Trim() == ".faixa")
                                    {
                                        valor[p] = linha.Substring(Convert.ToInt32(campos[p + 1].Split(';')[1].Trim()), Convert.ToInt32(campos[p + 1].Split(';')[2].Trim()));
                                        valor[p] = valor[p].Replace(".", ",");
                                    }
                                    else if (campos[p + 1].Split(';')[3].Trim() == ".Rota")
                                    {
                                        valor[p] = roteiro; //rota;

                                    }
                                    else if (campos[p + 1].Split(';')[3].Trim() == ".Grupo")
                                    {
                                        valor[p] = rota;

                                    }
                                    else if (campos[p + 1].Split(';')[3].Trim() == ".Mes")
                                    {
                                        valor[p] = mes;

                                    }
                                    else if (campos[p + 1].Split(';')[3].Trim() == ".Ano")
                                    {
                                        valor[p] = ano;

                                    }

                                    else if (campos[p + 1].Split(';')[3].Trim() == ".Num")
                                    {
                                        valor[p] = linha.Substring(Convert.ToInt32(campos[p + 1].Split(';')[1].Trim()), Convert.ToInt32(campos[p + 1].Split(';')[2].Trim()));
                                        if (valor[p].Trim().Length < 1)
                                            valor[p] = "0";

                                        valor[p] = valor[p].Replace(".", "");   //Retirar ponto de milhar
                                        valor[p] = valor[p].Replace(",", ".");  //Substituir vírgula de centavos

                                        if (valor[p].IndexOf("-") > 0)
                                        {
                                            valor[p] = valor[p].Replace("-", "");
                                            valor[p] = "-" + valor[p];
                                        }
                                    }
                                    else if (campos[p + 1].Split(';')[3].Trim() == ".Txt/daea")
                                    {
                                        valor[p] = linha.Substring(Convert.ToInt32(campos[p + 1].Split(';')[1].Trim()), Convert.ToInt32(campos[p + 1].Split(';')[2].Trim()));
                                        valor[p] = valor[p].Replace("LV1", "LVW").Replace("LV2", "LVY").Replace("LV3", "LVX");
                                        valor[p] = valor[p].Replace("0", "").Replace("1", "").Replace("2", "").Replace("3", "").Replace("4", "").Replace("5", "").Replace("6", "").Replace("7", "").Replace("8", "").Replace("9", "");
                                        valor[p] = valor[p].Replace("LVW", "LV1").Replace("LVY", "LV2").Replace("LVX", "LV3");
                                        valor[p] = valor[p].Trim().Replace(" ", "/");
                                    }
                                    else if (campos[p + 1].Split(';')[3].Trim() == ".Numeros")
                                    {
                                        valor[p] = linha.Substring(Convert.ToInt32(campos[p + 1].Split(';')[1].Trim()), Convert.ToInt32(campos[p + 1].Split(';')[2].Trim()));
                                        valor[p] = SomenteNumeros(valor[p].Trim());
                                    }
                                    else if (campos[p + 1].Split(';')[3].Trim() == ".Num/daea")
                                    {
                                        valor[p] = linha.Substring(Convert.ToInt32(campos[p + 1].Split(';')[1].Trim()), Convert.ToInt32(campos[p + 1].Split(';')[2].Trim()));
                                        valor[p] = valor[p].ToUpper().Replace("RES", "").Replace("COM", "").Replace("IND", "").Replace("PUB", "").Replace("SOC", "").Replace("CIV", "").Replace("PAR", "").Replace("LV1", "").Replace("LV2", "").Replace("LV3", "").Replace("NES", "");
                                        valor[p] = valor[p].Trim().Replace(" ", "/");
                                    }
                                    else if (campos[p + 1].Split(';')[3].Trim() == ".stsDAEA")
                                    {
                                        valor[p] = linha.Substring(Convert.ToInt32(campos[p + 1].Split(';')[1].Trim()), Convert.ToInt32(campos[p + 1].Split(';')[2].Trim()));
                                        valor[p] = valor[p].ToUpper().Replace("T", "1").Replace("F", "2").Replace("D", "3").Replace("E", "4");
                                    }
                                    else if (campos[p + 1].Split(';')[3].Trim() == ".declara")
                                    {
                                        valor[p] = linha.Substring(Convert.ToInt32(campos[p + 1].Split(';')[1].Trim()), Convert.ToInt32(campos[p + 1].Split(';')[2].Trim()));
                                        if (valor[p].Trim().Length > 0)
                                            valor[p] = "1";
                                    }
                                    else if (campos[p + 1].Split(';')[3].Trim() == ".N/0,S/1")
                                    {
                                        valor[p] = linha.Substring(Convert.ToInt32(campos[p + 1].Split(';')[1].Trim()), Convert.ToInt32(campos[p + 1].Split(';')[2].Trim()));
                                        valor[p] = valor[p].ToUpper().Replace("S", "1").Replace("N", "0");
                                    }
                                    else if ((campos[p + 1].Split(';')[3].Trim().Length > 5) && (campos[p + 1].Split(';')[3].Trim().Substring(0, 6) == ".Split"))
                                    {
                                        valor[p] = linha.Substring(Convert.ToInt32(campos[p + 1].Split(';')[1].Trim()), Convert.ToInt32(campos[p + 1].Split(';')[2].Trim()));
                                        if ((Convert.ToInt32(cliente) == 11) && (field[p].ToUpper() == "FAT_MES"))
                                        {
                                            valor[p] = valor[p].ToUpper();
                                            valor[p] = valor[p].Replace("JANEIRO", "01").Replace("FEVEREIRO", "02").Replace("MARÇO", "03").Replace("ABRIL", "04").Replace("MAIO", "05").Replace("JUNHO", "06").Replace("JULHO", "07").Replace("AGOSTO", "08").Replace("SETEMBRO", "09").Replace("OUTUBRO", "10").Replace("NOVEMBRO", "11").Replace("DEZEMBRO", "12").Replace("MARCO", "03");
                                        }

                                        char separ = Convert.ToChar(campos[p + 1].Split(';')[3].Trim().Substring(8, 1));
                                        int posic = Convert.ToInt32(campos[p + 1].Split(';')[3].Trim().Substring(12, 1));
                                        valor[p] = valor[p].Split(separ)[posic].Trim();
                                    }
                                    else
                                        valor[p] = campos[p + 1].Split(';')[3].Replace('"', ' ').Trim();
                                }
                                else
                                    valor[p] = linha.Substring(Convert.ToInt32(campos[p + 1].Split(';')[1].Trim()), Convert.ToInt32(campos[p + 1].Split(';')[2].Trim()));
                            }
                            p++;
                        }


                    }
                    catch (Exception ex)
                    {
                        txtStatus.Text += ex.Message;
                    }

                    //#endregion

                    #region CETIL
                    if (Convert.ToInt32(cliente) == 3)		//DAE Iepe - CETIL
                    {

                        if (idLinha != "Z")
                            continue;
                    }
                    #endregion

                    #region Comando SQL para o BANCO DE DADOS
                    sql = "INSERT INTO " + tabela + " (";

                    p = 0;
                    while (p < field.Length)
                    {
                        if (p > 0)
                        {
                            if (field[p - 1].Trim() != field[p].Trim())
                                sql += field[p].Trim() + ",";
                        }
                        else
                            sql += field[p].Trim() + ",";
                        p++;
                    }

                    sql = sql.Replace("A#", "");
                    sql = sql.Replace("B#", "");
                    sql = sql.Replace("C#", "");
                    sql = sql.Replace("D#", "");
                    sql = sql.Replace("E#", "");


                    sql = sql.Remove(sql.Length - 1, 1);
                    sql += ") VALUES ('";

                    p = 0;
                    while (p < valor.Length)
                    {
                        if (valor[p] != null)
                        {
                            if (p > 0)
                            {
                                if (field[p - 1].Trim() == field[p].Trim())
                                    sql = sql.Remove(sql.Length - 3, 3);

                                sql += valor[p].Trim() + "','";
                            }
                            else
                            {
                                sql += valor[p].Trim() + "','";
                            }
                        }
                        else
                            sql += "','";

                        p++;
                    }

                    sql = sql.Remove(sql.Length - 2, 2);
                    sql += ")";
                    try
                    {
                        erro = db.Ins(conexaoStr, sql);
                    }
                    catch (Exception ex)
                    {
                        msg += "Erro SQL - " + ex.Message;
                    }
                    if (erro.Trim().Length > 0)
                    {
                        if ((Convert.ToInt32(cliente) != 11) || ((Convert.ToInt32(cliente) == 11) && (tabela != "WTR_FAIXA")))
                        {
                            msg += "\r\n" + "Arquivo: " + file.Trim() + " - Registro: " + seq + " - Erro: " + erro;
                            //txtStatus.Refresh();
                        }
                    }

                    #endregion


                }


            }



            string tipo = "U";
            //if (chkRural.Checked)
            //  tipo = "R";

            if ((Convert.ToInt32(cliente) == 11) && (file.Substring(0, 1) != "A"))
                return -1;

            #region Geração do LIVRO.
            if (i > 0)
            {
                int contador;
                int total = GridView1.Rows.Count;
                for (contador = 0; contador < total; contador++)
                {
                    if (GridView1.Rows[contador].RowType == DataControlRowType.DataRow)
                    {
                        string sql2 = "INSERT WTR_LIVROS VALUES('"
                                        + cliente + "','"
                                        + "1" + "','"
                                        + mes.Trim() + "','"
                                        + mes.Trim() + "','"
                                        + ano + "','1','"
                                        + ((Label)GridView1.Rows[contador].FindControl("lblRota")).Text + "','"
                                        + "','"
                                        + DateTime.Now.Day.ToString().ToString().Trim() + "/" +
                                        mes.Trim() + "/" +
                                        ano + "','"
                                        + ((Label)GridView1.Rows[contador].FindControl("lblQtd")).Text + "','"
                                        + "0','"
                                        + ((Label)GridView1.Rows[contador].FindControl("lblQtd")).Text + "','"
                                        + "0','"
                                        + DateTime.Now.ToShortDateString() + "','"
                                        + ((DropDownList)GridView1.Rows[contador].FindControl("ddlLeiturista")).SelectedValue + "','"
                                        + "0','"
                                        + "0','"
                                        + tipo + "','"
                                        + "0','"
                                        + "N','"
                                        + msg1 + "','"
                                        + msg2 + "','"
                                        + msg3 + "',"
                                        + rota + ",'"
                                        + porcEsgoto + "','"
                                        + versaoArquivo + "','"
                                        + seqArquivo + "')";
                        string erro2 = db.Ins(conexaoStr, sql2);
                    }
                }
                //fileFTP += sql2 + "\r\n";
            }
            #endregion

            /*lblCount.Text = lblQtdLigacoes.Text;
            txtQtdRotas.Enabled = false;
            btGerar.Enabled = false;
            GridView1.DataSource = null;
            GridView1.DataBind();*/

            if (Convert.ToInt32(cliente) == 17)
                txtStatus.Text += "\r\n" + "Arquivo carregado com sucesso. " + i + " ligações.";
            else txtStatus.Text += "\r\n" + "Arquivo carregado com sucesso. " + seq + " ligações.";
            return 0;
        }
        catch (Exception ex)
        {
            txtStatus.Text += (seq + " #" + linha + " - " + ex.Message);
            return -1;
        }
        finally
        {
            sr.Close();
            ofs.Close();

        }
    }

    public void deletarDadosAntigosSaoCaetano(string rota, string mes, string ano, string strCon)
    {
        //DELETAR DADOS DAS TABELAS
        //WTR_HISTORICO
        //WTR_SERVICOS
        //WTR_FAIXA
        //WTR_MENSAGENS
        Data db = new Data();

        db.Exe(strCon, "wtr_2via", "delete from wtr_2via where grupo_id = " + rota); //+ " AND liv_mes =" + Request.Cookies["mes"].Value.ToString().Trim() + " AND liv_ano =" + ano);
        db.Exe(strCon, "WTR_SERVICO", "delete from WTR_SERVICO where grupo_id = " + rota + " AND srv_mes =" + mes + " AND srv_ano =" + ano);
        db.Exe(strCon, "WTR_FAIXA", "delete from WTR_FAIXA ");
        db.Exe(strCon, "WTR_INDICE", "delete from WTR_INDICE where grupo_id = " + rota + " AND IND_MES =" + mes + " AND IND_ANO =" + ano);


    }

    //FUNÇÃO QUE RETORNA SOMENTE NÚMEROS DE UMA STRING
    public string SomenteNumeros(string toNormalize)
    {
        string resultString = string.Empty;
        Regex regexObj = new Regex(@"[^\d]");
        resultString = regexObj.Replace(toNormalize, "");
        return resultString;
    }


    public void Export(string cam, string file, string cliente, string caminhoProjeto, String strCon,  HttpResponse response,Button btDownload,int mes, int ano)
    {
        string separador = xml.GetConfigValue(caminhoProjeto, "LOCAL", "EXPSEPARADOR", Convert.ToInt32(cliente).ToString());
        DataTable dt;
        DataTable dt2 = null;
        string ext = "";
        if (cliente == "3")
            ext = "s";
        else if ((cliente == "4") || (cliente == "10"))
            ext = "RET";
        else
            ext = "txt";
        string sql = "";
        string sql2 = "";//Busca Qtd Ocorrências
        string empresa = "1";
        Export export = new Export();
        ExportDAL expDAL = new ExportDAL();
        int p = 0;
        int i = 0;
        string fileNew = "";
        StreamWriter sw = StreamWriter.Null;
        int filler1 = 0;
        int filler2 = 0;
        Data db = new Data();


        try
        {

            #region Buscando informações no arquivo Import.xml

            //Verificar quais tabelas a serem exportadas (Config.xml : <TABEXPORT>)

            string[] tabExport = xml.GetConfigValue(caminhoProjeto, "LOCAL", "TABEXPORT", Convert.ToInt32(cliente).ToString()).Split(',');

            while (i < tabExport.Length)
            {
                export = expDAL.getExp(Convert.ToInt32(cliente), tabExport[i].Trim());

                #region Leitura de Conteúdo do Arquivo de Exportação
                string[] campos = export.Campos.Split('|');
                string tabela = campos[0];
                string[] field = new string[campos.Length - 1];
                string[] tamanho = new string[campos.Length - 1];
                string[] formato = new string[campos.Length - 1];
                string[] valor = new string[campos.Length - 1];

                p = 0;
                while (p < (campos.Length - 1))
                {
                    field[p] = campos[p + 1].Split(';')[0].Trim();
                    formato[p] = campos[p + 1].Split(';')[1].Trim();
                    tamanho[p] = campos[p + 1].Split(';')[2].Trim();

                    if (campos[p + 1].Split(';').Length > 3)	//Valor fixo
                    {
                        valor[p] = campos[p + 1].Split(';')[3].Replace('"', ' ').Trim();
                    }

                    p++;
                }
                #endregion

                int cid = Convert.ToInt32("001");
                int loc = Convert.ToInt32(file.Substring(6, 3));

                #region Comando SQL para o BANCO DE DADOS
                sql = "SELECT ";

                p = 0;
                while (p < field.Length)
                {
                    sql += field[p].Trim() + ",";
                    p++;
                }

                sql = sql.Remove(sql.Length - 1, 1);
                sql += " FROM " + tabela;

                if ((tabela == "WTR_LIVROS") || (tabela == "WTR_LEITURAS"))
                {
                    sql += " WHERE LIV_MES = " + mes
                        + " AND LIV_ANO = " + ano
                        + " AND CID_ID = " + cid
                        + " AND GRUPO_ID = " + loc
                        + " AND CLI_ID = " + cliente;
                    if (empresa != "0")
                        sql += " AND EMP_ID = " + empresa;

                    #region Verifica Qtd Ocorrências
                    if (cliente == "5")
                    {
                        if ((tabela == "WTR_LEITURAS"))//&&(cliente == 5)
                        {
                            sql2 = "SELECT LTR_ID, MSG_ID, COUNT(*) AS QTD FROM WTR_LEITURAS WHERE LIV_MES = '" + mes + "'"
                            + " AND CID_ID = '" + cid + "'"
                            + " AND LOC_ID = '" + loc + "'"
                            + " AND CLI_ID = '" + cliente + "'"
                            + " GROUP BY LTR_ID, MSG_ID "
                            + " ORDER BY MSG_ID ";

                            dt2 = db.GetDt(strCon, tabela, sql2);
                        }
                    }
                    #endregion
                }
                else if (tabela.IndexOf("WTR_FATURA") >= 0)
                {
                    sql += " WHERE WTR_FATURA.FAT_MES = " + mes
                       + " AND WTR_FATURA.CLI_ID = " + cliente
                       + " AND WTR_FATURA.FAT_PASTA = " + loc.ToString();
                    if (empresa != "0")
                        sql += " AND WTR_FATURA.EMP_ID = " + empresa;

                    if (cliente != "5")
                    {
                        sql2 = "SELECT LTR_ID, MSG_ID, COUNT(*) AS QTD FROM WTR_FATURA WHERE FAT_MES = " + mes
                        + " AND FAT_PASTA = '" + loc + "'"
                        + " AND CLI_ID = '" + cliente + "'"
                        + " GROUP BY LTR_ID, MSG_ID "
                        + " ORDER BY MSG_ID ";

                        dt2 = db.GetDt(strCon, tabela, sql2);
                    }
                }
                else if (tabela == "WTR_SERVICOS")
                {
                    sql += " WHERE SRV_MES = " + mes
                        + " AND SRV_LOCAL = " + loc
                        + " AND CLI_ID = " + cliente;
                    if (empresa != "0")
                        sql += " AND EMP_ID = " + empresa;
                }
                else if (tabela == "WTR_REGISTROLOG")
                {
                    sql += " WHERE LOG_MES = " + mes
                        + " AND LOG_LOCAL = " + loc
                        + " AND CLI_ID = " + cliente;
                    if (empresa != "0")
                        sql += " AND EMP_ID = " + empresa;
                }


                dt = db.GetDt(strCon, tabExport[i].Trim(), sql);

                if (dt.Rows.Count > 0)
                {
                    msg += "Exportando dados : " + tabExport[i].Trim().ToUpper();

                    #region MESMO ARQUIVO DE EXPORTAÇÃO PARA DUAS OU MAIS TABELAS
                    if (fileNew.Length == 0)
                    {

                        string nomeArq = xml.GetConfigValue(caminhoProjeto, "LOCAL", "ARQEXPORT", Convert.ToInt32(cliente).ToString());
                        string[] pedacos = nomeArq.Split(';');
                        int tamCampo = 0;

                        for (int pp = 0; pp < pedacos.Length; pp++)
                        {
                            tamCampo = dt.Rows[0][pedacos[pp].Split(',')[0].Trim()].ToString().Length;

                            if (tamCampo < (Convert.ToInt32(pedacos[pp].Split(',')[1])))
                                fileNew += dt.Rows[0][pedacos[pp].Split(',')[0].Trim()].ToString().PadLeft(Convert.ToInt32(pedacos[pp].Split(',')[1]), '0');
                            else
                                fileNew += dt.Rows[0][pedacos[pp].Split(',')[0].Trim()].ToString().Substring(tamCampo - Convert.ToInt32(pedacos[pp].Split(',')[1]), Convert.ToInt32(pedacos[pp].Split(',')[1]));
                        }


                        if (cliente == "5") //DAEM MARILIA - CEBI
                            sw = new StreamWriter(cam + @"\ROTA" + loc.ToString().Substring(0, 1).PadLeft(2, '0') + loc.ToString().PadLeft(4, '0') + "." + ext);
                        else
                            if (Convert.ToInt32(cliente) == 17)
                                sw = new StreamWriter(checkFileExists(cam, "T" + tabExport[i].Trim() + "_" + file, ext), true, System.Text.Encoding.GetEncoding("ISO8859-1"));
                            else
                                sw = new StreamWriter(checkFileExists(cam, tabExport[i].Trim() + "_" + file, ext), true, System.Text.Encoding.GetEncoding("ISO8859-1"));
                    }
                    else
                    {
                        if (sw.Equals(StreamWriter.Null))
                        {
                            if (fileNew.Length > 0)
                                sw = new StreamWriter(cam + @"\" + fileNew.Trim() + "." + ext);
                            else
                                sw = new StreamWriter(cam + @"\" + tabExport[i].Trim() + "_" + file + "." + ext);
                        }
                    }
                    #endregion

                    for (int a = 0; a < dt.Rows.Count; a++)
                    {
                        //lblCount.Text = a.ToString();
                        //lblCount.Refresh();
                        string linha = string.Empty;

                        #region Leitura de Campos na Tabela
                        for (int n = 0; n < campos.Length - 1; n++)
                        {
                            if (Convert.ToInt32(tamanho[n].Trim()) > 0)
                            {
                                if (valor[n] == null)
                                {
                                    if (formato[n] == "N")
                                    {
                                        #region "N"
                                        decimal vlr = 0;
                                        if (dt.Rows[a][field[n]].ToString().Trim().Length > 0)
                                            vlr = Convert.ToDecimal(dt.Rows[a][field[n]].ToString().Replace('.', ','));
                                        if (vlr >= 0)
                                            linha += vlr.ToString().PadLeft(Convert.ToInt32(tamanho[n].Trim()), '0');
                                        else
                                        {
                                            vlr = vlr * (-1);
                                            linha += "-" + vlr.ToString().PadLeft(Convert.ToInt32(tamanho[n].Trim()) - 1, '0');
                                        }
                                        #endregion
                                    }
                                    else if (formato[n] == "T")
                                        linha += dt.Rows[a][field[n]].ToString().PadRight(Convert.ToInt32(tamanho[n].Trim()), ' ');
                                    else if (formato[n] == "Decimal")
                                    {
                                        #region "Decimal"
                                        decimal vlr = 0;
                                        if (dt.Rows[a][field[n]].ToString().Trim().Length > 0)
                                            vlr = Convert.ToDecimal(dt.Rows[a][field[n]].ToString().Replace('.', ','));
                                        if (vlr >= 0)
                                            linha += vlr.ToString().PadLeft(Convert.ToInt32(tamanho[n].Trim()), '0').Replace(',', '.');
                                        else
                                        {
                                            vlr = vlr * (-1);
                                            linha += "-" + vlr.ToString().PadLeft(Convert.ToInt32(tamanho[n].Trim()) - 1, '0').Replace(',', '.');
                                        }
                                        #endregion
                                    }
                                    else if (formato[n] == "Decimal2")
                                    {
                                        #region "Decimal"
                                        decimal vlr = 0;
                                        if (dt.Rows[a][field[n]].ToString().Trim().Length > 0)
                                            vlr = Convert.ToDecimal(dt.Rows[a][field[n]].ToString().Replace('.', ','));
                                        if (vlr >= 0)
                                            linha += vlr.ToString().Replace(".", "").Replace(",", "").PadLeft(Convert.ToInt32(tamanho[n].Trim()), '0');
                                        else
                                        {
                                            vlr = vlr * (-1);
                                            linha += "-" + vlr.ToString().Replace(".", "").Replace(",", "").PadLeft(Convert.ToInt32(tamanho[n].Trim()) - 1, '0');
                                        }
                                        #endregion
                                    }
                                    else if (formato[n] == "D")
                                    {
                                        #region "D"
                                        if (Convert.ToString(dt.Rows[a][field[n]].ToString().Replace("/", "").PadRight(Convert.ToInt32(tamanho[n].Trim()), ' ')).Trim().Length <= 0)
                                        {
                                            string dataLeit = DateTime.Now.Day.ToString().PadLeft(2, '0')
                                                            + DateTime.Now.Month.ToString().PadLeft(2, '0')
                                                            + DateTime.Now.Year.ToString().PadLeft(4, '0');
                                            linha += dataLeit;
                                        }
                                        else
                                            linha += dt.Rows[a][field[n]].ToString().Replace("/", "").PadRight(Convert.ToInt32(tamanho[n].Trim()), ' ');
                                        #endregion
                                    }
                                    else if (formato[n] == "Data")
                                    {
                                        #region "Data"
                                        if (dt.Rows[a][field[n]].ToString().Length >= 10)
                                            linha += dt.Rows[a][field[n]].ToString().Substring(0, 10).PadRight(Convert.ToInt32(tamanho[n].Trim()), ' ');
                                        else
                                        {
                                            linha += dt.Rows[a][field[n]].ToString().PadRight(Convert.ToInt32(tamanho[n].Trim()), ' ');
                                        }
                                        #endregion
                                    }
                                    else if (formato[n] == "Impresso")
                                    {
                                        #region "Impresso"
                                        if (dt.Rows[a]["LEI_RETERCONTA"].ToString() == "S")
                                            linha += 'N';
                                        else
                                        {
                                            linha += dt.Rows[a]["LEI_IMPRESSO"].ToString();
                                        }
                                        #endregion
                                    }
                                    else if (formato[n] == "Analise")
                                    {
                                        #region "Impresso"
                                        if (dt.Rows[a]["GRUPO_ID"].ToString() == "99" || dt.Rows[a]["GRUPO_ID"].ToString() == "88" || dt.Rows[a]["GRUPO_ID"].ToString() == "77")
                                            linha += '1';
                                        else
                                        {
                                            linha += dt.Rows[a]["LEI_ANALISE"].ToString();
                                        }
                                        #endregion
                                    }
                                    else if (formato[n] == "DataAno")
                                    {
                                        #region "DataAno"
                                        if (dt.Rows[a][field[n]].ToString().Length >= 10)
                                            linha += Convert.ToString(dt.Rows[a][field[n]].ToString().Replace("/", "").Substring(6, 2) +
                                                    dt.Rows[a][field[n]].ToString().Replace("/", "").Substring(4, 2) +
                                                    dt.Rows[a][field[n]].ToString().Replace("/", "").Substring(0, 4)).PadRight(Convert.ToInt32(tamanho[n].Trim()), ' ');
                                        else
                                        {
                                            linha += Convert.ToString(dt.Rows[a][field[n]].ToString().Replace("/", "").Substring(6, 2) +
                                                    dt.Rows[a][field[n]].ToString().Replace("/", "").Substring(4, 2) +
                                                    dt.Rows[a][field[n]].ToString().Replace("/", "").Substring(0, 4)).PadRight(Convert.ToInt32(tamanho[n].Trim()), ' ');
                                        }
                                        #endregion
                                    }
                                    else if (formato[n] == "Ano2Num")
                                    {
                                        linha += dt.Rows[a]["LIV_ANO"].ToString().Substring(2, 2);

                                    }
                                    else if (formato[n] == "/100")
                                    {
                                        #region "/100"
                                        decimal ttmp = 0;

                                        if (dt.Rows[a][field[n]].ToString().Trim().Length > 0)
                                            ttmp = Convert.ToDecimal(dt.Rows[a][field[n]].ToString()) / 100;

                                        if (ttmp >= 0)
                                            linha += ttmp.ToString().PadLeft(Convert.ToInt32(tamanho[n].Trim()), '0');
                                        else
                                        {
                                            ttmp = ttmp * (-1);
                                            linha += "-" + ttmp.ToString().PadLeft(Convert.ToInt32(tamanho[n].Trim()) - 1, '0');
                                        }
                                        #endregion
                                    }
                                    else if (formato[n] == "Right")
                                    {
                                        #region "Right"
                                        if (dt.Rows[a][field[n]].ToString().Length > 0)
                                        {
                                            int dife = dt.Rows[a][field[n]].ToString().Length - Convert.ToInt32(tamanho[n].Trim());
                                            linha += dt.Rows[a][field[n]].ToString().Substring(dife, dt.Rows[a][field[n]].ToString().Length - dife);
                                        }
                                        else
                                            linha += dt.Rows[a][field[n]].ToString().PadLeft(Convert.ToInt32(tamanho[n].Trim(), ' '));
                                        #endregion
                                    }
                                    else if (formato[n].Contains("Left-"))
                                    {
                                        #region "Left-"
                                        if (dt.Rows[a][field[n]].ToString().Length > 0)
                                        {
                                            int ret = Convert.ToInt32(formato[n].Split('-')[1].ToString());
                                            int dife = dt.Rows[a][field[n]].ToString().Length - ret;

                                            if (formato[n].Contains("N"))
                                                linha += dt.Rows[a][field[n]].ToString().Substring(0, dife).PadLeft(Convert.ToInt32(tamanho[n].Trim()), '0');
                                            else
                                                linha += dt.Rows[a][field[n]].ToString().Substring(0, dife).PadRight(Convert.ToInt32(tamanho[n].Trim()), ' ');
                                        }
                                        else
                                        {
                                            if (formato[n].Contains("N"))
                                                linha += dt.Rows[a][field[n]].ToString().PadLeft(Convert.ToInt32(tamanho[n].Trim(), '0'));
                                            else
                                                linha += dt.Rows[a][field[n]].ToString().PadRight(Convert.ToInt32(tamanho[n].Trim(), ' '));
                                        }
                                        #endregion
                                    }
                                    else if ((formato[n] == "RightDate") && (tamanho[n] == "6"))
                                    {
                                        #region "RightDate"
                                        if (dt.Rows[a][field[n]].ToString().Length > 0)
                                        {
                                            int dife = dt.Rows[a][field[n]].ToString().Length - 8;
                                            string tpm = dt.Rows[a][field[n]].ToString().Substring(dife, dt.Rows[a][field[n]].ToString().Length - dife);
                                            tpm = tpm.Substring(0, 4) + tpm.Substring(6, 2);
                                            linha += tpm;
                                        }
                                        else
                                            linha += dt.Rows[a][field[n]].ToString().PadLeft(Convert.ToInt32(tamanho[n].Trim()), ' ');
                                        #endregion
                                    }
                                    else if (formato[n].Contains("Right"))
                                    {
                                        #region "Right"
                                        if (dt.Rows[a][field[n]].ToString().Length > 0)
                                        {
                                            int ctr = dt.Rows[a][field[n]].ToString().Length;

                                            if (formato[n].Contains("("))
                                                ctr = Convert.ToInt32(formato[n].Split('(')[1].ToString().Replace(")", ""));

                                            if (dt.Rows[a][field[n]].ToString().Length > ctr)
                                                ctr = dt.Rows[a][field[n]].ToString().Length - ctr;
                                            else
                                                ctr = dt.Rows[a][field[n]].ToString().Length;

                                            if (ctr > Convert.ToInt32(tamanho[n].Trim()))
                                                ctr = Convert.ToInt32(tamanho[n].Trim());

                                            if ((formato[n].Contains("N")) && (Convert.ToInt32(cliente) == 4))//SAAE PB
                                                linha += dt.Rows[a][field[n]].ToString().Substring(0, ctr).PadLeft(Convert.ToInt32(tamanho[n].Trim()), '0');
                                            else if (formato[n].Contains("N"))
                                                linha += dt.Rows[a][field[n]].ToString().Substring(ctr).PadRight(Convert.ToInt32(tamanho[n].Trim()), '0');
                                            else
                                                linha += dt.Rows[a][field[n]].ToString().Substring(ctr).PadLeft(Convert.ToInt32(tamanho[n].Trim()), ' ');

                                        }
                                        else
                                        {
                                            if (formato[n].Contains("N"))
                                                linha += dt.Rows[a][field[n]].ToString().PadLeft(Convert.ToInt32(tamanho[n].Trim()), '0');
                                            else
                                                linha += dt.Rows[a][field[n]].ToString().PadRight(Convert.ToInt32(tamanho[n].Trim()), ' ');
                                        }
                                        #endregion
                                    }
                                    else if (formato[n].Contains("Left"))
                                    {
                                        #region "Left"
                                        if (dt.Rows[a][field[n]].ToString().Length > 0)
                                        {
                                            int ctr = dt.Rows[a][field[n]].ToString().Length;

                                            if (formato[n].Contains("("))
                                                ctr = Convert.ToInt32(formato[n].Split('(')[1].ToString().Replace(")", ""));

                                            if (dt.Rows[a][field[n]].ToString().Length < ctr)
                                                ctr = dt.Rows[a][field[n]].ToString().Length;

                                            if (ctr > Convert.ToInt32(tamanho[n].Trim()))
                                                ctr = Convert.ToInt32(tamanho[n].Trim());

                                            string dtLeit = dt.Rows[a][field[n]].ToString().Substring(0, ctr).PadLeft(Convert.ToInt32(tamanho[n].Trim()), ' ');
                                            if (formato[n].Contains("N"))
                                                dtLeit = dt.Rows[a][field[n]].ToString().Substring(0, ctr).PadRight(Convert.ToInt32(tamanho[n].Trim()), '0');

                                            if (formato[n].Contains("Date"))
                                            {
                                                if (dtLeit.Substring(dtLeit.Length - 2, 2) == "00")
                                                    dtLeit = dtLeit.Substring(0, 4) + "20" + dtLeit.Substring(4, 2);
                                            }
                                            linha += dtLeit;
                                        }
                                        else
                                        {
                                            if (formato[n].Contains("N"))
                                                linha += dt.Rows[a][field[n]].ToString().PadLeft(Convert.ToInt32(tamanho[n].Trim()), '0');
                                            else
                                                linha += dt.Rows[a][field[n]].ToString().PadRight(Convert.ToInt32(tamanho[n].Trim()), ' ');
                                        }
                                        #endregion
                                    }
                                    else if (formato[n].Contains("Seq"))
                                    {
                                        #region "Seq"
                                        int b = a + 1;
                                        linha += b.ToString().PadLeft(Convert.ToInt32(tamanho[n]), '0');

                                        #endregion
                                    }
                                    else if (formato[n].Contains("Lado"))
                                    {
                                        #region "Lado"
                                        linha += "1";

                                        #endregion
                                    }
                                    else if (formato[n] == "S/N")
                                    {
                                        #region "S/N"
                                        if (dt.Rows[a][field[n]].ToString().Trim().Length == 0)
                                            linha += "N";
                                        else
                                            linha += dt.Rows[a][field[n]].ToString().Trim();
                                        #endregion
                                    }
                                    else if ((formato[n].Substring(0, 1) == "P") && (formato[n].Split('.').Length > 1))
                                        linha += dt.Rows[a][field[n]].ToString().Substring(Convert.ToInt32(formato[n].Split('.')[1]), Convert.ToInt32(tamanho[n]));
                                    else if (formato[n] == "SetRota")
                                    {
                                        #region "SetRota"
                                        if (Convert.ToInt32(cliente) == 6)
                                        {
                                            if (dt.Rows[a][field[n]].ToString().Substring(0, 2) == "14")
                                            {
                                                linha += Convert.ToString("14" + dt.Rows[a][field[n]].ToString().Substring(2).PadLeft(4, '0')).PadLeft(10, '0');
                                            }
                                            else
                                            {
                                                linha += Convert.ToString(dt.Rows[a][field[n]].ToString().Substring(0, 1) + dt.Rows[a][field[n]].ToString().Substring(1).PadLeft(4, '0')).PadLeft(10, '0');
                                            }
                                        }
                                        #endregion
                                    }
                                    else
                                        linha += dt.Rows[a][field[n]].ToString();

                                    if (Convert.ToInt32(tamanho[n].Trim()) < dt.Rows[a][field[n]].ToString().Trim().Length && ((field[n] != "lei_data") && (field[n] != "liv_ano") && formato[n] != "Impresso"))
                                    {
                                        if ((!formato[n].Contains("Right")) && (formato[n].Substring(0, 1) != "P") && (!formato[n].Contains("Left")))
                                        {
                                            int difff = dt.Rows[a][field[n]].ToString().Length - Convert.ToInt32(tamanho[n].Trim());
                                            if (linha.Length > difff)
                                                linha = linha.Substring(0, (linha.Length - difff));
                                        }
                                    }

                                }
                                else
                                {
                                    if (formato[n] == "N")
                                        linha += valor[n].ToString().PadLeft(Convert.ToInt32(tamanho[n].Trim()), '0');
                                    else
                                        linha += valor[n].ToString().PadRight(Convert.ToInt32(tamanho[n].Trim()), ' ');
                                }
                            }

                            if (separador.Length > 0)
                            {
                                if (Convert.ToInt32(cliente) == 1)
                                {
                                    if (tabela == "WTR_LEITURAS")
                                    {
                                        linha += separador;
                                    }
                                }
                                else
                                {
                                    linha += separador;
                                }
                            }
                        }

                        #endregion

                        linha = linha.ToUpper();
                        linha = linha.Replace("À", "A").Replace("Á", "A").Replace("Ã", "A").Replace("Â", "A").Replace("È", "E").Replace("É", "E").Replace("Ê", "E").Replace("Ì", "I").Replace("Í", "I").Replace("Ò", "O").Replace("Ó", "O").Replace("Õ", "O").Replace("Ô", "O").Replace("Ù", "U").Replace("Ú", "U").Replace("Û", "U").Replace("Ç", "C");

                        //if (linha.IndexOf("#") > 0)
                        //{
                        //    for (int s = 0; s < linha.Split('#').Length - 1; s++)
                        //    {
                        //        sw.WriteLine(linha.Split(Convert.ToChar("#"))[s] + "#");
                        //    }
                        //}
                        //else
                        //{
                        if (a == 0)
                        {
                            DataTable dtLivros = db.GetDt(strCon, "WTR_LIVROS",
                                "select * from wtr_livros where liv_mes = " + dt.Rows[a]["liv_mes"].ToString() +
                                " and liv_ano = " + dt.Rows[a]["liv_ano"].ToString() + " and loc_id = " +
                                dt.Rows[a]["LOC_ID"].ToString());
                            string linhaLivros = "0";
                            linhaLivros += " ".PadLeft(26, ' ');
                            linhaLivros += dtLivros.Rows[0]["GRUPO_ID"].ToString().PadLeft(2, ' ');
                            linhaLivros += dtLivros.Rows[0]["LIV_MES"].ToString().PadLeft(2, '0');
                            linhaLivros += dtLivros.Rows[0]["LIV_ANO"].ToString().Substring(2, 2).PadLeft(2, '0');
                            linhaLivros += dtLivros.Rows[0]["LIV_DATALEIT"].ToString().Split('/')[0].PadLeft(2, '0') + dtLivros.Rows[0]["LIV_DATALEIT"].ToString().Split('/')[1].PadLeft(2, '0') + dtLivros.Rows[0]["LIV_DATALEIT"].ToString().Split('/')[2].Substring(2, 2);
                            linhaLivros += dt.Rows[a]["LEI_VENCIMENTO"].ToString().Substring(6, 2) + dt.Rows[a]["LEI_VENCIMENTO"].ToString().Substring(4, 2) + dt.Rows[a]["LEI_VENCIMENTO"].ToString().Substring(2, 2);
                            linhaLivros += dtLivros.Rows[0]["GRUPO_ID"].ToString().PadLeft(2, '0');
                            linhaLivros += dtLivros.Rows[0]["LIV_SEQARQUIVO"].ToString().PadLeft(6, '0');
                            linhaLivros += " ".PadLeft(7, '0');
                            linhaLivros += dtLivros.Rows[0]["LIV_VERSAO"].ToString().PadLeft(2, '0');
                            sw.WriteLine(linhaLivros);


                        }

                        sw.WriteLine(linha);

                        //DADOS DA SEGUNDA VIA
                        linha = string.Empty;
                        string sql2Via = "select loc_id,liv_mes,liv_ano,lei_ligacao,lei_impresso from wtr_2via where lei_ligacao like '" + dt.Rows[a]["LEI_LIGACAO"].ToString() + "' and emp_id = 1 and cli_id = 17";
                        DataTable dt2Via = db.GetDt(strCon, "WTR_2VIA", sql2Via);



                        if (dt2Via.DefaultView.Count > 0)
                        {
                            int g = 0;
                            filler2++;
                            linha = "2";
                            linha += dt.Rows[a]["LEI_ZONA"].ToString().PadLeft(2, ' ');
                            linha += dt.Rows[a]["LEI_QUADRA"].ToString().PadLeft(4, ' ');
                            linha += dt.Rows[a]["LEI_LOTE"].ToString().PadLeft(3, ' ');
                            linha += dt.Rows[a]["LEI_LIGACAO"].ToString().PadLeft(8, '0');
                            linha += dt.Rows[a]["GRUPO_ID"].ToString().PadLeft(2, '0');
                            linha += dt.Rows[a]["LIV_MES"].ToString().PadLeft(2, '0');
                            linha += dt.Rows[a]["LIV_ANO"].ToString().PadLeft(4, '0');
                            linha += "0"; //IMPRIMIU DEBITOS ANTERIORES ?


                            for (g = 0; g < dt2Via.DefaultView.Count; g++)
                            {


                                linha += dt2Via.DefaultView[g].Row["LIV_ANO"].ToString().PadLeft(4, '0');
                                linha += dt2Via.DefaultView[g].Row["LIV_MES"].ToString().PadLeft(2, '0');
                                if (dt2Via.DefaultView[g].Row["LEI_IMPRESSO"].ToString().Trim() == "S")
                                    linha += "1";
                                else
                                    linha += "0";



                            }

                            for (; g < 7 && g >= dt2Via.DefaultView.Count; g++)
                            {

                                linha += "0000";
                                linha += "00";
                                linha += "0";

                            }




                            sw.WriteLine(linha);
                        }






                        //}
                    }


                    string Ultimalinha = string.Empty;
                    Ultimalinha = "9";
                    Ultimalinha += dt.DefaultView.Count.ToString().PadLeft(6, '0');
                    Ultimalinha += filler2.ToString().PadLeft(6, '0');
                    Ultimalinha += "0".PadLeft(6, '0');
                    Ultimalinha += "1".PadLeft(6, '0');
                    sw.WriteLine(Ultimalinha);



                }
                #endregion

                msg += "Dados da tabela " + tabExport[i].Trim().ToUpper() + " exportados com sucesso: ";





                if (!sw.Equals(StreamWriter.Null))
                    sw.Close();

               
                /*Download down = new Download(cam + @"\T" + tabExport[i].Trim() + "_" + file + "." + ext, file, response, context);

                // Create a thread to execute the task, and then
                // start the thread.
                Thread t = new Thread(new ThreadStart(down.Executa));
                t.Start();
                //downloadTxt(cam + @"\T"Q + tabExport[i].Trim() + "_" + file + "." + ext, file, response);
                t.Join();*/
           
                //ScriptManager.RegisterStartupScript(control, typeof(string), "OPEN_WINDOW", "var Mleft = (screen.width/2)-(760/2);var Mtop = (screen.height/2)-(700/2);window.open( 'download.aspx', null, 'height=700,width=760,status=yes,toolbar=no,scrollbars=yes,menubar=no,location=no,top=\'+Mtop+\', left=\'+Mleft+\'' );", true);

               // response.Redirect("Download.aspx",false);

                btDownload.Enabled = true;
                btDownload.Text = "Download: "+"T" + tabExport[i].Trim() + "_" + file + "." + ext;
               
                i++;


            }

            //Move nome arquivo para o outro list de backup
            //lstDirBkp.Items.Add(file);
            //lstDirExp.Items.Remove(file);


            #endregion


            #region Exibe Quantidade de Ocorrências
            if (dt2 != null)
            {
                if (dt2.DefaultView.Count > 0)
                {
                    string msgOcorrencia = "Cod.Ocorrência\tQuantidade\r\n";
                    for (int i2 = 0; i2 < dt2.DefaultView.Count; i2++)
                    {
                        msgOcorrencia += "\r\n" + dt2.DefaultView.Table.DefaultView[i2].Row["MSG_ID"].ToString()
                            + "\t\t" + dt2.DefaultView.Table.DefaultView[i2].Row["QTD"].ToString();
                    }
                    msg += msgOcorrencia + "          Leiturista: " + dt2.DefaultView.Table.DefaultView[0].Row["LTR_ID"].ToString();
                }
            }
            #endregion



            //FileInfo arquivo;
            //String nomeArquivo;
            //nomeArquivo = Server.MapPath("EXPORT/" + Convert.ToInt32(cliente)) + "\\FATURA_" + lstDirExp.SelectedItem.Text;
            //downloadTxt(lstDirExp.SelectedItem.Text);



        }
        catch (Exception ex)
        {
            string resp = ("Erro :" + " - " + ex.Message);
            msg += (resp);
        }
        finally
        {
            sw.Close();
            //lstDirExp.Items.Remove(file);
        }
    }


    public class Download
    {
        // State information used in the task.
        private string arquivo;
        private string nomeArq;
        private HttpResponse page;
        private HttpContext context;

        // The constructor obtains the state information.
        public Download(string arquivo, string nomeArq, HttpResponse page, HttpContext context)
        {
            this.arquivo = arquivo;
            this.nomeArq = nomeArq;
            this.page = page;
            this.context = context;
        }

        // The thread procedure performs the task, such as formatting
        // and printing a document.
        public void Executa()
        {
            try
            {

                //Response.ContentType = "application/octet-stream";
                //Response.AppendHeader("Content-Disposition", String.Format("attachment; filename={0}", nomeArq));
                //Response.TransmitFile(arquivo);
                //HttpContext.Current.ApplicationInstance.CompleteRequest();

                // Get the physical Path of the file
                string filepath = arquivo;

                // Create New instance of FileInfo class to get the properties of the file being downloaded
                FileInfo file = new FileInfo(filepath);

                // Checking if file exists
                if (file.Exists)
                {
                    // Clear the content of the response
                    page.ClearContent();

                    // LINE1: Add the file name and attachment, which will force the open/cance/save dialog to show, to the header
                    page.AddHeader("Content-Disposition", String.Format("attachment; filename={0}", file.Name));

                    // Add the file size into the response header
                    page.AddHeader("Content-Length", file.Length.ToString());

                    // Set the ContentType
                    page.ContentType = ReturnFiletype(file.Extension.ToLower());

                    // Write the file into the response (TransmitFile is for ASP.NET 2.0. In ASP.NET 1.1 you have to use WriteFile instead)
                    page.Write(file.FullName);

                    // End the response
                   
                    page.Flush();
                    page.End();
                    


                    //send statistics to the class
                }
            }
            catch (Exception ex)
            {
                // msg("Erro na download do arquivo: " + ex.Message);
            }
            finally
            {
                //context.ApplicationInstance.CompleteRequest();
                //ScriptManager.RegisterStartupScript(Me, Me.GetType(), "redirectScript", "window.location.href='whateverurlhere.aspx';", True)
            }
        }
    }

   
    private void downloadTxt(string arquivo, string nomeArq, HttpResponse response)
    {
        try
        {

            //Response.ContentType = "application/octet-stream";
            //Response.AppendHeader("Content-Disposition", String.Format("attachment; filename={0}", nomeArq));
            //Response.TransmitFile(arquivo);
            //HttpContext.Current.ApplicationInstance.CompleteRequest();

            // Get the physical Path of the file
            string filepath = arquivo;

            // Create New instance of FileInfo class to get the properties of the file being downloaded
            FileInfo file = new FileInfo(filepath);

            // Checking if file exists
            if (file.Exists)
            {
                // Clear the content of the response
                response.ClearContent();

                // LINE1: Add the file name and attachment, which will force the open/cance/save dialog to show, to the header
                response.AddHeader("Content-Disposition", String.Format("attachment; filename={0}", file.Name));

                // Add the file size into the response header
                response.AddHeader("Content-Length", file.Length.ToString());

                // Set the ContentType
                response.ContentType = ReturnFiletype(file.Extension.ToLower());

                // Write the file into the response (TransmitFile is for ASP.NET 2.0. In ASP.NET 1.1 you have to use WriteFile instead)
                response.TransmitFile(file.FullName);

                // End the response
                response.End();



                //send statistics to the class
            }
        }
        catch (Exception ex)
        {
            // msg("Erro na download do arquivo: " + ex.Message);
        }
    }

    public static string ReturnFiletype(string fileExtension)
    {
        switch (fileExtension)
        {
            case ".htm":
            case ".html":
            case ".log":
                return "text/HTML";
            case ".txt":
                return "text/plain";
            case ".doc":
                return "application/ms-word";
            case ".tiff":
            case ".tif":
                return "image/tiff";
            case ".asf":
                return "video/x-ms-asf";
            case ".avi":
                return "video/avi";
            case ".zip":
                return "application/zip";
            case ".xls":
            case ".csv":
                return "application/vnd.ms-excel";
            case ".gif":
                return "image/gif";
            case ".jpg":
            case "jpeg":
                return "image/jpeg";
            case ".bmp":
                return "image/bmp";
            case ".wav":
                return "audio/wav";
            case ".mp3":
                return "audio/mpeg3";
            case ".mpg":
            case "mpeg":
                return "video/mpeg";
            case ".rtf":
                return "application/rtf";
            case ".asp":
                return "text/asp";
            case ".pdf":
                return "application/pdf";
            case ".fdf":
                return "application/vnd.fdf";
            case ".ppt":
                return "application/mspowerpoint";
            case ".dwg":
                return "image/vnd.dwg";
            case ".msg":
                return "application/msoutlook";
            case ".xml":
            case ".sdxl":
                return "application/xml";
            case ".xdp":
                return "application/vnd.adobe.xdp+xml";
            default:
                return "application/octet-stream";
        }
    }

    private string checkFileExists(string cam, string arquivo, string ext)
    {
        try
        {
            string file = null;
            if (System.IO.File.Exists(cam + @"\" + arquivo + "." + ext))
            {
                //Panel1.Visible = true;
                //Panel3.Enabled = false;
                //Panel4.Enabled = false;
                //lblMensagem.Text = "Arquivo ja existente. Deseja substituí-lo ?";

                //if ()//ShowMessage("Arquivo " + arquivo + " já existe. Deseja substituí-lo?"))//, "Atenção!", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) == DialogResult.Yes)
                //  {
                try
                {

                    System.IO.File.Delete(cam + @"\" + arquivo + "." + ext);
                    file = cam + @"\" + arquivo + "." + ext;
                }
                catch (System.IO.IOException e)
                {
                    Console.WriteLine(e.Message);
                }
                //}
                //else
                //{
                //InputBox input = new InputBox();
                //file = checkFileExists(cam, input.Show(arquivo, "Informe o nome do arquivo: "), ext);
                //}
            }
            else
            {
                file = cam + @"\" + arquivo + "." + ext;
            }
            return (file.Contains("." + ext) ? file : file + "." + ext);
        }
        catch (Exception ex)
        {
            msg += ex.Message;
            return "";
        }
    }

    public void Entrega(String StrCon, DropDownList ddlLtr, DropDownList cbxMes, TextBox txtAno,DropDownList ddlRelatorio, TextBox txtIdentificacao, TextBox txtGrupo, 
        Label lblTOTAL, Panel PanelQtdRegistros, GridView gridFormaEntrega)
    {
        DataTable dt = new DataTable();
        try
        {
            Data db = new Data();
            String where = "";
            if (ddlLtr.SelectedValue.ToString() != "0" || cbxMes.SelectedValue.ToString() != "0" || txtAno.Text.Length > 0)
            {
                where += " where  wtr_leituras.cli_id = " + cliente;
                where += "  and wtr_leituras.emp_id = " + empresa;
                if (txtAno.Text.Length > 0 && cbxMes.SelectedIndex.ToString() == "0")
                {
                    if (ddlLtr.SelectedValue.ToString() != "0")
                        where += " and wtr_leituras.ltr_id =  " + ddlLtr.SelectedValue.ToString();
                }
                else
                {
                    if (ddlLtr.SelectedValue.ToString() != "0")
                        where += " and wtr_leituras.ltr_id =  " + ddlLtr.SelectedValue.ToString();
                    if (cbxMes.SelectedIndex.ToString() != "0")
                        where += " and wtr_leituras.liv_mes =  " + cbxMes.SelectedIndex.ToString();
                    if (txtAno.Text.Length > 0)
                        where += " and wtr_leituras.liv_ano =  " + txtAno.Text;
                    if (txtIdentificacao.Text.Length > 0)
                        where += " and wtr_leituras.wtr_ligacao =  " + txtIdentificacao.Text;
                    if (txtGrupo.Text.Length > 0)
                        where += " and wtr_leituras.grupo_id =  " + txtGrupo.Text;
                    if (ddlRelatorio.SelectedValue.ToString() == "Retidas")
                        where += " and wtr_leituras.lei_reterconta = 'S' and wtr_leituras.lei_tipobanco != '99' ";
                    if (ddlRelatorio.SelectedValue.ToString() == "Protocoladas")
                        where += " and wtr_leituras.lei_tipobanco = '99' ";


                }
            }

            string sql = "SELECT WTR_LEITURAS.LTR_ID, LTR_NOME,LEI_CONSUMIDOR,LEI_ENDERECO, LEI_NUMERO, LIV_MES, LIV_ANO,LEI_LIGACAO, LOC_ID, GRUPO_ID, LEI_ENTREGAFATURA, LEI_DATAENTREGA FROM WTR_LEITURAS"
                            + " INNER JOIN WTR_LEITURISTAS ON WTR_LEITURISTAS.LTR_ID = WTR_LEITURAS.LEI_LTRENTREGA " + where;

            sql += " order by wtr_leituras.liv_ano,wtr_leituras.liv_mes,wtr_leituras.loc_id";

            dt = db.GetDt(StrCon, "WTR_LEIT", sql);

            if (dt.DefaultView.Count > 0)
            {
                lblTOTAL.Text = string.Format("{0:C}", Convert.ToDecimal(dt.DefaultView.Count.ToString())).Replace("R$", "").Replace(" ", "").Split(',')[0];
                PanelQtdRegistros.Visible = true;
            }
            else PanelQtdRegistros.Visible = false;

            gridFormaEntrega.DataSource = dt;
            gridFormaEntrega.DataBind();
        //    Session["dt"] = dt;
        }
        catch (Exception ex)
        {
            
            

        }
    }

    public void AnaliseCritica(String strCon, TextBox txtLivro, DropDownList cbxMsg, DropDownList cbxMes, TextBox txtAno, TextBox txtEndereco, TextBox txtNumero,
      CheckBox chkCritica, TextBox txtPercent, Label lblMsg, DropDownList ddlSituacaoLeitura, DropDownList ddlLeituristas, TextBox txtIdentificacao,
      TextBox txtMedidor, RadioButton rbEnderecoOrdem, RadioButton rbLigacao, RadioButton rbSequencia, RadioButton rbNomeContribuinte, RadioButton rbRota, 
        GridView gridView, Panel panelMsg, DropDownList ddlTipoRelatorio, TextBox txtGrupo, Repeater RepeaterRolLeitura, System.Web.UI.HtmlControls.HtmlGenericControl PanelGrid,
        System.Web.UI.HtmlControls.HtmlGenericControl PanelRolLeituras, Panel PanelDetalhado, Repeater RepeaterDetalahado, Label lblTOTAL, Panel PanelQtdRegistros, DropDownList ddlLancamentos,
        RadioButton rbData, Repeater RepeaterFormaEntrega, System.Web.UI.HtmlControls.HtmlGenericControl PanelEntrega)
    {
        DataTable dt = new DataTable();

        try
        {


            double porc = 0;
            string pMax = "";
            string pMin = "";

            #region Porcentagem
            if (txtPercent.Text.Trim().Length < 1)
                porc = 1;
            else
            {
                try
                {
                    porc = Convert.ToInt32(txtPercent.Text.Trim());
                    pMax = Convert.ToDecimal(1 + (porc / 100)).ToString().Replace(",", ".");
                    pMin = Convert.ToDecimal(1 - (porc / 100)).ToString().Replace(",", ".");
                }
                catch (Exception ee)
                {
                    lblMsg.Text += "Informe o percentual correto da crítica";
                }
            }
            #endregion

            Data db = new Data();
            string sql = "";
            if (ddlTipoRelatorio.SelectedValue.ToString() != "Relatório de valores" && ddlTipoRelatorio.SelectedValue.ToString() != "Entrega de Contas")
            {
                sql =
                   "select LIV_ANO AS ANO, LIV_MES AS MES, CAST(lei_ligacao AS VARCHAR) AS LIGACAO,LEI_CONSUMIDOR AS CONSUMIDOR, WTR_LEITURAS.LOC_ID AS ROTA, " +
                   "LEI_ENDERECO AS ENDEREÇO, LEI_NUMERO AS NUMERO, LEI_DATA +' '+LEI_HORA AS DATA, LTR_NOME AS LEITURISTA, "
                            + " MSG_DESCRICAO AS OCORRENCIA, LEI_MEDIA AS MEDIA, LEI_ANTERIOR AS 'LEIT. ANT', LEI_LEITURA AS LEITURA, "
                            + " LEI_NUMMED AS MEDIDOR,  LEI_CONSUMO,LEI_LATITUDE,LEI_LONGITUDE ";

                if (ddlTipoRelatorio.SelectedValue.ToString() == "Rol de leituras")
                    sql = "select LOC_ID,categoria.cat_nome as nomecategoria,WTR_LEITURISTAS.LTR_ID,LEI_ECONOMIA,LEI_ECONOMIARESIDENCIAL,LEI_ECONOMIACOMERCIAL,LEI_ECONOMIAINDUSTRIAL,LEI_SEQUENCIA,LEI_LIGACAO,LEI_CONSUMIDOR,LEI_ENDERECO +'-'+CONVERT(VARCHAR,LEI_NUMERO)+' '+LEI_COMPLEMENTO AS ENDERECO,LEI_BAIRRO,LEI_NUMMED,LEI_CATEGORIA AS CATEGORIA,LEI_LEITURA,WTR_LEITURAS.MSG_ID AS OCORRENCIA";

               
                if (txtGrupo.Text.Length == 0)
                    sql += ",GRUPO_ID";

                sql += ",LEI_HORAREPASSE AS 'HORA REPASSE',LEI_HIDROTORTO AS 'HIDRO. INCLINADO'";

                sql += " FROM WTR_LEITURAS INNER JOIN WTR_MENSAGENS ON(WTR_LEITURAS.MSG_ID = WTR_MENSAGENS.MSG_ID and WTR_LEITURAS.cli_id = WTR_MENSAGENS.cli_id and WTR_MENSAGENS.emp_id = WTR_LEITURAS.emp_id) INNER JOIN WTR_LEITURISTAS ON (WTR_LEITURAS.LTR_ID = WTR_LEITURISTAS.LTR_ID  and WTR_LEITURAS.cli_id = WTR_LEITURISTAS.cli_id and WTR_LEITURISTAS.emp_id = WTR_LEITURAS.emp_id) ";
                sql += " INNER JOIN WTR_SUBCATEGORIA categoria ON (WTR_LEITURAS.lei_categoria = categoria.CAT_ID)";

                sql += "WHERE WTR_LEITURAS.emp_id =" +
                              empresa + " and WTR_LEITURAS.cid_id =" + empresa +
                              " and WTR_LEITURAS.CLI_ID = " + cliente;// Data.cli.ToString();

                if (ddlTipoRelatorio.SelectedValue.ToString() == "Relatório detalhado")
                {
                    sql = "select LEI_FORMAENTREGA,LEI_ECONOMIA,WTR_LEITURAS.MSG_ID,LTR_NOME,LEI_ECONOMIARESIDENCIAL,LEI_ECONOMIACOMERCIAL,LEI_ECONOMIAINDUSTRIAL,LEI_SEQUENCIA,LEI_LIGACAO,LEI_CONSUMIDOR,LEI_ENDERECO +'-'+CONVERT(VARCHAR,LEI_NUMERO)+' '+" +
                "LEI_COMPLEMENTO AS ENDERECO,LEI_BAIRRO AS BAIRRO,LEI_RETERCONTA,LEI_NUMMED AS HD,LEI_LEITURA,categoria.cat_nome as nomecategoria,LEI_ANTERIOR,LEI_CONSUMO,LEI_MEDIA AS MEDIA,WTR_MENSAGENS.MSG_DESCRICAO AS OCORRENCIA,CONVERT(VARCHAR,LEI_CATEGORIA) AS CATEGORIA,LEI_SITUACAOLEITURA as'SITUACAO'";
                    if (ddlSituacaoLeitura.SelectedValue.ToString() == "Alteração cadastral")
                        sql += ",novacat.CAT_NOME AS NOVACATEGORIA";
                    else
                        sql += ",LEI_NOVACATEGORIA AS NOVACATEGORIA";
                    sql += ",GRUPO_ID,LEI_LANCAMENTO ";


                    sql += " FROM WTR_LEITURAS INNER JOIN WTR_MENSAGENS ON(WTR_LEITURAS.MSG_ID = WTR_MENSAGENS.MSG_ID and WTR_LEITURAS.cli_id = WTR_MENSAGENS.cli_id and WTR_MENSAGENS.emp_id = WTR_LEITURAS.emp_id) INNER JOIN WTR_LEITURISTAS ON (WTR_LEITURAS.LTR_ID = WTR_LEITURISTAS.LTR_ID) ";
                    if (ddlSituacaoLeitura.SelectedValue == "Alteração cadastral")
                        sql += "INNER JOIN WTR_SUBCATEGORIA novacat ON (WTR_LEITURAS.LEI_NOVACATEGORIA = novacat.CAT_ID AND WTR_LEITURAS.LEI_NOVACATEGORIA!=0)";

                    sql += "INNER JOIN WTR_SUBCATEGORIA categoria ON (WTR_LEITURAS.lei_categoria = categoria.CAT_ID)";
                    sql += " WHERE WTR_LEITURAS.emp_id =" +
                                  empresa + " and WTR_LEITURAS.cid_id =" + empresa +
                                  " and WTR_LEITURAS.CLI_ID = " + Convert.ToInt32(cliente);// Data.cli.ToString();
                }
            }

            else if(ddlTipoRelatorio.SelectedValue.ToString() == "Entrega de Contas")
            {
                sql = "SELECT WTR_LEITURISTAS.LTR_NOME ,LEI_LIGACAO,GRUPO_ID,LEI_CONSUMIDOR,LEI_ENDERECO + '-' + CONVERT(VARCHAR, LEI_NUMERO) + ' ' + LEI_COMPLEMENTO AS ENDERECO, LEI_ENTREGAFATURA = CASE LEI_ENTREGAFATURA WHEN '0'THEN '' ELSE WTR_ENTREGA.ENT_DESCRICAO END, LEI_DATAENTREGA FROM WTR_LEITURAS " +
                        "INNER JOIN WTR_MENSAGENS ON(WTR_LEITURAS.MSG_ID = WTR_MENSAGENS.MSG_ID and WTR_LEITURAS.cli_id = WTR_MENSAGENS.cli_id and WTR_MENSAGENS.emp_id = WTR_LEITURAS.emp_id) "+
                        "INNER JOIN WTR_LEITURISTAS ON(WTR_LEITURAS.LEI_LTRENTREGA = WTR_LEITURISTAS.LTR_ID  and WTR_LEITURAS.cli_id = WTR_LEITURISTAS.cli_id and WTR_LEITURISTAS.emp_id = WTR_LEITURAS.emp_id) " +
                        "INNER JOIN WTR_SUBCATEGORIA categoria ON(WTR_LEITURAS.lei_categoria = categoria.CAT_ID) "+
                        "INNER JOIN WTR_ENTREGA ON WTR_ENTREGA.ENT_ID = WTR_LEITURAS.LEI_ENTREGAFATURA  WHERE WTR_LEITURAS.emp_id = "+empresa+ " and WTR_LEITURAS.cid_id = 01 " +
                        " and WTR_LEITURAS.CLI_ID = "+cliente;


            }
            else
            {//RELATÓRIO DE VALORES DAS FATURAS
                sql =
            "select LIV_ANO AS ANO, LIV_MES AS MES, ";
                if (Convert.ToInt16(cliente) == 5)
                    sql += "'09'+CAST(substring(lei_ligacao,2,len(lei_ligacao)) AS VARCHAR)";
                else
                    sql += "CAST(lei_ligacao AS VARCHAR)";

                if (Convert.ToInt16(cliente) == 5)
                    sql += "+'-'+CAST(LEI_DIGITOLIGACAO AS VARCHAR) ";

                sql += " AS LIGACAO,LEI_ENDERECO AS ENDEREÇO, LEI_NUMERO AS NUMERO, LEI_ANTERIOR AS 'LEIT. ANTERIOR',LEI_LEITURA AS LEITURA,WTR_MENSAGENS.MSG_DESCRICAO AS 'SITU. LEITURA',LEI_CONSUMO AS CONSUMO,LEI_CONSUMOFAT AS 'CONSUMO FAT.',LEI_VALORTOTAL AS 'VALOR TOTAL' ";

                sql += " FROM WTR_LEITURAS INNER JOIN WTR_MENSAGENS ON(WTR_LEITURAS.MSG_ID = WTR_MENSAGENS.MSG_ID and WTR_LEITURAS.cli_id = WTR_MENSAGENS.cli_id and WTR_MENSAGENS.emp_id = WTR_LEITURAS.emp_id) INNER JOIN WTR_LEITURISTAS ON (WTR_LEITURAS.LTR_ID = WTR_LEITURISTAS.LTR_ID) WHERE WTR_LEITURAS.emp_id =" +
                             empresa + " and WTR_LEITURAS.cid_id =" + empresa +
                             " and WTR_LEITURAS.CLI_ID = " + cliente.ToString().Trim();// Data.cli.ToString();


            }

            #region SQL E FILTROS

            if (txtLivro.Text.Trim().Length > 0)
            {
                sql += " AND WTR_LEITURAS.LOC_ID = '" + txtLivro.Text.Trim() + "' ";
            }

            if (cbxMsg.SelectedValue.ToString() != "1000")
                sql += " AND WTR_LEITURAS.MSG_ID = '" + cbxMsg.SelectedValue.ToString() + "' ";
            if (cbxMes.SelectedIndex > 0)
                sql += " AND LIV_MES = '" + cbxMes.SelectedIndex.ToString() + "' ";

            if (txtAno.Text.Trim().Length > 0)
                sql += " AND LIV_ANO = '" + txtAno.Text.Trim() + "' ";            
            if (txtEndereco.Text.Trim().Length > 0)
                sql += " AND (LEI_ENDERECO LIKE '" + txtEndereco.Text + "' or LEI_ENDERECOENTREGA LIKE '" + txtEndereco.Text + "') ";
            if (txtNumero.Text.Trim().Length > 0)
                sql += " AND (LEI_NUMERO = " + txtNumero.Text + " or LEI_NUMEROENTREGA LIKE '" + txtNumero.Text + "') ";

            if (ddlLancamentos.SelectedValue == "Fechado por débito")
                sql += " AND LEI_LANCAMENTO LIKE '01' AND LEI_CONSUMO > 0 ";
            else if (ddlLancamentos.SelectedValue == "Fechado a pedido")
                sql += " AND LEI_LANCAMENTO LIKE '03' AND LEI_CONSUMO > 0 ";


            #region Somente critica
            if (chkCritica.Checked)
            {
                sql += " AND (((LEI_LEITURA - LEI_ANTERIOR) > (LEI_MEDIA * " + pMax + ")) OR ((LEI_LEITURA - LEI_ANTERIOR) < (LEI_MEDIA * " + pMin + ")))";
            }
            #endregion
            if (ddlTipoRelatorio.SelectedValue.ToString() != "Relatório grupos")
            {

                #region Sem Leitura
                if (ddlSituacaoLeitura.SelectedValue.ToString() == "Com Leitura")
                {
                    sql += " AND LEI_LEITURA <> ''";
                }
                else
                {
                    if (ddlSituacaoLeitura.SelectedValue.ToString() == "Sem Leitura")
                    {
                        sql += " AND (LEI_LEITURA = '' or lei_leitura is null)";
                    }
                }
                #endregion

                #region Grupo
                if (txtGrupo.Text.Trim() != "")
                {
                    sql += " AND GRUPO_ID = " + txtGrupo.Text;
                }
                #endregion

                #region SITUACAO LEITURA
                if (ddlSituacaoLeitura.SelectedValue.ToString() == "Acréscimo de consumo" && cliente == "17")
                {
                    sql += " AND LEI_SITUACAOLEITURA like '+'";
                }
                else if (ddlSituacaoLeitura.SelectedValue.ToString() == "Decréscimo de consumo" && cliente == "17")
                    sql += " AND LEI_SITUACAOLEITURA like '-'";
                else if (ddlSituacaoLeitura.SelectedValue.ToString() == "Alteração cadastral")
                    sql += " AND LEI_NOVACATEGORIA > 0";
                else if (ddlSituacaoLeitura.SelectedValue.ToString() == "Hidro. inclinado")
                    sql += " AND LEI_HIDROTORTO = 'S'";
                else if (ddlSituacaoLeitura.SelectedValue.ToString() == "Contas retidas")
                    sql += " AND LEI_RETERCONTA = 'S' AND LEI_TIPOBANCO != '99'";
                else if (ddlSituacaoLeitura.SelectedValue.ToString() == "Contas Protocoladas")
                    sql += " AND LEI_TIPOBANCO = '99'";


                #endregion


                #region Leituristas
                if (ddlLeituristas.SelectedValue.ToString() != "0")
                {
                    sql += " AND WTR_LEITURAS.LTR_ID  =  " + ddlLeituristas.SelectedValue;
                }
                #endregion

                #region Identificação e Medidor
                if ((txtIdentificacao.Text.Trim().Length > 0) || (txtMedidor.Text.Trim().Length > 0))
                {
                    if (txtIdentificacao.Text.Trim().Length > 0)
                        sql += " AND LEI_LIGACAO LIKE '%" + txtIdentificacao.Text.Trim() + "%' ";

                    if (txtMedidor.Text.Trim().Length > 0)
                        sql += " AND LEI_NUMMED LIKE '%" + txtMedidor.Text.Trim() + "%' ";

                }
                #endregion

            }
            else
            {
                sql += " AND LEI_LEITURA <> ''";

                #region Grupo
                if (txtGrupo.Text.Trim() != "" && cliente == "17")
                {
                    sql += " AND GRUPO_ID = " + txtGrupo.Text;
                }
                #endregion

                #region SITUACAO LEITURA
                if (cliente == "17")
                {
                    sql += " AND (LEI_SITUACAOLEITURA != '' OR LEI_RETERCONTA = 'S') ";
                }
                #endregion


                #region Leituristas

                #endregion

                #region Identificação e Medidor


            }


                #endregion
            #endregion

            if (rbEnderecoOrdem.Checked && ddlTipoRelatorio.SelectedValue.ToString() == "Rol de leituras")
                sql += " ORDER BY LEI_ENDERECO,LEI_NUMERO,LEI_COMPLEMENTO,LIV_ANO, LIV_MES, WTR_LEITURAS.LOC_ID,LEI_SEQ";

            else if (rbLigacao.Checked)
                sql += " ORDER BY lei_ligacao,LIV_ANO, LIV_MES, WTR_LEITURAS.LOC_ID";
            else if (rbData.Checked)
                sql += " ORDER BY LEI_DATA,LEI_HORA,LIV_ANO, LIV_MES, WTR_LEITURAS.LOC_ID";
            else if (rbSequencia.Checked)
                sql += " ORDER BY lei_sequencia,LIV_ANO, LIV_MES, WTR_LEITURAS.LOC_ID";
            else if (rbNomeContribuinte.Checked)
                sql += " ORDER BY lei_consumidor,LIV_ANO, LIV_MES, WTR_LEITURAS.LOC_ID";
            else if (rbRota.Checked)
                sql += " ORDER BY WTR_LEITURAS.LOC_ID,LIV_ANO, LIV_MES,lei_seq";
            else
            {

                sql += " ORDER BY LIV_ANO, LIV_MES, ";
                if (txtGrupo.Text.Length == 0)
                    sql += "GRUPO_ID, ";
                sql += "lei_seq";
            }

            //}

            if (ddlTipoRelatorio.SelectedValue.ToString() == "Relatório grupos")
                sql = "select grupo_id as Grupo,sum(convert(decimal(12,2),lei_valortotal)) as 'Total R$',sum(lei_consumofat) as 'Consumo m³' from wtr_leituras where liv_mes = " + cbxMes.SelectedIndex.ToString() + " and liv_ano = " + txtAno.Text + "  group by grupo_id order by grupo_id";

            dt = db.GetDt(strCon, "WTR_LEITURAS", sql);

            if (dt.DefaultView.Count > 0)
            {
                lblTOTAL.Text = string.Format("{0:C}", Convert.ToDecimal(dt.DefaultView.Count.ToString())).Replace("R$", "").Replace(" ", "").Split(',')[0];
                PanelQtdRegistros.Visible = true;
            }
            else PanelQtdRegistros.Visible = false;

            if (ddlTipoRelatorio.SelectedValue.ToString() == "Rol de leituras")
            {
                RepeaterRolLeitura.DataSource = dt;
                RepeaterRolLeitura.DataBind();
                PanelGrid.Visible = false;
                PanelRolLeituras.Visible = true;
                PanelDetalhado.Visible = false;


            }

            if (ddlTipoRelatorio.SelectedValue.ToString() == "Entrega de Contas")
            {
                RepeaterFormaEntrega.DataSource = dt;
                RepeaterFormaEntrega.DataBind();
                PanelGrid.Visible = false;
                PanelEntrega.Visible = true;
                PanelDetalhado.Visible = false;


            }
            else if (ddlTipoRelatorio.SelectedValue.ToString() == "Relatório detalhado")
            {

                RepeaterDetalahado.DataSource = dt;
                RepeaterDetalahado.DataBind();

                PanelDetalhado.Visible = true;
                PanelGrid.Visible = false;
                PanelRolLeituras.Visible = false;
            }
            else
            {
                gridView.DataSource = dt;
                gridView.DataBind();
                PanelGrid.Visible = true;
                PanelRolLeituras.Visible = false;
                PanelDetalhado.Visible = false;

            }

        }
        catch (Exception ex)
        {
            lblMsg.Text = "Erro - " + ex.Message;
            panelMsg.Visible = true;

        }

    }

    public void CadLeiturista(String strCon, TextBox nome, TextBox SobreNome, TextBox usuario, TextBox senha)
    {
        DataTable dt = new DataTable();
        Data db = new Data();
        string erro = "";
        string sql = "";
        string insert = "";
        try
        {
            //sql = "SELECT * FROM WTR_LEITURISTAS WHERE LTR_NOME = '" + nome.Text + "'";
            //dt = db.GetDt(strCon, "WTR_LEITURISTAS", sql);

            insert = "INSERT INTO WTR_LEITURISTAS  (CLI_ID, EMP_ID, LTR_NOME, LTR_SOBRENOME,LTR_TIPO, LTR_USUARIO, LTR_SENHA)VALUES(7,1,'" + nome.Text + "','" + SobreNome.Text + "', 'U', '" + usuario.Text + "', " + senha.Text + ")";
            erro = db.Ins(strCon, insert);

        }
        catch (Exception ex)
        {
            Console.WriteLine(erro);
            throw;
        }


      

    }


}