﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WaterSyncLite.MDL
{
    public class Cidade
    {
        public Cidade()
        {
        }
        
        private int cidID;
        private string nome;       
        private int cliID;        
        private int empID;               
        
    
        public Cidade(int cidID, string nome, int cliID, int empID)
        {
            this.cidID = cidID;
            this.nome = nome;
            this.cliID = cliID;
            this.empID = empID;

            
        }
        public string Nome
        {
            get { return nome; }
            set { nome = value; }
        }
        public int CidID
        {
            get { return cidID; }
            set { cidID = value; }
        }
        
        public int EmpID
        {
            get { return empID; }
            set { empID = value; }
        }

        public int CliID
        {
            get { return cliID; }
            set { cliID = value; }
        }
        

    }
}
