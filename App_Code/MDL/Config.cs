﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WaterSyncLite.MDL
{
    public class Config
    {
        private Empresa empresa;
        private String strCon;
        private String linhasRel;

        public String LinhasRel
        {
            get { return linhasRel; }
            set { linhasRel = value; }
        }

        public Empresa Empresa
        {
            get { return empresa; }
            set { empresa = value; }
        }
        
        public String StrCon
        {
            get { return strCon; }
            set { strCon = value; }
        }
    }
}
