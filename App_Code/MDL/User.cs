﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WaterSyncLite.MDL
{
    public class User
    {
        private int id;
        private String name;
        public String userName;
        private String pwd;
        private String email;

        public User() 
        {
            this.id = 0;
            this.name = "";
            this.userName = "";
            this.pwd = "";
            this.email = "";
        }

        public int Id
        {
            get { return id; }
            set { id = value; }
        }

        public String Name
        {
            get { return name; }
            set { name = value; }
        }

        public String UserName
        {
            get { return userName; }
            set { userName = value; }
        }

        public String Pwd
        {
            get { return pwd; }
            set { pwd = value; }
        }

        public String Email
        {
            get { return email; }
            set { email = value; }
        }
    }
}
