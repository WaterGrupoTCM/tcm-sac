﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WaterSyncLite.MDL
{
    public class Leiturista
    {
        private Empresa empresa;
        private String nome;
        private int id;
        private String sobrenome;
        private String tipo;
        private String envioFoto;
        private String print;

        public String EnvioFoto
        {
            get { return envioFoto; }
            set { envioFoto = value; }
        }
        
        public String Print
        {
            get { return print; }
            set { print = value; }
        }

        internal Empresa Empresa
        {
            get { return empresa; }
            set { empresa = value; }
        }
        
        public String Nome
        {
            get { return nome; }
            set { nome = value; }
        }
        
        public int Id
        {
            get { return id; }
            set { id = value; }
        }
        
        public String Sobrenome
        {
            get { return sobrenome; }
            set { sobrenome = value; }
        }

        public String Tipo
        {
            get { return tipo; }
            set { tipo = value; }
        }

    }
}
