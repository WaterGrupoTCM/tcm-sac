﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WaterSyncLite.MDL
{
    public class PermissaoAcesso
    {
        private User usr;
        private Empresa emp;

        internal User Usr
        {
            get { return usr; }
            set { usr = value; }
        }
        
        internal Empresa Emp
        {
            get { return emp; }
            set { emp = value; }
        }
    }
}
