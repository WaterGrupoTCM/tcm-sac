﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WaterSyncLite.MDL
{
    public class Export
    {

        private int cli_id;
        private int emp_id;
        private string tabela;
        private string campos;

        public int Cli_id
        {
            get { return cli_id; }
            set { cli_id = value; }
        }

        public int Emp_id
        {
            get { return emp_id; }
            set { emp_id = value; }
        }

        public string Tabela
        {
            get { return tabela; }
            set { tabela = value; }
        }

        public string Campos
        {
            get { return campos; }
            set { campos = value; }
        }
    }
}
