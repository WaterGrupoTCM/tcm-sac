﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WaterSyncLite.MDL
{
    public class Mail
    {
        private String mailTo;
        private String mailFrom;
        private String pwd;
        private String smtp;
        private int port;

        public int Port
        {
            get { return port; }
            set { port = value; }
        }

        public String Smtp
        {
            get { return smtp; }
            set { smtp = value; }
        }

        public String Pwd
        {
            get { return pwd; }
            set { pwd = value; }
        }

        public String MailFrom
        {
            get { return mailFrom; }
            set { mailFrom = value; }
        }
        public String MailTo
        {
            get { return mailTo; }
            set { mailTo = value; }
        }

    }
}
