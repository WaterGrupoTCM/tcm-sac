﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WaterSyncLite.MDL
{
    public class Ftp
    {
        private String server;
        private String user;
        private String password;
        private String port;
        private String updatePath;
        private String manualPath;
        private String downloadPath;

        public String DownloadPath
        {
            get { return downloadPath; }
            set { downloadPath = value; }
        }

        public String ManualPath
        {
            get { return manualPath; }
            set { manualPath = value; }
        }

        public String UpdatePath
        {
            get { return updatePath; }
            set { updatePath = value; }
        }

        public String Server
        {
            get { return server; }
            set { server = value; }
        }
        
        public String User
        {
            get { return user; }
            set { user = value; }
        }
        
        public String Password
        {
            get { return password; }
            set { password = value; }
        }
        
        public String Port
        {
            get { return port; }
            set { port = value; }
        }
    }
}
