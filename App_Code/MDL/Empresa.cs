﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WaterSyncLite.MDL
{
    public class Empresa
    {

        private int cliId;
        private int empId;
        private String razaoSocial;
        private String _NomeFantasia;
        private String _Endereco;
        private String _Bairro;
        private String _CEP;

        
        private String _Cidade;
        private String _UF;


        private String _Email;


        private String _Cont1;


        private String _Cont2;


        private String _Fone1;


        private String _Fone2;


        private String _HomePage;

        


        public String HomePage
        {
            get { return _HomePage; }
            set { _HomePage = value; }
        }public String Fone2
        {
            get { return _Fone2; }
            set { _Fone2 = value; }
        }
        public String Fone1
        {
            get { return _Fone1; }
            set { _Fone1 = value; }
        }
        public String Cont2
        {
            get { return _Cont2; }
            set { _Cont2 = value; }
        }
        public String Cont1
        {
            get { return _Cont1; }
            set { _Cont1 = value; }
        }
        public String Email
        {
            get { return _Email; }
            set { _Email = value; }
        }
        
        public String UF
        {
            get { return _UF; }
            set { _UF = value; }
        }
        public String CEP
        {
            get { return _CEP; }
            set { _CEP = value; }
        }
        public String NomeFantasia
        {
            get { return _NomeFantasia; }
            set { _NomeFantasia = value; }
        }
        

        public String Endereco
        {
            get { return _Endereco; }
            set { _Endereco = value; }
        }
       

        public String Bairro
        {
            get { return _Bairro; }
            set { _Bairro = value; }
        }
        

        public String Cidade
        {
            get { return _Cidade; }
            set { _Cidade = value; }
        }
        

        
       
        public Empresa()
        {
        }

        public Empresa(int cliente, int empresa, String razaoSocial)
        {
            this.cliId = cliente;
            this.empId = empresa;
            this.razaoSocial = razaoSocial;
            
        }

        public int CliId
        {
            get { return cliId; }
            set { cliId = value; }
        }

        public int EmpId
        {
            get { return empId; }
            set { empId = value; }
        }

        public String RazaoSocial
        {
            get { return razaoSocial; }
            set { razaoSocial = value; }
        }

       

    }
}
