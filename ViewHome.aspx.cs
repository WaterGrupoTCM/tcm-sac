﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WaterSyncLite.Class;

public partial class ViewHome : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            this.Master.LoadHeader(this.Master, Request.Path.Substring(Request.Path.LastIndexOf("/") + 1));
            if (!IsPostBack)
            {
                WaterSyncLite.DAL.UserDAL usuarioDAL = new WaterSyncLite.DAL.UserDAL();
                lblUsuario.Text = usuarioDAL.getUser(Convert.ToInt32(Request.Cookies["user"].Value)).Name;

            }

        }
        catch (Exception ex)
        {
            ShowMessage("Erro - " + ex.Message);
        }

    }
    protected void ShowMessage(string msg)
    {
        String scriptString = "<script language = JavaScript>";
        scriptString += "confirm('" + msg + "');";
        scriptString += "</script>";
        Page.ClientScript.RegisterStartupScript(Page.GetType(), "scrt", scriptString);
    }

    
}