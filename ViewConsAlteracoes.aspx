﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="ViewConsAlteracoes.aspx.cs" Inherits="ViewConsAlteracoes" %>

<%@ MasterType VirtualPath="~/Site.master" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        th {
            text-align: center;
        }

        .LinhaSelecionada {
            color: #428bca;
            font-weight: bold;
        }

        .modalDialog {
            position: fixed;
            top: 0;
            right: 30%;
            bottom: 0;
            left: 30%;
            z-index: 9010;
            display: none;
            overflow: auto;
        }
    </style>
    <script type="text/javascript">

        function printDiv(divName) {
            var printContents = document.getElementById(divName).innerHTML;
            var originalContents = document.body.innerHTML;

            document.body.innerHTML = printContents;

            window.print();

            document.body.innerHTML = originalContents;
        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
   

    <div class="panel panel-primary">
        <div class="panel-heading">
            <h4 class="panel-title"><span class="TitulosPanel">Relatório de Alterações de Leitura</span></h4>
        </div>
        <div class="panel-body minha-classe">

            <form runat="server">
                <asp:Panel ID="PanelMsg" runat="server" Visible="false">
                    <div class="alert alert-info">
                        <strong>Atenção! </strong>
                        <asp:Label ID="lblMsg" runat="server" Text=""></asp:Label>
                    </div>
                </asp:Panel>

                <div id="print">
                      <div class="row">
                    <div class="col-lg-12">

                        <div class="col-lg-3" style="padding-left: 0px">
                            <div class="input-group">
                                <span class="input-group-addon">Mês</span>
                                <asp:DropDownList ID="cbxMes" runat="server" CssClass="form-control">
                                    <asp:ListItem>Selecione o mês</asp:ListItem>
                                    <asp:ListItem>Janeiro</asp:ListItem>
                                    <asp:ListItem>Fevereiro</asp:ListItem>
                                    <asp:ListItem>Março</asp:ListItem>
                                    <asp:ListItem>Abril</asp:ListItem>
                                    <asp:ListItem>Maio</asp:ListItem>
                                    <asp:ListItem>Junho</asp:ListItem>
                                    <asp:ListItem>Julho</asp:ListItem>
                                    <asp:ListItem>Agosto</asp:ListItem>
                                    <asp:ListItem>Setembro</asp:ListItem>
                                    <asp:ListItem>Outubro</asp:ListItem>
                                    <asp:ListItem>Novembro</asp:ListItem>
                                    <asp:ListItem>Dezembro</asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>

                        <div class="col-lg-2">
                            <div class="input-group">
                                <span class="input-group-addon" id="sizing-addon2">Ano</span>
                                <asp:TextBox ID="txtAno" runat="server" CssClass="form-control bfh-number"></asp:TextBox>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="input-group">
                                <span class="input-group-addon">Leiturista</span>
                                <asp:DropDownList ID="ddlLtr" runat="server" CssClass="form-control">
                                </asp:DropDownList>
                            </div>
                        </div>
                        

                        <div class="col-lg-3">
                            <div class="input-group">
                                <span class="input-group-addon">Situação</span>
                                <asp:DropDownList ID="ddlSituacao" runat="server" CssClass="form-control">
                                     <asp:ListItem>Todas</asp:ListItem>
                                    <asp:ListItem>Liberadas</asp:ListItem>
                                    <asp:ListItem>Modificadas</asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>

                    </div>

                </div>

                <div class="row" style="padding-top: 10px">
                    <div class="col-lg-12">
                        <div class="col-lg-3" style="padding-left: 0px">
                            <div class="input-group">
                                <span class="input-group-addon">Ligação</span>
                                <asp:TextBox ID="txtIdentificacao" runat="server" CssClass="form-control bfh-number"></asp:TextBox>
                            </div>
                        </div>
                        <div class="col-lg-2">
                            <div class="input-group">
                                <span class="input-group-addon">Rota</span>
                                <asp:TextBox ID="txtLivro" runat="server" CssClass="form-control bfh-number"></asp:TextBox>
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="input-group">
                                <span class="input-group-addon">Hidrômetro</span>
                                <asp:TextBox ID="txtMedidor" runat="server" CssClass="form-control bfh-number"></asp:TextBox>
                            </div>
                        </div>

                        <div class="col-lg-2" id="panelGrupo" runat="server">
                            <div class="input-group">
                                <span class="input-group-addon">Grupo</span>
                                <asp:TextBox ID="txtGrupo" runat="server" CssClass="form-control bfh-number"></asp:TextBox>
                            </div>
                        </div>

                        <div class="col-lg-1">
                            
                            <asp:ImageButton ID="ImageButton1" runat="server" CssClass="btn btn-primary BotaoPesquisa" OnClick="btPesquisar_Click" ImageUrl="~/imagens/search.png" />
                        </div>
                        <div class="col-lg-1">
                            <input type="image" onclick="printDiv('print')" class="btn btn-primary BotaoPesquisa" src="imagens/print.png" />
                        </div>


                    </div>
                </div>


                <div class="row" style="padding-top: 20px">
                    <div class="col-12">
                        <div class="panel panel-primary">
                            <div class="panel-heading" style="background-color: #4f5b69">
                                
                            </div>
                            <div class="panel-body minha-classe">
                                <div id="scrollDiv" style="overflow: auto; height: 100%; width: 100%">


                                    <asp:GridView ID="GridView1" runat="server" CellPadding="4" GridLines="None" EmptyDataText="Nenhum registro encontrado" BackColor="White"
                                        BorderColor="#999999" BorderStyle="None" BorderWidth="1px" Width="100%" PageSize="100" HeaderStyle-CssClass="tblTitle" >
                                        <AlternatingRowStyle BackColor="Gainsboro" />
                                        <Columns>
                                           
                                        </Columns>

                                        <EditRowStyle HorizontalAlign="Center" />

                                        <FooterStyle BackColor="#CCCCCC" ForeColor="Black" />
                                        <HeaderStyle BackColor="#4f5b69" Font-Bold="True" ForeColor="White" HorizontalAlign="Center" />
                                        <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Left" />
                                        <RowStyle BackColor="#EEEEEE" HorizontalAlign="Center" />
                                        <SelectedRowStyle BackColor="#e1ffff" Font-Bold="True" ForeColor="White" />
                                        <SortedAscendingCellStyle BackColor="#F1F1F1"></SortedAscendingCellStyle>
                                        <SortedAscendingHeaderStyle BackColor="#0000A9"></SortedAscendingHeaderStyle>
                                        <SortedDescendingCellStyle BackColor="#CAC9C9"></SortedDescendingCellStyle>
                                        <SortedDescendingHeaderStyle BackColor="#000065"></SortedDescendingHeaderStyle>
                                    </asp:GridView>

                                </div>
                            </div>
                        </div>

                    </div>
                </div>
               
                </div>

              
            </form>


        </div>
    </div>




</asp:Content>


