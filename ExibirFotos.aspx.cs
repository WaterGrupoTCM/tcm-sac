﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WaterSyncLite.Class;

public partial class ExibirFotos : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {

            String sql = "select LEI_ECONOMIA,LTR_NOME,LEI_RETERCONTA,LEI_ECONOMIARESIDENCIAL,LEI_ECONOMIACOMERCIAL,LEI_ECONOMIAINDUSTRIAL,LEI_SEQUENCIA,LEI_LIGACAO,LEI_CONSUMIDOR,LEI_ENDERECO +'-'+CONVERT(VARCHAR,LEI_NUMERO)+' '+" +
                "LEI_COMPLEMENTO AS ENDERECO,LEI_BAIRRO AS BAIRRO,LEI_NUMMED AS HD,LEI_LEITURA,LEI_ANTERIOR,LEI_CONSUMO,LEI_MEDIA AS MEDIA,WTR_MENSAGENS.MSG_DESCRICAO AS OCORRENCIA,CONVERT(VARCHAR,LEI_CATEGORIA) AS CATEGORIA,LEI_SITUACAOLEITURA 'SITUACAO'";

            if (Convert.ToInt32(Request.Cookies["CliID"].Value.ToString().Trim()) == 17)
                sql += ",GRUPO_ID,LEI_LANCAMENTO ";

            sql += " from wtr_leituras INNER JOIN WTR_MENSAGENS ON(WTR_LEITURAS.MSG_ID = WTR_MENSAGENS.MSG_ID and WTR_LEITURAS.cli_id = WTR_MENSAGENS.cli_id and WTR_MENSAGENS.emp_id = WTR_LEITURAS.emp_id) INNER JOIN WTR_LEITURISTAS ON (WTR_LEITURAS.LTR_ID = WTR_LEITURISTAS.LTR_ID) where lei_ligacao like '%" + Session["fotos"] + "' and liv_mes = " + Session["mesfotos"] + " and liv_ano = " + Session["anofotos"] + " and wtr_leituras.cli_id = " + Request.Cookies["CliID"].Value;

            Data db = new Data();

            DataTable dtFotos = db.GetDt(Request.Cookies["StrCon"].Value.ToString().Replace('|',';'),"WTR_LEITURAS",sql);


            for (int i = 0; i < dtFotos.DefaultView.Count; i++)
            {

                if (dtFotos.DefaultView[i].Row["LEI_ECONOMIARESIDENCIAL"].ToString().Trim() != "0")
                    dtFotos.DefaultView[i].Row["CATEGORIA"] = "RES";
                else if (dtFotos.DefaultView[i].Row["LEI_ECONOMIACOMERCIAL"].ToString().Trim() != "0")
                    dtFotos.DefaultView[i].Row["CATEGORIA"] = "COM";
                else if (dtFotos.DefaultView[i].Row["LEI_ECONOMIAINDUSTRIAL"].ToString().Trim() != "0")
                    dtFotos.DefaultView[i].Row["CATEGORIA"] = "IND";

                if (dtFotos.DefaultView[i].Row["LEI_ECONOMIARESIDENCIAL"].ToString().Trim() != "0")
                    dtFotos.DefaultView[i].Row["LEI_ECONOMIA"] = dtFotos.DefaultView[i].Row["LEI_ECONOMIARESIDENCIAL"].ToString().Trim();
                else if (dtFotos.DefaultView[i].Row["LEI_ECONOMIACOMERCIAL"].ToString().Trim() != "0")
                    dtFotos.DefaultView[i].Row["LEI_ECONOMIA"] = dtFotos.DefaultView[i].Row["LEI_ECONOMIACOMERCIAL"].ToString().Trim();
                else if (dtFotos.DefaultView[i].Row["LEI_ECONOMIAINDUSTRIAL"].ToString().Trim() != "0")
                    dtFotos.DefaultView[i].Row["LEI_ECONOMIA"] = dtFotos.DefaultView[i].Row["LEI_ECONOMIAINDUSTRIAL"].ToString().Trim();
            }

            Repeater1.DataSource = dtFotos;
            Repeater1.DataBind();
            
            DirectoryInfo minhaPastaImagens = new DirectoryInfo(Server.MapPath("~/Fotos/" + Request.Cookies["CliID"].Value.PadLeft(2,'0')));
            //galeriaDataList.DataSource = minhaPastaImagens.GetFiles("*" + Session["mesfotos"].ToString().PadLeft(2,'0') + Session["anofotos"] + Session["fotos"].ToString() + ".jpg");

            if (Convert.ToInt32(Request.Cookies["CliID"].Value.PadLeft(2, '0')) == 5)
                galeriaDataList.DataSource = minhaPastaImagens.GetFiles("*" + Session["fotos"].ToString().Replace("-","") + "_*.jpg");
            else
            {
                galeriaDataList.DataSource = minhaPastaImagens.GetFiles("*" + Session["fotos"].ToString() + ".jpg");
            }
            galeriaDataList.DataBind();

            

        }


    }
}