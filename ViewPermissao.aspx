﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="ViewPermissao.aspx.cs" Inherits="ViewPermissao" %>

<%@ MasterType VirtualPath="~/Site.master" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        th {
            text-align: center;
        }

        .LinhaSelecionada {
            color: #428bca;
            font-weight: bold;
        }

        .modalDialog {
            position: fixed;
            top: 0;
            right: 30%;
            bottom: 0;
            left: 30%;
            z-index: 9010;
            display: none;
            overflow: auto;
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="row">
        <div class="col-4"></div>
        <div class="col-4">
            <div class="modalDialog fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel">Detalhes da alteração</h4>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-6" id="leitura" runat="server">
                                    Leitura incorreta: 
                                    <asp:Label ID="lblLeituraAlterada" runat="server" Text=""></asp:Label><br />
                                    Leitura modificada: 
                                    <asp:Label ID="lblLeituraAtual" runat="server" Text=""></asp:Label>
                                </div>
                                <div class="col-6" id="ocorrencia" runat="server">
                                    Ocorrência incorreta:<asp:Label ID="lblMsgAlterada" runat="server" Text=""></asp:Label><br />
                                    Ocorrência modificada:<asp:Label ID="lblMsgAtual" runat="server" Text=""></asp:Label>
                                </div>
                            </div>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="button" class="btn btn-primary">Save changes</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-4"></div>

    </div>


    <div class="panel panel-primary">
        <div class="panel-heading">
            <h4 class="panel-title"><span class="TitulosPanel">Liberar Alteração de Leitura</span></h4>
        </div>
        <div class="panel-body minha-classe">

            <form runat="server">
                <asp:Panel ID="PanelMsg" runat="server" Visible="false">
                    <div class="alert alert-info">
                        <strong>Atenção! </strong>
                        <asp:Label ID="lblMsg" runat="server" Text=""></asp:Label>
                    </div>
                </asp:Panel>


                <div class="row">
                    <div class="col-lg-10">

                        <div class="col-lg-3" style="padding-left: 0px">
                            <div class="input-group">
                                <span class="input-group-addon">Mês</span>
                                <asp:DropDownList ID="cbxMes" runat="server" CssClass="form-control">
                                    <asp:ListItem>Selecione o mês</asp:ListItem>
                                    <asp:ListItem>Janeiro</asp:ListItem>
                                    <asp:ListItem>Fevereiro</asp:ListItem>
                                    <asp:ListItem>Março</asp:ListItem>
                                    <asp:ListItem>Abril</asp:ListItem>
                                    <asp:ListItem>Maio</asp:ListItem>
                                    <asp:ListItem>Junho</asp:ListItem>
                                    <asp:ListItem>Julho</asp:ListItem>
                                    <asp:ListItem>Agosto</asp:ListItem>
                                    <asp:ListItem>Setembro</asp:ListItem>
                                    <asp:ListItem>Outubro</asp:ListItem>
                                    <asp:ListItem>Novembro</asp:ListItem>
                                    <asp:ListItem>Dezembro</asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>

                        <div class="col-lg-2">
                            <div class="input-group">
                                <span class="input-group-addon" id="sizing-addon2">Ano</span>
                                <asp:TextBox ID="txtAno" runat="server" CssClass="form-control bfh-number"></asp:TextBox>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="input-group">
                                <span class="input-group-addon">Leiturista</span>
                                <asp:DropDownList ID="ddlLtr" runat="server" CssClass="form-control">
                                </asp:DropDownList>
                            </div>
                        </div>

                    </div>

                </div>

                <div class="row" style="padding-top: 10px">
                    <div class="col-lg-10">
                        <div class="col-lg-3" style="padding-left: 0px">
                            <div class="input-group">
                                <span class="input-group-addon">Ligação</span>
                                <asp:TextBox ID="txtIdentificacao" runat="server" CssClass="form-control bfh-number"></asp:TextBox>
                            </div>
                        </div>
                        <div class="col-lg-2">
                            <div class="input-group">
                                <span class="input-group-addon">Rota</span>
                                <asp:TextBox ID="txtLivro" runat="server" CssClass="form-control bfh-number"></asp:TextBox>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="input-group">
                                <span class="input-group-addon">Hidrômetro</span>
                                <asp:TextBox ID="txtMedidor" runat="server" CssClass="form-control bfh-number"></asp:TextBox>
                            </div>
                        </div>

                        <div class="col-lg-2" id="panelGrupo" runat="server">
                            <div class="input-group">
                                <span class="input-group-addon">Grupo</span>
                                <asp:TextBox ID="txtGrupo" runat="server" CssClass="form-control bfh-number"></asp:TextBox>
                            </div>
                        </div>

                        <div class="col-lg-1">
                            <asp:Button ID="btPesquisar" runat="server" Text="Pesquisar" CssClass="btn btn-primary BotaoPesquisa" OnClick="btPesquisar_Click" />
                        </div>
                    </div>
                </div>


                <div class="row" style="padding-top: 20px">
                    <div class="col-12">
                        <div class="panel panel-primary">
                            <div class="panel-heading" style="background-color: #4f5b69">
                                <h4 class="panel-title"><span class="">Ligações bloqueadas</span></h4>
                            </div>
                            <div class="panel-body minha-classe">
                                <div id="scrollDiv" style="overflow: auto; height: 100%; width: 100%">


                                    <asp:GridView ID="GridView1" runat="server" CellPadding="4" GridLines="None" EmptyDataText="Nenhum registro encontrado" BackColor="White"
                                        BorderColor="#999999" BorderStyle="None" BorderWidth="1px" Width="100%" PageSize="100" HeaderStyle-CssClass="tblTitle" OnRowCommand="GridView1_RowCommand" OnRowDataBound="GridView1_RowDataBound" OnPageIndexChanging="GridView1_PageIndexChanging">
                                        <AlternatingRowStyle BackColor="Gainsboro" />
                                        <Columns>
                                            <asp:ButtonField ButtonType="Image" CommandName="Ver" HeaderText="Detalhes" ImageUrl="~/imagens/ver.png" />
                                            <asp:ButtonField CommandName="Add" ButtonType="Image" HeaderText="Permitir" ImageUrl="~/imagens/edit.png">
                                                <ControlStyle Font-Size="12pt" />
                                            </asp:ButtonField>
                                        </Columns>

                                        <EditRowStyle HorizontalAlign="Center" />

                                        <FooterStyle BackColor="#CCCCCC" ForeColor="Black" />
                                        <HeaderStyle BackColor="#4f5b69" Font-Bold="True" ForeColor="White" HorizontalAlign="Center" />
                                        <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Left" />
                                        <RowStyle BackColor="#EEEEEE" HorizontalAlign="Center" />
                                        <SelectedRowStyle BackColor="#e1ffff" Font-Bold="True" ForeColor="White" />
                                        <SortedAscendingCellStyle BackColor="#F1F1F1"></SortedAscendingCellStyle>
                                        <SortedAscendingHeaderStyle BackColor="#0000A9"></SortedAscendingHeaderStyle>
                                        <SortedDescendingCellStyle BackColor="#CAC9C9"></SortedDescendingCellStyle>
                                        <SortedDescendingHeaderStyle BackColor="#000065"></SortedDescendingHeaderStyle>
                                    </asp:GridView>

                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="panel panel-primary" style="visibility: hidden">
                    <div class="panel-heading" style="background-color: #4f5b69">
                        <h4 class="panel-title"><span class="">Ligações permitidas</span></h4>
                    </div>
                    <div class="panel-body minha-classe">

                        <asp:GridView ID="GridView2" runat="server" CellPadding="4" ForeColor="#333333" GridLines="None" EmptyDataText="Nenhum registro encontrado" BackColor="White"
                            BorderColor="#999999" BorderStyle="None" BorderWidth="1px" Width="100%" AllowPaging="True" PageSize="100" HeaderStyle-CssClass="tblTitle" OnRowDataBound="GridView2_RowDataBound">
                            <AlternatingRowStyle BackColor="Gainsboro" />

                            <EditRowStyle HorizontalAlign="Center" />

                            <FooterStyle BackColor="#CCCCCC" ForeColor="Black" />
                            <HeaderStyle BackColor="#4f5b69" Font-Bold="True" ForeColor="White" HorizontalAlign="Center" />
                            <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Left" />
                            <RowStyle BackColor="#EEEEEE" ForeColor="Black" HorizontalAlign="Center" />
                            <SelectedRowStyle BackColor="#e1ffff" Font-Bold="True" ForeColor="White" />
                            <SortedAscendingCellStyle BackColor="#F1F1F1"></SortedAscendingCellStyle>
                            <SortedAscendingHeaderStyle BackColor="#0000A9"></SortedAscendingHeaderStyle>
                            <SortedDescendingCellStyle BackColor="#CAC9C9"></SortedDescendingCellStyle>
                            <SortedDescendingHeaderStyle BackColor="#000065"></SortedDescendingHeaderStyle>
                        </asp:GridView>
                    </div>
                </div>
            </form>


        </div>
    </div>




</asp:Content>

