﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="ViewRepasse.aspx.cs" Inherits="ViewRepasse" %>

<%@ MasterType VirtualPath="~/Site.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        th {
            text-align: center;
        }

        td {
            height: 37px;
        }

        .tooltip2 {
            position: relative;
            display: inline-block;
            /*border-bottom: 1px dotted black;*/
        }

            .tooltip2 .tooltiptext {
                visibility: hidden;
                width: 120px;
                background-color: black;
                color: #fff;
                text-align: center;
                border-radius: 6px;
                padding: 5px 0;
                /* Position the tooltip */
                position: absolute;
                z-index: 1;
            }

            .tooltip2:hover .tooltiptext {
                visibility: visible;
            }

        .LinhaSelecionada {
            color: #428bca;
            font-weight: bold;
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">


    <div class="panel panel-primary">
        <div class="panel-heading">
            <h4 class="panel-title"><span class="TitulosPanel">Geração de repasse</span></h4>
        </div>
        <div class="panel-body minha-classe">

            <form runat="server">
                <asp:Panel ID="PanelMsg" runat="server" Visible="false">
                    <div class="alert alert-info">
                        <strong>Atenção! </strong>
                        <asp:Label ID="lblMsg" runat="server" Text=""></asp:Label>
                    </div>
                </asp:Panel>
                <div class="row">
                    <div class="col-lg-10">

                        <div class="col-lg-3" style="padding-left: 0px">
                            <div class="input-group">
                                <span class="input-group-addon">Mês</span>
                                <asp:DropDownList ID="ddlMes" runat="server" CssClass="form-control">
                                    <asp:ListItem>Selecione o mês</asp:ListItem>
                                    <asp:ListItem>Janeiro</asp:ListItem>
                                    <asp:ListItem>Fevereiro</asp:ListItem>
                                    <asp:ListItem>Março</asp:ListItem>
                                    <asp:ListItem>Abril</asp:ListItem>
                                    <asp:ListItem>Maio</asp:ListItem>
                                    <asp:ListItem>Junho</asp:ListItem>
                                    <asp:ListItem>Julho</asp:ListItem>
                                    <asp:ListItem>Agosto</asp:ListItem>
                                    <asp:ListItem>Setembro</asp:ListItem>
                                    <asp:ListItem>Outubro</asp:ListItem>
                                    <asp:ListItem>Novembro</asp:ListItem>
                                    <asp:ListItem>Dezembro</asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>

                        <div class="col-lg-2">
                            <div class="input-group">
                                <span class="input-group-addon" id="sizing-addon2">Ano</span>
                                <asp:TextBox ID="txtAno" runat="server" CssClass="form-control bfh-number"></asp:TextBox>
                            </div>
                        </div>
                        <div class="col-lg-5">
                            <div class="input-group">
                                <span class="input-group-addon">Leiturista do repasse</span>
                                <asp:DropDownList ID="ddlLtr" runat="server" CssClass="form-control">
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="col-lg-2">
                            <asp:Button ID="btPesquisar" runat="server" Text="Pesquisar" CssClass="btn btn-primary BotaoPesquisa" OnClick="btPesquisar_Click" />
                        </div>
                    </div>

                </div>


                <div class="row" style="padding-top: 20px">
                    <div class="col-6">
                        <div class="panel panel-primary">
                            <div class="panel-heading" style="background-color: #4f5b69">
                                <h4 class="panel-title"><span class="">Grupos</span></h4>
                            </div>
                            <div class="panel-body minha-classe">
                                <asp:GridView ID="GridView1" runat="server" CellPadding="4" GridLines="None" EmptyDataText="Nenhum registro encontrado" BackColor="White"
                                    BorderColor="#999999" BorderStyle="None" BorderWidth="1px" Width="100%" AllowPaging="True" PageSize="100" HeaderStyle-CssClass="tblTitle" OnRowCommand="GridView1_RowCommand" OnRowDataBound="GridView1_RowDataBound">
                                    <AlternatingRowStyle BackColor="Gainsboro" />
                                    <Columns>
                                        <asp:ButtonField CommandName="Repasse" ButtonType="Image" HeaderText="GERAR REPASSE" ImageUrl="~/imagens/repasse.png">
                                            <ControlStyle Font-Size="12pt" />
                                        </asp:ButtonField>
                                    </Columns>

                                    <EditRowStyle HorizontalAlign="Center" />

                                    <FooterStyle BackColor="#CCCCCC" ForeColor="Black" />
                                    <HeaderStyle BackColor="#4f5b69" Font-Bold="True" ForeColor="White" HorizontalAlign="Center" />
                                    <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Left" />
                                    <RowStyle BackColor="#EEEEEE" HorizontalAlign="Center" />
                                    <SelectedRowStyle BackColor="#e1ffff" Font-Bold="True" ForeColor="White" />
                                    <SortedAscendingCellStyle BackColor="#F1F1F1"></SortedAscendingCellStyle>
                                    <SortedAscendingHeaderStyle BackColor="#0000A9"></SortedAscendingHeaderStyle>
                                    <SortedDescendingCellStyle BackColor="#CAC9C9"></SortedDescendingCellStyle>
                                    <SortedDescendingHeaderStyle BackColor="#000065"></SortedDescendingHeaderStyle>
                                </asp:GridView>

                            </div>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="panel panel-primary">
                            <div class="panel-heading" style="background-color: #4f5b69">
                                <h4 class="panel-title"><span class="">Repasses</span></h4>
                            </div>
                            <div class="panel-body minha-classe">

                                <asp:GridView ID="GridView2" runat="server" CellPadding="4" ForeColor="#333333" GridLines="None" EmptyDataText="Nenhum registro encontrado" BackColor="White"
                                    BorderColor="#999999" BorderStyle="None" BorderWidth="1px" Width="100%" AllowPaging="True" PageSize="100" HeaderStyle-CssClass="tblTitle" OnRowDataBound="GridView2_RowDataBound">
                                    <AlternatingRowStyle BackColor="Gainsboro" />

                                    <EditRowStyle HorizontalAlign="Center" />

                                    <FooterStyle BackColor="#CCCCCC" ForeColor="Black" />
                                    <HeaderStyle BackColor="#4f5b69" Font-Bold="True" ForeColor="White" HorizontalAlign="Center" />
                                    <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Left" />
                                    <RowStyle BackColor="#EEEEEE" ForeColor="Black" HorizontalAlign="Center" />
                                    <SelectedRowStyle BackColor="#e1ffff" Font-Bold="True" ForeColor="White" />
                                    <SortedAscendingCellStyle BackColor="#F1F1F1"></SortedAscendingCellStyle>
                                    <SortedAscendingHeaderStyle BackColor="#0000A9"></SortedAscendingHeaderStyle>
                                    <SortedDescendingCellStyle BackColor="#CAC9C9"></SortedDescendingCellStyle>
                                    <SortedDescendingHeaderStyle BackColor="#000065"></SortedDescendingHeaderStyle>
                                </asp:GridView>
                            </div>
                        </div>
                    </div>
                </div>


            </form>


        </div>
    </div>

</asp:Content>

