﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="ViewConsOcorrencia.aspx.cs" Inherits="ViewConsOcorrencia" %>

<%@ MasterType VirtualPath="~/Site.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript">

        $(function () {
            var tabName = $("[id*=TabName]").val() != "" ? $("[id*=TabName]").val() : "personal";
            $('#Tabs a[href="#' + tabName + '"]').tab('show');
            $("#Tabs a").click(function () {
                $("[id*=TabName]").val($(this).attr("href").replace("#", ""));
            });
        });

    </script>

    <style type="text/css">
        .BotaoPesquisa {
            margin-top: 51px;
        }

        th {
            text-align: center;
        }

        .painelInfo {
            min-height: 72px;
        }

    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <form runat="server">
        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h4 class="panel-title"><span class="TitulosPanel">Relatório de ocorrências</span></h4>
                    </div>
                    <div class="panel-body minha-classe">
                        <asp:Panel ID="PanelMsg" runat="server" Visible="false">
                            <div class="alert alert-info">
                                <strong>Atenção! </strong>
                                <asp:Label ID="lblMsg" runat="server" Text=""></asp:Label>
                            </div>
                        </asp:Panel>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="col-lg-2" style="padding-left: 0px">
                                    <div class="input-group">
                                        <span class="input-group-addon">Mês</span>
                                        <asp:DropDownList ID="cbxMes" runat="server" CssClass="form-control">
                                            <asp:ListItem>Selecione o mês</asp:ListItem>
                                            <asp:ListItem>Janeiro</asp:ListItem>
                                            <asp:ListItem>Fevereiro</asp:ListItem>
                                            <asp:ListItem>Março</asp:ListItem>
                                            <asp:ListItem>Abril</asp:ListItem>
                                            <asp:ListItem>Maio</asp:ListItem>
                                            <asp:ListItem>Junho</asp:ListItem>
                                            <asp:ListItem>Julho</asp:ListItem>
                                            <asp:ListItem>Agosto</asp:ListItem>
                                            <asp:ListItem>Setembro</asp:ListItem>
                                            <asp:ListItem>Outubro</asp:ListItem>
                                            <asp:ListItem>Novembro</asp:ListItem>
                                            <asp:ListItem>Dezembro</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>

                                <div class="col-lg-4">
                                    <div class="col-lg-6" style="padding-left: 0px;">

                                        <div class="input-group">
                                            <span class="input-group-addon">Ano</span>
                                            <asp:TextBox ID="txtAno" runat="server" CssClass="form-control bfh-number"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col-lg-6" style="padding-right: 0px;">
                                        <div class="input-group">
                                            <span class="input-group-addon">Rota</span>
                                            <asp:TextBox ID="txtRota" runat="server" CssClass="form-control bfh-number"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-4">
                                    <div class="input-group">
                                        <span class="input-group-addon">Leiturista</span>
                                        <asp:DropDownList ID="cbxLeit" runat="server" CssClass="form-control">
                                        </asp:DropDownList>
                                    </div>
                                </div>

                                <div class="col-lg-3" id="panelVencimento" runat="server">
                                    <div class="input-group">
                                        <span class="input-group-addon">Vencimento</span>
                                        <asp:DropDownList ID="ddlVencimento" runat="server" CssClass="form-control">
                                            <asp:ListItem>Escolha o vencimento</asp:ListItem>
                                            <asp:ListItem>1</asp:ListItem>
                                            <asp:ListItem>2</asp:ListItem>
                                            <asp:ListItem>3</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="col-lg-1">
                                    <asp:Button ID="btPesquisar" runat="server" Text="Pesquisar" CssClass="btn btn-primary" OnClick="btPesquisar_Click" />
                                </div>

                            </div>

                        </div>

                        <div class="row" style="padding-top: 10px;">
                            <div class="col-lg-6">
                                <div class="well well-small painelInfo " style="padding-right: 10px; background-color: white">

                                    <div style="background-color: #eee; text-align: center">Agrupar por:</div>
                                    <div class="col-lg-3" style="text-align:center">
                                        <div class="radio">
                                            <asp:RadioButton ID="rbMes" runat="server" GroupName="1" Text="Referência" Checked="True" />
                                        </div>

                                    </div>
                                    <div class="col-lg-3"style="text-align:center">
                                        <div class="radio">
                                            <asp:RadioButton ID="rbRota" runat="server" GroupName="1" Text="Rota"  />
                                        </div>

                                    </div>
                                    <div class="col-lg-3"style="text-align:center">
                                        <div class="radio">
                                            <asp:RadioButton ID="rbLtr" runat="server" GroupName="1" Text="Leiturista" />
                                        </div>
                                        
                                    </div>

                                    <div class="col-lg-3"style="text-align:center">
                                        <div class="radio">
                                            <asp:RadioButton ID="rbVencimento" runat="server" GroupName="1" Text="Vencimento" />
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="row" style="padding-top: 10px">

                            <div class="col-lg-12">
                                <div id="Panelgrid" runat="server" class="">
                                    <div id="scrollDiv" style="overflow: auto; height: 100%; width: 100%">
                                        <asp:GridView ID="GridView1" runat="server" CellPadding="4" ForeColor="#333333" GridLines="None" EmptyDataText="Nenhum registro encontrado" BackColor="White"
                                            BorderColor="#999999" BorderStyle="None" BorderWidth="1px" Width="100%" AllowPaging="True" PageSize="100" HeaderStyle-CssClass="tblTitle" OnPageIndexChanging="GridView1_PageIndexChanging">
                                            <AlternatingRowStyle BackColor="Gainsboro" />
                                            <EditRowStyle HorizontalAlign="Center" />
                                            <FooterStyle BackColor="#CCCCCC" ForeColor="Black" />
                                            <HeaderStyle BackColor="#4f5b69" Font-Bold="True" ForeColor="White" HorizontalAlign="Center" />
                                            <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Left" />
                                            <RowStyle BackColor="#EEEEEE" ForeColor="Black" HorizontalAlign="Center" />
                                            <SelectedRowStyle BackColor="#e1ffff" Font-Bold="True" ForeColor="White" />
                                            <SortedAscendingCellStyle BackColor="#F1F1F1"></SortedAscendingCellStyle>
                                            <SortedAscendingHeaderStyle BackColor="#0000A9"></SortedAscendingHeaderStyle>
                                            <SortedDescendingCellStyle BackColor="#CAC9C9"></SortedDescendingCellStyle>
                                            <SortedDescendingHeaderStyle BackColor="#000065"></SortedDescendingHeaderStyle>
                                        </asp:GridView>
                                    </div>
                                </div>
                            </div>

                        </div>

                    </div>
                </div>
            </div>
        </div>
    </form>
</asp:Content>

