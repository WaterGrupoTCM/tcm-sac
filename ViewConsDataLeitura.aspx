﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="ViewConsDataLeitura.aspx.cs" Inherits="ViewConsDataLeitura" %>
<%@ MasterType VirtualPath="~/Site.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="Scripts/jquery-ui-1.8.24.min.js"></script>
    
    <link href="Content/themes/base/jquery-ui.css" rel="stylesheet" />
    <link href="Content/themes/base/jquery.ui.accordion.css" rel="stylesheet" />
    <link href="Content/themes/base/jquery.ui.all.css" rel="stylesheet" />
    <link href="Content/themes/base/jquery.ui.autocomplete.css" rel="stylesheet" />
    <link href="Content/themes/base/jquery.ui.base.css" rel="stylesheet" />
    <link href="Content/themes/base/jquery.ui.button.css" rel="stylesheet" />
    <link href="Content/themes/base/jquery.ui.core.css" rel="stylesheet" />
    <link href="Content/themes/base/jquery.ui.datepicker.css" rel="stylesheet" />
    <link href="Content/themes/base/jquery.ui.dialog.css" rel="stylesheet" />
    <link href="Content/themes/base/jquery.ui.progressbar.css" rel="stylesheet" />
    <link href="Content/themes/base/jquery.ui.resizable.css" rel="stylesheet" />
    <link href="Content/themes/base/jquery.ui.selectable.css" rel="stylesheet" />
    <link href="Content/themes/base/jquery.ui.slider.css" rel="stylesheet" />
    <link href="Content/themes/base/jquery.ui.tabs.css" rel="stylesheet" />
    <link href="Content/themes/base/jquery.ui.theme.css" rel="stylesheet" />

   
    <script type="text/javascript">

        jQuery(function () {
            var tabName = jQuery("[id*=TabName]").val() != "" ? jQuery("[id*=TabName]").val() : "personal";
            jQuery('#Tabs a[href="#' + tabName + '"]').tab('show');
            jQuery("#Tabs a").click(function () {
                jQuery("[id*=TabName]").val(jQuery(this).attr("href").replace("#", ""));
            });
        });


        jQuery(document).ready(function () { jQuery("#ContentPlaceHolder1_txtDataInicial").mask("99/99/9999"); });
        jQuery(document).ready(function () { jQuery("#ContentPlaceHolder1_txtDataFinal").mask("99/99/9999"); });
        jQuery(document).ready(function () { jQuery("#ContentPlaceHolder1_txtDataFinal").mask("99/99/9999"); });


        jQuery(function () {

            jQuery("#ContentPlaceHolder1_txtDataInicial").datepicker({
                dateFormat: "dd/mm/yy", showOtherMonths: true,
                selectOtherMonths: true,
                dayNames: ["Domingo", "Segunda", "Terça", "Quarta", "Quinta", "Sexta", "Sábado", "Domingo"],
                dayNamesMin: ["D", "S", "T", "Q", "Q", "S", "S", "D"],
                dayNamesShort: ["Dom", "Seg", "Ter", "Qua", "Qui", "Sex", "Sáb", "Dom"],
                monthNames: ["Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro"],
                monthNamesShort: ["Jan", "Fev", "Mar", "Abr", "Mai", "Jun", "Jul", "Ago", "Set", "Out", "Nov", "Dez"],
                showAnim: "scale"
            });

            jQuery("#ContentPlaceHolder1_txtDataFinal").datepicker({
                dateFormat: "dd/mm/yy", showOtherMonths: true,
                selectOtherMonths: true,
                dayNames: ["Domingo", "Segunda", "Terça", "Quarta", "Quinta", "Sexta", "Sábado", "Domingo"],
                dayNamesMin: ["D", "S", "T", "Q", "Q", "S", "S", "D"],
                dayNamesShort: ["Dom", "Seg", "Ter", "Qua", "Qui", "Sex", "Sáb", "Dom"],
                monthNames: ["Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro"],
                monthNamesShort: ["Jan", "Fev", "Mar", "Abr", "Mai", "Jun", "Jul", "Ago", "Set", "Out", "Nov", "Dez"],
                showAnim: "scale"
            });



        });



    </script>

    <style type="text/css">
        .BotaoPesquisa{
            margin-top: 51px;
        }

        th{
            text-align:center;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <form runat="server">
        <div class="row">
            <div class="col-sm-12">
                <asp:HiddenField ID="TabName" runat="server" Value="data"/>
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h4 class="panel-title"><span class="TitulosPanel">Pesquisa de leituras por data ou referência</span></h4>
                    </div>
                    <div class="panel-body minha-classe">
                         <asp:Panel ID="PanelMsg" runat="server" Visible="false">
                            <div class="alert alert-info">
                                <strong>Atenção! </strong>
                                <asp:Label ID="lblMsg" runat="server" Text=""></asp:Label>
                            </div>
                        </asp:Panel>
                        <div class="row">
                            <div class="col-lg-5">
                                <div id="Tabs" role="tabpanel">
                                    <ul class="nav nav-tabs">
                                        <li class="active"><a data-toggle="tab" href="#data">Data</a></li>
                                        <li><a data-toggle="tab" href="#referencia">Referência</a></li>
                                    </ul>

                                    <div class="tab-content" style="padding-top:10px">
                                        <div id="data" class="tab-pane fade in active">
                                            <div class="row">
                                                <div class="col-lg-6">
                                                     <div class="input-group">
                                                        <span class="input-group-addon" >Data Inicial</span>
                                                        <asp:TextBox ID="txtDataInicial" runat="server" CssClass="form-control"></asp:TextBox>
                                                    </div>
                                                </div>

                                                <div class="col-lg-6">
                                                    <div class="input-group">
                                                        <span class="input-group-addon">Data Final</span>
                                                        <asp:TextBox ID="txtDataFinal" runat="server" CssClass="form-control"></asp:TextBox>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>

                                        <div id="referencia" class="tab-pane fade">
                                            
                                            <div class="row">
                                                <div class="col-lg-6">
                                                    <div class="input-group">
                                                        <span class="input-group-addon">Mês</span>
                                                        <asp:DropDownList ID="cbxMes" runat="server" CssClass="form-control">
                                                            <asp:ListItem>Selecione o mês</asp:ListItem>
                                                            <asp:ListItem>Janeiro</asp:ListItem>
                                                            <asp:ListItem>Fevereiro</asp:ListItem>
                                                            <asp:ListItem>Março</asp:ListItem>
                                                            <asp:ListItem>Abril</asp:ListItem>
                                                            <asp:ListItem>Maio</asp:ListItem>
                                                            <asp:ListItem>Junho</asp:ListItem>
                                                            <asp:ListItem>Julho</asp:ListItem>
                                                            <asp:ListItem>Agosto</asp:ListItem>
                                                            <asp:ListItem>Setembro</asp:ListItem>
                                                            <asp:ListItem>Outubro</asp:ListItem>
                                                            <asp:ListItem>Novembro</asp:ListItem>
                                                            <asp:ListItem>Dezembro</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>

                                                <div class="col-lg-4">
                                                    <div class="input-group">
                                                        <span class="input-group-addon" id="sizing-addon2">Ano</span>
                                                        <asp:TextBox ID="txtAno" runat="server" CssClass="form-control bfh-number"></asp:TextBox>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="col-lg-2">
                                <asp:Button ID="btPesquisar" runat="server" Text="Pesquisar" CssClass="btn btn-primary BotaoPesquisa" OnClick="btPesquisar_Click" />
                            </div>
                        </div>


                        <div class="row" style="padding-top:10px">

                            <div class="col-lg-12">
                                <div id="Panelgrid" runat="server" class="">
                                    <div id="scrollDiv" style="overflow: auto; height: 100%; width: 100%">
                                        <asp:GridView ID="GridView1" runat="server" CellPadding="4" ForeColor="#333333" GridLines="None" EmptyDataText="Nenhum registro encontrado" BackColor="White"
                                            BorderColor="#999999" BorderStyle="None" BorderWidth="1px" Width="100%" AllowPaging="True" PageSize="100" HeaderStyle-CssClass="tblTitle" OnPageIndexChanging="GridView1_PageIndexChanging">
                                            <AlternatingRowStyle BackColor="Gainsboro" />
                                            <EditRowStyle HorizontalAlign="Center" />
                                            <FooterStyle BackColor="#CCCCCC" ForeColor="Black" />
                                            <HeaderStyle BackColor="#4f5b69" Font-Bold="True" ForeColor="White" HorizontalAlign="Center" />
                                            <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Left" />
                                            <RowStyle BackColor="#EEEEEE" ForeColor="Black" HorizontalAlign="Center" />
                                            <SelectedRowStyle BackColor="#e1ffff" Font-Bold="True" ForeColor="White" />
                                            <SortedAscendingCellStyle BackColor="#F1F1F1"></SortedAscendingCellStyle>
                                            <SortedAscendingHeaderStyle BackColor="#0000A9"></SortedAscendingHeaderStyle>
                                            <SortedDescendingCellStyle BackColor="#CAC9C9"></SortedDescendingCellStyle>
                                            <SortedDescendingHeaderStyle BackColor="#000065"></SortedDescendingHeaderStyle>
                                        </asp:GridView>
                                    </div>
                                </div>
                            </div>

                        </div>

                    </div>
                </div>
            </div>
        </div>
    </form>



</asp:Content>
