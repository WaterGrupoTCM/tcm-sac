﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Globalization;
using WaterSyncLite.MDL;
using WaterSyncLite.DAL;
using WaterSyncLite.Class;
using System.IO;
using System.ServiceModel.Channels;
using System.ServiceModel.Security;
using System.Text;
using AjaxControlToolkit;
using iTextSharp.text;
using Microsoft.WindowsMobile.Samples.Location;
using System.Drawing;
using System.Drawing.Imaging;
using System.Drawing.Drawing2D;

public partial class ViewEquipamento : System.Web.UI.Page
{

    protected void Page_Load(object sender, EventArgs e)
    {
        DataTable dtLtr;
        DataTable dtGrid;
        this.Master.LoadHeader(this.Master, Request.Path.Substring(Request.Path.LastIndexOf("/") + 1));
        if (!IsPostBack)
        {

            this.Form.DefaultButton = btSalvar.UniqueID;

            String StrCon = carregaConexao();

            string sql = "";

            Data db = new Data();

            #region LEITURISTAS
            dtLtr = db.GetDt(carregaConexao(), "WTR_LEIT", "SELECT LTR_ID, LTR_NOME FROM "
                + " WTR_LEITURISTAS WHERE CLI_ID = " + Request.Cookies["CliID"].Value.ToString().Trim()
                + " AND EMP_ID = " + Request.Cookies["EmpID"].Value.ToString().Trim()
                + " ORDER BY LTR_NOME");

            DataRow dtr = dtLtr.NewRow();
            dtr["LTR_NOME"] = "Selecione um leiturista...";
            dtr["LTR_ID"] = 0;
            dtLtr.Rows.Add(dtr);
            if (dtLtr != null)
            {
                ddlLtr.DataSource = dtLtr;
                ddlLtr.DataTextField = "LTR_NOME";
                ddlLtr.DataValueField = "LTR_ID";
                ddlLtr.DataBind();
                ddlLtr.SelectedValue = "0";
            }

            Session["dtLtr"] = dtLtr;
            #endregion

            #region Grid
            dtGrid = db.GetDt(carregaConexao(), "WTR_EQT", "SELECT EQT_ID AS CÓDIGO, LTR_NOME AS LEITURISTA, EQT_TIPO AS EQUIPAMENTO,EQT_MODELO AS MODELO, EQT_SERIE AS SERIE FROM "
                + " WTR_EQUIPAMENTOS INNER JOIN WTR_LEITURISTAS ON WTR_EQUIPAMENTOS.LTR_ID = WTR_LEITURISTAS.LTR_ID WHERE WTR_EQUIPAMENTOS.CLI_ID = " + Request.Cookies["CliID"].Value.ToString().Trim()
                + "AND WTR_EQUIPAMENTOS.EMP_ID = " + Request.Cookies["EmpID"].Value.ToString().Trim()
                + " ORDER BY EQT_ID");

            GridEquipamento.DataSource = dtGrid;
            GridEquipamento.DataBind();

            #endregion
        }
    }


    protected String carregaConexao()
    {
        String StrCon = "";
        if (Request.Cookies["StrCon"] != null)
        {
            if (Request.Cookies["StrCon"].Value.ToString().Length > 0)
                StrCon = Request.Cookies["StrCon"].Value.ToString();
        }
        return StrCon.Replace('|', ';').Replace("usuario", xml.GetConexaoValue(Server.MapPath("~"), "LOCAL", "USER")).Replace("senha", xml.GetConexaoValue(Server.MapPath("~"), "LOCAL", "PASSWORD"));
    }


    protected void ShowMessage(string msg)
    {
        String scriptString = "<script language = JavaScript>";
        scriptString += "confirm('" + msg + "');";
        scriptString += "</script>";
        Page.ClientScript.RegisterStartupScript(Page.GetType(), "scrt", scriptString);
    }

    protected void btSalvar_Click(object sender, EventArgs e)
    {
        String StrCon = carregaConexao();

        string strCon = carregaConexao();
        DataTable dt = new DataTable();
        Data db = new Data();
        string erro = "";
        string sql = "";
       
        try
        {
            sql = "INSERT INTO WTR_EQUIPAMENTOS (EMP_ID,CLI_ID,LTR_ID, EQT_MARCA, EQT_MODELO, EQT_SERIE, EQT_TIPO)" +
                  "VALUES(1," + Convert.ToInt32(Request.Cookies["CliID"].Value.ToString().Trim()) +", "+ddlLtr.Text+", '"+txtMarca.Text+"', '"+txtModelo.Text + "', '"+txtSerie.Text + "', '"+txtTipo.Text + "' )";
            erro = db.Ins(strCon, sql);
        }
        catch (Exception ex)
        {
            Console.WriteLine(erro);
            throw;
        }

    }

    protected void btCancelar_Click(object sender, EventArgs e)
    {
        txtTipo.Text = "";
        txtMarca.Text = "";
        txtModelo.Text = "";
        txtSerie.Text = "";
    }

    protected void CarregadGrid()
    {
        DataTable dtLtr;
        DataTable dtGrid;
        Data db = new Data();

        #region LEITURISTAS
        dtLtr = db.GetDt(carregaConexao(), "WTR_LEIT", "SELECT LTR_ID, LTR_NOME FROM "
            + " WTR_LEITURISTAS WHERE CLI_ID = " + Request.Cookies["CliID"].Value.ToString().Trim()
            + " AND EMP_ID = " + Request.Cookies["EmpID"].Value.ToString().Trim()
            + " ORDER BY LTR_NOME");

        DataRow dtr = dtLtr.NewRow();
        dtr["LTR_NOME"] = "Selecione um leiturista...";
        dtr["LTR_ID"] = 0;
        dtLtr.Rows.Add(dtr);
        if (dtLtr != null)
        {
            ddlLtr.DataSource = dtLtr;
            ddlLtr.DataTextField = "LTR_NOME";
            ddlLtr.DataValueField = "LTR_ID";
            ddlLtr.DataBind();
            ddlLtr.SelectedValue = "0";
        }

        Session["dtLtr"] = dtLtr;
        #endregion

        #region Grid
        dtGrid = db.GetDt(carregaConexao(), "WTR_EQT", "SELECT EQT_ID AS CÓDIGO, LTR_NOME AS LEITURISTA, EQT_TIPO AS EQUIPAMENTO,EQT_MODELO AS MODELO, EQT_SERIE AS SERIE FROM "
            + " WTR_EQUIPAMENTOS INNER JOIN WTR_LEITURISTAS ON WTR_EQUIPAMENTOS.LTR_ID = WTR_LEITURISTAS.LTR_ID WHERE WTR_EQUIPAMENTOS.CLI_ID = " + Request.Cookies["CliID"].Value.ToString().Trim()
            + "AND WTR_EQUIPAMENTOS.EMP_ID = " + Request.Cookies["EmpID"].Value.ToString().Trim()
            + " ORDER BY EQT_ID");

        GridEquipamento.DataSource = dtGrid;
        GridEquipamento.DataBind();
        #endregion
    }
}