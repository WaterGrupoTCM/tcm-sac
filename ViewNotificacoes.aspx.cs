﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WaterSyncLite.Class;

public partial class ViewNotificacoes : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        this.Master.LoadHeader(this.Master, Request.Path.Substring(Request.Path.LastIndexOf("/") + 1));
        if (!IsPostBack)
        {
            carregaDispositivos();
            divMsg.Visible = false;
        }
    }

    protected String carregaConexao()
    {
        String StrCon = "";
        if (Request.Cookies["StrCon"] != null)
        {
            if (Request.Cookies["StrCon"].Value.ToString().Length > 0)
                StrCon = Request.Cookies["StrCon"].Value.ToString();
        }
        return StrCon.Replace('|', ';').Replace("usuario", xml.GetConexaoValue(Server.MapPath("~"), "LOCAL", "USER")).Replace("senha", xml.GetConexaoValue(Server.MapPath("~"), "LOCAL", "PASSWORD"));
    }

    protected void carregaDispositivos()
    {
        try
        {
            Data db = new Data();
            DataTable dt = db.GetDt(carregaConexao(), "WTR_LIVROS", "select dis_id,dis_modelo,DIS_USUARIO,dis_token,convert(varchar,dis_datacadastro,103) as dis_datacadastro from wtr_dispositivo");

            GridDispositivos.DataSource = dt;
            GridDispositivos.DataBind();

            Session["dt"] = dt;

        }
        catch (Exception ex)
        {
            divMsg.Visible = true;
            txtMsg.Text = "Erro - "+ex.Message;
        }


    }
    protected void GridDispositivos_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Msg")
        {
            Data db = new Data();
            int indexGrid = Convert.ToInt32(e.CommandArgument);
            DataTable dt = (DataTable)Session["dt"];
            DataRow dr = dt.Rows[indexGrid];

            lblToken.Text = dr["dis_token"].ToString();
            lblModelo.Text = dr["dis_modelo"].ToString();
            lblData.Text = dr["dis_datacadastro"].ToString();





            Page.ClientScript.RegisterStartupScript(GetType(), "MyKey", "jQuery('#myModal').modal('show');", true);


        }
    }
    protected void GridDispositivos_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        escondeColunaGrid(6, e);
    }
    public void escondeColunaGrid(int coluna, GridViewRowEventArgs e)
    {
        switch (e.Row.RowType)
        {
            case DataControlRowType.Header:
                e.Row.Cells[coluna].Visible = false;
                break;
            case DataControlRowType.DataRow:
                e.Row.Cells[coluna].Visible = false;
                break;
            case DataControlRowType.Footer:
                e.Row.Cells[coluna].Visible = false;
                break;
        }
    }
    protected void btEnviarMsg_Click(object sender, EventArgs e)
    {

        try {
            string SERVER_API_KEY = "AIzaSyAO_-BuKsZDfxOaaPFHunRX0XLDNjRK_Hg";
            var SENDER_ID = "application number";
            WebRequest tRequest;
            tRequest = WebRequest.Create("https://fcm.googleapis.com/fcm/send");
            tRequest.Method = "post";
            tRequest.ContentType = " application/json";
            tRequest.Headers.Add(string.Format("Authorization: key={0}", SERVER_API_KEY));

            string postData = "{\"notification\": {\"body\": \"" + txtMsg.Text + "\",\"title\": \"TCM-SAC - Alerta\",\"sound\": \"default\",\"priority\": \"high\"},\"data\":{\"id\": 1},\"to\": \"" + lblToken.Text + "\"}";
            Console.WriteLine(postData);
            Byte[] byteArray = Encoding.UTF8.GetBytes(postData);
            tRequest.ContentLength = byteArray.Length;

            Stream dataStream = tRequest.GetRequestStream();
            dataStream.Write(byteArray, 0, byteArray.Length);
            dataStream.Close();

            WebResponse tResponse = tRequest.GetResponse();

            dataStream = tResponse.GetResponseStream();

            StreamReader tReader = new StreamReader(dataStream);

            String sResponseFromServer = tReader.ReadToEnd();


            tReader.Close();
            dataStream.Close();
            tResponse.Close();
            if (sResponseFromServer.Contains("\"success\":1"))
            {
                lblMsg.Text = ("Mensagem enviada com sucesso.");
                divMsg.Attributes["class"] += " panel panel-success";
            }
            else
            {
                lblMsg.Text = ("Erro ao enviar mensagem");
                divMsg.Attributes["class"] += " panel panel-failed";
            }

            txtMsg.Text = "";

            divMsg.Visible = true;


            Page.ClientScript.RegisterStartupScript(GetType(), "MyKey", "jQuery('#myModal').modal('close');", true);
        }
        catch (Exception ex)
        {
            divMsg.Visible = true;
            lblMsg.Text = ("Erro ao enviar mensagem");
            divMsg.Attributes["class"] += " panel panel-danger";
            Page.ClientScript.RegisterStartupScript(GetType(), "MyKey", "jQuery('#myModal').modal('close');", true);

        }
        
    }

    protected void ShowMessage(string msg)
    {
        String scriptString = "<script language = JavaScript>";
        scriptString += "confirm('" + msg + "');";
        scriptString += "</script>";
        Page.ClientScript.RegisterStartupScript(Page.GetType(), "scrt", scriptString);
    }


    protected void btEnviarTodos_Click(object sender, EventArgs e)
    {
        try
        {
            string apiKey = "AIzaSyAO_-BuKsZDfxOaaPFHunRX0XLDNjRK_Hg";

            string keyGroup = VerificaGrupoExiste(Request.Cookies["CliID"].Value.PadLeft(2, '0').ToString().Trim());


            if (!keyGroup.Contains("Erro"))
            {
                keyGroup = keyGroup.Split(':')[1].Replace("}", "").Replace("\"", "");
            }

            WebRequest tRequest;
            tRequest = WebRequest.Create("https://gcm-http.googleapis.com/gcm/send");
            tRequest.Method = "post";
            tRequest.ContentType = "application/json";
            tRequest.Headers.Add(string.Format("Authorization: key={0}", apiKey));

            string postData = "{\"to\": \"" + keyGroup + "\",  	\"notification\": {		\"body\": \"" + txtMsgTodos.Text + "\",		\"title\": \"TCM-SAC Alerta\",		\"sound\": \"default\",		\"priority\": \"high\"}}";



            Byte[] byteArray = Encoding.UTF8.GetBytes(postData);
            tRequest.ContentLength = byteArray.Length;
            Stream dataStream = tRequest.GetRequestStream();
            dataStream.Write(byteArray, 0, byteArray.Length);
            dataStream.Close();
            WebResponse tResponse = tRequest.GetResponse();
            dataStream = tResponse.GetResponseStream();
            StreamReader tReader = new StreamReader(dataStream);
            String sResponseFromServer = tReader.ReadToEnd();
            HttpWebResponse httpResponse = (HttpWebResponse)tResponse;
            string statusCode = httpResponse.StatusCode.ToString();
            tReader.Close();
            dataStream.Close();
            tResponse.Close();

            Page.ClientScript.RegisterStartupScript(GetType(), "MyKey", "jQuery('#myModalTodos').modal('close');", true);
            txtMsgTodos.Text = "";
            divMsg.Visible = false;


        }
        catch (Exception ex)
        {
            ShowMessage("Erro - " + ex.Message);
        }

    }

    public string VerificaGrupoExiste(string cliente)
    {

        try
        {
            string SERVER_API_KEY = "AIzaSyAO_-BuKsZDfxOaaPFHunRX0XLDNjRK_Hg";
            string SENDER_ID = "342842078495";
            WebRequest tRequest;
            tRequest = WebRequest.Create("https://android.googleapis.com/gcm/notification?notification_key_name=" + cliente + "");
            tRequest.Method = "get";
            tRequest.ContentType = " application/json";
            tRequest.Headers.Add(string.Format("Authorization: key={0}", SERVER_API_KEY));
            tRequest.Headers.Add(string.Format("project_id: {0}", SENDER_ID));


            Stream dataStream = null;

            WebResponse tResponse = tRequest.GetResponse();

            dataStream = tResponse.GetResponseStream();

            StreamReader tReader = new StreamReader(dataStream);

            String sResponseFromServer = tReader.ReadToEnd();


            tReader.Close();
            dataStream.Close();
            tResponse.Close();

            return sResponseFromServer;

        }
        catch (Exception ex)
        {
            return "Erro - " + ex.Message;


        }



    }

    protected void imgBtMsgTodos_Click(object sender, ImageClickEventArgs e)
    {

        Page.ClientScript.RegisterStartupScript(GetType(), "MyKey", "jQuery('#myModalTodos').modal('show');", true);
        divMsg.Visible = false;

    }
    protected void chkMarcar_CheckedChanged(object sender, EventArgs e)
    {

    }
    protected void imgDesmarcar_CheckedChanged(object sender, EventArgs e)
    {

        CheckBox chkHeader = (CheckBox)sender;


        for (int i = 0; i < GridDispositivos.Rows.Count; i++)
        {
            if (GridDispositivos.Rows[i].RowType == DataControlRowType.DataRow)
            {
                string flag = "";

                CheckBox checkbox = (CheckBox)GridDispositivos.Rows[i].Cells[5].Controls[1];
                if (chkHeader.Checked)
                {
                    checkbox.Checked = true;

                    flag = "N";

                    Session["chkHeader"] = "true";
                }
                else
                {
                    checkbox.Checked = false;
                    flag = "S";
                    Session["chkHeader"] = "false";
                }
            }
        }
        divMsg.Visible = false;

    }
    protected void imgBtMsgSelect_Click(object sender, ImageClickEventArgs e)
    {

        Page.ClientScript.RegisterStartupScript(GetType(), "MyKey", "jQuery('#myModalSelecionados').modal('show');", true);
        divMsg.Visible = false;

        


    }

    public string GruposGCM(string cliente, string operacao, string[] device, string keyGroup)
    {

        try
        {
            string apiKey = "AIzaSyAO_-BuKsZDfxOaaPFHunRX0XLDNjRK_Hg";
            var senderId = "342842078495";

            WebRequest tRequest;
            tRequest = WebRequest.Create("https://android.googleapis.com/gcm/notification");
            tRequest.Method = "post";
            tRequest.ContentType = "application/json";
            tRequest.Headers.Add(string.Format("Authorization: key={0}", apiKey));
            tRequest.Headers.Add(string.Format("project_id: {0}", senderId));
            string postData = "{   \"operation\": \"" + operacao + "\",   \"notification_key_name\": \"" + cliente + "\",";
            if (operacao != "create")
            {
                postData += keyGroup.Replace("}", "").Replace("{", "") + ", ";
            }
            postData += "   \"registration_ids\": [\"";

            if (device.Length > 0)
            {
               

                foreach (string item in device)
                {
                    if(item != null && item != "")
                        postData += item + "\",\"";
                }

                postData = postData.Substring(0, postData.Length - 3);

            }
            else postData += device;
            postData += "\"]}";


            Byte[] byteArray = Encoding.UTF8.GetBytes(postData);
            tRequest.ContentLength = byteArray.Length;
            Stream dataStream = tRequest.GetRequestStream();
            dataStream.Write(byteArray, 0, byteArray.Length);
            dataStream.Close();
            WebResponse tResponse = tRequest.GetResponse();
            dataStream = tResponse.GetResponseStream();
            StreamReader tReader = new StreamReader(dataStream);
            String sResponseFromServer = tReader.ReadToEnd();
            HttpWebResponse httpResponse = (HttpWebResponse)tResponse;
            string statusCode = httpResponse.StatusCode.ToString();
            tReader.Close();
            dataStream.Close();
            tResponse.Close();
            return sResponseFromServer;

        }
        catch (Exception ex)
        {
            return "Erro - " + ex.Message;
        }

    }
    protected void GridDispositivos_RowDataBound1(object sender, GridViewRowEventArgs e)
    {
        escondeColunaGrid(0, e);
    }

    protected void btEnviarMsgSelecionadas_Click(object sender, EventArgs e)
    {
        String[] dispositivos = new String[GridDispositivos.Rows.Count];
        String[] TodosDispositivos = new String[GridDispositivos.Rows.Count];
        for (int i = 0; i < GridDispositivos.Rows.Count; i++)
        {
            if (GridDispositivos.Rows[i].RowType == DataControlRowType.DataRow)
            {
                string flag = "";

                CheckBox checkbox = (CheckBox)GridDispositivos.Rows[i].Cells[5].Controls[1];
                if (checkbox.Checked)
                {
                    dispositivos[i] = ((Label)GridDispositivos.Rows[i].Cells[0].Controls[1]).Text;

                }
                TodosDispositivos[i] = ((Label)GridDispositivos.Rows[i].Cells[0].Controls[1]).Text;
            }
        }
        Data db = new Data();
        DataTable dtUser = db.GetDt(Conexao.StrCon, "wtr_user", "select * from wtr_user where usr_id = " + Request.Cookies["user"].Value.ToString());
        string usuario = dtUser.DefaultView[0].Row["usr_username"].ToString();




        string keyGroup = VerificaGrupoExiste(usuario.Trim());
        string result = "";
        if (!keyGroup.Contains("Erro"))
        {
            result = GruposGCM(usuario, "remove", TodosDispositivos, keyGroup);

            result = GruposGCM(usuario, "create", dispositivos, result);
        }else result = GruposGCM(usuario, "create", dispositivos, keyGroup);


        String Api = EnviaSMS(result);
        if (Api.Contains("\"failure\":0"))
        {
            lblMsg.Text = ("Mensagem enviada com sucesso.");
            divMsg.Attributes["class"] += " panel panel-success";
        }
        else
        {
            lblMsg.Text = ("Erro ao enviar mensagem");
            divMsg.Attributes["class"] += " panel panel-failed";
        }

        txtMsg.Text = "";

        divMsg.Visible = true;


    }

    protected string EnviaSMS(string keyGroup)
    {
        try
        {
            string apiKey = "AIzaSyAO_-BuKsZDfxOaaPFHunRX0XLDNjRK_Hg";

            


            if (!keyGroup.Contains("Erro"))
            {
                keyGroup = keyGroup.Split(':')[1].Replace("}", "").Replace("\"", "");
            }

            WebRequest tRequest;
            tRequest = WebRequest.Create("https://gcm-http.googleapis.com/gcm/send");
            tRequest.Method = "post";
            tRequest.ContentType = "application/json";
            tRequest.Headers.Add(string.Format("Authorization: key={0}", apiKey));

            string postData = "{\"to\": \"" + keyGroup + "\",  	\"notification\": {		\"body\": \"" + txtMSgSelecionadas.Text + "\",		\"title\": \"TCM-SAC Alerta\",		\"sound\": \"default\",		\"priority\": \"high\"}}";



            Byte[] byteArray = Encoding.UTF8.GetBytes(postData);
            tRequest.ContentLength = byteArray.Length;
            Stream dataStream = tRequest.GetRequestStream();
            dataStream.Write(byteArray, 0, byteArray.Length);
            dataStream.Close();
            WebResponse tResponse = tRequest.GetResponse();
            dataStream = tResponse.GetResponseStream();
            StreamReader tReader = new StreamReader(dataStream);
            String sResponseFromServer = tReader.ReadToEnd();
            HttpWebResponse httpResponse = (HttpWebResponse)tResponse;
            string statusCode = httpResponse.StatusCode.ToString();
            tReader.Close();
            dataStream.Close();
            tResponse.Close();

            Page.ClientScript.RegisterStartupScript(GetType(), "MyKey", "jQuery('#myModalSelecionados').modal('close');", true);
            txtMSgSelecionadas.Text = "";
            divMsg.Visible = false;

            return sResponseFromServer;


        }
        catch (Exception ex)
        {
            return ("Erro - " + ex.Message);
        }
    }


}
