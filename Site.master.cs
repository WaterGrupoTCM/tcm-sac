﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WaterSyncLite.Class;

public partial class SiteMaster : System.Web.UI.MasterPage
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    public void CarregaCampos(string cliId, string mes, string ano, string usuario, string empID)
    {

        whriteCookie("CliID", cliId);
        whriteCookie("EmpID", empID);
        whriteCookie("mes", mes);
        whriteCookie("ano", ano);
        whriteCookie("user", usuario);



    }

    public void LoadHeader(MasterPage master,string pagina)
    {
        try
        {
            WaterSyncLite.DAL.EmpresaDAL empresaDAL = new WaterSyncLite.DAL.EmpresaDAL();
            //lblNomeCliente.Text = empresaDAL.getEmp(Convert.ToInt32(Request.Cookies["CliID"].Value)).RazaoSocial;*/

            Label empresa = (Label)master.FindControl("lblRazaoSocial");
            empresa.Text = empresaDAL.getEmp(Convert.ToInt32(Request.Cookies["CliID"].Value)).RazaoSocial;

            Label referencia = (Label)master.FindControl("lblRef");

            referencia.Text = Request.Cookies["mes"].Value.PadLeft(2, '0') + "/" + Request.Cookies["ano"].Value;

            Data db = new Data();
            DataTable dtUser = db.GetDt(Conexao.StrCon, "wtr_user", "select * from wtr_user where usr_id = " + Request.Cookies["user"].Value.ToString());

            if (dtUser.DefaultView.Count > 0)
            {

                #region MENU
                if (dtUser.DefaultView[0].Row["USR_TELAIMPORT"].ToString() == "False")
                    Import.Visible = false;
                else
                    Import.Visible = true;

                if (dtUser.DefaultView[0].Row["USR_TELAEXPORT"].ToString() == "False")
                    Export.Visible = false;
                else
                    Export.Visible = true;

                if (dtUser.DefaultView[0].Row["USR_TELASYNC"].ToString() == "False")
                    Sync.Visible = false;
                else
                    Sync.Visible = true;

                if (dtUser.DefaultView[0].Row["USR_TELAREPASSE"].ToString() == "False")
                    Repasse.Visible = false;
                else
                    Repasse.Visible = true;

                if (dtUser.DefaultView[0].Row["USR_TELALIBERARROTAS"].ToString() == "False")
                    LiberarRotas.Visible = false;
                else
                    LiberarRotas.Visible = true;

                if (dtUser.DefaultView[0].Row["USR_TELAALTERACAOLEITURA"].ToString() == "False")
                    LiberarAlteracao.Visible = false;
                else
                    LiberarAlteracao.Visible = true;

                if (dtUser.DefaultView[0].Row["USR_TELAANALISECRITICA"].ToString() == "False")
                    AnaliseCritica.Visible = false;
                else
                    AnaliseCritica.Visible = true;

                if (dtUser.DefaultView[0].Row["USR_TELAFECHAMENTO"].ToString() == "False")
                    Fechamentos.Visible = false;
                else
                    Fechamentos.Visible = true;

                if (dtUser.DefaultView[0].Row["USR_TELADATALEITURA"].ToString() == "False")
                    DataLeituras.Visible = false;
                else
                    DataLeituras.Visible = true;

                if (dtUser.DefaultView[0].Row["USR_TELAOCORRENCIAS"].ToString() == "False")
                    Ocorrencias.Visible = false;
                else
                    Ocorrencias.Visible = true;

                if (dtUser.DefaultView[0].Row["USR_TELAROTEIROS"].ToString() == "False")
                    Roteiros.Visible = false;
                else
                    Roteiros.Visible = true;

                if (dtUser.DefaultView[0].Row["USR_TELAGEOREFERENCIAMENTO"].ToString() == "False")
                    Georreferenciamento.Visible = false;
                else
                    Georreferenciamento.Visible = true;

                if (dtUser.DefaultView[0].Row["USR_ADM"].ToString() != "S")
                    Adm.Visible = false;
                else
                    Adm.Visible = true;
                if(dtUser.DefaultView[0].Row["USR_TELAFORMAENTREGA"].ToString() == "False")
                    Entrega.Visible = false;
                else
                    Entrega.Visible = true;

                #endregion

                #region Pagina
                if (pagina.Trim().Equals("ViewImport.aspx"))
                {
                    if (dtUser.DefaultView[0].Row["USR_TELAIMPORT"].ToString() == "False")
                        Response.Redirect("Default.aspx");
                }
                else if (pagina.Trim().Equals("ViewExport.aspx"))
                {
                    if (dtUser.DefaultView[0].Row["USR_TELAEXPORT"].ToString() == "False")
                        Response.Redirect("Default.aspx");
                }
                else if (pagina.Trim().Equals("ViewSincronizar.aspx"))
                {
                    if (dtUser.DefaultView[0].Row["USR_TELASYNC"].ToString() == "False")
                        Response.Redirect("Default.aspx");
                }
                else if (pagina.Trim().Equals("ViewRepasse.aspx"))
                {
                    if (dtUser.DefaultView[0].Row["USR_TELAREPASSE"].ToString() == "False")
                        Response.Redirect("Default.aspx");
                }
                else if (pagina.Trim().Equals("ViewLiberarRotas.aspx"))
                {
                    if (dtUser.DefaultView[0].Row["USR_TELALIBERARROTAS"].ToString() == "False")
                        Response.Redirect("Default.aspx");
                }
                else if (pagina.Trim().Equals("ViewAdm.aspx"))
                {
                    if (dtUser.DefaultView[0].Row["USR_ADM"].ToString() != "S")
                        Response.Redirect("Default.aspx");
                }
                else if (pagina.Trim().Equals("ViewPermissao.aspx"))
                {
                    if (dtUser.DefaultView[0].Row["USR_TELAALTERACAOLEITURA"].ToString() == "False")
                        Response.Redirect("Default.aspx");
                }
                else if (pagina.Trim().Equals("ViewConsAnalise.aspx"))
                {
                    if (dtUser.DefaultView[0].Row["USR_TELAANALISECRITICA"].ToString() == "False")
                        Response.Redirect("Default.aspx");
                }
                else if (pagina.Trim().Equals("ViewFechamentosMunicipais.aspx"))
                {
                    if (dtUser.DefaultView[0].Row["USR_TELAFECHAMENTO"].ToString() == "False")
                        Response.Redirect("Default.aspx");
                }
                else if (pagina.Trim().Equals("ViewConsDataLeitura.aspx"))
                {
                    if (dtUser.DefaultView[0].Row["USR_TELADATALEITURA"].ToString() == "False")
                        Response.Redirect("Default.aspx");
                }
                else if (pagina.Trim().Equals("ViewConsOcorrencia.aspx"))
                {
                    if (dtUser.DefaultView[0].Row["USR_TELAOCORRENCIAS"].ToString() == "False")
                        Response.Redirect("Default.aspx");
                }
                else if (pagina.Trim().Equals("ViewRoteiro.aspx"))
                {
                    if (dtUser.DefaultView[0].Row["USR_TELAROTEIROS"].ToString() == "False")
                        Response.Redirect("Default.aspx");
                }

                else if (pagina.Trim().Equals("Georreferenciamento.aspx"))
                {
                    if (dtUser.DefaultView[0].Row["USR_TELAGEOREFERENCIAMENTO"].ToString() == "False")
                        Response.Redirect("Default.aspx");
                }

                else if (pagina.Trim().Equals("ViewEntrega.aspx"))
                {
                    if (dtUser.DefaultView[0].Row["USR_TELAFORMAENTREGA"].ToString() == "False")
                        Response.Redirect("Default.aspx");
                }                

                #endregion

            }

        }
        catch (Exception ex)
        {
            Response.Redirect("Default.aspx");
        }
    }



    public void whriteCookie(string name, string value)
    {
        if (Request.Cookies[name] != null)
        {
            Request.Cookies[name].Values.Clear();
        }
        var newCookie = new HttpCookie(name);
        newCookie.Value = value;
        newCookie.Expires = DateTime.Now.AddMinutes(60); //Expira em 20 minutos. 
        //newCookie.Domain = "187.45.225.14"; //Comente Para funcionar Local 
        //newCookie.Path = "/WaterSyncSqlite"; //Comente Para funcionar Local 
        //newCookie.Secure = true; //Comente Para funcionar Local 
        Response.Cookies.Add(newCookie);
    }
}
