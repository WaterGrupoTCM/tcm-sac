﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="ViewEmpresa.aspx.cs" Inherits="ViewEmpresa" %>
<%@ MasterType VirtualPath="~/Site.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
    <style type="text/css">
   
        .style17
        {
            width: 100%;
        }
        .style18
        {
            font-size: x-large;
        }
        .style19
        {
            height: 51px;
        }
   
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
    <table class="style17">
        <tr>
            <td class="style18">
                Empresas</td>
        </tr>
        <tr>
            <td>
                <span __designer:mapid="b5" class="style9" style="width: 851px; height: 109px">
                <asp:GridView ID="GridView1" runat="server" CellPadding="0" ForeColor="#333333" 
                    GridLines="None" Height="58px" onrowcommand="GridView1_RowCommand" 
                    style="text-align: center" Width="772px">
                    <AlternatingRowStyle BackColor="White" />
                    <Columns>
                        <asp:ButtonField CommandName="Select" Text="Select" />
                    </Columns>
                    <EditRowStyle BackColor="#2461BF" />
                    <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                    <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                    <RowStyle BackColor="#EFF3FB" />
                    <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                    <SortedAscendingCellStyle BackColor="#F5F7FB" />
                    <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                    <SortedDescendingCellStyle BackColor="#E9EBEF" />
                    <SortedDescendingHeaderStyle BackColor="#4870BE" />
                    <SortedAscendingCellStyle BackColor="#F5F7FB" />
                    <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                    <SortedDescendingCellStyle BackColor="#E9EBEF" />
                    <SortedDescendingHeaderStyle BackColor="#4870BE" />
                </asp:GridView>
                </span>
            </td>
        </tr>
        <tr>
            <td>
                <br />
                Empresa&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Razão 
                Social&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Nome Fantasia<br 
                    __designer:mapid="ca" />
                <asp:TextBox ID="txtId" runat="server" Width="94px"></asp:TextBox>
                &nbsp;
                <asp:TextBox ID="txtRazao" runat="server" Width="262px"></asp:TextBox>
                &nbsp;
                <asp:TextBox ID="txtFantasia" runat="server" Width="358px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                Endereço&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
                Bairro&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; CEP<br 
                    __designer:mapid="cf" />
                <asp:TextBox ID="txtEnd" runat="server" Width="316px"></asp:TextBox>
                &nbsp;&nbsp;
                <asp:TextBox ID="txtBairro" runat="server" Width="189px"></asp:TextBox>
                &nbsp;&nbsp;&nbsp;
                <asp:TextBox ID="txtCEP" runat="server" Width="156px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="style19">
                Cidade&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; UF&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; e-mail<br 
                    __designer:mapid="d4" />
                <asp:TextBox ID="txtCidade" runat="server" Width="218px"></asp:TextBox>
                &nbsp;
                <asp:TextBox ID="txtUF" runat="server" Width="74px"></asp:TextBox>
                &nbsp;&nbsp;
                <asp:TextBox ID="txtEmail" runat="server" Width="319px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                Contato 1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Fone 1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
                Home Page<br __designer:mapid="d9" />
                <asp:TextBox ID="txtCto1" runat="server" Width="171px"></asp:TextBox>
                &nbsp;&nbsp;&nbsp;
                <asp:TextBox ID="txtFone1" runat="server" Width="151px"></asp:TextBox>
                &nbsp;&nbsp;
                <asp:TextBox ID="txtHomePage" runat="server" Width="349px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                Contato 2&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Fone 2&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <br __designer:mapid="de" />
                <asp:TextBox ID="txtCto2" runat="server" Width="181px"></asp:TextBox>
                &nbsp;
                <asp:TextBox ID="txtFone2" runat="server" Width="150px"></asp:TextBox>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <asp:Button ID="btnAlterar" runat="server" onclick="btnAlterar_Click" 
                    Text="Alterar" />
                &nbsp;&nbsp;&nbsp;&nbsp;
                <asp:Button ID="btnExcluir" runat="server" style="background-color: #FF0000" 
                    Text="Excluir" />
                &nbsp;&nbsp;&nbsp;&nbsp;
                <asp:Button ID="btnNovo" runat="server" style="background-color: #FF0000" 
                    Text="Novo" Width="59px" />
                <br />
                <br />
                <br />
                <br />
                <br />
            </td>
        </tr>
    </table>
    </asp:Content>

